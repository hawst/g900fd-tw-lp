.class public final Lsamsung/database/sqlite/SecSQLiteConnectionPool;
.super Ljava/lang/Object;
.source "SecSQLiteConnectionPool.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;,
        Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CONNECTION_FLAG_INTERACTIVE:I = 0x4

.field public static final CONNECTION_FLAG_PRIMARY_CONNECTION_AFFINITY:I = 0x2

.field public static final CONNECTION_FLAG_READ_ONLY:I = 0x1

.field private static final CONNECTION_POOL_BUSY_MILLIS:J = 0xfa0L

.field private static final TAG:Ljava/lang/String; = "SecSQLiteConnectionPool"


# instance fields
.field private final mAcquiredConnections:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteConnection;",
            "Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final mAvailableNonPrimaryConnections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

.field private final mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

.field private final mConnectionLeaked:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mConnectionWaiterPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

.field private mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

.field private mIsOpen:Z

.field private final mLock:Ljava/lang/Object;

.field private mMaxConnectionPoolSize:I

.field private mNextConnectionId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    .locals 1
    .param p1, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    .line 79
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionLeaked:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    .line 146
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-direct {v0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .line 147
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->setMaxConnectionPoolSizeLocked()V

    .line 148
    return-void
.end method

.method static synthetic access$0(Lsamsung/database/sqlite/SecSQLiteConnectionPool;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V
    .locals 0

    .prologue
    .line 718
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->cancelConnectionWaiterLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V

    return-void
.end method

.method private cancelConnectionWaiterLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V
    .locals 3
    .param p1, "waiter"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .prologue
    .line 719
    iget-object v2, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mAssignedConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-nez v2, :cond_0

    iget-object v2, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mException:Ljava/lang/RuntimeException;

    if-eqz v2, :cond_1

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 725
    :cond_1
    const/4 v1, 0x0

    .line 726
    .local v1, "predecessor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 727
    .local v0, "current":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    :goto_1
    if-ne v0, p1, :cond_2

    .line 732
    if-eqz v1, :cond_4

    .line 733
    iget-object v2, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    iput-object v2, v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 739
    :goto_2
    new-instance v2, Landroid/os/OperationCanceledException;

    invoke-direct {v2}, Landroid/os/OperationCanceledException;-><init>()V

    iput-object v2, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mException:Ljava/lang/RuntimeException;

    .line 740
    iget-object v2, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mThread:Ljava/lang/Thread;

    invoke-static {v2}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    .line 743
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->wakeConnectionWaitersLocked()V

    goto :goto_0

    .line 728
    :cond_2
    sget-boolean v2, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    if-nez v0, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 729
    :cond_3
    move-object v1, v0

    .line 730
    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    goto :goto_1

    .line 735
    :cond_4
    iget-object v2, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    iput-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    goto :goto_2
.end method

.method private closeAvailableConnectionsAndLogExceptionsLocked()V
    .locals 1

    .prologue
    .line 507
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked()V

    .line 509
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    .line 511
    const/4 v0, 0x0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 513
    :cond_0
    return-void
.end method

.method private closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked()V
    .locals 3

    .prologue
    .line 517
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 518
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 521
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 522
    return-void

    .line 519
    :cond_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    .line 518
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V
    .locals 4
    .param p1, "connection"    # Lsamsung/database/sqlite/SecSQLiteConnection;

    .prologue
    .line 537
    :try_start_0
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->close()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    :goto_0
    return-void

    .line 538
    :catch_0
    move-exception v0

    .line 539
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v1, "SecSQLiteConnectionPool"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to close connection, its fate is now in the hands of the merciful GC: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 540
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 539
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private closeExcessConnectionsAndLogExceptionsLocked()V
    .locals 4

    .prologue
    .line 526
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 527
    .local v0, "availableCount":I
    :goto_0
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "availableCount":I
    .local v1, "availableCount":I
    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mMaxConnectionPoolSize:I

    add-int/lit8 v3, v3, -0x1

    if-gt v0, v3, :cond_0

    .line 532
    return-void

    .line 529
    :cond_0
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 530
    .local v2, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    move v0, v1

    .end local v1    # "availableCount":I
    .restart local v0    # "availableCount":I
    goto :goto_0
.end method

.method private discardAcquiredConnectionsLocked()V
    .locals 1

    .prologue
    .line 546
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;->DISCARD:Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->markAcquiredConnectionsLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;)V

    .line 547
    return-void
.end method

.method private dispose(Z)V
    .locals 5
    .param p1, "finalized"    # Z

    .prologue
    .line 237
    if-nez p1, :cond_1

    .line 242
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 243
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->throwIfClosedLocked()V

    .line 245
    const/4 v1, 0x0

    iput-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mIsOpen:Z

    .line 247
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeAvailableConnectionsAndLogExceptionsLocked()V

    .line 249
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I

    move-result v0

    .line 250
    .local v0, "pendingCount":I
    if-eqz v0, :cond_0

    .line 251
    const-string v1, "SecSQLiteConnectionPool"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The connection pool for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v4, v4, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 252
    const-string v4, " has been closed but there are still "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 253
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " connections in use.  They will be closed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 254
    const-string v4, "as they are released back to the pool."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 251
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->wakeConnectionWaitersLocked()V

    .line 242
    monitor-exit v2

    .line 260
    .end local v0    # "pendingCount":I
    :cond_1
    return-void

    .line 242
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private finishAcquireConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;I)V
    .locals 5
    .param p1, "connection"    # Lsamsung/database/sqlite/SecSQLiteConnection;
    .param p2, "connectionFlags"    # I

    .prologue
    .line 918
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 919
    .local v1, "readOnly":Z
    :goto_0
    :try_start_0
    invoke-virtual {p1, v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->setOnlyAllowReadOnlyOperations(Z)V

    .line 921
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    sget-object v3, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;->NORMAL:Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    invoke-virtual {v2, p1, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 928
    return-void

    .line 918
    .end local v1    # "readOnly":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 922
    .restart local v1    # "readOnly":Z
    :catch_0
    move-exception v0

    .line 923
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v2, "SecSQLiteConnectionPool"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to prepare acquired connection for session, closing it: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 924
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", connectionFlags="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 923
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    .line 926
    throw v0
.end method

.method private static getPriority(I)I
    .locals 1
    .param p0, "connectionFlags"    # I

    .prologue
    .line 955
    and-int/lit8 v0, p0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSessionBlockingImportantConnectionWaitersLocked(ZI)Z
    .locals 3
    .param p1, "holdingPrimaryConnection"    # Z
    .param p2, "connectionFlags"    # I

    .prologue
    .line 932
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 933
    .local v1, "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    if-eqz v1, :cond_1

    .line 934
    invoke-static {p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->getPriority(I)I

    move-result v0

    .line 937
    .local v0, "priority":I
    :cond_0
    iget v2, v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mPriority:I

    if-le v0, v2, :cond_2

    .line 951
    .end local v0    # "priority":I
    :cond_1
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 944
    .restart local v0    # "priority":I
    :cond_2
    if-nez p1, :cond_3

    iget-boolean v2, v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mWantPrimaryConnection:Z

    if-nez v2, :cond_4

    .line 945
    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    .line 948
    :cond_4
    iget-object v1, v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 949
    if-nez v1, :cond_0

    goto :goto_0
.end method

.method private logConnectionPoolBusyLocked(JI)V
    .locals 12
    .param p1, "waitMillis"    # J
    .param p3, "connectionFlags"    # I

    .prologue
    .line 748
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    .line 749
    .local v8, "thread":Ljava/lang/Thread;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 750
    .local v5, "msg":Ljava/lang/StringBuilder;
    const-string v9, "The connection pool for database \'"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v10, v10, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 751
    const-string v9, "\' has been unable to grant a connection to thread "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 752
    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v9

    invoke-virtual {v5, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 753
    const-string/jumbo v9, "with flags 0x"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 754
    const-string v9, " for "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    long-to-float v10, p1

    const v11, 0x3a83126f    # 0.001f

    mul-float/2addr v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " seconds.\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 757
    .local v7, "requests":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 758
    .local v0, "activeConnections":I
    const/4 v4, 0x0

    .line 759
    .local v4, "idleConnections":I
    iget-object v9, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v9}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 760
    iget-object v9, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v9}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_3

    .line 770
    :cond_0
    iget-object v9, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 771
    .local v1, "availableConnections":I
    iget-object v9, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v9, :cond_1

    .line 772
    add-int/lit8 v1, v1, 0x1

    .line 775
    :cond_1
    const-string v9, "Connections: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " active, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 776
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " idle, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " available.\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 779
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 780
    const-string v9, "\nRequests in progress:\n"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 781
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_5

    .line 786
    :cond_2
    const-string v9, "SecSQLiteConnectionPool"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    return-void

    .line 760
    .end local v1    # "availableConnections":I
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 761
    .local v2, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-virtual {v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->describeCurrentOperationUnsafe()Ljava/lang/String;

    move-result-object v3

    .line 762
    .local v3, "description":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 763
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 764
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 766
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 781
    .end local v2    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    .end local v3    # "description":Ljava/lang/String;
    .restart local v1    # "availableConnections":I
    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 782
    .local v6, "request":Ljava/lang/String;
    const-string v10, "  "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private markAcquiredConnectionsLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;)V
    .locals 7
    .param p1, "status"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    .prologue
    .line 581
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v5}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 582
    new-instance v2, Ljava/util/ArrayList;

    .line 583
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v5}, Ljava/util/WeakHashMap;->size()I

    move-result v5

    .line 582
    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 585
    .local v2, "keysToUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteConnection;>;"
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v5}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 584
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 592
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 593
    .local v4, "updateCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v4, :cond_3

    .line 597
    .end local v1    # "i":I
    .end local v2    # "keysToUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteConnection;>;"
    .end local v4    # "updateCount":I
    :cond_1
    return-void

    .line 585
    .restart local v2    # "keysToUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteConnection;>;"
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 586
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lsamsung/database/sqlite/SecSQLiteConnection;Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    .line 587
    .local v3, "oldStatus":Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;
    if-eq p1, v3, :cond_0

    .line 588
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;->DISCARD:Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    if-eq v3, v5, :cond_0

    .line 589
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 594
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lsamsung/database/sqlite/SecSQLiteConnection;Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;>;"
    .end local v3    # "oldStatus":Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;
    .restart local v1    # "i":I
    .restart local v4    # "updateCount":I
    :cond_3
    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v6, v5, p1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private obtainConnectionWaiterLocked(Ljava/lang/Thread;JIZLjava/lang/String;I)Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    .locals 3
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "startTime"    # J
    .param p4, "priority"    # I
    .param p5, "wantPrimaryConnection"    # Z
    .param p6, "sql"    # Ljava/lang/String;
    .param p7, "connectionFlags"    # I

    .prologue
    const/4 v2, 0x0

    .line 979
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 980
    .local v0, "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    if-eqz v0, :cond_0

    .line 981
    iget-object v1, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    iput-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 982
    iput-object v2, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 986
    :goto_0
    iput-object p1, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mThread:Ljava/lang/Thread;

    .line 987
    iput-wide p2, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mStartTime:J

    .line 988
    iput p4, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mPriority:I

    .line 989
    iput-boolean p5, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mWantPrimaryConnection:Z

    .line 990
    iput-object p6, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mSql:Ljava/lang/String;

    .line 991
    iput p7, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mConnectionFlags:I

    .line 992
    return-object v0

    .line 984
    :cond_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .end local v0    # "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    invoke-direct {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;-><init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V

    .restart local v0    # "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    goto :goto_0
.end method

.method public static open(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    .locals 3
    .param p0, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .prologue
    .line 168
    if-nez p0, :cond_0

    .line 169
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "configuration must not be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 173
    :cond_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-direct {v0, p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    .line 174
    .local v0, "pool":Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->open()V

    .line 175
    return-object v0
.end method

.method private open()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 203
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-direct {p0, v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->openConnectionLocked(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;Z)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 207
    iput-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mIsOpen:Z

    .line 208
    return-void
.end method

.method private openConnectionLocked(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;Z)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 2
    .param p1, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;
    .param p2, "primaryConnection"    # Z

    .prologue
    .line 463
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mNextConnectionId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mNextConnectionId:I

    .line 464
    .local v0, "connectionId":I
    invoke-static {p0, p1, v0, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->open(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;IZ)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v1

    return-object v1
.end method

.method public static openSecure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;[B)Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    .locals 3
    .param p0, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;
    .param p1, "password"    # [B

    .prologue
    .line 188
    const-string v1, "SecSQLiteConnectionPool"

    const-string v2, "OpenSecure"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    if-nez p0, :cond_0

    .line 190
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "configuration must not be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 194
    :cond_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-direct {v0, p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    .line 195
    .local v0, "pool":Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    invoke-direct {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->openSecure([B)V

    .line 196
    return-object v0
.end method

.method private openSecure([B)V
    .locals 3
    .param p1, "password"    # [B

    .prologue
    const/4 v2, 0x1

    .line 211
    const-string v0, "SecSQLiteConnectionPool"

    const-string v1, "OpenSecure with password"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-direct {p0, v0, v2, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->openSecureConnectionLocked(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;Z[B)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 216
    iput-boolean v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mIsOpen:Z

    .line 217
    return-void
.end method

.method private openSecureConnectionLocked(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;Z[B)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 3
    .param p1, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;
    .param p2, "primaryConnection"    # Z
    .param p3, "password"    # [B

    .prologue
    .line 470
    const-string v1, "SecSQLiteConnectionPool"

    const-string/jumbo v2, "openSecureConnectionLocked"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mNextConnectionId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mNextConnectionId:I

    .line 472
    .local v0, "connectionId":I
    invoke-static {p0, p1, v0, p2, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->openSecure(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;IZ[B)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v1

    return-object v1
.end method

.method private reconfigureAllConnectionsLocked()V
    .locals 8

    .prologue
    .line 551
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v5, :cond_0

    .line 553
    :try_start_0
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v5, v6}, Lsamsung/database/sqlite/SecSQLiteConnection;->reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    :cond_0
    :goto_0
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 563
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    :goto_1
    if-lt v4, v1, :cond_1

    .line 576
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;->RECONFIGURE:Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    invoke-direct {p0, v5}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->markAcquiredConnectionsLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;)V

    .line 577
    return-void

    .line 554
    .end local v1    # "count":I
    .end local v4    # "i":I
    :catch_0
    move-exception v2

    .line 555
    .local v2, "ex":Ljava/lang/RuntimeException;
    const-string v5, "SecSQLiteConnectionPool"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to reconfigure available primary connection, closing it: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 556
    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 555
    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 557
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-direct {p0, v5}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    .line 558
    const/4 v5, 0x0

    iput-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    goto :goto_0

    .line 564
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    .restart local v1    # "count":I
    .restart local v4    # "i":I
    :cond_1
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 566
    .local v0, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :try_start_1
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v0, v5}, Lsamsung/database/sqlite/SecSQLiteConnection;->reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move v3, v4

    .line 563
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_1

    .line 567
    :catch_1
    move-exception v2

    .line 568
    .restart local v2    # "ex":Ljava/lang/RuntimeException;
    const-string v5, "SecSQLiteConnectionPool"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to reconfigure available non-primary connection, closing it: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 569
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 568
    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 570
    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    .line 571
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    add-int/lit8 v3, v4, -0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 572
    add-int/lit8 v1, v1, -0x1

    goto :goto_2
.end method

.method private recycleConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;)Z
    .locals 4
    .param p1, "connection"    # Lsamsung/database/sqlite/SecSQLiteConnection;
    .param p2, "status"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    .prologue
    .line 395
    sget-object v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;->RECONFIGURE:Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    if-ne p2, v1, :cond_0

    .line 397
    :try_start_0
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {p1, v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :cond_0
    :goto_0
    sget-object v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;->DISCARD:Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    if-ne p2, v1, :cond_1

    .line 405
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    .line 406
    const/4 v1, 0x0

    .line 408
    :goto_1
    return v1

    .line 398
    :catch_0
    move-exception v0

    .line 399
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v1, "SecSQLiteConnectionPool"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to reconfigure released connection, closing it: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 400
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 399
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 401
    sget-object p2, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;->DISCARD:Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    goto :goto_0

    .line 408
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private recycleConnectionWaiterLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V
    .locals 2
    .param p1, "waiter"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .prologue
    const/4 v1, 0x0

    .line 996
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    iput-object v0, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 997
    iput-object v1, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mThread:Ljava/lang/Thread;

    .line 998
    iput-object v1, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mSql:Ljava/lang/String;

    .line 999
    iput-object v1, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mAssignedConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 1000
    iput-object v1, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mException:Ljava/lang/RuntimeException;

    .line 1001
    iget v0, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNonce:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNonce:I

    .line 1002
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 1003
    return-void
.end method

.method private setMaxConnectionPoolSizeLocked()V
    .locals 2

    .prologue
    .line 959
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v0, v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 960
    const/4 v0, 0x4

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mMaxConnectionPoolSize:I

    .line 968
    :goto_0
    return-void

    .line 966
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mMaxConnectionPoolSize:I

    goto :goto_0
.end method

.method private throwIfClosedLocked()V
    .locals 2

    .prologue
    .line 971
    iget-boolean v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mIsOpen:Z

    if-nez v0, :cond_0

    .line 972
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot perform this operation because the connection pool has been closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 975
    :cond_0
    return-void
.end method

.method private tryAcquireNonPrimaryConnectionLocked(Ljava/lang/String;I)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 6
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "connectionFlags"    # I

    .prologue
    .line 881
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 882
    .local v0, "availableCount":I
    const/4 v4, 0x1

    if-le v0, v4, :cond_0

    if-eqz p1, :cond_0

    .line 885
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_1

    .line 894
    .end local v2    # "i":I
    :cond_0
    if-lez v0, :cond_3

    .line 896
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    add-int/lit8 v5, v0, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 897
    .local v1, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-direct {p0, v1, p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->finishAcquireConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;I)V

    .line 912
    .end local v1    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :goto_1
    return-object v1

    .line 886
    .restart local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 887
    .restart local v1    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-virtual {v1, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->isPreparedStatementInCache(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 888
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 889
    invoke-direct {p0, v1, p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->finishAcquireConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;I)V

    goto :goto_1

    .line 885
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 902
    .end local v1    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    .end local v2    # "i":I
    :cond_3
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v4}, Ljava/util/WeakHashMap;->size()I

    move-result v3

    .line 903
    .local v3, "openConnections":I
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v4, :cond_4

    .line 904
    add-int/lit8 v3, v3, 0x1

    .line 906
    :cond_4
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mMaxConnectionPoolSize:I

    if-lt v3, v4, :cond_5

    .line 907
    const/4 v1, 0x0

    goto :goto_1

    .line 909
    :cond_5
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .line 910
    const/4 v5, 0x0

    .line 909
    invoke-direct {p0, v4, v5}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->openConnectionLocked(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;Z)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v1

    .line 911
    .restart local v1    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-direct {p0, v1, p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->finishAcquireConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;I)V

    goto :goto_1
.end method

.method private tryAcquirePrimaryConnectionLocked(I)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 5
    .param p1, "connectionFlags"    # I

    .prologue
    const/4 v2, 0x0

    .line 854
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 855
    .local v1, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    if-eqz v1, :cond_0

    .line 856
    iput-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 857
    invoke-direct {p0, v1, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->finishAcquireConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;I)V

    move-object v2, v1

    .line 873
    :goto_0
    return-object v2

    .line 862
    :cond_0
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v3}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 870
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .line 871
    const/4 v3, 0x1

    .line 870
    invoke-direct {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->openConnectionLocked(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;Z)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v1

    .line 872
    invoke-direct {p0, v1, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->finishAcquireConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;I)V

    move-object v2, v1

    .line 873
    goto :goto_0

    .line 862
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 863
    .local v0, "acquiredConnection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteConnection;->isPrimaryConnection()Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0
.end method

.method private waitForConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 24
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "connectionFlags"    # I
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 604
    and-int/lit8 v3, p2, 0x2

    if-eqz v3, :cond_4

    const/4 v8, 0x1

    .line 608
    .local v8, "wantPrimaryConnection":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    move-object/from16 v23, v0

    monitor-enter v23

    .line 609
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->throwIfClosedLocked()V

    .line 612
    if-eqz p3, :cond_0

    .line 613
    invoke-virtual/range {p3 .. p3}, Landroid/os/CancellationSignal;->throwIfCanceled()V

    .line 617
    :cond_0
    const/4 v13, 0x0

    .line 618
    .local v13, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    if-nez v8, :cond_1

    .line 619
    invoke-direct/range {p0 .. p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->tryAcquireNonPrimaryConnectionLocked(Ljava/lang/String;I)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v13

    .line 622
    :cond_1
    if-nez v13, :cond_2

    .line 623
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->tryAcquirePrimaryConnectionLocked(I)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v13

    .line 625
    :cond_2
    if-eqz v13, :cond_5

    .line 626
    monitor-exit v23

    .line 694
    :cond_3
    :goto_1
    return-object v13

    .line 604
    .end local v8    # "wantPrimaryConnection":Z
    .end local v13    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :cond_4
    const/4 v8, 0x0

    goto :goto_0

    .line 630
    .restart local v8    # "wantPrimaryConnection":Z
    .restart local v13    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :cond_5
    invoke-static/range {p2 .. p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->getPriority(I)I

    move-result v7

    .line 631
    .local v7, "priority":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 632
    .local v5, "startTime":J
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    move-object/from16 v3, p0

    move-object/from16 v9, p1

    move/from16 v10, p2

    invoke-direct/range {v3 .. v10}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->obtainConnectionWaiterLocked(Ljava/lang/Thread;JIZLjava/lang/String;I)Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    move-result-object v22

    .line 634
    .local v22, "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    const/16 v20, 0x0

    .line 635
    .local v20, "predecessor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    move-object/from16 v21, v0

    .line 636
    .local v21, "successor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    :goto_2
    if-nez v21, :cond_9

    .line 644
    :goto_3
    if-eqz v20, :cond_b

    .line 645
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    iput-object v0, v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 650
    :goto_4
    move-object/from16 v0, v22

    iget v0, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNonce:I

    move/from16 v17, v0

    .line 608
    .local v17, "nonce":I
    monitor-exit v23
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 654
    if-eqz p3, :cond_6

    .line 655
    new-instance v3, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v17

    invoke-direct {v3, v0, v1, v2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;-><init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;I)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 669
    :cond_6
    const-wide/16 v11, 0xfa0

    .line 670
    .local v11, "busyTimeoutMillis":J
    :try_start_1
    move-object/from16 v0, v22

    iget-wide v3, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mStartTime:J

    add-long v15, v3, v11

    .line 673
    .local v15, "nextBusyTimeoutTime":J
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionLeaked:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x1

    const/4 v9, 0x0

    invoke-virtual {v3, v4, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 674
    move-object/from16 v0, p0

    iget-object v4, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 675
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->wakeConnectionWaitersLocked()V

    .line 674
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 680
    :cond_7
    const-wide/32 v3, 0xf4240

    mul-long/2addr v3, v11

    :try_start_3
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(Ljava/lang/Object;J)V

    .line 683
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 686
    move-object/from16 v0, p0

    iget-object v4, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 687
    :try_start_4
    invoke-direct/range {p0 .. p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->throwIfClosedLocked()V

    .line 689
    move-object/from16 v0, v22

    iget-object v13, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mAssignedConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 690
    move-object/from16 v0, v22

    iget-object v14, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mException:Ljava/lang/RuntimeException;

    .line 691
    .local v14, "ex":Ljava/lang/RuntimeException;
    if-nez v13, :cond_8

    if-eqz v14, :cond_e

    .line 692
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->recycleConnectionWaiterLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V

    .line 693
    if-eqz v13, :cond_d

    .line 694
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 711
    if-eqz p3, :cond_3

    .line 712
    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    goto/16 :goto_1

    .line 637
    .end local v11    # "busyTimeoutMillis":J
    .end local v14    # "ex":Ljava/lang/RuntimeException;
    .end local v15    # "nextBusyTimeoutTime":J
    .end local v17    # "nonce":I
    :cond_9
    :try_start_5
    move-object/from16 v0, v21

    iget v3, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mPriority:I

    if-le v7, v3, :cond_a

    .line 638
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    iput-object v0, v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    goto :goto_3

    .line 608
    .end local v5    # "startTime":J
    .end local v7    # "priority":I
    .end local v13    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    .end local v20    # "predecessor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    .end local v21    # "successor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    .end local v22    # "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    :catchall_0
    move-exception v3

    monitor-exit v23
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v3

    .line 641
    .restart local v5    # "startTime":J
    .restart local v7    # "priority":I
    .restart local v13    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    .restart local v20    # "predecessor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    .restart local v21    # "successor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    .restart local v22    # "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    :cond_a
    move-object/from16 v20, v21

    .line 642
    :try_start_6
    move-object/from16 v0, v21

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    move-object/from16 v21, v0

    goto/16 :goto_2

    .line 647
    :cond_b
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_4

    .line 674
    .restart local v11    # "busyTimeoutMillis":J
    .restart local v15    # "nextBusyTimeoutTime":J
    .restart local v17    # "nonce":I
    :catchall_1
    move-exception v3

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 709
    .end local v15    # "nextBusyTimeoutTime":J
    :catchall_2
    move-exception v3

    .line 711
    if-eqz p3, :cond_c

    .line 712
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 714
    :cond_c
    throw v3

    .line 696
    .restart local v14    # "ex":Ljava/lang/RuntimeException;
    .restart local v15    # "nextBusyTimeoutTime":J
    :cond_d
    :try_start_9
    throw v14

    .line 686
    .end local v14    # "ex":Ljava/lang/RuntimeException;
    :catchall_3
    move-exception v3

    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :try_start_a
    throw v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 699
    .restart local v14    # "ex":Ljava/lang/RuntimeException;
    :cond_e
    :try_start_b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 700
    .local v18, "now":J
    cmp-long v3, v18, v15

    if-gez v3, :cond_f

    .line 701
    sub-long v11, v18, v15

    .line 686
    :goto_6
    monitor-exit v4

    goto/16 :goto_5

    .line 703
    :cond_f
    move-object/from16 v0, v22

    iget-wide v9, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mStartTime:J

    sub-long v9, v18, v9

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v9, v10, v1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->logConnectionPoolBusyLocked(JI)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 704
    const-wide/16 v11, 0xfa0

    .line 705
    add-long v15, v18, v11

    goto :goto_6
.end method

.method private wakeConnectionWaitersLocked()V
    .locals 10

    .prologue
    .line 794
    const/4 v3, 0x0

    .line 795
    .local v3, "predecessor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 796
    .local v7, "waiter":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    const/4 v4, 0x0

    .line 797
    .local v4, "primaryConnectionNotAvailable":Z
    const/4 v2, 0x0

    .line 798
    .local v2, "nonPrimaryConnectionNotAvailable":Z
    :goto_0
    if-nez v7, :cond_0

    .line 849
    :goto_1
    return-void

    .line 799
    :cond_0
    const/4 v6, 0x0

    .line 800
    .local v6, "unpark":Z
    iget-boolean v8, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mIsOpen:Z

    if-nez v8, :cond_2

    .line 801
    const/4 v6, 0x1

    .line 834
    :cond_1
    :goto_2
    iget-object v5, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 835
    .local v5, "successor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    if-eqz v6, :cond_7

    .line 836
    if-eqz v3, :cond_6

    .line 837
    iput-object v5, v3, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 841
    :goto_3
    const/4 v8, 0x0

    iput-object v8, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNext:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    .line 843
    iget-object v8, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mThread:Ljava/lang/Thread;

    invoke-static {v8}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    .line 847
    :goto_4
    move-object v7, v5

    goto :goto_0

    .line 804
    .end local v5    # "successor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    :cond_2
    const/4 v0, 0x0

    .line 805
    .local v0, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :try_start_0
    iget-boolean v8, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mWantPrimaryConnection:Z

    if-nez v8, :cond_3

    if-nez v2, :cond_3

    .line 807
    iget-object v8, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mSql:Ljava/lang/String;

    iget v9, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mConnectionFlags:I

    .line 806
    invoke-direct {p0, v8, v9}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->tryAcquireNonPrimaryConnectionLocked(Ljava/lang/String;I)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v0

    .line 808
    if-nez v0, :cond_3

    .line 809
    const/4 v2, 0x1

    .line 812
    :cond_3
    if-nez v0, :cond_4

    if-nez v4, :cond_4

    .line 814
    iget v8, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mConnectionFlags:I

    .line 813
    invoke-direct {p0, v8}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->tryAcquirePrimaryConnectionLocked(I)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v0

    .line 815
    if-nez v0, :cond_4

    .line 816
    const/4 v4, 0x1

    .line 819
    :cond_4
    if-eqz v0, :cond_5

    .line 820
    iput-object v0, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mAssignedConnection:Lsamsung/database/sqlite/SecSQLiteConnection;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 821
    const/4 v6, 0x1

    goto :goto_2

    .line 822
    :cond_5
    if-eqz v2, :cond_1

    if-eqz v4, :cond_1

    goto :goto_1

    .line 827
    :catch_0
    move-exception v1

    .line 829
    .local v1, "ex":Ljava/lang/RuntimeException;
    iput-object v1, v7, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mException:Ljava/lang/RuntimeException;

    .line 830
    const/4 v6, 0x1

    goto :goto_2

    .line 839
    .end local v0    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    .restart local v5    # "successor":Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;
    :cond_6
    iput-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionWaiterQueue:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    goto :goto_3

    .line 845
    :cond_7
    move-object v3, v7

    goto :goto_4
.end method


# virtual methods
.method public acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 1
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "connectionFlags"    # I
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 349
    invoke-direct {p0, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->waitForConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->dispose(Z)V

    .line 233
    return-void
.end method

.method public collectDbStats(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 445
    .local p1, "dbStatsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;>;"
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 446
    :try_start_0
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v1, :cond_0

    .line 447
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v1, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->collectDbStats(Ljava/util/ArrayList;)V

    .line 450
    :cond_0
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 454
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 445
    monitor-exit v2

    .line 458
    return-void

    .line 450
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 451
    .local v0, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->collectDbStats(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 445
    .end local v0    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 454
    :cond_2
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 455
    .restart local v0    # "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->collectDbStatsUnsafe(Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public dump(Landroid/util/Printer;Z)V
    .locals 0
    .param p1, "printer"    # Landroid/util/Printer;
    .param p2, "verbose"    # Z

    .prologue
    .line 1012
    return-void
.end method

.method public exportDB(Ljava/lang/String;)V
    .locals 3
    .param p1, "attachedDB"    # Ljava/lang/String;

    .prologue
    .line 313
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 314
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->throwIfClosedLocked()V

    .line 317
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Release all active connections before starting DB export"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 322
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked()V

    .line 323
    sget-boolean v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 325
    :cond_1
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->exportDB(Ljava/lang/String;)V

    .line 313
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 153
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->dispose(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 157
    return-void

    .line 154
    :catchall_0
    move-exception v0

    .line 155
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 156
    throw v0
.end method

.method onConnectionLeaked()V
    .locals 3

    .prologue
    .line 497
    const-string v0, "SecSQLiteConnectionPool"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "A SecSQLiteConnection object for database \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 498
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v2, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' was leaked!  Please fix your application "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 499
    const-string/jumbo v2, "to end transactions in progress properly and to close the database "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 500
    const-string/jumbo v2, "when it is no longer needed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 497
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConnectionLeaked:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 503
    return-void
.end method

.method public reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    .locals 4
    .param p1, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .prologue
    .line 276
    if-nez p1, :cond_0

    .line 277
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "configuration must not be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 280
    :cond_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 281
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->throwIfClosedLocked()V

    .line 282
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v1, v1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    iget v3, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    if-eq v1, v3, :cond_1

    .line 291
    const/4 v1, 0x1

    .line 290
    invoke-direct {p0, p1, v1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->openConnectionLocked(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;Z)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v0

    .line 293
    .local v0, "newPrimaryConnection":Lsamsung/database/sqlite/SecSQLiteConnection;
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeAvailableConnectionsAndLogExceptionsLocked()V

    .line 294
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->discardAcquiredConnectionsLocked()V

    .line 296
    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 297
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v1, p1}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->updateParametersFrom(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    .line 298
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->setMaxConnectionPoolSizeLocked()V

    .line 308
    .end local v0    # "newPrimaryConnection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :goto_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->wakeConnectionWaitersLocked()V

    .line 280
    monitor-exit v2

    .line 310
    return-void

    .line 301
    :cond_1
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v1, p1}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->updateParametersFrom(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    .line 302
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->setMaxConnectionPoolSizeLocked()V

    .line 304
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeExcessConnectionsAndLogExceptionsLocked()V

    .line 305
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->reconfigureAllConnectionsLocked()V

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public releaseConnection(Lsamsung/database/sqlite/SecSQLiteConnection;)V
    .locals 4
    .param p1, "connection"    # Lsamsung/database/sqlite/SecSQLiteConnection;

    .prologue
    .line 365
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 366
    :try_start_0
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;

    .line 367
    .local v0, "status":Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;
    if-nez v0, :cond_0

    .line 368
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v3, "Cannot perform this operation because the specified connection was not acquired from this pool or has already been released."

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 365
    .end local v0    # "status":Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 373
    .restart local v0    # "status":Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;
    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mIsOpen:Z

    if-nez v1, :cond_1

    .line 374
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    .line 365
    :goto_0
    monitor-exit v2

    .line 390
    return-void

    .line 375
    :cond_1
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->isPrimaryConnection()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 376
    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->recycleConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 377
    sget-boolean v1, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 378
    :cond_2
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailablePrimaryConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 380
    :cond_3
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->wakeConnectionWaitersLocked()V

    goto :goto_0

    .line 381
    :cond_4
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mMaxConnectionPoolSize:I

    add-int/lit8 v3, v3, -0x1

    if-lt v1, v3, :cond_5

    .line 382
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->closeConnectionAndLogExceptionsLocked(Lsamsung/database/sqlite/SecSQLiteConnection;)V

    goto :goto_0

    .line 384
    :cond_5
    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->recycleConnectionLocked(Lsamsung/database/sqlite/SecSQLiteConnection;Lsamsung/database/sqlite/SecSQLiteConnectionPool$AcquiredConnectionStatus;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 385
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAvailableNonPrimaryConnections:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    :cond_6
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->wakeConnectionWaitersLocked()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public shouldYieldConnection(Lsamsung/database/sqlite/SecSQLiteConnection;I)Z
    .locals 3
    .param p1, "connection"    # Lsamsung/database/sqlite/SecSQLiteConnection;
    .param p2, "connectionFlags"    # I

    .prologue
    .line 423
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 424
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mAcquiredConnections:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot perform this operation because the specified connection was not acquired from this pool or has already been released."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 430
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mIsOpen:Z

    if-nez v0, :cond_1

    .line 431
    monitor-exit v1

    const/4 v0, 0x0

    .line 434
    :goto_0
    return v0

    .line 435
    :cond_1
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->isPrimaryConnection()Z

    move-result v0

    .line 434
    invoke-direct {p0, v0, p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->isSessionBlockingImportantConnectionWaitersLocked(ZI)Z

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1017
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SecSQLiteConnectionPool: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v1, v1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
