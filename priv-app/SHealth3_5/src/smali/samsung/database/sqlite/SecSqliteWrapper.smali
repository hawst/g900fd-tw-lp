.class public final Lsamsung/database/sqlite/SecSqliteWrapper;
.super Ljava/lang/Object;
.source "SecSqliteWrapper.java"


# static fields
.field private static final SQLITE_EXCEPTION_DETAIL_MESSAGE:Ljava/lang/String; = "unable to open database file"

.field private static final TAG:Ljava/lang/String; = "SecSqliteWrapper"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method public static checkSecSQLiteException(Landroid/content/Context;Lsamsung/database/sqlite/SecSQLiteException;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "e"    # Lsamsung/database/sqlite/SecSQLiteException;

    .prologue
    .line 48
    invoke-static {p1}, Lsamsung/database/sqlite/SecSqliteWrapper;->isLowMemory(Lsamsung/database/sqlite/SecSQLiteException;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "com.android.internal.R.string.low_memory"

    .line 51
    const/4 v1, 0x0

    .line 50
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 55
    return-void

    .line 53
    :cond_0
    throw p1
.end method

.method public static delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 92
    :try_start_0
    invoke-virtual {p1, p2, p3, p4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 96
    :goto_0
    return v1

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Lsamsung/database/sqlite/SecSQLiteException;
    const-string v1, "SecSqliteWrapper"

    const-string v2, "Catch a SecSQLiteException when delete: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 95
    invoke-static {p0, v0}, Lsamsung/database/sqlite/SecSqliteWrapper;->checkSecSQLiteException(Landroid/content/Context;Lsamsung/database/sqlite/SecSQLiteException;)V

    .line 96
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static insert(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 103
    :try_start_0
    invoke-virtual {p1, p2, p3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 107
    :goto_0
    return-object v1

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Lsamsung/database/sqlite/SecSQLiteException;
    const-string v1, "SecSqliteWrapper"

    const-string v2, "Catch a SecSQLiteException when insert: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 106
    invoke-static {p0, v0}, Lsamsung/database/sqlite/SecSqliteWrapper;->checkSecSQLiteException(Landroid/content/Context;Lsamsung/database/sqlite/SecSQLiteException;)V

    .line 107
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isLowMemory(Lsamsung/database/sqlite/SecSQLiteException;)Z
    .locals 2
    .param p0, "e"    # Lsamsung/database/sqlite/SecSQLiteException;

    .prologue
    .line 44
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to open database file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 60
    :try_start_0
    invoke-virtual/range {p1 .. p6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 64
    :goto_0
    return-object v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Lsamsung/database/sqlite/SecSQLiteException;
    const-string v1, "SecSqliteWrapper"

    const-string v2, "Catch a SecSQLiteException when query: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 63
    invoke-static {p0, v0}, Lsamsung/database/sqlite/SecSqliteWrapper;->checkSecSQLiteException(Landroid/content/Context;Lsamsung/database/sqlite/SecSQLiteException;)V

    .line 64
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static requery(Landroid/content/Context;Landroid/database/Cursor;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 71
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->requery()Z
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 75
    :goto_0
    return v1

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Lsamsung/database/sqlite/SecSQLiteException;
    const-string v1, "SecSqliteWrapper"

    const-string v2, "Catch a SecSQLiteException when requery: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 74
    invoke-static {p0, v0}, Lsamsung/database/sqlite/SecSqliteWrapper;->checkSecSQLiteException(Landroid/content/Context;Lsamsung/database/sqlite/SecSQLiteException;)V

    .line 75
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "where"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 81
    :try_start_0
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 85
    :goto_0
    return v1

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Lsamsung/database/sqlite/SecSQLiteException;
    const-string v1, "SecSqliteWrapper"

    const-string v2, "Catch a SecSQLiteException when update: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 84
    invoke-static {p0, v0}, Lsamsung/database/sqlite/SecSqliteWrapper;->checkSecSQLiteException(Landroid/content/Context;Lsamsung/database/sqlite/SecSQLiteException;)V

    .line 85
    const/4 v1, -0x1

    goto :goto_0
.end method
