.class public final Lsamsung/database/sqlite/SecSQLiteStatement;
.super Lsamsung/database/sqlite/SecSQLiteProgram;
.source "SecSQLiteStatement.java"


# direct methods
.method constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "sql"    # Ljava/lang/String;
    .param p3, "bindArgs"    # [Ljava/lang/Object;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lsamsung/database/sqlite/SecSQLiteProgram;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V

    .line 32
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 6

    .prologue
    .line 42
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->acquireReference()V

    .line 44
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getBindArgs()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getConnectionFlags()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteSession;->execute(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)V
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 51
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteStatement;->onCorruption(I)V

    .line 47
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 48
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catchall_0
    move-exception v1

    .line 49
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 50
    throw v1
.end method

.method public executeInsert()J
    .locals 6

    .prologue
    .line 84
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->acquireReference()V

    .line 86
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    .line 87
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getBindArgs()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getConnectionFlags()I

    move-result v4

    const/4 v5, 0x0

    .line 86
    invoke-virtual {v1, v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteSession;->executeForLastInsertedRowId(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)J
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    .line 92
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 86
    return-wide v1

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteStatement;->onCorruption(I)V

    .line 90
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catchall_0
    move-exception v1

    .line 92
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 93
    throw v1
.end method

.method public executeUpdateDelete()I
    .locals 6

    .prologue
    .line 62
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->acquireReference()V

    .line 64
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    .line 65
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getBindArgs()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getConnectionFlags()I

    move-result v4

    const/4 v5, 0x0

    .line 64
    invoke-virtual {v1, v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteSession;->executeForChangedRowCount(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)I
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 70
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 64
    return v1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteStatement;->onCorruption(I)V

    .line 68
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catchall_0
    move-exception v1

    .line 70
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 71
    throw v1
.end method

.method public simpleQueryForBlobFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 6

    .prologue
    .line 164
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->acquireReference()V

    .line 166
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    .line 167
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getBindArgs()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getConnectionFlags()I

    move-result v4

    const/4 v5, 0x0

    .line 166
    invoke-virtual {v1, v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteSession;->executeForBlobFileDescriptor(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 172
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 166
    return-object v1

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->onCorruption()V

    .line 170
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catchall_0
    move-exception v1

    .line 172
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 173
    throw v1
.end method

.method public simpleQueryForIntegrityCheck()Ljava/lang/String;
    .locals 6

    .prologue
    .line 143
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->acquireReference()V

    .line 145
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    .line 146
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getBindArgs()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getConnectionFlags()I

    move-result v4

    const/4 v5, 0x0

    .line 145
    invoke-virtual {v1, v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteSession;->executeForString(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Ljava/lang/String;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 151
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 149
    :goto_0
    return-object v1

    .line 147
    :catch_0
    move-exception v0

    .line 151
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 149
    const-string v1, "false"

    goto :goto_0

    .line 150
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catchall_0
    move-exception v1

    .line 151
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 152
    throw v1
.end method

.method public simpleQueryForLong()J
    .locals 6

    .prologue
    .line 105
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->acquireReference()V

    .line 107
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    .line 108
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getBindArgs()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getConnectionFlags()I

    move-result v4

    const/4 v5, 0x0

    .line 107
    invoke-virtual {v1, v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteSession;->executeForLong(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)J
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    .line 113
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 107
    return-wide v1

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteStatement;->onCorruption(I)V

    .line 111
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catchall_0
    move-exception v1

    .line 113
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 114
    throw v1
.end method

.method public simpleQueryForString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 126
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->acquireReference()V

    .line 128
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    .line 129
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getBindArgs()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getConnectionFlags()I

    move-result v4

    const/4 v5, 0x0

    .line 128
    invoke-virtual {v1, v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteSession;->executeForString(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Ljava/lang/String;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 134
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 128
    return-object v1

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteStatement;->onCorruption(I)V

    .line 132
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catchall_0
    move-exception v1

    .line 134
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->releaseReference()V

    .line 135
    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SecSQLiteProgram: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteStatement;->getSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
