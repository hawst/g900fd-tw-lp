.class public abstract Lsamsung/database/SecAbstractWindowedCursor;
.super Landroid/database/AbstractCursor;
.source "SecAbstractWindowedCursor.java"


# instance fields
.field protected mWindow:Lsamsung/database/SecCursorWindow;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkPosition()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Landroid/database/AbstractCursor;->checkPosition()V

    .line 144
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Landroid/database/StaleDataException;

    const-string v1, "Attempting to access a closed CursorWindow.Most probable cause: cursor is deactivated prior to calling this method."

    invoke-direct {v0, v1}, Landroid/database/StaleDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    return-void
.end method

.method protected clearOrCreateWindow(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 206
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Lsamsung/database/SecCursorWindow;

    invoke-direct {v0, p1}, Lsamsung/database/SecCursorWindow;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v0}, Lsamsung/database/SecCursorWindow;->clear()V

    goto :goto_0
.end method

.method protected closeWindow()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v0}, Lsamsung/database/SecCursorWindow;->close()V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    .line 196
    :cond_0
    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 2
    .param p1, "columnIndex"    # I
    .param p2, "buffer"    # Landroid/database/CharArrayBuffer;

    .prologue
    .line 62
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 63
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1, p2}, Lsamsung/database/SecCursorWindow;->copyStringToBuffer(IILandroid/database/CharArrayBuffer;)V

    .line 64
    return-void
.end method

.method public getBlob(I)[B
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 50
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 51
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getBlob(II)[B

    move-result-object v0

    return-object v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 92
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 93
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getDouble(II)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 86
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 87
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getFloat(II)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 74
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 75
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getInt(II)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 80
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 81
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getLong(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSecWindow()Lsamsung/database/SecCursorWindow;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    return-object v0
.end method

.method public getShort(I)S
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 68
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 69
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getShort(II)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 56
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 57
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(I)I
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 136
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 137
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getType(II)I

    move-result v0

    return v0
.end method

.method public getWindow()Landroid/database/CursorWindow;
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasWindow()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBlob(I)Z
    .locals 2
    .param p1, "columnIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lsamsung/database/SecAbstractWindowedCursor;->getType(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFloat(I)Z
    .locals 2
    .param p1, "columnIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lsamsung/database/SecAbstractWindowedCursor;->getType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLong(I)Z
    .locals 2
    .param p1, "columnIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 123
    invoke-virtual {p0, p1}, Lsamsung/database/SecAbstractWindowedCursor;->getType(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 98
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->checkPosition()V

    .line 99
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    iget v1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mPos:I

    invoke-virtual {v0, v1, p1}, Lsamsung/database/SecCursorWindow;->getType(II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isString(I)Z
    .locals 2
    .param p1, "columnIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lsamsung/database/SecAbstractWindowedCursor;->getType(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setWindow(Lsamsung/database/SecCursorWindow;)V
    .locals 1
    .param p1, "window"    # Lsamsung/database/SecCursorWindow;

    .prologue
    .line 172
    iget-object v0, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-eq p1, v0, :cond_0

    .line 173
    invoke-virtual {p0}, Lsamsung/database/SecAbstractWindowedCursor;->closeWindow()V

    .line 174
    iput-object p1, p0, Lsamsung/database/SecAbstractWindowedCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    .line 176
    :cond_0
    return-void
.end method
