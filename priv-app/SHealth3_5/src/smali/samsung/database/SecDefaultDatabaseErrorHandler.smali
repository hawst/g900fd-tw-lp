.class public final Lsamsung/database/SecDefaultDatabaseErrorHandler;
.super Ljava/lang/Object;
.source "SecDefaultDatabaseErrorHandler.java"

# interfaces
.implements Lsamsung/database/SecDatabaseErrorHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "SecDefaultDatabaseErrorHandler"


# instance fields
.field private err_msg:[Ljava/lang/String;

.field private err_num:[I

.field private suffix:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->err_num:[I

    .line 52
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ".corrupt"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ".back"

    aput-object v2, v0, v1

    iput-object v0, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->err_msg:[Ljava/lang/String;

    .line 53
    const-string v0, ".unknown"

    iput-object v0, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->suffix:Ljava/lang/String;

    .line 44
    return-void

    .line 51
    :array_0
    .array-data 4
        0xb
        0x1a
    .end array-data
.end method

.method private backupDatabaseFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 110
    const-string v2, ":memory:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const-string v2, "SecDefaultDatabaseErrorHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "backup the database file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .local v1, "f":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->suffix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    .end local v1    # "f":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SecDefaultDatabaseErrorHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "backup failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onCorruption(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 7
    .param p1, "dbObj"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 60
    const-string v4, "SecDefaultDatabaseErrorHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Corruption reported by sqlite on database: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getCorruptCode()I

    move-result v1

    .line 63
    .local v1, "err_code":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->err_num:[I

    array-length v4, v4

    if-lt v2, v4, :cond_1

    .line 70
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isOpen()Z

    move-result v4

    if-nez v4, :cond_3

    .line 77
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;->backupDatabaseFile(Ljava/lang/String;)V

    .line 107
    :cond_0
    :goto_1
    return-void

    .line 64
    :cond_1
    iget-object v4, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->err_num:[I

    aget v4, v4, v2

    if-ne v4, v1, :cond_2

    .line 65
    iget-object v4, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->err_msg:[Ljava/lang/String;

    aget-object v4, v4, v2

    iput-object v4, p0, Lsamsung/database/SecDefaultDatabaseErrorHandler;->suffix:Ljava/lang/String;

    .line 63
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    :cond_3
    const/4 v0, 0x0

    .line 86
    .local v0, "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :try_start_0
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getAttachedDbs()Ljava/util/List;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 91
    :goto_2
    :try_start_1
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V
    :try_end_1
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    :goto_3
    if-eqz v0, :cond_6

    .line 98
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 99
    .local v3, "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;->backupDatabaseFile(Ljava/lang/String;)V

    goto :goto_4

    .line 95
    .end local v3    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    move-object v5, v4

    .line 97
    if-eqz v0, :cond_5

    .line 98
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 106
    :goto_6
    throw v5

    .line 98
    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 99
    .restart local v3    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;->backupDatabaseFile(Ljava/lang/String;)V

    goto :goto_5

    .line 104
    .end local v3    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_5
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;->backupDatabaseFile(Ljava/lang/String;)V

    goto :goto_6

    :cond_6
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;->backupDatabaseFile(Ljava/lang/String;)V

    goto :goto_1

    .line 87
    :catch_0
    move-exception v4

    goto :goto_2

    .line 92
    :catch_1
    move-exception v4

    goto :goto_3
.end method
