.class public Lcom/sec/android/service/health/contentservice/HealthServiceNotification;
.super Ljava/lang/Object;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private mNotification:Landroid/app/Notification$Builder;

.field private mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 42

    const/16 v0, 0x27

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, -0x3d

    aput v40, v1, v39

    const/16 v39, 0x4301

    aput v39, v1, v38

    const/16 v38, -0x12d6

    aput v38, v1, v37

    const/16 v37, -0x67

    aput v37, v1, v36

    const/16 v36, 0x3458

    aput v36, v1, v35

    const/16 v35, -0x7a9

    aput v35, v1, v34

    const/16 v34, -0x6f

    aput v34, v1, v33

    const/16 v33, -0x37

    aput v33, v1, v32

    const/16 v32, -0x4a

    aput v32, v1, v31

    const/16 v31, 0x4a55

    aput v31, v1, v30

    const/16 v30, 0x4d25

    aput v30, v1, v29

    const/16 v29, -0x14fd

    aput v29, v1, v28

    const/16 v28, -0x72

    aput v28, v1, v27

    const/16 v27, 0x2815

    aput v27, v1, v26

    const/16 v26, 0x4741

    aput v26, v1, v25

    const/16 v25, 0x7931

    aput v25, v1, v24

    const/16 v24, 0x260b

    aput v24, v1, v23

    const/16 v23, 0x7d43

    aput v23, v1, v22

    const/16 v22, -0x74d2

    aput v22, v1, v21

    const/16 v21, -0x1d

    aput v21, v1, v20

    const/16 v20, -0xbd7

    aput v20, v1, v19

    const/16 v19, -0x68

    aput v19, v1, v18

    const/16 v18, 0x51e

    aput v18, v1, v17

    const/16 v17, -0x55a0

    aput v17, v1, v16

    const/16 v16, -0x1e

    aput v16, v1, v15

    const/16 v15, -0x1b

    aput v15, v1, v14

    const/16 v14, -0x269e

    aput v14, v1, v13

    const/16 v13, -0x4a

    aput v13, v1, v12

    const/4 v12, -0x6

    aput v12, v1, v11

    const/4 v11, -0x7

    aput v11, v1, v10

    const/16 v10, -0x1e

    aput v10, v1, v9

    const/16 v9, -0x17

    aput v9, v1, v8

    const/16 v8, 0x1553

    aput v8, v1, v7

    const/16 v7, -0x3a9f

    aput v7, v1, v6

    const/16 v6, -0x57

    aput v6, v1, v5

    const/16 v5, 0x564d

    aput v5, v1, v4

    const/16 v4, -0x2bcd

    aput v4, v1, v3

    const/16 v3, -0x64

    aput v3, v1, v2

    const/16 v2, 0x54a

    aput v2, v1, v0

    const/16 v0, 0x27

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, -0x53

    aput v41, v0, v40

    const/16 v40, 0x436e

    aput v40, v0, v39

    const/16 v39, -0x12bd

    aput v39, v0, v38

    const/16 v38, -0x13

    aput v38, v0, v37

    const/16 v37, 0x3439

    aput v37, v0, v36

    const/16 v36, -0x7cc

    aput v36, v0, v35

    const/16 v35, -0x8

    aput v35, v0, v34

    const/16 v34, -0x51

    aput v34, v0, v33

    const/16 v33, -0x21

    aput v33, v0, v32

    const/16 v32, 0x4a21

    aput v32, v0, v31

    const/16 v31, 0x4d4a

    aput v31, v0, v30

    const/16 v30, -0x14b3

    aput v30, v0, v29

    const/16 v29, -0x15

    aput v29, v0, v28

    const/16 v28, 0x2876

    aput v28, v0, v27

    const/16 v27, 0x4728

    aput v27, v0, v26

    const/16 v26, 0x7947

    aput v26, v0, v25

    const/16 v25, 0x2679

    aput v25, v0, v24

    const/16 v24, 0x7d26

    aput v24, v0, v23

    const/16 v23, -0x7483

    aput v23, v0, v22

    const/16 v22, -0x75

    aput v22, v0, v21

    const/16 v21, -0xba3

    aput v21, v0, v20

    const/16 v20, -0xc

    aput v20, v0, v19

    const/16 v19, 0x57f

    aput v19, v0, v18

    const/16 v18, -0x55fb

    aput v18, v0, v17

    const/16 v17, -0x56

    aput v17, v0, v16

    const/16 v16, -0x48

    aput v16, v0, v15

    const/16 v15, -0x26f0

    aput v15, v0, v14

    const/16 v14, -0x27

    aput v14, v0, v13

    const/16 v13, -0x77

    aput v13, v0, v12

    const/16 v12, -0x69

    aput v12, v0, v11

    const/16 v11, -0x79

    aput v11, v0, v10

    const/16 v10, -0x46

    aput v10, v0, v9

    const/16 v9, 0x153b

    aput v9, v0, v8

    const/16 v8, -0x3aeb

    aput v8, v0, v7

    const/16 v7, -0x3b

    aput v7, v0, v6

    const/16 v6, 0x562c

    aput v6, v0, v5

    const/16 v5, -0x2baa

    aput v5, v0, v4

    const/16 v4, -0x2c

    aput v4, v0, v3

    const/16 v3, 0x511

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->init()V

    return-void
.end method

.method private init()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->context:Landroid/content/Context;

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, -0x12

    aput v15, v2, v14

    const/16 v14, 0x3133

    aput v14, v2, v13

    const/16 v13, -0x57a8

    aput v13, v2, v12

    const/16 v12, -0x24

    aput v12, v2, v11

    const/16 v11, -0x16ae

    aput v11, v2, v10

    const/16 v10, -0x76

    aput v10, v2, v9

    const/16 v9, -0x128b

    aput v9, v2, v8

    const/16 v8, -0x75

    aput v8, v2, v7

    const/16 v7, -0x48ab

    aput v7, v2, v6

    const/16 v6, -0x3d

    aput v6, v2, v5

    const/16 v5, -0x2f8e

    aput v5, v2, v3

    const/16 v3, -0x42

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x80

    aput v16, v1, v15

    const/16 v15, 0x315c

    aput v15, v1, v14

    const/16 v14, -0x57cf

    aput v14, v1, v13

    const/16 v13, -0x58

    aput v13, v1, v12

    const/16 v12, -0x16cd

    aput v12, v1, v11

    const/16 v11, -0x17

    aput v11, v1, v10

    const/16 v10, -0x12e4

    aput v10, v1, v9

    const/16 v9, -0x13

    aput v9, v1, v8

    const/16 v8, -0x48c4

    aput v8, v1, v7

    const/16 v7, -0x49

    aput v7, v1, v6

    const/16 v6, -0x2fe3

    aput v6, v1, v5

    const/16 v5, -0x30

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotificationManager:Landroid/app/NotificationManager;

    new-instance v1, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public cancelNoti(I)V
    .locals 43

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x2ea038a31947782dL    # 4.17498801630287E-84

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x2ea038a31947782dL    # 4.17498801630287E-84

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotificationManager:Landroid/app/NotificationManager;

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x6926

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x6916

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_5

    const-wide v5, 0x2ea038a31947782dL    # 4.17498801630287E-84

    xor-long/2addr v1, v5

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-virtual {v4, v1}, Landroid/app/NotificationManager;->cancel(I)V

    :goto_2
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, -0x2a

    aput v41, v2, v40

    const/16 v40, -0x798e

    aput v40, v2, v39

    const/16 v39, -0x11

    aput v39, v2, v38

    const/16 v38, -0x29

    aput v38, v2, v37

    const/16 v37, -0x77cc

    aput v37, v2, v36

    const/16 v36, -0x15

    aput v36, v2, v35

    const/16 v35, -0x4c9e

    aput v35, v2, v34

    const/16 v34, -0x2b

    aput v34, v2, v33

    const/16 v33, -0x6ac0

    aput v33, v2, v32

    const/16 v32, -0x1f

    aput v32, v2, v31

    const/16 v31, -0x419f

    aput v31, v2, v30

    const/16 v30, -0x10

    aput v30, v2, v29

    const/16 v29, -0x51

    aput v29, v2, v28

    const/16 v28, -0x3d

    aput v28, v2, v27

    const/16 v27, -0x3d

    aput v27, v2, v26

    const/16 v26, 0x5f

    aput v26, v2, v25

    const/16 v25, -0x218e

    aput v25, v2, v24

    const/16 v24, -0x45

    aput v24, v2, v23

    const/16 v23, 0x4b24

    aput v23, v2, v22

    const/16 v22, 0x4523

    aput v22, v2, v21

    const/16 v21, -0x4acf

    aput v21, v2, v20

    const/16 v20, -0x27

    aput v20, v2, v19

    const/16 v19, -0x7bce

    aput v19, v2, v18

    const/16 v18, -0x1f

    aput v18, v2, v17

    const/16 v17, -0x34

    aput v17, v2, v16

    const/16 v16, -0x14

    aput v16, v2, v15

    const/16 v15, -0x76

    aput v15, v2, v14

    const/16 v14, 0x190d

    aput v14, v2, v13

    const/16 v13, -0x4496

    aput v13, v2, v12

    const/16 v12, -0x2b

    aput v12, v2, v11

    const/16 v11, 0x777e

    aput v11, v2, v10

    const/16 v10, -0x37dc

    aput v10, v2, v9

    const/16 v9, -0x60

    aput v9, v2, v8

    const/16 v8, -0x5e

    aput v8, v2, v7

    const/16 v7, -0x24

    aput v7, v2, v6

    const/16 v6, -0x6b

    aput v6, v2, v5

    const/16 v5, 0x416f

    aput v5, v2, v4

    const/16 v4, -0x45f7

    aput v4, v2, v3

    const/16 v3, -0x1f

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, -0x48

    aput v42, v1, v41

    const/16 v41, -0x79e3

    aput v41, v1, v40

    const/16 v40, -0x7a

    aput v40, v1, v39

    const/16 v39, -0x5d

    aput v39, v1, v38

    const/16 v38, -0x77ab

    aput v38, v1, v37

    const/16 v37, -0x78

    aput v37, v1, v36

    const/16 v36, -0x4cf5

    aput v36, v1, v35

    const/16 v35, -0x4d

    aput v35, v1, v34

    const/16 v34, -0x6ad7

    aput v34, v1, v33

    const/16 v33, -0x6b

    aput v33, v1, v32

    const/16 v32, -0x41f2

    aput v32, v1, v31

    const/16 v31, -0x42

    aput v31, v1, v30

    const/16 v30, -0x36

    aput v30, v1, v29

    const/16 v29, -0x60

    aput v29, v1, v28

    const/16 v28, -0x56

    aput v28, v1, v27

    const/16 v27, 0x29

    aput v27, v1, v26

    const/16 v26, -0x2200

    aput v26, v1, v25

    const/16 v25, -0x22

    aput v25, v1, v24

    const/16 v24, 0x4b77

    aput v24, v1, v23

    const/16 v23, 0x454b

    aput v23, v1, v22

    const/16 v22, -0x4abb

    aput v22, v1, v21

    const/16 v21, -0x4b

    aput v21, v1, v20

    const/16 v20, -0x7bad

    aput v20, v1, v19

    const/16 v19, -0x7c

    aput v19, v1, v18

    const/16 v18, -0x7c

    aput v18, v1, v17

    const/16 v17, -0x4f

    aput v17, v1, v16

    const/16 v16, -0x8

    aput v16, v1, v15

    const/16 v15, 0x1962

    aput v15, v1, v14

    const/16 v14, -0x44e7

    aput v14, v1, v13

    const/16 v13, -0x45

    aput v13, v1, v12

    const/16 v12, 0x771b

    aput v12, v1, v11

    const/16 v11, -0x3789

    aput v11, v1, v10

    const/16 v10, -0x38

    aput v10, v1, v9

    const/16 v9, -0x2a

    aput v9, v1, v8

    const/16 v8, -0x50

    aput v8, v1, v7

    const/16 v7, -0xc

    aput v7, v1, v6

    const/16 v6, 0x410a

    aput v6, v1, v5

    const/16 v5, -0x45bf

    aput v5, v1, v4

    const/16 v4, -0x46

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, -0x50

    aput v30, v2, v29

    const/16 v29, -0x67

    aput v29, v2, v28

    const/16 v28, -0x37

    aput v28, v2, v27

    const/16 v27, -0x6

    aput v27, v2, v26

    const/16 v26, -0x63

    aput v26, v2, v25

    const/16 v25, -0x2f

    aput v25, v2, v24

    const/16 v24, -0x5ea1

    aput v24, v2, v23

    const/16 v23, -0x33

    aput v23, v2, v22

    const/16 v22, 0x1472

    aput v22, v2, v21

    const/16 v21, 0x4a7a

    aput v21, v2, v20

    const/16 v20, 0x716a

    aput v20, v2, v19

    const/16 v19, 0x2

    aput v19, v2, v18

    const/16 v18, -0x2a97

    aput v18, v2, v17

    const/16 v17, -0xb

    aput v17, v2, v16

    const/16 v16, 0x5f41

    aput v16, v2, v15

    const/16 v15, -0x61d0

    aput v15, v2, v14

    const/16 v14, -0x9

    aput v14, v2, v13

    const/16 v13, -0x3c

    aput v13, v2, v12

    const/16 v12, -0x4f

    aput v12, v2, v11

    const/16 v11, 0x4b09

    aput v11, v2, v10

    const/16 v10, 0x1822

    aput v10, v2, v9

    const/16 v9, -0x1f82

    aput v9, v2, v8

    const/16 v8, -0x77

    aput v8, v2, v7

    const/16 v7, 0x643e

    aput v7, v2, v6

    const/16 v6, -0x5cf5

    aput v6, v2, v5

    const/16 v5, -0x13

    aput v5, v2, v3

    const/16 v3, -0x1d

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0x2b

    aput v31, v1, v30

    const/16 v30, -0x14

    aput v30, v1, v29

    const/16 v29, -0x5b

    aput v29, v1, v28

    const/16 v28, -0x65

    aput v28, v1, v27

    const/16 v27, -0x15

    aput v27, v1, v26

    const/16 v26, -0xf

    aput v26, v1, v25

    const/16 v25, -0x5ecd

    aput v25, v1, v24

    const/16 v24, -0x5f

    aput v24, v1, v23

    const/16 v23, 0x1407

    aput v23, v1, v22

    const/16 v22, 0x4a14

    aput v22, v1, v21

    const/16 v21, 0x714a

    aput v21, v1, v20

    const/16 v20, 0x71

    aput v20, v1, v19

    const/16 v19, -0x2b00

    aput v19, v1, v18

    const/16 v18, -0x2b

    aput v18, v1, v17

    const/16 v17, 0x5f2f

    aput v17, v1, v16

    const/16 v16, -0x61a1

    aput v16, v1, v15

    const/16 v15, -0x62

    aput v15, v1, v14

    const/16 v14, -0x50

    aput v14, v1, v13

    const/16 v13, -0x30

    aput v13, v1, v12

    const/16 v12, 0x4b6a

    aput v12, v1, v11

    const/16 v11, 0x184b

    aput v11, v1, v10

    const/16 v10, -0x1fe8

    aput v10, v1, v9

    const/16 v9, -0x20

    aput v9, v1, v8

    const/16 v8, 0x644a

    aput v8, v1, v7

    const/16 v7, -0x5c9c

    aput v7, v1, v6

    const/16 v6, -0x5d

    aput v6, v1, v5

    const/16 v5, -0x72

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6
.end method

.method public setNoti(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 43

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p4

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x2aa8759905897bd0L    # -1.3181252709403458E103

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x2aa8759905897bd0L    # -1.3181252709403458E103

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0xf

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x3f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_5

    const-wide v5, -0x2aa8759905897bd0L    # -1.3181252709403458E103

    xor-long/2addr v1, v5

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-virtual {v4, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    :goto_2
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, -0x71ea

    aput v41, v2, v40

    const/16 v40, -0x1f

    aput v40, v2, v39

    const/16 v39, -0x57dc

    aput v39, v2, v38

    const/16 v38, -0x24

    aput v38, v2, v37

    const/16 v37, -0x4d

    aput v37, v2, v36

    const/16 v36, -0x35

    aput v36, v2, v35

    const/16 v35, -0x45de

    aput v35, v2, v34

    const/16 v34, -0x24

    aput v34, v2, v33

    const/16 v33, -0x519b

    aput v33, v2, v32

    const/16 v32, -0x26

    aput v32, v2, v31

    const/16 v31, -0x2f

    aput v31, v2, v30

    const/16 v30, -0x5e

    aput v30, v2, v29

    const/16 v29, -0xf

    aput v29, v2, v28

    const/16 v28, -0x30

    aput v28, v2, v27

    const/16 v27, -0x38

    aput v27, v2, v26

    const/16 v26, -0x69

    aput v26, v2, v25

    const/16 v25, -0x6a82

    aput v25, v2, v24

    const/16 v24, -0x10

    aput v24, v2, v23

    const/16 v23, -0x51

    aput v23, v2, v22

    const/16 v22, -0x30

    aput v22, v2, v21

    const/16 v21, -0x599f

    aput v21, v2, v20

    const/16 v20, -0x36

    aput v20, v2, v19

    const/16 v19, 0x6e5d

    aput v19, v2, v18

    const/16 v18, 0x6f0b

    aput v18, v2, v17

    const/16 v17, 0x327

    aput v17, v2, v16

    const/16 v16, -0xaa2

    aput v16, v2, v15

    const/16 v15, -0x79

    aput v15, v2, v14

    const/16 v14, 0x5c6b

    aput v14, v2, v13

    const/16 v13, 0x742f

    aput v13, v2, v12

    const/16 v12, -0x2de6

    aput v12, v2, v11

    const/16 v11, -0x49

    aput v11, v2, v10

    const/16 v10, -0x39

    aput v10, v2, v9

    const/16 v9, 0x4074

    aput v9, v2, v8

    const/16 v8, -0x60cc

    aput v8, v2, v7

    const/16 v7, -0xd

    aput v7, v2, v6

    const/16 v6, -0x4490

    aput v6, v2, v5

    const/16 v5, -0x22

    aput v5, v2, v4

    const/16 v4, -0x1a8e

    aput v4, v2, v3

    const/16 v3, -0x42

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, -0x7188

    aput v42, v1, v41

    const/16 v41, -0x72

    aput v41, v1, v40

    const/16 v40, -0x57b3

    aput v40, v1, v39

    const/16 v39, -0x58

    aput v39, v1, v38

    const/16 v38, -0x2e

    aput v38, v1, v37

    const/16 v37, -0x58

    aput v37, v1, v36

    const/16 v36, -0x45b5

    aput v36, v1, v35

    const/16 v35, -0x46

    aput v35, v1, v34

    const/16 v34, -0x51f4

    aput v34, v1, v33

    const/16 v33, -0x52

    aput v33, v1, v32

    const/16 v32, -0x42

    aput v32, v1, v31

    const/16 v31, -0x14

    aput v31, v1, v30

    const/16 v30, -0x6c

    aput v30, v1, v29

    const/16 v29, -0x4d

    aput v29, v1, v28

    const/16 v28, -0x5f

    aput v28, v1, v27

    const/16 v27, -0x1f

    aput v27, v1, v26

    const/16 v26, -0x6af4

    aput v26, v1, v25

    const/16 v25, -0x6b

    aput v25, v1, v24

    const/16 v24, -0x4

    aput v24, v1, v23

    const/16 v23, -0x48

    aput v23, v1, v22

    const/16 v22, -0x59eb

    aput v22, v1, v21

    const/16 v21, -0x5a

    aput v21, v1, v20

    const/16 v20, 0x6e3c

    aput v20, v1, v19

    const/16 v19, 0x6f6e

    aput v19, v1, v18

    const/16 v18, 0x36f

    aput v18, v1, v17

    const/16 v17, -0xafd

    aput v17, v1, v16

    const/16 v16, -0xb

    aput v16, v1, v15

    const/16 v15, 0x5c04

    aput v15, v1, v14

    const/16 v14, 0x745c

    aput v14, v1, v13

    const/16 v13, -0x2d8c

    aput v13, v1, v12

    const/16 v12, -0x2e

    aput v12, v1, v11

    const/16 v11, -0x6c

    aput v11, v1, v10

    const/16 v10, 0x401c

    aput v10, v1, v9

    const/16 v9, -0x60c0

    aput v9, v1, v8

    const/16 v8, -0x61

    aput v8, v1, v7

    const/16 v7, -0x44ef

    aput v7, v1, v6

    const/16 v6, -0x45

    aput v6, v1, v5

    const/16 v5, -0x1ac6

    aput v5, v1, v4

    const/16 v4, -0x1b

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x5a02

    aput v30, v2, v29

    const/16 v29, 0x742f

    aput v29, v2, v28

    const/16 v28, -0x72e8

    aput v28, v2, v27

    const/16 v27, -0x14

    aput v27, v2, v26

    const/16 v26, -0x68

    aput v26, v2, v25

    const/16 v25, -0x64

    aput v25, v2, v24

    const/16 v24, -0x41

    aput v24, v2, v23

    const/16 v23, -0x2d5

    aput v23, v2, v22

    const/16 v22, -0x78

    aput v22, v2, v21

    const/16 v21, -0x1fd0

    aput v21, v2, v20

    const/16 v20, -0x40

    aput v20, v2, v19

    const/16 v19, -0x35

    aput v19, v2, v18

    const/16 v18, -0x4eff

    aput v18, v2, v17

    const/16 v17, -0x6f

    aput v17, v2, v16

    const/16 v16, -0x29

    aput v16, v2, v15

    const/16 v15, -0x3abc

    aput v15, v2, v14

    const/16 v14, -0x54

    aput v14, v2, v13

    const/16 v13, -0x2f

    aput v13, v2, v12

    const/16 v12, 0x2550

    aput v12, v2, v11

    const/16 v11, -0x3bba

    aput v11, v2, v10

    const/16 v10, -0x53

    aput v10, v2, v9

    const/16 v9, -0x76

    aput v9, v2, v8

    const/16 v8, -0x9d6

    aput v8, v2, v7

    const/16 v7, -0x7e

    aput v7, v2, v6

    const/4 v6, -0x5

    aput v6, v2, v5

    const/16 v5, -0x2c

    aput v5, v2, v3

    const/4 v3, -0x1

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x5a67

    aput v31, v1, v30

    const/16 v30, 0x745a

    aput v30, v1, v29

    const/16 v29, -0x728c

    aput v29, v1, v28

    const/16 v28, -0x73

    aput v28, v1, v27

    const/16 v27, -0x12

    aput v27, v1, v26

    const/16 v26, -0x44

    aput v26, v1, v25

    const/16 v25, -0x2d

    aput v25, v1, v24

    const/16 v24, -0x2b9

    aput v24, v1, v23

    const/16 v23, -0x3

    aput v23, v1, v22

    const/16 v22, -0x1fa2

    aput v22, v1, v21

    const/16 v21, -0x20

    aput v21, v1, v20

    const/16 v20, -0x48

    aput v20, v1, v19

    const/16 v19, -0x4e98

    aput v19, v1, v18

    const/16 v18, -0x4f

    aput v18, v1, v17

    const/16 v17, -0x47

    aput v17, v1, v16

    const/16 v16, -0x3ad5

    aput v16, v1, v15

    const/16 v15, -0x3b

    aput v15, v1, v14

    const/16 v14, -0x5b

    aput v14, v1, v13

    const/16 v13, 0x2531

    aput v13, v1, v12

    const/16 v12, -0x3bdb

    aput v12, v1, v11

    const/16 v11, -0x3c

    aput v11, v1, v10

    const/16 v10, -0x14

    aput v10, v1, v9

    const/16 v9, -0x9bd

    aput v9, v1, v8

    const/16 v8, -0xa

    aput v8, v1, v7

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, -0x66

    aput v6, v1, v5

    const/16 v5, -0x6e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6
.end method

.method public setPendingIntent(Landroid/app/PendingIntent;)V
    .locals 43

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, -0x2f93

    aput v41, v2, v40

    const/16 v40, -0x41

    aput v40, v2, v39

    const/16 v39, 0x7f1f

    aput v39, v2, v38

    const/16 v38, 0x340b

    aput v38, v2, v37

    const/16 v37, 0x2955

    aput v37, v2, v36

    const/16 v36, 0x184a

    aput v36, v2, v35

    const/16 v35, -0x4d8f

    aput v35, v2, v34

    const/16 v34, -0x2c

    aput v34, v2, v33

    const/16 v33, -0x68

    aput v33, v2, v32

    const/16 v32, -0x2aa3

    aput v32, v2, v31

    const/16 v31, -0x46

    aput v31, v2, v30

    const/16 v30, -0x5a

    aput v30, v2, v29

    const/16 v29, 0x7259

    aput v29, v2, v28

    const/16 v28, 0x3d11

    aput v28, v2, v27

    const/16 v27, -0x21ac

    aput v27, v2, v26

    const/16 v26, -0x58

    aput v26, v2, v25

    const/16 v25, 0x380b

    aput v25, v2, v24

    const/16 v24, -0x59a3

    aput v24, v2, v23

    const/16 v23, -0xb

    aput v23, v2, v22

    const/16 v22, -0x77

    aput v22, v2, v21

    const/16 v21, -0x36

    aput v21, v2, v20

    const/16 v20, -0x578f

    aput v20, v2, v19

    const/16 v19, -0x37

    aput v19, v2, v18

    const/16 v18, -0x66

    aput v18, v2, v17

    const/16 v17, -0x58

    aput v17, v2, v16

    const/16 v16, -0x4b

    aput v16, v2, v15

    const/16 v15, -0x6ad

    aput v15, v2, v14

    const/16 v14, -0x6a

    aput v14, v2, v13

    const/16 v13, -0x7d

    aput v13, v2, v12

    const/16 v12, -0x35

    aput v12, v2, v11

    const/16 v11, 0xe77

    aput v11, v2, v10

    const/16 v10, -0x8a3

    aput v10, v2, v9

    const/16 v9, -0x61

    aput v9, v2, v8

    const/16 v8, -0x42ec

    aput v8, v2, v7

    const/16 v7, -0x2f

    aput v7, v2, v6

    const/16 v6, -0x28

    aput v6, v2, v5

    const/16 v5, -0x5b

    aput v5, v2, v4

    const/16 v4, 0x541c

    aput v4, v2, v3

    const/16 v3, -0x65f1

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, -0x2ffd

    aput v42, v1, v41

    const/16 v41, -0x30

    aput v41, v1, v40

    const/16 v40, 0x7f76

    aput v40, v1, v39

    const/16 v39, 0x347f

    aput v39, v1, v38

    const/16 v38, 0x2934

    aput v38, v1, v37

    const/16 v37, 0x1829

    aput v37, v1, v36

    const/16 v36, -0x4de8

    aput v36, v1, v35

    const/16 v35, -0x4e

    aput v35, v1, v34

    const/16 v34, -0xf

    aput v34, v1, v33

    const/16 v33, -0x2ad7

    aput v33, v1, v32

    const/16 v32, -0x2b

    aput v32, v1, v31

    const/16 v31, -0x18

    aput v31, v1, v30

    const/16 v30, 0x723c

    aput v30, v1, v29

    const/16 v29, 0x3d72

    aput v29, v1, v28

    const/16 v28, -0x21c3

    aput v28, v1, v27

    const/16 v27, -0x22

    aput v27, v1, v26

    const/16 v26, 0x3879

    aput v26, v1, v25

    const/16 v25, -0x59c8

    aput v25, v1, v24

    const/16 v24, -0x5a

    aput v24, v1, v23

    const/16 v23, -0x1f

    aput v23, v1, v22

    const/16 v22, -0x42

    aput v22, v1, v21

    const/16 v21, -0x57e3

    aput v21, v1, v20

    const/16 v20, -0x58

    aput v20, v1, v19

    const/16 v19, -0x1

    aput v19, v1, v18

    const/16 v18, -0x20

    aput v18, v1, v17

    const/16 v17, -0x18

    aput v17, v1, v16

    const/16 v16, -0x6df

    aput v16, v1, v15

    const/4 v15, -0x7

    aput v15, v1, v14

    const/16 v14, -0x10

    aput v14, v1, v13

    const/16 v13, -0x5b

    aput v13, v1, v12

    const/16 v12, 0xe12

    aput v12, v1, v11

    const/16 v11, -0x8f2

    aput v11, v1, v10

    const/16 v10, -0x9

    aput v10, v1, v9

    const/16 v9, -0x42a0

    aput v9, v1, v8

    const/16 v8, -0x43

    aput v8, v1, v7

    const/16 v7, -0x47

    aput v7, v1, v6

    const/16 v6, -0x40

    aput v6, v1, v5

    const/16 v5, 0x5454

    aput v5, v1, v4

    const/16 v4, -0x65ac

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, -0x4ba5

    aput v30, v2, v29

    const/16 v29, -0x3f

    aput v29, v2, v28

    const/16 v28, -0x6c

    aput v28, v2, v27

    const/16 v27, -0x1293

    aput v27, v2, v26

    const/16 v26, -0x65

    aput v26, v2, v25

    const/16 v25, 0x3533

    aput v25, v2, v24

    const/16 v24, -0x34a7

    aput v24, v2, v23

    const/16 v23, -0x59

    aput v23, v2, v22

    const/16 v22, -0x73

    aput v22, v2, v21

    const/16 v21, -0x73

    aput v21, v2, v20

    const/16 v20, -0x7c98

    aput v20, v2, v19

    const/16 v19, -0x10

    aput v19, v2, v18

    const/16 v18, 0x5f6f

    aput v18, v2, v17

    const/16 v17, 0x687f

    aput v17, v2, v16

    const/16 v16, 0x5406

    aput v16, v2, v15

    const/16 v15, -0x40c5

    aput v15, v2, v14

    const/16 v14, -0x2a

    aput v14, v2, v13

    const/16 v13, -0x55e8

    aput v13, v2, v12

    const/16 v12, -0x35

    aput v12, v2, v11

    const/16 v11, 0x3e21

    aput v11, v2, v10

    const/16 v10, 0xa57

    aput v10, v2, v9

    const/16 v9, 0x506c

    aput v9, v2, v8

    const/16 v8, 0x6a39

    aput v8, v2, v7

    const/16 v7, -0x67e2

    aput v7, v2, v6

    const/16 v6, -0x9

    aput v6, v2, v5

    const/16 v5, 0x13e

    aput v5, v2, v3

    const/16 v3, -0x4994

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0x4bc2

    aput v31, v1, v30

    const/16 v30, -0x4c

    aput v30, v1, v29

    const/16 v29, -0x8

    aput v29, v1, v28

    const/16 v28, -0x12f4

    aput v28, v1, v27

    const/16 v27, -0x13

    aput v27, v1, v26

    const/16 v26, 0x3513

    aput v26, v1, v25

    const/16 v25, -0x34cb

    aput v25, v1, v24

    const/16 v24, -0x35

    aput v24, v1, v23

    const/16 v23, -0x8

    aput v23, v1, v22

    const/16 v22, -0x1d

    aput v22, v1, v21

    const/16 v21, -0x7cb8

    aput v21, v1, v20

    const/16 v20, -0x7d

    aput v20, v1, v19

    const/16 v19, 0x5f06

    aput v19, v1, v18

    const/16 v18, 0x685f

    aput v18, v1, v17

    const/16 v17, 0x5468

    aput v17, v1, v16

    const/16 v16, -0x40ac

    aput v16, v1, v15

    const/16 v15, -0x41

    aput v15, v1, v14

    const/16 v14, -0x5594

    aput v14, v1, v13

    const/16 v13, -0x56

    aput v13, v1, v12

    const/16 v12, 0x3e42

    aput v12, v1, v11

    const/16 v11, 0xa3e

    aput v11, v1, v10

    const/16 v10, 0x500a

    aput v10, v1, v9

    const/16 v9, 0x6a50

    aput v9, v1, v8

    const/16 v8, -0x6796

    aput v8, v1, v7

    const/16 v7, -0x68

    aput v7, v1, v6

    const/16 v6, 0x170

    aput v6, v1, v5

    const/16 v5, -0x49ff

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method

.method public startNoti(I)V
    .locals 55

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x668c617d467d5005L    # 9.64744358455158E185

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x668c617d467d5005L    # 9.64744358455158E185

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotificationManager:Landroid/app/NotificationManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotificationManager:Landroid/app/NotificationManager;

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x2a

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x1a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v1

    move-object v4, v1

    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, -0x6281

    aput v42, v2, v41

    const/16 v41, -0xe

    aput v41, v2, v40

    const/16 v40, -0x4ef6

    aput v40, v2, v39

    const/16 v39, -0x3b

    aput v39, v2, v38

    const/16 v38, -0x5183

    aput v38, v2, v37

    const/16 v37, -0x33

    aput v37, v2, v36

    const/16 v36, 0x5b1a

    aput v36, v2, v35

    const/16 v35, -0xdc3

    aput v35, v2, v34

    const/16 v34, -0x65

    aput v34, v2, v33

    const/16 v33, -0x32

    aput v33, v2, v32

    const/16 v32, -0x27

    aput v32, v2, v31

    const/16 v31, -0x56fd

    aput v31, v2, v30

    const/16 v30, -0x34

    aput v30, v2, v29

    const/16 v29, 0x16f

    aput v29, v2, v28

    const/16 v28, -0x6198

    aput v28, v2, v27

    const/16 v27, -0x18

    aput v27, v2, v26

    const/16 v26, -0x3b

    aput v26, v2, v25

    const/16 v25, -0x7d

    aput v25, v2, v24

    const/16 v24, 0x2e53

    aput v24, v2, v23

    const/16 v23, 0x5d46

    aput v23, v2, v22

    const/16 v22, -0x4ed7

    aput v22, v2, v21

    const/16 v21, -0x23

    aput v21, v2, v20

    const/16 v20, -0x6a

    aput v20, v2, v19

    const/16 v19, 0x1f66

    aput v19, v2, v18

    const/16 v18, -0x64a9

    aput v18, v2, v17

    const/16 v17, -0x3a

    aput v17, v2, v16

    const/16 v16, -0x6c

    aput v16, v2, v15

    const/16 v15, 0x3d02

    aput v15, v2, v14

    const/16 v14, 0x4f4e

    aput v14, v2, v13

    const/16 v13, 0x1421

    aput v13, v2, v12

    const/16 v12, -0x758f

    aput v12, v2, v11

    const/16 v11, -0x27

    aput v11, v2, v10

    const/16 v10, -0x15

    aput v10, v2, v9

    const/16 v9, -0x59cc

    aput v9, v2, v8

    const/16 v8, -0x36

    aput v8, v2, v7

    const/16 v7, -0x24aa

    aput v7, v2, v6

    const/16 v6, -0x42

    aput v6, v2, v5

    const/16 v5, -0x62

    aput v5, v2, v3

    const/16 v3, 0x3f4c

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, -0x62ef

    aput v43, v1, v42

    const/16 v42, -0x63

    aput v42, v1, v41

    const/16 v41, -0x4e9d

    aput v41, v1, v40

    const/16 v40, -0x4f

    aput v40, v1, v39

    const/16 v39, -0x51e4

    aput v39, v1, v38

    const/16 v38, -0x52

    aput v38, v1, v37

    const/16 v37, 0x5b73

    aput v37, v1, v36

    const/16 v36, -0xda5

    aput v36, v1, v35

    const/16 v35, -0xe

    aput v35, v1, v34

    const/16 v34, -0x46

    aput v34, v1, v33

    const/16 v33, -0x4a

    aput v33, v1, v32

    const/16 v32, -0x56b3

    aput v32, v1, v31

    const/16 v31, -0x57

    aput v31, v1, v30

    const/16 v30, 0x10c

    aput v30, v1, v29

    const/16 v29, -0x61ff

    aput v29, v1, v28

    const/16 v28, -0x62

    aput v28, v1, v27

    const/16 v27, -0x49

    aput v27, v1, v26

    const/16 v26, -0x1a

    aput v26, v1, v25

    const/16 v25, 0x2e00

    aput v25, v1, v24

    const/16 v24, 0x5d2e

    aput v24, v1, v23

    const/16 v23, -0x4ea3

    aput v23, v1, v22

    const/16 v22, -0x4f

    aput v22, v1, v21

    const/16 v21, -0x9

    aput v21, v1, v20

    const/16 v20, 0x1f03

    aput v20, v1, v19

    const/16 v19, -0x64e1

    aput v19, v1, v18

    const/16 v18, -0x65

    aput v18, v1, v17

    const/16 v17, -0x1a

    aput v17, v1, v16

    const/16 v16, 0x3d6d

    aput v16, v1, v15

    const/16 v15, 0x4f3d

    aput v15, v1, v14

    const/16 v14, 0x144f

    aput v14, v1, v13

    const/16 v13, -0x75ec

    aput v13, v1, v12

    const/16 v12, -0x76

    aput v12, v1, v11

    const/16 v11, -0x7d

    aput v11, v1, v10

    const/16 v10, -0x59c0

    aput v10, v1, v9

    const/16 v9, -0x5a

    aput v9, v1, v8

    const/16 v8, -0x24c9

    aput v8, v1, v7

    const/16 v7, -0x25

    aput v7, v1, v6

    const/16 v6, -0x2a

    aput v6, v1, v5

    const/16 v5, 0x3f17

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    return-void

    :cond_2
    :try_start_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_5

    const-wide v5, 0x668c617d467d5005L    # 9.64744358455158E185

    xor-long/2addr v1, v5

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthServiceNotification;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_4

    :catch_1
    move-exception v1

    :goto_5
    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, -0x71

    aput v41, v2, v40

    const/16 v40, -0x2bb9

    aput v40, v2, v39

    const/16 v39, -0x43

    aput v39, v2, v38

    const/16 v38, 0x1a3c

    aput v38, v2, v37

    const/16 v37, 0x4c7b

    aput v37, v2, v36

    const/16 v36, -0x51d1

    aput v36, v2, v35

    const/16 v35, -0x39

    aput v35, v2, v34

    const/16 v34, 0x556b

    aput v34, v2, v33

    const/16 v33, 0x6e3c

    aput v33, v2, v32

    const/16 v32, -0x5e6

    aput v32, v2, v31

    const/16 v31, -0x6b

    aput v31, v2, v30

    const/16 v30, 0x1041

    aput v30, v2, v29

    const/16 v29, 0x1875

    aput v29, v2, v28

    const/16 v28, 0x777b

    aput v28, v2, v27

    const/16 v27, 0x11e

    aput v27, v2, v26

    const/16 v26, 0x77

    aput v26, v2, v25

    const/16 v25, 0x5272

    aput v25, v2, v24

    const/16 v24, -0x79c9

    aput v24, v2, v23

    const/16 v23, -0x2b

    aput v23, v2, v22

    const/16 v22, -0x11

    aput v22, v2, v21

    const/16 v21, -0x35

    aput v21, v2, v20

    const/16 v20, 0x351f

    aput v20, v2, v19

    const/16 v19, -0x39ac

    aput v19, v2, v18

    const/16 v18, -0x5d

    aput v18, v2, v17

    const/16 v17, -0x1a

    aput v17, v2, v16

    const/16 v16, -0x1cf4

    aput v16, v2, v15

    const/16 v15, -0x6f

    aput v15, v2, v14

    const/16 v14, -0x6fe1

    aput v14, v2, v13

    const/16 v13, -0x1d

    aput v13, v2, v12

    const/16 v12, -0x51

    aput v12, v2, v11

    const/16 v11, -0x45f4

    aput v11, v2, v10

    const/16 v10, -0x17

    aput v10, v2, v9

    const/16 v9, 0x6339

    aput v9, v2, v8

    const/16 v8, -0x7ee9

    aput v8, v2, v7

    const/16 v7, -0x13

    aput v7, v2, v6

    const/16 v6, 0x6b31

    aput v6, v2, v5

    const/16 v5, 0x630e

    aput v5, v2, v4

    const/16 v4, 0x742b

    aput v4, v2, v3

    const/16 v3, 0x482f

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, -0x1f

    aput v42, v1, v41

    const/16 v41, -0x2bd8

    aput v41, v1, v40

    const/16 v40, -0x2c

    aput v40, v1, v39

    const/16 v39, 0x1a48

    aput v39, v1, v38

    const/16 v38, 0x4c1a

    aput v38, v1, v37

    const/16 v37, -0x51b4

    aput v37, v1, v36

    const/16 v36, -0x52

    aput v36, v1, v35

    const/16 v35, 0x550d

    aput v35, v1, v34

    const/16 v34, 0x6e55

    aput v34, v1, v33

    const/16 v33, -0x592

    aput v33, v1, v32

    const/16 v32, -0x6

    aput v32, v1, v31

    const/16 v31, 0x100f

    aput v31, v1, v30

    const/16 v30, 0x1810

    aput v30, v1, v29

    const/16 v29, 0x7718

    aput v29, v1, v28

    const/16 v28, 0x177

    aput v28, v1, v27

    const/16 v27, 0x1

    aput v27, v1, v26

    const/16 v26, 0x5200

    aput v26, v1, v25

    const/16 v25, -0x79ae

    aput v25, v1, v24

    const/16 v24, -0x7a

    aput v24, v1, v23

    const/16 v23, -0x79

    aput v23, v1, v22

    const/16 v22, -0x41

    aput v22, v1, v21

    const/16 v21, 0x3573

    aput v21, v1, v20

    const/16 v20, -0x39cb

    aput v20, v1, v19

    const/16 v19, -0x3a

    aput v19, v1, v18

    const/16 v18, -0x52

    aput v18, v1, v17

    const/16 v17, -0x1caf

    aput v17, v1, v16

    const/16 v16, -0x1d

    aput v16, v1, v15

    const/16 v15, -0x6f90

    aput v15, v1, v14

    const/16 v14, -0x70

    aput v14, v1, v13

    const/16 v13, -0x3f

    aput v13, v1, v12

    const/16 v12, -0x4597

    aput v12, v1, v11

    const/16 v11, -0x46

    aput v11, v1, v10

    const/16 v10, 0x6351

    aput v10, v1, v9

    const/16 v9, -0x7e9d

    aput v9, v1, v8

    const/16 v8, -0x7f

    aput v8, v1, v7

    const/16 v7, 0x6b50

    aput v7, v1, v6

    const/16 v6, 0x636b

    aput v6, v1, v5

    const/16 v5, 0x7463

    aput v5, v1, v4

    const/16 v4, 0x4874

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x32

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, 0x2f

    const/16 v51, 0x30

    const/16 v52, 0x31

    const/16 v53, -0x67

    aput v53, v2, v52

    const/16 v52, -0x3f

    aput v52, v2, v51

    const/16 v51, -0x14

    aput v51, v2, v50

    const/16 v50, -0x52d6

    aput v50, v2, v49

    const/16 v49, -0x25

    aput v49, v2, v48

    const/16 v48, -0x6fde

    aput v48, v2, v47

    const/16 v47, -0x4

    aput v47, v2, v46

    const/16 v46, -0x3c

    aput v46, v2, v45

    const/16 v45, -0x68

    aput v45, v2, v44

    const/16 v44, -0x8

    aput v44, v2, v43

    const/16 v43, 0x7a34

    aput v43, v2, v42

    const/16 v42, -0x25e1

    aput v42, v2, v41

    const/16 v41, -0x58

    aput v41, v2, v40

    const/16 v40, -0x71

    aput v40, v2, v39

    const/16 v39, -0x32

    aput v39, v2, v38

    const/16 v38, -0x26

    aput v38, v2, v37

    const/16 v37, -0x72a1

    aput v37, v2, v36

    const/16 v36, -0x1c

    aput v36, v2, v35

    const/16 v35, -0x59

    aput v35, v2, v34

    const/16 v34, -0x8b0

    aput v34, v2, v33

    const/16 v33, -0x6c

    aput v33, v2, v32

    const/16 v32, -0x6ccb

    aput v32, v2, v31

    const/16 v31, -0xb

    aput v31, v2, v30

    const/16 v30, 0x6f49

    aput v30, v2, v29

    const/16 v29, 0x1a1b

    aput v29, v2, v28

    const/16 v28, 0x3175

    aput v28, v2, v27

    const/16 v27, -0x4a81

    aput v27, v2, v26

    const/16 v26, -0x28

    aput v26, v2, v25

    const/16 v25, -0xb9a

    aput v25, v2, v24

    const/16 v24, -0x28

    aput v24, v2, v23

    const/16 v23, -0x75

    aput v23, v2, v22

    const/16 v22, -0x7d

    aput v22, v2, v21

    const/16 v21, -0x3c9b

    aput v21, v2, v20

    const/16 v20, -0x5e

    aput v20, v2, v19

    const/16 v19, -0x29

    aput v19, v2, v18

    const/16 v18, 0x5940

    aput v18, v2, v17

    const/16 v17, -0x44ec

    aput v17, v2, v16

    const/16 v16, -0x2b

    aput v16, v2, v15

    const/16 v15, -0x3eb4

    aput v15, v2, v14

    const/16 v14, -0x58

    aput v14, v2, v13

    const/16 v13, -0x79

    aput v13, v2, v12

    const/16 v12, -0x7ee3

    aput v12, v2, v11

    const/16 v11, -0x1e

    aput v11, v2, v10

    const/16 v10, -0x76b3

    aput v10, v2, v9

    const/16 v9, -0x11

    aput v9, v2, v8

    const/16 v8, -0x3a

    aput v8, v2, v7

    const/16 v7, -0x22

    aput v7, v2, v6

    const/16 v6, -0x5486

    aput v6, v2, v5

    const/16 v5, -0x1b

    aput v5, v2, v3

    const/16 v3, -0x19a7

    aput v3, v2, v1

    const/16 v1, 0x32

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, 0x2f

    const/16 v52, 0x30

    const/16 v53, 0x31

    const/16 v54, -0x4

    aput v54, v1, v53

    const/16 v53, -0x4c

    aput v53, v1, v52

    const/16 v52, -0x80

    aput v52, v1, v51

    const/16 v51, -0x52b5

    aput v51, v1, v50

    const/16 v50, -0x53

    aput v50, v1, v49

    const/16 v49, -0x6ffe

    aput v49, v1, v48

    const/16 v48, -0x70

    aput v48, v1, v47

    const/16 v47, -0x58

    aput v47, v1, v46

    const/16 v46, -0x13

    aput v46, v1, v45

    const/16 v45, -0x6a

    aput v45, v1, v44

    const/16 v44, 0x7a14

    aput v44, v1, v43

    const/16 v43, -0x2586

    aput v43, v1, v42

    const/16 v42, -0x26

    aput v42, v1, v41

    const/16 v41, -0x12

    aput v41, v1, v40

    const/16 v40, -0x12

    aput v40, v1, v39

    const/16 v39, -0x4c

    aput v39, v1, v38

    const/16 v38, -0x72d0

    aput v38, v1, v37

    const/16 v37, -0x73

    aput v37, v1, v36

    const/16 v36, -0x2d

    aput v36, v1, v35

    const/16 v35, -0x8cf

    aput v35, v1, v34

    const/16 v34, -0x9

    aput v34, v1, v33

    const/16 v33, -0x6ca4

    aput v33, v1, v32

    const/16 v32, -0x6d

    aput v32, v1, v31

    const/16 v31, 0x6f20

    aput v31, v1, v30

    const/16 v30, 0x1a6f

    aput v30, v1, v29

    const/16 v29, 0x311a

    aput v29, v1, v28

    const/16 v28, -0x4acf

    aput v28, v1, v27

    const/16 v27, -0x4b

    aput v27, v1, v26

    const/16 v26, -0xbba

    aput v26, v1, v25

    const/16 v25, -0xc

    aput v25, v1, v24

    const/16 v24, -0x7

    aput v24, v1, v23

    const/16 v23, -0x1a

    aput v23, v1, v22

    const/16 v22, -0x3cfe

    aput v22, v1, v21

    const/16 v21, -0x3d

    aput v21, v1, v20

    const/16 v20, -0x47

    aput v20, v1, v19

    const/16 v19, 0x5921

    aput v19, v1, v18

    const/16 v18, -0x44a7

    aput v18, v1, v17

    const/16 v17, -0x45

    aput v17, v1, v16

    const/16 v16, -0x3edd

    aput v16, v1, v15

    const/16 v15, -0x3f

    aput v15, v1, v14

    const/16 v14, -0xd

    aput v14, v1, v13

    const/16 v13, -0x7e84

    aput v13, v1, v12

    const/16 v12, -0x7f

    aput v12, v1, v11

    const/16 v11, -0x76dc

    aput v11, v1, v10

    const/16 v10, -0x77

    aput v10, v1, v9

    const/16 v9, -0x51

    aput v9, v1, v8

    const/16 v8, -0x56

    aput v8, v1, v7

    const/16 v7, -0x54eb

    aput v7, v1, v6

    const/16 v6, -0x55

    aput v6, v1, v5

    const/16 v5, -0x19cc

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :catch_2
    move-exception v1

    goto/16 :goto_5
.end method
