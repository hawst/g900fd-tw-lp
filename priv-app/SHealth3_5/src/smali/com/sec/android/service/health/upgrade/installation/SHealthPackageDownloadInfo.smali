.class Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;
.super Ljava/lang/Object;


# instance fields
.field public mAppId:Ljava/lang/String;

.field public mContentSize:Ljava/lang/String;

.field public mDownloadUri:Ljava/lang/String;

.field public mProductId:Ljava/lang/String;

.field public mProductName:Ljava/lang/String;

.field public mResultCode:Ljava/lang/String;

.field public mResultMessage:Ljava/lang/String;

.field public mVersionCode:Ljava/lang/String;

.field public mVersionName:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mAppId:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mResultCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mResultMessage:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mDownloadUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mContentSize:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mVersionCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mVersionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mProductId:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloadInfo;->mProductName:Ljava/lang/String;

    return-void
.end method
