.class Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/BTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/BTManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "android.bluetooth.adapter.extra.STATE"

    const/high16 v1, -0x80000000

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "Bluetooth off"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onStateChanged(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "Bluetooth on"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->initHealthHandlers()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    const/16 v1, 0x7d1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onStateChanged(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
