.class public Lcom/sec/android/service/health/cp/serversync/data/FirstBeatVariableItem;
.super Ljava/lang/Object;


# instance fields
.field public activityClass:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceFirstBeatVarPKId:Ljava/lang/String;

.field public deviceItemPKId:Ljava/lang/Long;

.field public deviceModifyTime:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public latestExerciseTime:Ljava/lang/String;

.field public latestFeedbackPhraseNumber:I

.field public maximumHeartRate:Ljava/lang/String;

.field public maximumMet:I

.field public previousToTrainingLevel:I

.field public previousTrainingLevel:I

.field public recoveryResource:I

.field public requestType:Ljava/lang/String;

.field public startTime:Ljava/lang/String;

.field public trainingLevel:I

.field public trainingLevelUpdate:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
