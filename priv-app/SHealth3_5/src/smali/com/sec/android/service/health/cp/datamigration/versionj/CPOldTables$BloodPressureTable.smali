.class public Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$BloodPressureTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodPressureTable"
.end annotation


# static fields
.field public static final CONTENT_ID_URI_BASE:Landroid/net/Uri;

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.samsung.cursor.item/health.blood_pressure"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.samsung.cursor.dir/health.blood_pressure"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_COMMENT:Ljava/lang/String; = "comment"

.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_HIGH:Ljava/lang/String; = "systolic"

.field public static final KEY_LOW:Ljava/lang/String; = "diastolic"

.field public static final KEY_MEAN:Ljava/lang/String; = "mean"

.field public static final KEY_PERSON_ID:Ljava/lang/String; = "person_id"

.field public static final KEY_PULSE:Ljava/lang/String; = "pulse"

.field public static final KEY_SENSOR_ID:Ljava/lang/String; = "sensor_id"

.field public static final TABLE_NAME:Ljava/lang/String; = "blood_pressure"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://SecHealth/blood_pressure"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$BloodPressureTable;->CONTENT_URI:Landroid/net/Uri;

    const-string v0, "content://SecHealth/blood_pressure/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$BloodPressureTable;->CONTENT_ID_URI_BASE:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
