.class public Lcom/sec/android/service/health/cp/HealthContentProvider;
.super Landroid/content/ContentProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/HealthContentProvider$CallingModule;
    }
.end annotation


# static fields
.field private static final ACCOUNT:Ljava/lang/String; = "Account"

.field public static final ACCOUNT_FOUND:Ljava/lang/String; = "ACCOUNT_FOUND"

.field public static final ACCOUNT_NOT_FOUND:Ljava/lang/String; = "ACCOUNT_NOT_FOUND"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "AccountType"

.field public static final ACCOUNT_TYPE_HEALTH:Ljava/lang/String; = "HEALTH_ACCOUNT"

.field public static final ACCOUNT_TYPE_SAM:Ljava/lang/String; = "SAMSUNG_ACCOUNT"

.field private static ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String; = null

.field private static final APPLICATIONID_URI_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static final CREATETIME_URI_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static final DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public static final GET_SAMSUNG_ACCOUNT:Ljava/lang/String; = "GET_SAMSUNG_ACCOUNT"

.field public static final PKG_NAME:Ljava/lang/String; = "com.sec.android.service.health"

.field public static final PROFILE_KEY_ACTIVITY_TYPE:I = 0x2711

.field public static final PROFILE_KEY_BIRTH_DATE:I = 0x2712

.field public static final PROFILE_KEY_BLOODGLUCOSE_UNIT:I = 0x2720

.field public static final PROFILE_KEY_COUNTRY:I = 0x2713

.field public static final PROFILE_KEY_CREATE_TIME:I = 0x271e

.field public static final PROFILE_KEY_DISCLOSE_YN:I = 0x271d

.field public static final PROFILE_KEY_DISTANCE_UNIT:I = 0x2714

.field public static final PROFILE_KEY_GENDER:I = 0x2715

.field public static final PROFILE_KEY_HEIGHT:I = 0x2716

.field public static final PROFILE_KEY_HEIGHT_UNIT:I = 0x2717

.field public static final PROFILE_KEY_IN_SYNC:I = 0x2718

.field public static final PROFILE_KEY_NAME:I = 0x2719

.field public static final PROFILE_KEY_TEMPERATURE_UNIT:I = 0x271f

.field public static final PROFILE_KEY_UPDATE_TIME:I = 0x271a

.field public static final PROFILE_KEY_WEIGHT:I = 0x271b

.field public static final PROFILE_KEY_WEIGHT_UNIT:I = 0x271c

.field private static final STATUS:Ljava/lang/String; = "Status"

.field public static final SYNC_PKG_NAME:Ljava/lang/String; = "xxx.x.x"

.field private static final TAG:Ljava/lang/String;

.field private static final THIRD_PARTY_IDENTIFIER:Ljava/lang/String; = "0000"

.field private static final UPDATETIME_URI_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public static final URI_PROFILE:Landroid/net/Uri;

.field private static final URI_TYPE_APPLICATION:I = 0x3ec

.field private static final URI_TYPE_APPLICATION_DEVICE_PREF:I = 0x3f0

.field private static final URI_TYPE_APPLICATION_DEVICE_PREF_ID:I = 0x3f1

.field private static final URI_TYPE_APPLICATION_ID:I = 0x3ed

.field private static final URI_TYPE_APPLICATION_PERMISSIONS:I = 0x3ee

.field private static final URI_TYPE_APPLICATION_PERMISSIONS_ID:I = 0x3ef

.field private static final URI_TYPE_BLOOD_GLUCOSE:I = 0xfba

.field private static final URI_TYPE_BLOOD_GLUCOSE_ID:I = 0xfbb

.field private static final URI_TYPE_BLOOD_GLUCOSE_MEASUREMENT_CONTEXT:I = 0x1397

.field private static final URI_TYPE_BLOOD_GLUCOSE_MEASUREMENT_CONTEXT_ID:I = 0x1398

.field private static final URI_TYPE_BLOOD_PRESSURE:I = 0xfbc

.field private static final URI_TYPE_BLOOD_PRESSURE_ID:I = 0xfbd

.field private static final URI_TYPE_BODY_TEMPERATURE:I = 0xfd1

.field private static final URI_TYPE_BODY_TEMPERATURE_ID:I = 0xfd2

.field private static final URI_TYPE_ELECTRO_CARDIOGRAPHY:I = 0xfef

.field private static final URI_TYPE_ELECTRO_CARDIOGRAPHY_ANALYSIS:I = 0xff3

.field private static final URI_TYPE_ELECTRO_CARDIOGRAPHY_ANALYSIS_ID:I = 0xff4

.field private static final URI_TYPE_ELECTRO_CARDIOGRAPHY_ID:I = 0xff0

.field private static final URI_TYPE_EXERCISE:I = 0xbc6

.field private static final URI_TYPE_EXERCISE_ACTIVITY:I = 0xbe5

.field private static final URI_TYPE_EXERCISE_ACTIVITY_ID:I = 0xbe6

.field private static final URI_TYPE_EXERCISE_DEVICE_INFO:I = 0xbfb

.field private static final URI_TYPE_EXERCISE_DEVICE_INFO_ID:I = 0xbfc

.field private static final URI_TYPE_EXERCISE_ID:I = 0xbc7

.field private static final URI_TYPE_EXERCISE_INFO:I = 0xbc8

.field private static final URI_TYPE_EXERCISE_INFO_ID:I = 0xbc9

.field private static final URI_TYPE_EXERCISE_MISSION:I = 0xc03

.field private static final URI_TYPE_EXERCISE_MISSION_ID:I = 0xc04

.field private static final URI_TYPE_EXERCISE_PHOTO:I = 0xbff

.field private static final URI_TYPE_EXERCISE_PHOTO_ID:I = 0xc00

.field private static final URI_TYPE_FIRST_BEAT_COACHING_DATA:I = 0xffe

.field private static final URI_TYPE_FIRST_BEAT_COACHING_DATA_ID:I = 0xfff

.field private static final URI_TYPE_FIRST_BEAT_COACHING_RESULT:I = 0xffa

.field private static final URI_TYPE_FIRST_BEAT_COACHING_RESULT_ID:I = 0xffb

.field private static final URI_TYPE_FIRST_BEAT_COACHING_VARIABLE:I = 0xffc

.field private static final URI_TYPE_FIRST_BEAT_COACHING_VARIABLE_ID:I = 0xffd

.field private static final URI_TYPE_FOOD_INFO:I = 0x7da

.field private static final URI_TYPE_FOOD_INFO_ID:I = 0x7db

.field private static final URI_TYPE_FOOD_NUTRIENT:I = 0x7dc

.field private static final URI_TYPE_FOOD_NUTRIENT_ID:I = 0x7dd

.field private static final URI_TYPE_GOAL:I = 0xfc0

.field private static final URI_TYPE_GOAL_ID:I = 0xfc1

.field private static final URI_TYPE_HEART_RATE:I = 0xfeb

.field private static final URI_TYPE_HEART_RATE_DATA:I = 0xfed

.field private static final URI_TYPE_HEART_RATE_DATA_ID:I = 0xfee

.field private static final URI_TYPE_HEART_RATE_ID:I = 0xfec

.field private static final URI_TYPE_LOCATION_DATA:I = 0xbd0

.field private static final URI_TYPE_LOCATION_DATA_ID:I = 0xbd1

.field private static final URI_TYPE_MEAL:I = 0x7d4

.field private static final URI_TYPE_MEAL_ID:I = 0x7d5

.field private static final URI_TYPE_MEAL_IMAGE:I = 0x7d8

.field private static final URI_TYPE_MEAL_IMAGE_ID:I = 0x7d9

.field private static final URI_TYPE_MEAL_ITEM:I = 0x7d6

.field private static final URI_TYPE_MEAL_ITEM_ID:I = 0x7d7

.field private static final URI_TYPE_MEAL_PLAN:I = 0x7de

.field private static final URI_TYPE_MEAL_PLAN_ID:I = 0x7df

.field private static final URI_TYPE_MIGRATE:I = 0x3e7

.field private static final URI_TYPE_MIGRATE_KIES:I = 0x3e8

.field private static final URI_TYPE_PAIRED_DEVICE:I = 0x138e

.field private static final URI_TYPE_PAIRED_DEVICE_ID:I = 0x138f

.field private static final URI_TYPE_PULSE_OXIMETER:I = 0xff5

.field private static final URI_TYPE_PULSE_OXIMETER_ID:I = 0xff6

.field private static final URI_TYPE_RAWQUERY:I = 0x3e4

.field private static final URI_TYPE_REALTIME_DATA:I = 0xbca

.field private static final URI_TYPE_REALTIME_DATA_ID:I = 0xbcb

.field private static final URI_TYPE_SENSOR_HANDLER_DETAILS:I = 0xfd9

.field private static final URI_TYPE_SENSOR_HANDLER_DETAILS_ID:I = 0xfda

.field private static final URI_TYPE_SKIN:I = 0x1393

.field private static final URI_TYPE_SKIN_ID:I = 0x1394

.field private static final URI_TYPE_SLEEP:I = 0xfd3

.field private static final URI_TYPE_SLEEP_DATA:I = 0xfdd

.field private static final URI_TYPE_SLEEP_DATA_ID:I = 0xfde

.field private static final URI_TYPE_SLEEP_ID:I = 0xfd4

.field private static final URI_TYPE_STRENGTH_FITNESS:I = 0xbe7

.field private static final URI_TYPE_STRENGTH_FITNESS_ID:I = 0xbe8

.field private static final URI_TYPE_STRESS:I = 0xfdf

.field private static final URI_TYPE_STRESS_ID:I = 0xfe0

.field private static final URI_TYPE_TAG:I = 0x1395

.field private static final URI_TYPE_TAG_ID:I = 0x1396

.field private static final URI_TYPE_TEMPERATURE_HUMIDITY:I = 0xfc2

.field private static final URI_TYPE_TEMPERATURE_HUMIDITY_ID:I = 0xfc3

.field private static final URI_TYPE_TYPE:I = 0xfd5

.field private static final URI_TYPE_TYPE_ID:I = 0xfd6

.field private static final URI_TYPE_ULTRAVIOLET_RAYS:I = 0xff1

.field private static final URI_TYPE_ULTRAVIOLET_RAYS_ID:I = 0xff2

.field private static final URI_TYPE_USER_DEVICE:I = 0x3ea

.field private static final URI_TYPE_USER_DEVICE_ID:I = 0x3eb

.field private static final URI_TYPE_UV_PROTECTION:I = 0x1399

.field private static final URI_TYPE_UV_PROTECTION_ID:I = 0x139a

.field private static final URI_TYPE_WALK_INFO:I = 0xbfd

.field private static final URI_TYPE_WALK_INFO_EXTENDED:I = 0xc01

.field private static final URI_TYPE_WALK_INFO_EXTENDED_ID:I = 0xc02

.field private static final URI_TYPE_WALK_INFO_ID:I = 0xbfe

.field private static final URI_TYPE_WATER_INGESTION:I = 0xfe7

.field private static final URI_TYPE_WATER_INGESTION_ID:I = 0xfe8

.field private static final URI_TYPE_WEIGHT:I = 0xfbe

.field private static final URI_TYPE_WEIGHT_ID:I = 0xfbf

.field public static final VALIDATION_POLICY:Ljava/lang/String; = "validation_policy"

.field private static mIDIncrementer:I

.field public static mapProfileKey:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sURIMatcher:Landroid/content/UriMatcher;

.field public static final uriPermissionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private map_PackageURIPermission:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-class v0, Lcom/sec/android/service/health/cp/HealthContentProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string/jumbo v1, "user_profile"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->URI_PROFILE:Landroid/net/Uri;

    const-string v0, "com.osp.app.signin"

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mIDIncrementer:I

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "user_device"

    const/16 v3, 0x3ea

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "user_device/*"

    const/16 v3, 0x3eb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "application"

    const/16 v3, 0x3ec

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "application/*"

    const/16 v3, 0x3ed

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "application_permissions"

    const/16 v3, 0x3ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "application_permissions/*"

    const/16 v3, 0x3ef

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "application_device_pref"

    const/16 v3, 0x3f0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "application_device_pref/*"

    const/16 v3, 0x3f1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal"

    const/16 v3, 0x7d4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal/#"

    const/16 v3, 0x7d5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal_plan"

    const/16 v3, 0x7de

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal_plan/#"

    const/16 v3, 0x7df

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal_item"

    const/16 v3, 0x7d6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal_item/#"

    const/16 v3, 0x7d7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal_image"

    const/16 v3, 0x7d8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "meal_image/#"

    const/16 v3, 0x7d9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "food_info"

    const/16 v3, 0x7da

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "food_info/#"

    const/16 v3, 0x7db

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "food_nutrient"

    const/16 v3, 0x7dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "food_nutrient/#"

    const/16 v3, 0x7dd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise"

    const/16 v3, 0xbc6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise/#"

    const/16 v3, 0xbc7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_info"

    const/16 v3, 0xbc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_info/#"

    const/16 v3, 0xbc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_photo"

    const/16 v3, 0xbff

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_photo/#"

    const/16 v3, 0xc00

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "realtime_data"

    const/16 v3, 0xbca

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "realtime_data/#"

    const/16 v3, 0xbcb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_activity"

    const/16 v3, 0xbe5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_activity/#"

    const/16 v3, 0xbe6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "location_data"

    const/16 v3, 0xbd0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "location_data/#"

    const/16 v3, 0xbd1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "strength_fitness"

    const/16 v3, 0xbe7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "strength_fitness/#"

    const/16 v3, 0xbe8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_device_info"

    const/16 v3, 0xbfb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_device_info/#"

    const/16 v3, 0xbfc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "walk_info"

    const/16 v3, 0xbfd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "walk_info/#"

    const/16 v3, 0xbfe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "walk_info_extended"

    const/16 v3, 0xc01

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "walk_info_extended/#"

    const/16 v3, 0xc02

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_mission"

    const/16 v3, 0xc03

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "exercise_mission/#"

    const/16 v3, 0xc04

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "blood_glucose"

    const/16 v3, 0xfba

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "blood_glucose/#"

    const/16 v3, 0xfbb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "blood_pressure"

    const/16 v3, 0xfbc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "blood_pressure/#"

    const/16 v3, 0xfbd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "body_temperature"

    const/16 v3, 0xfd1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "body_temperature/#"

    const/16 v3, 0xfd2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "temperature_humidity"

    const/16 v3, 0xfc2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "temperature_humidity/#"

    const/16 v3, 0xfc3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "weight"

    const/16 v3, 0xfbe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "weight/#"

    const/16 v3, 0xfbf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "goal"

    const/16 v3, 0xfc0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "goal/#"

    const/16 v3, 0xfc1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "sleep"

    const/16 v3, 0xfd3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "sleep/#"

    const/16 v3, 0xfd4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "sleep_data"

    const/16 v3, 0xfdd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "sleep_data/#"

    const/16 v3, 0xfde

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "stress"

    const/16 v3, 0xfdf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "stress/#"

    const/16 v3, 0xfe0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "type"

    const/16 v3, 0xfd5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "type/#"

    const/16 v3, 0xfd6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "sensor_handler_details"

    const/16 v3, 0xfd9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "sensor_handler_details/#"

    const/16 v3, 0xfda

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "heart_rate"

    const/16 v3, 0xfeb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "heart_rate/#"

    const/16 v3, 0xfec

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "heart_rate_data"

    const/16 v3, 0xfed

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "heart_rate_data/#"

    const/16 v3, 0xfee

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "electro_cardiography"

    const/16 v3, 0xfef

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "electro_cardiography/#"

    const/16 v3, 0xff0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "electro_cardiography_analysis"

    const/16 v3, 0xff3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "electro_cardiography_analysis/#"

    const/16 v3, 0xff4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "ultraviolet_rays"

    const/16 v3, 0xff1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "ultraviolet_rays/#"

    const/16 v3, 0xff2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "pulse_oximeter"

    const/16 v3, 0xff5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "pulse_oximeter/#"

    const/16 v3, 0xff6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "water_ingestion"

    const/16 v3, 0xfe7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "water_ingestion/#"

    const/16 v3, 0xfe8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "first_beat_coaching_result"

    const/16 v3, 0xffa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "first_beat_coaching_result/#"

    const/16 v3, 0xffb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "first_beat_coaching_variable"

    const/16 v3, 0xffc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "first_beat_coaching_variable/#"

    const/16 v3, 0xffd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "first_beat_coaching_data"

    const/16 v3, 0xffe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "first_beat_coaching_data/#"

    const/16 v3, 0xfff

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "paired_device"

    const/16 v3, 0x138e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "paired_device/#"

    const/16 v3, 0x138f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "skin"

    const/16 v3, 0x1393

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "skin/#"

    const/16 v3, 0x1394

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "tag"

    const/16 v3, 0x1395

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "tag/#"

    const/16 v3, 0x1396

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "blood_glucose_measurement_context"

    const/16 v3, 0x1397

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string v2, "blood_glucose_measurement_context/#"

    const/16 v3, 0x1398

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "uv_protection"

    const/16 v3, 0x1399

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "uv_protection/#"

    const/16 v3, 0x139a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "rawquery"

    const/16 v3, 0x3e4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "migrate/*"

    const/16 v3, 0x3e7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    const-string/jumbo v2, "migrate_kies/*"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMission;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$TemperatureHumidity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealPlan;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitness;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiography;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysis;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WaterIngestion;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContext;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMission;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$TemperatureHumidity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealPlan;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitness;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiography;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysis;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WaterIngestion;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContext;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMission;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$TemperatureHumidity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealPlan;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitness;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WaterIngestion;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiography;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysis;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContext;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMission;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$TemperatureHumidity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealPlan;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitness;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WaterIngestion;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiography;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysis;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContext;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "activity_type"

    const/16 v2, 0x2711

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "birth_date"

    const/16 v2, 0x2712

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "country"

    const/16 v2, 0x2713

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "distance_unit"

    const/16 v2, 0x2714

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "gender"

    const/16 v2, 0x2715

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "height"

    const/16 v2, 0x2716

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "height_unit"

    const/16 v2, 0x2717

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "in_sync"

    const/16 v2, 0x2718

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string/jumbo v1, "name"

    const/16 v2, 0x2719

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string/jumbo v1, "update_time"

    const/16 v2, 0x271a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string/jumbo v1, "weight"

    const/16 v2, 0x271b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string/jumbo v1, "weight_unit"

    const/16 v2, 0x271c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string/jumbo v1, "profile_disclose_yn"

    const/16 v2, 0x271d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string/jumbo v1, "profile_create_time"

    const/16 v2, 0x271e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string/jumbo v1, "temperature_unit"

    const/16 v2, 0x271f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    const-string v1, "blood_glucose_unit"

    const/16 v2, 0x2720

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->URI_PROFILE:Landroid/net/Uri;

    const-string v2, "UserProfile"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "BloodGlucose"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "BloodPressure"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "BodyTemperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Food"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "HeartRate"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Sleep"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Stress"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Weight"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "BodyTemperature"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$TemperatureHumidity;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "TemperatureHumidity"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMission;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Location"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitness;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingData;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDevice;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Exercise"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Food"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Food"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Food"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Food"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealPlan;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Food"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WaterIngestion;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Water"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiography;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "ECG"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysis;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "ECG"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "HeartRate"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "HeartRate"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "PulseOximeter"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "UltravioletRays"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Sleep"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Sleep"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Stress"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Weight"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Skin"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "Tag"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContext;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "BloodGlucose"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->uriPermissionMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "UVProtection"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/HealthContentProvider;->map_PackageURIPermission:Ljava/util/HashMap;

    return-void
.end method

.method private checkPermission(Landroid/net/Uri;Z)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private checkShealth25Exists(Ljava/lang/String;)Z
    .locals 10

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    const-string v0, "SHEALTH_2_5_EXISTS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    :goto_0
    const-string v3, "application__id = \'shealth2.5\' OR application__id = \'shealth2.6\'"

    const-string/jumbo v1, "user_device"

    new-array v2, v8, [Ljava/lang/String;

    const-string v5, "application__id"

    aput-object v5, v2, v9

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v8

    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v9

    goto :goto_1
.end method

.method private clearPermissionCacheOnApplicatioDelete(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x0

    const-string v0, "application"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "package_name"

    aput-object v1, v2, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    :try_start_0
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "package_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/HealthContentProvider;->map_PackageURIPermission:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/HealthContentProvider;->map_PackageURIPermission:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private getCallerPackage()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v2, "getCallerPackage(): This should not happen in usual scenario."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, ""

    goto :goto_0
.end method

.method private getDatabase(I)Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    .locals 1

    const/16 v0, 0x3e7

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    goto :goto_0
.end method

.method private getIdTimeStamp(Landroid/content/ContentValues;)J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getIdTimeStamp(Landroid/content/ContentValues;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private getIdTimeStamp(Landroid/content/ContentValues;J)J
    .locals 4

    sget v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mIDIncrementer:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mIDIncrementer:I

    rem-int/lit8 v0, v0, 0x64

    sput v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mIDIncrementer:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    sget v2, Lcom/sec/android/service/health/cp/HealthContentProvider;->mIDIncrementer:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private getTableNameForUri(I)Ljava/lang/String;
    .locals 3

    sparse-switch p1, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Table: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "application"

    :goto_0
    return-object v0

    :sswitch_1
    const-string v0, "application_permissions"

    goto :goto_0

    :sswitch_2
    const-string v0, "application_device_pref"

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "user_device"

    goto :goto_0

    :sswitch_4
    const-string/jumbo v0, "paired_device"

    goto :goto_0

    :sswitch_5
    const-string/jumbo v0, "meal"

    goto :goto_0

    :sswitch_6
    const-string/jumbo v0, "meal_item"

    goto :goto_0

    :sswitch_7
    const-string/jumbo v0, "meal_image"

    goto :goto_0

    :sswitch_8
    const-string/jumbo v0, "meal_plan"

    goto :goto_0

    :sswitch_9
    const-string v0, "food_info"

    goto :goto_0

    :sswitch_a
    const-string v0, "food_nutrient"

    goto :goto_0

    :sswitch_b
    const-string v0, "exercise"

    goto :goto_0

    :sswitch_c
    const-string v0, "exercise_info"

    goto :goto_0

    :sswitch_d
    const-string v0, "exercise_photo"

    goto :goto_0

    :sswitch_e
    const-string/jumbo v0, "realtime_data"

    goto :goto_0

    :sswitch_f
    const-string v0, "exercise_activity"

    goto :goto_0

    :sswitch_10
    const-string v0, "exercise_mission"

    goto :goto_0

    :sswitch_11
    const-string v0, "location_data"

    goto :goto_0

    :sswitch_12
    const-string/jumbo v0, "strength_fitness"

    goto :goto_0

    :sswitch_13
    const-string v0, "blood_glucose"

    goto :goto_0

    :sswitch_14
    const-string v0, "blood_pressure"

    goto :goto_0

    :sswitch_15
    const-string v0, "body_temperature"

    goto :goto_0

    :sswitch_16
    const-string/jumbo v0, "temperature_humidity"

    goto :goto_0

    :sswitch_17
    const-string/jumbo v0, "weight"

    goto :goto_0

    :sswitch_18
    const-string v0, "goal"

    goto :goto_0

    :sswitch_19
    const-string/jumbo v0, "sleep"

    goto :goto_0

    :sswitch_1a
    const-string/jumbo v0, "sleep_data"

    goto :goto_0

    :sswitch_1b
    const-string/jumbo v0, "stress"

    goto :goto_0

    :sswitch_1c
    const-string/jumbo v0, "type"

    goto :goto_0

    :sswitch_1d
    const-string/jumbo v0, "sensor_handler_details"

    goto :goto_0

    :sswitch_1e
    const-string v0, "exercise_device_info"

    goto :goto_0

    :sswitch_1f
    const-string/jumbo v0, "walk_info"

    goto :goto_0

    :sswitch_20
    const-string/jumbo v0, "walk_info_extended"

    goto :goto_0

    :sswitch_21
    const-string/jumbo v0, "water_ingestion"

    goto :goto_0

    :sswitch_22
    const-string v0, "first_beat_coaching_result"

    goto :goto_0

    :sswitch_23
    const-string v0, "first_beat_coaching_variable"

    goto :goto_0

    :sswitch_24
    const-string v0, "first_beat_coaching_data"

    goto :goto_0

    :sswitch_25
    const-string v0, "heart_rate"

    goto/16 :goto_0

    :sswitch_26
    const-string v0, "heart_rate_data"

    goto/16 :goto_0

    :sswitch_27
    const-string v0, "electro_cardiography"

    goto/16 :goto_0

    :sswitch_28
    const-string v0, "electro_cardiography_analysis"

    goto/16 :goto_0

    :sswitch_29
    const-string/jumbo v0, "ultraviolet_rays"

    goto/16 :goto_0

    :sswitch_2a
    const-string/jumbo v0, "pulse_oximeter"

    goto/16 :goto_0

    :sswitch_2b
    const-string/jumbo v0, "skin"

    goto/16 :goto_0

    :sswitch_2c
    const-string/jumbo v0, "tag"

    goto/16 :goto_0

    :sswitch_2d
    const-string v0, "blood_glucose_measurement_context"

    goto/16 :goto_0

    :sswitch_2e
    const-string/jumbo v0, "uv_protection"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_0
        0x3ed -> :sswitch_0
        0x3ee -> :sswitch_1
        0x3ef -> :sswitch_1
        0x3f0 -> :sswitch_2
        0x3f1 -> :sswitch_2
        0x7d4 -> :sswitch_5
        0x7d5 -> :sswitch_5
        0x7d6 -> :sswitch_6
        0x7d7 -> :sswitch_6
        0x7d8 -> :sswitch_7
        0x7d9 -> :sswitch_7
        0x7da -> :sswitch_9
        0x7db -> :sswitch_9
        0x7dc -> :sswitch_a
        0x7dd -> :sswitch_a
        0x7de -> :sswitch_8
        0x7df -> :sswitch_8
        0xbc6 -> :sswitch_b
        0xbc7 -> :sswitch_b
        0xbc8 -> :sswitch_c
        0xbc9 -> :sswitch_c
        0xbca -> :sswitch_e
        0xbcb -> :sswitch_e
        0xbd0 -> :sswitch_11
        0xbd1 -> :sswitch_11
        0xbe5 -> :sswitch_f
        0xbe6 -> :sswitch_f
        0xbe7 -> :sswitch_12
        0xbe8 -> :sswitch_12
        0xbfb -> :sswitch_1e
        0xbfc -> :sswitch_1e
        0xbfd -> :sswitch_1f
        0xbfe -> :sswitch_1f
        0xbff -> :sswitch_d
        0xc00 -> :sswitch_d
        0xc01 -> :sswitch_20
        0xc02 -> :sswitch_20
        0xc03 -> :sswitch_10
        0xc04 -> :sswitch_10
        0xfba -> :sswitch_13
        0xfbb -> :sswitch_13
        0xfbc -> :sswitch_14
        0xfbd -> :sswitch_14
        0xfbe -> :sswitch_17
        0xfbf -> :sswitch_17
        0xfc0 -> :sswitch_18
        0xfc1 -> :sswitch_18
        0xfc2 -> :sswitch_16
        0xfc3 -> :sswitch_16
        0xfd1 -> :sswitch_15
        0xfd2 -> :sswitch_15
        0xfd3 -> :sswitch_19
        0xfd4 -> :sswitch_19
        0xfd5 -> :sswitch_1c
        0xfd6 -> :sswitch_1c
        0xfd9 -> :sswitch_1d
        0xfda -> :sswitch_1d
        0xfdd -> :sswitch_1a
        0xfde -> :sswitch_1a
        0xfdf -> :sswitch_1b
        0xfe0 -> :sswitch_1b
        0xfe7 -> :sswitch_21
        0xfe8 -> :sswitch_21
        0xfeb -> :sswitch_25
        0xfec -> :sswitch_25
        0xfed -> :sswitch_26
        0xfee -> :sswitch_26
        0xfef -> :sswitch_27
        0xff0 -> :sswitch_27
        0xff1 -> :sswitch_29
        0xff2 -> :sswitch_29
        0xff3 -> :sswitch_28
        0xff4 -> :sswitch_28
        0xff5 -> :sswitch_2a
        0xff6 -> :sswitch_2a
        0xffa -> :sswitch_22
        0xffb -> :sswitch_22
        0xffc -> :sswitch_23
        0xffd -> :sswitch_23
        0xffe -> :sswitch_24
        0xfff -> :sswitch_24
        0x138e -> :sswitch_4
        0x138f -> :sswitch_4
        0x1393 -> :sswitch_2b
        0x1394 -> :sswitch_2b
        0x1395 -> :sswitch_2c
        0x1396 -> :sswitch_2c
        0x1397 -> :sswitch_2d
        0x1398 -> :sswitch_2d
        0x1399 -> :sswitch_2e
        0x139a -> :sswitch_2e
    .end sparse-switch
.end method

.method private static getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 2

    const-string/jumbo v0, "validation_policy"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "validation_policy"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->fromMask(I)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v0

    const-string/jumbo v1, "validation_policy"

    invoke-virtual {p0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getUIPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v0

    goto :goto_0
.end method

.method private handleApplicationId(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 8

    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    if-eqz p2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->APPLICATIONID_URI_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v1, "Request from Samsung Signed App"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "application__id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application__id"

    const-string v1, "1y90e30264"

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "application__id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Application Id must not provided by the application"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v1, "Request from Thirdparty App"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;

    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;-><init>()V

    const-string v1, "application"

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->getSQLiteDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string/jumbo v3, "package_name=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->query(Lsamsung/database/sqlite/SecSQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gez v1, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Thirdpary Application must be registered"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "application__id"

    invoke-virtual {p2, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private handleCreateTime(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleCreateTime(Landroid/net/Uri;Landroid/content/ContentValues;J)V

    return-void
.end method

.method private handleCreateTime(Landroid/net/Uri;Landroid/content/ContentValues;J)V
    .locals 2

    const-string/jumbo v0, "xxx.x.x"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "create_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "update_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "time_zone"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    const-string/jumbo v1, "time_zone"

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_3
    const-string v0, "daylight_saving"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "sample_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "sample_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getDSTSaving(J)I

    move-result v0

    :goto_1
    if-nez v0, :cond_6

    const-string v0, "daylight_saving"

    const v1, 0x445c2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    :cond_5
    invoke-static {p3, p4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getDSTSaving(J)I

    move-result v0

    goto :goto_1

    :cond_6
    const-string v0, "daylight_saving"

    const v1, 0x445c1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0
.end method

.method private handleDelete(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->clearPermissionCacheOnApplicatioDelete(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const-string/jumbo v3, "sync_status != 170002"

    :goto_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;

    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;-><init>()V

    invoke-virtual {v0, p2}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->getSQLiteDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v1

    move-object v4, p4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->query(Lsamsung/database/sqlite/SecSQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_4

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    invoke-virtual {p1, p2, p3, p4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    :goto_1
    return v0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and sync_status != 170002"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_4
    new-array v3, v0, [Landroid/content/ContentValues;

    move v0, v8

    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    aput-object v4, v3, v0

    aget-object v4, v3, v0

    const-string v5, "_id"

    const-string v6, "_id"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    aget-object v4, v3, v0

    const-string v5, "create_time"

    const-string v6, "create_time"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "application__id"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    const-string v4, "application__id"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_5

    aget-object v4, v3, v0

    const-string v5, "application__id"

    const-string v6, "application__id"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string/jumbo v4, "time_zone"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_6

    aget-object v4, v3, v0

    const-string/jumbo v5, "time_zone"

    const-string/jumbo v6, "time_zone"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_6
    aget-object v4, v3, v0

    const-string/jumbo v5, "table_name"

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    :cond_7
    if-eqz v1, :cond_8

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    invoke-virtual {p1, p2, p3, p4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    array-length v1, v3

    :goto_3
    if-ge v8, v1, :cond_a

    aget-object v4, v3, v8

    invoke-static {v4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    const-string v6, "deleted_info"

    invoke-virtual {p1, v6, v2, v4, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v5

    const-wide/16 v9, -0x1

    cmp-long v5, v5, v9

    if-nez v5, :cond_9

    sget-object v5, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid data, Insertion failed for : deleted_info values: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_a
    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    goto/16 :goto_1
.end method

.method private handleGenericDelete(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p2}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, ""

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-direct {p0, p1, v2, p3, p4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleDelete(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    return v0

    :sswitch_1
    const-string v0, "\'"

    :sswitch_2
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {p0, p1, v2, v0, p4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleDelete(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3eb -> :sswitch_1
        0x3ec -> :sswitch_0
        0x3ed -> :sswitch_1
        0x3ee -> :sswitch_0
        0x3ef -> :sswitch_2
        0x3f0 -> :sswitch_0
        0x3f1 -> :sswitch_1
        0x7d4 -> :sswitch_0
        0x7d5 -> :sswitch_2
        0x7d6 -> :sswitch_0
        0x7d7 -> :sswitch_2
        0x7d8 -> :sswitch_0
        0x7d9 -> :sswitch_2
        0x7da -> :sswitch_0
        0x7db -> :sswitch_2
        0x7dc -> :sswitch_0
        0x7dd -> :sswitch_2
        0x7de -> :sswitch_0
        0x7df -> :sswitch_2
        0xbc6 -> :sswitch_0
        0xbc7 -> :sswitch_2
        0xbc8 -> :sswitch_0
        0xbc9 -> :sswitch_2
        0xbca -> :sswitch_0
        0xbcb -> :sswitch_2
        0xbd0 -> :sswitch_0
        0xbd1 -> :sswitch_2
        0xbe5 -> :sswitch_0
        0xbe6 -> :sswitch_2
        0xbe7 -> :sswitch_0
        0xbe8 -> :sswitch_2
        0xbfb -> :sswitch_0
        0xbfc -> :sswitch_2
        0xbfd -> :sswitch_0
        0xbfe -> :sswitch_2
        0xbff -> :sswitch_0
        0xc00 -> :sswitch_2
        0xc01 -> :sswitch_0
        0xc02 -> :sswitch_2
        0xc03 -> :sswitch_0
        0xc04 -> :sswitch_2
        0xfba -> :sswitch_0
        0xfbb -> :sswitch_2
        0xfbc -> :sswitch_0
        0xfbd -> :sswitch_2
        0xfbe -> :sswitch_0
        0xfbf -> :sswitch_2
        0xfc0 -> :sswitch_0
        0xfc1 -> :sswitch_2
        0xfc2 -> :sswitch_0
        0xfc3 -> :sswitch_2
        0xfd1 -> :sswitch_0
        0xfd2 -> :sswitch_2
        0xfd3 -> :sswitch_0
        0xfd4 -> :sswitch_2
        0xfd5 -> :sswitch_0
        0xfd6 -> :sswitch_2
        0xfd9 -> :sswitch_0
        0xfda -> :sswitch_2
        0xfdd -> :sswitch_0
        0xfde -> :sswitch_2
        0xfdf -> :sswitch_0
        0xfe0 -> :sswitch_2
        0xfe7 -> :sswitch_0
        0xfe8 -> :sswitch_2
        0xfeb -> :sswitch_0
        0xfec -> :sswitch_2
        0xfed -> :sswitch_0
        0xfee -> :sswitch_2
        0xfef -> :sswitch_0
        0xff0 -> :sswitch_2
        0xff1 -> :sswitch_0
        0xff2 -> :sswitch_2
        0xff3 -> :sswitch_0
        0xff4 -> :sswitch_2
        0xff5 -> :sswitch_0
        0xff6 -> :sswitch_2
        0xffa -> :sswitch_0
        0xffb -> :sswitch_2
        0xffc -> :sswitch_0
        0xffd -> :sswitch_2
        0xffe -> :sswitch_0
        0xfff -> :sswitch_2
        0x138e -> :sswitch_0
        0x138f -> :sswitch_1
        0x1393 -> :sswitch_0
        0x1394 -> :sswitch_2
        0x1395 -> :sswitch_0
        0x1396 -> :sswitch_2
        0x1397 -> :sswitch_0
        0x1398 -> :sswitch_2
        0x1399 -> :sswitch_0
        0x139a -> :sswitch_2
    .end sparse-switch
.end method

.method private handleGenericInsert(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p2}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/16 v2, 0x3fa

    if-ne v1, v2, :cond_0

    const-string v2, "For_HRM"

    const-string v3, "for_hrm"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p3}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v2

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string/jumbo v3, "xxx.x.x"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "_id"

    invoke-direct {p0, p3}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getIdTimeStamp(Landroid/content/ContentValues;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0, p3, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_2

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p2, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    invoke-static {p2, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3ee -> :sswitch_0
        0x3f0 -> :sswitch_0
        0x7d4 -> :sswitch_0
        0x7d6 -> :sswitch_0
        0x7d8 -> :sswitch_0
        0x7da -> :sswitch_0
        0x7dc -> :sswitch_0
        0x7de -> :sswitch_0
        0xbc6 -> :sswitch_0
        0xbc8 -> :sswitch_0
        0xbca -> :sswitch_0
        0xbd0 -> :sswitch_0
        0xbe5 -> :sswitch_0
        0xbe7 -> :sswitch_0
        0xbfb -> :sswitch_0
        0xbfd -> :sswitch_0
        0xbff -> :sswitch_0
        0xc01 -> :sswitch_0
        0xc03 -> :sswitch_0
        0xfba -> :sswitch_0
        0xfbc -> :sswitch_0
        0xfbe -> :sswitch_0
        0xfc2 -> :sswitch_0
        0xfd1 -> :sswitch_0
        0xfd3 -> :sswitch_0
        0xfd5 -> :sswitch_0
        0xfd9 -> :sswitch_0
        0xfdd -> :sswitch_0
        0xfdf -> :sswitch_0
        0xfe7 -> :sswitch_0
        0xfeb -> :sswitch_0
        0xfed -> :sswitch_0
        0xfef -> :sswitch_0
        0xff1 -> :sswitch_0
        0xff3 -> :sswitch_0
        0xff5 -> :sswitch_0
        0xffa -> :sswitch_0
        0xffc -> :sswitch_0
        0xffe -> :sswitch_0
        0x1393 -> :sswitch_0
        0x1395 -> :sswitch_0
        0x1397 -> :sswitch_0
        0x1399 -> :sswitch_0
    .end sparse-switch
.end method

.method private handleGenericQuery(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    const/4 v5, 0x0

    new-instance v0, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;

    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p2}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    :goto_0
    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, v5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->query(Lsamsung/database/sqlite/SecSQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :cond_0
    return-object v0

    :sswitch_1
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3ec -> :sswitch_0
        0x3ee -> :sswitch_0
        0x3ef -> :sswitch_1
        0x3f0 -> :sswitch_0
        0x7d4 -> :sswitch_0
        0x7d5 -> :sswitch_1
        0x7d6 -> :sswitch_0
        0x7d7 -> :sswitch_1
        0x7d8 -> :sswitch_0
        0x7d9 -> :sswitch_1
        0x7da -> :sswitch_0
        0x7db -> :sswitch_1
        0x7dc -> :sswitch_0
        0x7dd -> :sswitch_1
        0x7de -> :sswitch_0
        0x7df -> :sswitch_1
        0xbc6 -> :sswitch_0
        0xbc7 -> :sswitch_1
        0xbc8 -> :sswitch_0
        0xbc9 -> :sswitch_1
        0xbca -> :sswitch_0
        0xbcb -> :sswitch_1
        0xbd0 -> :sswitch_0
        0xbd1 -> :sswitch_1
        0xbe5 -> :sswitch_0
        0xbe6 -> :sswitch_1
        0xbe7 -> :sswitch_0
        0xbe8 -> :sswitch_1
        0xbfb -> :sswitch_0
        0xbfc -> :sswitch_1
        0xbfd -> :sswitch_0
        0xbfe -> :sswitch_1
        0xbff -> :sswitch_0
        0xc00 -> :sswitch_1
        0xc01 -> :sswitch_0
        0xc02 -> :sswitch_1
        0xc03 -> :sswitch_0
        0xc04 -> :sswitch_1
        0xfba -> :sswitch_0
        0xfbb -> :sswitch_1
        0xfbc -> :sswitch_0
        0xfbd -> :sswitch_1
        0xfbe -> :sswitch_0
        0xfbf -> :sswitch_1
        0xfc0 -> :sswitch_0
        0xfc1 -> :sswitch_1
        0xfc2 -> :sswitch_0
        0xfc3 -> :sswitch_1
        0xfd1 -> :sswitch_0
        0xfd2 -> :sswitch_1
        0xfd3 -> :sswitch_0
        0xfd4 -> :sswitch_1
        0xfd5 -> :sswitch_0
        0xfd6 -> :sswitch_1
        0xfd9 -> :sswitch_0
        0xfda -> :sswitch_1
        0xfdd -> :sswitch_0
        0xfde -> :sswitch_1
        0xfdf -> :sswitch_0
        0xfe0 -> :sswitch_1
        0xfe7 -> :sswitch_0
        0xfe8 -> :sswitch_1
        0xfeb -> :sswitch_0
        0xfec -> :sswitch_1
        0xfed -> :sswitch_0
        0xfee -> :sswitch_1
        0xfef -> :sswitch_0
        0xff0 -> :sswitch_1
        0xff1 -> :sswitch_0
        0xff2 -> :sswitch_1
        0xff3 -> :sswitch_0
        0xff4 -> :sswitch_1
        0xff5 -> :sswitch_0
        0xff6 -> :sswitch_1
        0xffa -> :sswitch_0
        0xffb -> :sswitch_1
        0xffc -> :sswitch_0
        0xffd -> :sswitch_1
        0xffe -> :sswitch_0
        0xfff -> :sswitch_1
        0x138e -> :sswitch_0
        0x1393 -> :sswitch_0
        0x1394 -> :sswitch_1
        0x1395 -> :sswitch_0
        0x1396 -> :sswitch_1
        0x1397 -> :sswitch_0
        0x1398 -> :sswitch_1
        0x1399 -> :sswitch_0
        0x139a -> :sswitch_1
    .end sparse-switch
.end method

.method private handleGenericUpdate(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p2}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot get key from trust zone"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, ""

    invoke-static {p3}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    sparse-switch v2, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string/jumbo v3, "sync_status = 170002"

    const-string v0, "_id"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    :goto_0
    move-object v0, p1

    move-object v2, p3

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v6

    const-string/jumbo v0, "xxx.x.x"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "sync_status"

    const v2, 0x29813

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    const-string/jumbo v3, "sync_status != 170002"

    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    :goto_1
    move-object v0, p1

    move-object v2, p3

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    add-int/2addr v0, v6

    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    return v0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :sswitch_1
    const-string v0, "\'"

    move-object v6, v0

    :goto_3
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    const-string v0, " "

    const-string v2, "_id"

    invoke-virtual {p3, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v0, " AND "

    move-object v7, v0

    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "sync_status"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x29812

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id ="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " and "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p4, :cond_8

    const-string v0, ""

    :goto_5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v2, p3

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v9

    const-string/jumbo v0, "xxx.x.x"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "sync_status"

    const v2, 0x29813

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "sync_status"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " != "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x29812

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p4, :cond_7

    const-string p4, ""

    :cond_7
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v2, p3

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    add-int/2addr v0, v9

    goto/16 :goto_2

    :cond_8
    move-object v0, p4

    goto/16 :goto_5

    :cond_9
    move-object v7, v0

    goto/16 :goto_4

    :sswitch_2
    move-object v6, v0

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3eb -> :sswitch_1
        0x3ec -> :sswitch_0
        0x3ed -> :sswitch_1
        0x3ee -> :sswitch_0
        0x3ef -> :sswitch_2
        0x3f0 -> :sswitch_0
        0x3f1 -> :sswitch_1
        0x7d4 -> :sswitch_0
        0x7d5 -> :sswitch_2
        0x7d6 -> :sswitch_0
        0x7d7 -> :sswitch_2
        0x7d8 -> :sswitch_0
        0x7d9 -> :sswitch_2
        0x7da -> :sswitch_0
        0x7db -> :sswitch_2
        0x7dc -> :sswitch_0
        0x7dd -> :sswitch_2
        0x7de -> :sswitch_0
        0x7df -> :sswitch_2
        0xbc6 -> :sswitch_0
        0xbc7 -> :sswitch_2
        0xbc8 -> :sswitch_0
        0xbc9 -> :sswitch_2
        0xbca -> :sswitch_0
        0xbcb -> :sswitch_2
        0xbd0 -> :sswitch_0
        0xbd1 -> :sswitch_2
        0xbe5 -> :sswitch_0
        0xbe6 -> :sswitch_2
        0xbe7 -> :sswitch_0
        0xbe8 -> :sswitch_2
        0xbfb -> :sswitch_0
        0xbfc -> :sswitch_2
        0xbfd -> :sswitch_0
        0xbfe -> :sswitch_2
        0xbff -> :sswitch_0
        0xc00 -> :sswitch_2
        0xc01 -> :sswitch_0
        0xc02 -> :sswitch_2
        0xc03 -> :sswitch_0
        0xc04 -> :sswitch_2
        0xfba -> :sswitch_0
        0xfbb -> :sswitch_2
        0xfbc -> :sswitch_0
        0xfbd -> :sswitch_2
        0xfbe -> :sswitch_0
        0xfbf -> :sswitch_2
        0xfc0 -> :sswitch_0
        0xfc1 -> :sswitch_2
        0xfc2 -> :sswitch_0
        0xfc3 -> :sswitch_2
        0xfd1 -> :sswitch_0
        0xfd2 -> :sswitch_2
        0xfd3 -> :sswitch_0
        0xfd4 -> :sswitch_2
        0xfd5 -> :sswitch_0
        0xfd6 -> :sswitch_2
        0xfd9 -> :sswitch_0
        0xfda -> :sswitch_2
        0xfdd -> :sswitch_0
        0xfde -> :sswitch_2
        0xfdf -> :sswitch_0
        0xfe0 -> :sswitch_2
        0xfe7 -> :sswitch_0
        0xfe8 -> :sswitch_2
        0xfeb -> :sswitch_0
        0xfec -> :sswitch_2
        0xfed -> :sswitch_0
        0xfee -> :sswitch_2
        0xfef -> :sswitch_0
        0xff0 -> :sswitch_2
        0xff1 -> :sswitch_0
        0xff2 -> :sswitch_2
        0xff3 -> :sswitch_0
        0xff4 -> :sswitch_2
        0xff5 -> :sswitch_0
        0xff6 -> :sswitch_2
        0xffa -> :sswitch_0
        0xffb -> :sswitch_2
        0xffc -> :sswitch_0
        0xffd -> :sswitch_2
        0xffe -> :sswitch_0
        0xfff -> :sswitch_2
        0x138e -> :sswitch_0
        0x138f -> :sswitch_1
        0x1393 -> :sswitch_0
        0x1394 -> :sswitch_2
        0x1395 -> :sswitch_0
        0x1396 -> :sswitch_2
        0x1397 -> :sswitch_0
        0x1398 -> :sswitch_2
        0x1399 -> :sswitch_0
        0x139a -> :sswitch_2
    .end sparse-switch
.end method

.method private handleHDID(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "hdid"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hdid"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :pswitch_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3ec
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleUpdateTime(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 4

    const-string/jumbo v0, "xxx.x.x"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_3
    const-string v0, "create_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "create_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :cond_4
    const-string v0, "daylight_saving"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->DAYLIGHTSAVING_URI_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "sample_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "sample_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getDSTSaving(J)I

    move-result v0

    :goto_1
    if-nez v0, :cond_7

    const-string v0, "daylight_saving"

    const v1, 0x445c2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getDSTSaving(J)I

    move-result v0

    goto :goto_1

    :cond_7
    const-string v0, "daylight_saving"

    const v1, 0x445c1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0
.end method

.method private isNetworkAvailble()Z
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "key"

    invoke-virtual {v2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-virtual {v2, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    const-string/jumbo v1, "shared_pref"

    const-string v3, "key=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "shared_pref"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    :cond_0
    return-void
.end method

.method private validateInitializationStatus()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "validateInitializationStatus : mIsInitialized = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->isInitialized()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-static {}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return-void

    :cond_1
    :try_start_1
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "validateInitializationStatus : waiting..."

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v1, "call applyBatch"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot get key from trust zone"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 13

    const-wide/16 v11, -0x1

    const/4 v5, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v1, "bulkinsert .."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v2

    :goto_0
    return v2

    :sswitch_0
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v1, "bulkinsert"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    invoke-direct {p0, p1, v5}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Write Permission is not Available for the Uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    move v3, v2

    :goto_1
    array-length v6, p2

    if-ge v3, v6, :cond_4

    aget-object v6, p2, v3

    if-nez v6, :cond_1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    const-string/jumbo v6, "xxx.x.x"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    aget-object v6, p2, v3

    const-string/jumbo v7, "sync_status"

    const v8, 0x29812

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    aget-object v6, p2, v3

    invoke-direct {p0, p1, v6, v0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleCreateTime(Landroid/net/Uri;Landroid/content/ContentValues;J)V

    aget-object v6, p2, v3

    invoke-direct {p0, p1, v6}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleApplicationId(Landroid/net/Uri;Landroid/content/ContentValues;)V

    aget-object v6, p2, v3

    invoke-direct {p0, p1, v6}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleHDID(Landroid/net/Uri;Landroid/content/ContentValues;)V

    aget-object v6, p2, v3

    const-string v7, "_id"

    aget-object v8, p2, v3

    invoke-direct {p0, v8, v0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getIdTimeStamp(Landroid/content/ContentValues;J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v6

    aget-object v7, p2, v3

    invoke-static {v7}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v9, p2, v3

    invoke-virtual {v5, v6, v8, v9, v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v6

    cmp-long v6, v6, v11

    if-nez v6, :cond_3

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " values: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v3, p2, v3

    invoke-virtual {v3}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    goto/16 :goto_0

    :cond_3
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto/16 :goto_2

    :cond_4
    :try_start_1
    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, v10, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    array-length v2, p2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :sswitch_1
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bulkinsert "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    invoke-direct {p0, p1, v5}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Write Permission is not Available for the Uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-direct {p0, v4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getDatabase(I)Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move v0, v2

    :goto_3
    array-length v4, p2

    if-ge v0, v4, :cond_9

    aget-object v4, p2, v0

    if-nez v4, :cond_7

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    aget-object v4, p2, v0

    const-string v5, "_id"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    sget-object v4, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Creating new Id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v4, p2, v0

    const-string v5, "_id"

    aget-object v6, p2, v0

    invoke-direct {p0, v6}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getIdTimeStamp(Landroid/content/ContentValues;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_8
    aget-object v4, p2, v0

    invoke-static {v4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    aget-object v5, p2, v0

    invoke-virtual {v1, v3, v10, v5, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v4

    cmp-long v4, v4, v11

    if-nez v4, :cond_6

    sget-object v3, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v0, p2, v0

    invoke-virtual {v0}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    array-length v2, p2

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3e7 -> :sswitch_1
        0x3e8 -> :sswitch_1
        0xbca -> :sswitch_0
        0xbd0 -> :sswitch_0
        0xbfd -> :sswitch_0
        0xc01 -> :sswitch_0
    .end sparse-switch
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12

    const-wide/16 v10, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v9, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call test, method : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p3, :cond_2e

    const-string/jumbo v0, "value"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    instance-of v0, v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    :cond_0
    const-string v0, "CONFIG_OPTION_PUT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->URI_PROFILE:Landroid/net/Uri;

    invoke-direct {p0, v0, v4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Write Permission is not Available for profile "

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "CONFIG_OPTION_GET"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->URI_PROFILE:Landroid/net/Uri;

    invoke-direct {p0, v0, v7}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p3, :cond_2

    const-string v0, "GET_SAMSUNG_ACCOUNT"

    const-string v1, "key"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Read Permission is not Available for profile"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-eqz p1, :cond_4

    if-nez p3, :cond_5

    :cond_4
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "parameter method or extras should not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const-string/jumbo v0, "value"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    const-string v0, "CONFIG_OPTION_GET"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    if-eqz v8, :cond_d

    instance-of v0, v8, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v0, :cond_d

    check-cast v8, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    :try_start_0
    const-string/jumbo v1, "shared_pref"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_8

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    :try_start_1
    const-string v0, "key"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_7
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    if-eqz v9, :cond_9

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_9
    const-string/jumbo v0, "value"

    invoke-virtual {p3, v0, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_a
    :goto_2
    return-object p3

    :pswitch_1
    :try_start_3
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivityType(I)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setBirthDate(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v9, :cond_b

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0

    :pswitch_3
    :try_start_4
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setCountry(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setDistanceUnit(I)V

    goto :goto_1

    :pswitch_5
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setGender(I)V

    goto :goto_1

    :pswitch_6
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    goto :goto_1

    :pswitch_7
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V

    goto/16 :goto_1

    :pswitch_8
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setName(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_9
    const-string/jumbo v0, "update_time"

    const-string/jumbo v1, "value"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p3, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_1

    :pswitch_a
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    goto/16 :goto_1

    :pswitch_b
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    goto/16 :goto_1

    :pswitch_c
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setProfileDisclose(Z)V

    goto/16 :goto_1

    :cond_c
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setProfileDisclose(Z)V

    goto/16 :goto_1

    :pswitch_d
    const-string/jumbo v0, "profile_create_time"

    const-string/jumbo v1, "value"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p3, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_1

    :pswitch_e
    const-string/jumbo v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setTemperatureUnit(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    :cond_d
    const-string v0, "key"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    const-string v1, "GET_SAMSUNG_ACCOUNT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    const-string v1, "key"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->isNetworkAvailble()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    const-string v0, "key"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v1, "NONE"

    const-string v0, "ACCOUNT_NOT_FOUND"

    if-eqz v3, :cond_e

    array-length v2, v3

    if-gtz v2, :cond_f

    :cond_e
    sget-object v2, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v3, "No account found."

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    move-object v1, v9

    :goto_3
    const-string v3, "AccountType"

    invoke-virtual {p3, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Status"

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Account"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_2

    :cond_f
    const-string v0, "ACCOUNT_FOUND"

    const-string v2, "SAMSUNG_ACCOUNT"

    aget-object v1, v3, v7

    goto :goto_3

    :cond_10
    move-object p3, v9

    goto/16 :goto_2

    :cond_11
    if-eqz v0, :cond_12

    const-string v1, "SET_WIFI_STATUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    const-string/jumbo v0, "value"

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "SET_WIFI_STATUS"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_2

    :cond_12
    if-eqz v0, :cond_13

    const-string v1, "autobackup_interval"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    const-string/jumbo v0, "value"

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "autobackup_interval"

    const-wide/16 v3, 0x2a30

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-virtual {p3, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_2

    :cond_13
    if-eqz v0, :cond_15

    const-string v1, "GET_SYNC_STATUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v0, "Account"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "GET_SYNC_STATUS"

    invoke-virtual {v9, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_14
    move-object p3, v9

    goto/16 :goto_2

    :cond_15
    if-eqz v0, :cond_17

    const-string v1, "last_backup_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "last_restore_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    :cond_16
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    const-string/jumbo v1, "value"

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v0, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {p3, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_2

    :cond_17
    if-eqz v0, :cond_18

    const-string v1, "SHEALTH_2_5_EXISTS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "value"

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkShealth25Exists(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_2

    :cond_18
    if-eqz v0, :cond_19

    const-string v1, "SHEALTH_2_5_EXISTS_KIES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "value"

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkShealth25Exists(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_2

    :cond_19
    if-eqz v0, :cond_1a

    const-string v1, "Is_Restore_Completed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    const-string/jumbo v1, "value"

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v0, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_2

    :cond_1a
    if-eqz v8, :cond_1d

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    const-string v0, "key"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "key"

    invoke-virtual {p3, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v1, "shared_pref"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "value"

    aput-object v3, v2, v7

    const-string v3, "key=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v5, v4, v7

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1b

    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1b
    instance-of v2, v8, Ljava/lang/Integer;

    if-eqz v2, :cond_1c

    const-string/jumbo v2, "value"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_4
    if-eqz v1, :cond_a

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_1c
    :try_start_6
    instance-of v2, v8, Ljava/lang/Float;

    if-eqz v2, :cond_1e

    const-string/jumbo v2, "value"

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_4

    :catch_1
    move-exception v0

    if-eqz v1, :cond_1d

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1d
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Call method cannot used for private uses"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1e
    :try_start_7
    instance-of v2, v8, Ljava/lang/Long;

    if-eqz v2, :cond_20

    const-string/jumbo v2, "value"

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {p3, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_4

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_1f

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1f
    throw v0

    :cond_20
    :try_start_8
    instance-of v2, v8, Ljava/lang/Boolean;

    if-eqz v2, :cond_21

    const-string/jumbo v2, "value"

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    :cond_21
    const-string/jumbo v2, "value"

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_4

    :cond_22
    const-string v0, "CONFIG_OPTION_PUT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    if-eqz v8, :cond_24

    instance-of v0, v8, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v0, :cond_24

    const-string v0, "Profile has been updated"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    check-cast v8, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    const-string v0, "country"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "name"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "activity_type"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "birth_date"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "distance_unit"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "gender"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "height"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "height_unit"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "update_time"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getLastProfileUpdateTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "weight_unit"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "profile_disclose_yn"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v0

    if-eqz v0, :cond_23

    const-string v0, "Y"

    :goto_5
    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "profile_create_time"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileCreateTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "in_sync"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "temperature_unit"

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getTemperatureUnit()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->setProfileItems(ILjava/lang/String;)V

    goto/16 :goto_2

    :cond_23
    const-string v0, "N"

    goto :goto_5

    :cond_24
    const-string v0, "key"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_25

    const-string v0, "SET_WIFI_STATUS"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    const-string/jumbo v0, "value"

    invoke-virtual {p3, v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "SET_WIFI_STATUS"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    goto/16 :goto_2

    :cond_25
    if-eqz v6, :cond_26

    const-string v0, "autobackup_interval"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    const-string/jumbo v0, "value"

    invoke-virtual {p3, v0, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "autobackup_interval"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    goto/16 :goto_2

    :cond_26
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->mapProfileKey:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "key"

    invoke-virtual {v2, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v1, "shared_pref"

    const-string v3, "key=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v6, v4, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    if-nez v0, :cond_a

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v1, "shared_pref"

    invoke-virtual {v0, v1, v9, v2, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    goto/16 :goto_2

    :cond_27
    if-eqz v6, :cond_28

    const-string v0, "Is_Restore_Completed"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const-string/jumbo v0, "value"

    invoke-virtual {p3, v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "Is_Restore_Completed"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    goto/16 :goto_2

    :cond_28
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Call method cannot used for private uses"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_29
    const-string v0, "file_manager"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    if-eqz p2, :cond_2a

    const-string v0, "file_manager_create"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    const-string v0, "SYNCADAPTER"

    const-string v1, "INSIDE FILE MANAGER CREATE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "file_manager_details"

    invoke-static {}, Lcom/sec/android/service/health/cp/files/FileManager;->createProfileImageFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_2a
    if-eqz p2, :cond_2c

    const-string v0, "file_manager_rename"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-static {}, Lcom/sec/android/service/health/cp/files/FileManager;->getProfileImageFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2b

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_old"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    :cond_2b
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/files/FileManager;->createProfileImageFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_temp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    const-string v1, "SYNCADAPTER"

    const-string v2, "INSIDE FILE MANAGER RENAME"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "file_manager_modify"

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-virtual {p3, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_2

    :cond_2c
    invoke-static {}, Lcom/sec/android/service/health/cp/files/FileManager;->getProfileImageFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "SYNCADAPTER"

    const-string v1, "INSIDE FILE MANAGER DETAILS"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "file_manager_modify"

    invoke-static {}, Lcom/sec/android/service/health/cp/files/FileManager;->getProfileImageFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-virtual {p3, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "file_manager_details"

    invoke-static {}, Lcom/sec/android/service/health/cp/files/FileManager;->getProfileImageFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_2d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "illegal method"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2e
    move-object v0, v9

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call delete, URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selectionArgs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Write Permission is not Available for the Uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot get key from trust zone"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleGenericDelete(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 12

    const/4 v4, 0x1

    const-wide/16 v10, -0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call insert, URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call insert, URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ContentValues IS NULL!!! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v5

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    invoke-direct {p0, p1, v4}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Write Permission is not Available for the Uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/16 v0, 0x3e7

    if-eq v1, v0, :cond_2

    const/16 v0, 0x3e8

    if-ne v1, v0, :cond_6

    :cond_2
    const-string/jumbo v0, "type"

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string v1, "Type table is not available : ignore"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Insert "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getDatabase(I)Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Creating new Id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "_id"

    invoke-direct {p0, p2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getIdTimeStamp(Landroid/content/ContentValues;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_4
    invoke-static {p2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v5, p2, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v0

    cmp-long v2, v0, v10

    if-nez v2, :cond_5

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Replace fail "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot get key from trust zone"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    const-string/jumbo v2, "xxx.x.x"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string/jumbo v2, "sync_status"

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_8
    invoke-direct {p0, p1, v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleCreateTime(Landroid/net/Uri;Landroid/content/ContentValues;)V

    invoke-direct {p0, p1, v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleApplicationId(Landroid/net/Uri;Landroid/content/ContentValues;)V

    invoke-direct {p0, p1, v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleHDID(Landroid/net/Uri;Landroid/content/ContentValues;)V

    sparse-switch v1, :sswitch_data_0

    invoke-direct {p0, v0, p1, v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleGenericInsert(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0

    :sswitch_0
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_id"

    invoke-direct {p0, v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getIdTimeStamp(Landroid/content/ContentValues;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v2

    invoke-virtual {v0, v1, v5, v8, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v0

    cmp-long v2, v0, v10

    if-nez v2, :cond_9

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " values: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0

    :sswitch_1
    const-string/jumbo v2, "package_name"

    invoke-virtual {v8, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "_id"

    invoke-virtual {v8, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Package name information is missing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v9

    const-string/jumbo v3, "package_name = ? "

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v6, "package_name"

    invoke-virtual {v8, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v9

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Another insert with same package name cannot be allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    invoke-static {v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v3

    invoke-virtual {v0, v1, v5, v8, v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v0

    cmp-long v3, v0, v10

    if-nez v3, :cond_e

    if-eqz v2, :cond_d

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_d
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " values: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_e
    if-eqz v2, :cond_f

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0

    :sswitch_2
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v2

    invoke-virtual {v0, v1, v5, v8, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v0

    cmp-long v0, v0, v10

    if-nez v0, :cond_10

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " values: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    const-string v0, "_id"

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_2
        0x3ec -> :sswitch_1
        0xfc0 -> :sswitch_0
        0x138e -> :sswitch_2
    .end sparse-switch
.end method

.method public declared-synchronized onCreate()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate HCP"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    const/4 v5, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call query, URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " projection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sortOrder: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selectionArgs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Read Permission is not Available for the Uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->getSQLiteDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot get key from trust zone"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    new-instance v0, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;

    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;-><init>()V

    sparse-switch v2, :sswitch_data_0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleGenericQuery(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    :cond_2
    :goto_0
    return-object v5

    :sswitch_0
    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->query(Lsamsung/database/sqlite/SecSQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {v5, v0, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    :sswitch_1
    if-eqz p3, :cond_2

    invoke-virtual {v1, p3, p4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3e4 -> :sswitch_1
        0x3eb -> :sswitch_0
        0x3ed -> :sswitch_0
        0x3f1 -> :sswitch_0
        0x138f -> :sswitch_0
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call update, URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selectionArgs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->validateInitializationStatus()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    sget-object v0, Lcom/sec/android/service/health/cp/HealthContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->checkPermission(Landroid/net/Uri;Z)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Write Permission is not Available for the Uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot get key from trust zone"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/16 v2, 0x3e7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3e8

    if-ne v0, v2, :cond_3

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/cp/HealthContentProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "update "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getDatabase(I)Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    invoke-static {p2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->getValidationPolicy(Landroid/content/ContentValues;)Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    :goto_0
    return v0

    :cond_3
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    invoke-direct {p0, p1, v3}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleUpdateTime(Landroid/net/Uri;Landroid/content/ContentValues;)V

    move-object v0, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/HealthContentProvider;->handleGenericUpdate(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
