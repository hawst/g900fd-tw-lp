.class public Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAggregatedCount:I

.field private mAltitudeGain:D

.field private mAltitudeLoss:D

.field private mCadence:J

.field private mCadenceType:I

.field private mDistance:F

.field private mDuration:J

.field private mEnergyExpended:F

.field private mExerciseCadence:J

.field private mIncline:F

.field private mMaxCadence:J

.field private mMaxHeartRate:I

.field private mMaxSpeed:F

.field private mMeanCadence:F

.field private mMeanHeartRate:F

.field private mMeanSpeed:F

.field private mPower:F

.field private mRealTimeCadencePerMinute:F

.field private mRealTimeCaloricBurnPerHour:F

.field private mRealTimeCount:I

.field private mRealTimeHeartRate:F

.field private mRealTimeMet:F

.field private mRealTimeSpeed:F

.field private mResistance:I

.field private mStrideLength:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    return-void
.end method

.method private checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;)Z
    .locals 3

    const/4 v0, 0x0

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->duration:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data will be skipped. mandatory fields empty"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->duration:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-nez v1, :cond_1

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sensor issue - not all mandatory fields are assigned"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 11

    monitor-enter p0

    :try_start_0
    array-length v6, p1

    const/4 v1, 0x0

    move v5, v1

    :goto_0
    if-ge v5, v6, :cond_9

    aget-object v2, p1, v5

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    move-object v1, v0

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    return v1

    :cond_0
    :try_start_2
    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    const/4 v1, 0x0

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_b

    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistanceUnit:I

    const v3, 0x29812

    if-ne v1, v3, :cond_3

    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertYardsToMeters(F)F

    move-result v1

    move v4, v1

    :goto_2
    const/4 v1, 0x0

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxSpeed:F

    const v7, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_a

    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->speedUnit:I

    const v3, 0x30d42

    if-ne v1, v3, :cond_4

    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxSpeed:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertMilesToKiloMeters(F)F

    move-result v1

    move v3, v1

    :goto_3
    const/4 v1, 0x0

    iget v7, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanSpeed:F

    const v8, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_1

    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->speedUnit:I

    const v7, 0x30d42

    if-ne v1, v7, :cond_5

    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanSpeed:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertMilesToKiloMeters(F)F

    move-result v1

    :cond_1
    :goto_4
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "exercise_info__id"

    iget v9, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->equipmentType:I

    invoke-static {v7, v8, v9}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v8, "distance"

    invoke-static {v7, v8, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v8, "cadence"

    iget-wide v9, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadence:J

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v8, "input_source_type"

    const v9, 0x3f7a2

    invoke-static {v7, v8, v9}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v8, "total_calorie"

    iget v9, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeCalories:F

    invoke-static {v7, v8, v9}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v8, "start_time"

    iget-wide v9, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v8, "end_time"

    iget-wide v9, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    iget-object v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v8, :cond_2

    const-string v8, "application__id"

    iget-object v9, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v8

    if-nez v8, :cond_6

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error inserting exercise data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x0

    :try_start_3
    monitor-exit p0

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :cond_3
    :try_start_4
    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    move v4, v1

    goto/16 :goto_2

    :cond_4
    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxSpeed:F

    move v3, v1

    goto/16 :goto_3

    :cond_5
    iget v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanSpeed:F

    goto :goto_4

    :cond_6
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "exercise__id"

    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v9, v8}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "distance"

    invoke-static {v7, v8, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v4, "altitude_gain"

    iget-wide v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeAltitudeGain:D

    invoke-static {v7, v4, v8, v9}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v4, "altitude_loss"

    iget-wide v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeAltitudeLoss:D

    invoke-static {v7, v4, v8, v9}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v4, "cadence"

    iget-wide v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadence:J

    invoke-static {v7, v4, v8, v9}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v4, "calorie"

    iget v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeCalories:F

    invoke-static {v7, v4, v8}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v4, "max_speed_per_hour"

    invoke-static {v7, v4, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v3, "mean_speed_per_hour"

    invoke-static {v7, v3, v1}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_heart_rate_per_min"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxHeartRate:I

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "mean_heart_rate_per_min"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanHeartRate:F

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_cadence_rate_per_min"

    iget-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxCadence:J

    invoke-static {v7, v1, v3, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "mean_cadence_rate_per_min"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanCadence:F

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "incline"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->inclinePercent:F

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "resistance"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->resistanceLevel:I

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "power_watt"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->power:F

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "duration_millisecond"

    iget-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->duration:J

    invoke-static {v7, v1, v3, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "cadence_type"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->equipmentType:I

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "stride_length"

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cycleLength:F

    invoke-static {v7, v1, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "start_time"

    iget-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    invoke-static {v7, v1, v3, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "end_time"

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    invoke-static {v7, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error inserting exercise activity data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_8
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x1

    :try_start_5
    monitor-exit p0

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Error while inserting into db"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    monitor-exit p0

    goto/16 :goto_1

    :catch_1
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    :cond_a
    move v3, v1

    goto/16 :goto_3

    :cond_b
    move v4, v1

    goto/16 :goto_2
.end method

.method protected checkMandatoryFields()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;)Z

    move-result v0

    return v0
.end method

.method protected getActivityUpdate()Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "distance"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "altitude_gain"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAltitudeGain:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "altitude_loss"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAltitudeLoss:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "cadence"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mCadence:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "calorie"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mEnergyExpended:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_speed_per_hour"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxSpeed:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "mean_speed_per_hour"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanSpeed:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_heart_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxHeartRate:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "mean_heart_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanHeartRate:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_cadence_rate_per_min"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxCadence:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "mean_cadence_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanCadence:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "incline"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mIncline:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "resistance"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mResistance:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "power_watt"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mPower:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "duration_millisecond"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mDuration:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "cadence_type"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mCadenceType:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "stride_length"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mStrideLength:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    return-object v0
.end method

.method protected getContentValues()Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mExerciseId:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "data_type"

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "heart_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeHeartRate:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "speed_per_hour"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeSpeed:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "met"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeMet:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "calorie_burn_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCaloricBurnPerHour:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "cadence_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCadencePerMinute:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "stride_length"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mStrideLength:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "incline"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mIncline:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "resistance"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mResistance:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "power_watt"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mPower:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    return-object v0
.end method

.method protected getExerciseUpdate()Landroid/content/ContentValues;
    .locals 7

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "distance"

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v2, "cadence"

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mExerciseCadence:J

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadence:J

    add-long/2addr v3, v5

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v2, "total_calorie"

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeCalories:F

    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    return-object v1
.end method

.method protected processData()V
    .locals 9

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const/4 v6, 0x0

    const v8, 0x30d42

    const v7, 0x7fffffff

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    if-eqz v1, :cond_17

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->instantaneousHeartRate:I

    if-eq v1, v7, :cond_0

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeHeartRate:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->instantaneousHeartRate:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeHeartRate:F

    :cond_0
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->instantaneousSpeed:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeSpeed:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->instantaneousSpeed:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeSpeed:F

    :cond_1
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->intantaneousMet:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeMet:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->intantaneousMet:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeMet:F

    :cond_2
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->calorieBurnRate:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCaloricBurnPerHour:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->calorieBurnRate:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCaloricBurnPerHour:F

    :cond_3
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadencePerMinute:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCadencePerMinute:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadencePerMinute:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCadencePerMinute:F

    :cond_4
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_5

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistanceUnit:I

    const v2, 0x29812

    if-ne v1, v2, :cond_18

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertYardsToMeters(F)F

    move-result v1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mDistance:F

    :cond_5
    :goto_0
    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeAltitudeGain:D

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_6

    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeAltitudeGain:D

    iput-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAltitudeGain:D

    :cond_6
    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeAltitudeLoss:D

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_7

    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeAltitudeLoss:D

    iput-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAltitudeLoss:D

    :cond_7
    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadence:J

    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v1, v3

    if-eqz v1, :cond_8

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mCadence:J

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadence:J

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mCadence:J

    :cond_8
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeCalories:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_9

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeCalories:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mEnergyExpended:F

    :cond_9
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxSpeed:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_a

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->speedUnit:I

    if-ne v1, v8, :cond_19

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxSpeed:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertMilesToKiloMeters(F)F

    move-result v1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxSpeed:F

    :cond_a
    :goto_1
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanSpeed:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_b

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->speedUnit:I

    if-ne v1, v8, :cond_1a

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanSpeed:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertMilesToKiloMeters(F)F

    move-result v1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanSpeed:F

    :cond_b
    :goto_2
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->speedUnit:I

    if-ne v1, v8, :cond_c

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeSpeed:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertMilesToKiloMeters(F)F

    move-result v1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeSpeed:F

    :cond_c
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxHeartRate:I

    if-eq v1, v7, :cond_d

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxHeartRate:I

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxHeartRate:I

    :cond_d
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanHeartRate:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_e

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanHeartRate:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanHeartRate:F

    :cond_e
    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxCadence:J

    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v1, v3

    if-eqz v1, :cond_f

    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxCadence:J

    iput-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxCadence:J

    :cond_f
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanCadence:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_10

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanCadence:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanCadence:F

    :cond_10
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->inclinePercent:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_11

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mIncline:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->inclinePercent:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mIncline:F

    :cond_11
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->resistanceLevel:I

    if-eq v1, v7, :cond_12

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mResistance:I

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->resistanceLevel:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mResistance:I

    :cond_12
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->power:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_13

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mPower:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->power:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mPower:F

    :cond_13
    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->duration:J

    iput-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mDuration:J

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->equipmentType:I

    if-eq v1, v7, :cond_14

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->equipmentType:I

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mCadenceType:I

    :cond_14
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cycleLength:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_15

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mStrideLength:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mStrideLength:F

    iget v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cycleLength:F

    add-float/2addr v0, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mStrideLength:F

    :cond_15
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "distance"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "cadence"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "total_calorie"

    aput-object v1, v2, v0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mExerciseId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    const-wide/16 v2, 0x0

    :try_start_1
    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mExerciseCadence:J

    if-eqz v1, :cond_16

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "cadence"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mExerciseCadence:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_16
    if-eqz v1, :cond_17

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_17
    return-void

    :cond_18
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mDistance:F

    goto/16 :goto_0

    :cond_19
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxSpeed:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxSpeed:F

    goto/16 :goto_1

    :cond_1a
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanSpeed:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanSpeed:F

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_1b

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1b
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method protected resetAggregation()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeHeartRate:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeSpeed:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeMet:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCaloricBurnPerHour:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCadencePerMinute:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    return-void
.end method

.method protected resetInitialAggregation()V
    .locals 6

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mDistance:F

    iput-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAltitudeGain:D

    iput-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAltitudeLoss:D

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mCadence:J

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mEnergyExpended:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxSpeed:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanSpeed:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxHeartRate:I

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanHeartRate:F

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMaxCadence:J

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mMeanCadence:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mIncline:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mResistance:I

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mPower:F

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mDuration:J

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mCadenceType:I

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mStrideLength:F

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mRealTimeCount:I

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;->mAggregatedCount:I

    return-void
.end method
