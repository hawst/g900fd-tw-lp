.class public Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler$2;
    }
.end annotation


# static fields
.field private static final MSG_FITNESS_SUBSCRIBE_FITNESS_TYPE:I = 0xa

.field private static final TAG:Ljava/lang/String; = "[HealthSensor]FEAntProfileHandler"


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mFEState:I

.field private mFEType:I

.field private temp_cadence:J

.field private temp_calorie:F

.field private temp_distance:F

.field private temp_duration:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->ASLEEP_OFF:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEState:I

    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEType:I

    iput-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->temp_duration:J

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->temp_distance:F

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->temp_calorie:F

    iput-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->temp_cadence:J

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method private getFEStateToString(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;)Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusFitnessEquipmentPcc$EquipmentState:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "INVALID"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "OFF"

    goto :goto_0

    :pswitch_1
    const-string v0, "READY"

    goto :goto_0

    :pswitch_2
    const-string v0, "IN USE"

    goto :goto_0

    :pswitch_3
    const-string v0, "FINISHED/PAUSE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getFETypeToString(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;)Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusFitnessEquipmentPcc$EquipmentType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "INVALID"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "GENERAL"

    goto :goto_0

    :pswitch_1
    const-string v0, "TREADMILL"

    goto :goto_0

    :pswitch_2
    const-string v0, "ELLIPTICAL"

    goto :goto_0

    :pswitch_3
    const-string v0, "BIKE"

    goto :goto_0

    :pswitch_4
    const-string v0, "ROWER"

    goto :goto_0

    :pswitch_5
    const-string v0, "CLIMBER"

    goto :goto_0

    :pswitch_6
    const-string v0, "NORDIC SKIER"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private subscribeFitnessEquipmentToEvents(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;)V
    .locals 2

    const-string v0, "[HealthSensor]FEAntProfileHandler"

    const-string/jumbo v1, "subscribeFitnessEquipmentToEvents()"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getAntDeviceType()I
    .locals 1

    const/16 v0, 0x11

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getFEState()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEState:I

    return v0
.end method

.method public getFEType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEType:I

    return v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->getFEState()I

    move-result v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->READY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->getIntValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x7d1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->getFEState()I

    move-result v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->IN_USE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->getIntValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    const/16 v0, 0x7d2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->getFEState()I

    move-result v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->FINISHED_PAUSED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->getIntValue()I

    move-result v1

    if-ne v0, v1, :cond_2

    const/16 v0, 0x7d4

    goto :goto_0

    :cond_2
    const/16 v0, 0x7d0

    goto :goto_0
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 3

    const-string v0, "[HealthSensor]FEAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage() :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->subscribeFitnessEquipmentToEvents(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public notifyFEChannelStateChanged()V
    .locals 0

    return-void
.end method

.method protected requestAccess()V
    .locals 0

    return-void
.end method

.method public setFEState(I)Z
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEState:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEState:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFEType(I)Z
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEType:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;->mFEType:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method
