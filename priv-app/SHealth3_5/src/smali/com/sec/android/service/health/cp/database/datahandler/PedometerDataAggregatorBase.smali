.class public abstract Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field public static final AGGREGATION_TIME_INTERVAL_MIN:I = 0x927c0

.field private static final TAG:Ljava/lang/String;

.field private static bIsFirstUpdateFail:Z


# instance fields
.field protected mAppId:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field protected mCurrentMin:J

.field private mCurrentWalkInfoUri:Landroid/net/Uri;

.field protected mDevicePkId:Ljava/lang/String;

.field private mDistance:F

.field protected mDownStep:I

.field private mEnergyExpended:F

.field protected mExerciseId:J

.field protected mExercisePrevCadence:F

.field protected mExercisePrevDistance:F

.field protected mExercisePrevEnergy:F

.field protected mExerciseStartTime:J

.field protected mRunStep:I

.field protected mSamplePosition:I

.field private mSpeed:F

.field protected mTotalStep:I

.field protected mUpStep:I

.field protected mUpdownStep:I

.field public mUserDeviceId:Ljava/lang/String;

.field protected mWalkStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->bIsFirstUpdateFail:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    return-void
.end method

.method private checkAndInitializeTimeSlot()V
    .locals 9

    const-wide/32 v7, 0x927c0

    const/4 v6, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkAndInitializeTimeSlot  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    mul-long/2addr v2, v7

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    mul-long/2addr v4, v7

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "distance"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    const-string v0, "calorie"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mEnergyExpended:F

    const-string/jumbo v0, "total_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mTotalStep:I

    const-string/jumbo v0, "walk_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mWalkStep:I

    const-string/jumbo v0, "run_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mRunStep:I

    const-string/jumbo v0, "updown_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpdownStep:I

    const-string/jumbo v0, "up_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    const-string v0, "down_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    const-string/jumbo v0, "sample_position"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSamplePosition:I

    const-string v0, "hdid"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDevicePkId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private getContentValues()Landroid/content/ContentValues;
    .locals 6

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "distance"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string/jumbo v1, "speed"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSpeed:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string v1, "calorie"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mEnergyExpended:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string/jumbo v1, "updown_step"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpdownStep:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "up_step"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "down_step"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "total_step"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mTotalStep:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "walk_step"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mWalkStep:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "run_step"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mRunStep:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "sample_position"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSamplePosition:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "end_time"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method private insertIntoExerciseDeviceTable()V
    .locals 7

    const/4 v6, 0x0

    const-string v3, "exercise__id = ? and user_device__id = ? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUserDeviceId:Ljava/lang/String;

    aput-object v1, v4, v0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    :goto_0
    if-nez v1, :cond_2

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "exercise__id"

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v2, "user_device__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "application__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-void

    :cond_4
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v1, v6

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private updateData()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteConstraintException;
        }
    .end annotation

    const-wide/32 v6, 0x927c0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->getContentValues()Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentWalkInfoUri:Landroid/net/Uri;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentWalkInfoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "upCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->bIsFirstUpdateFail:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "First Pedometer data was not inserted."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "mAppID is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "appid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string/jumbo v1, "start_time"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    mul-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "end_time"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    mul-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentWalkInfoUri:Landroid/net/Uri;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->bIsFirstUpdateFail:Z

    :cond_2
    :goto_0
    return-void

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v1, "mCurrentWalkInfoUri is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addData-Start with exercise_id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    :goto_0
    return v1

    :cond_0
    :try_start_1
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/32 v6, 0x927c0

    div-long/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    sget-object v4, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " addData time = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v6, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " mCurrentMin "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/32 v6, 0x927c0

    div-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->resetAggregation()V

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->insertData()V

    :goto_1
    check-cast p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->updateExerciseSummery(J)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addData-End   : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto :goto_0

    :cond_1
    :try_start_3
    sget-object v4, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " update data time = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v6, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " mCurrentMin "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->updateData()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while updating db"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v3

    goto/16 :goto_0

    :catch_1
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v3

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method

.method protected checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z
    .locals 6

    const-wide v4, 0x7fffffffffffffffL

    const v3, 0x7fffffff

    const/4 v0, 0x0

    if-nez p1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "Data will be skipped. it is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    cmp-long v1, v1, v4

    if-nez v1, :cond_1

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-ne v1, v3, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "Data will be skipped. mandatory fields empty"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_2

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-ne v1, v3, :cond_3

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sensor issue - not all mandatory fields are assigned"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalizeAggregation()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v1, "Nothing to be done in finalization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected getExerciseDetails(J)V
    .locals 7

    const/4 v6, 0x0

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "distance"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "cadence"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "total_calorie"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "start_time"

    aput-object v1, v2, v0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "Loading Exercise Deatils"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "distance"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevDistance:F

    const-string v0, "cadence"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevCadence:F

    const-string/jumbo v0, "total_calorie"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevEnergy:F

    const-string/jumbo v0, "start_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseStartTime:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exercise Deatils -  ExercisePrevDistance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevDistance:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ExercisePrevCadence:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevCadence:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ExercisePrevEnergy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevEnergy:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ExerciseStartTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 7

    const/4 v6, 0x0

    const/4 v4, 0x1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUserDeviceId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-virtual {p0, p3, p4}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->getExerciseDetails(J)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->insertIntoExerciseDeviceTable()V

    const/16 v0, 0xc

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "distance"

    aput-object v1, v2, v0

    const-string/jumbo v0, "total_step"

    aput-object v0, v2, v4

    const/4 v0, 0x2

    const-string/jumbo v1, "run_step"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "walk_step"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "updown_step"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string/jumbo v1, "up_step"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "down_step"

    aput-object v1, v2, v0

    const/4 v0, 0x7

    const-string/jumbo v1, "speed"

    aput-object v1, v2, v0

    const/16 v0, 0x8

    const-string v1, "calorie"

    aput-object v1, v2, v0

    const/16 v0, 0x9

    const-string v1, "create_time"

    aput-object v1, v2, v0

    const/16 v0, 0xa

    const-string/jumbo v1, "start_time"

    aput-object v1, v2, v0

    const/16 v0, 0xb

    const-string v1, "_id"

    aput-object v1, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "create_time desc "

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "initializing member with last walkinfo record"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "distance"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    const-string/jumbo v0, "total_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mTotalStep:I

    const-string/jumbo v0, "run_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mRunStep:I

    const-string/jumbo v0, "walk_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mWalkStep:I

    const-string/jumbo v0, "updown_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpdownStep:I

    const-string/jumbo v0, "up_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    const-string v0, "down_step"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    const-string/jumbo v0, "speed"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSpeed:F

    const-string v0, "calorie"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mEnergyExpended:F

    const-string/jumbo v0, "start_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/32 v4, 0x927c0

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentWalkInfoUri:Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method protected insertData()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteConstraintException;
        }
    .end annotation

    const-wide/32 v8, 0x927c0

    const/4 v6, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v1, "insertData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->getContentValues()Landroid/content/ContentValues;

    move-result-object v7

    const-string/jumbo v0, "start_time"

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    mul-long/2addr v1, v8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "end_time"

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    mul-long/2addr v1, v8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    mul-long/2addr v4, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Data is duplicated time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    mul-long/2addr v3, v8

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentWalkInfoUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentWalkInfoUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v7, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mAppId:Ljava/lang/String;

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz v7, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentWalkInfoUri:Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    :try_start_3
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v2, "contentValues is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public isRunning(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mExerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "matched"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteConstraintException;
        }
    .end annotation

    const v7, 0x7f7fffff    # Float.MAX_VALUE

    const v6, 0x7fffffff

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    if-eqz p1, :cond_c

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distanceUnit:I

    const v1, 0x29812

    if-ne v0, v1, :cond_d

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertYardsToMeters(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    :goto_0
    iget-wide v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mCurrentMin:J

    const-wide/32 v4, 0x927c0

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_e

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSpeed:F

    :cond_0
    :goto_1
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mEnergyExpended:F

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mEnergyExpended:F

    :cond_1
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-eq v0, v6, :cond_2

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mTotalStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mTotalStep:I

    :cond_2
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    if-eq v0, v6, :cond_3

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mWalkStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mWalkStep:I

    :cond_3
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    if-eq v0, v6, :cond_4

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mRunStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mRunStep:I

    :cond_4
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-eq v0, v6, :cond_5

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpdownStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpdownStep:I

    :cond_5
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    if-eq v0, v6, :cond_6

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    :cond_6
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    if-eq v0, v6, :cond_7

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    :cond_7
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    if-eq v0, v6, :cond_8

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    :cond_8
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    if-eq v0, v6, :cond_9

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    :cond_9
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    if-eq v0, v6, :cond_a

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSamplePosition:I

    :cond_a
    iget-object v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->devicePkId:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->devicePkId:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDevicePkId:Ljava/lang/String;

    :cond_b
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pedometer_DATA :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-eq v0, v6, :cond_c

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_c

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_c

    iget-wide v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_c

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->processExerciseData(FIFJ)V

    :cond_c
    return-void

    :cond_d
    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    goto/16 :goto_0

    :cond_e
    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    long-to-float v0, v0

    div-float v0, v2, v0

    float-to-double v0, v0

    const-wide v2, 0x400ccccccccccccdL    # 3.6

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSpeed:F

    goto/16 :goto_1
.end method

.method protected processExerciseData(FIFJ)V
    .locals 3

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevDistance:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevDistance:F

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevCadence:F

    int-to-float v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevCadence:F

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevEnergy:F

    add-float/2addr v0, p3

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevEnergy:F

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "prepareExerciseData : XX-Exercise delta -  distance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " totalStep: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " calories: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -- XX-Exercise total - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " distance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevDistance:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " totalStep: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevCadence:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " calories: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevEnergy:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method resetAggregation()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string v1, " reset Aggregation "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDistance:F

    iput v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mEnergyExpended:F

    iput v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mTotalStep:I

    iput v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mWalkStep:I

    iput v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mRunStep:I

    iput v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpdownStep:I

    iput v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mUpStep:I

    iput v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDownStep:I

    iput v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mSamplePosition:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mDevicePkId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->checkAndInitializeTimeSlot()V

    return-void
.end method

.method protected updateExerciseSummery(J)V
    .locals 8

    const-wide/32 v4, 0xea60

    const-wide/16 v6, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateExerciseForBulkData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "distance"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevDistance:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string v1, "cadence"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevCadence:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string/jumbo v1, "total_calorie"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevEnergy:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string v1, "end_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "duration_min"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseStartTime:J

    sub-long v2, p1, v2

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseStartTime:J

    sub-long v1, p1, v1

    div-long v3, v1, v4

    cmp-long v5, v1, v6

    if-lez v5, :cond_0

    cmp-long v5, v3, v6

    if-lez v5, :cond_0

    const-wide/16 v5, 0x5a0

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    const-string v3, "duration_millisecond"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExerciseId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XX-Exercise Updated -  ExerciseDistance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevDistance:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ExerciseCadence:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevCadence:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ExerciseEnergy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;->mExercisePrevEnergy:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
