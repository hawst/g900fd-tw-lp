.class public Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
.implements Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;


# static fields
.field private static final PROFILE_NAME:Ljava/lang/String; = "hdp"

.field private static final PROTOCOL_NAME:Ljava/lang/String; = "Continua"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mContext:Landroid/content/Context;

.field private mData:Ljava/lang/Object;

.field private mDataType:I

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mErrorCode:I

.field private mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

.field private nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

.field premergePressureDataList:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;",
            ">;"
        }
    .end annotation
.end field

.field private uniqueId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->uniqueId:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol$1;-><init>(Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->premergePressureDataList:Landroid/util/LongSparseArray;

    return-void
.end method

.method private getMergedBpData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;
    .locals 6

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    if-eqz p1, :cond_6

    const v1, 0x186a0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->premergePressureDataList:Landroid/util/LongSparseArray;

    iget-wide v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    invoke-virtual {v0, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    if-eqz v0, :cond_7

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    int-to-float v4, v1

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    int-to-float v4, v2

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    :cond_0
    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    iput v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    :cond_1
    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    int-to-float v4, v1

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_2

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    int-to-float v4, v2

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    :cond_2
    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    iput v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    :cond_3
    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    if-gt v3, v1, :cond_4

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    if-ge v1, v2, :cond_5

    :cond_4
    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    iput v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->premergePressureDataList:Landroid/util/LongSparseArray;

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->remove(J)V

    :cond_6
    :goto_0
    return-object p1

    :cond_7
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    int-to-float v3, v1

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_8

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    int-to-float v3, v2

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_8

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    int-to-float v3, v1

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_8

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    int-to-float v3, v2

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_8

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    if-gt v0, v1, :cond_8

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    if-ge v0, v2, :cond_6

    :cond_8
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    int-to-float v3, v1

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_9

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    int-to-float v3, v2

    cmpg-float v0, v0, v3

    if-gez v0, :cond_a

    :cond_9
    iput v5, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    :cond_a
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    int-to-float v3, v1

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_b

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    int-to-float v3, v2

    cmpg-float v0, v0, v3

    if-gez v0, :cond_c

    :cond_b
    iput v5, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    :cond_c
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    if-gt v0, v1, :cond_d

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    if-ge v0, v2, :cond_e

    :cond_d
    const v0, 0x7fffffff

    iput v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    :cond_e
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->premergePressureDataList:Landroid/util/LongSparseArray;

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    invoke-virtual {v0, v1, v2, p1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static native nativeSendRawData(Ljava/lang/String;[BI)V
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->uniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->unregister(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocolName()Ljava/lang/String;
    .locals 1

    const-string v0, "Continua"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 2

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mData:Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->uniqueId:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->getNativeEventManager()Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->uniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->register(Ljava/lang/String;Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public notifyRawDataReceived([B)I
    .locals 5

    const/16 v4, -0x1a

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const/16 v0, 0x3e8

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "notifyRawDataReceived : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-byte v1, p1, v0

    if-ne v1, v4, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "notifyRawDataReceived ERROR"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    array-length v2, p1

    invoke-static {v1, p1, v2}, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->nativeSendRawData(Ljava/lang/String;[BI)V

    goto :goto_0

    :cond_2
    aget-byte v1, p1, v0

    const/16 v2, -0x1b

    if-eq v1, v2, :cond_3

    aget-byte v1, p1, v0

    if-ne v1, v4, :cond_1

    :cond_3
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "notifyRawDataReceived DISCONNECT CHANNEL"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public notifyStart()I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "notifyStart"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public notifyStop()I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "notifyStop"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onFinishSession()V
    .locals 5

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onFinishSession"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->premergePressureDataList:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->premergePressureDataList:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->premergePressureDataList:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iget v1, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mDataType:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    return-void
.end method

.method public onHealthServiceError(ILjava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthServiceError code"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " description"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    return-void
.end method

.method public onMeasuredDataReceived(Lcom/sec/android/service/health/sensor/data/MeasurementData;)V
    .locals 9

    const/4 v8, -0x1

    const/4 v0, 0x0

    const/4 v7, 0x0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onMeasuredDataReceived"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getDataSet()[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    move-result-object v1

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mErrorCode:I

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onMeasuredDataReceived type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getDeviceType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getDeviceType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onMeasuredDataReceived wrong device type"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mErrorCode:I

    :cond_0
    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_4

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x4a05

    if-ne v5, v6, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x4a06

    if-ne v5, v6, :cond_3

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    goto :goto_2

    :cond_3
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x482a

    if-ne v5, v6, :cond_1

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    goto :goto_2

    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->getMergedBpData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v1, v0, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->time:J

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v1, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto :goto_0

    :sswitch_3
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    array-length v3, v1

    :goto_3
    if-ge v0, v3, :cond_11

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe140

    if-ne v5, v6, :cond_5

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v5

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string v6, "kg"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    const v5, 0x1fbd1

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    :cond_5
    :goto_4
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe144

    if-ne v5, v6, :cond_b

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v5

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->height:F

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string v6, "cm"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    const v4, 0x249f1

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->heightUnit:I

    :cond_6
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string v6, "lb"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    const v5, 0x1fbd2

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    goto :goto_4

    :cond_8
    iput v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    goto :goto_4

    :cond_9
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ft"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    const v4, 0x249f2

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->heightUnit:I

    goto :goto_5

    :cond_a
    iput v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->heightUnit:I

    goto :goto_5

    :cond_b
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe150

    if-ne v5, v6, :cond_c

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMassIndex:F

    goto :goto_5

    :cond_c
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe14c

    if-ne v5, v6, :cond_d

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyFat:F

    goto :goto_5

    :cond_d
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf001

    if-ne v5, v6, :cond_e

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMetabolicRate:F

    goto :goto_5

    :cond_e
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf002

    if-ne v5, v6, :cond_f

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->visceralFat:F

    goto :goto_5

    :cond_f
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf003

    if-ne v5, v6, :cond_10

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyAge:I

    goto/16 :goto_5

    :cond_10
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf00a

    if-ne v5, v6, :cond_6

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->skeletalMuscle:F

    goto/16 :goto_5

    :cond_11
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v2, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_4
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->time:J

    array-length v3, v1

    :goto_6
    if-ge v0, v3, :cond_15

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x7114

    if-eq v5, v6, :cond_12

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x71b8

    if-ne v5, v6, :cond_13

    :cond_12
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v5

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucose:F

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "mg/dL"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    const v4, 0x222e1

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    :cond_13
    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_14
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "mmol/L"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    const v4, 0x222e2

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    goto :goto_7

    :cond_15
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v2, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1004 -> :sswitch_0
        0x1007 -> :sswitch_1
        0x1008 -> :sswitch_2
        0x100f -> :sswitch_3
        0x1011 -> :sswitch_4
        0x1029 -> :sswitch_5
        0x102a -> :sswitch_7
        0x1047 -> :sswitch_8
        0x1048 -> :sswitch_6
    .end sparse-switch
.end method

.method public onSensorInfoReceived(Lcom/sec/android/service/health/sensor/data/SensorInfo;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onSensorInfoReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStartSession()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onStartSession"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iget v1, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mDataType:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStarted(I)V

    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "request"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x3ea

    return v0
.end method

.method public sendDataToSensor([B)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sendDataToSensor"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->sendRawData([B)I

    return-void
.end method
