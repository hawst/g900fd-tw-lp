.class public Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/contentservice/HealthContentShow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "valueArrayAdapter"
.end annotation


# instance fields
.field private checkValue:Landroid/widget/CheckBox;

.field private itemChecked:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestTimeArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->itemChecked:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->items:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->mRequestTimeArray:Ljava/util/ArrayList;

    const/4 v0, 0x0

    const/4 v1, 0x0

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v2, v3

    long-to-int v3, v3

    if-gtz v3, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, v0

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, -0x3fe0084b7e455e73L    # -7.991899516120964

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x3fe0084b7e455e73L    # -7.991899516120964

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    :goto_0
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, 0x92b

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, 0x91b

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-eqz v3, :cond_5

    const-wide v3, -0x3fe0084b7e455e73L    # -7.991899516120964

    xor-long/2addr v0, v3

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v0, v3

    const/16 v3, 0x20

    shr-long/2addr v0, v3

    long-to-int v0, v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_e

    iget-object v3, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->itemChecked:Ljava/util/ArrayList;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x29

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x19

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_9

    const-wide v4, -0x3fe0084b7e455e73L    # -7.991899516120964

    xor-long/2addr v0, v4

    :cond_9
    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    shr-long/2addr v0, v4

    long-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 v3, 0x0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_a

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_b

    const-wide v4, -0x3fe0084b7e455e73L    # -7.991899516120964

    xor-long/2addr v0, v4

    :cond_b
    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    shr-long/2addr v0, v4

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v4, v2, v1

    long-to-int v1, v4

    if-gtz v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    const/4 v3, 0x0

    int-to-long v0, v0

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_d

    const-wide v6, -0x3fe0084b7e455e73L    # -7.991899516120964

    xor-long/2addr v0, v6

    :cond_d
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x3fe0084b7e455e73L    # -7.991899516120964

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    goto/16 :goto_0

    :cond_e
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->itemChecked:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 11

    const-wide v9, 0x38e1f1a0f6ef4cc0L

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 12

    const-wide v10, 0x32f92f8e340e8ad0L    # 3.826449992012688E-63

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v6

    ushr-long v3, v0, v6

    aget-wide v0, v2, v7

    cmp-long v5, v0, v8

    if-eqz v5, :cond_1

    xor-long/2addr v0, v10

    :cond_1
    ushr-long/2addr v0, v6

    shl-long/2addr v0, v6

    xor-long/2addr v0, v3

    xor-long/2addr v0, v10

    aput-wide v0, v2, v7

    return-wide v8
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 23

    const/4 v1, 0x2

    new-array v7, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v7, v1

    const/4 v1, 0x0

    array-length v2, v7

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v7, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    ushr-long v4, v1, v4

    aget-wide v1, v7, v3

    const-wide/16 v8, 0x0

    cmp-long v6, v1, v8

    if-eqz v6, :cond_1

    const-wide v8, 0x31b82df31aeff910L    # 3.503387335088009E-69

    xor-long/2addr v1, v8

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v1, v6

    const/16 v6, 0x20

    shl-long/2addr v1, v6

    xor-long/2addr v1, v4

    const-wide v4, 0x31b82df31aeff910L    # 3.503387335088009E-69

    xor-long/2addr v1, v4

    aput-wide v1, v7, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->mRequestTimeArray:Ljava/util/ArrayList;

    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v7, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x5a0c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x5a3c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v7, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_5

    const-wide v4, 0x31b82df31aeff910L    # 3.503387335088009E-69

    xor-long/2addr v1, v4

    :cond_5
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->dateFormet:Ljava/text/DateFormat;
    invoke-static {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$300(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->timeFormet:Ljava/text/DateFormat;
    invoke-static {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$400(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    sget v1, Lcom/sec/android/service/health/R$id;->date:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    sget v1, Lcom/sec/android/service/health/R$id;->time:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    sget v1, Lcom/sec/android/service/health/R$id;->value:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/sec/android/service/health/R$id;->value_symbol:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->items:Ljava/util/ArrayList;

    array-length v3, v7

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v7, v3

    long-to-int v3, v3

    if-gtz v3, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x4d

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x7d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, -0x47

    aput v21, v2, v20

    const/16 v20, 0x3552

    aput v20, v2, v19

    const/16 v19, 0x341

    aput v19, v2, v18

    const/16 v18, 0x3f62

    aput v18, v2, v17

    const/16 v17, 0x1453

    aput v17, v2, v16

    const/16 v16, -0xf8e

    aput v16, v2, v15

    const/16 v15, -0x62

    aput v15, v2, v14

    const/4 v14, -0x3

    aput v14, v2, v13

    const/16 v13, -0x44

    aput v13, v2, v12

    const/16 v12, -0x41c3

    aput v12, v2, v11

    const/16 v11, -0x35

    aput v11, v2, v10

    const/16 v10, 0x7623

    aput v10, v2, v9

    const/16 v9, 0x210f

    aput v9, v2, v8

    const/16 v8, 0x5440

    aput v8, v2, v3

    const/16 v3, -0x6fc8

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, -0x35

    aput v22, v1, v21

    const/16 v21, 0x3537

    aput v21, v1, v20

    const/16 v20, 0x335

    aput v20, v1, v19

    const/16 v19, 0x3f03

    aput v19, v1, v18

    const/16 v18, 0x143f

    aput v18, v1, v17

    const/16 v17, -0xfec

    aput v17, v1, v16

    const/16 v16, -0x10

    aput v16, v1, v15

    const/16 v15, -0x6c

    aput v15, v1, v14

    const/16 v14, -0x1d

    aput v14, v1, v13

    const/16 v13, -0x41b7

    aput v13, v1, v12

    const/16 v12, -0x42

    aput v12, v1, v11

    const/16 v11, 0x764c

    aput v11, v1, v10

    const/16 v10, 0x2176

    aput v10, v1, v9

    const/16 v9, 0x5421

    aput v9, v1, v8

    const/16 v8, -0x6fac

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v8, v1

    if-lt v3, v8, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v8, v1

    if-lt v3, v8, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    sget v2, Lcom/sec/android/service/health/R$layout;->alert_dialog_row:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_2

    :cond_6
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_7
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_a
    const/4 v3, 0x0

    aget-wide v3, v7, v3

    const-wide/16 v8, 0x0

    cmp-long v6, v3, v8

    if-eqz v6, :cond_b

    const-wide v8, 0x31b82df31aeff910L    # 3.503387335088009E-69

    xor-long/2addr v3, v8

    :cond_b
    const/16 v6, 0x20

    shl-long/2addr v3, v6

    const/16 v6, 0x20

    shr-long/2addr v3, v6

    long-to-int v3, v3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x1

    new-array v5, v4, [I

    const/4 v4, 0x0

    const/16 v6, -0x2597

    aput v6, v5, v4

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v6, 0x0

    const/16 v8, -0x25bc

    aput v8, v4, v6

    const/4 v6, 0x0

    :goto_9
    array-length v8, v4

    if-lt v6, v8, :cond_d

    array-length v4, v5

    new-array v4, v4, [C

    const/4 v6, 0x0

    :goto_a
    array-length v8, v4

    if-lt v6, v8, :cond_e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v6, v3, v4

    const/4 v4, 0x1

    aget-object v8, v3, v4

    :try_start_3
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->uri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$500(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    new-instance v10, Ljava/text/DecimalFormat;

    const/4 v3, 0x5

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v11, 0x2

    const/4 v12, 0x3

    const/4 v13, 0x4

    const/16 v14, -0x3a8b

    aput v14, v4, v13

    const/16 v13, -0x15

    aput v13, v4, v12

    const/16 v12, 0x1d2e

    aput v12, v4, v11

    const/16 v11, 0x493e

    aput v11, v4, v5

    const/16 v5, -0xd96

    aput v5, v4, v3

    const/4 v3, 0x5

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x2

    const/4 v13, 0x3

    const/4 v14, 0x4

    const/16 v15, -0x3aaa

    aput v15, v3, v14

    const/16 v14, -0x3b

    aput v14, v3, v13

    const/16 v13, 0x1d0d

    aput v13, v3, v12

    const/16 v12, 0x491d

    aput v12, v3, v11

    const/16 v11, -0xdb7

    aput v11, v3, v5

    const/4 v5, 0x0

    :goto_b
    array-length v11, v3

    if-lt v5, v11, :cond_f

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_c
    array-length v11, v3

    if-lt v5, v11, :cond_10

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v10, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->d:Ljava/text/DecimalFormat;
    invoke-static {v9, v10}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$602(Lcom/sec/android/service/health/contentservice/HealthContentShow;Ljava/text/DecimalFormat;)Ljava/text/DecimalFormat;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->d:Ljava/text/DecimalFormat;
    invoke-static {v3}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$600(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/text/DecimalFormat;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_d
    :try_start_4
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$500(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    sget v1, Lcom/sec/android/service/health/R$id;->blood_glucose_radio_group_layout:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_c
    :goto_f
    sget v1, Lcom/sec/android/service/health/R$id;->checkBox1:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->checkValue:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->checkValue:Landroid/widget/CheckBox;

    :try_start_6
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->checkValue:Landroid/widget/CheckBox;

    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v7, v1

    long-to-int v1, v1

    if-gtz v1, :cond_14

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, -0x7

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x37

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_d
    aget v8, v4, v6

    aget v9, v5, v6

    xor-int/2addr v8, v9

    aput v8, v5, v6

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_9

    :cond_e
    aget v8, v5, v6

    int-to-char v8, v8

    aput-char v8, v4, v6

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_a

    :cond_f
    aget v11, v3, v5

    aget v12, v4, v5

    xor-int/2addr v11, v12

    aput v11, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_b

    :cond_10
    aget v11, v4, v5

    int-to-char v11, v11

    aput-char v11, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_c

    :cond_11
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_d

    :cond_12
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_13
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_14
    const/4 v1, 0x0

    aget-wide v1, v7, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_15

    const-wide v4, 0x31b82df31aeff910L    # 3.503387335088009E-69

    xor-long/2addr v1, v4

    :cond_15
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->checkValue:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->itemChecked:Ljava/util/ArrayList;

    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v7, v1

    long-to-int v1, v1

    if-gtz v1, :cond_18

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x2e

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x1e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_16

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_16
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_17
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_18
    const/4 v1, 0x0

    aget-wide v1, v7, v1

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_19

    const-wide v5, 0x31b82df31aeff910L    # 3.503387335088009E-69

    xor-long/2addr v1, v5

    :cond_19
    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    shr-long/2addr v1, v5

    long-to-int v1, v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->checkValue:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter$1;-><init>(Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_14
    return-object p2

    :catch_1
    move-exception v1

    goto/16 :goto_3

    :catch_2
    move-exception v1

    goto/16 :goto_4

    :catch_3
    move-exception v1

    goto/16 :goto_d

    :catch_4
    move-exception v1

    goto/16 :goto_e

    :catch_5
    move-exception v1

    goto/16 :goto_f

    :catch_6
    move-exception v1

    goto :goto_14
.end method
