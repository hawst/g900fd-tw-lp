.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;


# static fields
.field public static final CONTENT_PROVIDER_ACCESSIBLE:Ljava/lang/String; = "com.sec.android.service.health.ContentProviderAccessible"

.field private static TAG:Ljava/lang/String;

.field static lockObject:Ljava/lang/Object;

.field public static volatile mIsSamsungTokenRecieved:Z

.field static mSamsungLockObject:Ljava/lang/Object;


# instance fields
.field private isRegistered:Z

.field listener:Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

.field private mAccessToken:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

.field private mErrorGettingSamsungToken:I

.field private mHadlerThread:Landroid/os/HandlerThread;

.field private mPowerManager:Landroid/os/PowerManager;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "HealthSyncAdapter"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mIsSamsungTokenRecieved:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;-><init>(Landroid/content/Context;ZZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;ZZ)V

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mErrorGettingSamsungToken:I

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isRegistered:Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->listener:Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$2;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;)Landroid/content/SyncResult;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSyncResult:Landroid/content/SyncResult;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mErrorGettingSamsungToken:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->unRegisterReceiver(Landroid/content/Context;)V

    return-void
.end method

.method private getIntentForSendingBroadcast(IJILjava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.private.syncadapter.receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "DATA_SYNC_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "Status_bTrueFinished"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "TimeStamp"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v1, "request_type"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, p5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private isCallerHealthService(Ljava/lang/String;)Z
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isCallerHealthService(): Package Name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method private isClearingDBNeeded(II)Z
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mayUnLinkShealthAccount(): request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    const/16 v1, 0x8

    if-ne p2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1869e
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isWifiConnected()Z
    .locals 3

    const/4 v1, 0x1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string v2, "isWifiConnected()"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string v2, "isWifiConnected(): Wifi connected"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string v1, "isWifiConnected(): Wifi not connected"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private managePowerLock(Z)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Wakelock"
        }
    .end annotation

    const/4 v3, 0x1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "managePowerLock(): bAcquire:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mPowerManager:Landroid/os/PowerManager;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mPowerManager:Landroid/os/PowerManager;

    const-string v1, "PARTIAL_POWER"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWifiManager:Landroid/net/wifi/WifiManager;

    const-string v1, "PARTIAL_WIFI"

    invoke-virtual {v0, v3, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string v1, "WAKE LOCK ACQUIRED"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string v1, "WIFI LOCK ACQUIRED"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string v1, "WAKE LOCK RELASED"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string v1, "WIFI LOCK RELASED"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerReceiver(Landroid/content/Context;)V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "registerReceiver(): REGISTERED THE RECEIVER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.service.health.ContentProviderAccessible"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isRegistered:Z

    return-void
.end method

.method private sendCompletedSpecialBroadcast(IILjava/lang/String;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendCompletedSpecialBroadcast(): requestType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". PackageName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isCallerHealthService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "bStatus"

    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "ERROR_TYPE"

    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :goto_1
    :pswitch_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sending Special Broadcast "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_1
    const-string v0, "action_restoration_ongoing_db_replace"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_2
    const-string v0, "action_backup_completed"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_3
    const-string v0, "bDataDeleted"

    if-nez p2, :cond_2

    :goto_2
    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "action_deletion_completed"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1869c
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sendErrorBroadCast(IIIJLjava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendErrorBroadCast(): syncType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". requestType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". ErrorType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". timeStamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". PackageName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.private.syncadapter.receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "DATA_SYNC_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "request_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "Status_bTrueFinished"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "ERROR_TYPE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "TimeStamp"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {v0, p6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private sendStartNotification(ILjava/lang/String;Z)V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendStartNotification(): requestType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". PackageName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". isManualSync:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isCallerHealthService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending Special Broadcast "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void

    :pswitch_1
    const-string v1, "action_backup_started"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_2
    const-string v1, "action_restoration_started"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_3
    const-string v1, "action_deletion_started"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1869c
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private setUpEnvironment()I
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setUpEnvironment()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    sput-boolean v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mIsSamsungTokenRecieved:Z

    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mAccessToken:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mAccessToken:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "1y90e30264"

    const-string v4, "80E7ECD9D301CB7888C73703639302E5"

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->listener:Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    :goto_0
    sget-boolean v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mIsSamsungTokenRecieved:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setUpEnvironment(): Samsung token not received"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "setUpEnvironment(): Inside synchronized"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mIsSamsungTokenRecieved:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "setUpEnvironment(): Inside synchronized. Samsung token received"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_1
    return v0

    :cond_1
    :try_start_1
    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "setUpEnvironment(): Inside synchronized. waiting for samsung token..."

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setUpEnvironment(): Inside synchronized. Inturrupted exception occurred. Error:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    const/4 v0, -0x1

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private unRegisterReceiver(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unRegisterReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isRegistered:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unRegisterReceiver(): UN REGISTERING THE RECEIVER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isRegistered:Z

    :cond_0
    return-void
.end method

.method private validateSyncType(I)Z
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "validateSyncType(): syncType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p1, :sswitch_data_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid Sync type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xb -> :sswitch_0
        0x66 -> :sswitch_0
        0x67 -> :sswitch_0
        0x68 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6c -> :sswitch_0
        0x6d -> :sswitch_0
        0x6e -> :sswitch_0
        0x6f -> :sswitch_0
        0xc8 -> :sswitch_0
        0xc9 -> :sswitch_0
        0x12c -> :sswitch_0
        0x190 -> :sswitch_0
        0x1f5 -> :sswitch_0
        0x1f6 -> :sswitch_0
        0x259 -> :sswitch_0
        0x25a -> :sswitch_0
        0x25b -> :sswitch_0
        0x320 -> :sswitch_0
        0x1869c -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 14

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync(): extras is not null"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, "packageName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync(): returning as it is not called from Shealth"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "Shealth_SDK_Is_Caller"

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    :cond_2
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    if-nez v1, :cond_6

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "SET_WIFI_STATUS"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isWifiConnected()Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync() : WIFI NOT CONNECTED FOR AUTO BACKUP - So returning the control back"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync() : Migration is required, complete the migration process"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onPerformSync() : Registering the BR for db state check"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->registerReceiver(Landroid/content/Context;)V

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onPerformSync() : Waiting for BR to receive for db state check"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    const-wide/16 v4, 0x2710

    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    if-nez v2, :cond_6

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync() : DB is still not accessible so returning"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v3

    :try_start_2
    sget-object v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onPerformSync() : Timeout happened while doing db state check"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->unRegisterReceiver(Landroid/content/Context;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_6
    if-eqz p2, :cond_19

    move-object/from16 v0, p5

    iget-boolean v2, v0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    if-nez v2, :cond_19

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onPerformSync(): extras is not null and sync is not in progress"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSyncResult:Landroid/content/SyncResult;

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->setUpEnvironment()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_7

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync(): user pressed cancel"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "Test-Thread"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mHadlerThread:Landroid/os/HandlerThread;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mHadlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    const-string/jumbo v2, "request_type"

    const v3, 0x1869f

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v2, "data_type"

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v4, "packageName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v4, "TimeStamp"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->initialize()Z

    move-result v4

    if-nez v4, :cond_8

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onPerformSync(): HttpConnector not initialized"

    invoke-static {v1, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x4

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->sendErrorBroadCast(IIIJLjava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->validateSyncType(I)Z

    move-result v4

    if-eqz v4, :cond_17

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getAccessToken()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_17

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "onPerformSync(): its valid synctype and accesstoken is not null"

    invoke-static {v4, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    iget-object v8, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    if-eqz v8, :cond_f

    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "onPerformSync(): workerthread is null"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->setBundle(Landroid/os/Bundle;)V

    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->managePowerLock(Z)V

    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BEFORE STARTING THE SYNC requestType :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " syncType :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " packageName : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v9, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    monitor-enter v9

    :try_start_3
    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "onPerformSync(): inside synchronized"

    invoke-static {v8, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v8, v3, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->startActualSync(II)V

    invoke-direct {p0, v3, v7, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->sendStartNotification(ILjava/lang/String;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "onPerformSync(): inside synchronized. object is going to wait"

    invoke-static {v8, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_2
    :try_start_5
    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "onPerformSync(): object finished waiting"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->managePowerLock(Z)V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->deInitialize()V

    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const-string v9, "com.private.syncadapter.receiver"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v9, "DATA_SYNC_TYPE"

    invoke-virtual {v8, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v9, "Status_bTrueFinished"

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v9, "TimeStamp"

    invoke-virtual {v8, v9, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v5, "request_type"

    invoke-virtual {v8, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v8, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v5

    if-nez v5, :cond_9

    if-eqz v4, :cond_13

    :cond_9
    if-eqz v4, :cond_11

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v6, v9, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_10

    sget-object v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "onPerformSync(): Device storage is low, Sync interrupted"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "ERROR_TYPE"

    const/16 v6, 0x10

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_a
    :goto_3
    const v5, 0x1869c

    if-ne v3, v5, :cond_b

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "ERROR_TYPE"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_b

    const/4 v5, 0x1

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/DatabaseUtility;->clearBackupDatabase(ZLcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    :cond_b
    sget-object v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onPerformSync(): Sending Normal Broadcast to SDK caller "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onPerformSync(): AFTER COMPLETING THE SYNC requestType :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " syncType :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " error code : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v1

    invoke-direct {p0, v3, v1, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->sendCompletedSpecialBroadcast(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->dumpStats()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->reset()V

    :cond_c
    const v1, 0x1869e

    if-ne v3, v1, :cond_e

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v1

    if-nez v1, :cond_d

    if-eqz v4, :cond_16

    :cond_d
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onPerformSync(): Interrupted status: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " and errorcode: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    :goto_4
    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v1

    invoke-direct {p0, v3, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isClearingDBNeeded(II)Z

    move-result v1

    if-eqz v1, :cond_f

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "REQUEST_TYPE"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPerformSync(): Need to delete the shealth account "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    :goto_5
    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync(): Making worker thread as null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    goto/16 :goto_0

    :catch_1
    move-exception v4

    :try_start_6
    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "onPerformSync(): inside synchronized. Got interrupted exception"

    invoke-static {v8, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v4, v8

    goto/16 :goto_2

    :catchall_1
    move-exception v1

    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v1

    :cond_10
    const-string v5, "ERROR_TYPE"

    const/4 v6, -0x1

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onPerformSync(): Interrupted is true and errorcode: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_11
    sget-object v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onPerformSync(): Not interrupted but error occurred: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ". RequestType: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const v5, 0x1869c

    if-ne v3, v5, :cond_12

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_12

    const-string v5, "ERROR_TYPE"

    const/4 v6, 0x0

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_3

    :cond_12
    const-string v5, "ERROR_TYPE"

    iget-object v6, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->errorFoundInExecuting()I

    move-result v6

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_3

    :cond_13
    sget-object v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onPerformSync(): No error and not interrupted. RequestType: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const v5, 0x1869f

    if-ne v3, v5, :cond_15

    const/4 v5, 0x1

    if-ne v2, v5, :cond_15

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "last_backup_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-interface {v5, v6, v9, v10}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_14
    :goto_6
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "Last_Successful_Request_Type"

    invoke-interface {v5, v6, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object/from16 v0, p5

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-virtual {v5}, Landroid/content/SyncStats;->clear()V

    const-string v5, "ERROR_TYPE"

    const/4 v6, 0x0

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const v5, 0x1869f

    if-ne v3, v5, :cond_a

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->isExerciseSyncError()Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "is_exercise_sync_error"

    const/4 v6, 0x1

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3

    :cond_15
    const v5, 0x1869e

    if-ne v3, v5, :cond_14

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "last_restore_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-interface {v5, v6, v9, v10}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_6

    :cond_16
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPerformSync(): REQUEST_TYPE_RESTORE and no error, not interrupted"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "action_restoration_completed"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_4

    :cond_17
    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->validateSyncType(I)Z

    move-result v1

    if-nez v1, :cond_18

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "onPerformSync(): Invalid sync type: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, p0

    move v9, v2

    move-wide v10, v5

    move v12, v3

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getIntentForSendingBroadcast(IJILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ERROR_TYPE"

    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_5

    :cond_18
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_f

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "onPerformSync(): Error getting samsung token: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v8, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mErrorGettingSamsungToken:I

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, p0

    move v9, v2

    move-wide v10, v5

    move v12, v3

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getIntentForSendingBroadcast(IJILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ERROR_TYPE"

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mErrorGettingSamsungToken:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mErrorGettingSamsungToken:I

    goto/16 :goto_5

    :cond_19
    if-eqz p2, :cond_1a

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPerformSync(): Extras "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPerformSync(): Sync Status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    iget-boolean v3, v0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPerformSync(): WIFI CONNECTION STATUS : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->isWifiConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onSyncCanceled()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onSyncCanceled()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onSyncCanceled(): WorkerThread is not null"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onSyncCanceled(): Insied synchronized"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mWorkerThread:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->cancelTask()V

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
