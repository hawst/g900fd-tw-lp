.class Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;
.super Ljava/lang/Object;
.source "ConnectionManager.java"

# interfaces
.implements Ljava/util/concurrent/RejectedExecutionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ImpRejectedExecutionHandler"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public rejectedExecution(Ljava/lang/Runnable;Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/Runnable;
    .param p2, "arg1"    # Ljava/util/concurrent/ThreadPoolExecutor;

    .prologue
    .line 98
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    const-string v1, "Execution was rejected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    return-void
.end method
