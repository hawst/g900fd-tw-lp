.class Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;
.super Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
.source "PostAsyncTask.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 1
    .param p1, "connectionManager"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "requestparameters"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    .line 34
    const-class v0, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;->TAG:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method protected varargs doInBackground2([Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .locals 22
    .param p1, "params"    # [Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 54
    const/4 v7, 0x0

    .line 55
    .local v7, "httpResponse":Lorg/apache/http/HttpResponse;
    const/4 v13, 0x0

    .line 56
    .local v13, "resCode":I
    const/16 v20, 0x0

    aget-object v20, p1, v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getUrl()Ljava/lang/String;

    move-result-object v18

    .line 57
    .local v18, "strURL":Ljava/lang/String;
    const/16 v20, 0x0

    aget-object v20, p1, v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getHttpEntity()Lorg/apache/http/HttpEntity;

    move-result-object v12

    .line 58
    .local v12, "requestEntity":Lorg/apache/http/HttpEntity;
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 59
    .local v14, "response":Ljava/lang/StringBuffer;
    const/16 v20, 0x0

    aget-object v20, p1, v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getSetHeadervalues()Ljava/util/HashMap;

    move-result-object v16

    .line 63
    .local v16, "setHeaderValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v20, v0

    if-nez v20, :cond_0

    .line 65
    new-instance v15, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v20, -0x1

    const/16 v21, -0x7

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v15, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 66
    .local v15, "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/16 v20, 0x0

    aget-object v20, p1, v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 67
    const/16 v20, 0x0

    aget-object v20, p1, v20

    .line 175
    .end local v15    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :goto_0
    return-object v20

    .line 72
    :cond_0
    :try_start_0
    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 103
    .local v8, "httppost":Lorg/apache/http/client/methods/HttpPost;
    if-eqz v16, :cond_3

    .line 105
    const/16 v19, 0x0

    .line 106
    .local v19, "value":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 107
    .local v11, "key":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 108
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "key":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 109
    .restart local v11    # "key":Ljava/lang/String;
    if-eqz v11, :cond_1

    .line 111
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "value":[Ljava/lang/String;
    check-cast v19, [Ljava/lang/String;

    .line 112
    .restart local v19    # "value":[Ljava/lang/String;
    if-eqz v19, :cond_2

    .line 114
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_2
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v10, v0, :cond_1

    .line 115
    aget-object v20, v19, v10

    move-object/from16 v0, v20

    invoke-virtual {v8, v11, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 119
    .end local v10    # "k":I
    :cond_2
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v8, v11, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 170
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "key":Ljava/lang/String;
    .end local v19    # "value":[Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 172
    .local v5, "ex":Ljava/lang/Exception;
    invoke-static {v13, v5}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v15

    .line 174
    .end local v5    # "ex":Ljava/lang/Exception;
    :goto_3
    const/16 v20, 0x0

    aget-object v20, p1, v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 175
    const/16 v20, 0x0

    aget-object v20, p1, v20

    goto :goto_0

    .line 125
    .restart local v8    # "httppost":Lorg/apache/http/client/methods/HttpPost;
    :cond_3
    if-eqz v12, :cond_4

    .line 127
    :try_start_1
    invoke-virtual {v8, v12}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 129
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v20

    if-eqz v20, :cond_5

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;->getHttpContext()Lorg/apache/http/protocol/BasicHttpContext;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v8, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    .line 133
    :cond_5
    if-nez v7, :cond_6

    .line 135
    new-instance v15, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v20, -0x1

    const/16 v21, -0x7

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v15, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 136
    .restart local v15    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/16 v20, 0x0

    aget-object v20, p1, v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 137
    const/16 v20, 0x0

    aget-object v20, p1, v20

    goto/16 :goto_0

    .line 139
    .end local v15    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_6
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v20

    const/16 v21, 0xc8

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_9

    .line 141
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    .line 142
    .local v6, "httpEntity":Lorg/apache/http/HttpEntity;
    if-eqz v6, :cond_8

    .line 144
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v20, Ljava/io/InputStreamReader;

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v20

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 145
    .local v2, "bufferedReader":Ljava/io/BufferedReader;
    :goto_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v17

    .local v17, "strLine":Ljava/lang/String;
    if-eqz v17, :cond_7

    .line 147
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 149
    :cond_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 151
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v17    # "strLine":Ljava/lang/String;
    :cond_8
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 153
    .local v4, "errorString":Ljava/lang/String;
    new-instance v15, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v20

    const/16 v21, -0x6

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v15, v0, v1, v4}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;)V

    .line 154
    .restart local v15    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    goto/16 :goto_3

    .line 157
    .end local v4    # "errorString":Ljava/lang/String;
    .end local v6    # "httpEntity":Lorg/apache/http/HttpEntity;
    .end local v15    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_9
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    .line 158
    .restart local v6    # "httpEntity":Lorg/apache/http/HttpEntity;
    if-eqz v6, :cond_b

    .line 160
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v20, Ljava/io/InputStreamReader;

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v20

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 161
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    :goto_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v17

    .restart local v17    # "strLine":Ljava/lang/String;
    if-eqz v17, :cond_a

    .line 163
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 165
    :cond_a
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 167
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v17    # "strLine":Ljava/lang/String;
    :cond_b
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v15

    .local v15, "result":Ljava/lang/String;
    goto/16 :goto_3
.end method
