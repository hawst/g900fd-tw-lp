.class public Lcom/sec/android/service/health/cp/common/DBTables$MEMO_TABLE;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MEMO_TABLE"
.end annotation


# static fields
.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_FILEPATH:Ljava/lang/String; = "filepath"

.field public static final KEY_MEASURE_ID:Ljava/lang/String; = "measure_id"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_TYPE_ID:Ljava/lang/String; = "type_id"

.field public static final TABLE_NAME:Ljava/lang/String; = "memo"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_memo_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_memo_trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
