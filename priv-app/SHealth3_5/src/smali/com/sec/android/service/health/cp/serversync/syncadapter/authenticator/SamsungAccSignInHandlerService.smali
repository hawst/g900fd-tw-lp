.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;
.super Landroid/app/IntentService;


# static fields
.field public static ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String; = null

.field public static final HEALTH_ACCOUNT_SHAREDPREF:Ljava/lang/String; = "health_account"

.field public static final SAMSUNG_ACCOUNT_KEY:Ljava/lang/String; = "SamAccount"

.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "SamsungAccSignInHandlerService"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    const-string v0, "com.osp.app.signin"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private isNewAccount(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    const-string v2, "accountName is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "health_account"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v2, "SamAccount"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    const-string v3, "No previous Account is found"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "SamAccount"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    const-string v2, "New User"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SamAccount"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    const-string v2, "No health_account preferences is found, Please check your preferences."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onHandleIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->isNewAccount(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "new Samsung Acc user is signed in => make all record\'s sync status as Added/False"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/DatabaseUtility;->updatePHRSyncStatusToAdded()V

    invoke-static {}, Lcom/sec/android/service/health/cp/DatabaseUtility;->updateProfileSyncStatusToFalse()V

    invoke-static {}, Lcom/sec/android/service/health/cp/DatabaseUtility;->updateProfileImageSyncStatusToAdded()V

    :cond_0
    return-void
.end method
