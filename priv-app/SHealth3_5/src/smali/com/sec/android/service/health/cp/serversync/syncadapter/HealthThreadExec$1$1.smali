.class Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;->this$1:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->setAccessToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;->this$1:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;->this$1:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestType:I
    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;->this$1:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestedDataTypebyApp:I
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->getMaintenanceNotice(II)V

    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;->this$1:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    const/16 v1, 0xb

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    return-void
.end method

.method public setNetworkFailure()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;->this$1:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    const/4 v1, 0x4

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    return-void
.end method
