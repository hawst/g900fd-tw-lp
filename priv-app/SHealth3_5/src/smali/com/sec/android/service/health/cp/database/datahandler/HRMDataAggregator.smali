.class public Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field static final ACTIVITY_MONITOR_TABLE:Ljava/lang/String; = "activity_monitor"

.field public static final AGGREGATION_TIME_INTERVAL:I = 0x2710

.field static final REALTIME_DATA_TABLE:Ljava/lang/String; = "realtime_data"

.field static final SESSION_ID_KEY:Ljava/lang/String; = "session_id"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field lastCreateTime:J

.field private mAggregationCount:I

.field private mAppId:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field protected mCurrentRealTimeIdUri:Landroid/net/Uri;

.field protected mCurrentTenSec:J

.field protected mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field protected mExerciseId:J

.field private mHeartRate:F

.field private mLastHeartBeatTime:J

.field private mMaxHeartRate:F

.field private mMeanHeartRateActivity:F

.field private mMeanHeartRateRealTime:F

.field private mRealTimeCount:I

.field protected mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->lastCreateTime:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMaxHeartRate:F

    return-void
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string v4, " addData "

    invoke-static {v1, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->checkMandatoryFields()Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    :goto_0
    return v1

    :cond_0
    :try_start_1
    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mCurrentTenSec:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/16 v6, 0x2710

    div-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mCurrentTenSec:J

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/16 v6, 0x2710

    div-long/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mCurrentTenSec:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->processData()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v2

    goto :goto_0

    :cond_2
    :try_start_3
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string v4, " Adding new Realtime Data "

    invoke-static {v1, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v4, :cond_3

    const-string v4, "application__id"

    iget-object v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getCurrentActivityUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->getActivityUpdate()Landroid/content/ContentValues;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->resetAggregation()V

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/16 v6, 0x2710

    div-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mCurrentTenSec:J
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_4
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Insert Data Contraint Failed: value : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->getContentValues()Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v3

    goto/16 :goto_0

    :catch_1
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v3

    goto/16 :goto_0

    :catch_2
    move-exception v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Insert Data Unknown Exception log : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v3

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "addData[] not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method protected checkMandatoryFields()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data will be skipped. mandatory fields empty"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sensor issue - not all mandatory fields are assigned"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalizeAggregation()V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->getActivityUpdate()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "end_time"

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getCurrentActivityUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->getContentValues()Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Data is out of range"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method protected getActivityUpdate()Landroid/content/ContentValues;
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "max_heart_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMaxHeartRate:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "mean_heart_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMeanHeartRateActivity:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    return-object v0
.end method

.method protected getContentValues()Landroid/content/ContentValues;
    .locals 6

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sample_time"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mCurrentTenSec:J

    const-wide/16 v4, 0x2710

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mExerciseId:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "data_type"

    const v2, 0x493e2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "heart_rate_per_min"

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMeanHeartRateRealTime:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "last_heart_beat_time"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mLastHeartBeatTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mCurrentTenSec:J

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mUserDeviceId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mExerciseId:J

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->resetInitialAggregation()V

    return-void
.end method

.method public isRunning(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mExerciseId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mExerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mExerciseId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "matched"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected processData()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    if-eqz v1, :cond_1

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mRealTimeCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mRealTimeCount:I

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAggregationCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAggregationCount:I

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mHeartRate:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mHeartRate:F

    iget v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mRealTimeCount:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mRealTimeCount:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mHeartRate:F

    const-string v1, "HRM_DATA"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mHeartRate:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMaxHeartRate:F

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMaxHeartRate:F

    :goto_0
    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMaxHeartRate:F

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMeanHeartRateRealTime:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mRealTimeCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mRealTimeCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMeanHeartRateRealTime:F

    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMeanHeartRateActivity:F

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAggregationCount:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAggregationCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMeanHeartRateActivity:F

    :cond_0
    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mLastHeartBeatTime:J

    :cond_1
    return-void

    :cond_2
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    int-to-float v1, v1

    goto :goto_0
.end method

.method protected resetAggregation()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mRealTimeCount:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mHeartRate:F

    return-void
.end method

.method protected resetInitialAggregation()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->resetAggregation()V

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMaxHeartRate:F

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mMeanHeartRateActivity:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mAggregationCount:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;->mLastHeartBeatTime:J

    return-void
.end method
