.class public Lcom/sec/android/service/health/keyManager/KeyOperation;
.super Ljava/lang/Object;


# static fields
.field private static final AES_KEY_LENGTH:I = 0x100

.field private static KEY_ALGORITHM:Ljava/lang/String; = null

.field private static final PBE_ITERATION_COUNT:I = 0x3e8

.field private static final PKCS5_SALT_LENGTH:I = 0x80

.field private static SALT_FILE:Ljava/lang/String;

.field private static SECRET_KEY_ALGORITHM:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 29

    const/4 v0, 0x4

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, -0x5

    aput v5, v1, v4

    const/16 v4, -0x33

    aput v4, v1, v3

    const/16 v3, 0x367b

    aput v3, v1, v2

    const/16 v2, -0x2d9b

    aput v2, v1, v0

    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/16 v6, -0x51

    aput v6, v0, v5

    const/16 v5, -0x7f

    aput v5, v0, v4

    const/16 v4, 0x363a

    aput v4, v0, v3

    const/16 v3, -0x2dca

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/keyManager/KeyOperation;->SALT_FILE:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, -0x44

    aput v19, v1, v18

    const/16 v18, -0x26ee

    aput v18, v1, v17

    const/16 v17, -0x6f

    aput v17, v1, v16

    const/16 v16, -0x4b

    aput v16, v1, v15

    const/16 v15, -0x6e

    aput v15, v1, v14

    const/16 v14, -0x6e

    aput v14, v1, v13

    const/16 v13, -0x2a

    aput v13, v1, v12

    const/16 v12, -0x40c6

    aput v12, v1, v11

    const/16 v11, -0x29

    aput v11, v1, v10

    const/16 v10, -0x27

    aput v10, v1, v9

    const/16 v9, -0x70

    aput v9, v1, v8

    const/16 v8, -0x6f8a

    aput v8, v1, v7

    const/16 v7, -0x5e

    aput v7, v1, v6

    const/16 v6, 0x6703

    aput v6, v1, v5

    const/16 v5, 0xe23

    aput v5, v1, v4

    const/16 v4, 0x7d45

    aput v4, v1, v3

    const/16 v3, 0x2f3f

    aput v3, v1, v2

    const/16 v2, -0x1281

    aput v2, v1, v0

    const/16 v0, 0x12

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, -0x73

    aput v20, v0, v19

    const/16 v19, -0x26ad

    aput v19, v0, v18

    const/16 v18, -0x27

    aput v18, v0, v17

    const/16 v17, -0x1a

    aput v17, v0, v16

    const/16 v16, -0xf

    aput v16, v0, v15

    const/16 v15, -0xd

    aput v15, v0, v14

    const/16 v14, -0x45

    aput v14, v0, v13

    const/16 v13, -0x408e

    aput v13, v0, v12

    const/16 v12, -0x41

    aput v12, v0, v11

    const/16 v11, -0x53

    aput v11, v0, v10

    const/4 v10, -0x7

    aput v10, v0, v9

    const/16 v9, -0x6fdf

    aput v9, v0, v8

    const/16 v8, -0x70

    aput v8, v0, v7

    const/16 v7, 0x6745

    aput v7, v0, v6

    const/16 v6, 0xe67

    aput v6, v0, v5

    const/16 v5, 0x7d0e

    aput v5, v0, v4

    const/16 v4, 0x2f7d

    aput v4, v0, v3

    const/16 v3, -0x12d1

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v3, v0

    if-lt v2, v3, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v3, v0

    if-lt v2, v3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/keyManager/KeyOperation;->SECRET_KEY_ALGORITHM:Ljava/lang/String;

    const/4 v0, 0x3

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/16 v4, -0x4e

    aput v4, v1, v3

    const/16 v3, 0x2a66

    aput v3, v1, v2

    const/16 v2, 0x7c6b

    aput v2, v1, v0

    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/16 v5, -0x1f

    aput v5, v0, v4

    const/16 v4, 0x2a23

    aput v4, v0, v3

    const/16 v3, 0x7c2a

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v3, v0

    if-lt v2, v3, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v3, v0

    if-lt v2, v3, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/keyManager/KeyOperation;->KEY_ALGORITHM:Ljava/lang/String;

    const/16 v0, 0x1a

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x5575

    aput v27, v1, v26

    const/16 v26, 0x263a

    aput v26, v1, v25

    const/16 v25, -0x11b1

    aput v25, v1, v24

    const/16 v24, -0x66

    aput v24, v1, v23

    const/16 v23, -0x78d8

    aput v23, v1, v22

    const/16 v22, -0xb

    aput v22, v1, v21

    const/16 v21, -0x7f

    aput v21, v1, v20

    const/16 v20, -0x2a9b

    aput v20, v1, v19

    const/16 v19, -0x66

    aput v19, v1, v18

    const/16 v18, -0x37

    aput v18, v1, v17

    const/16 v17, 0x6804

    aput v17, v1, v16

    const/16 v16, 0x6d23

    aput v16, v1, v15

    const/16 v15, -0x2ebd

    aput v15, v1, v14

    const/16 v14, -0x4c

    aput v14, v1, v13

    const/16 v13, -0x9

    aput v13, v1, v12

    const/16 v12, -0x6c

    aput v12, v1, v11

    const/16 v11, -0x48

    aput v11, v1, v10

    const/16 v10, -0x589f

    aput v10, v1, v9

    const/16 v9, -0x3e

    aput v9, v1, v8

    const/16 v8, -0x54

    aput v8, v1, v7

    const/16 v7, 0x481f

    aput v7, v1, v6

    const/16 v6, 0x403c

    aput v6, v1, v5

    const/16 v5, -0x2cd4

    aput v5, v1, v4

    const/16 v4, -0x4e

    aput v4, v1, v3

    const/16 v3, -0x52

    aput v3, v1, v2

    const/4 v2, -0x2

    aput v2, v1, v0

    const/16 v0, 0x1a

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x551b

    aput v28, v0, v27

    const/16 v27, 0x2655

    aput v27, v0, v26

    const/16 v26, -0x11da

    aput v26, v0, v25

    const/16 v25, -0x12

    aput v25, v0, v24

    const/16 v24, -0x78b7

    aput v24, v0, v23

    const/16 v23, -0x79

    aput v23, v0, v22

    const/16 v22, -0x1c

    aput v22, v0, v21

    const/16 v21, -0x2aeb

    aput v21, v0, v20

    const/16 v20, -0x2b

    aput v20, v0, v19

    const/16 v19, -0x50

    aput v19, v0, v18

    const/16 v18, 0x6861

    aput v18, v0, v17

    const/16 v17, 0x6d68

    aput v17, v0, v16

    const/16 v16, -0x2e93

    aput v16, v0, v15

    const/16 v15, -0x2f

    aput v15, v0, v14

    const/16 v14, -0x6c

    aput v14, v0, v13

    const/4 v13, -0x3

    aput v13, v0, v12

    const/16 v12, -0x32

    aput v12, v0, v11

    const/16 v11, -0x58ed

    aput v11, v0, v10

    const/16 v10, -0x59

    aput v10, v0, v9

    const/4 v9, -0x1

    aput v9, v0, v8

    const/16 v8, 0x4877

    aput v8, v0, v7

    const/16 v7, 0x4048

    aput v7, v0, v6

    const/16 v6, -0x2cc0

    aput v6, v0, v5

    const/16 v5, -0x2d

    aput v5, v0, v4

    const/16 v4, -0x35

    aput v4, v0, v3

    const/16 v3, -0x4a

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_6
    array-length v3, v0

    if-lt v2, v3, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_7
    array-length v3, v0

    if-lt v2, v3, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/keyManager/KeyOperation;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_5
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_6
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_7
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static generateRandom(I)[B
    .locals 14

    const/4 v0, 0x1

    const/4 v4, 0x0

    new-array v5, v0, [I

    aput p0, v5, v4

    const/4 v0, 0x0

    const/16 v1, 0x8

    :try_start_0
    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x5ea8

    aput v12, v2, v11

    const/16 v11, -0x11

    aput v11, v2, v10

    const/16 v10, -0x5d

    aput v10, v2, v9

    const/16 v9, -0x70

    aput v9, v2, v8

    const/16 v8, 0x6426

    aput v8, v2, v7

    const/16 v7, -0x23db

    aput v7, v2, v6

    const/16 v6, -0x6c

    aput v6, v2, v3

    const/16 v3, -0x4bf8

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x5ee1

    aput v13, v1, v12

    const/16 v12, -0x5f

    aput v12, v1, v11

    const/16 v11, -0xf

    aput v11, v1, v10

    const/16 v10, -0x40

    aput v10, v1, v9

    const/16 v9, 0x6417

    aput v9, v1, v8

    const/16 v8, -0x239c

    aput v8, v1, v7

    const/16 v7, -0x24

    aput v7, v1, v6

    const/16 v6, -0x4ba5

    aput v6, v1, v3

    move v3, v4

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    move v3, v4

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_2
    aget v1, v5, v4

    new-array v1, v1, [B

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    return-object v1

    :cond_0
    :try_start_1
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_2
.end method

.method public static getCipherKey(Landroid/content/Context;Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;
        }
    .end annotation

    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/sec/android/service/health/keyManager/KeyOperation;->getSalt(Landroid/content/Context;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v3, Ljavax/crypto/spec/PBEKeySpec;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    const/16 v2, 0x3e8

    const/16 v4, 0x100

    invoke-direct {v3, v1, v0, v2, v4}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    const/16 v0, 0x12

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, -0x7c

    aput v20, v1, v19

    const/16 v19, -0x6b

    aput v19, v1, v18

    const/16 v18, 0xd2e

    aput v18, v1, v17

    const/16 v17, 0x395e

    aput v17, v1, v16

    const/16 v16, 0x455a

    aput v16, v1, v15

    const/16 v15, -0x13dc

    aput v15, v1, v14

    const/16 v14, -0x7f

    aput v14, v1, v13

    const/16 v13, -0x3fa0

    aput v13, v1, v12

    const/16 v12, -0x58

    aput v12, v1, v11

    const/16 v11, -0x5ea1

    aput v11, v1, v10

    const/16 v10, -0x38

    aput v10, v1, v9

    const/16 v9, -0x1cd2

    aput v9, v1, v8

    const/16 v8, -0x2f

    aput v8, v1, v7

    const/16 v7, -0x27

    aput v7, v1, v6

    const/16 v6, 0x205e

    aput v6, v1, v5

    const/16 v5, -0x95

    aput v5, v1, v4

    const/16 v4, -0x43

    aput v4, v1, v2

    const/16 v2, -0x72b2

    aput v2, v1, v0

    const/16 v0, 0x12

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, -0x4b

    aput v21, v0, v20

    const/16 v20, -0x2c

    aput v20, v0, v19

    const/16 v19, 0xd66

    aput v19, v0, v18

    const/16 v18, 0x390d

    aput v18, v0, v17

    const/16 v17, 0x4539

    aput v17, v0, v16

    const/16 v16, -0x13bb

    aput v16, v0, v15

    const/16 v15, -0x14

    aput v15, v0, v14

    const/16 v14, -0x3fd8

    aput v14, v0, v13

    const/16 v13, -0x40

    aput v13, v0, v12

    const/16 v12, -0x5ed5

    aput v12, v0, v11

    const/16 v11, -0x5f

    aput v11, v0, v10

    const/16 v10, -0x1c87

    aput v10, v0, v9

    const/16 v9, -0x1d

    aput v9, v0, v8

    const/16 v8, -0x61

    aput v8, v0, v7

    const/16 v7, 0x201a

    aput v7, v0, v6

    const/16 v6, -0xe0

    aput v6, v0, v5

    const/4 v5, -0x1

    aput v5, v0, v4

    const/16 v4, -0x72e2

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v0

    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    const/4 v0, 0x3

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/16 v6, -0x6f

    aput v6, v1, v5

    const/16 v5, 0x2b26

    aput v5, v1, v2

    const/16 v2, 0x7c6a

    aput v2, v1, v0

    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, -0x3e

    aput v7, v0, v6

    const/16 v6, 0x2b63

    aput v6, v0, v5

    const/16 v5, 0x7c2b

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    move-object v0, v3

    :goto_4
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_4

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private static getSalt(Landroid/content/Context;)[B
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v12, 0x0

    const/4 v11, 0x1

    const-wide v9, 0x4d6ce037221b6e3aL    # 9.503065680050243E64

    const/16 v8, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v4, v11

    invoke-static {p0}, Lcom/sec/android/service/health/keyManager/KeyOperation;->getSaltFromFile(Landroid/content/Context;)[B

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/16 v0, 0x80

    invoke-static {v0}, Lcom/sec/android/service/health/keyManager/KeyOperation;->generateRandom(I)[B

    move-result-object v2

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, v3

    shl-long/2addr v0, v8

    ushr-long v5, v0, v8

    aget-wide v0, v4, v3

    cmp-long v7, v0, v12

    if-eqz v7, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v5

    xor-long/2addr v0, v9

    aput-wide v0, v4, v3

    :goto_1
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v11, [I

    const/16 v0, -0x6c0

    aput v0, v1, v3

    new-array v0, v11, [I

    const/16 v2, -0x690

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    aget-wide v0, v4, v3

    cmp-long v5, v0, v12

    if-eqz v5, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_b

    invoke-static {p0, v2}, Lcom/sec/android/service/health/keyManager/KeyOperation;->saveSaltToFile(Landroid/content/Context;[B)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v2

    goto/16 :goto_0

    :cond_6
    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_4
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_7

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_4

    :cond_7
    aget-wide v0, v4, v3

    cmp-long v5, v0, v12

    if-eqz v5, :cond_8

    xor-long/2addr v0, v9

    :cond_8
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v5, v4, v1

    long-to-int v1, v5

    if-gtz v1, :cond_9

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    int-to-long v0, v0

    shl-long/2addr v0, v8

    ushr-long v5, v0, v8

    aget-wide v0, v4, v3

    cmp-long v7, v0, v12

    if-eqz v7, :cond_a

    xor-long/2addr v0, v9

    :cond_a
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v5

    xor-long/2addr v0, v9

    aput-wide v0, v4, v3

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x4

    new-array v1, v0, [I

    const/4 v0, 0x2

    const/4 v2, 0x3

    const/16 v4, -0x7f

    aput v4, v1, v2

    const/16 v2, 0x6840

    aput v2, v1, v0

    const/16 v0, 0x2229

    aput v0, v1, v11

    const/16 v0, -0x418f

    aput v0, v1, v3

    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v2, 0x2

    const/4 v4, 0x3

    const/16 v5, -0x2b

    aput v5, v0, v4

    const/16 v4, 0x680c

    aput v4, v0, v2

    const/16 v2, 0x2268

    aput v2, v0, v11

    const/16 v2, -0x41de

    aput v2, v0, v3

    move v2, v3

    :goto_5
    array-length v4, v0

    if-lt v2, v4, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    :goto_6
    array-length v2, v0

    if-lt v3, v2, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_c
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_d
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6
.end method

.method private static getSaltFromFile(Landroid/content/Context;)[B
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/16 v10, 0x20

    const/4 v9, 0x1

    const/4 v3, 0x0

    new-array v4, v5, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v4, v9

    const/4 v0, 0x4

    new-array v1, v0, [I

    const/16 v0, -0x30

    aput v0, v1, v6

    const/16 v0, 0x3461

    aput v0, v1, v5

    const/16 v0, 0x7e75

    aput v0, v1, v9

    const/16 v0, 0x172d

    aput v0, v1, v3

    const/4 v0, 0x4

    new-array v0, v0, [I

    const/16 v2, -0x7c

    aput v2, v0, v6

    const/16 v2, 0x342d

    aput v2, v0, v5

    const/16 v2, 0x7e34

    aput v2, v0, v9

    const/16 v2, 0x177e

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_2

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    int-to-long v0, v0

    shl-long/2addr v0, v10

    ushr-long v5, v0, v10

    aget-wide v0, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v2, v0, v7

    if-eqz v2, :cond_3

    const-wide v7, -0x7011eee958bd84deL    # -6.052013190281352E-232

    xor-long/2addr v0, v7

    :cond_3
    ushr-long/2addr v0, v10

    shl-long/2addr v0, v10

    xor-long/2addr v0, v5

    const-wide v5, -0x7011eee958bd84deL    # -6.052013190281352E-232

    xor-long/2addr v0, v5

    aput-wide v0, v4, v3

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_6

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v9, [I

    const/16 v0, -0x1c

    aput v0, v1, v3

    new-array v0, v9, [I

    const/16 v2, -0x2c

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    aget-wide v0, v4, v3

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_7

    const-wide v4, -0x7011eee958bd84deL    # -6.052013190281352E-232

    xor-long/2addr v0, v4

    :cond_7
    shl-long/2addr v0, v10

    shr-long/2addr v0, v10

    long-to-int v0, v0

    new-array v0, v0, [B

    :try_start_0
    new-instance v5, Ljava/io/BufferedInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/16 v9, -0x87

    aput v9, v2, v8

    const/16 v8, -0x4d

    aput v8, v2, v7

    const/16 v7, -0x65b1

    aput v7, v2, v4

    const/16 v4, -0x37

    aput v4, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/16 v10, -0xd3

    aput v10, v1, v9

    const/4 v9, -0x1

    aput v9, v1, v8

    const/16 v8, -0x65f2

    aput v8, v1, v7

    const/16 v7, -0x66

    aput v7, v1, v4

    move v4, v3

    :goto_4
    array-length v7, v1

    if-lt v4, v7, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v6, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v5, v0}, Ljava/io/BufferedInputStream;->read([B)I

    :try_start_1
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    :goto_6
    return-object v0

    :cond_8
    :try_start_2
    aget v7, v1, v4

    aget v8, v2, v4

    xor-int/2addr v7, v8

    aput v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_9
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_6
.end method

.method private static saveSaltToFile(Landroid/content/Context;[B)Z
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    new-instance v5, Ljava/io/BufferedOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    const/4 v2, 0x4

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/16 v9, 0x5200

    aput v9, v3, v8

    const/16 v8, 0x6d1e

    aput v8, v3, v7

    const/16 v7, -0x12d4

    aput v7, v3, v4

    const/16 v4, -0x42

    aput v4, v3, v2

    const/4 v2, 0x4

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/16 v10, 0x5254

    aput v10, v2, v9

    const/16 v9, 0x6d52

    aput v9, v2, v8

    const/16 v8, -0x1293

    aput v8, v2, v7

    const/16 v7, -0x13

    aput v7, v2, v4

    move v4, v1

    :goto_0
    array-length v7, v2

    if-lt v4, v7, :cond_0

    array-length v2, v3

    new-array v2, v2, [C

    move v4, v1

    :goto_1
    array-length v7, v2

    if-lt v4, v7, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v5, p1}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    return v0

    :cond_0
    :try_start_4
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move v0, v1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v1

    goto :goto_2

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_3
.end method
