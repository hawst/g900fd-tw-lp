.class Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->requestAccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewAvailableDeviceList(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;)V
    .locals 5

    const/4 v1, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$13;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusWatchDownloaderPcc$DeviceListUpdateCode:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$900(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mFisrtConnect:Z
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1000(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "[HealthSensor]WATCHSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewAvailableDeviceList "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " return"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]WATCHSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onNewAvailableDeviceList "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    array-length v0, p2

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->deviceInfoArray:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    invoke-static {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1102(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;)[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->bDevicesInList:Z
    invoke-static {v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1202(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;Z)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    array-length v2, p2

    new-array v2, v2, [Ljava/lang/String;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1302(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    array-length v2, p2

    new-array v2, v2, [I

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsDeviceType:[I
    invoke-static {v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1402(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[I)[I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    array-length v2, p2

    new-array v2, v2, [I

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsManufacturerId:[I
    invoke-static {v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1502(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[I)[I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    array-length v2, p2

    new-array v2, v2, [Ljava/lang/String;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->UUIDtoString:[Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1602(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[Ljava/lang/String;)[Ljava/lang/String;

    move v0, v1

    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1300(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[Ljava/lang/String;

    move-result-object v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsDeviceType:[I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1400(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[I

    move-result-object v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getAntfsDeviceType()I

    move-result v3

    aput v3, v2, v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsManufacturerId:[I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1500(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[I

    move-result-object v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getAntfsManufacturerId()I

    move-result v3

    aput v3, v2, v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->UUIDtoString:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1600(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[Ljava/lang/String;

    move-result-object v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1700(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mFisrtConnect:Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1800(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->getAntDeviceType()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1300(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-static {v2, v1, v3}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->convertANTname(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1900(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->setConnectState(I)Z

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyChannelStateChanged()V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->stopTimer()V
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$2000(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->watchnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->watchnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1300(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsDeviceType:[I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1400(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsManufacturerId:[I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1500(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->UUIDtoString:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1600(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;->notifySearchData([Ljava/lang/String;[I[I[Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->bDevicesInList:Z
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$1202(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;Z)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
