.class Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MessageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "handleMessage SEND_DATA, appFileDiscriptor is null, break"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    if-nez v3, :cond_2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "handleMessage SEND_DATA, FileDiscriptor null == null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_3
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage SEND_DATA, IOException e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_4

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_4
    :goto_3
    throw v0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :sswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage CREATE_CHANNEL_DONE status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "Status failure not propagated to app"

    invoke-static {v0, v1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->createChannel()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->destroyChannel()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_2
    .end sparse-switch
.end method
