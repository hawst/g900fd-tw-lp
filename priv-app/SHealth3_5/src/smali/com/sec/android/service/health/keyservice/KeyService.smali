.class public Lcom/sec/android/service/health/keyservice/KeyService;
.super Landroid/app/Service;
.source "KeyService.java"


# static fields
.field private static DB_Key:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static DEBUG:Z = false

.field public static final GET:I = 0x1

.field public static final INTENT_ACTION:Ljava/lang/String; = "com.sec.android.service.health.keyservice.aidl.IKeyService"

.field public static final REMOVE:I = 0x3

.field public static final RESET:I = 0x2

.field public static final SET:I = 0x0

.field private static final ServiceName:Ljava/lang/String; = "KeyService, KeyManager"

.field private static final TAG:Ljava/lang/String;

.field private static final keyStub:Lcom/sec/android/service/health/keyservice/aidl/IKeyService$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;

    .line 18
    const-class v0, Lcom/sec/android/service/health/keyservice/KeyService;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    .line 26
    new-instance v0, Lcom/sec/android/service/health/keyservice/KeyService$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/keyservice/KeyService$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->keyStub:Lcom/sec/android/service/health/keyservice/aidl/IKeyService$Stub;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 14
    sget-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    return v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    sget-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    const-string v1, "KeyService, KeyManager.onBind is called."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.service.health.keyservice.aidl.IKeyService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    sget-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyService, KeyManager action is equals. : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_1
    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->keyStub:Lcom/sec/android/service/health/keyservice/aidl/IKeyService$Stub;

    .line 122
    :goto_0
    return-object v0

    .line 121
    :cond_2
    sget-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    const-string v1, "KeyService, KeyManager action is not equals."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 71
    sget-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    const-string v1, "KeyService, KeyManager.onCreate is called."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 74
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 78
    sget-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    const-string v1, "KeyService, KeyManager.onDestroy is called."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 82
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v8, 0x1

    .line 87
    :try_start_0
    const-string/jumbo v4, "setForeground"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 88
    .local v3, "setForeground":Z
    sget-boolean v4, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KeyService, KeyManager.onStartCommand is called. setForeground:  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    if-eqz v3, :cond_3

    .line 90
    new-instance v2, Landroid/app/Notification;

    sget v4, Lcom/sec/android/service/health/keyservice/R$drawable;->icon_foreground:I

    const-string v5, "S Health KeyService"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 91
    .local v2, "notification":Landroid/app/Notification;
    invoke-virtual {p0}, Lcom/sec/android/service/health/keyservice/KeyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "S Health KeyService"

    const-string v6, "S Health KeyService"

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 92
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/service/health/keyservice/KeyService;->startForeground(ILandroid/app/Notification;)V

    .line 97
    .end local v2    # "notification":Landroid/app/Notification;
    :goto_0
    const-string/jumbo v4, "resetServiceKey"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 98
    .local v1, "isReset":Z
    sget-boolean v4, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KeyService, KeyManager.onStartCommand is called. isReset: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_1
    if-eqz v1, :cond_2

    .line 100
    sget-object v4, Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;

    if-nez v4, :cond_4

    .line 101
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;

    .line 110
    .end local v1    # "isReset":Z
    .end local v3    # "setForeground":Z
    :cond_2
    :goto_1
    return v8

    .line 94
    .restart local v3    # "setForeground":Z
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/service/health/keyservice/KeyService;->stopForeground(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    .end local v3    # "setForeground":Z
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 103
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "isReset":Z
    .restart local v3    # "setForeground":Z
    :cond_4
    :try_start_1
    sget-object v4, Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 127
    sget-boolean v0, Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;

    const-string v1, "KeyService, KeyManager.unBind is called."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
