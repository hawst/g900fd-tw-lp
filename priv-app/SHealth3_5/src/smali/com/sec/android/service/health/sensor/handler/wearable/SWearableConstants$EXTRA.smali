.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$EXTRA;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EXTRA"
.end annotation


# static fields
.field public static final ACTION_NAME:Ljava/lang/String; = "EXTRA_ACTION_NAME"

.field public static final COACHING_ENERGY:Ljava/lang/String; = "EXTRA_COACHING_ENERGY"

.field public static final COACHING_PROFILE:Ljava/lang/String; = "EXTRA_COACHING_PROFILE"

.field public static final COACHING_RESPONSE:Ljava/lang/String; = "EXTRA_COACHING_RESPONSE"

.field public static final COACHING_RESULT:Ljava/lang/String; = "EXTRA_COACHING_RESULT"

.field public static final COACHING_RUN:Ljava/lang/String; = "EXTRA_COACHING_RUN"

.field public static final COACHING_USER:Ljava/lang/String; = "EXTRA_COACHING_USER"

.field public static final COACHING_VAR:Ljava/lang/String; = "EXTRA_COACHING_VAR"

.field public static final DISTANCE_UNIT:Ljava/lang/String; = "DISTANCE_UNIT"

.field public static final ERROR_CAUSE:Ljava/lang/String; = "ERROR_CAUSE"

.field public static final ERROR_CODE:Ljava/lang/String; = "EXTRA_ERROR_CODE"

.field public static final EXERCISE_RESULT:Ljava/lang/String; = "EXTRA_EXERCISE_RESULT"

.field public static final HEART_MONITOR:Ljava/lang/String; = "EXTRA_HEART_MONITOR"

.field public static final PEDOMETER:Ljava/lang/String; = "EXTRA_PEDOMETER"

.field public static final PEDOMETER_GOAL:Ljava/lang/String; = "EXTRA_PEDOMETER_GOAL"

.field public static final PEDOMETER_GOAL_HISTORY:Ljava/lang/String; = "EXTRA_PEDOMETER_GOAL_HISTORY"

.field public static final RESET:Ljava/lang/String; = "EXTRA_RESET"

.field public static final SLEEP:Ljava/lang/String; = "EXTRA_SLEEP"

.field public static final START_TIME:Ljava/lang/String; = "EXTRA_START_TIME"

.field public static final STRESS:Ljava/lang/String; = "EXTRA_STRESS"

.field public static final USER_PROFILE:Ljava/lang/String; = "EXTRA_USER_PROFILE"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
