.class Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;
.super Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    invoke-direct {p0}, Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageDeleteFailed(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "packageDeleteFailed() : deletion failed !!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v0, v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v1, v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void
.end method

.method public packageDeleted(Ljava/lang/String;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "packageDeleted() : deletion succeeded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v0, v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v1, v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "packageDeleted() : deletaion failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public packageInstallFailed(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "packageInstallFailed() : install failed !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v0, v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v1, v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void
.end method

.method public packageInstalled(Ljava/lang/String;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "packageInstalled() : Installation has been succeeded."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v0, v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    iget-object v1, v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Installation has been failed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
