.class Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;
.super Ljava/util/ArrayDeque;
.source "SamsungAccountUserTokenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArrayUniqueQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/ArrayDeque",
        "<TE;>;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 84
    .local p0, "this":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;, "Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue<TE;>;"
    invoke-direct {p0}, Ljava/util/ArrayDeque;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    .prologue
    .line 84
    .local p0, "this":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;, "Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue<TE;>;"
    invoke-direct {p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;-><init>()V

    return-void
.end method


# virtual methods
.method public offer(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;, "Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue<TE;>;"
    .local p1, "e":Ljava/lang/Object;, "TE;"
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 93
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Ljava/util/ArrayDeque;->offer(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
