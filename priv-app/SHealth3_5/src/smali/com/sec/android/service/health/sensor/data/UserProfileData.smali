.class public Lcom/sec/android/service/health/sensor/data/UserProfileData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/service/health/sensor/data/UserProfileData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityLevel:I

.field private mAge:I

.field private mGender:I

.field private mHeight:I

.field private mLifeAthlete:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/data/UserProfileData$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/data/UserProfileData$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/data/UserProfileData;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/service/health/sensor/data/UserProfileData$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/data/UserProfileData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getActivityLevel()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mActivityLevel:I

    return v0
.end method

.method public getAge()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mAge:I

    return v0
.end method

.method public getGender()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
    .locals 1

    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mGender:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mHeight:I

    return v0
.end method

.method public getLifeAthlete()Z
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mLifeAthlete:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mAge:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mHeight:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mActivityLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mLifeAthlete:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mGender:I

    return-void
.end method

.method public setActivityLevel(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mActivityLevel:I

    return-void
.end method

.method public setAge(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mAge:I

    return-void
.end method

.method public setGender(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;)V
    .locals 1

    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mGender:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mGender:I

    goto :goto_0
.end method

.method public setHeight(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mHeight:I

    return-void
.end method

.method public setLifeAthlete(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mLifeAthlete:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mLifeAthlete:I

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mAge:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mActivityLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mLifeAthlete:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/UserProfileData;->mGender:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
