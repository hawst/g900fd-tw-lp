.class public Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$4;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]BPWRSensor"

.field private static final default_notSupport:Ljava/lang/String; = "N/A"


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

.field private wheelCircumferenceInMeters:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "2.07"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->wheelCircumferenceInMeters:Ljava/math/BigDecimal;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Ljava/math/BigDecimal;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->wheelCircumferenceInMeters:Ljava/math/BigDecimal;

    return-object v0
.end method

.method private requestCrankParameters()Z
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestCrankParameters(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "[HealthSensor]BPWRSensor"

    const-string v2, "Request Could not be Made"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method private requestCustomCalibrationParameters([B)Z
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, p1, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "[HealthSensor]BPWRSensor"

    const-string v2, "Request Could not be Made"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method private requestManualCalibration()Z
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestManualCalibration(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "[HealthSensor]BPWRSensor"

    const-string v2, "Request Could not be Made"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method private setAutoZero(Z)Z
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, p1, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetAutoZero(ZLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "[HealthSensor]BPWRSensor"

    const-string v2, "Request Could not be Made"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method private setCrankParameters(ZLjava/math/BigDecimal;)Z
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    if-eqz p1, :cond_1

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->AUTO_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetCrankParameters(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    const-string v1, "[HealthSensor]BPWRSensor"

    const-string v2, "Request Could not be Made"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0

    :cond_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->MANUAL_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, v1, p2, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetCrankParameters(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    goto :goto_0
.end method

.method private setCtfSlope(Ljava/math/BigDecimal;)Z
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, p1, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetCtfSlope(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "[HealthSensor]BPWRSensor"

    const-string v2, "Request Could not be Made"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method private setCustomCalibrationParameters([B)Z
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    invoke-virtual {v0, p1, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "[HealthSensor]BPWRSensor"

    const-string v2, "Request Could not be Made"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method


# virtual methods
.method public getAntDeviceType()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 1

    const/16 v0, 0x7d2

    return v0
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 6

    const/4 v0, 0x0

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CMD_REQ_CRANK"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestCrankParameters()Z

    move-result v1

    const-string v3, "RES_REQ_CRANK"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    :goto_0
    if-eqz v1, :cond_7

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v1, "Request Successfully Sent"

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    :goto_1
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CMD_SET_CRANK"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PARAM_CRANK_AUTO"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    new-instance v3, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "PARAM_CRANK_LEN"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    float-to-double v4, v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-direct {p0, v1, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->setCrankParameters(ZLjava/math/BigDecimal;)Z

    move-result v1

    const-string v3, "RES_SET_CRANK"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CMD_SET_AUTO"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PARAM_AUTO_ZERO"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->setAutoZero(Z)Z

    move-result v1

    const-string v3, "RES_SET_AUTO"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CMD_SET_CTF"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "PARAM_CTF_SLOPE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v3

    float-to-double v3, v3

    invoke-direct {v1, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->setCtfSlope(Ljava/math/BigDecimal;)Z

    move-result v1

    const-string v3, "RES_SET_CTF"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CMD_REQ_MANUAL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestManualCalibration()Z

    move-result v1

    const-string v3, "RES_REQ_MANUAL"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CMD_REQ_CUSTOM"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PARAM_CUSTOM_CAL"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestCustomCalibrationParameters([B)Z

    move-result v1

    const-string v3, "RES_REQ_CUSTOM"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CMD_SET_CUSTOM"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PARAM_CUSTOM_CAL"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->setCustomCalibrationParameters([B)Z

    move-result v1

    const-string v3, "RES_SET_CUSTOM"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v0, "Request Failed to be Sent"

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    const/16 v0, 0x3e8

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto/16 :goto_0
.end method

.method protected requestAccess()V
    .locals 5

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->getANTbikeWheelsize()F

    move-result v0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/math/BigDecimal;

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->getANTbikeWheelsize()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->wheelCircumferenceInMeters:Ljava/math/BigDecimal;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntDeviceID:I

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method
