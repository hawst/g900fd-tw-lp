.class public Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field private static final ONE_HOUR_IN_SEC:I = 0xe10

.field private static final ONE_MINUTE_IN_MILLISEC:I = 0xea60

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

.field private mExerciseId:J

.field private mMaxSpeed:F

.field mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

.field private mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    return-void
.end method

.method private checkMandatoryFieldsLocation(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sensor issue - not all mandatory fields are assigned"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "NullPointerException at checkMandatoryFieldsLocation."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getLocationContentValues(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;JLandroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 4

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v0, "sample_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {v1, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v0, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "exercise__id"

    invoke-static {v1, v0, p2, p3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v0, "latitude"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    invoke-static {v1, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v0, "longitude"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    invoke-static {v1, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v0, "altitude"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->altitude:D

    invoke-static {v1, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v0, "accuracy"

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v0, "sample_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {p4, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v0, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-static {p4, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-static {p4, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v0, "data_type"

    const v2, 0x493e1

    invoke-static {p4, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v0, "speed_per_hour"

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    const/high16 v3, 0x45610000    # 3600.0f

    mul-float/2addr v2, v3

    invoke-static {p4, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;F)V

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mMaxSpeed:F

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    :goto_0
    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mMaxSpeed:F

    return-object v1

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mMaxSpeed:F

    goto :goto_0
.end method

.method private insertCoachingResultData(J)V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertCoachingResultData with exercise ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->distance:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "exercise__id"

    invoke-static {v0, v1, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "maximal_met"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteMaxMET:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "resource_recovery"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteResourceRecovery:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "training_load_peak"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteTrainingLoadPeak:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "end_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v1, "validation_policy"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->getPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->asMask()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private insertExerciseActivityData(J)V
    .locals 7

    const/high16 v6, 0x45610000    # 3600.0f

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertExerciseActivityData with exercise ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise__id"

    invoke-static {v0, v1, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "calorie"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string/jumbo v1, "mean_heart_rate_per_min"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string/jumbo v1, "max_heart_rate_per_min"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxHeartRate:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "duration_min"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "duration_millisecond"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string/jumbo v1, "start_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "end_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "max_speed_per_hour"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxSpeed:F

    mul-float/2addr v2, v6

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "mean_speed_per_hour"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->averageSpeed:F

    mul-float/2addr v2, v6

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_altitude"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxAltitude:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "min_altitude"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->minAltitude:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v1, :cond_1

    const-string v1, "incline_distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "decline_distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string/jumbo v1, "validation_policy"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->getPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->asMask()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private insertExerciseData()V
    .locals 6

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "insertExerciseData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise_info__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "start_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "end_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "heart_rate"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v1, "input_source_type"

    const v2, 0x3f7a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    const/16 v2, 0x4651

    if-ne v1, v2, :cond_3

    const-string v1, "exercise_type"

    const/16 v2, 0x4e23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    const-string/jumbo v1, "total_calorie"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "duration_min"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "duration_millisecond"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v1, "validation_policy"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->getPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->asMask()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Invalid data, Insertion failed rowId is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    return-void

    :cond_3
    const-string v1, "exercise_type"

    const/16 v2, 0x4e21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->insertIntoExerciseDeviceTable()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->insertLocationData([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;J)V

    :cond_5
    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->insertExerciseActivityData(J)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->insertCoachingResultData(J)V

    goto :goto_1
.end method

.method private insertIntoExerciseDeviceTable()V
    .locals 9

    const/4 v6, 0x0

    const-string v3, "exercise__id = ? and user_device__id = ? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mUserDeviceId:Ljava/lang/String;

    aput-object v1, v4, v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertIntoExerciseDeviceTable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v7, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "exercise__id"

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v2, "user_device__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertIntoExerciseDeviceTable : insert : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "application__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v2, "validation_policy"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->getPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->asMask()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private insertLocationData([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;J)V
    .locals 9

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertLocationData with exercise ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v1, p1

    new-array v3, v1, [Landroid/content/ContentValues;

    new-array v4, v1, [Landroid/content/ContentValues;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    array-length v5, p1

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v6, p1, v2

    invoke-direct {p0, v6}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->checkMandatoryFieldsLocation(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-direct {p0, v6, p2, p3, v0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->getLocationContentValues(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;JLandroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v7

    aput-object v7, v3, v1

    aput-object v0, v4, v1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v0, :cond_1

    aget-object v0, v3, v1

    const-string v7, "application__id"

    iget-object v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    aget-object v0, v4, v1

    const-string v7, "application__id"

    iget-object v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mMaxSpeed:F

    iget v7, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    cmpg-float v0, v0, v7

    if-gez v0, :cond_3

    iget v0, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    :goto_2
    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mMaxSpeed:F

    iget-object v0, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v0, :cond_2

    iget-object v0, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    :cond_2
    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mMaxSpeed:F

    goto :goto_2

    :cond_4
    if-lez v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    :cond_5
    return-void
.end method

.method private processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 2

    if-eqz p1, :cond_0

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    :goto_0
    return-void

    :cond_0
    const-string v0, "ExerciseDataAggregator"

    const-string v1, "Nothing to process"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->insertExerciseData()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while inserting into db"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 4

    const/4 v1, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "addData[] called"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    array-length v2, p1

    if-eq v0, v2, :cond_2

    :goto_1
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method protected checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public finalizeAggregation()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Nothing to be done in finalization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mUserDeviceId:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mAppId:Ljava/lang/String;

    return-void
.end method

.method public isRunning(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mExerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;->mExerciseId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "matched"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
