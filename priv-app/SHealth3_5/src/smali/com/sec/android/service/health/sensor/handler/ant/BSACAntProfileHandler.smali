.class public Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$4;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]BSACSensor"


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private manufacture:I

.field private wheelsize:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->manufacture:I

    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "2.095"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->wheelsize:Ljava/math/BigDecimal;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntPluginSubPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$1002(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->manufacture:I

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->manufacture:I

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->requestBikeSpeed(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntPluginSubPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Ljava/math/BigDecimal;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->wheelsize:Ljava/math/BigDecimal;

    return-object v0
.end method

.method private requestBikeCadence(Z)V
    .locals 6

    const/4 v2, 0x0

    if-nez p1, :cond_0

    const/4 v3, 0x1

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntDeviceID:I

    new-instance v4, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    invoke-direct {v4, p0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;Z)V

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc;->requestAccess(Landroid/content/Context;IIZLcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void

    :cond_0
    move v3, v2

    goto :goto_0
.end method

.method private requestBikeSpeed(Z)V
    .locals 6

    const/4 v2, 0x0

    if-nez p1, :cond_0

    const/4 v3, 0x1

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mAntDeviceID:I

    new-instance v4, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$2;

    invoke-direct {v4, p0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;Z)V

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->requestAccess(Landroid/content/Context;IIZLcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void

    :cond_0
    move v3, v2

    goto :goto_0
.end method


# virtual methods
.method public getAntDeviceType()I
    .locals 1

    const/16 v0, 0x79

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 1

    const/16 v0, 0x7d2

    return v0
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected requestAccess()V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->getANTbikeWheelsize()F

    move-result v0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/math/BigDecimal;

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->getANTbikeWheelsize()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->wheelsize:Ljava/math/BigDecimal;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSubType()I

    move-result v0

    const/16 v1, 0x7a

    if-ne v0, v1, :cond_1

    const-string v0, "[HealthSensor]BSACSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "requestAccess - DEVICE_TYPE_BIKE_CADENCE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSubType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->requestBikeCadence(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSubType()I

    move-result v0

    const/16 v1, 0x7b

    if-ne v0, v1, :cond_2

    const-string v0, "[HealthSensor]BSACSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "requestAccess - DEVICE_TYPE_BIKE_SPEED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSubType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->requestBikeSpeed(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSubType()I

    move-result v0

    const/16 v1, 0x79

    if-ne v0, v1, :cond_3

    const-string v0, "[HealthSensor]BSACSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "requestAccess - DEVICE_TYPE_BIKE_SPEED_CADENCE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSubType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->requestBikeCadence(Z)V

    goto :goto_0

    :cond_3
    const-string v0, "[HealthSensor]BSACSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "requestAccess - DEVICE_TYPE_BIKE_SPEED_CADENCE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSubType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->notifyOnJoinedError(I)V

    goto :goto_0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method
