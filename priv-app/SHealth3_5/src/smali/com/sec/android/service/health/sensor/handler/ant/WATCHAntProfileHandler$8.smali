.class Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->requestDownloadActivities(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

.field final synthetic val$requestAll:Z


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;Z)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iput-boolean p2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;->val$requestAll:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewFitFileDownloaded(Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V
    .locals 3

    const-string v0, "[HealthSensor]WATCHSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewFitFileDownloaded "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;->val$requestAll:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->getRawBytes()[B

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->checkFlagfile()Z
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$2100(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->FitFileWrite([B)V
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$2200(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[B)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->readFitFromData([B)V
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->access$2300(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[B)V

    :cond_1
    return-void
.end method
