.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;
.super Landroid/app/IntentService;


# static fields
.field private static final EXTRA_DEVICETYPE:Ljava/lang/String; = "EXTRA_DEVICETYPE"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mIntent:Landroid/content/Intent;

.field private mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private waitObj:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "GearSensorService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$3;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->waitObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$3;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->waitObj:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;Landroid/content/Intent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->isDeviceAvailable(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    return-object v0
.end method

.method private isDeviceAvailable(Landroid/content/Intent;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.shealth.ACTION_WINGTIP_SYNC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v3, 0x2723

    invoke-direct {v2, v3, v1, v5}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " bReturn = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v3, 0x2726

    invoke-direct {v2, v3, v1, v5}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v4, 0x2728

    invoke-direct {v3, v4, v1, v5}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v3

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.shealth.ACTION_GEAR_SYNC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v2, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v3, 0x272e

    invoke-direct {v2, v3, v1, v5}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v4, 0x2730

    invoke-direct {v3, v4, v1, v5}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v3

    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_6
    if-eqz v3, :cond_7

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_7
    move v0, v1

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.shealth.ACTION_SBAND_SYNC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance v2, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v3, 0x2727

    invoke-direct {v2, v3, v1, v5}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[+] deinitialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "mDevice.leave()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "mDeviceFinder.close()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->waitObj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->waitObj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[-] deinitialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method protected findDevice(Ljava/util/List;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;)",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ignore the device "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "No valid device "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :sswitch_0
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2723 -> :sswitch_0
        0x2726 -> :sswitch_0
        0x2728 -> :sswitch_0
        0x272e -> :sswitch_0
        0x2730 -> :sswitch_0
    .end sparse-switch
.end method

.method protected findDevice(Ljava/util/List;I)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;I)",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    if-ne v2, p2, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findDevice(X,X) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No valid device. Device Type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDestroy() : Thread Name "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[+] onHandleIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "intent is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "intent Action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->waitObj:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v2, "[+] waitObj.wait(}"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->waitObj:Ljava/lang/Object;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v2, "[-] waitObj.wait(}"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[-] onHandleIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto :goto_1
.end method
