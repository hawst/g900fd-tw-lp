.class public interface abstract Lcom/sec/android/service/health/sensor/manager/ISensorListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "[",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onDataRecordStarted(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZ)V
.end method

.method public abstract onDataStarted(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onDataStopped(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
.end method

.method public abstract onJoined(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onLeft(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Ljava/lang/Integer;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;",
            "Ljava/lang/Integer;",
            "I)V"
        }
    .end annotation
.end method

.method public abstract onScanningStarted(I)V
.end method

.method public abstract onScanningStopped(I)V
.end method

.method public abstract onScanningStopped(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onStateChanged(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;)V"
        }
    .end annotation
.end method
