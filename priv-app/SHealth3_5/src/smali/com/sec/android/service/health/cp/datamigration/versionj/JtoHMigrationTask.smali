.class public Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;
.super Lcom/sec/android/service/health/cp/BaseInitializationTask;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    return-void
.end method

.method private migrate2xLocalDBToH()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->TAG:Ljava/lang/String;

    const-string v1, "[+] migrate2xLocalDBToH"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;-><init>(Landroid/content/Context;)V

    :try_start_0
    new-instance v1, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v1}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    iget-object v1, v1, Lcom/sec/android/service/health/cp/common/AESEncryption;->str:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Getting AES password failed."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "H DB open failed with AES pass"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    :try_start_1
    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->TAG:Ljava/lang/String;

    const-string v1, "[-] migrate2xLocalDBToH"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method protected finishInitialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->TAG:Ljava/lang/String;

    const-string v1, "We should not notify the lock."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->TAG:Ljava/lang/String;

    const-string v1, "Notify mLock"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method

.method protected handelCommand(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v3, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->publishProgress([Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->migrate2xLocalDBToH()V

    new-array v0, v3, [Ljava/lang/Integer;

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->publishProgress([Ljava/lang/Object;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected initialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;->TAG:Ljava/lang/String;

    const-string v1, "No need to do initialize since this is only partial migration upto H."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
