.class public Lcom/sec/android/service/health/cp/database/validator/ValidationManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkForInsert(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->getInstance()Lcom/sec/android/service/health/cp/database/validator/DataParser;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->INSERT:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkValidationAndCorrection(Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Table \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" does not registered in the data_spec.xml or DataParser.initialize() is not called. Please implement this."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager;->printLog(Ljava/lang/String;)V

    throw v0
.end method

.method public static checkForReplace(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->getInstance()Lcom/sec/android/service/health/cp/database/validator/DataParser;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->REPLACE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkValidationAndCorrection(Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Table \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" does not registered in the data_spec.xml or DataParser.initialize() is not called. Please implement this."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager;->printLog(Ljava/lang/String;)V

    throw v0
.end method

.method public static checkForUpdate(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->getInstance()Lcom/sec/android/service/health/cp/database/validator/DataParser;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->UPDATE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    invoke-virtual {v0, p1, p2, v1, p5}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkValidationAndCorrection(Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Table \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" does not registered in the data_spec.xml or DataParser.initialize() is not called. Please implement this."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager;->printLog(Ljava/lang/String;)V

    throw v0
.end method

.method private static printLog(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VALIDATION_CHECK : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
