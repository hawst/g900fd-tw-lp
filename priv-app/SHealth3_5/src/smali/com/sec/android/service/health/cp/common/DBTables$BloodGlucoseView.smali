.class public Lcom/sec/android/service/health/cp/common/DBTables$BloodGlucoseView;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodGlucoseView"
.end annotation


# static fields
.field public static final KEY_AVG:Ljava/lang/String; = "avg"

.field public static final KEY_COUNT:Ljava/lang/String; = "count"

.field public static final KEY_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_MAX:Ljava/lang/String; = "max"

.field public static final KEY_TYPE:Ljava/lang/String; = "type"

.field public static final VIEW_NAME:Ljava/lang/String; = "glucose_view"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
