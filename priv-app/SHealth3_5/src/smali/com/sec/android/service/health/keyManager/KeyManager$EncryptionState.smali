.class Lcom/sec/android/service/health/keyManager/KeyManager$EncryptionState;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/keyManager/KeyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EncryptionState"
.end annotation


# static fields
.field static final AES_KEY:I = 0xa

.field static final DEVICE_ID_WITH_PLAIN_SALT:I = 0x5

.field static final DEVICE_ID_WITH_RAND_SALT:I = 0x0

.field static final ESN_WITH_PLAIN_SALT:I = 0x8

.field static final ESN_WITH_RAND_SALT:I = 0x3

.field static final IMEI_WITH_PLAIN_SALT:I = 0x6

.field static final IMEI_WITH_RAND_SALT:I = 0x1

.field static final MEID_WITH_PLAIN_SALT:I = 0x7

.field static final MEID_WITH_RAND_SALT:I = 0x2

.field static final NOT_DEFINED:I = -0x1

.field static final NO_ID_WITH_PLAIN_SALT:I = 0x9

.field static final NO_ID_WITH_RAND_SALT:I = 0x4

.field static final USER_PASSWORD:I = 0xb


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/keyManager/KeyManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/keyManager/KeyManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/keyManager/KeyManager$EncryptionState;->this$0:Lcom/sec/android/service/health/keyManager/KeyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
