.class abstract Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
.super Landroid/os/AsyncTask;
.source "AbstractASyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;",
        ">;"
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field protected connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private context:Lorg/apache/http/protocol/BasicHttpContext;

.field protected httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

.field private requestparamameters:Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;


# direct methods
.method protected constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 2
    .param p1, "connectionmanager"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "requestparamameters"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    const-class v0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->TAG:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    .line 38
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 43
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 44
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->requestparamameters:Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .line 69
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 70
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getHttpClientManager()Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 71
    iput-object p2, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->requestparamameters:Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .line 72
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    .line 73
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->requestparamameters:Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->doInBackground2([Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->doInBackground([Ljava/lang/Void;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    move-result-object v0

    return-object v0
.end method

.method protected varargs abstract doInBackground2([Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
.end method

.method protected getHttpContext()Lorg/apache/http/protocol/BasicHttpContext;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    return-object v0
.end method

.method protected getRequestParamameters()Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->requestparamameters:Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    return-object v0
.end method

.method protected onCancelled(Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 6
    .param p1, "result"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 166
    if-nez p1, :cond_1

    .line 168
    const-string v0, "AbstractASyncTask"

    const-string/jumbo v1, "onCancelled Request was cancelled "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->taskCompleted(J)V

    .line 172
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponsehandler()Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponsehandler()Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getPrivateId()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getTag()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->requestparamameters:Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;->onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->onCancelled(Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 8
    .param p1, "result"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 120
    if-nez p1, :cond_1

    .line 122
    const-string v0, "AbstractASyncTask"

    const-string/jumbo v1, "onPostExecute Stopped Response Processing: "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    const-string v0, "AbstractASyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onPostExecute Starting Response Handler : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->taskCompleted(J)V

    .line 127
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponsehandler()Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponse()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    if-eqz v0, :cond_2

    .line 133
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponsehandler()Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getPrivateId()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getTag()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->requestparamameters:Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v7

    .line 137
    .local v7, "ex":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 145
    .end local v7    # "ex":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponsehandler()Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getPrivateId()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getResponse()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;->onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 147
    :catch_1
    move-exception v7

    .line 149
    .restart local v7    # "ex":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->onPostExecute(Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    return-void
.end method

.method processGetResponse(Ljava/lang/String;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 21
    .param p1, "URL"    # Ljava/lang/String;
    .param p2, "httpResponse"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-interface/range {p2 .. p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v12

    .line 225
    .local v12, "httpEntity":Lorg/apache/http/HttpEntity;
    const/4 v15, -0x1

    .line 227
    .local v15, "responseCode":I
    if-nez v12, :cond_0

    .line 229
    new-instance v17, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v18, -0x1e

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v0, v15, v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    throw v17

    .line 231
    :cond_0
    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v10

    .line 232
    .local v10, "header":Lorg/apache/http/Header;
    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v17

    move-wide/from16 v0, v17

    long-to-int v7, v0

    .line 233
    .local v7, "contentLength":I
    if-nez v10, :cond_1

    .line 235
    new-instance v17, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v18, -0x1e

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v0, v15, v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    throw v17

    .line 237
    :cond_1
    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v11

    .line 238
    .local v11, "headerValue":Ljava/lang/String;
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v17

    if-lez v17, :cond_2

    if-nez v7, :cond_3

    .line 240
    :cond_2
    new-instance v17, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v18, -0x1e

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v0, v15, v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    throw v17

    .line 242
    :cond_3
    invoke-interface/range {p2 .. p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v15

    .line 243
    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v17

    const-string v18, "application/json;charset=utf-8"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v17

    const-string/jumbo v18, "text/plain"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 248
    :cond_4
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v17, Ljava/io/InputStreamReader;

    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 249
    .local v5, "bufferedReader":Ljava/io/BufferedReader;
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 250
    .local v14, "response":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v16

    .local v16, "strLine":Ljava/lang/String;
    if-eqz v16, :cond_5

    .line 252
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 257
    .end local v5    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v14    # "response":Ljava/lang/StringBuffer;
    .end local v16    # "strLine":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 259
    .local v9, "ex":Ljava/lang/Exception;
    invoke-static {v15, v9}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v8

    .line 283
    .end local v9    # "ex":Ljava/lang/Exception;
    :goto_1
    return-object v8

    .line 254
    .restart local v5    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v14    # "response":Ljava/lang/StringBuffer;
    .restart local v16    # "strLine":Ljava/lang/String;
    :cond_5
    :try_start_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 255
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_1

    .line 266
    .end local v5    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v14    # "response":Ljava/lang/StringBuffer;
    .end local v16    # "strLine":Ljava/lang/String;
    :cond_6
    :try_start_2
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getBufferSize()I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v4, v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 267
    .local v4, "bufferedInputStream":Ljava/io/BufferedInputStream;
    new-array v8, v7, [B

    .line 268
    .local v8, "data":[B
    const/4 v13, 0x0

    .line 269
    .local v13, "offset":I
    :goto_2
    if-ge v13, v7, :cond_7

    .line 271
    array-length v0, v8

    move/from16 v17, v0

    sub-int v17, v17, v13

    move/from16 v0, v17

    invoke-virtual {v4, v8, v13, v0}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v6

    .line 272
    .local v6, "bytesRead":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v6, v0, :cond_8

    .line 278
    .end local v6    # "bytesRead":I
    :cond_7
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 281
    .end local v4    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v8    # "data":[B
    .end local v13    # "offset":I
    :catch_1
    move-exception v9

    .line 283
    .restart local v9    # "ex":Ljava/lang/Exception;
    invoke-static {v15, v9}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v8

    goto :goto_1

    .line 276
    .end local v9    # "ex":Ljava/lang/Exception;
    .restart local v4    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v6    # "bytesRead":I
    .restart local v8    # "data":[B
    .restart local v13    # "offset":I
    :cond_8
    add-int/2addr v13, v6

    .line 277
    goto :goto_2
.end method
