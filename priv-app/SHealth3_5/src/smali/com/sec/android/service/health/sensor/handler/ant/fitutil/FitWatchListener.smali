.class public Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWatchListener;
.super Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;

# interfaces
.implements Lcom/garmin/fit/ActivityMesgListener;
.implements Lcom/garmin/fit/EventMesgListener;
.implements Lcom/garmin/fit/HrvMesgListener;
.implements Lcom/garmin/fit/LapMesgListener;
.implements Lcom/garmin/fit/LengthMesgListener;
.implements Lcom/garmin/fit/RecordMesgListener;
.implements Lcom/garmin/fit/SessionMesgListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private convertSport(Lcom/garmin/fit/Sport;)I
    .locals 3

    const/16 v0, 0x4268

    invoke-virtual {p1}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v1

    sget-object v2, Lcom/garmin/fit/Sport;->BASKETBALL:Lcom/garmin/fit/Sport;

    invoke-virtual {v2}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v2

    if-ne v1, v2, :cond_1

    const/16 v0, 0x2efd

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v1

    sget-object v2, Lcom/garmin/fit/Sport;->CYCLING:Lcom/garmin/fit/Sport;

    invoke-virtual {v2}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v2

    if-ne v1, v2, :cond_2

    const/16 v0, 0x36b1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v1

    sget-object v2, Lcom/garmin/fit/Sport;->RUNNING:Lcom/garmin/fit/Sport;

    invoke-virtual {v2}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v2

    if-ne v1, v2, :cond_3

    const/16 v0, 0x2af9

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v1

    sget-object v2, Lcom/garmin/fit/Sport;->SOCCER:Lcom/garmin/fit/Sport;

    invoke-virtual {v2}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v2

    if-ne v1, v2, :cond_4

    const/16 v0, 0x2f03

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v1

    sget-object v2, Lcom/garmin/fit/Sport;->SWIMMING:Lcom/garmin/fit/Sport;

    invoke-virtual {v2}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v2

    if-ne v1, v2, :cond_5

    const/16 v0, 0x32ca

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v1

    sget-object v2, Lcom/garmin/fit/Sport;->TENNIS:Lcom/garmin/fit/Sport;

    invoke-virtual {v2}, Lcom/garmin/fit/Sport;->getValue()S

    move-result v2

    if-ne v1, v2, :cond_0

    const/16 v0, 0x2f05

    goto :goto_0
.end method


# virtual methods
.method public onMesg(Lcom/garmin/fit/ActivityMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/EventMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/HrvMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/LapMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/LengthMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/RecordMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/SessionMesg;)V
    .locals 5

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;-><init>()V

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_0
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getSport()Lcom/garmin/fit/Sport;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getSport()Lcom/garmin/fit/Sport;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWatchListener;->convertSport(Lcom/garmin/fit/Sport;)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->equipmentType:I

    :cond_1
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getSubSport()Lcom/garmin/fit/SubSport;

    move-result-object v1

    if-eqz v1, :cond_2

    :cond_2
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getStartTime()Lcom/garmin/fit/DateTime;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getStartTime()Lcom/garmin/fit/DateTime;

    move-result-object v1

    invoke-virtual {v1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    const-wide v3, 0x92ee70e000L

    add-long/2addr v1, v3

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    :cond_3
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getTotalCalories()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getTotalCalories()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeCalories:F

    const v1, 0x33451

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeCaloriesUnit:I

    :cond_4
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getTotalDistance()Ljava/lang/Float;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getTotalDistance()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistance:F

    const v1, 0x29811

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cumulativeDistanceUnit:I

    :cond_5
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getTotalElapsedTime()Ljava/lang/Float;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getTotalElapsedTime()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->longValue()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->duration:J

    :cond_6
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getAvgSpeed()Ljava/lang/Float;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getAvgSpeed()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanSpeed:F

    :cond_7
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getMaxSpeed()Ljava/lang/Float;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getMaxSpeed()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxSpeed:F

    :cond_8
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getAvgHeartRate()Ljava/lang/Short;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getAvgHeartRate()Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Short;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanHeartRate:F

    :cond_9
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getMaxHeartRate()Ljava/lang/Short;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getMaxHeartRate()Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Short;->intValue()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxHeartRate:I

    :cond_a
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getAvgCadence()Ljava/lang/Short;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getAvgCadence()Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Short;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->meanCadence:F

    :cond_b
    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getMaxCadence()Ljava/lang/Short;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {p1}, Lcom/garmin/fit/SessionMesg;->getMaxCadence()Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Short;->longValue()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->maxCadence:J

    :cond_c
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWatchListener;->fitnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;->notifyMeasuredData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method
