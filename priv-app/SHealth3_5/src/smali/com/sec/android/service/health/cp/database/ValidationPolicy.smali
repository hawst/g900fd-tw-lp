.class public final Lcom/sec/android/service/health/cp/database/ValidationPolicy;
.super Ljava/lang/Object;


# instance fields
.field private doCorretion:Z

.field private doLogging:Z

.field private doThrowingException:Z


# direct methods
.method private constructor <init>(ZZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doLogging:Z

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doCorretion:Z

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doThrowingException:Z

    iput-boolean p1, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doLogging:Z

    iput-boolean p2, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doCorretion:Z

    iput-boolean p3, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doThrowingException:Z

    return-void
.end method

.method public static fromMask(I)Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    packed-switch p0, :pswitch_data_0

    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v1, v1, v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v2, v1, v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v1, v2, v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v2, v2, v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v1, v1, v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v2, v1, v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getGear2FitPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    return-object v0
.end method

.method public static getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1, v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    return-object v0
.end method

.method public static getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    return-object v0
.end method

.method public static getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    return-object v0
.end method

.method public static getUIPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1, v2}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;-><init>(ZZZ)V

    return-object v0
.end method


# virtual methods
.method public asMask()I
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doLogging:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doCorretion:Z

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doThrowingException:Z

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    return v0
.end method

.method public isDoCorretion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doCorretion:Z

    return v0
.end method

.method public isDoLogging()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doLogging:Z

    return v0
.end method

.method public isDoThrowingException()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->doThrowingException:Z

    return v0
.end method
