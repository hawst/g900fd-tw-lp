.class Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/ScanningManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScanThread"
.end annotation


# instance fields
.field managerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/manager/ScanningManager;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->managerList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method addAbstractManager(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    iget v0, v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mDataType:I

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDataTypeSupported(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->managerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method isThreadScannable()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->managerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->managerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 11

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->managerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    iget v0, v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mDuration:I

    :goto_0
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    if-ne v8, v6, :cond_2

    move v1, v2

    move v3, v0

    move v0, v2

    :goto_1
    move v7, v2

    move v4, v0

    :goto_2
    if-ge v7, v8, :cond_7

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->managerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    :try_start_0
    iget-object v5, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    invoke-virtual {v5}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "ScanningManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Start Scanning"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListenerImpl;

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    invoke-direct {v5, v9, p0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListenerImpl;-><init>(Lcom/sec/android/service/health/sensor/manager/ScanningManager;Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;)V

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    iget-object v9, v9, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {v0, v5, v9}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchStartScanning(Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;Lcom/sec/android/service/health/sensor/manager/util/Filter;)I

    move-result v5

    if-nez v5, :cond_6

    add-int v5, v7, v4

    if-ne v5, v8, :cond_5

    add-int/lit8 v4, v4, -0x1

    move v5, v6

    :goto_3
    add-int/2addr v5, v3

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v9, v5

    invoke-static {v9, v10}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->sleep(J)V

    :goto_4
    const-string v5, "ScanningManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Stop Scanning"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchStopScanning()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v4

    :goto_5
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v4, v0

    goto :goto_2

    :cond_2
    mul-int/lit8 v1, v8, 0xa

    if-le v0, v1, :cond_4

    mul-int/lit8 v1, v8, 0xf

    if-le v0, v1, :cond_3

    const/16 v1, 0xa

    mul-int v3, v8, v1

    sub-int/2addr v0, v3

    move v3, v1

    move v1, v0

    move v0, v2

    goto/16 :goto_1

    :cond_3
    div-int v1, v0, v8

    rem-int/2addr v0, v8

    move v3, v1

    move v1, v2

    goto/16 :goto_1

    :cond_4
    div-int v1, v0, v8

    rem-int/2addr v0, v8

    move v3, v1

    move v1, v2

    goto/16 :goto_1

    :cond_5
    move v5, v2

    goto :goto_3

    :cond_6
    :try_start_1
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->this$0:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    iput v5, v9, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mErrorFromManager:I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception v5

    const-string v5, "ScanningManager"

    const-string v9, "InterruptedException received"

    invoke-static {v5, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchStopScanning()Z

    move v0, v4

    goto :goto_5

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method
