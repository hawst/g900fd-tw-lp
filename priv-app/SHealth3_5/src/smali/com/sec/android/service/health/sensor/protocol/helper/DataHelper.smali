.class public Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "DataHelper"

.field private static dhInstance:Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->dhInstance:Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->dhInstance:Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    return-object v0
.end method


# virtual methods
.method public addDataToDb(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    invoke-virtual {v0, p1, p3, p2}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DataHelper"

    const-string v1, "DB data insert Failure"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    invoke-virtual {v0, p1, p3, p2}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;[Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DataHelper"

    const-string v1, "DB data insert Failure"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startData(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;JI)Z"
        }
    .end annotation

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-wide v5, p5

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    const/4 v0, 0x1

    return v0
.end method

.method public stopData(Ljava/lang/String;ILjava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    const/4 v0, 0x1

    return v0
.end method
