.class public Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;
.super Ljava/lang/Object;


# instance fields
.field public accessoryId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public daylightSaving:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceTHPKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public deviceUpdateTime:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public humidity:Ljava/lang/Double;

.field public measuredTime:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public temperature:Ljava/lang/Double;

.field public temperatureUnit:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
