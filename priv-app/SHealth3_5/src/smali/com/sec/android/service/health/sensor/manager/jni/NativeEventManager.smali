.class public Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;
.super Ljava/lang/Object;


# static fields
.field private static final EVENT_BLUETOOTH_SEND_DATA:I = 0x12c

.field private static final EVENT_HEALTHCARE_CHANNEL_ABORTED:I = 0xcb

.field private static final EVENT_HEALTHCARE_CHANNEL_CREATED:I = 0xca

.field private static final EVENT_HEALTHCARE_CHANNEL_RELEASE:I = 0xc9

.field private static final EVENT_HEALTHCARE_GETDATA:I = 0xc8

.field private static final MEASUREDATA:I = 0x3e9

.field private static final SENSOR_INFO:I = 0x3e8

.field protected static final TAG:Ljava/lang/String;

.field private static hashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private static mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private static nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->hashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    :try_start_0
    const-string v0, "health_jni_k"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager static health_jni loadLibrary succeeded"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager static Could not load native library health_jni"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->nativeSetup(Ljava/lang/Object;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->nativeStart()V

    return-void
.end method

.method private static convertProfile2DataType(I)I
    .locals 1

    const/4 v0, 0x0

    sparse-switch p0, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    const/16 v0, 0x100f

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x1007

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1011

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x904 -> :sswitch_1
        0x90c -> :sswitch_0
        0x910 -> :sswitch_2
    .end sparse-switch
.end method

.method private static createMeasurementData(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/service/health/sensor/data/MeasurementData;
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x6

    const-string v2, "\\|"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x4

    :try_start_0
    aget-object v2, v5, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sget-object v3, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[MEASUREDATA] deviceType = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " len:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v6, v5

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_0

    const/4 v2, -0x1

    sget-object v3, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3, p0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3, p0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v3

    if-eqz v3, :cond_3

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2, p0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v2

    move v3, v2

    :goto_0
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->convertProfile2DataType(I)I

    move-result v2

    sget-object v4, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[MEASUREDATA] deviceType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " profile = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v4, v2

    array-length v2, v5

    if-ne v2, v0, :cond_2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "[MEASUREDATA] no date type"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/data/MeasurementData;

    invoke-direct {v0, p0, v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData;-><init>(Ljava/lang/String;I)V

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[MEASUREDATA] data = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x5

    aget-object v4, v5, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x5

    aget-object v2, v5, v2

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->setMeasuredDataField2(Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    move v2, v0

    move-object v0, v1

    :goto_2
    array-length v3, v5

    if-ge v2, v3, :cond_1

    new-instance v3, Lcom/sec/android/service/health/sensor/data/MeasurementData;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData;-><init>(Ljava/lang/String;I)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[MEASUREDATA] data = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v5, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    aget-object v0, v5, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->setPersonId(I)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[MEASUREDATA] person id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x3

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v5, v2

    invoke-virtual {v3, v0}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->setMeasuredDataField2(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v3

    goto :goto_2

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v3, "NativeEventManager NumberFormatException exception occured while handling measured data."

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    move-object v0, v1

    goto :goto_1

    :cond_3
    move v3, v2

    goto/16 :goto_0
.end method

.method protected static createSensorInformation(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/service/health/sensor/data/SensorInfo;
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/data/SensorInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/data/SensorInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/data/SensorInfo;->setSensorInfoField(Ljava/lang/String;)V

    return-object v0
.end method

.method public static declared-synchronized getNativeEventManager()Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;
    .locals 2

    const-class v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->nativeEventManager:Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static native nativeCloseConnect(Ljava/lang/String;I)V
.end method

.method public static native nativeDisconnect(Ljava/lang/String;)V
.end method

.method public static native nativeGetSystemInfo(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static native nativeRequestMeasuredData(Ljava/lang/String;)V
.end method

.method public static native nativeRequestPMStore(Ljava/lang/String;)V
.end method

.method public static native nativeSendClearAbsTimeSegments(Ljava/lang/String;)V
.end method

.method public static native nativeSendClearAllSegments(Ljava/lang/String;)V
.end method

.method public static native nativeSendClearBOTimeSegments(Ljava/lang/String;JJI)V
.end method

.method public static native nativeSendClearSegIdSegments(Ljava/lang/String;I)V
.end method

.method public static native nativeSendGetAttributes(Ljava/lang/String;)V
.end method

.method public static native nativeSendGetSegmentsInfoAbsTime(Ljava/lang/String;)V
.end method

.method public static native nativeSendGetSegmentsInfoAll(Ljava/lang/String;)V
.end method

.method public static native nativeSendGetSegmentsInfoBOTime(Ljava/lang/String;JJI)V
.end method

.method public static native nativeSendGetSegmentsInfoSegId(Ljava/lang/String;I)V
.end method

.method public static native nativeSendRequestToUsbAgent(Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public static native nativeSendSetScannerState(Ljava/lang/String;II)V
.end method

.method public static native nativeSetSystemTime(Ljava/lang/String;IIIIII)V
.end method

.method public static native nativeSetup(Ljava/lang/Object;)V
.end method

.method public static native nativeStart()V
.end method

.method public static native nativeStop(Ljava/lang/String;)V
.end method

.method public static native nativeTrigSegmentDataXfer(Ljava/lang/String;JJ)V
.end method

.method private static postHealthEventFromNative(Ljava/lang/Object;Ljava/lang/String;II[B)V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NativeEventManager postHealthEventFromNative event:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dataType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager postHealthEventFromNative UID null or Zero Length So returning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->hashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager postHealthEventFromNative nativeHandler null So returning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sparse-switch p2, :sswitch_data_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NativeEventManager postHealthEventFromNative event["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not available. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_0
    sget-object v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "NativeEventManager postHealthEventFromNative EVENT_HEALTHCARE_CHANNEL_CREATED"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;->onStartSession()V

    goto :goto_0

    :sswitch_1
    sget-object v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "NativeEventManager postHealthEventFromNative EVENT_HEALTHCARE_CHANNEL_RELEASE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;->onFinishSession()V

    goto :goto_0

    :sswitch_2
    sget-object v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "NativeEventManager postHealthEventFromNative EVENT_HEALTHCARE_CHANNEL_ABORTED"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x65

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;->onHealthServiceError(ILjava/lang/String;)V

    goto :goto_0

    :sswitch_3
    sget-object v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "NativeEventManager postHealthEventFromNative EVENT_HEALTHCARE_GETDATA"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x3e8

    if-ne p3, v1, :cond_4

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "NativeEventManager postHealthEventFromNative EVENT_HEALTHCARE_GETDATA SENSOR_INFO"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p4}, Ljava/lang/String;-><init>([B)V

    invoke-static {p1, v1}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->createSensorInformation(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/service/health/sensor/data/SensorInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;->onSensorInfoReceived(Lcom/sec/android/service/health/sensor/data/SensorInfo;)V

    goto :goto_0

    :cond_4
    const/16 v1, 0x3e9

    if-ne p3, v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "NativeEventManager postHealthEventFromNative EVENT_HEALTHCARE_GETDATA MEASUREDATA"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p4}, Ljava/lang/String;-><init>([B)V

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[MEASUREDATA] data = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, v1}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->createMeasurementData(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/service/health/sensor/data/MeasurementData;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;->onMeasuredDataReceived(Lcom/sec/android/service/health/sensor/data/MeasurementData;)V

    goto/16 :goto_0

    :sswitch_4
    sget-object v1, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v2, "NativeEventManager postHealthEventFromNative EVENT_BLUETOOTH_SEND_DATA"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0, p4}, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;->sendDataToSensor([B)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_3
        0xc9 -> :sswitch_1
        0xca -> :sswitch_0
        0xcb -> :sswitch_2
        0x12c -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public native nativeRelease()V
.end method

.method public register(Ljava/lang/String;Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->hashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager register register was success no previous registration"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager register register previously UID has been registered, but not unregistered"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unregister(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->hashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager unregister Failure, UID has not been registed earlier"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->TAG:Ljava/lang/String;

    const-string v1, "NativeEventManager unregister Success, Value registered previously has been removed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
