.class public Lcom/sec/android/service/health/connectionmanager2/NetException;
.super Ljava/lang/Exception;
.source "NetException.java"


# static fields
.field public static final ERR_CLIENT_BIND:I = -0xf

.field public static final ERR_CLIENT_CONFIGCHANGED:I = -0x14

.field public static final ERR_CLIENT_DATA_PARSING:I = -0x1

.field public static final ERR_CLIENT_DIRECTORY:I = -0x12

.field public static final ERR_CLIENT_FILEIO:I = -0x4

.field public static final ERR_CLIENT_FILENOTFOUND:I = -0x13

.field public static final ERR_CLIENT_GENERAL_ERROR:I = -0x17

.field public static final ERR_CLIENT_HOST_NOT_FOUND:I = -0xe

.field public static final ERR_CLIENT_HTTP:I = -0x11

.field public static final ERR_CLIENT_ILLEGAL_STATE:I = -0x20

.field public static final ERR_CLIENT_INTERNAL:I = -0xa

.field public static final ERR_CLIENT_IO:I = -0x1f

.field public static final ERR_CLIENT_KEYSTORE:I = -0x1b

.field public static final ERR_CLIENT_KEY_MANAGEMENT:I = -0x1a

.field public static final ERR_CLIENT_MALFORMEDURL:I = -0x9

.field public static final ERR_CLIENT_NETWORKIO:I = -0xc

.field public static final ERR_CLIENT_NETWORK_CONN:I = -0x3

.field public static final ERR_CLIENT_NO_CONTENT:I = -0x1e

.field public static final ERR_CLIENT_NO_SUCH_AlGORITHM:I = -0x18

.field public static final ERR_CLIENT_PROTOCOL:I = -0x7

.field public static final ERR_CLIENT_REQUEST_CANCELED:I = -0x15

.field public static final ERR_CLIENT_RESPONSE_TIMEOUT:I = -0x5

.field public static final ERR_CLIENT_RETRY:I = -0xd

.field public static final ERR_CLIENT_SESSION_TIMEOUT:I = -0x10

.field public static final ERR_CLIENT_SOCKET:I = -0xb

.field public static final ERR_CLIENT_TABLE:I = -0x2

.field public static final ERR_CLIENT_THREAD_INTERRUPTED:I = -0x1d

.field public static final ERR_CLIENT_UNAUTHORIZED:I = -0x8

.field public static final ERR_CLIENT_UNEXPECTED:I = -0x6

.field public static final ERR_CLIENT_UNRECOVERABLE_KEY:I = -0x1c

.field public static final ERR_CLIENT_USE_DUMMY_DATA:I = -0x16

.field public static final ERR_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "NetException"

.field private static final serialVersionUID:J = 0x27f0ea5a854475L


# instance fields
.field private e:Ljava/lang/Exception;

.field private errorCode:I

.field private httpResCode:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "httpResCode"    # I
    .param p2, "errorCode"    # I

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    .line 60
    return-void
.end method

.method public constructor <init>(IILjava/lang/Exception;)V
    .locals 1
    .param p1, "httpResCode"    # I
    .param p2, "errorCode"    # I
    .param p3, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    .line 71
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 1
    .param p1, "httpResCode"    # I
    .param p2, "errorCode"    # I
    .param p3, "desc"    # Ljava/lang/String;

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    .line 83
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "httpResCode"    # I
    .param p2, "errorCode"    # I
    .param p3, "desc"    # Ljava/lang/String;
    .param p4, "e"    # Ljava/lang/Exception;

    .prologue
    .line 95
    invoke-direct {p0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/connectionmanager2/NetException;->errorCode:I

    .line 97
    const-string v0, "NetException"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NetException - httpResCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iput p1, p0, Lcom/sec/android/service/health/connectionmanager2/NetException;->httpResCode:I

    .line 100
    iput p2, p0, Lcom/sec/android/service/health/connectionmanager2/NetException;->errorCode:I

    .line 101
    iput-object p4, p0, Lcom/sec/android/service/health/connectionmanager2/NetException;->e:Ljava/lang/Exception;

    .line 102
    return-void
.end method

.method public static createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;
    .locals 7
    .param p0, "httpStatus"    # I
    .param p1, "ex"    # Ljava/lang/Exception;

    .prologue
    const/4 v6, -0x6

    const/4 v5, -0x7

    const/16 v4, -0x9

    const/16 v3, -0xe

    const/4 v2, -0x3

    .line 483
    if-nez p1, :cond_0

    .line 485
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v6, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    .line 588
    .end local p1    # "ex":Ljava/lang/Exception;
    :goto_0
    return-object p1

    .line 487
    .restart local p1    # "ex":Ljava/lang/Exception;
    :cond_0
    instance-of v0, p1, Lcom/sec/android/service/health/connectionmanager2/NetException;

    if-eqz v0, :cond_1

    .line 489
    check-cast p1, Lcom/sec/android/service/health/connectionmanager2/NetException;

    goto :goto_0

    .line 491
    :cond_1
    instance-of v0, p1, Ljava/net/MalformedURLException;

    if-eqz v0, :cond_2

    .line 493
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v4, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0

    .line 495
    :cond_2
    instance-of v0, p1, Ljava/net/ProtocolException;

    if-eqz v0, :cond_3

    .line 497
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v5, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0

    .line 499
    :cond_3
    instance-of v0, p1, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_4

    .line 501
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v1, -0x5

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0

    .line 503
    :cond_4
    instance-of v0, p1, Ljava/net/BindException;

    if-eqz v0, :cond_5

    .line 505
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0xf

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0

    .line 507
    :cond_5
    instance-of v0, p1, Ljava/net/ConnectException;

    if-eqz v0, :cond_6

    .line 509
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v2, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0

    .line 511
    :cond_6
    instance-of v0, p1, Ljava/net/HttpRetryException;

    if-eqz v0, :cond_7

    .line 513
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0xd

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0

    .line 515
    :cond_7
    instance-of v0, p1, Ljava/net/NoRouteToHostException;

    if-eqz v0, :cond_8

    .line 517
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v2, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto :goto_0

    .line 519
    :cond_8
    instance-of v0, p1, Ljava/net/PortUnreachableException;

    if-eqz v0, :cond_9

    .line 521
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v2, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 523
    :cond_9
    instance-of v0, p1, Ljava/net/UnknownHostException;

    if-eqz v0, :cond_a

    .line 525
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v3, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 527
    :cond_a
    instance-of v0, p1, Ljava/net/UnknownServiceException;

    if-eqz v0, :cond_b

    .line 529
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v3, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 531
    :cond_b
    instance-of v0, p1, Ljava/net/URISyntaxException;

    if-eqz v0, :cond_c

    .line 533
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v4, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 535
    :cond_c
    instance-of v0, p1, Ljava/net/SocketException;

    if-eqz v0, :cond_d

    .line 537
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0xb

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 542
    :cond_d
    instance-of v0, p1, Ljava/security/NoSuchAlgorithmException;

    if-eqz v0, :cond_e

    .line 544
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0x18

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 550
    :cond_e
    instance-of v0, p1, Ljava/security/KeyManagementException;

    if-eqz v0, :cond_f

    .line 552
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0x1a

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 554
    :cond_f
    instance-of v0, p1, Ljava/security/KeyStoreException;

    if-eqz v0, :cond_10

    .line 556
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0x1b

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 558
    :cond_10
    instance-of v0, p1, Ljava/security/UnrecoverableKeyException;

    if-eqz v0, :cond_11

    .line 560
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0x1c

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 563
    :cond_11
    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_12

    .line 565
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0x1d

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 568
    :cond_12
    instance-of v0, p1, Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_13

    .line 570
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0x20

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 572
    :cond_13
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_14

    .line 574
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v1, -0x1f

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 583
    :cond_14
    instance-of v0, p1, Lorg/apache/http/client/ClientProtocolException;

    if-eqz v0, :cond_15

    .line 585
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v5, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0

    .line 588
    :cond_15
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1, p1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    move-object p1, v0

    goto/16 :goto_0
.end method

.method public static createNetException(Ljava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;
    .locals 1
    .param p0, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 471
    const/4 v0, -0x1

    invoke-static {v0, p0}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/android/service/health/connectionmanager2/NetException;->errorCode:I

    return v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/NetException;->e:Ljava/lang/Exception;

    return-object v0
.end method

.method public getHttpResCode()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/sec/android/service/health/connectionmanager2/NetException;->httpResCode:I

    return v0
.end method
