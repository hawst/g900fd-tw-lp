.class public Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestDeleteTagHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TAG_GET_INDEX_MAX:I = 0x64

.field private static final TAG_SET_INDEX_MAX:I = 0x64


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTag(Ljava/lang/Object;)V
    .locals 8

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->TAG:Ljava/lang/String;

    const-string v1, "addTag"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/data/TagList;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/TagList;

    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/TagList;->tagList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/TagList;->tagList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/TagList;->tagList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "_id"

    iget-wide v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->devicePKId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "application__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->appId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "table_name_type"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->recordType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceRecordPKId:Ljava/lang/Long;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceRecordPKId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    const-string/jumbo v4, "table_record_id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceRecordPKId:Ljava/lang/Long;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string/jumbo v4, "tag_index"

    iget v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->tagIndex:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "custom_icon_index"

    iget v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->customIconIndex:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "custom_tag_name"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->customTagName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "input_source_type"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->sourceType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "create_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "update_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceModifyTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "time_zone"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceTimeZone:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "hdid"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->hdid:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "sync_status"

    const v5, 0x29811

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceDaylightSaving:Ljava/lang/Integer;

    if-eqz v4, :cond_1

    const-string v4, "daylight_saving"

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceDaylightSaving:Ljava/lang/Integer;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    const-string/jumbo v0, "tag"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v1, v0, v4, v3, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error inserting tag data. Invalid values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :cond_1
    :try_start_3
    const-string v0, "daylight_saving"

    const v4, 0x445c2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    :cond_3
    return-void
.end method

.method public static getDeletedTag(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDeletedLocalData()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestDeleteTagHistory;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestDeleteTagHistory;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "deleted_info"

    const-string/jumbo v3, "table_name = \"tag\""

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestDeleteTagHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    const-string v3, "create_time"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestDeleteTagHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return-object v2

    :cond_2
    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_TAG_INDEX:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v8

    goto :goto_1
.end method

.method public static getModifiedTag(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/CursorWindowAllocationException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->TAG:Ljava/lang/String;

    const-string v1, "getUnsyncedLocalData()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v3, "sync_status = ? or sync_status = ? "

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const v1, 0x29812

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    const/4 v1, 0x1

    const v5, 0x29813

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/data/TagList;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/data/TagList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v1, "tag"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v6, v8

    move-object v7, v9

    move-object v9, v10

    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/data/TagItem;-><init>()V

    const-string v0, "application__id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->appId:Ljava/lang/String;

    const-string/jumbo v0, "time_zone"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceTimeZone:Ljava/lang/String;

    const-string v0, "_id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->devicePKId:J

    const-string v0, "custom_icon_index"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->customIconIndex:I

    const-string v0, "custom_tag_name"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->customTagName:Ljava/lang/String;

    const-string/jumbo v0, "table_name_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->recordType:Ljava/lang/String;

    const-string/jumbo v0, "table_record_id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceRecordPKId:Ljava/lang/Long;

    const-string/jumbo v0, "tag_index"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->tagIndex:I

    const-string v0, "input_source_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->sourceType:Ljava/lang/String;

    const-string v0, "hdid"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->hdid:Ljava/lang/String;

    const-string v0, "daylight_saving"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "daylight_saving"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceDaylightSaving:Ljava/lang/Integer;

    :goto_1
    const-string v0, "create_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceCreateTime:Ljava/lang/String;

    const-string/jumbo v0, "update_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceModifyTime:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v1, "tag"

    iget-wide v2, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->devicePKId:J

    const-string/jumbo v4, "update_time"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TagList;->tagList:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v6, 0x1

    const/16 v1, 0x64

    if-ne v0, v1, :cond_5

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_TAG_INDEX:Ljava/lang/String;

    invoke-direct {v0, v1, v9, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/data/TagList;

    invoke-direct {v2}, Lcom/sec/android/service/health/cp/serversync/data/TagList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move v0, v8

    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move v6, v0

    move-object v7, v1

    move-object v9, v2

    goto/16 :goto_0

    :cond_0
    const v0, 0x445c2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/TagItem;->deviceDaylightSaving:Ljava/lang/Integer;

    goto :goto_1

    :cond_1
    move v8, v6

    move-object v10, v9

    move-object v9, v7

    :cond_2
    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    if-lez v8, :cond_4

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_TAG_INDEX:Ljava/lang/String;

    invoke-direct {v0, v1, v10, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v11

    :cond_5
    move-object v1, v7

    move-object v2, v9

    goto :goto_2
.end method

.method public static getTagDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;I)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDetailHistoryData()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;

    invoke-direct {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;-><init>()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " items for tag"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v4, 0x63

    if-ne v0, v4, :cond_0

    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_TAG_INDEX:Ljava/lang/String;

    invoke-direct {v0, v4, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils$RequestGetTagHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_TAG_INDEX:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v2
.end method
