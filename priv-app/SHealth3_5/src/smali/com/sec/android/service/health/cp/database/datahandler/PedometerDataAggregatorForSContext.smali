.class public Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;
.super Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    return-void
.end method


# virtual methods
.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 12

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addData-Start with exercise_id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mExerciseId:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    const/4 v3, 0x0

    :try_start_0
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_3

    aget-object v2, p1, v6

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v4

    :goto_1
    return v1

    :cond_0
    :try_start_1
    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object v3, v0

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v8, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/32 v10, 0x927c0

    div-long/2addr v8, v10

    iget-wide v10, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mCurrentMin:J

    cmp-long v1, v8, v10

    if-eqz v1, :cond_2

    sget-object v8, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " time = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v10, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " mCurrentMin "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v9, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mCurrentMin:J

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mCurrentMin:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "updateExerciseForBulkData before new row is created. end time: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v9, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->insertData()V

    iget-wide v8, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->updateExerciseSummery(J)V

    :cond_1
    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v8, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/32 v10, 0x927c0

    div-long/2addr v8, v10

    iput-wide v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mCurrentMin:J

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->resetAggregation()V

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    :goto_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_0

    :cond_2
    sget-object v8, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " process data  time = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v10, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " mCurrentMin = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v9, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mCurrentMin:J

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while updating db"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v5

    goto/16 :goto_1

    :cond_3
    if-eqz v3, :cond_4

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " inserting/updating the data remaining data "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " current minute = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mCurrentMin:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " mTotalStep "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mTotalStep:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->insertData()V

    iget-wide v1, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->updateExerciseSummery(J)V

    :cond_4
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mTotalStep not inserted : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->mTotalStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addData-End   : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method
