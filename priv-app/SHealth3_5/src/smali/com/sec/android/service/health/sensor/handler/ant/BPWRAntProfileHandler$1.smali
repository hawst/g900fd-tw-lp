.class Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$4;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$RequestStatus:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "Request Failed to be Sent"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "Request Successfully Sent"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
