.class public Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthServiceInstalled(Landroid/content/Context;)Z

    move-result v2

    if-eq v2, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService2xInstalled(Landroid/content/Context;)Z

    move-result v2

    if-ne v2, v0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static isSHealthUpgradeNeeded(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
