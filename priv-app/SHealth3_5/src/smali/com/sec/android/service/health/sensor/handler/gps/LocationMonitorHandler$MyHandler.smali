.class final Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MyHandler"
.end annotation


# instance fields
.field private mEnclosingClassWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;->mEnclosingClassWeakRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;->mEnclosingClassWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PARAM_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "PARAM_CHECK_WEARABLE_DEVICE_CONNECTED"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "PARAM_HEIGHT"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v3

    const-string v4, "PARAM_WEIGHT"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    const-string v5, "PARAM_PSERVICE"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/location/monitor/LocationMonitorCallback;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Lcom/samsung/location/monitor/LocationMonitor;

    move-result-object v7

    if-eqz v7, :cond_3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Lcom/samsung/location/monitor/LocationMonitor;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring()Z

    move-result v7

    if-eqz v7, :cond_3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "stop previous location monitor"

    invoke-static {v0, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Lcom/samsung/location/monitor/LocationMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/monitor/LocationMonitor;->stopMonitoring()V

    :cond_2
    :goto_1
    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_4

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v7, "android.hardware.sensor.barometer"

    invoke-virtual {v0, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    # setter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z
    invoke-static {v6, v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$302(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Z)Z

    :goto_2
    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LOCATION_CREATE isAvailableBarometer : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "type = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isWearableDeviceConnected = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " h = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " w = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " persiveType ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Lcom/samsung/location/monitor/LocationMonitor;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Lcom/samsung/location/monitor/LocationMonitor;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/location/monitor/LocationMonitor;->startMonitoring(IZFFI)V

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LOCATION_CREATE - startMonitoring is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "startMonitoring is called but UnsupportedOperationException is occurred"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->stopReceivingData(I)I

    goto/16 :goto_0

    :cond_3
    :try_start_1
    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Lcom/samsung/location/monitor/LocationMonitor;

    move-result-object v7

    if-nez v7, :cond_2

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$100()Ljava/lang/String;

    move-result-object v7

    const-string v8, "create new LocationMonitor"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8, v0}, Lcom/samsung/location/monitor/LocationMonitor;-><init>(Landroid/content/Context;Lcom/samsung/location/monitor/LocationMonitorCallback;)V

    # setter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v6, v7}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$002(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor;

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    # setter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z
    invoke-static {v6, v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$302(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Z)Z
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
    .end packed-switch
.end method
