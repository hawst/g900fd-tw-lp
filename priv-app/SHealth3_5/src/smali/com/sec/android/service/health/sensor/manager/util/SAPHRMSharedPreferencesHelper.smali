.class public Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;
.super Ljava/lang/Object;


# static fields
.field public static final ANT_BIKE_WHEEL_SIZE:Ljava/lang/String; = "ant_bike_wheel_size"

.field public static final GEAR2_DEVICE_MAC:Ljava/lang/String; = "gear2_device_MAC"

.field public static final GEAR2_DEVICE_NAME:Ljava/lang/String; = "gear2_device_name"

.field public static final GEAR2_LAST_SYNC_TIME:Ljava/lang/String; = "gear2_last_sync_time"

.field public static final GEAR2_PREF_VERSION:Ljava/lang/String; = "GEAR2_PREF_VERSION"

.field public static final GEAR_DEVICE_MAC:Ljava/lang/String; = "gear_device_MAC"

.field public static final GEAR_DEVICE_NAME:Ljava/lang/String; = "gear_device_name"

.field public static final GEAR_LAST_SYNC_TIME:Ljava/lang/String; = "gear_last_sync_time"

.field private static final MY_SAPHRM:Ljava/lang/String; = "my_saphrm"

.field public static final PREF_VERSION:Ljava/lang/String; = "PREF_VERSION"

.field private static final SAPHRM_KEY_DISCONNECTED_BY_USER:Ljava/lang/String; = "saphrm_key_disconnected_by_user"

.field private static final SAPHRM_KEY_IS_PAIRED:Ljava/lang/String; = "saphrm_key_is_paired"

.field public static final SBAND_LAST_SYNC_TIME:Ljava/lang/String; = "sband_last_sync_time"

.field private static final TAG:Ljava/lang/String; = "[HealthSensor]SAPHRMSharedPreferencesHelper"

.field public static final WINGTIP_DEVICE_MAC:Ljava/lang/String; = "wingtip_device_MAC"

.field public static final WINGTIP_DEVICE_NAME:Ljava/lang/String; = "wingtip_device_name"

.field public static final WINGTIP_LAST_SYNC_TIME:Ljava/lang/String; = "wingtip_last_sync_time"

.field public static final WINGTIP_PREF_VERSION:Ljava/lang/String; = "WINGTIP_PREF_VERSION"

.field private static prefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getANTbikeWheelsize()F
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "ant_bike_wheel_size"

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getGear2DeviceMAC()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "gear2_device_MAC"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getGear2DeviceName()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "gear2_device_name"

    const-string v2, "Gear 2"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getGearDeviceMAC()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "gear_device_MAC"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getGearDeviceName()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "gear_device_name"

    const-string v2, "Samsung Galaxy Gear"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastSyncTimeKeyList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "gear_last_sync_time"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static getMySAPHRMMAC()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "my_saphrm"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPairedState()Z
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "saphrm_key_is_paired"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v2, "SHRM getPairedState : true"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    const-string v1, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v2, "SHRM getPairedState : false"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getSAPHRMName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, ""

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserDisconnection()Z
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "saphrm_key_disconnected_by_user"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v2, "SHRM getUserDisconnection : true"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    const-string v1, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v2, "SHRM getUserDisconnection : false"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getWearableLastSyncTime(ILjava/lang/String;)J
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gear_last_sync_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getWearableLastSyncTime(Ljava/lang/String;)J
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-wide/16 v1, 0x0

    invoke-interface {v0, p0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getWingtipDeviceMAC()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "wingtip_device_MAC"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWingtipDeviceName()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "wingtip_device_name"

    const-string v2, "Gear Fit"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "PREF_VERSION"

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveANTbikeWheelsize(F)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ant_bike_wheel_size"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveDisconnectionByUser(Z)V
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v1, "SHRM saveDisconnectionByUser : true"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "saphrm_key_disconnected_by_user"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    const-string v0, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v1, "SHRM saveDisconnectionByUser : false"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveGear2DeviceMAC(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "gear2_device_MAC"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveGear2DeviceName(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "gear2_device_name"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveGearDeviceMAC(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "gear_device_MAC"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveGearDeviceName(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "gear_device_name"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveMySAPHRMMAC(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "my_saphrm"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static savePairedState(Z)V
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v1, "SHRM savePairedState : true"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "saphrm_key_is_paired"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    const-string v0, "[HealthSensor]SAPHRMSharedPreferencesHelper"

    const-string v1, "SHRM savePairedState : false"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveWearableLastSyncTime(ILjava/lang/String;J)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gear_last_sync_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveWearableLastSyncTime(Ljava/lang/String;J)V
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveWingtipDeviceMAC(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "wingtip_device_MAC"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static saveWingtipDeviceName(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "wingtip_device_name"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static setSAPHRMName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
