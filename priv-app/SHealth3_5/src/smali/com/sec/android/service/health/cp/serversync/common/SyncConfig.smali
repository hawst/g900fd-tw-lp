.class public Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;
.super Ljava/lang/Object;


# static fields
.field private static IS_PROFILING_ENABLED:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->IS_PROFILING_ENABLED:Z

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-boolean v2, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->IS_PROFILING_ENABLED:Z

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isProfilingEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->IS_PROFILING_ENABLED:Z

    return v0
.end method
