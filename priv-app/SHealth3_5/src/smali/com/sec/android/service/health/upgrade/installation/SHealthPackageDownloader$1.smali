.class Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader$1;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader$1;->this$0:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const/4 v1, 0x1

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader$1;->this$0:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;

    # getter for: Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;->mDownloadHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;->access$000(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageDownloader;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
