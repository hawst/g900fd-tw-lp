.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageUrlDetails"
.end annotation


# instance fields
.field public filePath:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public mTotalFileSize:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->imageUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    return-void
.end method
