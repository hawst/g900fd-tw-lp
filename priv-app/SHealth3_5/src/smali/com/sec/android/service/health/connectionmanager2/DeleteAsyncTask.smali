.class Lcom/sec/android/service/health/connectionmanager2/DeleteAsyncTask;
.super Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
.source "DeleteAsyncTask.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 0
    .param p1, "connectionManager"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "requestparameters"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected varargs doInBackground2([Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .locals 21
    .param p1, "params"    # [Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 46
    const/4 v12, 0x0

    .line 47
    .local v12, "responseCode":I
    const/4 v6, 0x0

    .line 48
    .local v6, "httpResponse":Lorg/apache/http/HttpResponse;
    const/16 v18, 0x0

    aget-object v18, p1, v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getUrl()Ljava/lang/String;

    move-result-object v16

    .line 50
    .local v16, "strURL":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    .line 52
    .local v11, "response":Ljava/lang/StringBuffer;
    const/16 v18, 0x0

    aget-object v18, p1, v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getSetHeadervalues()Ljava/util/HashMap;

    move-result-object v14

    .line 54
    .local v14, "setHeaderValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/DeleteAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v18, v0

    if-nez v18, :cond_0

    .line 56
    new-instance v13, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v18, -0x1

    const/16 v19, -0x7

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v13, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 57
    .local v13, "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/16 v18, 0x0

    aget-object v18, p1, v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 58
    const/16 v18, 0x0

    aget-object v18, p1, v18

    .line 148
    .end local v13    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :goto_0
    return-object v18

    .line 63
    :cond_0
    :try_start_0
    new-instance v7, Lorg/apache/http/client/methods/HttpDelete;

    move-object/from16 v0, v16

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 93
    .local v7, "httpdelete":Lorg/apache/http/client/methods/HttpDelete;
    if-eqz v14, :cond_4

    .line 95
    const/16 v17, 0x0

    .line 96
    .local v17, "value":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 97
    .local v10, "key":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 98
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "key":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 99
    .restart local v10    # "key":Ljava/lang/String;
    if-eqz v10, :cond_1

    .line 101
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "value":[Ljava/lang/String;
    check-cast v17, [Ljava/lang/String;

    .line 102
    .restart local v17    # "value":[Ljava/lang/String;
    if-eqz v17, :cond_2

    .line 104
    const/4 v9, 0x0

    .local v9, "k":I
    :goto_2
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v9, v0, :cond_1

    .line 105
    aget-object v18, v17, v9

    move-object/from16 v0, v18

    invoke-virtual {v7, v10, v0}, Lorg/apache/http/client/methods/HttpDelete;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 109
    .end local v9    # "k":I
    :cond_2
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v7, v10, v0}, Lorg/apache/http/client/methods/HttpDelete;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 143
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v7    # "httpdelete":Lorg/apache/http/client/methods/HttpDelete;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "key":Ljava/lang/String;
    .end local v17    # "value":[Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 145
    .local v4, "ex":Ljava/lang/Exception;
    invoke-static {v12, v4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    .line 147
    .end local v4    # "ex":Ljava/lang/Exception;
    :cond_3
    :goto_3
    const/16 v18, 0x0

    aget-object v18, p1, v18

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 148
    const/16 v18, 0x0

    aget-object v18, p1, v18

    goto :goto_0

    .line 116
    .restart local v7    # "httpdelete":Lorg/apache/http/client/methods/HttpDelete;
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/DeleteAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v18

    if-eqz v18, :cond_5

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/DeleteAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/connectionmanager2/DeleteAsyncTask;->getHttpContext()Lorg/apache/http/protocol/BasicHttpContext;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v7, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 120
    :cond_5
    if-nez v6, :cond_6

    .line 122
    new-instance v18, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v19, -0x1

    const/16 v20, -0x7

    invoke-direct/range {v18 .. v20}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    throw v18

    .line 124
    :cond_6
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v12

    .line 125
    const/16 v18, 0xc8

    move/from16 v0, v18

    if-eq v12, v0, :cond_7

    .line 128
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v18

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v13

    .line 129
    .restart local v13    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/16 v18, 0x0

    aget-object v18, p1, v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 130
    const/16 v18, 0x0

    aget-object v18, p1, v18

    goto/16 :goto_0

    .line 132
    .end local v13    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_7
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    .line 133
    .local v5, "httpEntity":Lorg/apache/http/HttpEntity;
    if-eqz v5, :cond_3

    .line 135
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v18, Ljava/io/InputStreamReader;

    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 136
    .local v2, "bufferedReader":Ljava/io/BufferedReader;
    :goto_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v15

    .local v15, "strLine":Ljava/lang/String;
    if-eqz v15, :cond_8

    .line 138
    invoke-virtual {v11, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 140
    :cond_8
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method
