.class public Lcom/sec/android/service/health/cp/common/DBTables$MapPathTable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MapPathTable"
.end annotation


# static fields
.field public static final ACCURACY:Ljava/lang/String; = "accuracy"

.field public static final ALTITUDE:Ljava/lang/String; = "altitude"

.field public static final DB_INDEX:Ljava/lang/String; = "db_index"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MAP_PATH_ID:Ljava/lang/String; = "_id"

.field public static final TABLE_NAME:Ljava/lang/String; = "map_path"

.field public static final TIME:Ljava/lang/String; = "time"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
