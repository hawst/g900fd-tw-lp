.class public Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MealItem"
.end annotation


# instance fields
.field public Calorie:D

.field public amount:F

.field public appId:Ljava/lang/String;

.field public carbohydrate:Ljava/lang/Float;

.field public cholesterol:Ljava/lang/Float;

.field public deviceCreateTimeFoodItem:Ljava/lang/String;

.field public deviceCreateTimeMeal:Ljava/lang/String;

.field public deviceDaylightSavingFoodItem:Ljava/lang/String;

.field public deviceFoodItemPKId:Ljava/lang/String;

.field public deviceModifyTimeFoodItem:Ljava/lang/String;

.field public deviceTimeZoneFoodItem:Ljava/lang/String;

.field public fat:Ljava/lang/Float;

.field public foodId:Ljava/lang/String;

.field public foodItemUnit:Ljava/lang/String;

.field public foodName:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public protein:Ljava/lang/Float;

.field public sodium:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
