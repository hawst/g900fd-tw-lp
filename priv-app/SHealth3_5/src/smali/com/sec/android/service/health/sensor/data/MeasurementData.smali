.class public Lcom/sec/android/service/health/sensor/data/MeasurementData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/service/health/sensor/data/MeasurementData;",
            ">;"
        }
    .end annotation
.end field

.field public static final DATA:I = 0x7

.field public static final DEVICE_TYPE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "[HealthSensor]HealthService"

.field public static final TIME:I = 0x6

.field public static final TOTAL_NUM_OF_DATA:I = 0x5

.field public static final USER_ID:I = 0x3


# instance fields
.field private dataNum:I

.field private dataStartPosition:I

.field private mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

.field private mDeviceType:I

.field private mMeasureDataArray:[Ljava/lang/String;

.field private mPersonId:I

.field private mTimeStamp:Landroid/text/format/Time;

.field private mTimeStampType:I

.field private mTotalDataSet:I

.field private mUid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/data/MeasurementData$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/data/MeasurementData$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mPersonId:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataStartPosition:I

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mPersonId:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataStartPosition:I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mUid:Ljava/lang/String;

    const-string v0, "[HealthSensor]HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MeasurementData constructor, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mPersonId:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataStartPosition:I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mUid:Ljava/lang/String;

    iput p2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDeviceType:I

    const-string v0, "[HealthSensor]HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MeasurementData constructor, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", deviceType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILandroid/text/format/Time;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mPersonId:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataStartPosition:I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mUid:Ljava/lang/String;

    iput p2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDeviceType:I

    iput p3, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStampType:I

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const-string v0, "[HealthSensor]HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MeasurementData constructor, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private parsingMeasureData(Ljava/lang/String;)V
    .locals 10

    const/4 v9, 0x6

    const/4 v6, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    const-string v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    if-ge v0, v9, :cond_1

    const-string v0, "[HealthSensor]HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "parsingMeasureData, the data string is too short. measureData = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " len="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    const-string v0, "[HealthSensor]HealthService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "parsingMeasureData  CHECK! measureData :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v2, v6

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const-string v3, "[HealthSensor]HealthService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "parsingMeasureData  CHECK! userId :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v2, v6

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDeviceType:I

    const/4 v0, 0x5

    aget-object v0, v2, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    aget-object v0, v2, v9

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->setDateField(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    new-array v0, v0, [Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    if-ge v0, v3, :cond_0

    iget v3, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataStartPosition:I

    add-int/2addr v3, v0

    aget-object v3, v2, v3

    const-string v4, "[HealthSensor]HealthService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "parsingMeasureData value:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " j: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    new-instance v5, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    invoke-direct {v5}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;-><init>()V

    aput-object v5, v4, v0

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-string v4, "[HealthSensor]HealthService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "parsingMeasureData valueArray[0]:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " valueArray[1]: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " valueArray[2]: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    aget-object v4, v4, v0

    aget-object v5, v3, v1

    aget-object v6, v3, v7

    aget-object v3, v3, v8

    # invokes: Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v6, v3}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->access$000(Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private parsingMeasureData2(Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    const-string v0, "[HealthSensor]HealthService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "parsingMeasureData2 measureData:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    const-string v0, "[HealthSensor]HealthService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "parsingMeasureData2 mTotalDataSet:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v2, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->setDateField(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    const-string v0, "[HealthSensor]HealthService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "parsingMeasureData2 dataNum:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    new-array v0, v0, [Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    if-ge v0, v3, :cond_0

    add-int/lit8 v3, v0, 0x1

    aget-object v3, v2, v3

    const-string v4, "[HealthSensor]HealthService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "parsingMeasureData value:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " j: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    new-instance v5, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    invoke-direct {v5}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;-><init>()V

    aput-object v5, v4, v0

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-string v4, "[HealthSensor]HealthService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "parsingMeasureData valueArray[0]:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " valueArray[1]: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " valueArray[2]: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    aget-object v4, v4, v0

    aget-object v5, v3, v1

    aget-object v6, v3, v7

    aget-object v3, v3, v8

    # invokes: Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v6, v3}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->access$000(Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 7

    const-string v0, "[HealthSensor]HealthService"

    const-string/jumbo v1, "readFromParcel"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mUid:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDeviceType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStampType:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    if-nez v0, :cond_0

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-lez v1, :cond_1

    new-array v0, v1, [Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    new-instance v3, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    invoke-direct {v3}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    aget-object v2, v2, v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->set(IFLjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public addDataSet(IFI)V
    .locals 3

    new-instance v0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;-><init>()V

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->set(IFI)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDataSet()[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDeviceType:I

    return v0
.end method

.method public getPersonId()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mPersonId:I

    return v0
.end method

.method public getTimeStamp()Landroid/text/format/Time;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    return-object v0
.end method

.method public getTimeStampType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStampType:I

    return v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mUid:Ljava/lang/String;

    return-object v0
.end method

.method protected setDateField(Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x6

    const/4 v5, 0x0

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v1, v0, v5

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "[HealthSensor]HealthService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "This is time data!! setDateField date:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0x990

    if-ne v1, v2, :cond_0

    aget-object v1, v0, v5

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStampType:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->year:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v2, 0x2

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Landroid/text/format/Time;->month:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v2, 0x4

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->hour:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    const/4 v2, 0x5

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->minute:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    aget-object v0, v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Landroid/text/format/Time;->second:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataStartPosition:I

    const-string v0, "[HealthSensor]HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This is time data!! setDateField datalen:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    iput v6, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataStartPosition:I

    const-string v0, "[HealthSensor]HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No time stamp!! setDateField dataNum:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->dataNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setMeasuredDataField(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->parsingMeasureData(Ljava/lang/String;)V

    return-void
.end method

.method public setMeasuredDataField2(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->parsingMeasureData2(Ljava/lang/String;)V

    return-void
.end method

.method public setPersonId(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mPersonId:I

    return-void
.end method

.method public setTime(Landroid/text/format/Time;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mUid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDeviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStampType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->second:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTimeStamp:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mTotalDataSet:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mDataSet:[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData;->mMeasureDataArray:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
