.class Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onJoined status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    if-nez p1, :cond_2

    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "startReceivingData"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->initialize(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$600(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->deinitialize()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onLeft(I)V
    .locals 0

    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 0

    return-void
.end method

.method public onStateChanged(I)V
    .locals 0

    return-void
.end method
