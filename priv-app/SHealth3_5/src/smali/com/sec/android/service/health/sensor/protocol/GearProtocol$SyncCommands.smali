.class public Lcom/sec/android/service/health/sensor/protocol/GearProtocol$SyncCommands;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/protocol/GearProtocol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncCommands"
.end annotation


# static fields
.field public static CMD_START_SYNC:Ljava/lang/String;

.field public static CMD_START_SYNC_RSP:Ljava/lang/String;

.field public static CMD_STOP_SYNC:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "CMD_START_SYNC"

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$SyncCommands;->CMD_START_SYNC:Ljava/lang/String;

    const-string v0, "CMD_START_SYNC_RSP"

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$SyncCommands;->CMD_START_SYNC_RSP:Ljava/lang/String;

    const-string v0, "CMD_STOP_SYNC"

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$SyncCommands;->CMD_STOP_SYNC:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
