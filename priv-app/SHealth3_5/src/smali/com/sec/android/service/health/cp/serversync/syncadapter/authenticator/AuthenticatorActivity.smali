.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;
    }
.end annotation


# static fields
.field private static ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String; = null

.field private static final HEALTH_ACCOUNT_KEY:Ljava/lang/String; = "account"

.field private static final HEALTH_ACCOUNT_SHAREDPREF:Ljava/lang/String; = "health_account"

.field private static final SAMSUNG_ACCOUNT_DISABLED:I = 0xd

.field private static final SIGN_IN_ACCOUNT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AuthenticatorActivity"


# instance fields
.field private isSamsungAccountDisabled:Z

.field private mCallerpackage:Ljava/lang/String;

.field private mSamsungAccount:Landroid/accounts/Account;

.field private validPackNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "com.osp.app.signin"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.sec.android.service.health"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "com.sec.android.app.shealth"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.sec.android.app.shealth.plugins.sleepmonitor"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->validPackNames:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->isSamsungAccountDisabled:Z

    return-void
.end method

.method private SignUpSamsung()V
    .locals 4

    const/4 v3, 0x1

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    const-string v2, "1y90e30264"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    const-string v2, "80E7ECD9D301CB7888C73703639302E5"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "mypackage"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "MODE"

    const-string v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AuthenticatorActivity"

    const-string v1, "Samsung account disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->isSamsungAccountDisabled:Z

    goto :goto_0
.end method

.method private finishActivityWithResult(Z)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->handleHealthAccountSignIn(Ljava/lang/String;)V

    const-string v0, "accountAdded"

    invoke-virtual {v1, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-wide/16 v2, 0x2a30

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    const-string v5, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v4, v5, v7}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    const-string v5, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v4, v5, v7}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    const-string v5, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v4, v5, v0, v2, v3}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    const-string v0, "AuthenticatorActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addPeriodicSync time in sec = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    const-string v2, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0, v2}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "AuthenticatorActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPeriodicSyncs time in sec = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/PeriodicSync;

    iget-wide v4, v0, Landroid/content/PeriodicSync;->period:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->setResult(ILandroid/content/Intent;)V

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finish()V

    return-void

    :cond_1
    const-string v0, "accountAdded"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->isSamsungAccountDisabled:Z

    if-eqz v0, :cond_2

    iput-boolean v6, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->isSamsungAccountDisabled:Z

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->setResult(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->setResult(I)V

    goto :goto_0
.end method

.method private isNewHealthAccount(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_1

    const-string v1, "AuthenticatorActivity"

    const-string v2, "accountName is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "health_account"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "account"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, "AuthenticatorActivity"

    const-string v3, "No previous Account is found"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "account"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "AuthenticatorActivity"

    const-string v2, "New User"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isValidCaller(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->validPackNames:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private signInOrSignUpSamsung()V
    .locals 3

    const/4 v2, 0x1

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->SignUpSamsung()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->SignUpSamsung()V

    goto :goto_0
.end method


# virtual methods
.method public handleHealthAccountSignIn(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->isNewHealthAccount(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AuthenticatorActivity"

    const-string/jumbo v1, "new SHealth Acc user is signed in."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "AuthenticatorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Actovity results received."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/accounts/AccountAuthenticatorActivity;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p2, :cond_0

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    const-string v0, "AuthenticatorActivity"

    const-string v1, "Add Logs here"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    if-ne v1, v4, :cond_1

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mSamsungAccount:Landroid/accounts/Account;

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    goto :goto_0

    :cond_3
    if-ne p2, v4, :cond_4

    const-string v0, "AuthenticatorActivity"

    const-string v1, "Samsung account signin fails for 5 times"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finish()V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "AuthenticatorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCreate("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "packageName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mCallerpackage:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mCallerpackage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->mCallerpackage:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->isValidCaller(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->signInOrSignUpSamsung()V

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget v1, Lcom/sec/android/service/health/R$string;->health_service_name:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    return-void

    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;->finishActivityWithResult(Z)V

    goto :goto_0
.end method
