.class Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;->this$0:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "SHealthUpgrade(SHealthPackageManager)"

    const-string/jumbo v1, "packageInstalled()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;->this$0:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;

    # getter for: Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->access$000(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;)Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;->this$0:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;

    # getter for: Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->access$000(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;)Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;->packageInstalled(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
