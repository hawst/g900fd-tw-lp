.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol$Timeout;
    }
.end annotation


# instance fields
.field private CMD_GET_TIME:Ljava/lang/String;

.field private CMD_SET_TIME:Ljava/lang/String;

.field DEBUG:Z

.field private PARAM_TIME:Ljava/lang/String;

.field private final STATE_GET_TIME:I

.field private final STATE_NONE:I

.field private final STATE_SET_TIME:I

.field private final STX:I

.field private bReady:Z

.field private bufferData:[B

.field private crcTable:[I

.field private datasetSize:I

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mDeviceType:I

.field private mMsgHandler:Landroid/os/Handler;

.field private mSendQue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mTAG:Ljava/lang/String;

.field private final maxFragment:I

.field private mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

.field private multiTrans:Z

.field private presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

.field private state:I

.field private timer:Ljava/util/Timer;

.field private updatePacketCnt:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x80

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->STX:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v0, "HME"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->multiTrans:Z

    const/16 v0, 0x400

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->maxFragment:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    const-string v0, "CMD_GET_TIME"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->CMD_GET_TIME:Ljava/lang/String;

    const-string v0, "CMD_SET_TIME"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->CMD_SET_TIME:Ljava/lang/String;

    const-string v0, "PARAM_TIME"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->PARAM_TIME:Ljava/lang/String;

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->STATE_NONE:I

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->STATE_GET_TIME:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->STATE_SET_TIME:I

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const/16 v0, 0x100

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->crcTable:[I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol$1;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void

    :array_0
    .array-data 4
        0x0
        0xc0c1
        0xc181
        0x140
        0xc301
        0x3c0
        0x280
        0xc241
        0xc601
        0x6c0
        0x780
        0xc741
        0x500
        0xc5c1
        0xc481
        0x440
        0xcc01
        0xcc0
        0xd80
        0xcd41
        0xf00
        0xcfc1
        0xce81
        0xe40
        0xa00
        0xcac1
        0xcb81
        0xb40
        0xc901
        0x9c0
        0x880
        0xc841
        0xd801
        0x18c0
        0x1980
        0xd941
        0x1b00
        0xdbc1
        0xda81
        0x1a40
        0x1e00
        0xdec1
        0xdf81
        0x1f40
        0xdd01
        0x1dc0
        0x1c80
        0xdc41
        0x1400
        0xd4c1
        0xd581
        0x1540
        0xd701
        0x17c0
        0x1680
        0xd641
        0xd201
        0x12c0
        0x1380
        0xd341
        0x1100
        0xd1c1
        0xd081
        0x1040
        0xf001
        0x30c0
        0x3180
        0xf141
        0x3300
        0xf3c1
        0xf281
        0x3240
        0x3600
        0xf6c1
        0xf781
        0x3740
        0xf501
        0x35c0
        0x3480
        0xf441
        0x3c00
        0xfcc1
        0xfd81
        0x3d40
        0xff01
        0x3fc0
        0x3e80
        0xfe41
        0xfa01
        0x3ac0
        0x3b80
        0xfb41
        0x3900
        0xf9c1
        0xf881
        0x3840
        0x2800
        0xe8c1
        0xe981
        0x2940
        0xeb01
        0x2bc0
        0x2a80
        0xea41
        0xee01
        0x2ec0
        0x2f80
        0xef41
        0x2d00
        0xedc1
        0xec81
        0x2c40
        0xe401
        0x24c0
        0x2580
        0xe541
        0x2700
        0xe7c1
        0xe681
        0x2640
        0x2200
        0xe2c1
        0xe381
        0x2340
        0xe101
        0x21c0
        0x2080
        0xe041
        0xa001
        0x60c0
        0x6180
        0xa141
        0x6300
        0xa3c1
        0xa281
        0x6240
        0x6600
        0xa6c1
        0xa781
        0x6740
        0xa501
        0x65c0
        0x6480
        0xa441
        0x6c00
        0xacc1
        0xad81
        0x6d40
        0xaf01
        0x6fc0
        0x6e80
        0xae41
        0xaa01
        0x6ac0
        0x6b80
        0xab41
        0x6900
        0xa9c1
        0xa881
        0x6840
        0x7800
        0xb8c1
        0xb981
        0x7940
        0xbb01
        0x7bc0
        0x7a80
        0xba41
        0xbe01
        0x7ec0
        0x7f80
        0xbf41
        0x7d00
        0xbdc1
        0xbc81
        0x7c40
        0xb401
        0x74c0
        0x7580
        0xb541
        0x7700
        0xb7c1
        0xb681
        0x7640
        0x7200
        0xb2c1
        0xb381
        0x7340
        0xb101
        0x71c0
        0x7080
        0xb041
        0x5000
        0x90c1
        0x9181
        0x5140
        0x9301
        0x53c0
        0x5280
        0x9241
        0x9601
        0x56c0
        0x5780
        0x9741
        0x5500
        0x95c1
        0x9481
        0x5440
        0x9c01
        0x5cc0
        0x5d80
        0x9d41
        0x5f00
        0x9fc1
        0x9e81
        0x5e40
        0x5a00
        0x9ac1
        0x9b81
        0x5b40
        0x9901
        0x59c0
        0x5880
        0x9841
        0x8801
        0x48c0
        0x4980
        0x8941
        0x4b00
        0x8bc1
        0x8a81
        0x4a40
        0x4e00
        0x8ec1
        0x8f81
        0x4f40
        0x8d01
        0x4dc0
        0x4c80
        0x8c41
        0x4400
        0x84c1
        0x8581
        0x4540
        0x8701
        0x47c0
        0x4680
        0x8641
        0x8201
        0x42c0
        0x4380
        0x8341
        0x4100
        0x81c1
        0x8081
        0x4040
    .end array-data
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->errorProcess(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method private calCRC([BIII)I
    .locals 4

    move v0, p2

    :goto_0
    add-int v1, p3, p2

    if-ge v0, v1, :cond_0

    aget-byte v1, p1, v0

    ushr-int/lit8 v2, p4, 0x8

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->crcTable:[I

    xor-int/2addr v1, p4

    and-int/lit16 v1, v1, 0xff

    aget v1, v3, v1

    xor-int p4, v2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return p4
.end method

.method private calCRC([BII)[B
    .locals 6

    const/4 v2, 0x0

    move v0, p2

    move v1, v2

    :goto_0
    add-int v3, p3, p2

    if-ge v0, v3, :cond_0

    aget-byte v3, p1, v0

    ushr-int/lit8 v4, v1, 0x8

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->crcTable:[I

    xor-int/2addr v1, v3

    and-int/lit16 v1, v1, 0xff

    aget v1, v5, v1

    xor-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "%04x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method private checkCRC([B[B[B)Z
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    array-length v2, p2

    if-eqz p3, :cond_6

    array-length v3, p3

    add-int/2addr v2, v3

    move v4, v2

    :goto_0
    new-array v5, v4, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    array-length v6, p2

    invoke-static {p2, v2, v5, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-eqz p3, :cond_0

    const/4 v2, 0x0

    array-length v3, p2

    array-length v6, p3

    invoke-static {p3, v2, v5, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    move v2, v0

    move v3, v0

    :goto_1
    if-ge v2, v4, :cond_1

    aget-byte v6, v5, v2

    ushr-int/lit8 v7, v3, 0x8

    iget-object v8, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->crcTable:[I

    xor-int/2addr v3, v6

    and-int/lit16 v3, v3, 0xff

    aget v3, v8, v3

    xor-int/2addr v3, v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>([B)V

    const/16 v4, 0x10

    invoke-static {v2, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v3, v4, :cond_5

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, p2}, Ljava/lang/String;-><init>([B)V

    const/16 v2, 0x64

    move v1, v0

    :goto_2
    sget-object v6, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    array-length v6, v6

    if-ge v1, v6, :cond_4

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v7, v7, v1

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v5, v6, v7, v8, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v6, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_3

    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Check CRC Fail!! (Received:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", Calculator:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    iput v1, v3, Landroid/os/Message;->what:I

    iput-object v2, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_4
    return v0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v4, v2

    goto/16 :goto_0
.end method

.method private checkUpdateAck(I[B)Z
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v1, p2, v2, v3}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x2

    array-length v4, p2

    add-int/lit8 v4, v4, -0x2

    invoke-direct {v2, p2, v3, v4}, Ljava/lang/String;-><init>([BII)V

    const-string v3, "ACK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendNextPacket(I)V

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->updatePacketCnt:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_0
.end method

.method private clearPacket(I)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    iget v4, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mCmd:I

    if-ne v4, p1, :cond_3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v0, v1

    :goto_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v0, :cond_4

    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Queue Size ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method private connectionLost()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol connectionLost"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bReady:Z

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "CancelTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "connectionLost"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectionLost() Exception :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createAgentInfoReport()V
    .locals 0

    return-void
.end method

.method private errorProcess(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RFComProtocol errorProcess "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->isMaxRetry()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol Retry expire"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->endConnection()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->increaseRetryCount()V

    iget-boolean v1, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mSetTimer:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([B)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([BZ)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method private getDeviceType()Z
    .locals 3

    const/4 v2, 0x3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makeSimplePacket(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[B)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V

    const/4 v0, 0x1

    return v0
.end method

.method private getMeasurementData()Z
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-array v1, v4, [B

    const/4 v2, 0x0

    const/16 v3, 0x41

    aput-byte v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "FFFFFFFFFFFFFF"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makePacket(Ljava/lang/String;Ljava/util/ArrayList;)[B

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-direct {v1, v5, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[B)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V

    return v4
.end method

.method private getNextPacket()[B
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol getNextPacket bufferData is NULL, so returning NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v1, v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol getNextPacket bufferData length is then 6, so returning NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    const/4 v3, 0x2

    const/4 v4, 0x4

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v2, v1, 0x6

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v1, v1

    if-ge v2, v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol getNextPacket bufferData length is then 6, so returning NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-array v1, v2, [B

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    invoke-static {v3, v5, v1, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v3, v3

    sub-int/2addr v3, v2

    if-nez v3, :cond_3

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v3, "RFComProtocol getNextPacket bufferData bufferData.length-totalPacketSize == 0"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v0, v0

    sub-int/2addr v0, v2

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v0, v0

    sub-int/2addr v0, v2

    new-array v0, v0, [B

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v4, v4

    sub-int/2addr v4, v2

    invoke-static {v3, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RFComProtocol getNextPacket bufferData bufferData.length-totalPacketSize > 0, leftoutData.length = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v0, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private getTransMode()Z
    .locals 3

    const/16 v2, 0x1e

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "getTransMode"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makeSimplePacket(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[B)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V

    const/4 v0, 0x1

    return v0
.end method

.method private isPacketReady([B)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RFComProtocol isPacketReady Length Of Data Received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :cond_0
    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RFComProtocol isPacketReady combined the data buffers new data Length "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol isPacketReady Buffer Data is NULL so returning"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v1, v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol isPacketReady bufferData.length <= 6, so returning FALSE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    const/4 v3, 0x2

    const/4 v4, 0x4

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v1, v1, 0x6

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RFComProtocol isPacketReady total Packet size required is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    array-length v2, v2

    if-gt v1, v2, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol isPacketReady totalPacketSize <= bufferData.length. got the required Data, so sending TRUE  "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol isPacketReady returning False"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private makePacket(Ljava/lang/String;Ljava/util/ArrayList;)[B
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<[B>;)[B"
        }
    .end annotation

    const/4 v2, 0x0

    const/16 v3, 0xf

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v0, v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    new-array v4, v1, [B

    add-int/2addr v3, v1

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v6, v0

    invoke-static {v0, v2, v4, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v0, v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    new-array v0, v3, [B

    add-int/lit8 v1, v3, -0x6

    invoke-static {p1, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->toByteHeader(Ljava/lang/String;I)[B

    move-result-object v1

    array-length v3, v1

    invoke-static {v1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v1

    add-int/2addr v1, v2

    array-length v3, v4

    invoke-static {v4, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v3, v4

    add-int/2addr v1, v3

    const/4 v3, 0x6

    array-length v4, v4

    add-int/lit8 v4, v4, 0x5

    invoke-direct {p0, v0, v3, v4}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->calCRC([BII)[B

    move-result-object v3

    array-length v4, v3

    invoke-static {v3, v2, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method private makeSimplePacket(Ljava/lang/String;)[B
    .locals 5

    const/4 v4, 0x0

    const/16 v0, 0xf

    new-array v0, v0, [B

    const/16 v1, 0x9

    invoke-static {p1, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->toByteHeader(Ljava/lang/String;I)[B

    move-result-object v1

    array-length v2, v1

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v1

    add-int/2addr v1, v4

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->calCRC([BII)[B

    move-result-object v2

    array-length v3, v2

    invoke-static {v2, v4, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method private parseErrorList([B)V
    .locals 16

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->multiTrans:Z

    if-eqz v3, :cond_2

    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2, v3}, Ljava/lang/String;-><init>([BII)V

    const/4 v2, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    move v1, v2

    :goto_0
    const/4 v2, 0x0

    move v9, v2

    :goto_1
    if-ge v9, v8, :cond_0

    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v1, v3}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x4

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v1, v4}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x2

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v1, v5}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x2

    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v1, v6}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x2

    new-instance v6, Ljava/lang/String;

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-direct {v6, v0, v1, v7}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x2

    new-instance v7, Ljava/lang/String;

    const/4 v10, 0x2

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v1, v10}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x2

    new-instance v12, Ljava/lang/String;

    const/4 v10, 0x2

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v1, v10}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x2

    new-instance v13, Ljava/lang/String;

    const/4 v10, 0x2

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v1, v10}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v1, v1, 0x2

    new-instance v14, Ljava/lang/String;

    const/4 v10, 0x2

    move-object/from16 v0, p1

    invoke-direct {v14, v0, v1, v10}, Ljava/lang/String;-><init>([BII)V

    add-int/lit8 v10, v1, 0x2

    new-instance v15, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ErrorData;

    invoke-direct {v15}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ErrorData;-><init>()V

    new-instance v1, Ljava/util/Date;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit16 v2, v2, -0x76c

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct/range {v1 .. v7}, Ljava/util/Date;-><init>(IIIIII)V

    iput-object v1, v15, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ErrorData;->date:Ljava/util/Date;

    iput-object v12, v15, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ErrorData;->errClass:Ljava/lang/String;

    iput-object v13, v15, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ErrorData;->errType:Ljava/lang/String;

    iput-object v14, v15, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ErrorData;->errCode:Ljava/lang/String;

    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move v1, v10

    goto/16 :goto_1

    :cond_0
    move-object/from16 v0, p1

    array-length v2, v0

    sub-int/2addr v2, v1

    new-instance v3, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v1, v2}, Ljava/lang/String;-><init>([BII)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;

    invoke-direct {v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    iput v2, v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->type:I

    iput-object v3, v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    iput-object v11, v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    const/16 v3, 0x27

    iput v3, v2, Landroid/os/Message;->what:I

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_2
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    move v8, v1

    move v1, v2

    goto/16 :goto_0
.end method

.method private parseMeasurementData1([B)V
    .locals 9

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v8, 0x2

    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget-byte v2, p1, v0

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    aput-byte v1, p1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, v4, v4}, Ljava/lang/String;-><init>([BII)V

    new-instance v2, Ljava/lang/String;

    const/16 v3, 0x8

    invoke-direct {v2, p1, v3, v8}, Ljava/lang/String;-><init>([BII)V

    new-instance v3, Ljava/lang/String;

    const/16 v4, 0xa

    invoke-direct {v3, p1, v4, v8}, Ljava/lang/String;-><init>([BII)V

    new-instance v4, Ljava/lang/String;

    const/16 v5, 0xc

    invoke-direct {v4, p1, v5, v8}, Ljava/lang/String;-><init>([BII)V

    new-instance v5, Ljava/lang/String;

    const/16 v6, 0xe

    invoke-direct {v5, p1, v6, v8}, Ljava/lang/String;-><init>([BII)V

    new-instance v6, Ljava/lang/String;

    const/16 v7, 0x10

    invoke-direct {v6, p1, v7, v8}, Ljava/lang/String;-><init>([BII)V

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v7, Landroid/text/format/Time;->year:I

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v7, Landroid/text/format/Time;->month:I

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v7, Landroid/text/format/Time;->monthDay:I

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v7, Landroid/text/format/Time;->hour:I

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v7, Landroid/text/format/Time;->minute:I

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v7, Landroid/text/format/Time;->second:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "parseMeasurementData1 mDeviceType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_1
    const/16 v0, 0x1a

    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x5

    invoke-direct {v2, p1, v0, v3}, Ljava/lang/String;-><init>([BII)V

    new-instance v0, Lcom/sec/android/service/health/sensor/data/MeasurementData;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1011

    invoke-direct {v0, v3, v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v7}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->setTime(Landroid/text/format/Time;)V

    const/16 v3, 0x71b8

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    const/16 v5, 0x852

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->addDataSet(IFI)V

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;-><init>()V

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucose:F

    const v2, 0x222e1

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, v7}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->time:J

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private processReceiver(Ljava/lang/String;[B)V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x3

    const/4 v1, 0x0

    const/16 v2, 0x64

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RFComProtocol processReceiver command ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_1

    aget-byte v3, p2, v0

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    aput-byte v1, p2, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    :try_start_0
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_29

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v4, v4, v0

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v3, v4, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RFComProtocol processReceiver command matches to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v0

    :goto_2
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bReady:Z

    if-nez v0, :cond_2

    if-nez v7, :cond_2

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    const-string v2, "ACK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bReady:Z

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->createAgentInfoReport()V

    :cond_2
    :goto_3
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bReady:Z

    if-nez v0, :cond_6

    :cond_3
    :goto_4
    :sswitch_0
    return-void

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->errorProcess(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "processReceiver() Exception :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "processReceiver cmd ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;

    invoke-direct {v8}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;-><init>()V

    sparse-switch v7, :sswitch_data_0

    :cond_7
    :goto_5
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v7, v0, Landroid/os/Message;->what:I

    iput-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_8
    const/16 v0, 0x64

    if-eq v7, v0, :cond_9

    if-eq v7, v10, :cond_9

    invoke-direct {p0, v7}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendNextPacket(I)V

    :cond_9
    const/4 v0, 0x6

    if-ne v7, v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol set Device Time Command response has arrived "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iget v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    if-ne v0, v10, :cond_27

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol set Device Time Command ACK Received"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto/16 :goto_4

    :sswitch_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    const/4 v2, 0x0

    iput v2, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :goto_6
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_a

    iput v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :sswitch_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_b

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->connectionLost()V

    goto/16 :goto_4

    :cond_b
    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    const/4 v2, 0x0

    iput v2, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :goto_7
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_c

    iput v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :sswitch_4
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p2, v1, v2}, Ljava/lang/String;-><init>([BII)V

    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-direct {v1, p2, v2, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    iput v0, v2, Landroid/text/format/Time;->hour:I

    iput v1, v2, Landroid/text/format/Time;->minute:I

    iput-object v2, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    goto/16 :goto_5

    :sswitch_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    goto/16 :goto_5

    :sswitch_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    goto/16 :goto_5

    :sswitch_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p2}, Ljava/lang/String;-><init>([B)V

    move v0, v1

    :goto_8
    const/16 v3, 0x8

    if-ge v1, v3, :cond_e

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/DeviceTypeIFName;->DEVICE_TYPE_NAME:[Ljava/lang/String;

    aget-object v3, v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v4, v3, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_d

    move v0, v1

    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_e
    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_10

    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    :cond_f
    :goto_9
    iput-object v2, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    iput v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->type:I

    goto/16 :goto_5

    :cond_10
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    if-ne v0, v11, :cond_11

    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    goto :goto_9

    :cond_11
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_12

    const/16 v0, 0x28

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    goto :goto_9

    :cond_12
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    if-ne v0, v10, :cond_13

    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    goto :goto_9

    :cond_13
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    if-ne v0, v9, :cond_f

    const/16 v0, 0x1f

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    goto :goto_9

    :sswitch_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    :goto_a
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_14

    iput v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_15
    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    goto/16 :goto_5

    :sswitch_9
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_16

    const/4 v1, 0x3

    iput v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :cond_16
    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    goto/16 :goto_5

    :sswitch_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_17

    const/4 v1, 0x3

    iput v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :cond_17
    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    goto/16 :goto_5

    :sswitch_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_18

    const/4 v0, 0x3

    iput v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    goto/16 :goto_5

    :cond_18
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x5

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AutoConnectionSyncTime;

    invoke-direct {v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AutoConnectionSyncTime;-><init>()V

    const-string v4, "Y"

    invoke-virtual {v1, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_19

    const/4 v1, 0x1

    iput-boolean v1, v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AutoConnectionSyncTime;->set:Z

    :goto_b
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AutoConnectionSyncTime;->hour:I

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AutoConnectionSyncTime;->min:I

    iput-object v3, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    goto/16 :goto_5

    :cond_19
    const/4 v1, 0x0

    iput-boolean v1, v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AutoConnectionSyncTime;->set:Z

    goto :goto_b

    :sswitch_c
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1a

    const/4 v1, 0x3

    iput v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :goto_c
    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    goto/16 :goto_5

    :cond_1a
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1b

    const/4 v1, 0x1

    iput-boolean v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->set:Z

    goto :goto_c

    :cond_1b
    const/4 v1, 0x0

    iput-boolean v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->set:Z

    goto :goto_c

    :sswitch_d
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1c

    const/4 v1, 0x3

    iput v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    :goto_d
    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->result:Ljava/lang/String;

    goto/16 :goto_5

    :cond_1c
    const-string v1, "M"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1d

    const/4 v1, 0x1

    iput-boolean v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->set:Z

    goto :goto_d

    :cond_1d
    const/4 v1, 0x0

    iput-boolean v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->set:Z

    goto :goto_d

    :sswitch_e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1e

    const/4 v0, 0x3

    iput v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    const/16 v2, 0x3e8

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto/16 :goto_5

    :cond_1e
    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    const/16 v4, 0x8

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x8

    const/16 v5, 0xa

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xa

    const/16 v6, 0xc

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0xc

    const/16 v9, 0xe

    invoke-virtual {v0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Ljava/util/Date;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v1, v1, -0x76c

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ljava/util/Date;-><init>(IIIIII)V

    iput-object v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v2, :cond_7

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->PARAM_TIME:Ljava/lang/String;

    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setResponse(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto/16 :goto_5

    :sswitch_f
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1f

    const/4 v0, 0x3

    iput v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    goto/16 :goto_5

    :cond_1f
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [B

    const/4 v4, 0x0

    const/4 v5, -0x1

    aput-byte v5, v2, v4

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_20

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_20
    const/4 v2, -0x1

    :goto_e
    sget-object v4, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Unit;->UNIT_NAME:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_28

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Unit;->UNIT_NAME:[Ljava/lang/String;

    aget-object v4, v4, v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v4, v5, v0, v6, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_21

    move v0, v1

    :goto_f
    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/UnitData;

    invoke-direct {v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/UnitData;-><init>()V

    iput-object v3, v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/UnitData;->metric:Ljava/lang/String;

    iput v0, v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/UnitData;->unit:I

    iput-object v1, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    goto/16 :goto_5

    :cond_21
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :sswitch_10
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_22

    const/4 v0, 0x3

    iput v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    goto/16 :goto_5

    :cond_22
    new-instance v0, Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, p2, v2, v3}, Ljava/lang/String;-><init>([BII)V

    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x2

    invoke-direct {v2, p2, v3, v4}, Ljava/lang/String;-><init>([BII)V

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-direct {v3, p2, v4, v5}, Ljava/lang/String;-><init>([BII)V

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x5

    const/4 v6, 0x2

    invoke-direct {v4, p2, v5, v6}, Ljava/lang/String;-><init>([BII)V

    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x7

    const/4 v9, 0x7

    invoke-direct {v5, p2, v6, v9}, Ljava/lang/String;-><init>([BII)V

    new-instance v6, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;

    invoke-direct {v6}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->setIndex(I)V

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->setAlarmStatus(Z)V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v0, :cond_23

    if-eqz v2, :cond_23

    if-nez v3, :cond_24

    :cond_23
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->setAlarmStatus(Z)V

    :goto_10
    iput-object v6, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    goto/16 :goto_5

    :cond_24
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v4, Landroid/text/format/Time;->hour:I

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v4, Landroid/text/format/Time;->minute:I

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v4, Landroid/text/format/Time;->second:I

    invoke-virtual {v6, v4}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->setTime(Landroid/text/format/Time;)V

    const/4 v0, 0x7

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    :goto_11
    const/4 v2, 0x7

    if-ge v1, v2, :cond_26

    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x59

    if-ne v2, v3, :cond_25

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_25
    add-int/lit8 v1, v1, 0x1

    goto :goto_11

    :cond_26
    invoke-virtual {v6, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->setAlarmDays([Z)V

    goto :goto_10

    :sswitch_11
    invoke-direct {p0, v7, p2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->checkUpdateAck(I[B)Z

    goto/16 :goto_5

    :sswitch_12
    array-length v0, p2

    if-lez v0, :cond_7

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x3

    iput v0, v8, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->ack:I

    goto/16 :goto_5

    :cond_27
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol set Device Time resulted in an error"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    const/16 v2, 0x3f0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    :cond_28
    move v0, v2

    goto/16 :goto_f

    :cond_29
    move v7, v2

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_3
        0x2 -> :sswitch_5
        0x3 -> :sswitch_7
        0x5 -> :sswitch_e
        0x6 -> :sswitch_2
        0x7 -> :sswitch_4
        0x8 -> :sswitch_2
        0x9 -> :sswitch_6
        0x14 -> :sswitch_9
        0x15 -> :sswitch_a
        0x16 -> :sswitch_a
        0x17 -> :sswitch_a
        0x18 -> :sswitch_f
        0x19 -> :sswitch_8
        0x1a -> :sswitch_b
        0x1b -> :sswitch_8
        0x1c -> :sswitch_c
        0x1d -> :sswitch_8
        0x1e -> :sswitch_d
        0x1f -> :sswitch_8
        0x20 -> :sswitch_10
        0x21 -> :sswitch_8
        0x22 -> :sswitch_8
        0x23 -> :sswitch_8
        0x24 -> :sswitch_1
        0x25 -> :sswitch_1
        0x26 -> :sswitch_8
        0x27 -> :sswitch_12
        0x28 -> :sswitch_1
        0x29 -> :sswitch_11
        0x64 -> :sswitch_0
    .end sparse-switch

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private sendGetSensorTime()Z
    .locals 3

    const/4 v2, 0x5

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makeSimplePacket(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[B)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V

    const/4 v0, 0x1

    return v0
.end method

.method private sendNextPacket(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    iget v0, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mCmd:I

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->increaseRetryCount()V

    iget-boolean v1, v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mSetTimer:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([B)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([BZ)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method private sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;Z)V

    return-void
.end method

.method private sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getCmd()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getCmd()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    iget-boolean v0, p1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mSetTimer:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->increaseRetryCount()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([B)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([BZ)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->increaseRetryCount()V

    iget-boolean v0, p1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mSetTimer:Z

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([B)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->getPacket()[B

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([BZ)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private sendPowerOff(Z)Z
    .locals 3

    const/16 v2, 0x28

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makeSimplePacket(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-direct {v1, v2, v0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[BZ)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V

    const/4 v0, 0x0

    return v0
.end method

.method private sendSetSensorTime(Ljava/util/Date;)Z
    .locals 13

    const/4 v12, 0x1

    const/4 v11, 0x6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/Date;->getYear()I

    move-result v1

    add-int/lit16 v1, v1, 0x76c

    invoke-virtual {p1}, Ljava/util/Date;->getMonth()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1}, Ljava/util/Date;->getDate()I

    move-result v3

    invoke-virtual {p1}, Ljava/util/Date;->getHours()I

    move-result v4

    invoke-virtual {p1}, Ljava/util/Date;->getMinutes()I

    move-result v5

    invoke-virtual {p1}, Ljava/util/Date;->getSeconds()I

    move-result v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    const-string v8, "%04d%02d%02d%02d%02d%02d"

    new-array v9, v11, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v12

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v1

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v1, v1, v11

    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makePacket(Ljava/lang/String;Ljava/util/ArrayList;)[B

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-direct {v1, v11, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[B)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V

    return v12
.end method

.method private startCommunication()Z
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makeSimplePacket(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[B)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;)V

    const/4 v0, 0x1

    return v0
.end method

.method private write([B)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([BZ)V

    return-void
.end method

.method private write([BZ)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->sendRawData([B)I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->connectionLost()V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "write() Exception :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p2, :cond_1

    :try_start_1
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol$Timeout;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol$Timeout;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol$1;)V

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    const-wide/32 v2, 0x5b8d80

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "write() Exception :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public deinitialize()V
    .locals 0

    return-void
.end method

.method public endConnection()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol endConnection"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makeSimplePacket(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->write([BZ)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "CancelTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    :cond_0
    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocolName()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 1

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    const/4 v0, 0x0

    return v0
.end method

.method public notifyRawDataReceived([B)I
    .locals 15

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    invoke-direct/range {p0 .. p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->isPacketReady([B)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol notifyRawDataReceived NextPacket isPacketReady returned False, so returning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->getNextPacket()[B

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol notifyRawDataReceived NextPacket is NULL, sor returning "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :try_start_0
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    const/16 v2, 0x80

    if-ne v0, v2, :cond_10

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol notifyRawDataReceived First Byte is STX"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v0

    sget-byte v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->ReceiveFlow:B

    if-ne v0, v2, :cond_10

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol notifyRawDataReceived Second Byte is ReceiveFlow"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0x9

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v3, "RFComProtocol notifyRawDataReceived Canceling the Timer"

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    :cond_2
    new-array v3, v2, [B

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    sub-int v4, v2, v0

    invoke-virtual {v5, v3, v0, v4}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_3
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->getPackeSize([B)I

    move-result v6

    const/4 v0, 0x4

    const/4 v2, 0x5

    new-array v7, v2, [B

    const/4 v2, 0x0

    const/4 v4, 0x5

    invoke-static {v3, v0, v7, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v7}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->getCommand([B)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RFComProtocol notifyRawDataReceived Command Received is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v0, v0, v2

    invoke-virtual {v8, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComProtocol notifyRawDataReceived Command Received is GET_MEASUREMENT_DATA"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-direct {p0, v7, v0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->calCRC([BIII)I

    move-result v0

    add-int/lit8 v2, v6, -0x5

    add-int/lit8 v6, v2, -0x4

    const/4 v2, 0x0

    if-lez v6, :cond_26

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v3, "RFComProtocol notifyRawDataReceived Multi Mode"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->multiTrans:Z

    if-eqz v3, :cond_8

    const/4 v3, 0x4

    new-array v3, v3, [B

    const/4 v4, 0x0

    const/4 v7, 0x4

    invoke-virtual {v5, v3, v4, v7}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v4

    if-lez v4, :cond_7

    new-instance v4, Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v9, 0x4

    invoke-direct {v4, v3, v7, v9}, Ljava/lang/String;-><init>([BII)V

    const/16 v3, 0x10

    invoke-static {v4, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    :goto_2
    add-int/lit8 v4, v3, -0x1

    if-lez v3, :cond_7

    iget v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    new-array v7, v3, [B

    const/4 v3, 0x0

    :goto_3
    iget v9, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    if-ge v3, v9, :cond_4

    iget v9, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    sub-int/2addr v9, v3

    invoke-virtual {v5, v7, v3, v9}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v9

    add-int/2addr v3, v9

    goto :goto_3

    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v3, :cond_6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    array-length v10, v7

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v10, :cond_5

    aget-byte v11, v7, v3

    const-string v12, "%02X "

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v11}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    aput-object v11, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "RFComProtocol read bytes : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v3, 0x0

    iget v9, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->datasetSize:I

    invoke-direct {p0, v7, v3, v9, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->calCRC([BIII)I

    move-result v0

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v4

    goto :goto_2

    :cond_7
    move-object v3, v1

    move-object v1, v2

    move v2, v0

    :goto_5
    const/4 v0, 0x4

    new-array v4, v0, [B

    const/4 v0, 0x0

    :goto_6
    const/4 v7, 0x4

    if-ge v0, v7, :cond_b

    rsub-int/lit8 v7, v0, 0x4

    invoke-virtual {v5, v4, v0, v7}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v7

    add-int/2addr v0, v7

    goto :goto_6

    :cond_8
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v4, "RFComProtocol notifyRawDataReceived Not Multi Mode"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-array v4, v6, [B

    const/4 v3, 0x0

    :goto_7
    if-ge v3, v6, :cond_9

    sub-int v7, v6, v3

    invoke-virtual {v5, v4, v3, v7}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v7

    add-int/2addr v3, v7

    goto :goto_7

    :cond_9
    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v3, :cond_a

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>([B)V

    :cond_a
    const/4 v3, 0x0

    invoke-direct {p0, v4, v3, v6, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->calCRC([BIII)I

    move-result v0

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v1

    move-object v1, v2

    move v2, v0

    goto :goto_5

    :cond_b
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v0, :cond_d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    array-length v7, v4

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v7, :cond_c

    aget-byte v9, v4, v0

    const-string v10, "%C"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v9}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    aput-object v9, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RFComProtocol Read Command :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", Body :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", Crc :"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    const/16 v5, 0x10

    invoke-static {v3, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v2, :cond_11

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComprotocol Sending NACK"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "NACK"

    :goto_9
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v8, v4}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makePacket(Ljava/lang/String;Ljava/util/ArrayList;)[B

    move-result-object v1

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v1, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[BZ)V

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->clearPacket(I)V

    const/4 v1, 0x1

    invoke-direct {p0, v2, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;Z)V

    if-nez v0, :cond_e

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendNextPacket(I)V

    :cond_e
    if-gtz v6, :cond_10

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v0, :cond_f

    const-string v0, "No Body"

    :cond_f
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol onFinishDataTransfer : onFinishDataTransfer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    :cond_10
    :goto_a
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_11
    if-lez v6, :cond_12

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->parseMeasurementData(Ljava/util/List;)V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v2, "RFComprotocol Sending ACK"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ACK"

    goto :goto_9

    :cond_12
    const-string v1, "ACK"

    goto :goto_9

    :cond_13
    const/4 v0, 0x0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    const/16 v3, 0x27

    aget-object v2, v2, v3

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    const/16 v9, 0x27

    aget-object v4, v4, v9

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v8, v0, v2, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string/jumbo v1, "notifyRawDataReceived Command Received is ERROR_LIST"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v6, -0x5

    add-int/lit8 v3, v0, -0x4

    const/4 v0, 0x0

    if-lez v3, :cond_25

    new-array v0, v3, [B

    const/4 v1, 0x0

    :goto_b
    if-ge v1, v3, :cond_14

    sub-int v2, v3, v1

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_b

    :cond_14
    move-object v2, v0

    :goto_c
    const/4 v0, 0x4

    new-array v4, v0, [B

    const/4 v0, 0x0

    :goto_d
    const/4 v1, 0x4

    if-ge v0, v1, :cond_15

    rsub-int/lit8 v1, v0, 0x4

    invoke-virtual {v5, v4, v0, v1}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_d

    :cond_15
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v0, :cond_17

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    array-length v5, v4

    const/4 v0, 0x0

    :goto_e
    if-ge v0, v5, :cond_16

    aget-byte v6, v4, v0

    const-string v9, "%C"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_16
    if-lez v3, :cond_19

    new-instance v0, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v0, v2, v5, v3}, Ljava/lang/String;-><init>([BII)V

    :goto_f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Read Command :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Body :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", Crc :"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17
    invoke-direct {p0, v4, v7, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->checkCRC([B[B[B)Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v1, v1, v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v3, v1, :cond_18

    new-instance v1, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3}, Ljava/lang/String;-><init>([BII)V

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_18

    const/4 v0, 0x1

    :cond_18
    if-eqz v0, :cond_1a

    invoke-direct {p0, v8, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->processReceiver(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_a

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->connectionLost()V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "run() Exception :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :cond_19
    :try_start_1
    const-string v0, "No Body"

    goto :goto_f

    :cond_1a
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    if-lez v3, :cond_1b

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->parseErrorList([B)V

    const/4 v0, 0x1

    :cond_1b
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    const/16 v3, 0x27

    aget-object v2, v2, v3

    invoke-direct {p0, v2, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->makePacket(Ljava/lang/String;Ljava/util/ArrayList;)[B

    move-result-object v1

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;

    const/16 v3, 0x27

    invoke-direct {v2, v3, v1, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;-><init>(I[BZ)V

    const/16 v1, 0x27

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->clearPacket(I)V

    const/4 v1, 0x1

    invoke-direct {p0, v2, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPacket(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;Z)V

    if-nez v0, :cond_10

    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendNextPacket(I)V

    goto/16 :goto_a

    :cond_1c
    invoke-direct {p0, v8}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->errorProcess(Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_1d
    if-lez v6, :cond_10

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string/jumbo v2, "notifyRawDataReceived Processing the Command"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    add-int/lit8 v0, v6, -0x5

    new-array v9, v0, [B

    move v4, v0

    :goto_10
    if-lez v4, :cond_20

    invoke-virtual {v5, v9, v2, v4}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v10

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v0, :cond_1f

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    move-object v3, v0

    move v0, v2

    :goto_11
    add-int v11, v10, v2

    if-ge v0, v11, :cond_1e

    const-string v11, "%02X "

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aget-byte v14, v9, v0

    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_1e
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "read bytes : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, "(offset :"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, ", read :"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, ", expect length :"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, ")"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1f
    add-int/2addr v2, v10

    sub-int v0, v4, v10

    move v4, v0

    goto :goto_10

    :cond_20
    const/4 v0, 0x0

    add-int/lit8 v2, v6, -0x5

    add-int/lit8 v2, v2, -0x4

    new-array v3, v2, [B

    const/4 v4, 0x0

    invoke-static {v9, v0, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int v4, v0, v2

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v0, :cond_24

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    move-object v2, v0

    :goto_12
    const/4 v0, 0x4

    new-array v5, v0, [B

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {v9, v4, v5, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->DEBUG:Z

    if-eqz v0, :cond_22

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    const/4 v0, 0x0

    :goto_13
    const/4 v4, 0x4

    if-ge v0, v4, :cond_21

    const-string v4, "%C"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    aget-byte v10, v5, v0

    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v10

    aput-object v10, v6, v9

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    :cond_21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read Command :"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", Body :"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", Crc :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RFComProtocol "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_22
    invoke-direct {p0, v5, v7, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->checkCRC([B[B[B)Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "notifyRawDataReceived CheckCRC Received is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_23

    invoke-direct {p0, v8, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->processReceiver(Ljava/lang/String;[B)V

    goto/16 :goto_a

    :cond_23
    invoke-direct {p0, v8}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->errorProcess(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_a

    :cond_24
    move-object v2, v1

    goto/16 :goto_12

    :cond_25
    move-object v2, v0

    goto/16 :goto_c

    :cond_26
    move-object v3, v1

    move-object v1, v2

    move v2, v0

    goto/16 :goto_5
.end method

.method public notifyStart()I
    .locals 6

    const/16 v5, 0x3e8

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->startCommunication()Z

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->CMD_GET_TIME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendGetSensorTime()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol notifyState STATE_GET_TIME returned ERROR"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v1, v5, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->PARAM_TIME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendSetSensorTime(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComProtocol notifyState STATE_SET_TIME returned ERROR"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v1, v5, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mlistener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStarted(I)V

    :cond_3
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->getDeviceType()Z

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->getTransMode()Z

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->getMeasurementData()Z

    goto :goto_0
.end method

.method public notifyStop()I
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iput-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->bufferData:[B

    iput-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mSendQue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string v1, "RFComprotocol notifyStop CancelTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->timer:Ljava/util/Timer;

    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->sendPowerOff(Z)Z

    return v2
.end method

.method parseMeasurementData(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "parseMeasurementData() datas size :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->parseMeasurementData1([B)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method parseMeasurementData([B)Z
    .locals 12

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x4

    :try_start_0
    new-instance v10, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;

    invoke-direct {v10}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;-><init>()V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    iput v0, v10, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->type:I

    move v0, v7

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-byte v1, p1, v0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    aput-byte v1, p1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x4

    invoke-direct {v1, p1, v0, v2}, Ljava/lang/String;-><init>([BII)V

    const/16 v0, 0x8

    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x2

    invoke-direct {v2, p1, v0, v3}, Ljava/lang/String;-><init>([BII)V

    const/16 v0, 0xa

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x2

    invoke-direct {v3, p1, v0, v4}, Ljava/lang/String;-><init>([BII)V

    const/16 v0, 0xc

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x2

    invoke-direct {v4, p1, v0, v5}, Ljava/lang/String;-><init>([BII)V

    const/16 v0, 0xe

    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x2

    invoke-direct {v5, p1, v0, v6}, Ljava/lang/String;-><init>([BII)V

    const/16 v0, 0x10

    new-instance v6, Ljava/lang/String;

    const/4 v11, 0x2

    invoke-direct {v6, p1, v0, v11}, Ljava/lang/String;-><init>([BII)V

    const/16 v11, 0x12

    new-instance v0, Ljava/util/Date;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v1, v1, -0x76c

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ljava/util/Date;-><init>(IIIIII)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "parseMeasurementData() deviceType :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mDeviceType:I

    packed-switch v1, :pswitch_data_0

    move v0, v7

    :goto_1
    return v0

    :pswitch_0
    new-instance v1, Ljava/lang/String;

    const/16 v2, 0x8

    invoke-direct {v1, p1, v11, v2}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x1a

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x5

    invoke-direct {v3, p1, v2, v4}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x1f

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x2

    invoke-direct {v4, p1, v2, v5}, Ljava/lang/String;-><init>([BII)V

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/GlucoseData;

    invoke-direct {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/GlucoseData;-><init>()V

    iput-object v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/GlucoseData;->date:Ljava/util/Date;

    iput-object v1, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/GlucoseData;->Unit:Ljava/lang/String;

    iput-object v4, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/GlucoseData;->flag:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/GlucoseData;->value:F

    iput-object v2, v10, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    :goto_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    const-string/jumbo v1, "parseMeasurementData() sendMessage ()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v9, v0, Landroid/os/Message;->what:I

    iput-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mMsgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    :goto_3
    move v0, v8

    goto :goto_1

    :pswitch_1
    new-instance v1, Ljava/lang/String;

    const/16 v2, 0x8

    invoke-direct {v1, p1, v11, v2}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x1a

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x3

    invoke-direct {v3, p1, v2, v4}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x1d

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x3

    invoke-direct {v4, p1, v2, v5}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x20

    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x3

    invoke-direct {v5, p1, v2, v6}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x23

    new-instance v6, Ljava/lang/String;

    const/4 v7, 0x3

    invoke-direct {v6, p1, v2, v7}, Ljava/lang/String;-><init>([BII)V

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;

    invoke-direct {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;-><init>()V

    iput-object v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;->date:Ljava/util/Date;

    iput-object v1, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;->Unit:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;->Systolic:I

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;->Diastolic:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;->MeanArterial:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_4
    :try_start_2
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;->PulseRate:I

    iput-object v2, v10, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "parseMeasurementData() Exception :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/BloodPressureData;->MeanArterial:I

    goto :goto_4

    :pswitch_2
    new-instance v1, Ljava/lang/String;

    const/16 v2, 0x8

    invoke-direct {v1, p1, v11, v2}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x1a

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x3

    invoke-direct {v3, p1, v2, v4}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x1d

    new-instance v4, Ljava/lang/String;

    const/16 v5, 0x8

    invoke-direct {v4, p1, v2, v5}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x25

    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x3

    invoke-direct {v5, p1, v2, v6}, Ljava/lang/String;-><init>([BII)V

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;

    invoke-direct {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;-><init>()V

    iput-object v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;->date:Ljava/util/Date;

    iput-object v1, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;->weiUnit:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;->weight:F

    const-string v0, ""

    iput-object v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;->heiUnit:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;->height:F
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    iput-object v4, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;->heiUnit:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/WeightData;->height:F
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    iput-object v2, v10, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    goto/16 :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->mTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "() Exception :"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :pswitch_3
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x3

    invoke-direct {v1, p1, v11, v2}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x15

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x3

    invoke-direct {v3, p1, v2, v4}, Ljava/lang/String;-><init>([BII)V

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/PulseOxygenData;

    invoke-direct {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/PulseOxygenData;-><init>()V

    iput-object v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/PulseOxygenData;->date:Ljava/util/Date;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/PulseOxygenData;->SPO2:I

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/PulseOxygenData;->PulseRate:I

    iput-object v2, v10, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;

    goto/16 :goto_2

    :pswitch_4
    new-instance v1, Ljava/lang/String;

    const/16 v2, 0x8

    invoke-direct {v1, p1, v11, v2}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x1a

    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x5

    invoke-direct {v3, p1, v2, v4}, Ljava/lang/String;-><init>([BII)V

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ThermData;

    invoke-direct {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ThermData;-><init>()V

    iput-object v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ThermData;->date:Ljava/util/Date;

    iput-object v1, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ThermData;->Unit:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/ThermData;->therm:F

    iput-object v2, v10, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/ResponseData;->obj:Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->CMD_GET_TIME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->CMD_SET_TIME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->state:I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;->presentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    goto :goto_0

    :cond_1
    const/16 v0, 0x3ea

    goto :goto_0
.end method
