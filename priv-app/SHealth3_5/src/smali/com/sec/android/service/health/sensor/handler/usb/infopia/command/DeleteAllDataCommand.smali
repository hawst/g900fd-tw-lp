.class public final Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]Infopia"


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mInfopiaProtocol:Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->mInfopiaProtocol:Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private createResponse(Z)V
    .locals 4

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    const-string v1, "[HealthSensor]Infopia"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[DeleteAllDataCommand]  createResponse() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "RES_CLEAR_DATA"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v1, "delete success"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->mInfopiaProtocol:Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->mInfopiaProtocol:Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->mInfopiaProtocol:Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v1, "delete failure"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]Infopia"

    const-string v1, "Protocol listener is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public doAction(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    .locals 2

    const-string v0, "[HealthSensor]Infopia"

    const-string v1, "[DeleteAllDataCommand]  doAction()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->ERS:[B

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendDataForStartSession([BLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method public responseReceived([B)V
    .locals 6

    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const-string v2, "[HealthSensor]Infopia"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[DeleteAllDataCommand]  responseReceived() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    array-length v2, p1

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->ERS:[B

    aget-byte v2, v2, v1

    aget-byte v3, p1, v1

    if-ne v2, v3, :cond_1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->ERS:[B

    aget-byte v2, v2, v0

    aget-byte v3, p1, v0

    if-ne v2, v3, :cond_1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->ERS:[B

    aget-byte v2, v2, v5

    aget-byte v3, p1, v5

    if-ne v2, v3, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->createResponse(Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
