.class public Lcom/sec/android/service/health/cp/serversync/data/FitnessItem;
.super Ljava/lang/Object;


# instance fields
.field public appId:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:Ljava/lang/Long;

.field public deviceTimeZone:Ljava/lang/String;

.field public duration:I

.field public durationMilli:Ljava/lang/Long;

.field public grip:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public laterality:Ljava/lang/String;

.field public movement:Ljava/lang/String;

.field public position:Ljava/lang/String;

.field public repetitionCount:Ljava/lang/Integer;

.field public requestType:Ljava/lang/String;

.field public resistance:Ljava/lang/Integer;

.field public responseCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
