.class Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->requestAccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private subscribeToEvents()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeCalculatedPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$2;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeCalculatedTorqueEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$3;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$3;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeCalculatedCrankCadenceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$4;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->wheelCircumferenceInMeters:Ljava/math/BigDecimal;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$4;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;Ljava/math/BigDecimal;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeCalculatedWheelSpeedEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$5;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->wheelCircumferenceInMeters:Ljava/math/BigDecimal;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$5;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;Ljava/math/BigDecimal;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeCalculatedWheelDistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$6;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$6;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeInstantaneousCadenceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$7;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$7;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRawPowerOnlyDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$8;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$8;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribePedalPowerBalanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$9;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$9;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRawWheelTorqueDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$10;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$10;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRawCrankTorqueDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$11;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$11;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeTorqueEffectivenessEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$12;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$12;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribePedalSmoothnessEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$13;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$13;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRawCtfDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$14;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$14;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$15;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$15;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeAutoZeroStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$16;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$16;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeCrankParametersEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$17;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$17;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeMeasurementOutputDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->subscribeCommonToEvents()V

    return-void
.end method


# virtual methods
.method public onResultReceived(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$4;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$RequestAccessResult:[I

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "[HealthSensor]BPWRSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error. Unrecognized result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyOnJoinedError(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-static {v0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$002(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->setConnectState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyChannelStateChanged()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->state:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const-string v0, "[HealthSensor]BPWRSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onResultReceived goto disconnect "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mAntDeviceID:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    const/4 v1, 0x3

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->state:I
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->access$502(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;I)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->subscribeToEvents()V

    goto :goto_0

    :pswitch_1
    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "Error. Channel Not Available"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyOnJoinedError(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "Error. RequestAccess failed. See logcat for details."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyOnJoinedError(I)V

    goto :goto_0

    :pswitch_3
    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "Error. RequestAccess failed. See logcat for details."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "User Cancelled."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "Error. Upgrade Required?"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "[HealthSensor]BPWRSensor"

    const-string v1, "Error. Search timeout."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 0

    check-cast p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    return-void
.end method
