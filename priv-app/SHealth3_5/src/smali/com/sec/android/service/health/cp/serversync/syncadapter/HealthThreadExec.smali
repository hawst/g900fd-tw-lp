.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ExceptionObject;,
        Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ResponseGetMaintenanceNotice;,
        Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$RequestGetMaintenanceNotice;
    }
.end annotation


# static fields
.field public static final SERVER_MAINTENANCE_STATUS_CODE_NEGATIVE:Ljava/lang/String; = "NA"

.field public static final SERVER_MAINTENANCE_STATUS_CODE_POSITIVE:Ljava/lang/String; = "AV"

.field public static final SERVER_STATUS_GET_MAINTENANCE_NOTICE:I = 0x4e23

.field private static TAG:Ljava/lang/String;

.field protected static mGson:Lcom/google/gson/Gson;


# instance fields
.field private IS_EXECUTION_IN_SERIAL:Z

.field private mBudleExtra:Landroid/os/Bundle;

.field private mContext:Landroid/content/Context;

.field private mCurProgPercent:I

.field private mErrorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsErrorDuringExercise:Z

.field private mNumTotalTasks:I

.field private mRequestType:I

.field private mRequestedDataTypebyApp:I

.field private mTasksList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalTasksWeight:J

.field private mbOnStartedCalled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "HealthThreadExec"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->IS_EXECUTION_IN_SERIAL:Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    iput-boolean v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mbOnStartedCalled:Z

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestedDataTypebyApp:I

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mNumTotalTasks:I

    iput-boolean v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mIsErrorDuringExercise:Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "HealthThreadExec()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mErrorList:Ljava/util/ArrayList;

    iput-boolean v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mbOnStartedCalled:Z

    iput-boolean v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mIsErrorDuringExercise:Z

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestType:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestedDataTypebyApp:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->addTempTasks(II)V

    return-void
.end method

.method private addTempTasks(II)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addTempTasks(): requestType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". dataType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x1869c

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->deleteUserData(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addTempTasks(): mCurProgPercent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mNumTotalTasks:I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->callExecution()V

    return-void

    :cond_0
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserGoalTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioBloodGlucoseTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioBloodPressureTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioWeightTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioBodyTemperatureTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioHeartRateTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioECGTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioPOTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioSkinTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUVTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUVProtectionTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncTemperatureHumidityTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseActivityTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseFitnessTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseWalkingTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseExtendedWalkInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncFirstBeatCoachingResultTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseRealtimeTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseLocationTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseManualTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseImageTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncFirstBeatCoachingTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncFoodNutrientsTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncMealTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncWaterTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncSleepTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncStressTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncTagTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioBloodGlucoseTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioBloodPressureTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserGoalTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioWeightTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioBodyTemperatureTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioHeartRateTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncTagTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioECGTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncTagTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioPOTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncBioSkinTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserGoalTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseActivityTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseFitnessTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseWalkingTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseExtendedWalkInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseRealtimeTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseLocationTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseManualTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncFirstBeatCoachingResultTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseImageTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncFirstBeatCoachingTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserGoalTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseWalkingTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncExerciseExtendedWalkInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserGoalTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncFoodNutrientsTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncMealTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserGoalTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncWaterTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncSleepTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncTagTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncStressTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncTagTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUVTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUVProtectionTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->syncTemperatureHumidityTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xb -> :sswitch_0
        0x66 -> :sswitch_2
        0x67 -> :sswitch_3
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6e -> :sswitch_8
        0x6f -> :sswitch_9
        0xc8 -> :sswitch_a
        0xc9 -> :sswitch_b
        0x12c -> :sswitch_d
        0x190 -> :sswitch_1
        0x1f5 -> :sswitch_e
        0x1f6 -> :sswitch_f
        0x259 -> :sswitch_10
        0x25a -> :sswitch_11
        0x25b -> :sswitch_12
        0x320 -> :sswitch_c
    .end sparse-switch
.end method

.method private callExecution()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "callExecution()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->IS_EXECUTION_IN_SERIAL:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "callExecution(): IS_EXECUTION_IN_SERIAL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->IS_EXECUTION_IN_SERIAL:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->start()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mbOnStartedCalled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, v3, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "callExecution(): Got out of memory error while starting new thread"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    const/4 v0, 0x4

    invoke-direct {p0, v0, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private cleanDataStructures()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "cleanDataStructures()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mErrorList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private deleteUserData(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 4

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    const v2, 0x1869c

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/DeleteUserDataTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/DeleteUserDataTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/DeleteUserDataTask;->getWeight()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private sendProgressBroadcast(I)V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendProgressBroadcast(): dataSyncType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.private.syncadapter.receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "DATA_SYNC_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "PROGRESS_STATUS"

    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "TimeStamp"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    const-string v3, "TimeStamp"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    const-string/jumbo v2, "packageName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_TYPE"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sendProgressBroadcast(): SENDING THE BROADCAST "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private sendStartedCallBack(ILjava/lang/String;)V
    .locals 5

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.private.syncadapter.receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "DATA_SYNC_TYPE"

    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestedDataTypebyApp:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "ERROR_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    const-string v2, "TimeStamp"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    const-string/jumbo v4, "packageName"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "TimeStamp"

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sendStartedCallBack() before sending broadcast"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mbOnStartedCalled:Z

    return-void
.end method

.method private syncBioBloodGlucoseTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBloodGluccoseTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBloodGluccoseTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBloodGluccoseTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncBioBloodPressureTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBloodPressureTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBloodPressureTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBloodPressureTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncBioBodyTemperatureTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBodyTemperatureTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBodyTemperatureTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioBodyTemperatureTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncBioECGTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioECGTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioECGTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioECGTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncBioHeartRateTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioHeartRateTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioHeartRateTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioHeartRateTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncBioPOTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioPulseOximeterTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioPulseOximeterTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioPulseOximeterTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncBioSkinTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioSkinTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioSkinTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioSkinTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncBioWeightTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioWeightTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioWeightTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BioWeightTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseActivityTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseActivityTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseActivityTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseActivityTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseExtendedWalkInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseExtendedWalkInfoTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseExtendedWalkInfoTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseExtendedWalkInfoTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseFitnessTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseFitnessTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseFitnessTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseFitnessTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseImageTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseImageTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseImageTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseImageTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseInfoTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseInfoTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseInfoTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseInfoTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseLocationTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseLocationTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseLocationTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseLocationTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseManualTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseRealtimeTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseRealtimeTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseRealtimeTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;III)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseRealtimeTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncExerciseWalkingTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseWalkingTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseWalkingTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseWalkingTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncFirstBeatCoachingResultTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FirstBeatCoachingResultTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FirstBeatCoachingResultTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FirstBeatCoachingResultTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncFirstBeatCoachingTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FirstBeatCoachingTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FirstBeatCoachingTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FirstBeatCoachingTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncFoodNutrientsTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FoodNutrientsTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FoodNutrientsTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/FoodNutrientsTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncMealTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/MealTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/MealTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/MealTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncProfileTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ProfileTask;

    invoke-direct {v1, v0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ProfileTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ProfileTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncSleepTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/SleepTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/SleepTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/SleepTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncStressTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/StressTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/StressTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/StressTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncTagTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TagTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TagTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;III)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TagTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncTemperatureHumidityTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TemperatureHumidityTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TemperatureHumidityTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TemperatureHumidityTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncUVProtectionTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVProtectionTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVProtectionTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVProtectionTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncUVTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVTask;

    const/16 v2, -0x65

    invoke-direct {v1, v0, p1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncUserDevice(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UserDeviceTask;

    invoke-direct {v1, v0, p1, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UserDeviceTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UserDeviceTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncUserGoalTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UserGoalTask;

    invoke-direct {v1, v0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UserGoalTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UserGoalTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method

.method private syncWaterTask(II)Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
    .locals 6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/WaterTask;

    invoke-direct {v1, v0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/WaterTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/WaterTask;->getWeight()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    return-object v1
.end method


# virtual methods
.method public cancelTask()V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "cancelTask()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->IS_EXECUTION_IN_SERIAL:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelTask(): IS_EXECUTION_IN_SERIAL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->IS_EXECUTION_IN_SERIAL:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->cancelTask()V

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Canceled the task"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " NAME : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "cancelTask(): No tasks to cancel"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public errorFoundInExecuting()I
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v2, "errorFoundInExecuting()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mErrorList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "errorFoundInExecuting(): Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mErrorList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mErrorList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v2, "errorFoundInExecuting(): Error none"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "getBundle()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    return-object v0
.end method

.method public getMaintenanceNotice(II)V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v1, "getMaintenanceNotice()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$RequestGetMaintenanceNotice;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$RequestGetMaintenanceNotice;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$RequestGetMaintenanceNotice;->languageCode:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntDateFormat(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$RequestGetMaintenanceNotice;->dateFormat:I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntTimeFormat(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$RequestGetMaintenanceNotice;->timeFormat:I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mGson:Lcom/google/gson/Gson;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v0

    const/16 v2, 0x4e23

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_MAINTENANCE_NOTICE:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mGson:Lcom/google/gson/Gson;

    invoke-virtual {v4, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;

    invoke-direct {v4, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)V

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->execute(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)I

    return-void
.end method

.method public isExerciseSyncError()Z
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isExerciseSyncError(): Error occurred during exercise sync: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mIsErrorDuringExercise:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mIsErrorDuringExercise:Z

    return v0
.end method

.method public onErrorDuringExerciseSync()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onErrorDuringExerciseSync(): Error occurred during exercise sync"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mIsErrorDuringExercise:Z

    return-void
.end method

.method public onFinished(II)V
    .locals 8

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onFinished(): dataSyncType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mErrorList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/16 v0, 0x8

    if-eq p2, v0, :cond_1

    const/4 v0, 0x5

    if-ne p2, v0, :cond_2

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onFinished(): remove all tasks"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onFinished(): inside synchronized"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->IS_EXECUTION_IN_SERIAL:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onFinished(): inside synchronized: IS_EXECUTION_IN_SERIAL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->IS_EXECUTION_IN_SERIAL:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x9

    if-ne p2, v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onFinished(): inside synchronized: error: cancelled"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    :goto_0
    return-void

    :cond_3
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestType:I

    const v2, 0x1869e

    if-ne v0, v2, :cond_4

    if-eqz p2, :cond_4

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onFinished(): inside synchronized: error occurred during restore. Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " call notify"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onFinished(): BEFORE CALULCATING THE  PERCENTAGE mCurProgPercent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_7

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    const/16 v2, 0x64

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mNumTotalTasks:I

    div-int/2addr v2, v3

    add-int/2addr v0, v2

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onFinished: mCurProgPercent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendProgressBroadcast(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onFinished(): start first task in queue"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->start()V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_6
    :goto_2
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :cond_7
    :try_start_6
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_8

    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_7
    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onFinished: IN EXCEPTION mCurProgPercent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :cond_8
    :try_start_8
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I

    int-to-long v2, v0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getWeight()J

    move-result-wide v4

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTotalTasksWeight:J

    div-long/2addr v4, v6

    add-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mCurProgPercent:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    :catch_1
    move-exception v0

    :try_start_9
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onFinished(): Got out of memory error while starting new thread"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mErrorList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mTasksList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mbOnStartedCalled:Z

    goto :goto_2

    :cond_9
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onFinished(): no tasks"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string v2, "NOTIFYING THE ADAPTER"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mbOnStartedCalled:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2
.end method

.method public onStarted(II)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted(): dataSyncType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Error in onStart - check we can coninue to next task or not ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mbOnStartedCalled:Z

    :cond_0
    return-void
.end method

.method public setBundle(Landroid/os/Bundle;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setBundle()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mBudleExtra:Landroid/os/Bundle;

    return-void
.end method

.method public startActualSync(II)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startActualSync(): requestType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". dataType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->cleanDataStructures()V

    const v0, 0x1869e

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startActualSync(): clear backup database"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/DatabaseUtility;->clearBackupDatabase(ZLcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    :cond_0
    const v0, 0x1869f

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startActualSync(): delete zero walkinfo values"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/DatabaseUtility;->clearZeroValuesForWalkinfo()V

    :cond_1
    iput p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestedDataTypebyApp:I

    iput p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestType:I

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->getMaintenanceNotice(II)V

    return-void
.end method
