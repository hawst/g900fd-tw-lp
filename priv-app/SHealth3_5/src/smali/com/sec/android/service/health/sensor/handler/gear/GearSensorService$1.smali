.class Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 7

    const/16 v6, 0x2724

    const/4 v2, 0x0

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onServiceConnected() error: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # invokes: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->isDeviceAvailable()Z
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Z

    move-result v0

    if-nez v0, :cond_1

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No gear device is available!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.action.SYNC_FAILED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->deinitialize()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$200(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    if-eqz v1, :cond_3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v3, "calling mDeviceFinder.getConnectedDeviceList"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$200(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    const/4 v3, 0x0

    const/16 v4, 0x2724

    const/4 v5, 0x5

    invoke-virtual {v1, v3, v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    if-ne v3, v6, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # setter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$302(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$402(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;Ljava/lang/String;)Ljava/lang/String;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "join for mDeviceId = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceId:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$500(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_8

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->deinitialize()V

    goto/16 :goto_0

    :cond_3
    :try_start_2
    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v3, "mDeviceFinder is null"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move v0, v2

    goto :goto_2

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v0, v2

    goto :goto_2

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    move v0, v2

    goto :goto_2

    :catch_7
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    move v0, v2

    goto :goto_2

    :catch_8
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public onServiceDisconnected(I)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onServiceDisconnected() error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->deinitialize()V

    return-void
.end method
