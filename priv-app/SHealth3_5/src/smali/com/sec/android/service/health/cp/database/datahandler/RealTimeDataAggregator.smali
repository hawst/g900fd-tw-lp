.class public abstract Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field static final ACTIVITY_MONITOR_TABLE:Ljava/lang/String; = "activity_monitor"

.field public static final AGGREGATION_TIME_INTERVAL:I = 0x2710

.field static final REALTIME_DATA_TABLE:Ljava/lang/String; = "realtime_data"

.field static final SESSION_ID_KEY:Ljava/lang/String; = "session_id"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public mAppId:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field protected mCurrentActivityIdUri:Landroid/net/Uri;

.field protected mCurrentRealTimeIdUri:Landroid/net/Uri;

.field protected mCurrentTenSec:J

.field protected mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field protected mExerciseId:J

.field private mPreviousSessionId:I

.field protected mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mPreviousSessionId:I

    return-void
.end method

.method private addActivitySummeryEntry(J)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mExerciseId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "duration_min"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "duration_millisecond"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "start_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "end_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentActivityIdUri:Landroid/net/Uri;

    return-void
.end method

.method private insertNewRealtimeData()V
    .locals 6

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->getContentValues()Landroid/content/ContentValues;

    move-result-object v0

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sample_time"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentTenSec:J

    const-wide/16 v4, 0x2710

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentRealTimeIdUri:Landroid/net/Uri;

    return-void
.end method

.method private updateData()V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->getContentValues()Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Values not recieved from Sensor device"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->getActivityUpdate()Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-nez v1, :cond_2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Values not recieved from Sensor device"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->getExerciseUpdate()Landroid/content/ContentValues;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mExerciseId:J

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentRealTimeIdUri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentActivityIdUri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->checkMandatoryFields()Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    :goto_0
    return v1

    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mPreviousSessionId:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_1

    if-eqz p2, :cond_3

    const-string/jumbo v1, "session_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    :goto_1
    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mPreviousSessionId:I

    if-eq v4, v1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v5, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-direct {p0, v5, v6}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->addActivitySummeryEntry(J)V

    iput v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mPreviousSessionId:I

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    move-object v1, v0

    iget-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/16 v6, 0x2710

    div-long/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentTenSec:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/16 v6, 0x2710

    div-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentTenSec:J

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->resetAggregation()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->processData()V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->insertNewRealtimeData()V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->updateData()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v2

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->processData()V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->updateData()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v1

    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while updating db"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v3

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v1, v3

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    :cond_3
    move v4, v3

    goto :goto_1
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "addData[] not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract checkMandatoryFields()Z
.end method

.method public finalizeAggregation()V
    .locals 6

    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "end_time"

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentActivityIdUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->getExerciseUpdate()Landroid/content/ContentValues;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "end_time"

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mExerciseId:J

    invoke-static {v1, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Data is out of range"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method protected abstract getActivityUpdate()Landroid/content/ContentValues;
.end method

.method protected abstract getContentValues()Landroid/content/ContentValues;
.end method

.method protected getExerciseUpdate()Landroid/content/ContentValues;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mPreviousSessionId:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mCurrentTenSec:J

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mUserDeviceId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mExerciseId:J

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->resetInitialAggregation()V

    return-void
.end method

.method public isRunning(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mExerciseId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mExerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/RealTimeDataAggregator;->mExerciseId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "matched"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract processData()V
.end method

.method protected abstract resetAggregation()V
.end method

.method protected abstract resetInitialAggregation()V
.end method
