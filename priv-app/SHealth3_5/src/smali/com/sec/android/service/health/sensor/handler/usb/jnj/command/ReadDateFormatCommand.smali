.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# instance fields
.field private dateFormat:Ljava/lang/String;

.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->dateFormat:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private getDataFormat([B)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x2

    new-array v1, v3, [B

    array-length v2, v1

    invoke-static {p1, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_DATE_FORMAT:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    aget-byte v1, p1, v3

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const-string v0, "EU"

    goto :goto_0

    :cond_2
    aget-byte v1, p1, v3

    if-nez v1, :cond_0

    const-string v0, "US"

    goto :goto_0
.end method


# virtual methods
.method public doAction()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->READ_DATE_FORMAT:[B

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method protected getDateFormat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->dateFormat:Ljava/lang/String;

    return-object v0
.end method

.method public responseReceived([B)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->getDataFormat([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->dateFormat:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendAck()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readTotalNumberCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V

    return-void
.end method
