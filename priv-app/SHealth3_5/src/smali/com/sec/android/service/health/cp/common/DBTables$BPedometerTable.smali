.class public Lcom/sec/android/service/health/cp/common/DBTables$BPedometerTable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BPedometerTable"
.end annotation


# static fields
.field public static final DB_INDEX:Ljava/lang/String; = "db_index"

.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_DISTANCE:Ljava/lang/String; = "distance"

.field public static final KEY_KCAL:Ljava/lang/String; = "kcal"

.field public static final KEY_KIND_OF_WALKING:Ljava/lang/String; = "kind_of_walking"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_RUN_STEP:Ljava/lang/String; = "run_steps"

.field public static final KEY_SPEED:Ljava/lang/String; = "walk_speed"

.field public static final KEY_TOTAL_STEP:Ljava/lang/String; = "total_steps"

.field public static final KEY_UPDOWN_STEP:Ljava/lang/String; = "updown_steps"

.field public static final KEY_WALK_STEP:Ljava/lang/String; = "walk_steps"

.field public static final TABLE_NAME:Ljava/lang/String; = "b_pedometer"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_walk_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_walk_trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
