.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Unit;
.super Ljava/lang/Object;


# static fields
.field public static final BP_KPA:I = 0x7

.field public static final BP_MMHG:I = 0x6

.field public static final GLU_MG:I = 0x0

.field public static final GLU_MMOL:I = 0x1

.field public static final HEIGHT_CM:I = 0x4

.field public static final HEIGTH_INCH:I = 0x5

.field public static final THERM_DEDC:I = 0x8

.field public static final THERM_FAHR:I = 0x9

.field public static final UNIT_NAME:[Ljava/lang/String;

.field public static final WEIGHT_KG:I = 0x2

.field public static final WEIGHT_LB:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "mg/dL"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "mmol/L"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "kg"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "lb"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "centi"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "inch"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "mmHg"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "kPa"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dedc"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "fahr"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Unit;->UNIT_NAME:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
