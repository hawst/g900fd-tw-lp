.class public interface abstract Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
.super Ljava/lang/Object;


# static fields
.field public static final TASK_CMD_NOT_SUPPORTED:I = -0x1

.field public static final TASK_DB_IS_INITIALIZED:I = 0x4

.field public static final TASK_EXCEPTION:I = 0x1

.field public static final TASK_INVALID_ARGUMENT:I = 0x3

.field public static final TASK_PRE_CONDITION_ERR:I = 0x2

.field public static final TASK_SUCCESS:I


# virtual methods
.method public abstract onCancelled()V
.end method

.method public abstract onFinished(Ljava/lang/Integer;)V
.end method

.method public abstract onProgressUpdate(Ljava/lang/Integer;)V
.end method
