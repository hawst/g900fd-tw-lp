.class public Lcom/sec/android/service/health/sensor/protocol/GearProtocol;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/protocol/GearProtocol$SyncCommands;
    }
.end annotation


# static fields
.field private static final DEVICE_NAME_GEAR:Ljava/lang/String; = "GALAXY Gear"

.field public static final DUPLEX_SYNC_LOGIC_REV:I = 0xf423f

.field public static final NOTIFY_START:I = 0x1

.field public static final NOTIFY_STOP:I = 0x0

.field private static final PEDOMETER_ACTION_REQ_SYNC:Ljava/lang/String; = "com.sec.android.app.pedometer.REQ_SYNC"

.field private static final SHEALTH_ACTION_REQ_SYNC_FAIL:Ljava/lang/String; = "com.sec.android.app.shealth.RSP_SYNC_FAIL"

.field private static final SHEALTH_ACTION_RSP_SYNC:Ljava/lang/String; = "com.sec.android.app.shealth.RSP_SYNC"

.field private static final SHEALTH_ACTION_SAP_CONNECT_STATE:Ljava/lang/String; = "com.sec.android.app.shealth.SAP_CONNECT_STATE"

.field private static final STATE_IDLE:I = 0x2

.field private static final STATE_PROCESSING_CMD:I = 0x1

.field private static final STATE_PROCESSING_REQ:I = 0x0

.field public static final TAG:Ljava/lang/String;

.field private static final TIME_OUT:I = 0x7530


# instance fields
.field private bWatingStaus:Z

.field private lastSyncTimeTemp:J

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field public mContext:Landroid/content/Context;

.field private mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

.field private mCurrentState:I

.field public mData:Ljava/lang/Object;

.field public mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field public mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

.field private mRespTimer:Ljava/util/Timer;

.field private mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

.field public sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$1;-><init>(Lcom/sec/android/service/health/sensor/protocol/GearProtocol;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->bWatingStaus:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/protocol/GearProtocol;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    return-void
.end method

.method private noti_stop(Z)V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v3, "[+] noti_stop"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->stopTimer()V

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->bWatingStaus:Z

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v2, :cond_6

    iget v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v3, 0x5

    if-nez p1, :cond_1

    :goto_1
    invoke-interface {v2, v3, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    :goto_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] noti_stop"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    iget v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    if-ne v2, v1, :cond_5

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->stopData(Ljava/lang/String;ILjava/util/List;)Z

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    if-nez v2, :cond_3

    const-string v2, "INVALID_COMMAND"

    :goto_3
    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    if-nez p1, :cond_4

    :goto_4
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v3}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto :goto_2

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_RSP"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_4

    :cond_5
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "Invalid state reported !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "initialize not called."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private notifyStartLocal()I
    .locals 12

    const-wide/high16 v5, -0x8000000000000000L

    const/16 v7, 0x3e8

    const/4 v8, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] notifyStartLocal"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->getWearableLastSyncTime(ILjava/lang/String;)J

    move-result-wide v0

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getWearableLastSyncTime (type= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",id= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->bWatingStaus:Z

    if-eqz v2, :cond_1

    const/16 v0, 0x3ec

    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->bWatingStaus:Z

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v9, :cond_2

    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "startReceivingData type = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :cond_2
    :goto_1
    cmp-long v2, v0, v5

    if-eqz v2, :cond_9

    cmp-long v2, v0, v3

    if-gez v2, :cond_9

    const-wide/16 v5, 0x1

    add-long v1, v0, v5

    :goto_2
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "send PEDOMETER_ACTION_REQ_SYNC"

    invoke-static {v0, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v9, Landroid/content/Intent;

    const-string v0, "com.sec.android.app.pedometer.REQ_SYNC"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, "_"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->makeSyncReqToGear(Landroid/content/Context;JJZLjava/lang/String;)[B

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string/jumbo v0, "param"

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iput-wide v3, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->lastSyncTimeTemp:J

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->startTimer()V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Param String "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStarted(I)V

    :cond_4
    :goto_3
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] notifyStartLocal"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v8

    goto/16 :goto_0

    :pswitch_0
    const-string v9, "com.sec.android.app.shealth.RSP_SYNC"

    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v9, "com.sec.android.app.shealth.SAP_CONNECT_STATE"

    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v9, "com.sec.android.app.shealth.RSP_SYNC_FAIL"

    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v9, "com.sec.android.app.pedometer.REQ_SYNC"

    invoke-virtual {v2, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v9, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    invoke-direct {v9, p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;-><init>(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    if-eqz v9, :cond_5

    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "mWearableReceiver, registerReceiver"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    invoke-virtual {v9, v10, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_5
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v9, "Gear1"

    invoke-static {v2, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] notifyStart"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "Error doing new string"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v7

    if-eqz v7, :cond_7

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getExerciseId()J

    move-result-wide v5

    invoke-virtual {v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getProcessId()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->startData(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)Z

    :cond_7
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "No Need to notify end user !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_8
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "initialize not called."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_9
    move-wide v1, v5

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x2724
        :pswitch_0
    .end packed-switch
.end method

.method private parseJsonMsg(Lorg/json/JSONObject;)V
    .locals 16

    const-string v1, ""

    const-string v1, ""

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "parseJsonMsg"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string/jumbo v1, "responseMessage"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string/jumbo v1, "responseMessage"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :try_start_1
    const-string v1, "gearMessageRevision"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    move v13, v1

    :goto_0
    :try_start_2
    const-string/jumbo v1, "shealth-get-req"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onReceive() : CODE_REQ_SYNC_FROM_SHEALTH"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, "results"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    if-nez v14, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v2, "RSP_SYN Result is Null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    const/4 v1, 0x0

    move v13, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_a

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "##syncDataArray.length() is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v12, v1

    :goto_2
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v12, v1, :cond_3

    invoke-virtual {v14, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    const-string/jumbo v2, "timeCreated"

    invoke-virtual {v10, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string/jumbo v4, "totalStep"

    invoke-virtual {v10, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string/jumbo v5, "runSteps"

    invoke-virtual {v10, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string/jumbo v6, "walkSteps"

    invoke-virtual {v10, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string/jumbo v7, "updownSteps"

    invoke-virtual {v10, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v8, "calorie"

    invoke-virtual {v10, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    double-to-float v8, v8

    const/4 v9, 0x0

    const-string v11, "distance"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v10

    double-to-float v10, v10

    const/4 v11, 0x3

    invoke-direct/range {v1 .. v11}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;-><init>(JIIIIFFFI)V

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->lastSyncTimeTemp:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getKcal()F

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getDistance()F

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getRunSteps()I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getStep()I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getTime()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getUpDownSteps()I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getWalkSteps()I

    move-result v1

    iput v1, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "pedo_data[ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto/16 :goto_2

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v2, "lastSyncTimeTemp >= syncData.getTime() == false"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    goto/16 :goto_1

    :cond_3
    :try_start_3
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v3, v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    aput-object v1, v3, v2

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    :cond_4
    array-length v1, v3

    new-array v2, v1, [Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->getWearableLastSyncTime(ILjava/lang/String;)J

    move-result-wide v4

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWearableLastSyncTime (type= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",id= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_5
    array-length v6, v3

    if-ge v1, v6, :cond_5

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    aput-object v6, v2, v1

    aget-object v6, v2, v1

    const-string v7, "LAST_SYNC_TIME"

    invoke-virtual {v6, v7, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v1, :cond_9

    if-eqz v3, :cond_9

    array-length v1, v3

    if-lez v1, :cond_9

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    if-nez v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v1, v3, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    :cond_6
    :goto_6
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v2, "Gear Sync Data Received : pedo data OnDataReceived"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0xf423f

    if-ge v13, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->lastSyncTimeTemp:J

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->saveWearableLastSyncTime(ILjava/lang/String;J)V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "saveWearableLastSyncTime (type= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",id= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->lastSyncTimeTemp:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_7
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    goto/16 :goto_1

    :cond_8
    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v2, v4}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    goto/16 :goto_1

    :cond_9
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v2, "initialize not called."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_a
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v2, "RSP Gear Synced Data is Null. No Save"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :cond_b
    const-string/jumbo v1, "requestMessage"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "requestMessage"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "pedometer-sync-cmd"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onReceive() : CODE_CMD_SYNC_FROM_GEAR"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1
.end method

.method private startTimer()V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] startTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->stopTimer()V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mRespTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mRespTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$2;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$2;-><init>(Lcom/sec/android/service/health/sensor/protocol/GearProtocol;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] startTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private stopTimer()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] stopTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mRespTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mRespTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "Timer canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] stopTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->stopTimer()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocolName()Ljava/lang/String;
    .locals 1

    const-string v0, "GEAR"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mData:Ljava/lang/Object;

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public notifyRawDataReceived([B)I
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] notifyRawDataReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "notifyRawDataReceived: received buffer is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    const/16 v0, 0x3e8

    :goto_0
    return v0

    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "responseMessage"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->parseJsonMsg(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_1
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] notifyRawDataReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    goto :goto_1
.end method

.method public notifyStart()I
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "---- notifyStart --- Current State is Before start :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "---- notifyStart --- Current State is processing  :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->notifyStartLocal()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "---- notifyStart --- Current State is Before returning :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public notifyStop()I
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] notifyStop"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->noti_stop(Z)V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] notifyStop"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v2
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] request"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$SyncCommands;->CMD_START_SYNC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->mCurrentState:I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->notifyStartLocal()I

    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] request"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x3ea

    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol$SyncCommands;->CMD_STOP_SYNC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;->notifyStop()I

    goto :goto_0
.end method
