.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
.implements Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;
.implements Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;


# static fields
.field private static final CMD_GET_BATTERY_LEVEL:B = 0xbt

.field private static final CMD_GET_DATA_HRM:B = 0x1t

.field private static final CMD_GET_DEVICE_VER:B = 0x23t

.field private static final CMD_GET_SW_VER:B = 0x24t

.field private static final RSP_BATTERY_LEVEL:B = -0x75t

.field private static final RSP_DEVICE_VER:B = -0x5dt

.field private static final RSP_STREAMING_DATA:B = -0x7ft

.field private static final RSP_SW_VER:B = -0x5ct

.field public static final SAP_WRITE_MESSAGE_ERROR:I = 0x6d

.field public static final SAP_WRONG_DATA_FORMAT:I = 0x67


# instance fields
.field private TAG:Ljava/lang/String;

.field private appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

.field private device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

.field private mStmHRMArray:[B

.field private profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private reStart:Z

.field private sessionId:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->reStart:Z

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mStmHRMArray:[B

    return-void
.end method

.method public constructor <init>(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->reStart:Z

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mStmHRMArray:[B

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iput-wide p4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    return-void
.end method

.method private handleReceivedData([B)V
    .locals 5

    const/16 v4, 0xb

    const/16 v1, 0x9

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {v2, p1}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMUtils;->printRaw(I[B)V

    aget-byte v0, p1, v2

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mStmHRMArray:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([BB)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mStmHRMArray:[B

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mStmHRMArray:[B

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMUtils;->byteBufferToInt([B)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[HRM][RSP] RSP_STREAMING_DATA - [Samsung HRM] HRM : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;-><init>()V

    if-gez v0, :cond_0

    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    goto :goto_1

    :sswitch_1
    aget-byte v0, p1, v3

    invoke-static {v0}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[HRM][RSP] RSP_BATTERY_LEVEL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_2
    new-array v0, v4, [B

    invoke-static {p1, v3, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[HRM][RSP] RSP_DEVICE_VER : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_3
    new-array v0, v1, [B

    invoke-static {p1, v3, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[HRM][RSP] RSP_SW_VER : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f -> :sswitch_0
        -0x75 -> :sswitch_1
        -0x5d -> :sswitch_2
        -0x5c -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->reStart:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    return-void
.end method

.method public getBatteryLevel()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string v1, "[HRM][CMD] getBatteryLevel"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0xb

    aput-byte v2, v0, v1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->send(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J[B)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string v1, "[HRM][CMD] getBatteryLevel : mSAPAccManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getDeviceVersion()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string v1, "[HRM][CMD] getDeviceVirsion"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x23

    aput-byte v2, v0, v1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->send(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J[B)I

    return-void
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSwVersion()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string v1, "[HRM][CMD] getSwVersion"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x24

    aput-byte v2, v0, v1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->send(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J[B)I

    return-void
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 4

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz p4, :cond_0

    check-cast p4, Ljava/util/HashMap;

    const-string v0, "MANAGER"

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const-string v0, "DEVICE"

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    const-string v0, "SESSION"

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->registerDataListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const/4 v1, 0x4

    invoke-virtual {v0, p0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->registerEventListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;I)Z

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x6a

    goto :goto_0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onAccessoryAttached(J)V
    .locals 0

    return-void
.end method

.method public onAccessoryDetached(J)V
    .locals 0

    return-void
.end method

.method public onCloseSessionRequest(JJ)V
    .locals 0

    return-void
.end method

.method public onDeviceFound(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/String;IJJ)I
    .locals 3

    sparse-switch p2, :sswitch_data_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onError - Unknown Error So Returning "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v0, 0x0

    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_0
        0x6d -> :sswitch_0
    .end sparse-switch
.end method

.method public onError(III)V
    .locals 0

    return-void
.end method

.method public onOpenSessionAccepted(JJ)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onOpenSessionAccepted accessoryId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SessionId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v0, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->reStart:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onOpenSessionAccepted ReJoin is True so start Streaming of the Data"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-virtual {v0, p0, v1, p3, p4}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->registerDataListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->startStreamData()I

    :cond_0
    return-void
.end method

.method public onOpenSessionRequest(JJ)V
    .locals 0

    return-void
.end method

.method public onPairedStatus(IJZ)V
    .locals 0

    return-void
.end method

.method public onReceive([BJJ)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->handleReceivedData([B)V

    const/4 v0, 0x0

    return v0
.end method

.method public onSessionClosed(JJ)V
    .locals 0

    return-void
.end method

.method public onTransportStatusChanged(II)V
    .locals 0

    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/16 v0, 0x3ea

    return v0
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 7

    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string v3, "[HRM][CMD] startReceivingData"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-virtual {v2, v3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->isConnected(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "startReceivingData Device is not connected but start ReceivingData called so rejoin True"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->reStart:Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v1, v6, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->startStreamData()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "startReceivingData startStreamData status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v2, v1, :cond_1

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->reStart:Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v1, v6, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public startStreamData()I
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->TAG:Ljava/lang/String;

    const-string v1, "[HRM][CMD] startHRMStreamData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v2, [B

    const/4 v1, 0x0

    aput-byte v2, v0, v1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->appDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->sessionId:J

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->send(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J[B)I

    move-result v0

    return v0
.end method

.method public stopReceivingData()I
    .locals 3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->reStart:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;->profileListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    return v2
.end method
