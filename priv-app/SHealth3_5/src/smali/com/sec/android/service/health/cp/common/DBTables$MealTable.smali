.class public Lcom/sec/android/service/health/cp/common/DBTables$MealTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MealTable"
.end annotation


# static fields
.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_COMMENT:Ljava/lang/String; = "comment"

.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_DELYN:Ljava/lang/String; = "rec_delyn"

.field public static final KEY_KCAL:Ljava/lang/String; = "kcal"

.field public static final KEY_KIND:Ljava/lang/String; = "kind"

.field public static final KEY_MEAL_NAME:Ljava/lang/String; = "name"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final TABLE_NAME:Ljava/lang/String; = "meal"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_meal_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_meal_trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
