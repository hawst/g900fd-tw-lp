.class public Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/cp/common/ICompatibilityManager;


# static fields
.field private static final SqlCreateBluetooth_Devices_Table:Ljava/lang/String; = "create table bluetooth_devices ( _id INTEGER PRIMARY KEY, data_type INTEGER, sys_name TEXT, manufacture TEXT, def_web_site TEXT, user_name TEXT);"

.field private static final SqlCreateCignaTipTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS tip_instance_table (_id INTEGER PRIMARY KEY, tip_instance INTEGER);"

.field private static final SqlCreateExerciseInfoTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS exercise_info (_id INTEGER PRIMARY KEY,kcal REAL, deleted INTEGER, image TEXT, name TEXT, lang TEXT, my INTEGER, favorite INTEGER, pinyin TEXT, pinyin_sort TEXT, app_name TEXT);"

.field private static final SqlCreateExerciseTable_MR:Ljava/lang/String; = "CREATE TABLE exercise (_id INTEGER PRIMARY KEY,create_time INTEGER, min INTEGER, id_exercise_db INTEGER, calories REAL, comment TEXT, app_name TEXT);"

.field private static final SqlCreateFoodImagesTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS images_table (_id INTEGER PRIMARY KEY,file_path TEXT, measure_type_id INTEGER, measure_id INTEGER, app_name TEXT);"

.field private static final SqlCreateHealthboardTipsTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS healthboard_tips (_id INTEGER PRIMARY KEY AUTOINCREMENT, rule_id INTEGER,create_time INTEGER, hb_data_one TEXT, hb_data_two TEXT, article TEXT, boardcontentid TEXT, meta_data TEXT, day_st INTEGER, week_st_mon INTEGER, week_st_sun INTEGER, month_st INTEGER, year_st INTEGER);"

.field private static final SqlCreateKindOfFoodTake:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS food_info (_id INTEGER PRIMARY KEY,name TEXT, lang TEXT, kcal REAL, create_time INTEGER, image TEXT, my INTEGER, api_id INTEGER, deleted INTEGER,favorite INTEGER,pinyin TEXT,pinyin_sort TEXT,health_star INTEGER,app_name TEXT);"

.field private static final SqlCreateMealItemsTable_MR:Ljava/lang/String; = "CREATE TABLE meal_items (_id INTEGER PRIMARY KEY,id_meal_table INTEGER, id_food_info INTEGER, mark_number INTEGER, mark_posx REAL, mark_posy REAL, percent REAL,app_name TEXT);"

.field private static final SqlCreateMealTable_MR:Ljava/lang/String; = "CREATE TABLE meal (_id INTEGER PRIMARY KEY,create_time INTEGER, name TEXT, kcal REAL, kind INTEGER, comment TEXT,rec_delyn INTEGER,app_name TEXT);"

.field private static final SqlCreatePressureTable_MR:Ljava/lang/String; = "CREATE TABLE blood_pressure (_id INTEGER PRIMARY KEY, sensor_id INTEGER, person_id INTEGER, comment TEXT, systolic REAL, diastolic REAL, mean INTEGER, pulse INTEGER, app_name TEXT, create_time INTEGER);"

.field private static final SqlCreateRunningProTable_MR:Ljava/lang/String; = "CREATE TABLE running_pro (_id INTEGER PRIMARY KEY,create_time INTEGER, min INTEGER, id_exercise_db INTEGER, calories REAL, comment TEXT, app_name TEXT);"

.field private static final SqlCreateSensortable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS sensor (_id INTEGER PRIMARY KEY,type INTEGER, manufacture TEXT, modelnumber TEXT, version TEXT, name TEXT, connectivity INTEGER NOT NULL DEFAULT (0), app_name TEXT);"

.field private static final SqlCreateSugarTable_MR:Ljava/lang/String; = "CREATE TABLE blood_glucose (_id INTEGER PRIMARY KEY,sensor_id INTEGER, person_id INTEGER, comment TEXT, type INTEGER, value REAL, unit TEXT, app_name TEXT, create_time INTEGER);"

.field private static final SqlCreateViewWalkForLife:Ljava/lang/String; = "DROP VIEW IF EXISTS walk_for_life_view;CREATE VIEW walk_for_life_view AS SELECT create_time, day_st, max(total_steps) AS maxTotal, min(total_steps) AS minTotal, sum(total_steps) AS sumTotal, sum(run_steps) AS sumRun, sum(walk_steps) AS sumWalk, sum(updown_steps) AS sumUD, sum(kcal) AS sumKcal, sum(distance) AS sumDistance, max(walk_speed) AS maxSpeed, min(walk_speed) AS minSpeed, avg(walk_speed) AS avgSpeed, count(_id) AS count FROM (SELECT * FROM temp_walk_for_life UNION SELECT * FROM walk_for_life) GROUP BY day_st;"

.field private static final SqlCreateWeightTable_MR:Ljava/lang/String; = "CREATE TABLE weight (_id INTEGER PRIMARY KEY,sensor_id INTEGER, person_id INTEGER, comment TEXT, value REAL, app_name TEXT, bmi REAL, body_fat REAL, body_year REAL, height REAL, skeletal_muscle REAL, visceral_fat REAL, bmr REAL, create_time INTEGER);"

.field private static final TAG:Ljava/lang/String;

.field private static final UPGRADE3TO4_SQL:Ljava/lang/String; = "UPDATE3TO4"

.field private static final UPGRADE5TO6_STEP_1_SQL:Ljava/lang/String; = "UPDATE5TO6_STEP_1"

.field private static final UPGRADE5TO6_STEP_LAST_SQL:Ljava/lang/String; = "UPDATE5TO6_STEP_LAST"

.field private static final sqlCreateAntDeviceListTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS ant_device_list (_id INTEGER PRIMARY KEY, device_id INTEGER, device_type INTEGER, uuid TEXT, name TEXT);"

.field private static final sqlCreateBPedometerTable:Ljava/lang/String; = "create table b_pedometer ( _id INTEGER PRIMARY KEY, db_index INTEGER, create_time INTEGER, total_steps INTEGER, run_steps INTEGER, walk_steps INTEGER, updown_steps INTEGER, kcal REAL, walk_speed REAL, distance REAL, kind_of_walking INTEGER, day_st INTEGER, week_st_mon INTEGER, week_st_sun INTEGER, month_st INTEGER, year_st INTEGER);"

.field private static final sqlCreateExerciseDeleteTrigger:Ljava/lang/String; = "DROP TRIGGER IF EXISTS delete_exercise_trigger;\nCREATE TRIGGER delete_exercise_trigger DELETE ON exercise \n BEGIN\n   DELETE FROM map_path WHERE db_index = OLD._id;\n   DELETE FROM realtime_heartbeat WHERE db_index = OLD._id;\n   DELETE FROM realtime_speed WHERE db_index = OLD._id;\n   DELETE FROM training_load_peak WHERE db_index = OLD._id;\n   DELETE FROM b_pedometer WHERE db_index = OLD._id;\n END;"

.field private static final sqlCreateMapPathTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS map_path (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, latitude REAL, longitude REAL, altitude REAL, accuracy REAL);"

.field private static final sqlCreateMyBluetoothDeviceTable:Ljava/lang/String; = "create table my_bluetooth_devices ( _id INTEGER PRIMARY KEY, uuid TEXT, device_user_name TEXT);"

.field private static final sqlCreateRealtimeHeartbeatTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS realtime_heartbeat (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, bpm INTEGER);"

.field private static final sqlCreateRealtimeSpeedTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS realtime_speed (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, speed REAL);"

.field private static final sqlCreateTrainingLoadPeakTable:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS training_load_peak (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, training_load_peak INTEGER);"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

.field private mIsCPDatabasePresent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mIsCPDatabasePresent:Z

    iput-object p1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method private checkCPDatabasePresence()Z
    .locals 6

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/databases/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "secure_sec_health.db"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DB insert/update "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " values "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private readFromCP(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->checkCPDatabasePresence()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mIsCPDatabasePresent:Z

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mIsCPDatabasePresent:Z

    invoke-static {v0}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->setMode(Z)V

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$FoodInfoTable;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "food_info"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllFromTable(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {v1}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getFoodInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v2, "food_info"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {p1, v2, v3, v0, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    const-string v2, "food_info"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_0
    :try_start_1
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$MealTable;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v2, "meal"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllFromTable(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_3
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string/jumbo v2, "meal"

    const/4 v3, 0x0

    invoke-static {v1}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getMealContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllMealItemsContentValuesByMealId(IILandroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string/jumbo v3, "meal_items"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {p1, v3, v4, v0, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    const-string/jumbo v3, "meal_items"

    invoke-direct {p0, v3, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    :goto_2
    :try_start_3
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$ExerciseInfoTable;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "exercise_info"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllFromTable(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_5
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v2, "exercise_info"

    const/4 v3, 0x0

    invoke-static {v1}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getExerciseInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllExercisesContentValuesByExerciseInfo(IILandroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v3, "exercise"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {p1, v3, v4, v0, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    const-string v3, "exercise"

    invoke-direct {p0, v3, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_3

    :catch_1
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    :goto_4
    :try_start_5
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$WeightTable;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v2, "weight"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllFromTable(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    invoke-static {v1}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getWeightContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v0

    const-string/jumbo v2, "weight"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {p1, v2, v3, v0, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    const-string/jumbo v2, "weight"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result v0

    if-nez v0, :cond_7

    :cond_8
    if-eqz v1, :cond_9

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_9
    :goto_5
    :try_start_6
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$BloodGlucoseTable;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "blood_glucose"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllFromTable(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    invoke-static {v1}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getBloodGlucoseContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v2, "blood_glucose"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {p1, v2, v3, v0, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    const-string v2, "blood_glucose"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-result v0

    if-nez v0, :cond_a

    :cond_b
    if-eqz v1, :cond_c

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_c
    :goto_6
    :try_start_7
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$BloodPressureTable;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "blood_pressure"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getAllFromTable(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    invoke-static {v1}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getBloodPressureContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v2, "blood_pressure"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {p1, v2, v3, v0, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    const-string v2, "blood_pressure"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    move-result v0

    if-nez v0, :cond_d

    :cond_e
    if-eqz v1, :cond_f

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_f
    :goto_7
    return-void

    :catch_2
    move-exception v0

    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_10

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_10
    throw v0

    :cond_11
    :try_start_9
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    :cond_12
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_13

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_13
    throw v0

    :cond_14
    :try_start_a
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result v0

    if-nez v0, :cond_5

    :cond_15
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_4

    :catchall_2
    move-exception v0

    if-eqz v1, :cond_16

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_16
    throw v0

    :catch_3
    move-exception v0

    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    if-eqz v1, :cond_9

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_5

    :catchall_3
    move-exception v0

    if-eqz v1, :cond_17

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_17
    throw v0

    :catch_4
    move-exception v0

    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    if-eqz v1, :cond_c

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_6

    :catchall_4
    move-exception v0

    if-eqz v1, :cond_18

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_18
    throw v0

    :catch_5
    move-exception v0

    :try_start_d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    if-eqz v1, :cond_f

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_7

    :catchall_5
    move-exception v0

    if-eqz v1, :cond_19

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_19
    throw v0
.end method

.method private upgradeFrom1to3(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updating1to3"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "moving data from foods"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CREATE TABLE IF NOT EXISTS food_info (_id INTEGER PRIMARY KEY,name TEXT, lang TEXT, kcal REAL, create_time INTEGER, image TEXT, my INTEGER, api_id INTEGER, deleted INTEGER,favorite INTEGER,pinyin TEXT,pinyin_sort TEXT,health_star INTEGER,app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE meal (_id INTEGER PRIMARY KEY,create_time INTEGER, name TEXT, kcal REAL, kind INTEGER, comment TEXT,rec_delyn INTEGER,app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE meal_items (_id INTEGER PRIMARY KEY,id_meal_table INTEGER, id_food_info INTEGER, mark_number INTEGER, mark_posx REAL, mark_posy REAL, percent REAL,app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS images_table (_id INTEGER PRIMARY KEY,file_path TEXT, measure_type_id INTEGER, measure_id INTEGER, app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "moving data from exercises"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CREATE TABLE exercise (_id INTEGER PRIMARY KEY,create_time INTEGER, min INTEGER, id_exercise_db INTEGER, calories REAL, comment TEXT, app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS exercise_info (_id INTEGER PRIMARY KEY,kcal REAL, deleted INTEGER, image TEXT, name TEXT, lang TEXT, my INTEGER, favorite INTEGER, pinyin TEXT, pinyin_sort TEXT, app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "moving weight table"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CREATE TABLE weight (_id INTEGER PRIMARY KEY,sensor_id INTEGER, person_id INTEGER, comment TEXT, value REAL, app_name TEXT, bmi REAL, body_fat REAL, body_year REAL, height REAL, skeletal_muscle REAL, visceral_fat REAL, bmr REAL, create_time INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "moving sugar table"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CREATE TABLE blood_glucose (_id INTEGER PRIMARY KEY,sensor_id INTEGER, person_id INTEGER, comment TEXT, type INTEGER, value REAL, unit TEXT, app_name TEXT, create_time INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "moving pressure table"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CREATE TABLE blood_pressure (_id INTEGER PRIMARY KEY, sensor_id INTEGER, person_id INTEGER, comment TEXT, systolic REAL, diastolic REAL, mean INTEGER, pulse INTEGER, app_name TEXT, create_time INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "moving running pro table"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CREATE TABLE running_pro (_id INTEGER PRIMARY KEY,create_time INTEGER, min INTEGER, id_exercise_db INTEGER, calories REAL, comment TEXT, app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "moving sensor table"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CREATE TABLE IF NOT EXISTS sensor (_id INTEGER PRIMARY KEY,type INTEGER, manufacture TEXT, modelnumber TEXT, version TEXT, name TEXT, connectivity INTEGER NOT NULL DEFAULT (0), app_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, "DatabaseHelper upgradeFrom3to4 UpdatingFromVersion1"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->readFromCP(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    return-void
.end method

.method private upgradeFrom3to4(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 8

    const/4 v7, 0x0

    :try_start_0
    const-string v0, "UPDATE3TO4"

    invoke-static {v0, p1}, Lcom/sec/android/service/health/cp/database/utils/SqlParser;->execSqlFile(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v0, "SELECT * FROM weight"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getWeightContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string/jumbo v1, "weight"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string/jumbo v0, "weight"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_10
    .catchall {:try_start_2 .. :try_end_2} :catchall_10

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    if-eqz v6, :cond_29

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v1, v6

    :cond_2
    :goto_0
    :try_start_3
    const-string v0, "SELECT * FROM meal"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v6

    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getMealContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string/jumbo v1, "meal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string/jumbo v0, "meal"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_f
    .catchall {:try_start_4 .. :try_end_4} :catchall_f

    move-result v0

    if-nez v0, :cond_3

    :cond_4
    if-eqz v6, :cond_28

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v1, v6

    :cond_5
    :goto_1
    :try_start_5
    const-string v0, "SELECT * FROM blood_glucose"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v6

    :try_start_6
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getBloodGlucoseContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v1, "blood_glucose"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string v0, "blood_glucose"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_e
    .catchall {:try_start_6 .. :try_end_6} :catchall_e

    move-result v0

    if-nez v0, :cond_6

    :cond_7
    if-eqz v6, :cond_27

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v1, v6

    :cond_8
    :goto_2
    :try_start_7
    const-string v0, "SELECT * FROM blood_pressure"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v6

    :try_start_8
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getBloodPressureContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v1, "blood_pressure"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string v0, "blood_pressure"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_d
    .catchall {:try_start_8 .. :try_end_8} :catchall_d

    move-result v0

    if-nez v0, :cond_9

    :cond_a
    if-eqz v6, :cond_26

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v1, v6

    :cond_b
    :goto_3
    :try_start_9
    const-string v0, "SELECT * FROM exercise"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    move-result-object v6

    :try_start_a
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_c
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPUtils;->getExerciseContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v1, "exercise"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string v0, "exercise"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_c
    .catchall {:try_start_a .. :try_end_a} :catchall_c

    move-result v0

    if-nez v0, :cond_c

    :cond_d
    if-eqz v6, :cond_e

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_e
    :goto_4
    :try_start_b
    const-string v0, "SELECT * FROM exercise"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    move-result-object v6

    :try_start_c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_f
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/JToHMigrationUtil;->getExerciseContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v1, "exercise"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string v0, "exercise"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_b
    .catchall {:try_start_c .. :try_end_c} :catchall_a

    move-result v0

    if-nez v0, :cond_f

    :cond_10
    if-eqz v6, :cond_11

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_11
    :goto_5
    invoke-static {p1}, Lcom/sec/android/service/health/cp/datamigration/versionj/JToHMigrationUtil;->migrateExerciseInfos(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p1}, Lcom/sec/android/service/health/cp/datamigration/versionj/JToHMigrationUtil;->updateExerciseInfos(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Z

    :cond_12
    :try_start_d
    const-string v0, "SELECT * FROM meal_items"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_13
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v0, "my"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "food_info"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "id_food_info"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string v0, "food_info"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_13

    :cond_14
    const-string v0, "food_info"

    const-string/jumbo v1, "my = 0"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    if-eqz v7, :cond_15

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_15
    :goto_6
    :try_start_e
    const-string v0, "SELECT * FROM walk_for_life"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_16
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/JToHMigrationUtil;->getWalkForLifeContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string/jumbo v1, "walk_for_life"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string/jumbo v0, "walk_for_life"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    move-result v0

    if-nez v0, :cond_16

    :cond_17
    if-eqz v6, :cond_24

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v1, v6

    :goto_7
    :try_start_f
    const-string v0, "SELECT * FROM goal"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_9
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    move-result-object v6

    :try_start_10
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_19

    :cond_18
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v6}, Lcom/sec/android/service/health/cp/datamigration/versionj/JToHMigrationUtil;->getGoalContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v1, "goal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    const-string v0, "goal"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->displayLog(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a
    .catchall {:try_start_10 .. :try_end_10} :catchall_9

    move-result v0

    if-nez v0, :cond_18

    :cond_19
    if-eqz v6, :cond_1a

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1a
    :goto_8
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Database upgrade from to version 3 to 4 failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    move-object v1, v7

    :goto_9
    :try_start_11
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_11

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v6, v7

    :goto_a
    if-eqz v6, :cond_1b

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1b
    throw v0

    :catch_2
    move-exception v0

    :goto_b
    :try_start_12
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    move-object v6, v1

    :goto_c
    if-eqz v6, :cond_1c

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1c
    throw v0

    :catch_3
    move-exception v0

    :goto_d
    :try_start_13
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    if-eqz v1, :cond_8

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :catchall_2
    move-exception v0

    move-object v6, v1

    :goto_e
    if-eqz v6, :cond_1d

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1d
    throw v0

    :catch_4
    move-exception v0

    :goto_f
    :try_start_14
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    if-eqz v1, :cond_b

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :catchall_3
    move-exception v0

    move-object v6, v1

    :goto_10
    if-eqz v6, :cond_1e

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1e
    throw v0

    :catch_5
    move-exception v0

    :goto_11
    :try_start_15
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    if-eqz v1, :cond_e

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_4

    :catchall_4
    move-exception v0

    move-object v6, v1

    :goto_12
    if-eqz v6, :cond_1f

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1f
    throw v0

    :catch_6
    move-exception v0

    move-object v1, v0

    move-object v0, v7

    :goto_13
    :try_start_16
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_b

    if-eqz v0, :cond_25

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v6, v0

    goto/16 :goto_5

    :catchall_5
    move-exception v0

    move-object v6, v7

    :goto_14
    if-eqz v6, :cond_20

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_20
    throw v0

    :catch_7
    move-exception v0

    :try_start_17
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_6

    if-eqz v7, :cond_15

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_6

    :catchall_6
    move-exception v0

    if-eqz v7, :cond_21

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_21
    throw v0

    :catch_8
    move-exception v0

    :try_start_18
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    if-eqz v6, :cond_24

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v1, v6

    goto/16 :goto_7

    :catchall_7
    move-exception v0

    if-eqz v6, :cond_22

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_22
    throw v0

    :catch_9
    move-exception v0

    :goto_15
    :try_start_19
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_8

    if-eqz v1, :cond_1a

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_8

    :catchall_8
    move-exception v0

    move-object v6, v1

    :goto_16
    if-eqz v6, :cond_23

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_23
    throw v0

    :catchall_9
    move-exception v0

    goto :goto_16

    :catch_a
    move-exception v0

    move-object v1, v6

    goto :goto_15

    :catchall_a
    move-exception v0

    goto :goto_14

    :catchall_b
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_14

    :catch_b
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    goto :goto_13

    :catchall_c
    move-exception v0

    goto :goto_12

    :catch_c
    move-exception v0

    move-object v1, v6

    goto :goto_11

    :catchall_d
    move-exception v0

    goto :goto_10

    :catch_d
    move-exception v0

    move-object v1, v6

    goto/16 :goto_f

    :catchall_e
    move-exception v0

    goto/16 :goto_e

    :catch_e
    move-exception v0

    move-object v1, v6

    goto/16 :goto_d

    :catchall_f
    move-exception v0

    goto/16 :goto_c

    :catch_f
    move-exception v0

    move-object v1, v6

    goto/16 :goto_b

    :catchall_10
    move-exception v0

    goto/16 :goto_a

    :catchall_11
    move-exception v0

    move-object v6, v1

    goto/16 :goto_a

    :catch_10
    move-exception v0

    move-object v1, v6

    goto/16 :goto_9

    :cond_24
    move-object v1, v6

    goto/16 :goto_7

    :cond_25
    move-object v6, v0

    goto/16 :goto_5

    :cond_26
    move-object v1, v6

    goto/16 :goto_3

    :cond_27
    move-object v1, v6

    goto/16 :goto_2

    :cond_28
    move-object v1, v6

    goto/16 :goto_1

    :cond_29
    move-object v1, v6

    goto/16 :goto_0
.end method

.method private upgradeFrom4to5(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 2

    const-string v0, "CREATE TABLE IF NOT EXISTS tip_instance_table (_id INTEGER PRIMARY KEY, tip_instance INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS healthboard_tips (_id INTEGER PRIMARY KEY AUTOINCREMENT, rule_id INTEGER,create_time INTEGER, hb_data_one TEXT, hb_data_two TEXT, article TEXT, boardcontentid TEXT, meta_data TEXT, day_st INTEGER, week_st_mon INTEGER, week_st_sun INTEGER, month_st INTEGER, year_st INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "alter table images_table add column sync_key INTEGER DEFAULT("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS ant_device_list (_id INTEGER PRIMARY KEY, device_id INTEGER, device_type INTEGER, uuid TEXT, name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS map_path (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, latitude REAL, longitude REAL, altitude REAL, accuracy REAL);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS realtime_heartbeat (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, bpm INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS realtime_speed (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, speed REAL);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS training_load_peak (_id INTEGER PRIMARY KEY, db_index INTEGER, time INTEGER, training_load_peak INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "create table b_pedometer ( _id INTEGER PRIMARY KEY, db_index INTEGER, create_time INTEGER, total_steps INTEGER, run_steps INTEGER, walk_steps INTEGER, updown_steps INTEGER, kcal REAL, walk_speed REAL, distance REAL, kind_of_walking INTEGER, day_st INTEGER, week_st_mon INTEGER, week_st_sun INTEGER, month_st INTEGER, year_st INTEGER);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TRIGGER IF EXISTS delete_exercise_trigger;\nCREATE TRIGGER delete_exercise_trigger DELETE ON exercise \n BEGIN\n   DELETE FROM map_path WHERE db_index = OLD._id;\n   DELETE FROM realtime_heartbeat WHERE db_index = OLD._id;\n   DELETE FROM realtime_speed WHERE db_index = OLD._id;\n   DELETE FROM training_load_peak WHERE db_index = OLD._id;\n   DELETE FROM b_pedometer WHERE db_index = OLD._id;\n END;"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "alter table extend_food_info add column cholesterol INTEGER"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "alter table extend_food_info add column sodium INTEGER"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "alter table extend_food_info add column potassium INTEGER"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "alter table extend_food_info add column vitamin_a INTEGER"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "alter table extend_food_info add column vitamin_c INTEGER"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "alter table extend_food_info add column calcium INTEGER"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "alter table extend_food_info add column iron INTEGER"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    const-string v0, "create table my_bluetooth_devices ( _id INTEGER PRIMARY KEY, uuid TEXT, device_user_name TEXT);"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, "createTablesRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "DROP VIEW IF EXISTS walk_for_life_view;CREATE VIEW walk_for_life_view AS SELECT create_time, day_st, max(total_steps) AS maxTotal, min(total_steps) AS minTotal, sum(total_steps) AS sumTotal, sum(run_steps) AS sumRun, sum(walk_steps) AS sumWalk, sum(updown_steps) AS sumUD, sum(kcal) AS sumKcal, sum(distance) AS sumDistance, max(walk_speed) AS maxSpeed, min(walk_speed) AS minSpeed, avg(walk_speed) AS avgSpeed, count(_id) AS count FROM (SELECT * FROM temp_walk_for_life UNION SELECT * FROM walk_for_life) GROUP BY day_st;"

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public upgrade(II)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->upgradeFrom1to3(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->upgradeFrom3to4(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->upgradeFrom4to5(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
