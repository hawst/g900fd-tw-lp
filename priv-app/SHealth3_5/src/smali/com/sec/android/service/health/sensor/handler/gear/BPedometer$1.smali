.class final Lcom/sec/android/service/health/sensor/handler/gear/BPedometer$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;
    .locals 13

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v9

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v10

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v11

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v12

    invoke-direct/range {v0 .. v12}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;-><init>(IIJIIIIFFFI)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;
    .locals 1

    new-array v0, p1, [Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer$1;->newArray(I)[Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    move-result-object v0

    return-object v0
.end method
