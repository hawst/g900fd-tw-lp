.class public Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;
.super Landroid/app/IntentService;


# static fields
.field private static final ACTION_PEDOMETER_SETTING_SYNC:Ljava/lang/String; = "android.intent.action.PEDOMETER_SETTING_SYNC"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final WAIT_TIME:I

.field private mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mDeviceId:Ljava/lang/String;

.field private mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private waitObj:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "GearSensorService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x2af8

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->WAIT_TIME:I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;-><init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;-><init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$3;-><init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->waitObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x2af8

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->WAIT_TIME:I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$1;-><init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$2;-><init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$3;-><init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->waitObj:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Z
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->isDeviceAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    return-object v0
.end method

.method private isDeviceAvailable()Z
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v2, 0x2724

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " bReturn = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[+] deinitialize "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->waitObj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->waitObj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[-] deinitialize "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDestroy() : Thread Name "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[+] onHandleIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "intent Action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PEDOMETER_SETTING_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->waitObj:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->waitObj:Ljava/lang/Object;

    const-wide/16 v2, 0x2af8

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->deinitialize()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;

    const-string v1, "[-] onHandleIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->deinitialize()V

    goto :goto_0
.end method
