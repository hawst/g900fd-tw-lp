.class public final Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;
.super Ljava/lang/Object;


# static fields
.field public static final INVALID_STATE:I = -0x1

.field private static final LOG_TAG:Ljava/lang/String; = "[HealthSensor]USB"

.field public static final RECIEVE_ACK_STATE:I = 0x1

.field public static final RECIEVE_COMMAND_STATE:I = 0x2

.field public static final SEND_ACK_STATE:I = 0x3

.field public static final SEND_COMMAND_STATE:I

.field private static mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;


# instance fields
.field public commStateFlag:I

.field private mUsbHandlerListener:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbHandlerListener;

.field public meterListener:Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;

.field public ret:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->meterListener:Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->ret:I

    return-void
.end method

.method private createApdu([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)[B
    .locals 5

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-string v0, "[HealthSensor]USB"

    const-string v1, "createApdu [ START]"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, p2, [B

    aput-byte v2, v0, v4

    const/4 v1, 0x1

    aput-byte p2, v0, v1

    aput-byte v4, v0, v2

    array-length v1, p1

    invoke-static {p1, v4, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v1, p2, -0x3

    aput-byte v3, v0, v1

    add-int/lit8 v1, p2, -0x2

    new-array v1, v1, [B

    array-length v2, v1

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->createCRC([B)[B

    move-result-object v1

    add-int/lit8 v2, p2, -0x2

    array-length v3, v1

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method private createCRC([B)[B
    .locals 14

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/16 v11, 0x10

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v7, v12, [B

    const v0, 0xffff

    const/16 v8, 0x1021

    array-length v9, p1

    move v6, v3

    :goto_0
    if-ge v6, v9, :cond_4

    aget-byte v10, p1, v6

    move v5, v3

    :goto_1
    const/16 v1, 0x8

    if-ge v5, v1, :cond_3

    rsub-int/lit8 v1, v5, 0x7

    shr-int v1, v10, v1

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    move v4, v2

    :goto_2
    shr-int/lit8 v1, v0, 0xf

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_3
    shl-int/lit8 v0, v0, 0x1

    xor-int/2addr v1, v4

    if-eqz v1, :cond_0

    xor-int/2addr v0, v8

    :cond_0
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_1
    move v4, v3

    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_3

    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    :cond_4
    const v1, 0xffff

    and-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v12, :cond_6

    const-string v1, "0"

    invoke-static {v1, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-byte v1, v1

    aput-byte v1, v7, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v7, v3

    :cond_5
    :goto_4
    return-object v7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v12, :cond_7

    const-string v1, "0"

    invoke-static {v1, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-byte v1, v1

    aput-byte v1, v7, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v7, v3

    goto :goto_4

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v13, :cond_8

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-byte v1, v1

    aput-byte v1, v7, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v7, v3

    goto :goto_4

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v4, 0x4

    if-ne v1, v4, :cond_5

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v3, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-byte v1, v1

    aput-byte v1, v7, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v12, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v7, v3

    goto :goto_4
.end method

.method private extractApplicationData([B)[B
    .locals 4

    const/4 v0, 0x0

    const-string v1, "[HealthSensor]USB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[CommunicationManager] extractApplicationData() commStateFlag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->meterListener:Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;

    instance-of v0, v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;

    if-eqz v0, :cond_0

    :goto_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    :goto_1
    return-object p1

    :cond_0
    array-length v0, p1

    add-int/lit8 v0, v0, -0x6

    new-array v0, v0, [B

    const/4 v1, 0x3

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "extractApplicationData commStateFlag is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but cmd msg recieved."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x67

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->errorStatus(Ljava/lang/String;I)V

    move-object p1, v0

    goto :goto_1
.end method

.method private log(Ljava/lang/String;)V
    .locals 3

    const-string v0, "[HealthSensor]USB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CommunicationManager]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public checkCRC([B)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x2

    new-array v1, v1, [B

    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    if-gez v2, :cond_1

    const-string v1, "[HealthSensor]USB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[CommunicationManager] checkCRC() wrong cmdLength="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-array v3, v2, [B

    array-length v4, v3

    invoke-static {p1, v0, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, v1

    invoke-static {p1, v2, v1, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->createCRC([B)[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public destroyConnection()V
    .locals 2

    const-string v0, "[HealthSensor]USB"

    const-string v1, " [CommunicationManager] destroyConnection usbSppClose "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->meterListener:Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;

    return-void
.end method

.method public errorStatus(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error: ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->log(Ljava/lang/String;)V

    return-void
.end method

.method public parsingCommnadData([B)V
    .locals 4

    const/16 v3, 0x67

    const/4 v2, 0x6

    const-string v0, "[HealthSensor]USB"

    const-string v1, "[CommunicationManager] parsingCommnadData "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->checkCRC([B)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "[HealthSensor]USB"

    const-string v1, "[CommunicationManager] parsingCommnadData crc has wrong value. "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CRC data has worng value."

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->errorStatus(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    array-length v0, p1

    if-ge v0, v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "parsingCommnadData, recived data is too short. len="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->log(Ljava/lang/String;)V

    const-string/jumbo v0, "recived data is too short."

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->errorStatus(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    array-length v0, p1

    if-ne v0, v2, :cond_3

    const-string v0, "[HealthSensor]USB"

    const-string v1, "[CommunicationManager] parsingCommnadData ACK received "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    goto :goto_0

    :cond_3
    const-string v0, "[HealthSensor]USB"

    const-string v1, "[CommunicationManager]msg received "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->extractApplicationData([B)[B

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->meterListener:Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;

    invoke-interface {v1, v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;->responseReceived([B)V

    goto :goto_0
.end method

.method public sendAck()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x6

    new-array v0, v0, [B

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->ACK:[B

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->createCRC([B)[B

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->ACK:[B

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->ACK:[B

    array-length v3, v3

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->ACK:[B

    array-length v2, v2

    array-length v3, v1

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->sendRawData([B)I

    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    const-string v0, "[HealthSensor]USB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CommunicationManager] sendAck commStateFlag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ret = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->ret:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " sendAck commStateFlag is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " but ack send."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->errorStatus(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V
    .locals 3

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->meterListener:Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->createApdu([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)[B

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->sendRawData([B)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    const-string v0, "[HealthSensor]USB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CommunicationManager] sendData listener="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ret = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->ret:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public sendDataForStartSession([BLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V
    .locals 3

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->meterListener:Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->sendRawData([B)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    const-string v0, "[HealthSensor]USB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CommunicationManager] sendDataForStartSession ret="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->ret:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " commStateFlag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setProtocolListener(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;)V
    .locals 0

    sput-object p1, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    return-void
.end method

.method public startCommnunicationManager(I)Z
    .locals 3

    const-string v0, "[HealthSensor]USB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " [CommunicationManager] startCommnunicationManager baudRate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method
