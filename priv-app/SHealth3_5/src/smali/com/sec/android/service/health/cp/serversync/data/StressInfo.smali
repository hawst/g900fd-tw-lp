.class public Lcom/sec/android/service/health/cp/serversync/data/StressInfo;
.super Ljava/lang/Object;


# instance fields
.field public accId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceCreateDate:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyDate:Ljava/lang/String;

.field public deviceStressPKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public endTime:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public score:Ljava/lang/Float;

.field public startTime:Ljava/lang/String;

.field public stressStatus:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
