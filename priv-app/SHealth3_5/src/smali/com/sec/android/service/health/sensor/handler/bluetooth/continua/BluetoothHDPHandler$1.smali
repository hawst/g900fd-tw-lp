.class Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;
.super Landroid/bluetooth/BluetoothHealthCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->registerSinkAppConfiguration()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealthCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onHealthAppConfigurationStatusChange(Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthAppConfigurationStatusChange() name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getDataType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p2, :pswitch_data_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthAppConfigurationStatusChange default status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthAppConfigurationStatusChange status REGISTRATION SUCCESS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    invoke-static {v0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$402(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthAppConfigurationStatusChange status UNREGISTRATION SUCCESS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    invoke-static {v0, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$402(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthAppConfigurationStatusChange status REGISTRATION FAILURE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    invoke-static {v0, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$402(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthAppConfigurationStatusChange status UN-REGISTRATION FAILURE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    invoke-static {v0, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$402(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onHealthChannelStateChange(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthChannelStateChange() ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p4, :pswitch_data_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthChannelStateChange default state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthChannelStateChange() CREATE_CHANNEL_DONE SUCCESS case mDoesDataListenerExist = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mDoesDataListenerExist:Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$500(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mDoesDataListenerExist:Z
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$500(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p5, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "handleMessage CREATE_CHANNEL_DONE fd == null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;
    invoke-static {v0, p5}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$002(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Landroid/os/ParcelFileDescriptor;)Landroid/os/ParcelFileDescriptor;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mChannelId:I
    invoke-static {v0, p6}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$602(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;I)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    const/16 v1, 0x3e9

    # invokes: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->sendMessage(II)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;II)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthChannelStateChange() STATE_CHANNEL_DISCONNECTED info.mUid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$800(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " app:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$900(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyStop()I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mstateofBTCommunication:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$1000(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)I

    move-result v0

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_DISCONNECTED:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$1100()I

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->onDataStopped(II)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    const/16 v1, 0x3ea

    # invokes: Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->sendMessage(II)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;II)V

    goto/16 :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onHealthChannelStateChange() STATE CHANNEL CONNECTING"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onHealthChannelStateChange() STATE CHANNEL DISCONNECTING"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
