.class Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$1;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewHeartRateData(JLjava/util/EnumSet;IJLjava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;IJ",
            "Ljava/math/BigDecimal;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;",
            ")V"
        }
    .end annotation

    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewHeartRateData - computedHeartRate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", heartBeatCounter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "ant_heart_rate_counter"

    long-to-float v2, p5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;-><init>()V

    iput p4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$1;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method
