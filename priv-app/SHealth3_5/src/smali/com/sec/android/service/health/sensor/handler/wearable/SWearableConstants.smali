.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$DATA_UPDATE_ERROR_CODE;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$ERROR_CODE;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$BundleKey;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$EXTRA;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$GoalType;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$SELF_PERMISSION_VERSIONCODE;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$GEAR1_ANDROID;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$ACTIVITY_TRACKER;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$NEWGEAR;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$GEAR2;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$GEARFIT;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$ACTION;,
        Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$DEVICE_FIXED_NAME;
    }
.end annotation


# static fields
.field public static final ACTIVITY_TRACKER_MANAGER_PKG:Ljava/lang/String; = "com.samsung.android.app.atracker"

.field public static final DB_INSERT_ERROR_DATA_NAME:Ljava/lang/String; = "DB_INSERT_ERROR_DATA_NAME"

.field public static final DEVICE_KEY:Ljava/lang/String; = "DEVICE"

.field public static final GEAR_MANAGER_AUTHORITY:Ljava/lang/String; = "com.samsung.android.uhm.framework.appregistry.BaseContentProvider.provider"

.field public static final GEAR_MANAGER_DEVICE_URL:Ljava/lang/String; = "content://com.samsung.android.uhm.framework.appregistry.BaseContentProvider.provider/Device"

.field public static final GEAR_MANAGER_PKG:Ljava/lang/String; = "com.samsung.android.app.watchmanager"

.field public static final GEAR_MANAGER_PKG_GOPROVIDER:Ljava/lang/String; = "com.samsung.accessory.goproviders"

.field public static final HEALTH_PERMISSION:Ljava/lang/String; = "com.sec.android.service.health.permission.SENSOR"

.field public static final INTENT_ACTION_LASTSYNCTIME:Ljava/lang/String; = "com.samsung.android.shealth.UPDATE_LAST_SYNC_TIME"

.field public static final LAST_SURPPORTED_PROTOCOL_VERSION:D = 3.01

.field public static final NOTIFICATION_FLAG:Ljava/lang/String; = "NOTIFICATION_FLAG"

.field public static final SHEALTH_LAUNCH_NOTIFICATION_ID:I = 0x42c

.field public static final SUB_TAG:Ljava/lang/String; = "[HS_Wearable_sync]"

.field public static final SUB_TAG_ERROR:Ljava/lang/String; = "[HS_Wearable_sync_ERROR]"

.field public static final SYNC_NUMBER:Ljava/lang/String; = "sync_number"

.field public static final SYNC_TIME_OUT:I = 0x493e0

.field public static final TIME_6_HOUR:I = 0x1499700

.field public static final UTF_8:Ljava/lang/String; = "UTF-8"

.field public static final VERSION_KEY:Ljava/lang/String; = "VERSION"

.field public static final WEARABLE_COMM_PROTOCOL_VERSION_3_01:D = 3.01

.field public static final WEARABLE_NOTIFICATION_ID:I = 0x235b

.field public static final WINGTIP_MANAGER_PKG:Ljava/lang/String; = "com.samsung.android.wms"

.field public static final bGearOTest:Z = false

.field public static final bNewVersion:Z = true

.field public static final bSavePrintLog:Z

.field public static final bTest:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
