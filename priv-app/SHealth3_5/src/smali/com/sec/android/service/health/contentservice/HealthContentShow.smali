.class public Lcom/sec/android/service/health/contentservice/HealthContentShow;
.super Landroid/support/v4/app/FragmentActivity;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;,
        Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;
    }
.end annotation


# static fields
.field public static final ACTION_ACCOUNT:I = 0x3ec

.field public static final ACTION_DELETE:I = 0x3eb

.field public static final ACTION_INSERT:I = 0x3e9

.field public static final ACTION_RECORD:I = 0x3ed

.field public static ACTION_RECORD_DATA:Ljava/lang/String; = null

.field public static ACTION_THIRD_PARTY_APP_FORCE_CLOSE:Ljava/lang/String; = null

.field public static final ACTION_UPDATE:I = 0x3ea


# instance fields
.field adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

.field private after_meal:Landroid/widget/RadioButton;

.field private bloodGlucose_switch:Landroid/widget/RadioGroup;

.field private closeByApp:Z

.field private contentBloodGlucose_layout:Landroid/widget/LinearLayout;

.field private contentParentLayout:Landroid/widget/LinearLayout;

.field private contentParentLayout_value_total:Landroid/widget/LinearLayout;

.field private contentui_dialog_cancel_bottom:Landroid/widget/Button;

.field private contentui_dialog_ok_bottom:Landroid/widget/Button;

.field private currentList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/contentservice/_ContentValueData;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/text/DecimalFormat;

.field data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/contentservice/_ContentValueData;",
            ">;>;"
        }
    .end annotation
.end field

.field private dateFormet:Ljava/text/DateFormat;

.field private isDirectSensorData:Z

.field private isStreaming:Z

.field private mAccpet:[Z

.field private mAction:I

.field private mApprovedColumns:[Ljava/lang/String;

.field private mBodyTemperaturText:Ljava/lang/String;

.field private mChecked:[Z

.field private mCloseContentServiceReceiver:Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;

.field private mContentValueListView:Landroid/widget/ListView;

.field private mContentViewLIstViewLayout:Landroid/widget/LinearLayout;

.field private mDeviceId:Ljava/lang/String;

.field private mDeviceObjectId:I

.field private mMealTime:Z

.field private mNotification:Landroid/app/Notification;

.field private mRequestTime:J

.field private mUniqueKey:Ljava/lang/String;

.field private mealChecked:I

.field private nm:Landroid/app/NotificationManager;

.field private popupTitle_text:Landroid/widget/TextView;

.field private rb_fasting:Landroid/widget/RadioButton;

.field private textContent:Landroid/widget/TextView;

.field private textDate:Landroid/widget/TextView;

.field private textListDesc:Landroid/widget/TextView;

.field private textTime:Landroid/widget/TextView;

.field private textValue:Landroid/widget/TextView;

.field private textValue_symbol:Landroid/widget/TextView;

.field private timeFormet:Ljava/text/DateFormat;

.field private unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

.field private uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 52

    const/16 v0, 0x2c

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x5a14

    aput v45, v1, v44

    const/16 v44, -0x4cd8

    aput v44, v1, v43

    const/16 v43, -0x24

    aput v43, v1, v42

    const/16 v42, -0x7c

    aput v42, v1, v41

    const/16 v41, -0x58be

    aput v41, v1, v40

    const/16 v40, -0x2b

    aput v40, v1, v39

    const/16 v39, -0x2bbc

    aput v39, v1, v38

    const/16 v38, -0x5a

    aput v38, v1, v37

    const/16 v37, -0xf

    aput v37, v1, v36

    const/16 v36, -0x26b9

    aput v36, v1, v35

    const/16 v35, -0x49

    aput v35, v1, v34

    const/16 v34, -0x5287

    aput v34, v1, v33

    const/16 v33, -0x22

    aput v33, v1, v32

    const/16 v32, 0xb30

    aput v32, v1, v31

    const/16 v31, 0x2863

    aput v31, v1, v30

    const/16 v30, 0x65c

    aput v30, v1, v29

    const/16 v29, 0x316a

    aput v29, v1, v28

    const/16 v28, -0xdb0

    aput v28, v1, v27

    const/16 v27, -0x69

    aput v27, v1, v26

    const/16 v26, -0x63aa

    aput v26, v1, v25

    const/16 v25, -0x4e

    aput v25, v1, v24

    const/16 v24, -0x32

    aput v24, v1, v23

    const/16 v23, -0x5d

    aput v23, v1, v22

    const/16 v22, 0x2a25

    aput v22, v1, v21

    const/16 v21, 0x765c

    aput v21, v1, v20

    const/16 v20, -0x52fc

    aput v20, v1, v19

    const/16 v19, -0x38

    aput v19, v1, v18

    const/16 v18, 0x6c4a

    aput v18, v1, v17

    const/16 v17, 0x7b42

    aput v17, v1, v16

    const/16 v16, -0x18e1

    aput v16, v1, v15

    const/16 v15, -0x72

    aput v15, v1, v14

    const/16 v14, -0x4d

    aput v14, v1, v13

    const/16 v13, -0x6b

    aput v13, v1, v12

    const/16 v12, 0x675e

    aput v12, v1, v11

    const/16 v11, -0x14f7

    aput v11, v1, v10

    const/16 v10, -0x76

    aput v10, v1, v9

    const/16 v9, -0x70

    aput v9, v1, v8

    const/16 v8, -0x59ad

    aput v8, v1, v7

    const/16 v7, -0x3d

    aput v7, v1, v6

    const/16 v6, -0x57

    aput v6, v1, v5

    const/16 v5, -0x4e

    aput v5, v1, v4

    const/16 v4, -0x16

    aput v4, v1, v3

    const/16 v3, -0x26

    aput v3, v1, v2

    const/16 v2, -0x75da

    aput v2, v1, v0

    const/16 v0, 0x2c

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x5a70

    aput v46, v0, v45

    const/16 v45, -0x4ca6

    aput v45, v0, v44

    const/16 v44, -0x4d

    aput v44, v0, v43

    const/16 v43, -0x19

    aput v43, v0, v42

    const/16 v42, -0x58d9

    aput v42, v0, v41

    const/16 v41, -0x59

    aput v41, v0, v40

    const/16 v40, -0x2b96

    aput v40, v0, v39

    const/16 v39, -0x2c

    aput v39, v0, v38

    const/16 v38, -0x62

    aput v38, v0, v37

    const/16 v37, -0x26cc

    aput v37, v0, v36

    const/16 v36, -0x27

    aput v36, v0, v35

    const/16 v35, -0x52e4

    aput v35, v0, v34

    const/16 v34, -0x53

    aput v34, v0, v33

    const/16 v33, 0xb1e

    aput v33, v0, v32

    const/16 v32, 0x280b

    aput v32, v0, v31

    const/16 v31, 0x628

    aput v31, v0, v30

    const/16 v30, 0x3106

    aput v30, v0, v29

    const/16 v29, -0xdcf

    aput v29, v0, v28

    const/16 v28, -0xe

    aput v28, v0, v27

    const/16 v27, -0x63c2

    aput v27, v0, v26

    const/16 v26, -0x64

    aput v26, v0, v25

    const/16 v25, -0x55

    aput v25, v0, v24

    const/16 v24, -0x40

    aput v24, v0, v23

    const/16 v23, 0x2a4c

    aput v23, v0, v22

    const/16 v22, 0x762a

    aput v22, v0, v21

    const/16 v21, -0x528a

    aput v21, v0, v20

    const/16 v20, -0x53

    aput v20, v0, v19

    const/16 v19, 0x6c39

    aput v19, v0, v18

    const/16 v18, 0x7b6c

    aput v18, v0, v17

    const/16 v17, -0x1885

    aput v17, v0, v16

    const/16 v16, -0x19

    aput v16, v0, v15

    const/16 v15, -0x24

    aput v15, v0, v14

    const/16 v14, -0x19

    aput v14, v0, v13

    const/16 v13, 0x673a

    aput v13, v0, v12

    const/16 v12, -0x1499

    aput v12, v0, v11

    const/16 v11, -0x15

    aput v11, v0, v10

    const/16 v10, -0x42

    aput v10, v0, v9

    const/16 v9, -0x59d0

    aput v9, v0, v8

    const/16 v8, -0x5a

    aput v8, v0, v7

    const/16 v7, -0x26

    aput v7, v0, v6

    const/16 v6, -0x64

    aput v6, v0, v5

    const/16 v5, -0x79

    aput v5, v0, v4

    const/16 v4, -0x4b

    aput v4, v0, v3

    const/16 v3, -0x75bb

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->ACTION_RECORD_DATA:Ljava/lang/String;

    const/16 v0, 0x31

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x3841

    aput v50, v1, v49

    const/16 v49, -0x41b5

    aput v49, v1, v48

    const/16 v48, -0x2f

    aput v48, v1, v47

    const/16 v47, -0x6ce8

    aput v47, v1, v46

    const/16 v46, -0x10

    aput v46, v1, v45

    const/16 v45, 0x5f2d

    aput v45, v1, v44

    const/16 v44, 0x93a

    aput v44, v1, v43

    const/16 v43, -0x1696

    aput v43, v1, v42

    const/16 v42, -0x65

    aput v42, v1, v41

    const/16 v41, 0x5f27

    aput v41, v1, v40

    const/16 v40, 0x2339

    aput v40, v1, v39

    const/16 v39, -0x1ef3

    aput v39, v1, v38

    const/16 v38, -0x6d

    aput v38, v1, v37

    const/16 v37, -0x5e

    aput v37, v1, v36

    const/16 v36, -0x3a81

    aput v36, v1, v35

    const/16 v35, -0x55

    aput v35, v1, v34

    const/16 v34, 0x4c51

    aput v34, v1, v33

    const/16 v33, 0x543f

    aput v33, v1, v32

    const/16 v32, -0x4d86

    aput v32, v1, v31

    const/16 v31, -0x26

    aput v31, v1, v30

    const/16 v30, 0x790a

    aput v30, v1, v29

    const/16 v29, -0x19eb

    aput v29, v1, v28

    const/16 v28, -0x79

    aput v28, v1, v27

    const/16 v27, -0x3c

    aput v27, v1, v26

    const/16 v26, -0x4ba9

    aput v26, v1, v25

    const/16 v25, -0x66

    aput v25, v1, v24

    const/16 v24, 0x1161

    aput v24, v1, v23

    const/16 v23, -0x2c8e

    aput v23, v1, v22

    const/16 v22, -0x46

    aput v22, v1, v21

    const/16 v21, -0x30

    aput v21, v1, v20

    const/16 v20, -0x62

    aput v20, v1, v19

    const/16 v19, -0x67

    aput v19, v1, v18

    const/16 v18, -0x17

    aput v18, v1, v17

    const/16 v17, -0x1d

    aput v17, v1, v16

    const/16 v16, -0x37d6

    aput v16, v1, v15

    const/16 v15, -0x5f

    aput v15, v1, v14

    const/16 v14, -0x71

    aput v14, v1, v13

    const/16 v13, -0x1bd3

    aput v13, v1, v12

    const/16 v12, -0x80

    aput v12, v1, v11

    const/16 v11, -0x78

    aput v11, v1, v10

    const/16 v10, 0x694c

    aput v10, v1, v9

    const/16 v9, 0x5747

    aput v9, v1, v8

    const/16 v8, 0x4e34

    aput v8, v1, v7

    const/16 v7, 0x182b

    aput v7, v1, v6

    const/16 v6, -0x5295

    aput v6, v1, v5

    const/16 v5, -0x7d

    aput v5, v1, v4

    const/16 v4, -0x7f

    aput v4, v1, v3

    const/16 v3, -0x1e

    aput v3, v1, v2

    const/16 v2, -0x79c0

    aput v2, v1, v0

    const/16 v0, 0x31

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x3824

    aput v51, v0, v50

    const/16 v50, -0x41c8

    aput v50, v0, v49

    const/16 v49, -0x42

    aput v49, v0, v48

    const/16 v48, -0x6c8c

    aput v48, v0, v47

    const/16 v47, -0x6d

    aput v47, v0, v46

    const/16 v46, 0x5f03

    aput v46, v0, v45

    const/16 v45, 0x95f

    aput v45, v0, v44

    const/16 v44, -0x16f7

    aput v44, v0, v43

    const/16 v43, -0x17

    aput v43, v0, v42

    const/16 v42, 0x5f48

    aput v42, v0, v41

    const/16 v41, 0x235f

    aput v41, v0, v40

    const/16 v40, -0x1edd

    aput v40, v0, v39

    const/16 v39, -0x1f

    aput v39, v0, v38

    const/16 v38, -0x33

    aput v38, v0, v37

    const/16 v37, -0x3af4

    aput v37, v0, v36

    const/16 v36, -0x3b

    aput v36, v0, v35

    const/16 v35, 0x4c34

    aput v35, v0, v34

    const/16 v34, 0x544c

    aput v34, v0, v33

    const/16 v33, -0x4dac

    aput v33, v0, v32

    const/16 v32, -0x4e

    aput v32, v0, v31

    const/16 v31, 0x797e

    aput v31, v0, v30

    const/16 v30, -0x1987

    aput v30, v0, v29

    const/16 v29, -0x1a

    aput v29, v0, v28

    const/16 v28, -0x5f

    aput v28, v0, v27

    const/16 v27, -0x4bc1

    aput v27, v0, v26

    const/16 v26, -0x4c

    aput v26, v0, v25

    const/16 v25, 0x1104

    aput v25, v0, v24

    const/16 v24, -0x2cef

    aput v24, v0, v23

    const/16 v23, -0x2d

    aput v23, v0, v22

    const/16 v22, -0x5a

    aput v22, v0, v21

    const/16 v21, -0x14

    aput v21, v0, v20

    const/16 v20, -0x4

    aput v20, v0, v19

    const/16 v19, -0x66

    aput v19, v0, v18

    const/16 v18, -0x33

    aput v18, v0, v17

    const/16 v17, -0x37b2

    aput v17, v0, v16

    const/16 v16, -0x38

    aput v16, v0, v15

    const/16 v15, -0x20

    aput v15, v0, v14

    const/16 v14, -0x1ba1

    aput v14, v0, v13

    const/16 v13, -0x1c

    aput v13, v0, v12

    const/16 v12, -0x1a

    aput v12, v0, v11

    const/16 v11, 0x692d

    aput v11, v0, v10

    const/16 v10, 0x5769

    aput v10, v0, v9

    const/16 v9, 0x4e57

    aput v9, v0, v8

    const/16 v8, 0x184e

    aput v8, v0, v7

    const/16 v7, -0x52e8

    aput v7, v0, v6

    const/16 v6, -0x53

    aput v6, v0, v5

    const/16 v5, -0x14

    aput v5, v0, v4

    const/16 v4, -0x73

    aput v4, v0, v3

    const/16 v3, -0x79dd

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v3, v0

    if-lt v2, v3, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v3, v0

    if-lt v2, v3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->ACTION_THIRD_PARTY_APP_FORCE_CLOSE:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mealChecked:I

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Landroid/app/NotificationManager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->nm:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/service/health/contentservice/HealthContentShow;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mMealTime:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/contentservice/HealthContentShow;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mealChecked:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/text/DateFormat;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->dateFormet:Ljava/text/DateFormat;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/text/DateFormat;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->timeFormet:Ljava/text/DateFormat;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/text/DecimalFormat;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->d:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/service/health/contentservice/HealthContentShow;Ljava/text/DecimalFormat;)Ljava/text/DecimalFormat;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->d:Ljava/text/DecimalFormat;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/contentservice/HealthContentShow;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceObjectId:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/service/health/contentservice/HealthContentShow;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->closeByApp:Z

    return p1
.end method

.method public static dLPrmaYUQG()Ljava/lang/String;
    .locals 56

    const/16 v0, 0x35

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, -0x18

    aput v54, v1, v53

    const/16 v53, -0x33db

    aput v53, v1, v52

    const/16 v52, -0x5b

    aput v52, v1, v51

    const/16 v51, 0x23

    aput v51, v1, v50

    const/16 v50, 0x4375

    aput v50, v1, v49

    const/16 v49, 0xb6b

    aput v49, v1, v48

    const/16 v48, -0xa92

    aput v48, v1, v47

    const/16 v47, -0x67

    aput v47, v1, v46

    const/16 v46, -0x2d

    aput v46, v1, v45

    const/16 v45, -0x7b

    aput v45, v1, v44

    const/16 v44, -0x6

    aput v44, v1, v43

    const/16 v43, -0x75

    aput v43, v1, v42

    const/16 v42, -0x7e

    aput v42, v1, v41

    const/16 v41, -0x54ca

    aput v41, v1, v40

    const/16 v40, -0x36

    aput v40, v1, v39

    const/16 v39, -0x8

    aput v39, v1, v38

    const/16 v38, -0x1b

    aput v38, v1, v37

    const/16 v37, -0x60

    aput v37, v1, v36

    const/16 v36, 0x642

    aput v36, v1, v35

    const/16 v35, 0x4e28

    aput v35, v1, v34

    const/16 v34, 0x7767

    aput v34, v1, v33

    const/16 v33, -0x58a1

    aput v33, v1, v32

    const/16 v32, -0x2c

    aput v32, v1, v31

    const/16 v31, -0x63

    aput v31, v1, v30

    const/16 v30, -0x2d

    aput v30, v1, v29

    const/16 v29, -0x49

    aput v29, v1, v28

    const/16 v28, -0x4e

    aput v28, v1, v27

    const/16 v27, -0x59

    aput v27, v1, v26

    const/16 v26, -0x4892

    aput v26, v1, v25

    const/16 v25, -0x2e

    aput v25, v1, v24

    const/16 v24, -0x3382

    aput v24, v1, v23

    const/16 v23, -0x1e

    aput v23, v1, v22

    const/16 v22, -0x75

    aput v22, v1, v21

    const/16 v21, -0x4dbc

    aput v21, v1, v20

    const/16 v20, -0x3a

    aput v20, v1, v19

    const/16 v19, -0x66ab

    aput v19, v1, v18

    const/16 v18, -0x4

    aput v18, v1, v17

    const/16 v17, -0x44

    aput v17, v1, v16

    const/16 v16, -0xd

    aput v16, v1, v15

    const/16 v15, -0x4f

    aput v15, v1, v14

    const/16 v14, -0x58

    aput v14, v1, v13

    const/16 v13, 0x1a19

    aput v13, v1, v12

    const/16 v12, 0x737d

    aput v12, v1, v11

    const/16 v11, 0x5b53

    aput v11, v1, v10

    const/16 v10, 0x5066

    aput v10, v1, v9

    const/16 v9, -0x6d90

    aput v9, v1, v8

    const/4 v8, -0x5

    aput v8, v1, v7

    const/16 v7, -0x1b

    aput v7, v1, v6

    const/16 v6, 0x676e

    aput v6, v1, v5

    const/16 v5, -0x3fb9

    aput v5, v1, v4

    const/16 v4, -0x57

    aput v4, v1, v3

    const/16 v3, 0x543d

    aput v3, v1, v2

    const/16 v2, -0x22ff

    aput v2, v1, v0

    const/16 v0, 0x35

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, -0x2d

    aput v55, v0, v54

    const/16 v54, -0x33f4

    aput v54, v0, v53

    const/16 v53, -0x34

    aput v53, v0, v52

    const/16 v52, 0x51

    aput v52, v0, v51

    const/16 v51, 0x4300

    aput v51, v0, v50

    const/16 v50, 0xb43

    aput v50, v0, v49

    const/16 v49, -0xaf5

    aput v49, v0, v48

    const/16 v48, -0xb

    aput v48, v0, v47

    const/16 v47, -0x4f

    aput v47, v0, v46

    const/16 v46, -0x1c

    aput v46, v0, v45

    const/16 v45, -0x6a

    aput v45, v0, v44

    const/16 v44, -0x12

    aput v44, v0, v43

    const/16 v43, -0x1f

    aput v43, v0, v42

    const/16 v42, -0x54bc

    aput v42, v0, v41

    const/16 v41, -0x55

    aput v41, v0, v40

    const/16 v40, -0x58

    aput v40, v0, v39

    const/16 v39, -0x6f

    aput v39, v0, v38

    const/16 v38, -0x3b

    aput v38, v0, v37

    const/16 v37, 0x625

    aput v37, v0, v36

    const/16 v36, 0x4e06

    aput v36, v0, v35

    const/16 v35, 0x774e

    aput v35, v0, v34

    const/16 v34, -0x5889

    aput v34, v0, v33

    const/16 v33, -0x59

    aput v33, v0, v32

    const/16 v32, -0x4

    aput v32, v0, v31

    const/16 v31, -0x5f

    aput v31, v0, v30

    const/16 v30, -0x3d

    aput v30, v0, v29

    const/16 v29, -0x36

    aput v29, v0, v28

    const/16 v28, -0x1e

    aput v28, v0, v27

    const/16 v27, -0x48e6

    aput v27, v0, v26

    const/16 v26, -0x49

    aput v26, v0, v25

    const/16 v25, -0x33e7

    aput v25, v0, v24

    const/16 v24, -0x34

    aput v24, v0, v23

    const/16 v23, -0x5e

    aput v23, v0, v22

    const/16 v22, -0x4d94

    aput v22, v0, v21

    const/16 v21, -0x4e

    aput v21, v0, v20

    const/16 v20, -0x66c5

    aput v20, v0, v19

    const/16 v19, -0x67

    aput v19, v0, v18

    const/16 v18, -0x38

    aput v18, v0, v17

    const/16 v17, -0x63

    aput v17, v0, v16

    const/16 v16, -0x8

    aput v16, v0, v15

    const/16 v15, -0x24

    aput v15, v0, v14

    const/16 v14, 0x1a7c

    aput v14, v0, v13

    const/16 v13, 0x731a

    aput v13, v0, v12

    const/16 v12, 0x5b73

    aput v12, v0, v11

    const/16 v11, 0x505b

    aput v11, v0, v10

    const/16 v10, -0x6db0

    aput v10, v0, v9

    const/16 v9, -0x6e

    aput v9, v0, v8

    const/16 v8, -0x69

    aput v8, v0, v7

    const/16 v7, 0x671b

    aput v7, v0, v6

    const/16 v6, -0x3f99

    aput v6, v0, v5

    const/16 v5, -0x40

    aput v5, v0, v4

    const/16 v4, 0x544f

    aput v4, v0, v3

    const/16 v3, -0x22ac

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getWeightUnitValue(FLjava/lang/String;)F
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v1, v7, [I

    const/16 v0, -0x31

    aput v0, v1, v6

    const/16 v0, -0xa

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, -0x53

    aput v2, v0, v6

    const/16 v2, -0x66

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_1

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertKgToLb(F)F

    move-result p1

    :cond_0
    :goto_2
    return p1

    :cond_1
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_3
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    new-array v1, v7, [I

    const/16 v0, 0x1211

    aput v0, v1, v6

    const/16 v0, 0x779

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, 0x1276

    aput v2, v0, v6

    const/16 v2, 0x712

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result p1

    goto :goto_2

    :cond_4
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static rEVx3t7fZ()Ljava/lang/String;
    .locals 59

    const/16 v0, 0x38

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x334c

    aput v57, v1, v56

    const/16 v56, -0x47ae

    aput v56, v1, v55

    const/16 v55, -0x34

    aput v55, v1, v54

    const/16 v54, -0x28

    aput v54, v1, v53

    const/16 v53, -0x5a

    aput v53, v1, v52

    const/16 v52, -0x38d7

    aput v52, v1, v51

    const/16 v51, -0x58

    aput v51, v1, v50

    const/16 v50, -0x28

    aput v50, v1, v49

    const/16 v49, -0x43

    aput v49, v1, v48

    const/16 v48, -0x6c

    aput v48, v1, v47

    const/16 v47, 0x4009

    aput v47, v1, v46

    const/16 v46, 0xe34

    aput v46, v1, v45

    const/16 v45, 0x716d

    aput v45, v1, v44

    const/16 v44, 0x4614

    aput v44, v1, v43

    const/16 v43, 0x634

    aput v43, v1, v42

    const/16 v42, -0x6d91

    aput v42, v1, v41

    const/16 v41, -0x2a

    aput v41, v1, v40

    const/16 v40, -0x61

    aput v40, v1, v39

    const/16 v39, -0xb85

    aput v39, v1, v38

    const/16 v38, -0x27

    aput v38, v1, v37

    const/16 v37, -0x6fd9

    aput v37, v1, v36

    const/16 v36, -0x22

    aput v36, v1, v35

    const/16 v35, 0x2a09

    aput v35, v1, v34

    const/16 v34, 0x2d65

    aput v34, v1, v33

    const/16 v33, 0x286e

    aput v33, v1, v32

    const/16 v32, 0x5b6b

    aput v32, v1, v31

    const/16 v31, 0x5a1a

    aput v31, v1, v30

    const/16 v30, -0x57fb

    aput v30, v1, v29

    const/16 v29, -0x1a

    aput v29, v1, v28

    const/16 v28, -0x6ad2

    aput v28, v1, v27

    const/16 v27, -0x24

    aput v27, v1, v26

    const/16 v26, -0x63

    aput v26, v1, v25

    const/16 v25, -0x78

    aput v25, v1, v24

    const/16 v24, -0x4dce

    aput v24, v1, v23

    const/16 v23, -0x6e

    aput v23, v1, v22

    const/16 v22, -0x63e4

    aput v22, v1, v21

    const/16 v21, -0x5f

    aput v21, v1, v20

    const/16 v20, -0x65

    aput v20, v1, v19

    const/16 v19, -0x45f0

    aput v19, v1, v18

    const/16 v18, -0x2b

    aput v18, v1, v17

    const/16 v17, -0x30

    aput v17, v1, v16

    const/16 v16, -0x7de

    aput v16, v1, v15

    const/16 v15, -0x65

    aput v15, v1, v14

    const/16 v14, 0x125

    aput v14, v1, v13

    const/16 v13, -0x6b94

    aput v13, v1, v12

    const/16 v12, -0x4c

    aput v12, v1, v11

    const/16 v11, -0x3c81

    aput v11, v1, v10

    const/16 v10, -0x54

    aput v10, v1, v9

    const/16 v9, -0x10c1

    aput v9, v1, v8

    const/16 v8, -0x56

    aput v8, v1, v7

    const/16 v7, -0x6f

    aput v7, v1, v6

    const/16 v6, -0x5d

    aput v6, v1, v5

    const/16 v5, -0x49f3

    aput v5, v1, v4

    const/16 v4, -0x6a

    aput v4, v1, v3

    const/16 v3, -0x78fd

    aput v3, v1, v2

    const/16 v2, -0x12

    aput v2, v1, v0

    const/16 v0, 0x38

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x3371

    aput v58, v0, v57

    const/16 v57, -0x47cd

    aput v57, v0, v56

    const/16 v56, -0x48

    aput v56, v0, v55

    const/16 v55, -0x47

    aput v55, v0, v54

    const/16 v54, -0x1e

    aput v54, v0, v53

    const/16 v53, -0x38a5

    aput v53, v0, v52

    const/16 v52, -0x39

    aput v52, v0, v51

    const/16 v51, -0x55

    aput v51, v0, v50

    const/16 v50, -0x2d

    aput v50, v0, v49

    const/16 v49, -0xf

    aput v49, v0, v48

    const/16 v48, 0x405a

    aput v48, v0, v47

    const/16 v47, 0xe40

    aput v47, v0, v46

    const/16 v46, 0x710e

    aput v46, v0, v45

    const/16 v45, 0x4671

    aput v45, v0, v44

    const/16 v44, 0x646

    aput v44, v0, v43

    const/16 v43, -0x6dfa

    aput v43, v0, v42

    const/16 v42, -0x6e

    aput v42, v0, v41

    const/16 v41, -0x14

    aput v41, v0, v40

    const/16 v40, -0xbee

    aput v40, v0, v39

    const/16 v39, -0xc

    aput v39, v0, v38

    const/16 v38, -0x6f8d

    aput v38, v0, v37

    const/16 v37, -0x70

    aput v37, v0, v36

    const/16 v36, 0x2a5c

    aput v36, v0, v35

    const/16 v35, 0x2d2a

    aput v35, v0, v34

    const/16 v34, 0x282d

    aput v34, v0, v33

    const/16 v33, 0x5b28

    aput v33, v0, v32

    const/16 v32, 0x5a5b

    aput v32, v0, v31

    const/16 v31, -0x57a6

    aput v31, v0, v30

    const/16 v30, -0x58

    aput v30, v0, v29

    const/16 v29, -0x6a9f

    aput v29, v0, v28

    const/16 v28, -0x6b

    aput v28, v0, v27

    const/16 v27, -0x37

    aput v27, v0, v26

    const/16 v26, -0x35

    aput v26, v0, v25

    const/16 v25, -0x4d8d

    aput v25, v0, v24

    const/16 v24, -0x4e

    aput v24, v0, v23

    const/16 v23, -0x63df

    aput v23, v0, v22

    const/16 v22, -0x64

    aput v22, v0, v21

    const/16 v21, -0x45

    aput v21, v0, v20

    const/16 v20, -0x4582

    aput v20, v0, v19

    const/16 v19, -0x46

    aput v19, v0, v18

    const/16 v18, -0x47

    aput v18, v0, v17

    const/16 v17, -0x7aa

    aput v17, v0, v16

    const/16 v16, -0x8

    aput v16, v0, v15

    const/16 v15, 0x164

    aput v15, v0, v14

    const/16 v14, -0x6bff

    aput v14, v0, v13

    const/16 v13, -0x6c

    aput v13, v0, v12

    const/16 v12, -0x3ce7

    aput v12, v0, v11

    const/16 v11, -0x3d

    aput v11, v0, v10

    const/16 v10, -0x10e1

    aput v10, v0, v9

    const/16 v9, -0x11

    aput v9, v0, v8

    const/16 v8, -0x3e

    aput v8, v0, v7

    const/16 v7, -0x11

    aput v7, v0, v6

    const/16 v6, -0x49b8

    aput v6, v0, v5

    const/16 v5, -0x4a

    aput v5, v0, v4

    const/16 v4, -0x7893

    aput v4, v0, v3

    const/16 v3, -0x79

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private roundNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private unbindService()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->finish()V

    return-void
.end method


# virtual methods
.method public getViewForContentUri(Landroid/net/Uri;)Landroid/view/View;
    .locals 21

    const/4 v2, 0x3

    new-array v6, v2, [J

    const/4 v2, 0x2

    const-wide/16 v3, 0x3

    aput-wide v3, v6, v2

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/service/health/R$string;->contentui_single_message:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/sec/android/service/health/R$string;->bloodglucose:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const/16 v2, 0xc

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, -0xe

    aput v18, v3, v17

    const/16 v17, -0x9

    aput v17, v3, v16

    const/16 v16, -0x19e8

    aput v16, v3, v15

    const/16 v15, -0x7e

    aput v15, v3, v14

    const/16 v14, 0x160c

    aput v14, v3, v13

    const/16 v13, -0x89e

    aput v13, v3, v12

    const/16 v12, -0x67

    aput v12, v3, v11

    const/16 v11, -0x28

    aput v11, v3, v10

    const/16 v10, -0x7c

    aput v10, v3, v9

    const/16 v9, -0x16

    aput v9, v3, v8

    const/16 v8, -0x329c

    aput v8, v3, v4

    const/16 v4, -0x52

    aput v4, v3, v2

    const/16 v2, 0xc

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, -0x6d

    aput v19, v2, v18

    const/16 v18, -0x7d

    aput v18, v2, v17

    const/16 v17, -0x1987

    aput v17, v2, v16

    const/16 v16, -0x1a

    aput v16, v2, v15

    const/16 v15, 0x1653

    aput v15, v2, v14

    const/16 v14, -0x8ea

    aput v14, v2, v13

    const/16 v13, -0x9

    aput v13, v2, v12

    const/16 v12, -0x43

    aput v12, v2, v11

    const/16 v11, -0x10

    aput v11, v2, v10

    const/16 v10, -0x7c

    aput v10, v2, v9

    const/16 v9, -0x32f5

    aput v9, v2, v8

    const/16 v8, -0x33

    aput v8, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v8, v2

    if-lt v4, v8, :cond_0

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v8, v2

    if-lt v4, v8, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v7, v6, v4

    long-to-int v4, v7

    if-gtz v4, :cond_2

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    aget v8, v2, v4

    aget v9, v3, v4

    xor-int/2addr v8, v9

    aput v8, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    aget v8, v3, v4

    int-to-char v8, v8

    aput-char v8, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v7, 0x20

    shl-long/2addr v2, v7

    const/16 v7, 0x20

    ushr-long v7, v2, v7

    aget-wide v2, v6, v4

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_3

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_3
    const/16 v9, 0x20

    ushr-long/2addr v2, v9

    const/16 v9, 0x20

    shl-long/2addr v2, v9

    xor-long/2addr v2, v7

    const-wide v7, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v7

    aput-wide v2, v6, v4

    :goto_3
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_6

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0xa8f

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0xabf

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_4
    array-length v6, v2

    if-lt v4, v6, :cond_4

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5
    array-length v6, v2

    if-lt v4, v6, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_4
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_5
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v7, 0x0

    cmp-long v4, v2, v7

    if-eqz v4, :cond_7

    const-wide v7, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v7

    :cond_7
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_a

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x3aa

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x39a

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_6
    array-length v6, v2

    if-lt v4, v6, :cond_8

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_7
    array-length v6, v2

    if-lt v4, v6, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_8
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_9
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_a
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_b

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_b
    const/16 v8, 0x20

    shl-long/2addr v2, v8

    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v4, 0x0

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_c

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-eqz v7, :cond_d

    const-wide v7, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v7

    :cond_d
    const/16 v7, 0x20

    shl-long/2addr v2, v7

    const/16 v7, 0x20

    shr-long/2addr v2, v7

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v7, v6, v3

    long-to-int v3, v7

    if-gtz v3, :cond_e

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_e
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v7, 0x20

    shl-long/2addr v2, v7

    const/16 v7, 0x20

    ushr-long v7, v2, v7

    aget-wide v2, v6, v4

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_f

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_f
    const/16 v9, 0x20

    ushr-long/2addr v2, v9

    const/16 v9, 0x20

    shl-long/2addr v2, v9

    xor-long/2addr v2, v7

    const-wide v7, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v7

    aput-wide v2, v6, v4

    goto/16 :goto_3

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v3, Lcom/sec/android/service/health/R$string;->health_content_title_received_data:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x1

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v8, v6, v4

    long-to-int v4, v8

    if-lt v3, v4, :cond_11

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_12

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_12
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    :goto_8
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_15

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, -0x1

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x32

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_9
    array-length v6, v2

    if-lt v4, v6, :cond_13

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_a
    array-length v6, v2

    if-lt v4, v6, :cond_14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_13
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_14
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_15
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v4, v2, v8

    if-eqz v4, :cond_16

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_16
    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_19

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x7d5

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x7e6

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_b
    array-length v6, v2

    if-lt v4, v6, :cond_17

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_c
    array-length v6, v2

    if-lt v4, v6, :cond_18

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_17
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    :cond_18
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    :cond_19
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_1a

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_1a
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->roundNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x26

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v9, -0x6

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_d
    array-length v9, v2

    if-lt v4, v9, :cond_1b

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_e
    array-length v9, v2

    if-lt v4, v9, :cond_1c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x4679

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, 0x4654

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_f
    array-length v9, v2

    if-lt v4, v9, :cond_1d

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_10
    array-length v9, v2

    if-lt v4, v9, :cond_1e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_21

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x7668

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x7659

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_11
    array-length v6, v2

    if-lt v4, v6, :cond_1f

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_12
    array-length v6, v2

    if-lt v4, v6, :cond_20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1b
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_d

    :cond_1c
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_e

    :cond_1d
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    :cond_1e
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    :cond_1f
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_11

    :cond_20
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    :cond_21
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_22

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_22
    const/16 v10, 0x20

    shr-long/2addr v2, v10

    long-to-int v2, v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual {v8, v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getBloodGlucoseUnit(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_25

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x3f

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x10

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_13
    array-length v6, v2

    if-lt v4, v6, :cond_23

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_14
    array-length v6, v2

    if-lt v4, v6, :cond_24

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_23
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_13

    :cond_24
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_14

    :cond_25
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_26

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_26
    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-wide v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x1

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-lt v4, v2, :cond_27

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_27
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_28

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_28
    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v8, v6, v3

    long-to-int v3, v8

    if-lt v4, v3, :cond_29

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_29
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_2a

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_2a
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    goto/16 :goto_8

    :cond_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentViewLIstViewLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    sget v2, Lcom/sec/android/service/health/R$id;->contentui_list_description:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textListDesc:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textListDesc:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/service/health/R$string;->contentui_multiple_message:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/sec/android/service/health/R$string;->bloodglucose:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1, v5, v7}, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;-><init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentValueListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_2c
    :goto_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentParentLayout:Landroid/widget/LinearLayout;

    return-object v2

    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->roundNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    const/4 v3, 0x0

    array-length v7, v6

    add-int/lit8 v7, v7, -0x1

    aget-wide v7, v6, v7

    long-to-int v7, v7

    if-gtz v7, :cond_2e

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2e
    const/4 v7, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    const/16 v8, 0x20

    ushr-long v8, v2, v8

    aget-wide v2, v6, v7

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_2f

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_2f
    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    const/16 v10, 0x20

    shl-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-wide v7, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_32

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x1e

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x2e

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_16
    array-length v6, v2

    if-lt v4, v6, :cond_30

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_17
    array-length v6, v2

    if-lt v4, v6, :cond_31

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_30
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_16

    :cond_31
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_17

    :cond_32
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v6, v2, v10

    if-eqz v6, :cond_33

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_33
    const/16 v6, 0x20

    shl-long/2addr v2, v6

    const/16 v6, 0x20

    shr-long/2addr v2, v6

    long-to-int v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getBloodGlucoseUnit(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentParentLayout_value_total:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue_symbol:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v3, Lcom/sec/android/service/health/R$string;->health_content_title_received_data:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v7, v8}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->dateFormet:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textDate:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->timeFormet:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textTime:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentBloodGlucose_layout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->bloodGlucose_switch:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/sec/android/service/health/contentservice/HealthContentShow$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/service/health/contentservice/HealthContentShow$1;-><init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto/16 :goto_15

    :catch_1
    move-exception v2

    :cond_34
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_99

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/service/health/R$string;->contentui_single_message:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/sec/android/service/health/R$string;->weight:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0xc

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, -0x4ad2

    aput v18, v3, v17

    const/16 v17, -0x3f

    aput v17, v3, v16

    const/16 v16, 0x4528

    aput v16, v3, v15

    const/16 v15, 0x6121

    aput v15, v3, v14

    const/16 v14, 0x343e

    aput v14, v3, v13

    const/16 v13, -0x43c0

    aput v13, v3, v12

    const/16 v12, -0x2e

    aput v12, v3, v11

    const/16 v11, 0x460f

    aput v11, v3, v10

    const/16 v10, 0x3a32

    aput v10, v3, v9

    const/16 v9, -0x21ac

    aput v9, v3, v8

    const/16 v8, -0x4f

    aput v8, v3, v4

    const/16 v4, 0x437

    aput v4, v3, v2

    const/16 v2, 0xc

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, -0x4ab1

    aput v19, v2, v18

    const/16 v18, -0x4b

    aput v18, v2, v17

    const/16 v17, 0x4549

    aput v17, v2, v16

    const/16 v16, 0x6145

    aput v16, v2, v15

    const/16 v15, 0x3461

    aput v15, v2, v14

    const/16 v14, -0x43cc

    aput v14, v2, v13

    const/16 v13, -0x44

    aput v13, v2, v12

    const/16 v12, 0x466a

    aput v12, v2, v11

    const/16 v11, 0x3a46

    aput v11, v2, v10

    const/16 v10, -0x21c6

    aput v10, v2, v9

    const/16 v9, -0x22

    aput v9, v2, v8

    const/16 v8, 0x454

    aput v8, v2, v4

    const/4 v4, 0x0

    :goto_18
    array-length v8, v2

    if-lt v4, v8, :cond_35

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_19
    array-length v8, v2

    if-lt v4, v8, :cond_36

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_1a
    const/4 v2, 0x0

    const/4 v3, 0x0

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v6, v4

    long-to-int v4, v4

    if-gtz v4, :cond_37

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_35
    aget v8, v2, v4

    aget v9, v3, v4

    xor-int/2addr v8, v9

    aput v8, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_18

    :cond_36
    aget v8, v3, v4

    int-to-char v8, v8

    aput-char v8, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    :catch_2
    move-exception v2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    goto :goto_1a

    :cond_37
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    ushr-long v8, v2, v5

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v5, v2, v10

    if-eqz v5, :cond_38

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_38
    const/16 v5, 0x20

    ushr-long/2addr v2, v5

    const/16 v5, 0x20

    shl-long/2addr v2, v5

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    :goto_1b
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_3b

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x23

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x13

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_1c
    array-length v6, v2

    if-lt v4, v6, :cond_39

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1d
    array-length v6, v2

    if-lt v4, v6, :cond_3a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_39
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1c

    :cond_3a
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1d

    :cond_3b
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_3c

    const-wide v4, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v4

    :cond_3c
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_45

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_3f

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x79

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x49

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_1e
    array-length v6, v2

    if-lt v4, v6, :cond_3d

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1f
    array-length v6, v2

    if-lt v4, v6, :cond_3e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3d
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1e

    :cond_3e
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1f

    :cond_3f
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_40

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_40
    const/16 v8, 0x20

    shl-long/2addr v2, v8

    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v4, 0x0

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_41

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_41
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-eqz v5, :cond_42

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_42
    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    shr-long/2addr v2, v5

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v8, v6, v3

    long-to-int v3, v8

    if-gtz v3, :cond_43

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_43
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    ushr-long v8, v2, v5

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v5, v2, v10

    if-eqz v5, :cond_44

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_44
    const/16 v5, 0x20

    ushr-long/2addr v2, v5

    const/16 v5, 0x20

    shl-long/2addr v2, v5

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    goto/16 :goto_1b

    :cond_45
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_8b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v3, Lcom/sec/android/service/health/R$string;->health_content_title_received_data:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v8, v6, v4

    long-to-int v4, v8

    if-lt v3, v4, :cond_46

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_46
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_47

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_47
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    :goto_20
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_4a

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0xa

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x39

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_21
    array-length v6, v2

    if-lt v4, v6, :cond_48

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_22
    array-length v6, v2

    if-lt v4, v6, :cond_49

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_48
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_21

    :cond_49
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_22

    :cond_4a
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v4, v2, v8

    if-eqz v4, :cond_4b

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_4b
    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    if-ltz v2, :cond_8a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_4e

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x31

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, -0x2

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_23
    array-length v6, v2

    if-lt v4, v6, :cond_4c

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_24
    array-length v6, v2

    if-lt v4, v6, :cond_4d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_4c
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_23

    :cond_4d
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_24

    :cond_4e
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_4f

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_4f
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual {v4, v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_52

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x4441

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x4470

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_25
    array-length v6, v2

    if-lt v4, v6, :cond_50

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_26
    array-length v6, v2

    if-lt v4, v6, :cond_51

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_50
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_25

    :cond_51
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_26

    :cond_52
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_53

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_53
    const/16 v10, 0x20

    shr-long/2addr v2, v10

    long-to-int v2, v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->device_unit:I

    invoke-virtual {v8, v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(I)Ljava/lang/String;

    move-result-object v2

    if-ne v4, v2, :cond_60

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_56

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x60eb

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x60dc

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_27
    array-length v6, v2

    if-lt v4, v6, :cond_54

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_28
    array-length v6, v2

    if-lt v4, v6, :cond_55

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_54
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_27

    :cond_55
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_28

    :cond_56
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_57

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_57
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x61

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x41

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_29
    array-length v9, v2

    if-lt v4, v9, :cond_58

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_2a
    array-length v9, v2

    if-lt v4, v9, :cond_59

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x5c

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x77

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_2b
    array-length v9, v2

    if-lt v4, v9, :cond_5a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_2c
    array-length v9, v2

    if-lt v4, v9, :cond_5b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_5e

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x62

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x51

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_2d
    array-length v6, v2

    if-lt v4, v6, :cond_5c

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_2e
    array-length v6, v2

    if-lt v4, v6, :cond_5d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_58
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_29

    :cond_59
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2a

    :cond_5a
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2b

    :cond_5b
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2c

    :cond_5c
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2d

    :cond_5d
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2e

    :cond_5e
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_5f

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_5f
    const/16 v10, 0x20

    shr-long/2addr v2, v10

    long-to-int v2, v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_84

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x75

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x46

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_30
    array-length v6, v2

    if-lt v4, v6, :cond_82

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_31
    array-length v6, v2

    if-lt v4, v6, :cond_83

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_60
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_63

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x73

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x44

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_32
    array-length v6, v2

    if-lt v4, v6, :cond_61

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_33
    array-length v6, v2

    if-lt v4, v6, :cond_62

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_61
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_32

    :cond_62
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_33

    :cond_63
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_64

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_64
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual {v4, v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    if-ne v2, v3, :cond_71

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_67

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x40

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0xf

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_34
    array-length v6, v2

    if-lt v4, v6, :cond_65

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_35
    array-length v6, v2

    if-lt v4, v6, :cond_66

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_65
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_34

    :cond_66
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_35

    :cond_67
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_68

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_68
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x7f

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x5f

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_36
    array-length v9, v2

    if-lt v4, v9, :cond_69

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_37
    array-length v9, v2

    if-lt v4, v9, :cond_6a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x5f

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x74

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_38
    array-length v9, v2

    if-lt v4, v9, :cond_6b

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_39
    array-length v9, v2

    if-lt v4, v9, :cond_6c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_6f

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x13f5

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x13c6

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_3a
    array-length v6, v2

    if-lt v4, v6, :cond_6d

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3b
    array-length v6, v2

    if-lt v4, v6, :cond_6e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_69
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_36

    :cond_6a
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_37

    :cond_6b
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_38

    :cond_6c
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_39

    :cond_6d
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3a

    :cond_6e
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3b

    :cond_6f
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_70

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_70
    const/16 v10, 0x20

    shr-long/2addr v2, v10

    long-to-int v2, v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->device_unit:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2f

    :cond_71
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_74

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x6006

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x6037

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_3c
    array-length v6, v2

    if-lt v4, v6, :cond_72

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3d
    array-length v6, v2

    if-lt v4, v6, :cond_73

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_72
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3c

    :cond_73
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3d

    :cond_74
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_75

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_75
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_78

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x3664

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x3655

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_3e
    array-length v6, v2

    if-lt v4, v6, :cond_76

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3f
    array-length v6, v2

    if-lt v4, v6, :cond_77

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_76
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3e

    :cond_77
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3f

    :cond_78
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v11, 0x0

    cmp-long v11, v2, v11

    if-eqz v11, :cond_79

    const-wide v11, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v11

    :cond_79
    const/16 v11, 0x20

    shr-long/2addr v2, v11

    long-to-int v2, v2

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual {v9, v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getWeightUnitValue(FLjava/lang/String;)F

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x65ca

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x65ea

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_40
    array-length v9, v2

    if-lt v4, v9, :cond_7a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_41
    array-length v9, v2

    if-lt v4, v9, :cond_7b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x6a

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x45

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_42
    array-length v9, v2

    if-lt v4, v9, :cond_7c

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_43
    array-length v9, v2

    if-lt v4, v9, :cond_7d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_80

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x61

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x52

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_44
    array-length v6, v2

    if-lt v4, v6, :cond_7e

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_45
    array-length v6, v2

    if-lt v4, v6, :cond_7f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_7a
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_40

    :cond_7b
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_41

    :cond_7c
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_42

    :cond_7d
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_43

    :cond_7e
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_44

    :cond_7f
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_45

    :cond_80
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_81

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_81
    const/16 v10, 0x20

    shr-long/2addr v2, v10

    long-to-int v2, v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2f

    :cond_82
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_30

    :cond_83
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_31

    :cond_84
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_85

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_85
    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-wide v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x1

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-lt v4, v2, :cond_86

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_86
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_87

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_87
    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    add-int/lit8 v2, v2, -0x1

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v8, v6, v3

    long-to-int v3, v8

    if-lt v4, v3, :cond_88

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_88
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_89

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_89
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    goto/16 :goto_20

    :cond_8a
    sget v2, Lcom/sec/android/service/health/R$id;->contentui_list_description:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textListDesc:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textListDesc:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/service/health/R$string;->contentui_multiple_message:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/sec/android/service/health/R$string;->weight:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentViewLIstViewLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v2, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1, v5, v7}, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;-><init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentValueListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_15

    :cond_8b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    const/4 v3, 0x0

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v6, v4

    long-to-int v4, v4

    if-gtz v4, :cond_8c

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8c
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    ushr-long v8, v2, v5

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v5, v2, v10

    if-eqz v5, :cond_8d

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_8d
    const/16 v5, 0x20

    ushr-long/2addr v2, v5

    const/16 v5, 0x20

    shl-long/2addr v2, v5

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-wide v8, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_90

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x6d

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x5d

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_46
    array-length v6, v2

    if-lt v4, v6, :cond_8e

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_47
    array-length v6, v2

    if-lt v4, v6, :cond_8f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_8e
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_46

    :cond_8f
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_47

    :cond_90
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v5, v2, v10

    if-eqz v5, :cond_91

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_91
    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    shr-long/2addr v2, v5

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->device_unit:I

    invoke-virtual {v4, v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_92

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    :goto_48
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentParentLayout_value_total:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v4, Lcom/sec/android/service/health/R$string;->health_content_title_received_data:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    new-instance v10, Ljava/text/DecimalFormat;

    const/4 v3, 0x5

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v11, 0x2

    const/4 v12, 0x3

    const/4 v13, 0x4

    const/16 v14, -0x2e

    aput v14, v4, v13

    const/16 v13, -0x14

    aput v13, v4, v12

    const/16 v12, -0x69

    aput v12, v4, v11

    const/16 v11, -0x49

    aput v11, v4, v5

    const/16 v5, -0x51

    aput v5, v4, v3

    const/4 v3, 0x5

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x2

    const/4 v13, 0x3

    const/4 v14, 0x4

    const/16 v15, -0xf

    aput v15, v3, v14

    const/16 v14, -0x3e

    aput v14, v3, v13

    const/16 v13, -0x4c

    aput v13, v3, v12

    const/16 v12, -0x6c

    aput v12, v3, v11

    const/16 v11, -0x74

    aput v11, v3, v5

    const/4 v5, 0x0

    :goto_49
    array-length v11, v3

    if-lt v5, v11, :cond_93

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_4a
    array-length v11, v3

    if-lt v5, v11, :cond_94

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v10, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->d:Ljava/text/DecimalFormat;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->d:Ljava/text/DecimalFormat;

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_97

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, -0x3

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x33

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_4b
    array-length v6, v2

    if-lt v4, v6, :cond_95

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_4c
    array-length v6, v2

    if-lt v4, v6, :cond_96

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_92
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getWeightUnitValue(FLjava/lang/String;)F

    move-result v2

    goto/16 :goto_48

    :cond_93
    aget v11, v3, v5

    aget v12, v4, v5

    xor-int/2addr v11, v12

    aput v11, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_49

    :cond_94
    aget v11, v4, v5

    int-to-char v11, v11

    aput-char v11, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4a

    :cond_95
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4b

    :cond_96
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4c

    :cond_97
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-eqz v5, :cond_98

    const-wide v5, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v5

    :cond_98
    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    shr-long/2addr v2, v5

    long-to-int v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->getWeightUnit(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue_symbol:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v8, v9}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->dateFormet:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textDate:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->timeFormet:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textTime:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_15

    :catch_3
    move-exception v2

    :cond_99
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_5

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2c

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/service/health/R$string;->contentui_single_message:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/sec/android/service/health/R$string;->bloodpressure:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const/16 v2, 0xc

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, -0x36

    aput v19, v3, v18

    const/16 v18, -0x29d3

    aput v18, v3, v17

    const/16 v17, -0x49

    aput v17, v3, v16

    const/16 v16, -0x7f

    aput v16, v3, v15

    const/16 v15, -0x20

    aput v15, v3, v14

    const/16 v14, 0x2658

    aput v14, v3, v13

    const/16 v13, -0x7b8

    aput v13, v3, v12

    const/16 v12, -0x63

    aput v12, v3, v11

    const/16 v11, 0xb05

    aput v11, v3, v10

    const/16 v10, -0x499b

    aput v10, v3, v9

    const/16 v9, -0x27

    aput v9, v3, v4

    const/16 v4, -0x33

    aput v4, v3, v2

    const/16 v2, 0xc

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x2

    const/4 v11, 0x3

    const/4 v12, 0x4

    const/4 v13, 0x5

    const/4 v14, 0x6

    const/4 v15, 0x7

    const/16 v16, 0x8

    const/16 v17, 0x9

    const/16 v18, 0xa

    const/16 v19, 0xb

    const/16 v20, -0x55

    aput v20, v2, v19

    const/16 v19, -0x29a7

    aput v19, v2, v18

    const/16 v18, -0x2a

    aput v18, v2, v17

    const/16 v17, -0x1b

    aput v17, v2, v16

    const/16 v16, -0x41

    aput v16, v2, v15

    const/16 v15, 0x262c

    aput v15, v2, v14

    const/16 v14, -0x7da

    aput v14, v2, v13

    const/4 v13, -0x8

    aput v13, v2, v12

    const/16 v12, 0xb71

    aput v12, v2, v11

    const/16 v11, -0x49f5

    aput v11, v2, v10

    const/16 v10, -0x4a

    aput v10, v2, v9

    const/16 v9, -0x52

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_4d
    array-length v9, v2

    if-lt v4, v9, :cond_9a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_4e
    array-length v9, v2

    if-lt v4, v9, :cond_9b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_4

    :goto_4f
    const/4 v2, 0x0

    const/4 v3, 0x0

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v8, v6, v4

    long-to-int v4, v8

    if-gtz v4, :cond_9c

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_9a
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4d

    :cond_9b
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4e

    :catch_4
    move-exception v2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    goto :goto_4f

    :cond_9c
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    const/16 v8, 0x20

    ushr-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_9d

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_9d
    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    const/16 v10, 0x20

    shl-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    :goto_50
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_a0

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x1e2f

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x1e1f

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_51
    array-length v6, v2

    if-lt v4, v6, :cond_9e

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_52
    array-length v6, v2

    if-lt v4, v6, :cond_9f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_9e
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_51

    :cond_9f
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_52

    :cond_a0
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v4, v2, v8

    if-eqz v4, :cond_a1

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_a1
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_aa

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_a4

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x1e

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x2e

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_53
    array-length v6, v2

    if-lt v4, v6, :cond_a2

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_54
    array-length v6, v2

    if-lt v4, v6, :cond_a3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_a2
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_53

    :cond_a3
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_54

    :cond_a4
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_a5

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_a5
    const/16 v9, 0x20

    shl-long/2addr v2, v9

    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v4, 0x0

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-gtz v2, :cond_a6

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_a6
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_a7

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_a7
    const/16 v8, 0x20

    shl-long/2addr v2, v8

    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v8, v6, v3

    long-to-int v3, v8

    if-gtz v3, :cond_a8

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_a8
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    const/16 v8, 0x20

    ushr-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_a9

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_a9
    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    const/16 v10, 0x20

    shl-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    goto/16 :goto_50

    :cond_aa
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_c8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v3, Lcom/sec/android/service/health/R$string;->health_content_title_received_data:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x1

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v8, v6, v4

    long-to-int v4, v8

    if-lt v3, v4, :cond_ab

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_ab
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_ac

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_ac
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    :goto_55
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_af

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x41be

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x418d

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_56
    array-length v6, v2

    if-lt v4, v6, :cond_ad

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_57
    array-length v6, v2

    if-lt v4, v6, :cond_ae

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_ad
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_56

    :cond_ae
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_57

    :cond_af
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v4, v2, v8

    if-eqz v4, :cond_b0

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_b0
    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_c7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_b3

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x3d

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0xe

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_58
    array-length v6, v2

    if-lt v4, v6, :cond_b1

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_59
    array-length v6, v2

    if-lt v4, v6, :cond_b2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_b1
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_58

    :cond_b2
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_59

    :cond_b3
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_b4

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_b4
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->roundNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x4cae

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x4c83

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_5a
    array-length v9, v2

    if-lt v4, v9, :cond_b5

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5b
    array-length v9, v2

    if-lt v4, v9, :cond_b6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_b9

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x79ec

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x79db

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_5c
    array-length v6, v2

    if-lt v4, v6, :cond_b7

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5d
    array-length v6, v2

    if-lt v4, v6, :cond_b8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_b5
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5a

    :cond_b6
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5b

    :cond_b7
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5c

    :cond_b8
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5d

    :cond_b9
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-eqz v9, :cond_ba

    const-wide v9, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v9

    :cond_ba
    const/16 v9, 0x20

    shr-long/2addr v2, v9

    long-to-int v2, v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->roundNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x60

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x80

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_5e
    array-length v9, v2

    if-lt v4, v9, :cond_bb

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5f
    array-length v9, v2

    if-lt v4, v9, :cond_bc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x3f

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v9, -0x14

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_60
    array-length v9, v2

    if-lt v4, v9, :cond_bd

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_61
    array-length v9, v2

    if-lt v4, v9, :cond_be

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/service/health/R$string;->bloodpressure_unit:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_c1

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x4b

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x7c

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_62
    array-length v6, v2

    if-lt v4, v6, :cond_bf

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_63
    array-length v6, v2

    if-lt v4, v6, :cond_c0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_bb
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5e

    :cond_bc
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5f

    :cond_bd
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_60

    :cond_be
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_61

    :cond_bf
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_62

    :cond_c0
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_63

    :cond_c1
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_c2

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_c2
    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-wide v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x1

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-lt v4, v2, :cond_c3

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c3
    const/4 v2, 0x0

    aget-wide v2, v6, v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_c4

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    :cond_c4
    const/16 v8, 0x20

    shr-long/2addr v2, v8

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v8, v6, v3

    long-to-int v3, v8

    if-lt v4, v3, :cond_c5

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c5
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v8, 0x20

    shl-long v8, v2, v8

    aget-wide v2, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_c6

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_c6
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    ushr-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v8

    aput-wide v2, v6, v4

    goto/16 :goto_55

    :cond_c7
    sget v2, Lcom/sec/android/service/health/R$id;->contentui_list_description:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textListDesc:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textListDesc:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/service/health/R$string;->contentui_multiple_message:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/sec/android/service/health/R$string;->bloodpressure:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentViewLIstViewLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v2, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1, v5, v7}, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;-><init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentValueListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_15

    :cond_c8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v4, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-wide v8, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    const/4 v2, 0x0

    const/4 v3, 0x2

    array-length v10, v6

    add-int/lit8 v10, v10, -0x1

    aget-wide v10, v6, v10

    long-to-int v10, v10

    if-lt v3, v10, :cond_c9

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c9
    const/4 v10, 0x1

    int-to-long v2, v2

    const/16 v11, 0x20

    shl-long/2addr v2, v11

    const/16 v11, 0x20

    ushr-long v11, v2, v11

    aget-wide v2, v6, v10

    const-wide/16 v13, 0x0

    cmp-long v13, v2, v13

    if-eqz v13, :cond_ca

    const-wide v13, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v13

    :cond_ca
    const/16 v13, 0x20

    ushr-long/2addr v2, v13

    const/16 v13, 0x20

    shl-long/2addr v2, v13

    xor-long/2addr v2, v11

    const-wide v11, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v11

    aput-wide v2, v6, v10

    :goto_64
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_cd

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x4ff6

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x4fc8

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_65
    array-length v6, v2

    if-lt v4, v6, :cond_cb

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_66
    array-length v6, v2

    if-lt v4, v6, :cond_cc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_cb
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_65

    :cond_cc
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_66

    :cond_cd
    const/4 v2, 0x1

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_ce

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_ce
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    shr-long/2addr v2, v10

    long-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_db

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_d1

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x3a79

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x3a4b

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_67
    array-length v6, v2

    if-lt v4, v6, :cond_cf

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_68
    array-length v6, v2

    if-lt v4, v6, :cond_d0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_cf
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_67

    :cond_d0
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_68

    :cond_d1
    const/4 v2, 0x1

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_d2

    const-wide v10, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v10

    :cond_d2
    const/16 v10, 0x20

    shl-long/2addr v2, v10

    const/16 v10, 0x20

    shr-long/2addr v2, v10

    long-to-int v10, v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_d5

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x4b55

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x4b67

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_69
    array-length v6, v2

    if-lt v4, v6, :cond_d3

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_6a
    array-length v6, v2

    if-lt v4, v6, :cond_d4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_d3
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_69

    :cond_d4
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_6a

    :cond_d5
    const/4 v2, 0x1

    aget-wide v2, v6, v2

    const-wide/16 v12, 0x0

    cmp-long v12, v2, v12

    if-eqz v12, :cond_d6

    const-wide v12, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v12

    :cond_d6
    const/16 v12, 0x20

    shl-long/2addr v2, v12

    const/16 v12, 0x20

    shr-long/2addr v2, v12

    long-to-int v2, v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->roundNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v10

    const/4 v10, 0x2

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v6, v2

    long-to-int v2, v2

    if-lt v10, v2, :cond_d7

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_d7
    const/4 v2, 0x1

    aget-wide v2, v6, v2

    const-wide/16 v11, 0x0

    cmp-long v11, v2, v11

    if-eqz v11, :cond_d8

    const-wide v11, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v11

    :cond_d8
    const/16 v11, 0x20

    shl-long/2addr v2, v11

    const/16 v11, 0x20

    shr-long/2addr v2, v11

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v11, v6, v3

    long-to-int v3, v11

    if-lt v10, v3, :cond_d9

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_d9
    const/4 v10, 0x1

    int-to-long v2, v2

    const/16 v11, 0x20

    shl-long/2addr v2, v11

    const/16 v11, 0x20

    ushr-long v11, v2, v11

    aget-wide v2, v6, v10

    const-wide/16 v13, 0x0

    cmp-long v13, v2, v13

    if-eqz v13, :cond_da

    const-wide v13, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v13

    :cond_da
    const/16 v13, 0x20

    ushr-long/2addr v2, v13

    const/16 v13, 0x20

    shl-long/2addr v2, v13

    xor-long/2addr v2, v11

    const-wide v11, -0x43c0c36eeea52b8bL    # -1.6933388588265638E-18

    xor-long/2addr v2, v11

    aput-wide v2, v6, v10

    goto/16 :goto_64

    :cond_db
    array-length v2, v4

    const/4 v3, 0x3

    if-ne v2, v3, :cond_de

    const/4 v2, 0x0

    aget-object v2, v4, v2

    const/4 v3, 0x1

    aget-object v5, v4, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x6e

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v10, -0x43

    aput v10, v2, v4

    const/4 v4, 0x0

    :goto_6b
    array-length v10, v2

    if-lt v4, v10, :cond_dc

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_6c
    array-length v10, v2

    if-lt v4, v10, :cond_dd

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_6d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentParentLayout_value_total:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v3, Lcom/sec/android/service/health/R$string;->health_content_title_received_data:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue_symbol:Landroid/widget/TextView;

    sget v3, Lcom/sec/android/service/health/R$string;->bloodpressure_unit:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v8, v9}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->dateFormet:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textDate:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->timeFormet:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textTime:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_15

    :cond_dc
    aget v10, v2, v4

    aget v11, v3, v4

    xor-int/2addr v10, v11

    aput v10, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_6b

    :cond_dd
    aget v10, v3, v4

    int-to-char v10, v10

    aput-char v10, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_6c

    :catch_5
    move-exception v2

    goto/16 :goto_15

    :cond_de
    move-object v2, v5

    goto :goto_6d
.end method

.method public onClick(Landroid/view/View;)V
    .locals 52

    const/4 v1, 0x3

    new-array v4, v1, [J

    const/4 v1, 0x2

    const-wide/16 v2, 0x3

    aput-wide v2, v4, v1

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->closeByApp:Z

    const/4 v1, 0x1

    new-array v1, v1, [Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput-boolean v3, v1, v2

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mAccpet:[Z

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/16 v12, -0x2e8c

    aput v12, v2, v11

    const/16 v11, -0x72

    aput v11, v2, v10

    const/16 v10, -0x4fbd

    aput v10, v2, v9

    const/16 v9, -0x24

    aput v9, v2, v8

    const/16 v8, -0x2f

    aput v8, v2, v7

    const/16 v7, -0x3fd2

    aput v7, v2, v3

    const/16 v3, -0x61

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/16 v13, -0x2ed5

    aput v13, v1, v12

    const/16 v12, -0x2f

    aput v12, v1, v11

    const/16 v11, -0x4fd1

    aput v11, v1, v10

    const/16 v10, -0x50

    aput v10, v1, v9

    const/16 v9, -0x50

    aput v9, v1, v8

    const/16 v8, -0x3f8f

    aput v8, v1, v7

    const/16 v7, -0x40

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v7, v1

    if-lt v3, v7, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v7, v1

    if-lt v3, v7, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mApprovedColumns:[Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/service/health/R$id;->contentui_dialog_ok_bottom:I

    if-ne v1, v2, :cond_4e

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x58

    aput v13, v2, v12

    const/16 v12, 0x2259

    aput v12, v2, v11

    const/16 v11, -0x23b5

    aput v11, v2, v10

    const/16 v10, -0x56

    aput v10, v2, v9

    const/16 v9, -0x42

    aput v9, v2, v8

    const/16 v8, -0x318f

    aput v8, v2, v7

    const/16 v7, -0x63

    aput v7, v2, v6

    const/16 v6, -0xf

    aput v6, v2, v5

    const/16 v5, -0x51

    aput v5, v2, v3

    const/16 v3, 0x2046

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x33

    aput v14, v1, v13

    const/16 v13, 0x223a

    aput v13, v1, v12

    const/16 v12, -0x23de

    aput v12, v1, v11

    const/16 v11, -0x24

    aput v11, v1, v10

    const/16 v10, -0x34

    aput v10, v1, v9

    const/16 v9, -0x31ec

    aput v9, v1, v8

    const/16 v8, -0x32

    aput v8, v1, v7

    const/16 v7, -0x6d

    aput v7, v1, v6

    const/16 v6, -0x3a

    aput v6, v1, v5

    const/16 v5, 0x202a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0x5f23

    aput v18, v2, v17

    const/16 v17, -0x47c4

    aput v17, v2, v16

    const/16 v16, -0x2f

    aput v16, v2, v15

    const/16 v15, -0x25

    aput v15, v2, v14

    const/16 v14, -0x10

    aput v14, v2, v13

    const/16 v13, -0x76

    aput v13, v2, v12

    const/16 v12, -0xe

    aput v12, v2, v11

    const/16 v11, -0x56

    aput v11, v2, v10

    const/16 v10, 0x5f69

    aput v10, v2, v9

    const/16 v9, 0x31

    aput v9, v2, v8

    const/16 v8, 0x1d6f

    aput v8, v2, v7

    const/16 v7, -0x35c3

    aput v7, v2, v6

    const/16 v6, -0x5c

    aput v6, v2, v3

    const/16 v3, -0x2b

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0x5f48

    aput v19, v1, v18

    const/16 v18, -0x47a1

    aput v18, v1, v17

    const/16 v17, -0x48

    aput v17, v1, v16

    const/16 v16, -0x49

    aput v16, v1, v15

    const/16 v15, -0x6d

    aput v15, v1, v14

    const/16 v14, -0x56

    aput v14, v1, v13

    const/16 v13, -0x47

    aput v13, v1, v12

    const/16 v12, -0x1b

    aput v12, v1, v11

    const/16 v11, 0x5f49

    aput v11, v1, v10

    const/16 v10, 0x5f

    aput v10, v1, v9

    const/16 v9, 0x1d00

    aput v9, v1, v8

    const/16 v8, -0x35e3

    aput v8, v1, v7

    const/16 v7, -0x36

    aput v7, v1, v6

    const/16 v6, -0x44

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/4 v13, -0x3

    aput v13, v2, v12

    const/16 v12, 0x5647

    aput v12, v2, v11

    const/16 v11, -0x5ec1

    aput v11, v2, v10

    const/16 v10, -0x29

    aput v10, v2, v9

    const/16 v9, 0x1d0b

    aput v9, v2, v8

    const/16 v8, -0x7988

    aput v8, v2, v7

    const/16 v7, -0x2b

    aput v7, v2, v6

    const/16 v6, 0xa

    aput v6, v2, v5

    const/16 v5, 0x3a69

    aput v5, v2, v3

    const/16 v3, -0x41aa

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x68

    aput v14, v1, v13

    const/16 v13, 0x5624

    aput v13, v1, v12

    const/16 v12, -0x5eaa

    aput v12, v1, v11

    const/16 v11, -0x5f

    aput v11, v1, v10

    const/16 v10, 0x1d79

    aput v10, v1, v9

    const/16 v9, -0x79e3

    aput v9, v1, v8

    const/16 v8, -0x7a

    aput v8, v1, v7

    const/16 v7, 0x68

    aput v7, v1, v6

    const/16 v6, 0x3a00

    aput v6, v1, v5

    const/16 v5, -0x41c6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->rEVx3t7fZ()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isDirectSensorData:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isDirectSensorData:Z

    if-eqz v1, :cond_3b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentValueListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentValueListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    const/4 v2, 0x0

    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v5, v4, v3

    long-to-int v3, v5

    if-gtz v3, :cond_8

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_8
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_9

    const-wide v7, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v7

    :cond_9
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->adapter:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->itemChecked:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->access$000(Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x5b43

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x5b73

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_c
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_d

    const-wide v5, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v5

    :cond_d
    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    shr-long/2addr v1, v5

    long-to-int v1, v1

    new-array v1, v1, [Z

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mChecked:[Z

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_10

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0xd

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x3d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_10
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_11

    const-wide v5, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v5

    :cond_11
    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    shr-long/2addr v1, v5

    long-to-int v1, v1

    new-array v1, v1, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mApprovedColumns:[Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-wide v5, v4, v5

    long-to-int v5, v5

    if-lt v2, v5, :cond_12

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_12
    const/4 v5, 0x0

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long v6, v1, v6

    aget-wide v1, v4, v5

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_13

    const-wide v8, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v8

    :cond_13
    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    aput-wide v1, v4, v5

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_16

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x76

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x46

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_14

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_14
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_15
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_16
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_17

    const-wide v5, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v5

    :cond_17
    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    shr-long/2addr v1, v5

    long-to-int v1, v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x2

    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-wide v5, v4, v5

    long-to-int v5, v5

    if-lt v2, v5, :cond_18

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_18
    const/4 v5, 0x1

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    ushr-long v6, v1, v6

    aget-wide v1, v4, v5

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_19

    const-wide v8, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v8

    :cond_19
    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    const/16 v8, 0x20

    shl-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    aput-wide v1, v4, v5

    :goto_e
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x7a8c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x7abb

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_1a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_1b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_1c
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_1d

    const-wide v5, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v5

    :cond_1d
    const/16 v5, 0x20

    shr-long/2addr v1, v5

    long-to-int v5, v1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_20

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x32

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, -0x2

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_1e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_1f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_20
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_21

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    :cond_21
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    if-ge v5, v1, :cond_3a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mChecked:[Z

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_24

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x321a

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x322b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_22

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_23

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_22
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_23
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_14

    :cond_24
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_25

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    :cond_25
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v6, v1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_28

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x6572

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x6540

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_15
    array-length v5, v1

    if-lt v3, v5, :cond_26

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_16
    array-length v5, v1

    if-lt v3, v5, :cond_27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_26
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_15

    :cond_27
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_16

    :cond_28
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_29

    const-wide v7, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v7

    :cond_29
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    aput-boolean v1, v5, v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mApprovedColumns:[Ljava/lang/String;

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_2c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x5efe

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x5ecd

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_17
    array-length v5, v1

    if-lt v3, v5, :cond_2a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_18
    array-length v5, v1

    if-lt v3, v5, :cond_2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_17

    :cond_2b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    :cond_2c
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_2d

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    :cond_2d
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v6, v1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->currentList:Ljava/util/ArrayList;

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_30

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x23

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x14

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_19
    array-length v5, v1

    if-lt v3, v5, :cond_2e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1a
    array-length v5, v1

    if-lt v3, v5, :cond_2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_19

    :cond_2f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1a

    :cond_30
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_31

    const-wide v8, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v8

    :cond_31
    const/16 v8, 0x20

    shr-long/2addr v1, v8

    long-to-int v1, v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    iget-object v1, v1, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    aput-object v1, v5, v6

    const/4 v5, 0x1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-lt v5, v1, :cond_32

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v5}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_32
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_33

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    :cond_33
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v6, v4, v2

    long-to-int v2, v6

    if-lt v5, v2, :cond_34

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v5}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_34
    const/4 v5, 0x0

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long v6, v1, v6

    aget-wide v1, v4, v5

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_35

    const-wide v8, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v8

    :cond_35
    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    aput-wide v1, v4, v5

    const/4 v5, 0x2

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-lt v5, v1, :cond_36

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v5}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_36
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_37

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    :cond_37
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    add-int/lit8 v1, v1, -0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v6, v4, v2

    long-to-int v2, v6

    if-lt v5, v2, :cond_38

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v5}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_38
    const/4 v5, 0x1

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    ushr-long v6, v1, v6

    aget-wide v1, v4, v5

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_39

    const-wide v8, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v8

    :cond_39
    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    const/16 v8, 0x20

    shl-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x289c5ba87b4435d6L    # 4.606147523752869E-113

    xor-long/2addr v1, v6

    aput-wide v1, v4, v5

    goto/16 :goto_e

    :cond_3a
    new-instance v4, Landroid/content/Intent;

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x11

    aput v50, v2, v49

    const/16 v49, -0x6bd6

    aput v49, v2, v48

    const/16 v48, -0x20

    aput v48, v2, v47

    const/16 v47, -0xbc1

    aput v47, v2, v46

    const/16 v46, -0x6f

    aput v46, v2, v45

    const/16 v45, -0xe

    aput v45, v2, v44

    const/16 v44, -0x2c

    aput v44, v2, v43

    const/16 v43, 0x3877

    aput v43, v2, v42

    const/16 v42, 0x225b

    aput v42, v2, v41

    const/16 v41, 0x600c

    aput v41, v2, v40

    const/16 v40, 0x7c12

    aput v40, v2, v39

    const/16 v39, 0x6613

    aput v39, v2, v38

    const/16 v38, -0x70eb

    aput v38, v2, v37

    const/16 v37, -0x1f

    aput v37, v2, v36

    const/16 v36, 0x4a60

    aput v36, v2, v35

    const/16 v35, -0x59c7

    aput v35, v2, v34

    const/16 v34, -0x78

    aput v34, v2, v33

    const/16 v33, -0x76

    aput v33, v2, v32

    const/16 v32, -0x7f

    aput v32, v2, v31

    const/16 v31, -0x3e

    aput v31, v2, v30

    const/16 v30, -0x28

    aput v30, v2, v29

    const/16 v29, -0x32

    aput v29, v2, v28

    const/16 v28, 0x1f56

    aput v28, v2, v27

    const/16 v27, 0x4331

    aput v27, v2, v26

    const/16 v26, 0x826

    aput v26, v2, v25

    const/16 v25, -0x7195

    aput v25, v2, v24

    const/16 v24, -0x19

    aput v24, v2, v23

    const/16 v23, -0x4e

    aput v23, v2, v22

    const/16 v22, -0x11

    aput v22, v2, v21

    const/16 v21, -0x7b

    aput v21, v2, v20

    const/16 v20, -0x6bfd

    aput v20, v2, v19

    const/16 v19, -0x46

    aput v19, v2, v18

    const/16 v18, -0x11

    aput v18, v2, v17

    const/16 v17, -0x3e

    aput v17, v2, v16

    const/16 v16, -0x3c

    aput v16, v2, v15

    const/16 v15, -0x57

    aput v15, v2, v14

    const/16 v14, 0x766e

    aput v14, v2, v13

    const/16 v13, -0x1ae8

    aput v13, v2, v12

    const/16 v12, -0x7c

    aput v12, v2, v11

    const/16 v11, -0x5d

    aput v11, v2, v10

    const/16 v10, 0xc75

    aput v10, v2, v9

    const/16 v9, -0xa97

    aput v9, v2, v8

    const/16 v8, -0x7a

    aput v8, v2, v7

    const/16 v7, -0x29

    aput v7, v2, v6

    const/16 v6, -0x4f

    aput v6, v2, v5

    const/16 v5, -0x79

    aput v5, v2, v3

    const/16 v3, -0x2d

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, -0x7a

    aput v51, v1, v50

    const/16 v50, -0x6ba1

    aput v50, v1, v49

    const/16 v49, -0x6c

    aput v49, v1, v48

    const/16 v48, -0xbaf

    aput v48, v1, v47

    const/16 v47, -0xc

    aput v47, v1, v46

    const/16 v46, -0x7a

    aput v46, v1, v45

    const/16 v45, -0x46

    aput v45, v1, v44

    const/16 v44, 0x3818

    aput v44, v1, v43

    const/16 v43, 0x2238

    aput v43, v1, v42

    const/16 v42, 0x6022

    aput v42, v1, v41

    const/16 v41, 0x7c60

    aput v41, v1, v40

    const/16 v40, 0x667c

    aput v40, v1, v39

    const/16 v39, -0x709a

    aput v39, v1, v38

    const/16 v38, -0x71

    aput v38, v1, v37

    const/16 v37, 0x4a05

    aput v37, v1, v36

    const/16 v36, -0x59b6

    aput v36, v1, v35

    const/16 v35, -0x5a

    aput v35, v1, v34

    const/16 v34, -0x1e

    aput v34, v1, v33

    const/16 v33, -0xb

    aput v33, v1, v32

    const/16 v32, -0x52

    aput v32, v1, v31

    const/16 v31, -0x47

    aput v31, v1, v30

    const/16 v30, -0x55

    aput v30, v1, v29

    const/16 v29, 0x1f3e

    aput v29, v1, v28

    const/16 v28, 0x431f

    aput v28, v1, v27

    const/16 v27, 0x843

    aput v27, v1, v26

    const/16 v26, -0x71f8

    aput v26, v1, v25

    const/16 v25, -0x72

    aput v25, v1, v24

    const/16 v24, -0x3c

    aput v24, v1, v23

    const/16 v23, -0x63

    aput v23, v1, v22

    const/16 v22, -0x20

    aput v22, v1, v21

    const/16 v21, -0x6b90

    aput v21, v1, v20

    const/16 v20, -0x6c

    aput v20, v1, v19

    const/16 v19, -0x75

    aput v19, v1, v18

    const/16 v18, -0x55

    aput v18, v1, v17

    const/16 v17, -0x55

    aput v17, v1, v16

    const/16 v16, -0x25

    aput v16, v1, v15

    const/16 v15, 0x760a

    aput v15, v1, v14

    const/16 v14, -0x1a8a

    aput v14, v1, v13

    const/16 v13, -0x1b

    aput v13, v1, v12

    const/16 v12, -0x73

    aput v12, v1, v11

    const/16 v11, 0xc16

    aput v11, v1, v10

    const/16 v10, -0xaf4

    aput v10, v1, v9

    const/16 v9, -0xb

    aput v9, v1, v8

    const/4 v8, -0x7

    aput v8, v1, v7

    const/16 v7, -0x24

    aput v7, v1, v6

    const/16 v6, -0x18

    aput v6, v1, v5

    const/16 v5, -0x50

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1b
    array-length v5, v1

    if-lt v3, v5, :cond_3c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1c
    array-length v5, v1

    if-lt v3, v5, :cond_3d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x74

    aput v14, v2, v13

    const/16 v13, -0x6e

    aput v13, v2, v12

    const/16 v12, -0x1c

    aput v12, v2, v11

    const/16 v11, -0x68c5

    aput v11, v2, v10

    const/16 v10, -0x1d

    aput v10, v2, v9

    const/16 v9, 0x4b24

    aput v9, v2, v8

    const/16 v8, -0x39d2

    aput v8, v2, v7

    const/16 v7, -0x4d

    aput v7, v2, v6

    const/16 v6, -0x25

    aput v6, v2, v5

    const/16 v5, -0x1b

    aput v5, v2, v3

    const/16 v3, -0x11e6

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x17

    aput v15, v1, v14

    const/4 v14, -0x1

    aput v14, v1, v13

    const/16 v13, -0x73

    aput v13, v1, v12

    const/16 v12, -0x6891

    aput v12, v1, v11

    const/16 v11, -0x69

    aput v11, v1, v10

    const/16 v10, 0x4b57

    aput v10, v1, v9

    const/16 v9, -0x39b5

    aput v9, v1, v8

    const/16 v8, -0x3a

    aput v8, v1, v7

    const/16 v7, -0x56

    aput v7, v1, v6

    const/16 v6, -0x80

    aput v6, v1, v5

    const/16 v5, -0x1198

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1d
    array-length v5, v1

    if-lt v3, v5, :cond_3e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1e
    array-length v5, v1

    if-lt v3, v5, :cond_3f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mRequestTime:J

    invoke-virtual {v4, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0x75b3

    aput v12, v2, v11

    const/16 v11, -0x1c

    aput v11, v2, v10

    const/16 v10, -0x40

    aput v10, v2, v9

    const/16 v9, 0x5d13

    aput v9, v2, v8

    const/16 v8, 0x503c

    aput v8, v2, v7

    const/16 v7, -0x4bcb

    aput v7, v2, v6

    const/16 v6, -0x3a

    aput v6, v2, v5

    const/16 v5, -0x33

    aput v5, v2, v3

    const/16 v3, -0xf99

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0x75d6

    aput v13, v1, v12

    const/16 v12, -0x76

    aput v12, v1, v11

    const/16 v11, -0x57

    aput v11, v1, v10

    const/16 v10, 0x5d7e

    aput v10, v1, v9

    const/16 v9, 0x505d

    aput v9, v1, v8

    const/16 v8, -0x4bb0

    aput v8, v1, v7

    const/16 v7, -0x4c

    aput v7, v1, v6

    const/16 v6, -0x47

    aput v6, v1, v5

    const/16 v5, -0xfec

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1f
    array-length v5, v1

    if-lt v3, v5, :cond_40

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_20
    array-length v5, v1

    if-lt v3, v5, :cond_41

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isStreaming:Z

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x23

    aput v9, v2, v8

    const/16 v8, -0x25b8

    aput v8, v2, v7

    const/16 v7, -0x52

    aput v7, v2, v6

    const/16 v6, 0x7c24

    aput v6, v2, v5

    const/16 v5, 0x7508

    aput v5, v2, v3

    const/16 v3, 0x1806

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x52

    aput v10, v1, v9

    const/16 v9, -0x25c3

    aput v9, v1, v8

    const/16 v8, -0x26

    aput v8, v1, v7

    const/16 v7, 0x7c45

    aput v7, v1, v6

    const/16 v6, 0x757c

    aput v6, v1, v5

    const/16 v5, 0x1875

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_21
    array-length v5, v1

    if-lt v3, v5, :cond_42

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_22
    array-length v5, v1

    if-lt v3, v5, :cond_43

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mAccpet:[Z

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Z)Landroid/content/Intent;

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/16 v7, -0xa93

    aput v7, v2, v6

    const/16 v6, -0x6c

    aput v6, v2, v5

    const/16 v5, -0x12

    aput v5, v2, v3

    const/16 v3, -0x7a

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/16 v8, -0xaff

    aput v8, v1, v7

    const/16 v7, -0xb

    aput v7, v1, v6

    const/16 v6, -0x75

    aput v6, v1, v5

    const/16 v5, -0x15

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_23
    array-length v5, v1

    if-lt v3, v5, :cond_44

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_24
    array-length v5, v1

    if-lt v3, v5, :cond_45

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mMealTime:Z

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0x13

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, -0x5297

    aput v22, v2, v21

    const/16 v21, -0x2b

    aput v21, v2, v20

    const/16 v20, -0x4

    aput v20, v2, v19

    const/16 v19, -0x4fd2

    aput v19, v2, v18

    const/16 v18, -0x2b

    aput v18, v2, v17

    const/16 v17, -0x27c9

    aput v17, v2, v16

    const/16 v16, -0x53

    aput v16, v2, v15

    const/16 v15, -0x21

    aput v15, v2, v14

    const/16 v14, -0x1e

    aput v14, v2, v13

    const/16 v13, -0x59

    aput v13, v2, v12

    const/16 v12, 0x3f3d

    aput v12, v2, v11

    const/16 v11, -0xab1

    aput v11, v2, v10

    const/16 v10, -0x68

    aput v10, v2, v9

    const/4 v9, -0x2

    aput v9, v2, v8

    const/16 v8, -0x54de

    aput v8, v2, v7

    const/16 v7, -0x2e

    aput v7, v2, v6

    const/16 v6, -0x6c84

    aput v6, v2, v5

    const/4 v5, -0x4

    aput v5, v2, v3

    const/16 v3, -0x28be

    aput v3, v2, v1

    const/16 v1, 0x13

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, -0x52e3

    aput v23, v1, v22

    const/16 v22, -0x53

    aput v22, v1, v21

    const/16 v21, -0x67

    aput v21, v1, v20

    const/16 v20, -0x4fa6

    aput v20, v1, v19

    const/16 v19, -0x50

    aput v19, v1, v18

    const/16 v18, -0x27bb

    aput v18, v1, v17

    const/16 v17, -0x28

    aput v17, v1, v16

    const/16 v16, -0x55

    aput v16, v1, v15

    const/16 v15, -0x7d

    aput v15, v1, v14

    const/16 v14, -0x2b

    aput v14, v1, v13

    const/16 v13, 0x3f58

    aput v13, v1, v12

    const/16 v12, -0xac1

    aput v12, v1, v11

    const/16 v11, -0xb

    aput v11, v1, v10

    const/16 v10, -0x65

    aput v10, v1, v9

    const/16 v9, -0x54aa

    aput v9, v1, v8

    const/16 v8, -0x55

    aput v8, v1, v7

    const/16 v7, -0x6ce8

    aput v7, v1, v6

    const/16 v6, -0x6d

    aput v6, v1, v5

    const/16 v5, -0x28e0

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_25
    array-length v5, v1

    if-lt v3, v5, :cond_46

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_26
    array-length v5, v1

    if-lt v3, v5, :cond_47

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mBodyTemperaturText:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/16 v10, -0x26

    aput v10, v2, v9

    const/16 v9, -0x79

    aput v9, v2, v8

    const/16 v8, -0x38a

    aput v8, v2, v7

    const/16 v7, -0x61

    aput v7, v2, v6

    const/16 v6, 0x4113

    aput v6, v2, v5

    const/16 v5, -0x6cd7

    aput v5, v2, v3

    const/16 v3, -0x10

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, -0x42

    aput v11, v1, v10

    const/16 v10, -0x1e

    aput v10, v1, v9

    const/16 v9, -0x3e3

    aput v9, v1, v8

    const/4 v8, -0x4

    aput v8, v1, v7

    const/16 v7, 0x4176

    aput v7, v1, v6

    const/16 v6, -0x6cbf

    aput v6, v1, v5

    const/16 v5, -0x6d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_27
    array-length v5, v1

    if-lt v3, v5, :cond_48

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_28
    array-length v5, v1

    if-lt v3, v5, :cond_49

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mChecked:[Z

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Z)Landroid/content/Intent;

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x77

    aput v17, v2, v16

    const/16 v16, 0x1054

    aput v16, v2, v15

    const/16 v15, 0x417d

    aput v15, v2, v14

    const/16 v14, -0xfcc

    aput v14, v2, v13

    const/16 v13, -0x64

    aput v13, v2, v12

    const/16 v12, -0x26e7

    aput v12, v2, v11

    const/16 v11, -0x46

    aput v11, v2, v10

    const/16 v10, -0x45

    aput v10, v2, v9

    const/16 v9, -0x1c

    aput v9, v2, v8

    const/16 v8, 0x1c2a

    aput v8, v2, v7

    const/16 v7, 0x7273

    aput v7, v2, v6

    const/16 v6, 0x4400

    aput v6, v2, v5

    const/16 v5, -0xacc

    aput v5, v2, v3

    const/16 v3, -0x6c

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x6

    aput v18, v1, v17

    const/16 v17, 0x103a

    aput v17, v1, v16

    const/16 v16, 0x4110

    aput v16, v1, v15

    const/16 v15, -0xfbf

    aput v15, v1, v14

    const/16 v14, -0x10

    aput v14, v1, v13

    const/16 v13, -0x268a

    aput v13, v1, v12

    const/16 v12, -0x27

    aput v12, v1, v11

    const/16 v11, -0x21

    aput v11, v1, v10

    const/16 v10, -0x7f

    aput v10, v1, v9

    const/16 v9, 0x1c5c

    aput v9, v1, v8

    const/16 v8, 0x721c

    aput v8, v1, v7

    const/16 v7, 0x4472

    aput v7, v1, v6

    const/16 v6, -0xabc

    aput v6, v1, v5

    const/16 v5, -0xb

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_29
    array-length v5, v1

    if-lt v3, v5, :cond_4a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2a
    array-length v5, v1

    if-lt v3, v5, :cond_4b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mApprovedColumns:[Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0xc9d

    aput v11, v2, v10

    const/16 v10, -0x46

    aput v10, v2, v9

    const/16 v9, -0x12

    aput v9, v2, v8

    const/16 v8, 0x3330

    aput v8, v2, v7

    const/16 v7, -0x1dbe

    aput v7, v2, v6

    const/16 v6, -0x75

    aput v6, v2, v5

    const/16 v5, 0x3f

    aput v5, v2, v3

    const/16 v3, -0x7d8b

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0xcf9

    aput v12, v1, v11

    const/16 v11, -0xd

    aput v11, v1, v10

    const/16 v10, -0x75

    aput v10, v1, v9

    const/16 v9, 0x3345

    aput v9, v1, v8

    const/16 v8, -0x1dcd

    aput v8, v1, v7

    const/16 v7, -0x1e

    aput v7, v1, v6

    const/16 v6, 0x51

    aput v6, v1, v5

    const/16 v5, -0x7e00

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2b
    array-length v5, v1

    if-lt v3, v5, :cond_4c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2c
    array-length v5, v1

    if-lt v3, v5, :cond_4d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mUniqueKey:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static/range {p0 .. p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    :cond_3b
    :goto_2d
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unbindService()V

    return-void

    :cond_3c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1b

    :cond_3d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1c

    :cond_3e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1d

    :cond_3f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1e

    :cond_40
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1f

    :cond_41
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_20

    :cond_42
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_21

    :cond_43
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_22

    :cond_44
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_23

    :cond_45
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_24

    :cond_46
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_25

    :cond_47
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_26

    :cond_48
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_27

    :cond_49
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_28

    :cond_4a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_29

    :cond_4b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2a

    :cond_4c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2b

    :cond_4d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2c

    :cond_4e
    const/4 v1, 0x1

    new-array v1, v1, [Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-boolean v3, v1, v2

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mAccpet:[Z

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isDirectSensorData:Z

    if-eqz v1, :cond_3b

    new-instance v4, Landroid/content/Intent;

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x28ae

    aput v50, v2, v49

    const/16 v49, -0x5e

    aput v49, v2, v48

    const/16 v48, 0x6042

    aput v48, v2, v47

    const/16 v47, -0x5f2

    aput v47, v2, v46

    const/16 v46, -0x61

    aput v46, v2, v45

    const/16 v45, -0x20

    aput v45, v2, v44

    const/16 v44, -0x55c5

    aput v44, v2, v43

    const/16 v43, -0x3b

    aput v43, v2, v42

    const/16 v42, -0x33

    aput v42, v2, v41

    const/16 v41, -0x35e7

    aput v41, v2, v40

    const/16 v40, -0x48

    aput v40, v2, v39

    const/16 v39, -0x26

    aput v39, v2, v38

    const/16 v38, -0x6299

    aput v38, v2, v37

    const/16 v37, -0xd

    aput v37, v2, v36

    const/16 v36, -0x6d

    aput v36, v2, v35

    const/16 v35, -0x4ce3

    aput v35, v2, v34

    const/16 v34, -0x63

    aput v34, v2, v33

    const/16 v33, -0x58

    aput v33, v2, v32

    const/16 v32, 0x657d

    aput v32, v2, v31

    const/16 v31, 0x4209

    aput v31, v2, v30

    const/16 v30, 0x923

    aput v30, v2, v29

    const/16 v29, 0x2f6c

    aput v29, v2, v28

    const/16 v28, 0x4247

    aput v28, v2, v27

    const/16 v27, 0x166c

    aput v27, v2, v26

    const/16 v26, -0x788d

    aput v26, v2, v25

    const/16 v25, -0x1c

    aput v25, v2, v24

    const/16 v24, -0x518d

    aput v24, v2, v23

    const/16 v23, -0x28

    aput v23, v2, v22

    const/16 v22, -0x78eb

    aput v22, v2, v21

    const/16 v21, -0x1e

    aput v21, v2, v20

    const/16 v20, -0x7dee

    aput v20, v2, v19

    const/16 v19, -0x54

    aput v19, v2, v18

    const/16 v18, -0x59

    aput v18, v2, v17

    const/16 v17, -0xc

    aput v17, v2, v16

    const/16 v16, -0x2

    aput v16, v2, v15

    const/16 v15, -0x2abe

    aput v15, v2, v14

    const/16 v14, -0x4f

    aput v14, v2, v13

    const/16 v13, 0x3954

    aput v13, v2, v12

    const/16 v12, 0x758

    aput v12, v2, v11

    const/16 v11, 0x4629

    aput v11, v2, v10

    const/16 v10, 0x2b25

    aput v10, v2, v9

    const/16 v9, 0x1e4e

    aput v9, v2, v8

    const/16 v8, 0x296d

    aput v8, v2, v7

    const/16 v7, -0x1af9

    aput v7, v2, v6

    const/16 v6, -0x78

    aput v6, v2, v5

    const/16 v5, -0x21af

    aput v5, v2, v3

    const/16 v3, -0x43

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, -0x28c5

    aput v51, v1, v50

    const/16 v50, -0x29

    aput v50, v1, v49

    const/16 v49, 0x6036

    aput v49, v1, v48

    const/16 v48, -0x5a0

    aput v48, v1, v47

    const/16 v47, -0x6

    aput v47, v1, v46

    const/16 v46, -0x6c

    aput v46, v1, v45

    const/16 v45, -0x55ab

    aput v45, v1, v44

    const/16 v44, -0x56

    aput v44, v1, v43

    const/16 v43, -0x52

    aput v43, v1, v42

    const/16 v42, -0x35c9

    aput v42, v1, v41

    const/16 v41, -0x36

    aput v41, v1, v40

    const/16 v40, -0x4b

    aput v40, v1, v39

    const/16 v39, -0x62ec

    aput v39, v1, v38

    const/16 v38, -0x63

    aput v38, v1, v37

    const/16 v37, -0xa

    aput v37, v1, v36

    const/16 v36, -0x4c92

    aput v36, v1, v35

    const/16 v35, -0x4d

    aput v35, v1, v34

    const/16 v34, -0x40

    aput v34, v1, v33

    const/16 v33, 0x6509

    aput v33, v1, v32

    const/16 v32, 0x4265

    aput v32, v1, v31

    const/16 v31, 0x942

    aput v31, v1, v30

    const/16 v30, 0x2f09

    aput v30, v1, v29

    const/16 v29, 0x422f

    aput v29, v1, v28

    const/16 v28, 0x1642

    aput v28, v1, v27

    const/16 v27, -0x78ea

    aput v27, v1, v26

    const/16 v26, -0x79

    aput v26, v1, v25

    const/16 v25, -0x51e6

    aput v25, v1, v24

    const/16 v24, -0x52

    aput v24, v1, v23

    const/16 v23, -0x7899

    aput v23, v1, v22

    const/16 v22, -0x79

    aput v22, v1, v21

    const/16 v21, -0x7d9f

    aput v21, v1, v20

    const/16 v20, -0x7e

    aput v20, v1, v19

    const/16 v19, -0x3d

    aput v19, v1, v18

    const/16 v18, -0x63

    aput v18, v1, v17

    const/16 v17, -0x6f

    aput v17, v1, v16

    const/16 v16, -0x2ad0

    aput v16, v1, v15

    const/16 v15, -0x2b

    aput v15, v1, v14

    const/16 v14, 0x393a

    aput v14, v1, v13

    const/16 v13, 0x739

    aput v13, v1, v12

    const/16 v12, 0x4607

    aput v12, v1, v11

    const/16 v11, 0x2b46

    aput v11, v1, v10

    const/16 v10, 0x1e2b

    aput v10, v1, v9

    const/16 v9, 0x291e

    aput v9, v1, v8

    const/16 v8, -0x1ad7

    aput v8, v1, v7

    const/16 v7, -0x1b

    aput v7, v1, v6

    const/16 v6, -0x21c2

    aput v6, v1, v5

    const/16 v5, -0x22

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2e
    array-length v5, v1

    if-lt v3, v5, :cond_4f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2f
    array-length v5, v1

    if-lt v3, v5, :cond_50

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x44

    aput v9, v2, v8

    const/16 v8, -0x71

    aput v8, v2, v7

    const/16 v7, -0x12

    aput v7, v2, v6

    const/16 v6, -0x24

    aput v6, v2, v5

    const/16 v5, -0x13

    aput v5, v2, v3

    const/16 v3, 0x4e44

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x31

    aput v10, v1, v9

    const/4 v9, -0x6

    aput v9, v1, v8

    const/16 v8, -0x66

    aput v8, v1, v7

    const/16 v7, -0x43

    aput v7, v1, v6

    const/16 v6, -0x67

    aput v6, v1, v5

    const/16 v5, 0x4e37

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_30
    array-length v5, v1

    if-lt v3, v5, :cond_51

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_31
    array-length v5, v1

    if-lt v3, v5, :cond_52

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mAccpet:[Z

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Z)Landroid/content/Intent;

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x79e9

    aput v14, v2, v13

    const/16 v13, -0x15

    aput v13, v2, v12

    const/16 v12, 0x2030

    aput v12, v2, v11

    const/16 v11, 0x5a74

    aput v11, v2, v10

    const/16 v10, -0x7ad2

    aput v10, v2, v9

    const/16 v9, -0xa

    aput v9, v2, v8

    const/16 v8, -0x6abc

    aput v8, v2, v7

    const/16 v7, -0x20

    aput v7, v2, v6

    const/16 v6, -0x7ca4

    aput v6, v2, v5

    const/16 v5, -0x1a

    aput v5, v2, v3

    const/4 v3, -0x6

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x798e

    aput v15, v1, v14

    const/16 v14, -0x7a

    aput v14, v1, v13

    const/16 v13, 0x2059

    aput v13, v1, v12

    const/16 v12, 0x5a20

    aput v12, v1, v11

    const/16 v11, -0x7aa6

    aput v11, v1, v10

    const/16 v10, -0x7b

    aput v10, v1, v9

    const/16 v9, -0x6adf

    aput v9, v1, v8

    const/16 v8, -0x6b

    aput v8, v1, v7

    const/16 v7, -0x7cd3

    aput v7, v1, v6

    const/16 v6, -0x7d

    aput v6, v1, v5

    const/16 v5, -0x78

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_32
    array-length v5, v1

    if-lt v3, v5, :cond_53

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_33
    array-length v5, v1

    if-lt v3, v5, :cond_54

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mRequestTime:J

    invoke-virtual {v4, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0x18

    aput v12, v2, v11

    const/16 v11, -0x2d

    aput v11, v2, v10

    const/16 v10, -0x3c

    aput v10, v2, v9

    const/16 v9, -0x2c

    aput v9, v2, v8

    const/16 v8, 0x311b

    aput v8, v2, v7

    const/16 v7, -0x34ac

    aput v7, v2, v6

    const/16 v6, -0x47

    aput v6, v2, v5

    const/16 v5, 0x135d

    aput v5, v2, v3

    const/16 v3, -0x10a0

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0x71

    aput v13, v1, v12

    const/16 v12, -0x43

    aput v12, v1, v11

    const/16 v11, -0x53

    aput v11, v1, v10

    const/16 v10, -0x47

    aput v10, v1, v9

    const/16 v9, 0x317a

    aput v9, v1, v8

    const/16 v8, -0x34cf

    aput v8, v1, v7

    const/16 v7, -0x35

    aput v7, v1, v6

    const/16 v6, 0x1329

    aput v6, v1, v5

    const/16 v5, -0x10ed

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_34
    array-length v5, v1

    if-lt v3, v5, :cond_55

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_35
    array-length v5, v1

    if-lt v3, v5, :cond_56

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isStreaming:Z

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x78

    aput v11, v2, v10

    const/16 v10, -0x15fc

    aput v10, v2, v9

    const/16 v9, -0x71

    aput v9, v2, v8

    const/16 v8, -0xbb5

    aput v8, v2, v7

    const/16 v7, -0x7b

    aput v7, v2, v6

    const/16 v6, -0x68b1

    aput v6, v2, v5

    const/4 v5, -0x7

    aput v5, v2, v3

    const/16 v3, 0x794b

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x14

    aput v12, v1, v11

    const/16 v11, -0x15b3

    aput v11, v1, v10

    const/16 v10, -0x16

    aput v10, v1, v9

    const/16 v9, -0xbc2

    aput v9, v1, v8

    const/16 v8, -0xc

    aput v8, v1, v7

    const/16 v7, -0x68da

    aput v7, v1, v6

    const/16 v6, -0x69

    aput v6, v1, v5

    const/16 v5, 0x793e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_36
    array-length v5, v1

    if-lt v3, v5, :cond_57

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_37
    array-length v5, v1

    if-lt v3, v5, :cond_58

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mUniqueKey:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static/range {p0 .. p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_2d

    :cond_4f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2e

    :cond_50
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2f

    :cond_51
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_30

    :cond_52
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_31

    :cond_53
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_32

    :cond_54
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_33

    :cond_55
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_34

    :cond_56
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_35

    :cond_57
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_36

    :cond_58
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_37
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 54

    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->requestWindowFeature(I)Z

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    sget v1, Lcom/sec/android/service/health/R$layout;->alert_dialog_hs:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->setContentView(I)V

    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->dateFormet:Ljava/text/DateFormat;

    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->timeFormet:Ljava/text/DateFormat;

    sget v1, Lcom/sec/android/service/health/R$id;->pop_up_textTitle:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/service/health/R$id;->contentParentLayout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentParentLayout:Landroid/widget/LinearLayout;

    sget v1, Lcom/sec/android/service/health/R$id;->contentParentLayout_total:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentParentLayout_value_total:Landroid/widget/LinearLayout;

    sget v1, Lcom/sec/android/service/health/R$id;->contentui_list_layout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentViewLIstViewLayout:Landroid/widget/LinearLayout;

    sget v1, Lcom/sec/android/service/health/R$id;->listview:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mContentValueListView:Landroid/widget/ListView;

    sget v1, Lcom/sec/android/service/health/R$id;->value:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/service/health/R$id;->value_symbol:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textValue_symbol:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/service/health/R$id;->date:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textDate:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/service/health/R$id;->time:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textTime:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/service/health/R$id;->blood_glucose_switch:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentBloodGlucose_layout:Landroid/widget/LinearLayout;

    sget v1, Lcom/sec/android/service/health/R$id;->blood_glucose_radio_group:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->bloodGlucose_switch:Landroid/widget/RadioGroup;

    sget v1, Lcom/sec/android/service/health/R$id;->rb_fasting:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->after_meal:Landroid/widget/RadioButton;

    sget v1, Lcom/sec/android/service/health/R$id;->rb_after_meal:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->rb_fasting:Landroid/widget/RadioButton;

    sget v1, Lcom/sec/android/service/health/R$id;->contentui_dialog_ok_bottom:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentui_dialog_ok_bottom:Landroid/widget/Button;

    sget v1, Lcom/sec/android/service/health/R$id;->contentui_dialog_cancel_bottom:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentui_dialog_cancel_bottom:Landroid/widget/Button;

    sget v1, Lcom/sec/android/service/health/R$id;->textContent:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentui_dialog_ok_bottom:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->contentui_dialog_cancel_bottom:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->unitSettingHelper:Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/16 v6, 0x701c

    aput v6, v2, v5

    const/16 v5, 0x1502

    aput v5, v2, v3

    const/16 v3, 0x3060

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, 0x7075

    aput v7, v1, v6

    const/16 v6, 0x1570

    aput v6, v1, v5

    const/16 v5, 0x3015

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/16 v6, -0x2bf9

    aput v6, v2, v5

    const/16 v5, -0x5a

    aput v5, v2, v3

    const/16 v3, 0x5c6c

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, -0x2b92

    aput v7, v1, v6

    const/16 v6, -0x2c

    aput v6, v1, v5

    const/16 v5, 0x5c19

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->uri:Landroid/net/Uri;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x40c2

    aput v12, v2, v11

    const/16 v11, -0x24

    aput v11, v2, v10

    const/16 v10, -0x18

    aput v10, v2, v9

    const/16 v9, -0x66

    aput v9, v2, v8

    const/16 v8, -0x14eb

    aput v8, v2, v7

    const/16 v7, -0x72

    aput v7, v2, v6

    const/16 v6, 0x3455

    aput v6, v2, v5

    const/16 v5, 0x6156

    aput v5, v2, v4

    const/16 v4, 0x5608

    aput v4, v2, v3

    const/16 v3, -0x25c6

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x40a5

    aput v13, v1, v12

    const/16 v12, -0x41

    aput v12, v1, v11

    const/16 v11, -0x7f

    aput v11, v1, v10

    const/16 v10, -0x14

    aput v10, v1, v9

    const/16 v9, -0x1499

    aput v9, v1, v8

    const/16 v8, -0x15

    aput v8, v1, v7

    const/16 v7, 0x3406

    aput v7, v1, v6

    const/16 v6, 0x6134

    aput v6, v1, v5

    const/16 v5, 0x5661

    aput v5, v1, v4

    const/16 v4, -0x25aa

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->dLPrmaYUQG()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/16 v8, -0x6e

    aput v8, v2, v7

    const/16 v7, -0xaec

    aput v7, v2, v3

    const/16 v3, -0x80

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, -0x5

    aput v9, v1, v8

    const/16 v8, -0xa9a

    aput v8, v1, v7

    const/16 v7, -0xb

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v7, v1

    if-lt v3, v7, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v7, v1

    if-lt v3, v7, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0x32

    aput v12, v2, v11

    const/16 v11, 0x463f

    aput v11, v2, v10

    const/16 v10, -0x4ad1

    aput v10, v2, v9

    const/16 v9, -0x28

    aput v9, v2, v8

    const/16 v8, -0x16

    aput v8, v2, v7

    const/16 v7, -0x17

    aput v7, v2, v6

    const/16 v6, -0x14

    aput v6, v2, v5

    const/16 v5, -0xdde

    aput v5, v2, v3

    const/16 v3, -0x7f

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0x57

    aput v13, v1, v12

    const/16 v12, 0x4651

    aput v12, v1, v11

    const/16 v11, -0x4aba

    aput v11, v1, v10

    const/16 v10, -0x4b

    aput v10, v1, v9

    const/16 v9, -0x75

    aput v9, v1, v8

    const/16 v8, -0x74

    aput v8, v1, v7

    const/16 v7, -0x62

    aput v7, v1, v6

    const/16 v6, -0xdaa

    aput v6, v1, v5

    const/16 v5, -0xe

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0x66

    aput v12, v2, v11

    const/16 v11, -0x21

    aput v11, v2, v10

    const/16 v10, -0x14

    aput v10, v2, v9

    const/16 v9, -0xe

    aput v9, v2, v8

    const/16 v8, -0x4c

    aput v8, v2, v7

    const/16 v7, 0x7679

    aput v7, v2, v6

    const/16 v6, -0x1bfc

    aput v6, v2, v5

    const/16 v5, -0x70

    aput v5, v2, v3

    const/16 v3, -0x43

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/4 v13, -0x3

    aput v13, v1, v12

    const/16 v12, -0x4f

    aput v12, v1, v11

    const/16 v11, -0x7b

    aput v11, v1, v10

    const/16 v10, -0x61

    aput v10, v1, v9

    const/16 v9, -0x2b

    aput v9, v1, v8

    const/16 v8, 0x761c

    aput v8, v1, v7

    const/16 v7, -0x1b8a

    aput v7, v1, v6

    const/16 v6, -0x1c

    aput v6, v1, v5

    const/16 v5, -0x32

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_13

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isStreaming:Z

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isDirectSensorData:Z

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x12

    aput v14, v2, v13

    const/16 v13, -0x4c

    aput v13, v2, v12

    const/16 v12, -0x2fd2

    aput v12, v2, v11

    const/16 v11, -0x4d

    aput v11, v2, v10

    const/16 v10, 0x3c3d

    aput v10, v2, v9

    const/16 v9, -0x7eac

    aput v9, v2, v8

    const/16 v8, -0x3e

    aput v8, v2, v7

    const/16 v7, -0x6e97

    aput v7, v2, v6

    const/16 v6, -0x10

    aput v6, v2, v5

    const/16 v5, 0x3070

    aput v5, v2, v3

    const/16 v3, -0x2ba3

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x76

    aput v15, v1, v14

    const/16 v14, -0x2f

    aput v14, v1, v13

    const/16 v13, -0x2fbb

    aput v13, v1, v12

    const/16 v12, -0x30

    aput v12, v1, v11

    const/16 v11, 0x3c58

    aput v11, v1, v10

    const/16 v10, -0x7ec4

    aput v10, v1, v9

    const/16 v9, -0x7f

    aput v9, v1, v8

    const/16 v8, -0x6efb

    aput v8, v1, v7

    const/16 v7, -0x6f

    aput v7, v1, v6

    const/16 v6, 0x3015

    aput v6, v1, v5

    const/16 v5, -0x2bd0

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_15

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x56

    aput v14, v2, v13

    const/16 v13, -0x4387

    aput v13, v2, v12

    const/16 v12, -0x29

    aput v12, v2, v11

    const/16 v11, -0x6eb8

    aput v11, v2, v10

    const/16 v10, -0xc

    aput v10, v2, v9

    const/16 v9, -0x1f

    aput v9, v2, v8

    const/16 v8, 0x7262

    aput v8, v2, v7

    const/16 v7, 0x451e

    aput v7, v2, v6

    const/16 v6, -0x36dc

    aput v6, v2, v5

    const/16 v5, -0x54

    aput v5, v2, v3

    const/16 v3, -0x69

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x32

    aput v15, v1, v14

    const/16 v14, -0x43e4

    aput v14, v1, v13

    const/16 v13, -0x44

    aput v13, v1, v12

    const/16 v12, -0x6ed5

    aput v12, v1, v11

    const/16 v11, -0x6f

    aput v11, v1, v10

    const/16 v10, -0x77

    aput v10, v1, v9

    const/16 v9, 0x7221

    aput v9, v1, v8

    const/16 v8, 0x4572

    aput v8, v1, v7

    const/16 v7, -0x36bb

    aput v7, v1, v6

    const/16 v6, -0x37

    aput v6, v1, v5

    const/4 v5, -0x6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v5, v1

    if-lt v3, v5, :cond_17

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->after_meal:Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->rb_fasting:Landroid/widget/RadioButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0x5422

    aput v15, v2, v14

    const/16 v14, 0x5020

    aput v14, v2, v13

    const/16 v13, -0x4fcf

    aput v13, v2, v12

    const/16 v12, -0x2c

    aput v12, v2, v11

    const/16 v11, 0x2b52

    aput v11, v2, v10

    const/16 v10, -0x5fa1

    aput v10, v2, v9

    const/16 v9, -0x32

    aput v9, v2, v8

    const/16 v8, -0x47

    aput v8, v2, v7

    const/16 v7, -0x6a

    aput v7, v2, v6

    const/16 v6, -0x5b

    aput v6, v2, v5

    const/16 v5, -0xa

    aput v5, v2, v3

    const/16 v3, -0x3a

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0x5443

    aput v16, v1, v15

    const/16 v15, 0x5054

    aput v15, v1, v14

    const/16 v14, -0x4fb0

    aput v14, v1, v13

    const/16 v13, -0x50

    aput v13, v1, v12

    const/16 v12, 0x2b0d

    aput v12, v1, v11

    const/16 v11, -0x5fd5

    aput v11, v1, v10

    const/16 v10, -0x60

    aput v10, v1, v9

    const/16 v9, -0x24

    aput v9, v1, v8

    const/16 v8, -0x1e

    aput v8, v1, v7

    const/16 v7, -0x35

    aput v7, v1, v6

    const/16 v6, -0x67

    aput v6, v1, v5

    const/16 v5, -0x5b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_19

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, -0x14

    aput v15, v2, v14

    const/16 v14, -0xfda

    aput v14, v2, v13

    const/16 v13, -0x6f

    aput v13, v2, v12

    const/16 v12, -0x51

    aput v12, v2, v11

    const/16 v11, -0x2a

    aput v11, v2, v10

    const/16 v10, -0x5ab0

    aput v10, v2, v9

    const/16 v9, -0x35

    aput v9, v2, v8

    const/16 v8, -0x46eb

    aput v8, v2, v7

    const/16 v7, -0x33

    aput v7, v2, v6

    const/16 v6, -0x12ef

    aput v6, v2, v5

    const/16 v5, -0x7e

    aput v5, v2, v3

    const/16 v3, 0x5f29

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x73

    aput v16, v1, v15

    const/16 v15, -0xfae

    aput v15, v1, v14

    const/16 v14, -0x10

    aput v14, v1, v13

    const/16 v13, -0x35

    aput v13, v1, v12

    const/16 v12, -0x77

    aput v12, v1, v11

    const/16 v11, -0x5adc

    aput v11, v1, v10

    const/16 v10, -0x5b

    aput v10, v1, v9

    const/16 v9, -0x4690

    aput v9, v1, v8

    const/16 v8, -0x47

    aput v8, v1, v7

    const/16 v7, -0x1281

    aput v7, v1, v6

    const/16 v6, -0x13

    aput v6, v1, v5

    const/16 v5, 0x5f4a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_1b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/4 v14, -0x3

    aput v14, v2, v13

    const/16 v13, -0x56

    aput v13, v2, v12

    const/16 v12, -0x24

    aput v12, v2, v11

    const/16 v11, -0x75

    aput v11, v2, v10

    const/16 v10, -0x46

    aput v10, v2, v9

    const/16 v9, -0xc

    aput v9, v2, v8

    const/16 v8, 0x786e

    aput v8, v2, v7

    const/16 v7, -0x40f3

    aput v7, v2, v6

    const/16 v6, -0x32

    aput v6, v2, v5

    const/16 v5, 0x2f2e

    aput v5, v2, v3

    const/16 v3, 0x215d

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x68

    aput v15, v1, v14

    const/16 v14, -0x39

    aput v14, v1, v13

    const/16 v13, -0x4b

    aput v13, v1, v12

    const/16 v12, -0x21

    aput v12, v1, v11

    const/16 v11, -0x32

    aput v11, v1, v10

    const/16 v10, -0x79

    aput v10, v1, v9

    const/16 v9, 0x780b

    aput v9, v1, v8

    const/16 v8, -0x4088

    aput v8, v1, v7

    const/16 v7, -0x41

    aput v7, v1, v6

    const/16 v6, 0x2f4b

    aput v6, v1, v5

    const/16 v5, 0x212f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_1d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_15
    array-length v5, v1

    if-lt v3, v5, :cond_1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x57

    aput v14, v2, v13

    const/16 v13, -0x38

    aput v13, v2, v12

    const/16 v12, -0x50

    aput v12, v2, v11

    const/16 v11, -0x2d

    aput v11, v2, v10

    const/16 v10, 0x5e73

    aput v10, v2, v9

    const/16 v9, 0x2d

    aput v9, v2, v8

    const/16 v8, -0x579b

    aput v8, v2, v7

    const/16 v7, -0x23

    aput v7, v2, v6

    const/16 v6, 0x77a

    aput v6, v2, v5

    const/16 v5, 0x6462

    aput v5, v2, v3

    const/16 v3, -0x8ea

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x34

    aput v15, v1, v14

    const/16 v14, -0x5b

    aput v14, v1, v13

    const/16 v13, -0x27

    aput v13, v1, v12

    const/16 v12, -0x79

    aput v12, v1, v11

    const/16 v11, 0x5e07

    aput v11, v1, v10

    const/16 v10, 0x5e

    aput v10, v1, v9

    const/16 v9, -0x5800

    aput v9, v1, v8

    const/16 v8, -0x58

    aput v8, v1, v7

    const/16 v7, 0x70b

    aput v7, v1, v6

    const/16 v6, 0x6407

    aput v6, v1, v5

    const/16 v5, -0x89c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_16
    array-length v5, v1

    if-lt v3, v5, :cond_1f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_17
    array-length v5, v1

    if-lt v3, v5, :cond_20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mRequestTime:J

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x322f

    aput v12, v2, v11

    const/16 v11, 0x1b57

    aput v11, v2, v10

    const/16 v10, -0x16b0

    aput v10, v2, v9

    const/16 v9, -0x74

    aput v9, v2, v8

    const/16 v8, -0x2a

    aput v8, v2, v7

    const/16 v7, 0x13d

    aput v7, v2, v6

    const/16 v6, -0x7a98

    aput v6, v2, v5

    const/16 v5, -0x15

    aput v5, v2, v3

    const/16 v3, -0x6f

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x3256

    aput v13, v1, v12

    const/16 v12, 0x1b32

    aput v12, v1, v11

    const/16 v11, -0x16e5

    aput v11, v1, v10

    const/16 v10, -0x17

    aput v10, v1, v9

    const/16 v9, -0x5d

    aput v9, v1, v8

    const/16 v8, 0x14c

    aput v8, v1, v7

    const/16 v7, -0x7aff

    aput v7, v1, v6

    const/16 v6, -0x7b

    aput v6, v1, v5

    const/16 v5, -0x1c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_18
    array-length v5, v1

    if-lt v3, v5, :cond_21

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_19
    array-length v5, v1

    if-lt v3, v5, :cond_22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0x5de8

    aput v12, v2, v11

    const/16 v11, -0x39

    aput v11, v2, v10

    const/16 v10, -0x6796

    aput v10, v2, v9

    const/4 v9, -0x3

    aput v9, v2, v8

    const/16 v8, -0x7d

    aput v8, v2, v7

    const/4 v7, -0x4

    aput v7, v2, v6

    const/16 v6, -0x2b

    aput v6, v2, v5

    const/16 v5, -0x6e

    aput v5, v2, v3

    const/16 v3, -0x6b

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0x5d9f

    aput v13, v1, v12

    const/16 v12, -0x5e

    aput v12, v1, v11

    const/16 v11, -0x67df

    aput v11, v1, v10

    const/16 v10, -0x68

    aput v10, v1, v9

    const/16 v9, -0xa

    aput v9, v1, v8

    const/16 v8, -0x73

    aput v8, v1, v7

    const/16 v7, -0x44

    aput v7, v1, v6

    const/4 v6, -0x4

    aput v6, v1, v5

    const/16 v5, -0x20

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1a
    array-length v5, v1

    if-lt v3, v5, :cond_23

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1b
    array-length v5, v1

    if-lt v3, v5, :cond_24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mUniqueKey:Ljava/lang/String;

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x40

    aput v9, v2, v8

    const/16 v8, 0x5145

    aput v8, v2, v7

    const/16 v7, -0x73c8

    aput v7, v2, v6

    const/4 v6, -0x8

    aput v6, v2, v5

    const/16 v5, -0x7a

    aput v5, v2, v3

    const/16 v3, 0x241c

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x52

    aput v10, v1, v9

    const/16 v9, 0x512a

    aput v9, v1, v8

    const/16 v8, -0x73af

    aput v8, v1, v7

    const/16 v7, -0x74

    aput v7, v1, v6

    const/16 v6, -0x1b

    aput v6, v1, v5

    const/16 v5, 0x247d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1c
    array-length v5, v1

    if-lt v3, v5, :cond_25

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1d
    array-length v5, v1

    if-lt v3, v5, :cond_26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x1f0

    aput v9, v2, v8

    const/16 v8, -0x6f

    aput v8, v2, v7

    const/16 v7, -0x5ab5

    aput v7, v2, v6

    const/16 v6, -0x2f

    aput v6, v2, v5

    const/16 v5, -0x6e9f

    aput v5, v2, v3

    const/16 v3, -0x10

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x182

    aput v10, v1, v9

    const/4 v9, -0x2

    aput v9, v1, v8

    const/16 v8, -0x5ade

    aput v8, v1, v7

    const/16 v7, -0x5b

    aput v7, v1, v6

    const/16 v6, -0x6efe

    aput v6, v1, v5

    const/16 v5, -0x6f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1e
    array-length v5, v1

    if-lt v3, v5, :cond_27

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1f
    array-length v5, v1

    if-lt v3, v5, :cond_28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mAction:I

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x15

    aput v11, v2, v10

    const/16 v10, -0x1ec1

    aput v10, v2, v9

    const/16 v9, -0x7c

    aput v9, v2, v8

    const/16 v8, 0x3908

    aput v8, v2, v7

    const/16 v7, 0x7e50

    aput v7, v2, v6

    const/16 v6, 0x3b08

    aput v6, v2, v5

    const/16 v5, -0x7fa2

    aput v5, v2, v3

    const/16 v3, -0x1c

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x71

    aput v12, v1, v11

    const/16 v11, -0x1eaa

    aput v11, v1, v10

    const/16 v10, -0x1f

    aput v10, v1, v9

    const/16 v9, 0x396b

    aput v9, v1, v8

    const/16 v8, 0x7e39

    aput v8, v1, v7

    const/16 v7, 0x3b7e

    aput v7, v1, v6

    const/16 v6, -0x7fc5

    aput v6, v1, v5

    const/16 v5, -0x80

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_20
    array-length v5, v1

    if-lt v3, v5, :cond_29

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_21
    array-length v5, v1

    if-lt v3, v5, :cond_2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x32

    aput v11, v2, v10

    const/16 v10, -0x63

    aput v10, v2, v9

    const/16 v9, -0x6693

    aput v9, v2, v8

    const/4 v8, -0x6

    aput v8, v2, v7

    const/16 v7, -0x3d

    aput v7, v2, v6

    const/16 v6, 0x381a

    aput v6, v2, v5

    const/16 v5, -0x61a3

    aput v5, v2, v3

    const/4 v3, -0x6

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x56

    aput v12, v1, v11

    const/16 v11, -0xc

    aput v11, v1, v10

    const/16 v10, -0x66f8

    aput v10, v1, v9

    const/16 v9, -0x67

    aput v9, v1, v8

    const/16 v8, -0x56

    aput v8, v1, v7

    const/16 v7, 0x386c

    aput v7, v1, v6

    const/16 v6, -0x61c8

    aput v6, v1, v5

    const/16 v5, -0x62

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_22
    array-length v5, v1

    if-lt v3, v5, :cond_2b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_23
    array-length v5, v1

    if-lt v3, v5, :cond_2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceId:Ljava/lang/String;

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, -0x5e

    aput v16, v2, v15

    const/16 v15, -0x60d4

    aput v15, v2, v14

    const/4 v14, -0x6

    aput v14, v2, v13

    const/16 v13, -0x36

    aput v13, v2, v12

    const/16 v12, -0x9

    aput v12, v2, v11

    const/16 v11, -0x5eb

    aput v11, v2, v10

    const/16 v10, -0x61

    aput v10, v2, v9

    const/16 v9, -0x148b

    aput v9, v2, v8

    const/16 v8, -0x61

    aput v8, v2, v7

    const/16 v7, -0x30

    aput v7, v2, v6

    const/16 v6, -0x8b7

    aput v6, v2, v5

    const/16 v5, -0x6d

    aput v5, v2, v4

    const/16 v4, -0x78a7

    aput v4, v2, v3

    const/16 v3, -0x12

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x3a

    aput v17, v1, v16

    const/16 v16, -0x60bb

    aput v16, v1, v15

    const/16 v15, -0x61

    aput v15, v1, v14

    const/16 v14, -0x57

    aput v14, v1, v13

    const/16 v13, -0x62

    aput v13, v1, v12

    const/16 v12, -0x59d

    aput v12, v1, v11

    const/4 v11, -0x6

    aput v11, v1, v10

    const/16 v10, -0x14ef

    aput v10, v1, v9

    const/16 v9, -0x15

    aput v9, v1, v8

    const/16 v8, -0x42

    aput v8, v1, v7

    const/16 v7, -0x8d4

    aput v7, v1, v6

    const/16 v6, -0x9

    aput v6, v1, v5

    const/16 v5, -0x78c9

    aput v5, v1, v4

    const/16 v4, -0x79

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_24
    array-length v4, v1

    if-lt v3, v4, :cond_2d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_25
    array-length v4, v1

    if-lt v3, v4, :cond_2e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0x1b15

    aput v18, v2, v17

    const/16 v17, -0x18e

    aput v17, v2, v16

    const/16 v16, -0x65

    aput v16, v2, v15

    const/16 v15, -0x69

    aput v15, v2, v14

    const/16 v14, -0x6d

    aput v14, v2, v13

    const/16 v13, -0x49

    aput v13, v2, v12

    const/16 v12, -0x5faa

    aput v12, v2, v11

    const/16 v11, -0x3c

    aput v11, v2, v10

    const/16 v10, -0xe

    aput v10, v2, v9

    const/16 v9, -0x41

    aput v9, v2, v8

    const/16 v8, 0x2f5b

    aput v8, v2, v7

    const/16 v7, 0x214b

    aput v7, v2, v6

    const/16 v6, 0x5a4f

    aput v6, v2, v3

    const/16 v3, 0x1133

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0x1b71

    aput v19, v1, v18

    const/16 v18, -0x1e5

    aput v18, v1, v17

    const/16 v17, -0x2

    aput v17, v1, v16

    const/16 v16, -0xc

    aput v16, v1, v15

    const/4 v15, -0x6

    aput v15, v1, v14

    const/16 v14, -0x3f

    aput v14, v1, v13

    const/16 v13, -0x5fcd

    aput v13, v1, v12

    const/16 v12, -0x60

    aput v12, v1, v11

    const/16 v11, -0x7a

    aput v11, v1, v10

    const/16 v10, -0x2f

    aput v10, v1, v9

    const/16 v9, 0x2f3e

    aput v9, v1, v8

    const/16 v8, 0x212f

    aput v8, v1, v7

    const/16 v7, 0x5a21

    aput v7, v1, v6

    const/16 v6, 0x115a

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_26
    array-length v6, v1

    if-lt v3, v6, :cond_2f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_27
    array-length v6, v1

    if-lt v3, v6, :cond_30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x79

    aput v17, v2, v16

    const/16 v16, -0x37b6

    aput v16, v2, v15

    const/16 v15, -0x44

    aput v15, v2, v14

    const/16 v14, -0x30ce

    aput v14, v2, v13

    const/16 v13, -0x56

    aput v13, v2, v12

    const/16 v12, 0x1103

    aput v12, v2, v11

    const/16 v11, 0x3773

    aput v11, v2, v10

    const/16 v10, 0x258

    aput v10, v2, v9

    const/16 v9, -0x1d99

    aput v9, v2, v8

    const/16 v8, -0x7f

    aput v8, v2, v7

    const/16 v7, -0xc

    aput v7, v2, v6

    const/16 v6, 0x5a15

    aput v6, v2, v5

    const/16 v5, -0x75c1

    aput v5, v2, v3

    const/16 v3, -0x12

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x1d

    aput v18, v1, v17

    const/16 v17, -0x37dd

    aput v17, v1, v16

    const/16 v16, -0x38

    aput v16, v1, v15

    const/16 v15, -0x30af

    aput v15, v1, v14

    const/16 v14, -0x31

    aput v14, v1, v13

    const/16 v13, 0x1169

    aput v13, v1, v12

    const/16 v12, 0x3711

    aput v12, v1, v11

    const/16 v11, 0x237

    aput v11, v1, v10

    const/16 v10, -0x1dfe

    aput v10, v1, v9

    const/16 v9, -0x1e

    aput v9, v1, v8

    const/16 v8, -0x63

    aput v8, v1, v7

    const/16 v7, 0x5a63

    aput v7, v1, v6

    const/16 v6, -0x75a6

    aput v6, v1, v5

    const/16 v5, -0x76

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_28
    array-length v5, v1

    if-lt v3, v5, :cond_31

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_29
    array-length v5, v1

    if-lt v3, v5, :cond_32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x47ed

    aput v17, v2, v16

    const/16 v16, -0x2f

    aput v16, v2, v15

    const/16 v15, -0x21

    aput v15, v2, v14

    const/16 v14, -0xfa8

    aput v14, v2, v13

    const/16 v13, -0x6b

    aput v13, v2, v12

    const/16 v12, -0x45fa

    aput v12, v2, v11

    const/16 v11, -0x28

    aput v11, v2, v10

    const/16 v10, -0x17

    aput v10, v2, v9

    const/16 v9, -0x65

    aput v9, v2, v8

    const/16 v8, -0x1ae4

    aput v8, v2, v7

    const/16 v7, -0x74

    aput v7, v2, v6

    const/4 v6, -0x8

    aput v6, v2, v5

    const/16 v5, -0x31

    aput v5, v2, v3

    const/16 v3, -0x78

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x4789

    aput v18, v1, v17

    const/16 v17, -0x48

    aput v17, v1, v16

    const/16 v16, -0x55

    aput v16, v1, v15

    const/16 v15, -0xfc5

    aput v15, v1, v14

    const/16 v14, -0x10

    aput v14, v1, v13

    const/16 v13, -0x4594

    aput v13, v1, v12

    const/16 v12, -0x46

    aput v12, v1, v11

    const/16 v11, -0x7a

    aput v11, v1, v10

    const/4 v10, -0x2

    aput v10, v1, v9

    const/16 v9, -0x1a81

    aput v9, v1, v8

    const/16 v8, -0x1b

    aput v8, v1, v7

    const/16 v7, -0x72

    aput v7, v1, v6

    const/16 v6, -0x56

    aput v6, v1, v5

    const/16 v5, -0x14

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2a
    array-length v5, v1

    if-lt v3, v5, :cond_33

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2b
    array-length v5, v1

    if-lt v3, v5, :cond_34

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceObjectId:I

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, -0x5df9

    aput v16, v2, v15

    const/16 v15, -0x35

    aput v15, v2, v14

    const/16 v14, -0x20

    aput v14, v2, v13

    const/16 v13, 0x5f63

    aput v13, v2, v12

    const/16 v12, -0x44ca

    aput v12, v2, v11

    const/16 v11, -0x33

    aput v11, v2, v10

    const/16 v10, 0x205e

    aput v10, v2, v9

    const/16 v9, 0x1144

    aput v9, v2, v8

    const/16 v8, -0x7e9b

    aput v8, v2, v7

    const/16 v7, -0x11

    aput v7, v2, v6

    const/16 v6, -0x28

    aput v6, v2, v5

    const/16 v5, -0x17d1

    aput v5, v2, v4

    const/16 v4, -0x7a

    aput v4, v2, v3

    const/4 v3, -0x1

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x5d9d

    aput v17, v1, v16

    const/16 v16, -0x5e

    aput v16, v1, v15

    const/16 v15, -0x7b

    aput v15, v1, v14

    const/16 v14, 0x5f00

    aput v14, v1, v13

    const/16 v13, -0x44a1

    aput v13, v1, v12

    const/16 v12, -0x45

    aput v12, v1, v11

    const/16 v11, 0x203b

    aput v11, v1, v10

    const/16 v10, 0x1120

    aput v10, v1, v9

    const/16 v9, -0x7eef

    aput v9, v1, v8

    const/16 v8, -0x7f

    aput v8, v1, v7

    const/16 v7, -0x43

    aput v7, v1, v6

    const/16 v6, -0x17b5

    aput v6, v1, v5

    const/16 v5, -0x18

    aput v5, v1, v4

    const/16 v4, -0x6a

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2c
    array-length v4, v1

    if-lt v3, v4, :cond_35

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2d
    array-length v4, v1

    if-lt v3, v4, :cond_36

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x2ae4

    aput v18, v2, v17

    const/16 v17, -0x44

    aput v17, v2, v16

    const/16 v16, -0x34

    aput v16, v2, v15

    const/16 v15, -0x64

    aput v15, v2, v14

    const/16 v14, -0x1a7

    aput v14, v2, v13

    const/16 v13, -0x78

    aput v13, v2, v12

    const/16 v12, -0x18

    aput v12, v2, v11

    const/16 v11, 0x1f2b

    aput v11, v2, v10

    const/16 v10, -0x795

    aput v10, v2, v9

    const/16 v9, -0x6a

    aput v9, v2, v8

    const/16 v8, -0x1e

    aput v8, v2, v7

    const/16 v7, -0x5389

    aput v7, v2, v6

    const/16 v6, -0x3e

    aput v6, v2, v3

    const/16 v3, -0x3c88

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, -0x2a88

    aput v19, v1, v18

    const/16 v18, -0x2b

    aput v18, v1, v17

    const/16 v17, -0x57

    aput v17, v1, v16

    const/16 v16, -0x1

    aput v16, v1, v15

    const/16 v15, -0x1d0

    aput v15, v1, v14

    const/4 v14, -0x2

    aput v14, v1, v13

    const/16 v13, -0x73

    aput v13, v1, v12

    const/16 v12, 0x1f4f

    aput v12, v1, v11

    const/16 v11, -0x7e1

    aput v11, v1, v10

    const/4 v10, -0x8

    aput v10, v1, v9

    const/16 v9, -0x79

    aput v9, v1, v8

    const/16 v8, -0x53ed

    aput v8, v1, v7

    const/16 v7, -0x54

    aput v7, v1, v6

    const/16 v6, -0x3cef

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2e
    array-length v6, v1

    if-lt v3, v6, :cond_37

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2f
    array-length v6, v1

    if-lt v3, v6, :cond_38

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceObjectId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x5642

    aput v12, v2, v11

    const/16 v11, 0x2035

    aput v11, v2, v10

    const/16 v10, 0x6c49

    aput v10, v2, v9

    const/16 v9, -0x60e6

    aput v9, v2, v8

    const/16 v8, -0x13

    aput v8, v2, v7

    const/16 v7, 0x6012

    aput v7, v2, v6

    const/16 v6, 0x3633

    aput v6, v2, v5

    const/16 v5, 0x3054

    aput v5, v2, v4

    const/16 v4, -0x17a7

    aput v4, v2, v3

    const/16 v3, -0x7c

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x5627

    aput v13, v1, v12

    const/16 v12, 0x2056

    aput v12, v1, v11

    const/16 v11, 0x6c20

    aput v11, v1, v10

    const/16 v10, -0x6094

    aput v10, v1, v9

    const/16 v9, -0x61

    aput v9, v1, v8

    const/16 v8, 0x6077

    aput v8, v1, v7

    const/16 v7, 0x3660

    aput v7, v1, v6

    const/16 v6, 0x3036

    aput v6, v1, v5

    const/16 v5, -0x17d0

    aput v5, v1, v4

    const/16 v4, -0x18

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_30
    array-length v4, v1

    if-lt v3, v4, :cond_39

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_31
    array-length v4, v1

    if-lt v3, v4, :cond_3a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x580a

    aput v33, v2, v32

    const/16 v32, -0x49b

    aput v32, v2, v31

    const/16 v31, -0x3c

    aput v31, v2, v30

    const/16 v30, 0x2633

    aput v30, v2, v29

    const/16 v29, -0x29ae

    aput v29, v2, v28

    const/16 v28, -0x49

    aput v28, v2, v27

    const/16 v27, -0x4cdf

    aput v27, v2, v26

    const/16 v26, -0x14

    aput v26, v2, v25

    const/16 v25, 0xb5b

    aput v25, v2, v24

    const/16 v24, -0x439b

    aput v24, v2, v23

    const/16 v23, -0x27

    aput v23, v2, v22

    const/16 v22, -0x1781

    aput v22, v2, v21

    const/16 v21, -0x79

    aput v21, v2, v20

    const/16 v20, -0x6181

    aput v20, v2, v19

    const/16 v19, -0x42

    aput v19, v2, v18

    const/16 v18, -0x689a

    aput v18, v2, v17

    const/16 v17, -0xa

    aput v17, v2, v16

    const/16 v16, -0x1a

    aput v16, v2, v15

    const/16 v15, -0x58

    aput v15, v2, v14

    const/16 v14, 0x111c

    aput v14, v2, v13

    const/16 v13, 0x2d65

    aput v13, v2, v12

    const/16 v12, 0x848

    aput v12, v2, v11

    const/16 v11, 0x1764

    aput v11, v2, v10

    const/16 v10, -0x4a8e

    aput v10, v2, v9

    const/16 v9, -0x2f

    aput v9, v2, v8

    const/16 v8, -0x20ec

    aput v8, v2, v7

    const/16 v7, -0x53

    aput v7, v2, v6

    const/16 v6, 0x1f3c

    aput v6, v2, v3

    const/16 v3, 0x3c79

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x582a

    aput v34, v1, v33

    const/16 v33, -0x4a8

    aput v33, v1, v32

    const/16 v32, -0x5

    aput v32, v1, v31

    const/16 v31, 0x2652

    aput v31, v1, v30

    const/16 v30, -0x29da

    aput v30, v1, v29

    const/16 v29, -0x2a

    aput v29, v1, v28

    const/16 v28, -0x4cbb

    aput v28, v1, v27

    const/16 v27, -0x4d

    aput v27, v1, v26

    const/16 v26, 0xb2f

    aput v26, v1, v25

    const/16 v25, -0x43f5

    aput v25, v1, v24

    const/16 v24, -0x44

    aput v24, v1, v23

    const/16 v23, -0x17ef

    aput v23, v1, v22

    const/16 v22, -0x18

    aput v22, v1, v21

    const/16 v21, -0x61e4

    aput v21, v1, v20

    const/16 v20, -0x62

    aput v20, v1, v19

    const/16 v19, -0x68eb

    aput v19, v1, v18

    const/16 v18, -0x69

    aput v18, v1, v17

    const/16 v17, -0x72

    aput v17, v1, v16

    const/16 v16, -0x78

    aput v16, v1, v15

    const/16 v15, 0x1179

    aput v15, v1, v14

    const/16 v14, 0x2d11

    aput v14, v1, v13

    const/16 v13, 0x82d

    aput v13, v1, v12

    const/16 v12, 0x1708

    aput v12, v1, v11

    const/16 v11, -0x4ae9

    aput v11, v1, v10

    const/16 v10, -0x4b

    aput v10, v1, v9

    const/16 v9, -0x20cc

    aput v9, v1, v8

    const/16 v8, -0x21

    aput v8, v1, v7

    const/16 v7, 0x1f53

    aput v7, v1, v6

    const/16 v6, 0x3c1f

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_32
    array-length v6, v1

    if-lt v3, v6, :cond_3b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_33
    array-length v6, v1

    if-lt v3, v6, :cond_3c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x5f

    aput v17, v2, v16

    const/16 v16, -0x31

    aput v16, v2, v15

    const/16 v15, -0x36f0

    aput v15, v2, v14

    const/16 v14, -0x53

    aput v14, v2, v13

    const/16 v13, -0x67

    aput v13, v2, v12

    const/16 v12, -0x37

    aput v12, v2, v11

    const/16 v11, -0x69

    aput v11, v2, v10

    const/16 v10, -0x41b3

    aput v10, v2, v9

    const/16 v9, -0x36

    aput v9, v2, v8

    const/16 v8, 0x704a

    aput v8, v2, v7

    const/16 v7, -0x6ae1

    aput v7, v2, v3

    const/16 v3, -0xa

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, -0x40

    aput v18, v1, v17

    const/16 v17, -0x45

    aput v17, v1, v16

    const/16 v16, -0x368f

    aput v16, v1, v15

    const/16 v15, -0x37

    aput v15, v1, v14

    const/16 v14, -0x3a

    aput v14, v1, v13

    const/16 v13, -0x43

    aput v13, v1, v12

    const/4 v12, -0x7

    aput v12, v1, v11

    const/16 v11, -0x41d8

    aput v11, v1, v10

    const/16 v10, -0x42

    aput v10, v1, v9

    const/16 v9, 0x7024

    aput v9, v1, v8

    const/16 v8, -0x6a90

    aput v8, v1, v7

    const/16 v7, -0x6b

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_34
    array-length v7, v1

    if-lt v3, v7, :cond_3d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_35
    array-length v7, v1

    if-lt v3, v7, :cond_3e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isStreaming:Z

    if-nez v1, :cond_3f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getViewForContentUri(Landroid/net/Uri;)Landroid/view/View;

    :goto_36
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    const/16 v1, 0x31

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, 0x2f

    const/16 v51, 0x30

    const/16 v52, -0xf

    aput v52, v2, v51

    const/16 v51, -0x31d8

    aput v51, v2, v50

    const/16 v50, -0x5f

    aput v50, v2, v49

    const/16 v49, 0x252

    aput v49, v2, v48

    const/16 v48, -0x129f

    aput v48, v2, v47

    const/16 v47, -0x3d

    aput v47, v2, v46

    const/16 v46, -0x4f

    aput v46, v2, v45

    const/16 v45, -0x27

    aput v45, v2, v44

    const/16 v44, 0x5a02

    aput v44, v2, v43

    const/16 v43, 0x1f35

    aput v43, v2, v42

    const/16 v42, 0x6c79

    aput v42, v2, v41

    const/16 v41, -0x34be

    aput v41, v2, v40

    const/16 v40, -0x47

    aput v40, v2, v39

    const/16 v39, -0x7e

    aput v39, v2, v38

    const/16 v38, -0x1f

    aput v38, v2, v37

    const/16 v37, -0x66f2

    aput v37, v2, v36

    const/16 v36, -0x4

    aput v36, v2, v35

    const/16 v35, -0x80

    aput v35, v2, v34

    const/16 v34, -0x41

    aput v34, v2, v33

    const/16 v33, 0x154e

    aput v33, v2, v32

    const/16 v32, -0x4a9f

    aput v32, v2, v31

    const/16 v31, -0x27

    aput v31, v2, v30

    const/16 v30, 0x6b75

    aput v30, v2, v29

    const/16 v29, -0x41f2

    aput v29, v2, v28

    const/16 v28, -0x2a

    aput v28, v2, v27

    const/16 v27, 0x3457

    aput v27, v2, v26

    const/16 v26, -0x23af

    aput v26, v2, v25

    const/16 v25, -0x41

    aput v25, v2, v24

    const/16 v24, 0x2102

    aput v24, v2, v23

    const/16 v23, -0x2da9

    aput v23, v2, v22

    const/16 v22, -0x60

    aput v22, v2, v21

    const/16 v21, -0x62d0

    aput v21, v2, v20

    const/16 v20, -0x12

    aput v20, v2, v19

    const/16 v19, -0xf0

    aput v19, v2, v18

    const/16 v18, -0x65

    aput v18, v2, v17

    const/16 v17, -0x16

    aput v17, v2, v16

    const/16 v16, -0x1d

    aput v16, v2, v15

    const/16 v15, 0xe55

    aput v15, v2, v14

    const/16 v14, -0x5c96

    aput v14, v2, v13

    const/16 v13, -0x33

    aput v13, v2, v12

    const/16 v12, -0x25

    aput v12, v2, v11

    const/16 v11, 0x572b

    aput v11, v2, v10

    const/16 v10, -0x77cc

    aput v10, v2, v9

    const/16 v9, -0x13

    aput v9, v2, v8

    const/16 v8, -0x30

    aput v8, v2, v7

    const/16 v7, -0x65

    aput v7, v2, v6

    const/16 v6, 0x2335

    aput v6, v2, v5

    const/16 v5, 0x234c

    aput v5, v2, v3

    const/16 v3, 0x4440

    aput v3, v2, v1

    const/16 v1, 0x31

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, 0x2f

    const/16 v52, 0x30

    const/16 v53, -0x6c

    aput v53, v1, v52

    const/16 v52, -0x31a5

    aput v52, v1, v51

    const/16 v51, -0x32

    aput v51, v1, v50

    const/16 v50, 0x23e

    aput v50, v1, v49

    const/16 v49, -0x12fe

    aput v49, v1, v48

    const/16 v48, -0x13

    aput v48, v1, v47

    const/16 v47, -0x2c

    aput v47, v1, v46

    const/16 v46, -0x46

    aput v46, v1, v45

    const/16 v45, 0x5a70

    aput v45, v1, v44

    const/16 v44, 0x1f5a

    aput v44, v1, v43

    const/16 v43, 0x6c1f

    aput v43, v1, v42

    const/16 v42, -0x3494

    aput v42, v1, v41

    const/16 v41, -0x35

    aput v41, v1, v40

    const/16 v40, -0x13

    aput v40, v1, v39

    const/16 v39, -0x6e

    aput v39, v1, v38

    const/16 v38, -0x66a0

    aput v38, v1, v37

    const/16 v37, -0x67

    aput v37, v1, v36

    const/16 v36, -0xd

    aput v36, v1, v35

    const/16 v35, -0x6f

    aput v35, v1, v34

    const/16 v34, 0x1526

    aput v34, v1, v33

    const/16 v33, -0x4aeb

    aput v33, v1, v32

    const/16 v32, -0x4b

    aput v32, v1, v31

    const/16 v31, 0x6b14

    aput v31, v1, v30

    const/16 v30, -0x4195

    aput v30, v1, v29

    const/16 v29, -0x42

    aput v29, v1, v28

    const/16 v28, 0x3479

    aput v28, v1, v27

    const/16 v27, -0x23cc

    aput v27, v1, v26

    const/16 v26, -0x24

    aput v26, v1, v25

    const/16 v25, 0x216b

    aput v25, v1, v24

    const/16 v24, -0x2ddf

    aput v24, v1, v23

    const/16 v23, -0x2e

    aput v23, v1, v22

    const/16 v22, -0x62ab

    aput v22, v1, v21

    const/16 v21, -0x63

    aput v21, v1, v20

    const/16 v20, -0xc2

    aput v20, v1, v19

    const/16 v19, -0x1

    aput v19, v1, v18

    const/16 v18, -0x7d

    aput v18, v1, v17

    const/16 v17, -0x74

    aput v17, v1, v16

    const/16 v16, 0xe27

    aput v16, v1, v15

    const/16 v15, -0x5cf2

    aput v15, v1, v14

    const/16 v14, -0x5d

    aput v14, v1, v13

    const/16 v13, -0x46

    aput v13, v1, v12

    const/16 v12, 0x5705

    aput v12, v1, v11

    const/16 v11, -0x77a9

    aput v11, v1, v10

    const/16 v10, -0x78

    aput v10, v1, v9

    const/16 v9, -0x5d

    aput v9, v1, v8

    const/16 v8, -0x4b

    aput v8, v1, v7

    const/16 v7, 0x2358

    aput v7, v1, v6

    const/16 v6, 0x2323

    aput v6, v1, v5

    const/16 v5, 0x4423

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_37
    array-length v5, v1

    if-lt v3, v5, :cond_40

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_38
    array-length v5, v1

    if-lt v3, v5, :cond_41

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;-><init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mCloseContentServiceReceiver:Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;

    invoke-static/range {p0 .. p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mCloseContentServiceReceiver:Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;

    invoke-virtual {v1, v2, v4}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    const/16 v1, 0x12

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, -0x8

    aput v20, v2, v19

    const/16 v19, 0x6a7c

    aput v19, v2, v18

    const/16 v18, -0x2e4

    aput v18, v2, v17

    const/16 v17, -0x6c

    aput v17, v2, v16

    const/16 v16, 0x444b

    aput v16, v2, v15

    const/16 v15, 0x6a27

    aput v15, v2, v14

    const/16 v14, 0x5a0f

    aput v14, v2, v13

    const/16 v13, -0x4dd8

    aput v13, v2, v12

    const/16 v12, -0x6e

    aput v12, v2, v11

    const/16 v11, 0x6506

    aput v11, v2, v10

    const/16 v10, 0x4900

    aput v10, v2, v9

    const/16 v9, -0x1ac3

    aput v9, v2, v8

    const/16 v8, -0x6a

    aput v8, v2, v7

    const/16 v7, -0x43

    aput v7, v2, v6

    const/16 v6, 0x105

    aput v6, v2, v5

    const/16 v5, 0x7e66

    aput v5, v2, v4

    const/16 v4, -0x2e5

    aput v4, v2, v3

    const/16 v3, -0x71

    aput v3, v2, v1

    const/16 v1, 0x12

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, -0x76

    aput v21, v1, v20

    const/16 v20, 0x6a19

    aput v20, v1, v19

    const/16 v19, -0x296

    aput v19, v1, v18

    const/16 v18, -0x3

    aput v18, v1, v17

    const/16 v17, 0x442e

    aput v17, v1, v16

    const/16 v16, 0x6a44

    aput v16, v1, v15

    const/16 v15, 0x5a6a

    aput v15, v1, v14

    const/16 v14, -0x4da6

    aput v14, v1, v13

    const/16 v13, -0x4e

    aput v13, v1, v12

    const/16 v12, 0x6574

    aput v12, v1, v11

    const/16 v11, 0x4965

    aput v11, v1, v10

    const/16 v10, -0x1ab7

    aput v10, v1, v9

    const/16 v9, -0x1b

    aput v9, v1, v8

    const/16 v8, -0x2c

    aput v8, v1, v7

    const/16 v7, 0x170

    aput v7, v1, v6

    const/16 v6, 0x7e01

    aput v6, v1, v5

    const/16 v5, -0x282

    aput v5, v1, v4

    const/4 v4, -0x3

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_39
    array-length v4, v1

    if-lt v3, v4, :cond_42

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3a
    array-length v4, v1

    if-lt v3, v4, :cond_43

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x12

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, -0x5e

    aput v21, v2, v20

    const/16 v20, -0x3fb5

    aput v20, v2, v19

    const/16 v19, -0x4c

    aput v19, v2, v18

    const/16 v18, -0x77

    aput v18, v2, v17

    const/16 v17, -0x7e

    aput v17, v2, v16

    const/16 v16, 0x225b

    aput v16, v2, v15

    const/16 v15, -0x1fb1

    aput v15, v2, v14

    const/16 v14, -0x71

    aput v14, v2, v13

    const/16 v13, -0x3fb7

    aput v13, v2, v12

    const/16 v12, -0x20

    aput v12, v2, v11

    const/16 v11, -0x6a

    aput v11, v2, v10

    const/16 v10, 0x4617

    aput v10, v2, v9

    const/16 v9, -0x26ce

    aput v9, v2, v8

    const/16 v8, -0x56

    aput v8, v2, v7

    const/16 v7, 0x7218

    aput v7, v2, v6

    const/16 v6, -0x5beb

    aput v6, v2, v5

    const/16 v5, -0x3f

    aput v5, v2, v3

    const/16 v3, -0x1a

    aput v3, v2, v1

    const/16 v1, 0x12

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, -0x3a

    aput v22, v1, v21

    const/16 v21, -0x3fd2

    aput v21, v1, v20

    const/16 v20, -0x40

    aput v20, v1, v19

    const/16 v19, -0x14

    aput v19, v1, v18

    const/16 v18, -0x12

    aput v18, v1, v17

    const/16 v17, 0x222b

    aput v17, v1, v16

    const/16 v16, -0x1fde

    aput v16, v1, v15

    const/16 v15, -0x20

    aput v15, v1, v14

    const/16 v14, -0x3fd6

    aput v14, v1, v13

    const/16 v13, -0x40

    aput v13, v1, v12

    const/16 v12, -0x1c

    aput v12, v1, v11

    const/16 v11, 0x4672

    aput v11, v1, v10

    const/16 v10, -0x26ba

    aput v10, v1, v9

    const/16 v9, -0x27

    aput v9, v1, v8

    const/16 v8, 0x7271

    aput v8, v1, v7

    const/16 v7, -0x5b8e

    aput v7, v1, v6

    const/16 v6, -0x5c

    aput v6, v1, v5

    const/16 v5, -0x6c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3b
    array-length v5, v1

    if-lt v3, v5, :cond_44

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3c
    array-length v5, v1

    if-lt v3, v5, :cond_45

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_9
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_d
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_e
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_f
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_10
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_11
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_12
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_13
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_14
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_15
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_16
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_17
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_18
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_19
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_1a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_1b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_12

    :cond_1c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_13

    :cond_1d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_14

    :cond_1e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_15

    :cond_1f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_16

    :cond_20
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_17

    :cond_21
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_18

    :cond_22
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_19

    :cond_23
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1a

    :cond_24
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1b

    :cond_25
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1c

    :cond_26
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1d

    :cond_27
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1e

    :cond_28
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1f

    :cond_29
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_20

    :cond_2a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_21

    :cond_2b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_22

    :cond_2c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_23

    :cond_2d
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_24

    :cond_2e
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_25

    :cond_2f
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_26

    :cond_30
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_27

    :cond_31
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_28

    :cond_32
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_29

    :cond_33
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2a

    :cond_34
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2b

    :cond_35
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2c

    :cond_36
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2d

    :cond_37
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2e

    :cond_38
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2f

    :cond_39
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_30

    :cond_3a
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_31

    :cond_3b
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_32

    :cond_3c
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_33

    :cond_3d
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_34

    :cond_3e
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_35

    :cond_3f
    sget v1, Lcom/sec/android/service/health/R$string;->record_popup_text:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isDirectSensorData:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->textContent:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->popupTitle_text:Landroid/widget/TextView;

    sget v2, Lcom/sec/android/service/health/R$string;->health_content_title_start_recording:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_36

    :cond_40
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_37

    :cond_41
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_38

    :cond_42
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_39

    :cond_43
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3a

    :cond_44
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3b

    :cond_45
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3c

    :catch_0
    move-exception v1

    goto/16 :goto_36
.end method

.method protected onDestroy()V
    .locals 19

    const/16 v0, 0xf

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, -0x69

    aput v16, v1, v15

    const/16 v15, -0x44

    aput v15, v1, v14

    const/16 v14, 0x734d

    aput v14, v1, v13

    const/16 v13, -0x2de1

    aput v13, v1, v12

    const/16 v12, -0x4d

    aput v12, v1, v11

    const/16 v11, -0x1bf5

    aput v11, v1, v10

    const/16 v10, -0x63

    aput v10, v1, v9

    const/16 v9, -0x708a

    aput v9, v1, v8

    const/4 v8, -0x3

    aput v8, v1, v7

    const/16 v7, -0x19

    aput v7, v1, v6

    const/16 v6, -0x26

    aput v6, v1, v5

    const/16 v5, -0x48

    aput v5, v1, v4

    const/16 v4, -0x3599

    aput v4, v1, v3

    const/16 v3, -0x5c

    aput v3, v1, v2

    const/16 v2, 0x4835

    aput v2, v1, v0

    const/16 v0, 0xf

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, -0xd

    aput v17, v0, v16

    const/16 v16, -0x27

    aput v16, v0, v15

    const/16 v15, 0x7321

    aput v15, v0, v14

    const/16 v14, -0x2d8d

    aput v14, v0, v13

    const/16 v13, -0x2e

    aput v13, v0, v12

    const/16 v12, -0x1b98

    aput v12, v0, v11

    const/16 v11, -0x1c

    aput v11, v0, v10

    const/16 v10, -0x70e7

    aput v10, v0, v9

    const/16 v9, -0x71

    aput v9, v0, v8

    const/16 v8, -0x6d

    aput v8, v0, v7

    const/16 v7, -0x57

    aput v7, v0, v6

    const/16 v6, -0x23

    aput v6, v0, v5

    const/16 v5, -0x35fd

    aput v5, v0, v4

    const/16 v4, -0x36

    aput v4, v0, v3

    const/16 v3, 0x485a

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    const/16 v0, 0xf

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, -0xc

    aput v17, v1, v16

    const/16 v16, 0x345b

    aput v16, v1, v15

    const/16 v15, 0x6e58

    aput v15, v1, v14

    const/16 v14, 0x6002

    aput v14, v1, v13

    const/16 v13, 0x5a01

    aput v13, v1, v12

    const/16 v12, -0x48c7

    aput v12, v1, v11

    const/16 v11, -0x32

    aput v11, v1, v10

    const/16 v10, -0x69

    aput v10, v1, v9

    const/16 v9, -0x5c

    aput v9, v1, v8

    const/16 v8, 0x3b1a

    aput v8, v1, v7

    const/16 v7, 0x1548

    aput v7, v1, v6

    const/16 v6, 0x4270

    aput v6, v1, v5

    const/16 v5, -0x5dda

    aput v5, v1, v4

    const/16 v4, -0x34

    aput v4, v1, v2

    const/16 v2, -0x37

    aput v2, v1, v0

    const/16 v0, 0xf

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, -0x70

    aput v18, v0, v17

    const/16 v17, 0x343e

    aput v17, v0, v16

    const/16 v16, 0x6e34

    aput v16, v0, v15

    const/16 v15, 0x606e

    aput v15, v0, v14

    const/16 v14, 0x5a60

    aput v14, v0, v13

    const/16 v13, -0x48a6

    aput v13, v0, v12

    const/16 v12, -0x49

    aput v12, v0, v11

    const/4 v11, -0x8

    aput v11, v0, v10

    const/16 v10, -0x2a

    aput v10, v0, v9

    const/16 v9, 0x3b6e

    aput v9, v0, v8

    const/16 v8, 0x153b

    aput v8, v0, v7

    const/16 v7, 0x4215

    aput v7, v0, v6

    const/16 v6, -0x5dbe

    aput v6, v0, v5

    const/16 v5, -0x5e

    aput v5, v0, v4

    const/16 v4, -0x5a

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public onStop()V
    .locals 18

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->closeByApp:Z

    if-nez v1, :cond_0

    new-instance v4, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/service/health/contentservice/HealthContentShow;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0xfdf

    aput v12, v2, v11

    const/16 v11, -0x62

    aput v11, v2, v10

    const/16 v10, -0x24

    aput v10, v2, v9

    const/16 v9, 0x561f

    aput v9, v2, v8

    const/16 v8, 0x3b37

    aput v8, v2, v7

    const/16 v7, -0x66a2

    aput v7, v2, v6

    const/16 v6, -0x15

    aput v6, v2, v5

    const/16 v5, -0x6c

    aput v5, v2, v3

    const/16 v3, -0x1686

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0xfba

    aput v13, v1, v12

    const/16 v12, -0x10

    aput v12, v1, v11

    const/16 v11, -0x4b

    aput v11, v1, v10

    const/16 v10, 0x5672

    aput v10, v1, v9

    const/16 v9, 0x3b56

    aput v9, v1, v8

    const/16 v8, -0x66c5

    aput v8, v1, v7

    const/16 v7, -0x67

    aput v7, v1, v6

    const/16 v6, -0x20

    aput v6, v1, v5

    const/16 v5, -0x16f7

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->isStreaming:Z

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0x633a

    aput v15, v2, v14

    const/16 v14, -0x3be9

    aput v14, v2, v13

    const/16 v13, -0x5b

    aput v13, v2, v12

    const/16 v12, -0xb

    aput v12, v2, v11

    const/16 v11, -0x4f

    aput v11, v2, v10

    const/16 v10, -0x33

    aput v10, v2, v9

    const/16 v9, -0x33

    aput v9, v2, v8

    const/16 v8, -0x6b

    aput v8, v2, v7

    const/16 v7, 0x555f

    aput v7, v2, v6

    const/16 v6, 0x113b

    aput v6, v2, v5

    const/16 v5, -0x1582

    aput v5, v2, v3

    const/16 v3, -0x77

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0x635b

    aput v16, v1, v15

    const/16 v15, -0x3b9d

    aput v15, v1, v14

    const/16 v14, -0x3c

    aput v14, v1, v13

    const/16 v13, -0x6f

    aput v13, v1, v12

    const/16 v12, -0x12

    aput v12, v1, v11

    const/16 v11, -0x47

    aput v11, v1, v10

    const/16 v10, -0x5d

    aput v10, v1, v9

    const/16 v9, -0x10

    aput v9, v1, v8

    const/16 v8, 0x552b

    aput v8, v1, v7

    const/16 v7, 0x1155

    aput v7, v1, v6

    const/16 v6, -0x15ef

    aput v6, v1, v5

    const/16 v5, -0x16

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->data:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x53

    aput v9, v2, v8

    const/16 v8, 0x3c75

    aput v8, v2, v7

    const/16 v7, -0x64ab

    aput v7, v2, v6

    const/16 v6, -0x11

    aput v6, v2, v5

    const/16 v5, -0x76e1

    aput v5, v2, v3

    const/16 v3, -0x18

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x3d

    aput v10, v1, v9

    const/16 v9, 0x3c1a

    aput v9, v1, v8

    const/16 v8, -0x64c4

    aput v8, v1, v7

    const/16 v7, -0x65

    aput v7, v1, v6

    const/16 v6, -0x7684

    aput v6, v1, v5

    const/16 v5, -0x77

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mAction:I

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/16 v6, -0x3e

    aput v6, v2, v5

    const/16 v5, -0x66

    aput v5, v2, v3

    const/16 v3, 0x1305

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, -0x55

    aput v7, v1, v6

    const/16 v6, -0x18

    aput v6, v1, v5

    const/16 v5, 0x1370

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x4391

    aput v14, v2, v13

    const/16 v13, -0x27

    aput v13, v2, v12

    const/16 v12, -0x31

    aput v12, v2, v11

    const/16 v11, 0xf0b

    aput v11, v2, v10

    const/16 v10, 0x436a

    aput v10, v2, v9

    const/16 v9, -0x62d5

    aput v9, v2, v8

    const/16 v8, -0x22

    aput v8, v2, v7

    const/16 v7, -0x50

    aput v7, v2, v6

    const/16 v6, 0x71f

    aput v6, v2, v5

    const/16 v5, -0x5f9e

    aput v5, v2, v3

    const/16 v3, -0x33

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x43f5

    aput v15, v1, v14

    const/16 v14, -0x44

    aput v14, v1, v13

    const/16 v13, -0x5c

    aput v13, v1, v12

    const/16 v12, 0xf68

    aput v12, v1, v11

    const/16 v11, 0x430f

    aput v11, v1, v10

    const/16 v10, -0x62bd

    aput v10, v1, v9

    const/16 v9, -0x63

    aput v9, v1, v8

    const/16 v8, -0x24

    aput v8, v1, v7

    const/16 v7, 0x77e

    aput v7, v1, v6

    const/16 v6, -0x5ff9

    aput v6, v1, v5

    const/16 v5, -0x60

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mealChecked:I

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x768f

    aput v14, v2, v13

    const/16 v13, -0x1c

    aput v13, v2, v12

    const/16 v12, 0x696b

    aput v12, v2, v11

    const/16 v11, -0x50c3

    aput v11, v2, v10

    const/16 v10, -0x25

    aput v10, v2, v9

    const/16 v9, -0x40c9

    aput v9, v2, v8

    const/16 v8, -0x26

    aput v8, v2, v7

    const/16 v7, -0x65

    aput v7, v2, v6

    const/16 v6, -0x4d4

    aput v6, v2, v5

    const/16 v5, -0x62

    aput v5, v2, v3

    const/16 v3, -0x50b3

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x76ec

    aput v15, v1, v14

    const/16 v14, -0x77

    aput v14, v1, v13

    const/16 v13, 0x6902

    aput v13, v1, v12

    const/16 v12, -0x5097

    aput v12, v1, v11

    const/16 v11, -0x51

    aput v11, v1, v10

    const/16 v10, -0x40bc

    aput v10, v1, v9

    const/16 v9, -0x41

    aput v9, v1, v8

    const/16 v8, -0x12

    aput v8, v1, v7

    const/16 v7, -0x4a3

    aput v7, v1, v6

    const/4 v6, -0x5

    aput v6, v1, v5

    const/16 v5, -0x50c1

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mRequestTime:J

    invoke-virtual {v4, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/4 v12, -0x5

    aput v12, v2, v11

    const/16 v11, -0x35

    aput v11, v2, v10

    const/16 v10, -0x60

    aput v10, v2, v9

    const/16 v9, -0x67

    aput v9, v2, v8

    const/16 v8, -0xa

    aput v8, v2, v7

    const/16 v7, -0x9

    aput v7, v2, v6

    const/16 v6, -0x31

    aput v6, v2, v5

    const/16 v5, -0x74

    aput v5, v2, v3

    const/16 v3, -0x14

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0x7e

    aput v13, v1, v12

    const/16 v12, -0x52

    aput v12, v1, v11

    const/16 v11, -0x15

    aput v11, v1, v10

    const/4 v10, -0x4

    aput v10, v1, v9

    const/16 v9, -0x7d

    aput v9, v1, v8

    const/16 v8, -0x7a

    aput v8, v1, v7

    const/16 v7, -0x5a

    aput v7, v1, v6

    const/16 v6, -0x1e

    aput v6, v1, v5

    const/16 v5, -0x67

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mUniqueKey:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x14000000

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, ""

    sget v1, Lcom/sec/android/service/health/R$string;->insert:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    long-to-int v1, v1

    const/high16 v2, 0x8000000

    move-object/from16 v0, p0

    invoke-static {v0, v1, v4, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0x180d

    aput v16, v2, v15

    const/16 v15, -0x889

    aput v15, v2, v14

    const/16 v14, -0x62

    aput v14, v2, v13

    const/16 v13, 0x6b05

    aput v13, v2, v12

    const/16 v12, -0x21f6

    aput v12, v2, v11

    const/16 v11, -0x43

    aput v11, v2, v10

    const/16 v10, -0x30

    aput v10, v2, v9

    const/16 v9, -0x31

    aput v9, v2, v8

    const/16 v8, 0xe48

    aput v8, v2, v7

    const/16 v7, 0x377a

    aput v7, v2, v6

    const/16 v6, -0x6da8

    aput v6, v2, v3

    const/4 v3, -0x4

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0x1863

    aput v17, v1, v16

    const/16 v16, -0x8e8

    aput v16, v1, v15

    const/16 v15, -0x9

    aput v15, v1, v14

    const/16 v14, 0x6b71

    aput v14, v1, v13

    const/16 v13, -0x2195

    aput v13, v1, v12

    const/16 v12, -0x22

    aput v12, v1, v11

    const/16 v11, -0x47

    aput v11, v1, v10

    const/16 v10, -0x57

    aput v10, v1, v9

    const/16 v9, 0xe21

    aput v9, v1, v8

    const/16 v8, 0x370e

    aput v8, v1, v7

    const/16 v7, -0x6dc9

    aput v7, v1, v6

    const/16 v6, -0x6e

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v6, v1

    if-lt v3, v6, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v6, v1

    if-lt v3, v6, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->nm:Landroid/app/NotificationManager;

    new-instance v1, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/sec/android/service/health/R$string;->s_health:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    sget v2, Lcom/sec/android/service/health/R$string;->content_ui_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v5, v3, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    sget v2, Lcom/sec/android/service/health/R$drawable;->s_health_icon_notification:I

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mNotification:Landroid/app/Notification;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->nm:Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mUniqueKey:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow;->mNotification:Landroid/app/Notification;

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->finish()V

    :cond_0
    invoke-super/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_f
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_10
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f
.end method
