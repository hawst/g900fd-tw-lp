.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private createResponse()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setEvent(I)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    return-void
.end method


# virtual methods
.method public doAction()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->DISCONNECT:[B

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendDataForStartSession([BLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method public responseReceived([B)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->DISCONNECT_ACK:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->AUTO_UPDATE_MODE:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->createResponse()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readDateFormatCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]JNJ"

    const-string v1, "[OnCommand]  cmdData has wrong value."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
