.class public Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
.super Ljava/lang/Object;


# static fields
.field private static instance:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

.field private static instanceForRestoreDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->instance:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    sput-object v0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->instanceForRestoreDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    return-void
.end method

.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    return-void
.end method

.method public static getInstance(Lsamsung/database/sqlite/SecSQLiteDatabase;)Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->instance:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->instance:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->instance:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    return-object v0
.end method

.method private throwException(Lsamsung/database/sqlite/SecSQLiteException;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "throwException "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/database/sqlite/SQLiteException;

    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    throw v0
.end method


# virtual methods
.method public beginTransaction()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->TAG:Ljava/lang/String;

    const-string v1, "beginTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransaction()V

    return-void
.end method

.method public delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v1, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    goto :goto_0
.end method

.method public endTransaction()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->TAG:Ljava/lang/String;

    const-string v1, "endTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    return-void
.end method

.method public execSQL(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    goto :goto_0
.end method

.method public getSQLiteDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getVersion()I

    move-result v0

    return v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    .locals 3

    const-string/jumbo v0, "shared_pref"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Insert Table : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/service/health/R$bool;->db_is_validator_enabled:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-static {v0, p1, p3, p4}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager;->checkForInsert(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    :cond_2
    const-wide/16 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v2, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    goto :goto_0
.end method

.method public query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    const/4 v8, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Lsamsung/database/sqlite/SecSQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    move-object v0, v8

    goto :goto_0
.end method

.method public query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    const/4 v9, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lsamsung/database/sqlite/SecSQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    move-object v0, v9

    goto :goto_0
.end method

.method public rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v1, p1, p2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    goto :goto_0
.end method

.method public replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    .locals 3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/service/health/R$bool;->db_is_validator_enabled:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-static {v0, p1, p3, p4}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager;->checkForReplace(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    :cond_0
    const-wide/16 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v2, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    goto :goto_0
.end method

.method public setTransactionSuccessful()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setTransactionSuccessful"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->setTransactionSuccessful()V

    return-void
.end method

.method public update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I
    .locals 6

    const-string/jumbo v0, "shared_pref"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update Table : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/service/health/R$bool;->db_is_validator_enabled:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager;->checkForUpdate(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    :cond_2
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v1, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->throwException(Lsamsung/database/sqlite/SecSQLiteException;)V

    goto :goto_0
.end method
