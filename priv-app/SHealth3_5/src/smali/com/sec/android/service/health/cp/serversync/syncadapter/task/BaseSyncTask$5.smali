.class Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->downloadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

.field final synthetic val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;",
            "J",
            "Lcom/sec/android/service/health/connectionmanager2/NetException;",
            "Ljava/lang/Object;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start Downloading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iput-wide p5, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->mTotalFileSize:J

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onDownload"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download STARTED, requestId : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", filePath : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->start(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->ONGOING:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ongoing Downloading... size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-wide v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->mTotalFileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Done Downloading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onDownload"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download DONE, requestId : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", filePath : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->end(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->dump()V

    :cond_3
    const-string v0, "/storage/emulated/0/SHealth3/cache/img/user_photo.jpg"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "downloadImageItem(): Profile image downloaded successfully"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/common/SyncPreference;->setLastProfileImageSyncedTime(J)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleImageDownloadDoneStopped()V

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v0, :cond_0

    if-eqz p7, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopped Downloading..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$500(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v4, v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Stopped Downloading..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleImageDownloadDoneStopped()V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopped Downloading..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$500(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v4, v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Stopped Downloading..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
