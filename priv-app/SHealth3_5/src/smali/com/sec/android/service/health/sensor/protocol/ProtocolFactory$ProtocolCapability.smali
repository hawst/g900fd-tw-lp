.class Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProtocolCapability"
.end annotation


# instance fields
.field deviceNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field deviceType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field devicedataType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protocol:Ljava/lang/String;

.field protocolClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->this$0:Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->deviceNames:Ljava/util/List;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->protocolClass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->deviceNames:Ljava/util/List;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->deviceType:Ljava/util/List;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->devicedataType:Ljava/util/List;

    iput-object p5, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->protocol:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->protocolClass:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method protected getDataTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->devicedataType:Ljava/util/List;

    return-object v0
.end method

.method protected getDeviceNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->deviceNames:Ljava/util/List;

    return-object v0
.end method

.method protected getDeviceTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->deviceType:Ljava/util/List;

    return-object v0
.end method

.method protected getProtocol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method protected getProtocolClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->protocolClass:Ljava/lang/Class;

    return-object v0
.end method
