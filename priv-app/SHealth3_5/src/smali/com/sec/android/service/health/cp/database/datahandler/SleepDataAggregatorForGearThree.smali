.class public Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field lastCreateTime:J

.field private mAppId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

.field private mSleepId:J

.field private mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->lastCreateTime:J

    return-void
.end method

.method private getContentValuesDetail()[Landroid/content/ContentValues;
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->status:[I

    array-length v1, v1

    new-array v3, v1, [Landroid/content/ContentValues;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v2, v3, v0

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v2, "sleep__id"

    iget-wide v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mSleepId:J

    invoke-static {v5, v2, v6, v7}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->status:[I

    aget v2, v2, v1

    const v6, 0x7fffffff

    if-eq v2, v6, :cond_0

    const-string/jumbo v2, "sleep_status"

    iget-object v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->status:[I

    aget v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    const-string/jumbo v2, "sample_time"

    iget-object v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->time:[J

    aget-wide v6, v6, v1

    invoke-static {v5, v2, v6, v7}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "application__id"

    iget-object v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v5, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v2, v1, 0x1

    aput-object v5, v3, v1

    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    :cond_2
    return-object v3
.end method

.method private getContentValuesSummary()Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "rise_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->endTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "bed_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->startTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "efficiency"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->efficiency:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string/jumbo v1, "quality"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->rating:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "hdid"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->devicePkId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getTagContentValues()Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "table_name_type"

    const v2, 0x55731

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->tagIndex:I

    if-eqz v1, :cond_0

    const-string v1, "input_source_type"

    const v2, 0x1adb2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "tag_index"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->tagIndex:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    :goto_0
    const-string/jumbo v1, "table_record_id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mSleepId:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "hdid"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->devicePkId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-string v1, "custom_tag_name"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->tagging:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "tag_index"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->tagIcon:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "input_source_type"

    const v2, 0x1adb1

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private insertDetailData()V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "insertDetailData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->getContentValuesDetail()[Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    int-to-long v0, v0

    const-string v2, "SleepDataAggregator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Bulk Data insert count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private insertSummaryData()V
    .locals 7

    const/4 v4, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "insertSummaryData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->getContentValuesSummary()Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hdid = \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->devicePkId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mSleepId:J

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->getTagContentValues()Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hdid = \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->devicePkId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mSleepId:J

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "table_record_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mSleepId:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sleep__id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mSleepId:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    new-instance v0, Landroid/database/sqlite/SQLiteConstraintException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SleepId does not find with this hdid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->devicePkId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteConstraintException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 2

    if-eqz p1, :cond_0

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    :goto_0
    return-void

    :cond_0
    const-string v0, "SleepDataAggregator"

    const-string v1, "Nothing to process"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "addData not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v3, "addData[]"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    array-length v3, p1

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, p1, v2

    invoke-virtual {p0, v4}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z

    move-result v5

    if-nez v5, :cond_0

    monitor-exit p0

    :goto_1
    return v0

    :cond_0
    invoke-direct {p0, v4}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->insertSummaryData()V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->insertDetailData()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while inserting into db"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v0, v1

    goto :goto_1

    :cond_1
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method protected checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public finalizeAggregation()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "Nothing to be done in finalization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    return-void
.end method

.method public isRunning(J)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "Don\'t have streaming Data"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method
