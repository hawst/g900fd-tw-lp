.class public abstract Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field protected lastDBtime:J

.field private final tempCal:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->lastDBtime:J

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    return-void
.end method

.method private getProfile(Landroid/content/Context;)Landroid/os/Parcelable;
    .locals 4

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SProfile;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, -0x1

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->age:I

    :goto_0
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->gender:I

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->heightUnit:I

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->height:F

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weightUnit:I

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getProfileUpdatedTime(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getSystemTimeMillisForBirthday(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->birthday:J

    invoke-static {p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getDistanceUnit(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getProfile:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/SProfile;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getSystemTimeMillisForBirthday(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getAgeValue(J)I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->age:I

    goto :goto_0
.end method


# virtual methods
.method protected changeProfileWeight(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 7

    const-wide v5, 0x40c3880000000000L    # 10000.0

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    float-to-double v1, v0

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Original Value == "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    mul-double/2addr v1, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    div-double/2addr v1, v5

    double-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Changed Value  == "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-object p1
.end method

.method protected changeProfileWeight(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;
    .locals 7

    const-wide v5, 0x40c3880000000000L    # 10000.0

    iget v0, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    float-to-double v0, v0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Original Value == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    mul-double/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    div-double/2addr v0, v5

    double-to-float v0, v0

    iput v0, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Changed Value  == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-object p1
.end method

.method protected checkDefaultValue(II)I
    .locals 6

    const/16 v0, 0x64

    const/16 v2, 0x1e

    const/16 v1, 0xa

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkDefaultValue() goalType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " input value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return p2

    :pswitch_1
    if-ge p2, v0, :cond_0

    move p2, v0

    goto :goto_0

    :pswitch_2
    if-gez p2, :cond_1

    const/16 p2, 0x12c

    goto :goto_0

    :cond_1
    if-ge p2, v1, :cond_0

    move p2, v1

    goto :goto_0

    :pswitch_3
    if-ge p2, v2, :cond_0

    move p2, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected getActivityTypeGoalValue(Landroid/content/Context;II)I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkDefaultValue() activityType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " goaltype : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    sparse-switch p2, :sswitch_data_0

    :goto_0
    invoke-virtual {p0, p3, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->checkDefaultValue(II)I

    move-result v0

    return v0

    :sswitch_0
    const v0, 0x9c47

    invoke-virtual {p0, p1, v0, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    goto :goto_0

    :sswitch_1
    const v0, 0x9c48

    invoke-virtual {p0, p1, v0, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    goto :goto_0

    :sswitch_2
    const v0, 0x9c49

    invoke-virtual {p0, p1, v0, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    goto :goto_0

    :sswitch_3
    const v0, 0x9c4a

    invoke-virtual {p0, p1, v0, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2712 -> :sswitch_1
        0x2afa -> :sswitch_0
        0x2ee1 -> :sswitch_3
        0x36b2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected getAgeValue(J)I
    .locals 7

    const/4 v6, 0x5

    const/4 v3, 0x1

    const/4 v5, 0x2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ge v3, v4, :cond_1

    add-int/lit8 v0, v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method protected getCoachingEnergy(Landroid/content/Context;)[Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getCoachingProfile(Landroid/content/Context;)[Landroid/os/Parcelable;
    .locals 7

    const/4 v6, 0x4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    const-string v1, "getCoachingProfile()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-array v1, v6, [Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;

    new-array v2, v6, [I

    fill-array-data v2, :array_0

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->lastDBtime:J

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    aget v4, v2, v0

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x3

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeGoal:J

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x2

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->distanceGoal:F

    aget-object v3, v1, v0

    aget v4, v2, v0

    invoke-virtual {p0, p1, v4, v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->caloryGoal:F

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x5

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectIntensityGoal:F

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x7

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectTimeGoal:J

    aget-object v3, v1, v0

    iget-wide v4, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->lastDBtime:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    nop

    :array_0
    .array-data 4
        0x2712
        0x2afa
        0x36b2
        0x2ee1
    .end array-data
.end method

.method protected getCoachingVar(Landroid/content/Context;)Landroid/os/Parcelable;
    .locals 7

    const/4 v2, 0x0

    new-instance v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;

    invoke-direct {v6}, Lcom/samsung/android/sdk/health/sensor/SCoaching;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "create_time DESC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_1

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ac"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-char v0, v0

    iput-char v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    const-string/jumbo v0, "maximum_heart_rate"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-char v0, v0

    iput-char v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    const-string/jumbo v0, "maximum_met"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    const-string/jumbo v0, "recovery_resource"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    const-string/jumbo v0, "start_time"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    const-string/jumbo v0, "training_level"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    const-string/jumbo v0, "training_level_update"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    const-string/jumbo v0, "previous_training_level"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    const-string/jumbo v0, "previous_to_previous_training_level"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    const-string v0, "latest_feedback_phrase_number"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    const-string v0, "latest_exercise_time"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v6

    :cond_3
    const/16 v0, 0x32

    :try_start_1
    iput-char v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    const/4 v0, 0x0

    iput-char v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    const-wide/16 v0, 0x0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    const/4 v0, 0x0

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    const/4 v0, -0x1

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    const-wide/16 v0, 0x0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    const/4 v0, -0x1

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    const/4 v0, -0x1

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    const/4 v0, -0x1

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    const-wide/16 v0, 0x0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method protected getEndOfDay(J)J
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calendar time zone offset "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected getGoalDataValue(Landroid/content/Context;II)I
    .locals 8

    const/4 v6, 0x0

    sparse-switch p2, :sswitch_data_0

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getGoalDataValue() goalType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " activityType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    const-string v3, "goal_type=?  AND goal_subtype=? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v7, -0x1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->lastDBtime:J

    const-string/jumbo v4, "update_time"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    const-string/jumbo v2, "update_time"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->lastDBtime:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return v0

    :sswitch_0
    const p2, 0x9c48

    goto :goto_0

    :sswitch_1
    const p2, 0x9c47

    goto :goto_0

    :sswitch_2
    const p2, 0x9c49

    goto/16 :goto_0

    :sswitch_3
    const p2, 0x9c4a

    goto/16 :goto_0

    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v7

    move-object v1, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_4
    move v0, v7

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2712 -> :sswitch_0
        0x2afa -> :sswitch_2
        0x2ee1 -> :sswitch_3
        0x36b2 -> :sswitch_1
    .end sparse-switch
.end method

.method protected getPedometerGoal(Landroid/content/Context;)Landroid/os/Parcelable;
    .locals 8

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "set_time"

    aput-object v0, v2, v5

    const-string/jumbo v0, "value"

    aput-object v0, v2, v4

    const/4 v0, 0x2

    const-string v1, "goal_type"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "goal_subtype"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "update_time"

    aput-object v1, v2, v0

    const-string v3, "goal_type=? "

    new-array v4, v4, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v5

    const v0, 0x9c41

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    if-eqz p1, :cond_5

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v5, "set_time DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/SGoal;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/SGoal;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getStepCount(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-int v0, v0

    if-ge v3, v0, :cond_1

    const/4 v0, 0x0

    iput v0, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->isAcheived:I

    :goto_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "pedometer goal= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "set_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    const-string v0, "goal_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->type:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v0, v2

    :goto_2
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_3
    return-object v0

    :cond_1
    const/4 v0, 0x1

    :try_start_3
    iput v0, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->isAcheived:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v7, v0

    move-object v0, v2

    move-object v2, v1

    move-object v1, v7

    :goto_4
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :cond_2
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v1, v6

    move-object v0, v6

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_5
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    move-object v0, v6

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    :cond_4
    move-object v0, v6

    goto :goto_2

    :cond_5
    move-object v1, v6

    goto/16 :goto_0
.end method

.method protected getProfileUpdatedTime(Landroid/content/Context;)J
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v1, "shared_pref"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "value"

    aput-object v3, v2, v7

    const-string v3, "key=?"

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v6, "update_time"

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_3

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "value"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v5, :cond_2

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_2
    move-wide v0, v8

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide v0, v8

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method protected getStartOfDay(J)J
    .locals 4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected getStepCount(Landroid/content/Context;)I
    .locals 8

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select *  from pedometer_integrated_view where start_time>= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "< "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getEndOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz p1, :cond_5

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "total_step"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    add-int/2addr v0, v2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    :cond_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v6

    move v0, v7

    :cond_2
    :goto_3
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return v0

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2

    :cond_4
    move v0, v7

    goto :goto_3

    :cond_5
    move-object v1, v6

    goto :goto_0
.end method

.method protected getSystemTimeMillisForBirthday(Ljava/lang/String;)J
    .locals 8

    const-wide/16 v0, 0x0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSystemTimeMillis : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyyMMdd"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyyMMdd HH:mm:ss"

    invoke-direct {v4, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v5, 0x1499700

    add-long/2addr v2, v5

    new-instance v5, Ljava/sql/Timestamp;

    invoke-direct {v5, v2, v3}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSystemTimeMillis : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", TimeZone.getDefault() : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-wide v0, v2

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v2

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    const-string v3, "getSystemTimeInDays : date is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected getyyyyMMddForBirthday(J)Ljava/lang/String;
    .locals 5

    const-wide/32 v0, 0x1499700

    add-long/2addr v0, p1

    :try_start_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyyMMdd HH:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/sql/Timestamp;

    invoke-direct {v3, v0, v1}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "yyyyMMdd"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/sql/Timestamp;

    invoke-direct {v4, v0, v1}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSystemTimeMillis_get. yyyyMMdd HH:mm:ss : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", yyyyMMdd "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", TimeZone.getDefault() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isExistGoalData(Landroid/content/Context;II)Z
    .locals 9

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v0, "value"

    aput-object v0, v2, v8

    const-string v3, "goal_type=?  AND goal_subtype=? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isExistGoalData() goalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " activityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v1, v6

    move v0, v8

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, v8

    goto :goto_0
.end method

.method public abstract makeMessageFromIntent(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Landroid/content/Intent;I)Landroid/os/Message;
.end method

.method public abstract makeMessageFromJWearableData(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;I)Landroid/os/Message;
.end method

.method public sendCoachingResponse(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/String;DJ)Z
    .locals 21

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getStartOfDay(J)J

    move-result-wide v4

    const-wide v6, 0x8b0bb400L

    sub-long/2addr v4, v6

    invoke-static/range {p2 .. p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getCoachingResponseActionName(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    const-string v3, "coaching_response, not found action name. return"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "start_time > "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "start_time"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " < "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x0

    if-eqz p1, :cond_11

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object v8, v2

    :goto_1
    if-eqz v8, :cond_9

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9

    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "exercise_type"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const-string v3, "distance"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string/jumbo v3, "total_calorie"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const-string v4, "duration_millisecond"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v4, v4

    const-wide/32 v6, 0xea60

    div-long v6, v4, v6

    long-to-int v14, v6

    const-string/jumbo v6, "start_time"

    invoke-interface {v8, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v8, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-string v15, "end_time"

    invoke-interface {v8, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v8, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    const-string v17, "_id"

    move-object/from16 v0, v17

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move/from16 v0, v17

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    if-lez v14, :cond_1

    const-wide/16 v19, 0x0

    cmp-long v4, v4, v19

    if-lez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, v15, v4

    if-eqz v4, :cond_1

    cmp-long v4, v6, v15

    if-nez v4, :cond_3

    :cond_1
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    const-string v3, "durationMin <= 0 || endTime == 0 || startTime == endTime = TRUE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    :cond_3
    const/16 v4, 0x4e21

    if-eq v2, v4, :cond_4

    const/16 v4, 0x4e22

    if-ne v2, v4, :cond_7

    :cond_4
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    const-string v3, "exerciseType == ExerciseType.ACTIVITY || exerciseType == ExerciseType.FITNESS = TRUE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exercise__id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v17

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;-><init>()V

    iput-wide v15, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    int-to-double v4, v13

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->distance:D

    const-string/jumbo v4, "training_load_peak"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteTrainingLoadPeak:I

    const-string/jumbo v4, "maximal_met"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteMaxMET:I

    const-string/jumbo v4, "resource_recovery"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteResourceRecovery:I

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SCoachingResult with TLP : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    :goto_4
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :cond_6
    int-to-double v3, v13

    const-wide/16 v5, 0x0

    cmpl-double v3, v3, v5

    if-lez v3, :cond_5

    new-instance v3, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;

    invoke-direct {v3}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;-><init>()V

    move-wide v0, v15

    invoke-virtual {v3, v0, v1}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->setEndTime(J)V

    invoke-virtual {v3, v14}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->setDuration(I)V

    int-to-double v4, v13

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->setDistance(D)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SCoachingResult temp : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_7
    int-to-double v4, v13

    const-wide/16 v6, 0x0

    cmpl-double v2, v4, v6

    if-lez v2, :cond_8

    new-instance v2, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;

    invoke-direct {v2}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;-><init>()V

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->setEndTime(J)V

    invoke-virtual {v2, v14}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->setDuration(I)V

    int-to-double v3, v13

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->setDistance(D)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCoachingResult temp : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x0

    cmpl-float v2, v3, v2

    if-lez v2, :cond_2

    new-instance v2, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;

    invoke-direct {v2}, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;-><init>()V

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->setEndTime(J)V

    int-to-long v4, v14

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->setDuration(J)V

    float-to-double v3, v3

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->setCalories(D)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCoachingResult temp : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_9
    if-eqz v8, :cond_a

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_a
    const-wide/16 v2, 0x0

    cmpl-double v2, p4, v2

    if-nez v2, :cond_f

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<EXERCISE RESULT>> : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_c

    const/4 v2, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v6, v3, [Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v2

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    add-int/lit8 v4, v3, 0x1

    aput-object v2, v6, v3

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EXERCISE Add data"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    goto :goto_5

    :cond_b
    const-string v2, "EXTRA_COACHING_RESULT"

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_6
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<RUNNING EXERCISE>> : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_10

    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v6, v3, [Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v2

    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;

    add-int/lit8 v4, v3, 0x1

    aput-object v2, v6, v3

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RUNNING Add data"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    goto :goto_7

    :cond_c
    const/4 v2, 0x0

    const-string v3, "EXTRA_COACHING_RESULT"

    invoke-virtual {v5, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_6

    :cond_d
    const-string v2, "EXTRA_COACHING_RUN"

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_8
    if-eqz p2, :cond_e

    const-string v2, "DEVICE"

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getWearableFixedNameFromDeviceType(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_e
    const-string v2, "VERSION"

    const-wide v3, 0x4008147ae147ae14L    # 3.01

    invoke-virtual {v5, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    const-string v2, "EXTRA_COACHING_VAR"

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getCoachingVar(Landroid/content/Context;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "EXTRA_USER_PROFILE"

    invoke-direct/range {p0 .. p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getProfile(Landroid/content/Context;)Landroid/os/Parcelable;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->changeProfileWeight(Landroid/os/Parcelable;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "EXTRA_PEDOMETER_GOAL"

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getPedometerGoal(Landroid/content/Context;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "EXTRA_COACHING_ENERGY"

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getCoachingEnergy(Landroid/content/Context;)[Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "EXTRA_COACHING_PROFILE"

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->getCoachingProfile(Landroid/content/Context;)[Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    const-string v3, "coaching_response, Old version ~Gear2"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    move-wide/from16 v0, p6

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->saveWearableLastSyncTime(ILjava/lang/String;J)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "saveWearableLastSyncTime (type= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",id= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_10
    const/4 v2, 0x0

    const-string v3, "EXTRA_COACHING_RUN"

    invoke-virtual {v5, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_8

    :cond_11
    move-object v8, v2

    goto/16 :goto_1
.end method

.method protected setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I
    .locals 7

    const/4 v0, -0x1

    sparse-switch p2, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    const v2, 0x9c47

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->setGoalDataValue(Landroid/content/Context;IIIJ)I

    move-result v0

    goto :goto_0

    :sswitch_1
    const v2, 0x9c48

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->setGoalDataValue(Landroid/content/Context;IIIJ)I

    move-result v0

    goto :goto_0

    :sswitch_2
    const v2, 0x9c49

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->setGoalDataValue(Landroid/content/Context;IIIJ)I

    move-result v0

    goto :goto_0

    :sswitch_3
    const v2, 0x9c4a

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->setGoalDataValue(Landroid/content/Context;IIIJ)I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2712 -> :sswitch_1
        0x2afa -> :sswitch_0
        0x2ee1 -> :sswitch_3
        0x36b2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected setGoalDataValue(Landroid/content/Context;IIIJ)I
    .locals 6

    const/4 v5, 0x0

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->isExistGoalData(Landroid/content/Context;II)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v2, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "goal_type=?  AND goal_subtype=? "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setGoalDataValue() goalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " activityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vlaue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setGoalDataValue() Exception occured while Goal insert: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "set_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v2, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "period"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-wide v0

    long-to-int v0, v0

    goto/16 :goto_0

    :catch_2
    move-exception v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setGoalDataValue() Exception occured while Goal insert: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
