.class public final enum Lcom/sec/android/service/health/connectionmanager2/MethodType;
.super Ljava/lang/Enum;
.source "MethodType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/connectionmanager2/MethodType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/connectionmanager2/MethodType;

.field public static final enum DELETE:Lcom/sec/android/service/health/connectionmanager2/MethodType;

.field public static final enum GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

.field public static final enum POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

.field public static final enum PUT:Lcom/sec/android/service/health/connectionmanager2/MethodType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/MethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    .line 18
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/connectionmanager2/MethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    .line 22
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/connectionmanager2/MethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->PUT:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    .line 26
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/service/health/connectionmanager2/MethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->DELETE:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/service/health/connectionmanager2/MethodType;

    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/MethodType;->PUT:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/MethodType;->DELETE:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->$VALUES:[Lcom/sec/android/service/health/connectionmanager2/MethodType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/MethodType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/connectionmanager2/MethodType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->$VALUES:[Lcom/sec/android/service/health/connectionmanager2/MethodType;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/connectionmanager2/MethodType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/connectionmanager2/MethodType;

    return-object v0
.end method
