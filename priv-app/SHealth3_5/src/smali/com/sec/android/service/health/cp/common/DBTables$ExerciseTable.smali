.class public Lcom/sec/android/service/health/cp/common/DBTables$ExerciseTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExerciseTable"
.end annotation


# static fields
.field public static final ACTIVITY_CLASS:Ljava/lang/String; = "activity_class"

.field public static final AVERAGE_HEART_BEAT:Ljava/lang/String; = "average_heart_beat"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "device_type"

.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_CALORIES:Ljava/lang/String; = "calories"

.field public static final KEY_COMMENT:Ljava/lang/String; = "comment"

.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_DISTANCE:Ljava/lang/String; = "distance"

.field public static final KEY_EXERCISE_ID:Ljava/lang/String; = "id_exercise_db"

.field public static final KEY_MIN:Ljava/lang/String; = "min"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final KEY_PULSE:Ljava/lang/String; = "pulse"

.field public static final KEY_SENSOR_ID:Ljava/lang/String; = "sensor_id"

.field public static final KEY_SPEED:Ljava/lang/String; = "walk_speed"

.field public static final MAX_SPEED:Ljava/lang/String; = "max_speed"

.field public static final STEP_COUNTS:Ljava/lang/String; = "step_counts"

.field public static final TABLE_NAME:Ljava/lang/String; = "exercise"

.field public static final TRAINING_EFFECT:Ljava/lang/String; = "training_effect"

.field public static final TRIGGER_DELETE_NAME:Ljava/lang/String; = "delete_exercise_trigger"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_exercise_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_exercise_trigger"

.field public static final WORKOUT_ID:Ljava/lang/String; = "workout_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
