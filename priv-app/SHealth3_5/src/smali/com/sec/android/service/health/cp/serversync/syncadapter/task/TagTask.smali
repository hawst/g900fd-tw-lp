.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TagTask;
.super Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;


# static fields
.field private static final TAG_CODE:Ljava/lang/String; = "tag"


# instance fields
.field private mDataType:I


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    iput p4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TagTask;->mDataType:I

    return-void
.end method


# virtual methods
.method protected getDeletedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->getDeletedTag(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getDetailHistoryData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TagTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/TagTask;->mDataType:I

    invoke-static {v0, p0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->getTagDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getSyncTypeString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "tag"

    return-object v0
.end method

.method protected getTableName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "tag"

    return-object v0
.end method

.method protected getUnsyncedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->getModifiedTag(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected updateItemsLocally(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncTagUtils;->addTag(Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method
