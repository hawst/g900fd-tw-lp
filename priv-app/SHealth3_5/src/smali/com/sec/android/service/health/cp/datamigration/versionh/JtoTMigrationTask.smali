.class public Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;
.super Lcom/sec/android/service/health/cp/BaseInitializationTask;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    return-void
.end method

.method private migrate2xLocalDB()V
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "migrate2xLocalDB"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "upgrade_prefs"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->upgradeToH()V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->rename25DbFile()V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_version_of_upgrade"

    sget-object v3, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->H:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isUpgradeCheckRequired"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private rename25DbFile()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->TAG:Ljava/lang/String;

    const-string v1, "Renaming secure_shealth2.db to platform.db"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "secure_shealth2.db"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "platform.db"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "rename secure_shealth2.db to platform.db failed"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "ERR_MI25"

    const-string v3, "FAIL1"

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "rename secure_shealth2.db to platform.db failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->TAG:Ljava/lang/String;

    const-string v1, "Db file secure_shealth2.db not present. "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "rename secure_shealth2.db to platform.db failed -- secure_shealth2.db not present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private upgradeToH()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "upgradeToH"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;-><init>(Landroid/content/Context;)V

    :try_start_0
    new-instance v1, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v1}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    iget-object v1, v1, Lcom/sec/android/service/health/cp/common/AESEncryption;->str:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "changeAEStoTrustZonePassword failed KEY_GET_AES_PASSWORD"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->TAG:Ljava/lang/String;

    const-string v2, "H DB open failed with AES password"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "H DB open failed with AES password"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    :try_start_1
    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    return-void
.end method


# virtual methods
.method protected handelCommand(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check2XDBFileExist(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-array v0, v3, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->publishProgress([Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->migrate2xLocalDB()V

    new-array v0, v3, [Ljava/lang/Integer;

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->publishProgress([Ljava/lang/Object;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->initialize()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v3, [Ljava/lang/Integer;

    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->publishProgress([Ljava/lang/Object;)V

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->H:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->migrateFiles(Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;)V

    new-array v0, v3, [Ljava/lang/Integer;

    const/16 v1, 0x50

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->publishProgress([Ljava/lang/Object;)V

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->H:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->upgradeSharedPreference(Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;)V

    new-array v0, v3, [Ljava/lang/Integer;

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->publishProgress([Ljava/lang/Object;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.shealth"

    const-string v3, "ERR_MI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FAIL5:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    throw v0
.end method
