.class Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceConnected() : PackageInstallService is connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    invoke-static {p2}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v1

    # setter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$002(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$100(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;->registerCallback(Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$200(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$200(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$200(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v1, :cond_2

    :cond_0
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceConnected() : [UPGRADE_STATE_2_2] Deleting FitnessWithGear."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealthlite"

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;->deletePackage(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$200(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v1, :cond_3

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceConnected() : [UPGRADE_STATE_2_3] Deleting FitnessWithGear."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_2
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealthlite"

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;->deletePackage(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSensorServiceInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSensorServiceEnabled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    :try_start_3
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceConnected() : [UPGRADE_STATE_2_3] Disabling Sensor Service."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    const-string v1, "com.sec.android.service.health.sensor"

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;->disablePackage(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$200(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSleepMonitorInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    if-eqz v0, :cond_4

    :try_start_4
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceConnected() : [UPGRADE_STATE_APP_UPGRADE_DONE] Deleting Sleep Monitor."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.plugins.sleepmonitor"

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;->deletePackage(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_5

    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSensorServiceInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSensorServiceEnabled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_5
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceConnected() : [UPGRADE_STATE_APP_UPGRADE_DONE] Disabling Sensor Service."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    # getter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    move-result-object v0

    const-string v1, "com.sec.android.service.health.sensor"

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;->disablePackage(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    :cond_5
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceConnected() : ILLEGAL STATE. UPGRADE SCENARIO DOES NOT TREAT THIS SCENARIO."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "onServiceDisconnected() : PackageInstallService is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;->this$0:Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->access$002(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    return-void
.end method
