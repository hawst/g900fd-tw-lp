.class Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->requestDownloadAllhistory()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadMeasurementsStatus(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x1

    const-string v0, "[HealthSensor]BPSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDownloadMeasurementsStatus code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " statusCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const-string v0, "[HealthSensor]BPSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDownloadMeasurementsStatus goto disconnect "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mAntDeviceID:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I
    invoke-static {v0, v4}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->access$802(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;I)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->access$1000(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->access$900(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->access$1102(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;I)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$7;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$AntFsRequestStatus:[I

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "[HealthSensor]BPSensor"

    const-string v1, "DownloadAllHistory failed, unkwon error."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_0
    const-string v0, "[HealthSensor]BPSensor"

    const-string v1, "DownloadAllHistory finished successfully."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_1
    const-string v0, "[HealthSensor]BPSensor"

    const-string v1, "DownloadAllHistory failed, device busy."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor]BPSensor"

    const-string v1, "DownloadAllHistory failed, communication error."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_3
    const-string v0, "[HealthSensor]BPSensor"

    const-string v1, "DownloadAllHistory failed, authentication rejected."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_4
    const-string v0, "[HealthSensor]BPSensor"

    const-string v1, "DownloadAllHistory failed, transmission lost."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
