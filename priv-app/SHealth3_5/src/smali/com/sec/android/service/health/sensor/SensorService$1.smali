.class final Lcom/sec/android/service/health/sensor/SensorService$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/SensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "status"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_3

    sget-object v4, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    :try_start_1
    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    new-array v8, v8, [Z

    const/4 v9, 0x0

    aput-boolean v3, v8, v9

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "__all__"

    aput-object v11, v9, v10

    invoke-interface {v1, v6, v7, v8, v9}, Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;->onDataAction(J[Z[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregister(Ljava/lang/Object;)Z

    :cond_2
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    return-void
.end method
