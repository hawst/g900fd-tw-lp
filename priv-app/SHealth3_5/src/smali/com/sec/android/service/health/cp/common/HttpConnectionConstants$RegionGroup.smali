.class public final enum Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
.super Ljava/lang/Enum;
.source "HttpConnectionConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RegionGroup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

.field public static final enum CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

.field public static final enum EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

.field public static final enum KR:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

.field public static final enum US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    const-string v1, "US"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    new-instance v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    const-string v1, "EU"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    new-instance v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    const-string v1, "CN"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    new-instance v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    const-string v1, "KR"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->KR:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->KR:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->$VALUES:[Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->$VALUES:[Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    return-object v0
.end method
