.class public final Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatLng"
.end annotation


# instance fields
.field public latitude:D

.field public longitude:D


# direct methods
.method public constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    iput-wide p3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    return-void
.end method
