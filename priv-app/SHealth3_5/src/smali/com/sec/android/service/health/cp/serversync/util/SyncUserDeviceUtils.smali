.class public Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;
    }
.end annotation


# static fields
.field private static final MAX_USERDEVICE_RECORDS:I = 0x1f4

.field private static final REQUEST_TYPE:Ljava/lang/String; = "I"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized addModifyUserDevice(Ljava/lang/Object;)V
    .locals 8

    const-class v1, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils;->TAG:Ljava/lang/String;

    const-string v2, "addModifyUserDevice"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v2, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;

    invoke-virtual {v0, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;

    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->userAccessaryList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->userAccessaryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->userAccessaryList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "_id"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceUserAccPKId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "application__id"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->appId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "device_id"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->serialId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "device_type"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceTypeCode:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "custom_name"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->accessaryName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "model"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->modelId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "connectivity_type"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->connectivityType:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "manufacture"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->manufactureName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "create_time"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v5, "update_time"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceModifyTime:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v5, "time_zone"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceDaylightSaving:Ljava/lang/String;

    if-eqz v5, :cond_0

    const-string v5, "daylight_saving"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string v5, "hdid"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->hdid:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "device_group_type"

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceGroupType:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "sync_status"

    const v5, 0x29811

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string/jumbo v0, "user_device"

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v6

    invoke-virtual {v2, v0, v5, v4, v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error inserting user_device data. Invalid values: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_5
    const-string v5, "daylight_saving"

    const v6, 0x445c2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_2
    monitor-exit v1

    return-void
.end method

.method public static getModifiedUserDevice(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/CursorWindowAllocationException;
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v1, "select distinct application__id from user_device"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    const-string v3, "application__id = ? "

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x0

    const-string v5, "application__id"

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const-string/jumbo v1, "user_device"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_3

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;

    invoke-direct {v5}, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;-><init>()V

    const-string v1, "application__id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->appId:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;

    invoke-direct {v7}, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;-><init>()V

    const-string v10, "application__id"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->appId:Ljava/lang/String;

    const-string v10, "_id"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->accessaryUId:Ljava/lang/String;

    const-string/jumbo v10, "sync_status"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v10}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->requestType:Ljava/lang/String;

    const-string v10, "device_type"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceTypeCode:Ljava/lang/String;

    const-string v10, "connectivity_type"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->connectivityType:Ljava/lang/String;

    const-string v10, "device_id"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->serialId:Ljava/lang/String;

    const-string v10, "custom_name"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->accessaryName:Ljava/lang/String;

    const-string/jumbo v10, "model"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->modelId:Ljava/lang/String;

    const-string/jumbo v10, "manufacture"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->manufactureName:Ljava/lang/String;

    const-string v10, "create_time"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceCreateTime:Ljava/lang/String;

    const-string/jumbo v10, "update_time"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceModifyTime:Ljava/lang/String;

    const-string/jumbo v10, "time_zone"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceTimeZone:Ljava/lang/String;

    const-string v10, "_id"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceUserAccPKId:Ljava/lang/String;

    const-string v10, "daylight_saving"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceDaylightSaving:Ljava/lang/String;

    const-string v10, "hdid"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->hdid:Ljava/lang/String;

    const-string v10, "device_group_type"

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceGroupType:Ljava/lang/String;

    iget-object v10, v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->userAccessaryList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/16 v11, 0x1f4

    if-ne v10, v11, :cond_0

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v11, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USERDEVICE_INFO:Ljava/lang/String;

    invoke-direct {v10, v11, v5, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;

    invoke-direct {v5}, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;-><init>()V

    const-string v10, "application__id"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->appId:Ljava/lang/String;

    :cond_0
    iget-object v10, v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->userAccessaryList:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v11, "user_device"

    iget-object v7, v7, Lcom/sec/android/service/health/cp/serversync/data/UserDeviceItem;->deviceUserAccPKId:Ljava/lang/String;

    const-string/jumbo v12, "update_time"

    invoke-interface {v6, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v6, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-direct {v10, v11, v7, v12, v13}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_1

    :cond_1
    iget-object v7, v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetUserDevice;->userAccessaryList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_2

    new-instance v7, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v10, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USERDEVICE_INFO:Ljava/lang/String;

    invoke-direct {v7, v10, v5, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    :cond_4
    if-eqz v9, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_5
    return-object v8
.end method

.method public static getUserDeviceDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requesting "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " items for user device"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->appId:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->appId:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;

    invoke-direct {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;-><init>()V

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMdd(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->startDate:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMdd(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->endDate:Ljava/lang/String;

    iput-object v0, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->appId:Ljava/lang/String;

    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->appId:Ljava/lang/String;

    iget-object v6, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->appId:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v6, 0x3e7

    if-ne v0, v6, :cond_4

    iget-object v6, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v6, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USERDEVICE_INFO:Ljava/lang/String;

    invoke-direct {v0, v6, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    :cond_3
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    iget-object v6, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    iget-object v0, v5, Lcom/sec/android/service/health/cp/serversync/util/SyncUserDeviceUtils$RequestGetUserDeviceHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USERDEVICE_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    return-object v3
.end method
