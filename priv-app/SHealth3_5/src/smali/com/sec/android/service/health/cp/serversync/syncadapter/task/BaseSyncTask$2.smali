.class Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleSetServerData(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->syncedRowIdList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DB-update"

    const-string/jumbo v3, "update sync status STARTED"

    invoke-static {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->start(Ljava/lang/String;)V

    :cond_0
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->syncedRowIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->syncedRowIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    invoke-virtual {v3, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->updateSyncStatusField(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DB-update"

    const-string/jumbo v3, "update sync status DONE"

    invoke-static {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->end(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->dump()V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    const/16 v1, 0x2711

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V

    const/4 v0, 0x0

    return-object v0
.end method
