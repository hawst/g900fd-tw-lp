.class public interface abstract Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil$CRUDType;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CRUDType"
.end annotation


# static fields
.field public static final CREATE:I = 0x1

.field public static final DELETE:I = 0x4

.field public static final READ:I = 0x2

.field public static final UPDATE:I = 0x3
