.class public Lcom/sec/android/service/health/contentservice/ContentService;
.super Landroid/app/Service;


# static fields
.field private static mCbContentServiceHandler:Landroid/os/Handler;

.field private static mWatchDogColumns:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBinder:Lcom/samsung/android/sdk/health/content/_ContentService$Stub;

.field private mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 22

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;

    sget-object v3, Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v0, 0xf

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, -0x60af

    aput v20, v1, v19

    const/16 v19, -0x6

    aput v19, v1, v18

    const/16 v18, -0x68

    aput v18, v1, v17

    const/16 v17, -0x23

    aput v17, v1, v16

    const/16 v16, -0x63

    aput v16, v1, v15

    const/16 v15, -0x699d

    aput v15, v1, v14

    const/16 v14, -0x1b

    aput v14, v1, v13

    const/16 v13, -0x17

    aput v13, v1, v12

    const/16 v12, 0x1e76

    aput v12, v1, v11

    const/16 v11, 0x6e73

    aput v11, v1, v10

    const/16 v10, 0x1231

    aput v10, v1, v9

    const/16 v9, -0x1695

    aput v9, v1, v8

    const/16 v8, -0x73

    aput v8, v1, v7

    const/16 v7, -0x32

    aput v7, v1, v2

    const/16 v2, -0x3f

    aput v2, v1, v0

    const/16 v0, 0xf

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, -0x60d7

    aput v21, v0, v20

    const/16 v20, -0x61

    aput v20, v0, v19

    const/16 v19, -0x4

    aput v19, v0, v18

    const/16 v18, -0x4d

    aput v18, v0, v17

    const/16 v17, -0xc

    aput v17, v0, v16

    const/16 v16, -0x69c4

    aput v16, v0, v15

    const/16 v15, -0x6a

    aput v15, v0, v14

    const/16 v14, -0x66

    aput v14, v0, v13

    const/16 v13, 0x1e17

    aput v13, v0, v12

    const/16 v12, 0x6e1e

    aput v12, v0, v11

    const/16 v11, 0x126e

    aput v11, v0, v10

    const/16 v10, -0x16ee

    aput v10, v0, v9

    const/16 v9, -0x17

    aput v9, v0, v8

    const/16 v8, -0x5f

    aput v8, v0, v7

    const/16 v7, -0x5d

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v7, v0

    if-lt v2, v7, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v7, v0

    if-lt v2, v7, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/16 v11, -0x678f

    aput v11, v1, v10

    const/16 v10, -0x10

    aput v10, v1, v9

    const/16 v9, -0xc

    aput v9, v1, v8

    const/16 v8, 0x722f

    aput v8, v1, v7

    const/16 v7, -0x4e9

    aput v7, v1, v2

    const/16 v2, -0x74

    aput v2, v1, v0

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/16 v12, -0x67fb

    aput v12, v0, v11

    const/16 v11, -0x68

    aput v11, v0, v10

    const/16 v10, -0x6d

    aput v10, v0, v9

    const/16 v9, 0x7246

    aput v9, v0, v8

    const/16 v8, -0x48e

    aput v8, v0, v7

    const/4 v7, -0x5

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v7, v0

    if-lt v2, v7, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v7, v0

    if-lt v2, v7, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x11

    aput v13, v1, v12

    const/16 v12, -0x6aa0

    aput v12, v1, v11

    const/4 v11, -0x7

    aput v11, v1, v10

    const/16 v10, -0x12

    aput v10, v1, v9

    const/16 v9, -0x6bb5

    aput v9, v1, v8

    const/16 v8, -0x19

    aput v8, v1, v7

    const/16 v7, -0x48bf

    aput v7, v1, v2

    const/16 v2, -0x3c

    aput v2, v1, v0

    const/16 v0, 0x8

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, -0x74

    aput v14, v0, v13

    const/16 v13, -0x6af7

    aput v13, v0, v12

    const/16 v12, -0x6b

    aput v12, v0, v11

    const/16 v11, -0x7f

    aput v11, v0, v10

    const/16 v10, -0x6bc1

    aput v10, v0, v9

    const/16 v9, -0x6c

    aput v9, v0, v8

    const/16 v8, -0x48c8

    aput v8, v0, v7

    const/16 v7, -0x49

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v7, v0

    if-lt v2, v7, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v7, v0

    if-lt v2, v7, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    const/16 v0, 0x9

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, -0x43

    aput v14, v1, v13

    const/4 v13, -0x7

    aput v13, v1, v12

    const/16 v12, -0x12a8

    aput v12, v1, v11

    const/16 v11, -0x7e

    aput v11, v1, v10

    const/16 v10, -0xf

    aput v10, v1, v9

    const/16 v9, 0x6a27

    aput v9, v1, v8

    const/16 v8, -0xcf5

    aput v8, v1, v7

    const/16 v7, -0x66

    aput v7, v1, v2

    const/16 v2, 0x245a

    aput v2, v1, v0

    const/16 v0, 0x9

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, -0x22

    aput v15, v0, v14

    const/16 v14, -0x70

    aput v14, v0, v13

    const/16 v13, -0x12cc

    aput v13, v0, v12

    const/16 v12, -0x13

    aput v12, v0, v11

    const/16 v11, -0x7b

    aput v11, v0, v10

    const/16 v10, 0x6a54

    aput v10, v0, v9

    const/16 v9, -0xc96

    aput v9, v0, v8

    const/16 v8, -0xd

    aput v8, v0, v7

    const/16 v7, 0x243e

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_6
    array-length v7, v0

    if-lt v2, v7, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_7
    array-length v7, v0

    if-lt v2, v7, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v6, 0x2

    const/4 v0, 0x5

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/16 v10, 0x3b08

    aput v10, v1, v9

    const/16 v9, 0x3448

    aput v9, v1, v8

    const/16 v8, -0x45a8

    aput v8, v1, v7

    const/16 v7, -0x31

    aput v7, v1, v2

    const/16 v2, 0x618

    aput v2, v1, v0

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/16 v11, 0x3b6d

    aput v11, v0, v10

    const/16 v10, 0x343b

    aput v10, v0, v9

    const/16 v9, -0x45cc

    aput v9, v0, v8

    const/16 v8, -0x46

    aput v8, v0, v7

    const/16 v7, 0x668

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_8
    array-length v7, v0

    if-lt v2, v7, :cond_8

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_9
    array-length v7, v0

    if-lt v2, v7, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/16 v12, -0x22

    aput v12, v1, v11

    const/16 v11, -0x58

    aput v11, v1, v10

    const/16 v10, 0x3423

    aput v10, v1, v9

    const/16 v9, 0x5c57

    aput v9, v1, v8

    const/16 v8, -0x7dd7

    aput v8, v1, v7

    const/16 v7, -0x12

    aput v7, v1, v2

    const/16 v2, -0x3fc

    aput v2, v1, v0

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/16 v13, -0x45

    aput v13, v0, v12

    const/16 v12, -0x25

    aput v12, v0, v11

    const/16 v11, 0x344c

    aput v11, v0, v10

    const/16 v10, 0x5c34

    aput v10, v0, v9

    const/16 v9, -0x7da4

    aput v9, v0, v8

    const/16 v8, -0x7e

    aput v8, v0, v7

    const/16 v7, -0x39d

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_a
    array-length v7, v0

    if-lt v2, v7, :cond_a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_b
    array-length v7, v0

    if-lt v2, v7, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    const/4 v0, 0x5

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/16 v10, -0x78

    aput v10, v1, v9

    const/16 v9, -0x42

    aput v9, v1, v8

    const/16 v8, -0x699d

    aput v8, v1, v7

    const/16 v7, -0xc

    aput v7, v1, v2

    const/16 v2, -0x5c

    aput v2, v1, v0

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/16 v11, -0x15

    aput v11, v0, v10

    const/16 v10, -0x71

    aput v10, v0, v9

    const/16 v9, -0x69fe

    aput v9, v0, v8

    const/16 v8, -0x6a

    aput v8, v0, v7

    const/16 v7, -0x34

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_c
    array-length v7, v0

    if-lt v2, v7, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_d
    array-length v7, v0

    if-lt v2, v7, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v0, 0xb

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, -0x79

    aput v16, v1, v15

    const/16 v15, -0x77

    aput v15, v1, v14

    const/16 v14, 0x102a

    aput v14, v1, v13

    const/16 v13, 0x7464

    aput v13, v1, v12

    const/16 v12, 0x1915

    aput v12, v1, v11

    const/16 v11, 0x3b6b

    aput v11, v1, v10

    const/16 v10, -0x2aa2

    aput v10, v1, v9

    const/16 v9, -0x5b

    aput v9, v1, v8

    const/16 v8, 0x394f

    aput v8, v1, v7

    const/16 v7, 0x595c

    aput v7, v1, v2

    const/16 v2, -0x49d3

    aput v2, v1, v0

    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, -0x1e

    aput v17, v0, v16

    const/16 v16, -0x5

    aput v16, v0, v15

    const/16 v15, 0x105f

    aput v15, v0, v14

    const/16 v14, 0x7410

    aput v14, v0, v13

    const/16 v13, 0x1974

    aput v13, v0, v12

    const/16 v12, 0x3b19

    aput v12, v0, v11

    const/16 v11, -0x2ac5

    aput v11, v0, v10

    const/16 v10, -0x2b

    aput v10, v0, v9

    const/16 v9, 0x3922

    aput v9, v0, v8

    const/16 v8, 0x5939

    aput v8, v0, v7

    const/16 v7, -0x49a7

    aput v7, v0, v2

    const/4 v2, 0x0

    :goto_e
    array-length v7, v0

    if-lt v2, v7, :cond_e

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_f
    array-length v7, v0

    if-lt v2, v7, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/sec/android/service/health/contentservice/ContentService$2;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/ContentService$2;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/contentservice/ContentService;->mCbContentServiceHandler:Landroid/os/Handler;

    return-void

    :cond_0
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_5
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_6
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    :cond_7
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7

    :cond_8
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_8

    :cond_9
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_9

    :cond_a
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_a

    :cond_b
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_b

    :cond_c
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_c

    :cond_d
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_d

    :cond_e
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_e

    :cond_f
    aget v7, v1, v2

    int-to-char v7, v7

    aput-char v7, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_f
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/sec/android/service/health/contentservice/ContentService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/contentservice/ContentService$1;-><init>(Lcom/sec/android/service/health/contentservice/ContentService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/contentservice/ContentService;->mBinder:Lcom/samsung/android/sdk/health/content/_ContentService$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/contentservice/ContentService;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/ContentService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/contentservice/ContentService;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/ContentService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    return-object p1
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/contentservice/ContentService;->mCbContentServiceHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/ContentService;->mBinder:Lcom/samsung/android/sdk/health/content/_ContentService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method
