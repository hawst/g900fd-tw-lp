.class public Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeLogger"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;,
        Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mLogTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;",
            ">;"
        }
    .end annotation
.end field

.field private static mTotalLogCount:J

.field private static mTotalTimeSpent:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static dumpStats()V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->TAG:Ljava/lang/String;

    const-string v1, "dumpStats"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "total count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-wide v3, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalLogCount:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", total duration : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalTimeSpent:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static end(J)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "input param key invalid"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->end(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    move-result-object v0

    return-object v0
.end method

.method public static end(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "input param key is null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->start(Ljava/lang/String;)V

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->setEndTime()V

    sget v1, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalTimeSpent:F

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->getDuration()F

    move-result v0

    add-float/2addr v0, v1

    sput v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalTimeSpent:F

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;-><init>(Ljava/lang/String;Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$1;)V

    return-object v0
.end method

.method public static reset()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalLogCount:J

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalTimeSpent:F

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "reset"

    const-string/jumbo v2, "table and stats are reset."

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static start(J)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "input param key is invalid"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->start(Ljava/lang/String;)V

    return-void
.end method

.method public static start(Ljava/lang/String;)V
    .locals 4

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "input param key is null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    new-instance v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-wide v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalLogCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mTotalLogCount:J

    return-void
.end method
