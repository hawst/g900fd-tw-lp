.class public Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    }
.end annotation


# static fields
.field private static final CHECK_FILE_WITH_FITNESS_WITH_GEAR:Ljava/lang/String; = "/FitnessWithGearUpgradeMarkFlag.xml"

.field private static EDITOR_LOCK:Ljava/lang/Object; = null

.field private static final TAG:Ljava/lang/String; = "SHealthUpgrade(SHealthUpgradeUtil)"

.field private static currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field private static editor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_NONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static check3xDBExists(Landroid/content/Context;)Z
    .locals 7

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService2xInstalled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService1xInstalled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService0xInstalled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v3, "[3XPRECEDENCE] Some version of HealthService is present"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

    const-string v4, "check_platform_db_exists"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    const-string/jumbo v2, "value_of_check_if_platform_db_exists"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "[3XPRECEDENCE] 3x DB does not exist"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return v0

    :catch_0
    move-exception v1

    const-string v3, "SHealthUpgrade(SHealthUpgradeUtil)"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[3XPRECEDENCE] Probable Exception is because MigrationCP Provider not found as Device was in Ultra power saving mode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    goto :goto_0

    :cond_2
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "[3XPRECEDENCE] 3x DB is found"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "[3XPRECEDENCE] 3x DB does not exist because no HealthService was present"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static clearMigrationState(Landroid/content/Context;)V
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/FitnessWithGearUpgradeMarkFlag.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MigrationSharedPreference"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    sput-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.sec.android.app.shealth"

    const-string v3, "ERR_USTATUS"

    const-string v4, "FAIL7"

    const/4 v5, 0x0

    invoke-static {p0, v1, v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "/shared_prefs/MigrationSharedPreference.xml"

    new-instance v1, Ljava/io/File;

    const-string v2, "/shared_prefs/MigrationSharedPreference.xml"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "clearMigrationState() : Failed to delete the preference file, clear the preference contents - Ignoreable"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "clearMigrationState() : Failed to delete the checking file - CRITICAL!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static createUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : APP TYPE = Full"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "MigrationSharedPreference"

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isFitnessWithGearInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_b

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthServiceInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_9

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService1xInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_2

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : [ILLEGAL STATE] Installed Package = Fitness with Gear, S Heatlh 3.5, HealthSerivce 1.x !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->check3xDBExists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "[ILLEGAL STATE] [3XPRECEDENCE] 3X DB present, setting migration state to 3X"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "com.sec.android.app.shealth"

    const-string v1, "ERR_LOSS"

    const-string v2, "FAIL1:Combination: 2.5/FWG/HS1.X: migrate 3x data"

    invoke-static {p0, v0, v1, v2, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService2xInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_8

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_2_3 ==========="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check2xAnd3xDBFilesExist(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, " Both 2.X and 3.5.X DB are present, FWG was not successfully uninstalled, Not setting Migration Flag (preference to 3.5.X DB)"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, " Maintaining the same current state as the current state is used to decide another attempt to uninstall FWG"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.sec.android.app.shealth"

    const-string v1, "ERR_LOSS"

    const-string v2, "FAIL4:Combination: 2.5/FWG/HS2X/35XDB: FWG uninstall failure migrate 35x data"

    invoke-static {p0, v0, v1, v2, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->check3xDBExists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : [ILLEGAL STATE] Installed Package = Fitness with Gear, S Heatlh 3.5, HealthSerivce 1.x !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : [ILLEGAL STATE] [3XPRECEDENCE] 3XDB and FWG is present"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "[ILLEGAL STATE] [3XPRECEDENCE] 3X DB present, setting migration state to 3X"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_4
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_5
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const-string v0, "com.sec.android.app.shealth"

    const-string v1, "ERR_LOSS"

    const-string v2, "FAIL2:Combination: 2.5/FWG/HS2.X/3XDB: migrate 3x data"

    invoke-static {p0, v0, v1, v2, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_6
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, " 2.X DB will be present"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_6
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL1"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_7
    monitor-exit v1

    goto :goto_1

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0

    :cond_8
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : [ILLEGAL STATE] Installed Package = Fitness with Gear, S Heatlh 3.5, HealthSerivce (Not 0.x, 1.x, 2.x) !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_NONE ==========="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_NONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :cond_9
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_2_2 ==========="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string/jumbo v1, "migration from 2.5 FWG and health service is needed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_7
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL2"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_a
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    throw v0

    :cond_b
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthServiceInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_19

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService1xInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eq v0, v2, :cond_c

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService0xInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_c
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_4_1 =========== migrationState: 4"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check35DBFileExist(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_9
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_d
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    :goto_3
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_5
    move-exception v0

    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    throw v0

    :cond_e
    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_b
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_f
    monitor-exit v1

    goto :goto_3

    :catchall_6
    move-exception v0

    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    throw v0

    :cond_10
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService2xInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_18

    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check2xAnd3xDBFilesExist(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "2x db and platform.db present, giving importance to platform.db"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_APP_UPGRADE_DONE PLDB ==========="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_c
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_11
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_7

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_7
    move-exception v0

    :try_start_d
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    throw v0

    :cond_12
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->check3xDBExists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check35DBFileExist(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "[3XPRECEDENCE] [FOTACASE] 3X DB is found and 35X DB not present"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_e
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_13
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_8
    move-exception v0

    :try_start_f
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    throw v0

    :cond_14
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check2XDBFileExist(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_3_1 =========== migrationState: 3"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : For FOTA case,  J / H, and S Health 2.5 base models. INSTALLED PACKAGE =  [S Health, HealthService 2.x, SensorService]."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : This case can not go to UPGRADE_STATE_3_1 case natually because health service is updated forcely by FOTA."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : so in this case, call isFromSHealth25 function."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : isFromSHealth25 function looks for shared preference from s health 2.5, and search if gender field is in shared preference file or not."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : if gender filed is existed, we can judge this upgrade is from s health 2.5."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_10
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_15

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL5"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_15
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_9

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_3_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_9
    move-exception v0

    :try_start_11
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_9

    throw v0

    :cond_16
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_APP_UPGRADE_DONE ==========="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_12
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_17

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_17
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_a

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_a
    move-exception v0

    :try_start_13
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    throw v0

    :cond_18
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : [ILLEGAL STATE] Installed Package = Fitness with Gear, S Heatlh 3.5, HealthSerivce 1.x !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_19
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "checkState() : =========== UPGRADE_STATE_3_1 =========== migrationState: 3"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->check3xDBExists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "[3XPRECEDENCE] Surprise Surprise! 3X DB found!!! Setting migration to 3X."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_14
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_1a

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL4"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1a
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_b

    const-string v0, "com.sec.android.app.shealth"

    const-string v1, "ERR_LOSS"

    const-string v2, "FAIL3:Combination: 2.5/NO_HS/3XDB: migrate 3x data"

    invoke-static {p0, v0, v1, v2, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_b
    move-exception v0

    :try_start_15
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_b

    throw v0

    :cond_1b
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check35DBFileExist(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1d

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string/jumbo v1, "migration from 2.5 and health service is needed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_16
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_1c

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL6"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1c
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_c

    :goto_4
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_3_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    goto/16 :goto_0

    :catchall_c
    move-exception v0

    :try_start_17
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_c

    throw v0

    :cond_1d
    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_18
    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_1e

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL6"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1e
    monitor-exit v1

    goto :goto_4

    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_d

    throw v0
.end method

.method private static getIsUpgradeFromFitnessWithGear()Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const-string/jumbo v2, "true"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/FitnessWithGearUpgradeMarkFlag.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    :goto_0
    if-nez v2, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    if-eqz v0, :cond_1

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "MIGRATION: MIGRATION_FROM_FITNESS_WITH_GEAR enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v3

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_2

    :catch_2
    move-exception v2

    move-object v2, v0

    goto :goto_0

    :catch_3
    move-exception v3

    goto :goto_0
.end method

.method public static getMigrationState(Landroid/content/Context;)I
    .locals 9

    const/4 v2, 0x4

    const/4 v4, 0x0

    const/4 v1, 0x0

    const-string v0, "MigrationSharedPreference"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v3, "MigrationState"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    if-le v0, v2, :cond_1

    :cond_0
    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v3, "getMigrationState() : Invalid upgrade id value! return as default value(NOT_AVAILABLE)"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :cond_1
    :goto_0
    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getIsUpgradeFromFitnessWithGear()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService2xInstalled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check35DBFileExist(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v5, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

    const-string v6, "check_platform_db_exists"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_3

    const-string/jumbo v4, "value_of_check_if_platform_db_exists"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "MIGRATION : Direct upgrade from 3.x"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    :cond_3
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[MIGRATION] getMigrationState() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :catch_0
    move-exception v3

    const-string v5, "SHealthUpgrade(SHealthUpgradeUtil)"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MIGRATION_AUTHORITY_URI is not found (MigrationCPProvider) : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static getUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->currentStatus:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    return-object v0
.end method

.method public static isFitnessWithGearInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealthlite"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isFitnessWithGearInstalled() : Fitness with Gear is Installed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "isFitnessWithGearInstalled() : Fitness with Gear isn\'t Installed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFromSHealth25(Landroid/content/Context;)Z
    .locals 4

    const/4 v3, -0x1

    const/4 v0, 0x0

    const-string v1, "com.sec.android.app.shealth_preferences"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "com.sec.android.app.shealth_preferences is not existed !!! NOT Upgrade From S Heatlh 2.5 !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    const-string v2, "gender"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "NOT Upgrade From S Heatlh 2.5 !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isSensorServiceEnabled(Landroid/content/Context;)Z
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.service.health.sensor"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string v2, "SHealthUpgrade(SHealthUpgradeUtil)"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSensorServiceEnabled() : SensorService is Installed. SENSOR SERVICE ENABLED = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v4, v4, Landroid/content/pm/ApplicationInfo;->enabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v0, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isSensorServiceEnabled() : SensorService is NOT Installed !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isSensorServiceInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.service.health.sensor"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isSensorServiceInstalled() : SensorService is Installed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isSensorServiceInstalled() : SensorService is NOT Installed !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isShealth35Installed(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    const-string v2, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v3, "isShealth35Installed() : S HEALTH is INSTALLED."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "3.5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v1, "isShealth35Installed() : HEALTH VERSION = 3.5"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealth35Installed() : S HEALTH is NOT INSTALLED."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealth35Installed() : HEALTH VERSION is NOT 3.5 !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isShealthService0xInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.service.health"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    const-string v2, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v3, "isShealthService0xInstalled() : HEALTH SERVICE is INSTALLED."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "0."

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService0xInstalled() : HEALTH SERVICE VERSION = 0.x"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService0xInstalled() : HEALTH SERVICE VERSION is NOT 0.x !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService0xInstalled() : HealthService isn\'t Installed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isShealthService1xInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.service.health"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    const-string v2, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v3, "isShealthService1xInstalled() : HEALTH SERVICE is INSTALLED."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "1."

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService1xInstalled() : HEALTH SERVICE VERSION = 1.x"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService1xInstalled() : HEALTH SERVICE VERSION is NOT 1.x !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService1xInstalled() : HealthService isn\'t Installed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isShealthService2xInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.service.health"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    const-string v2, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v3, "isShealthService2xInstalled() : HEALTH SERVICE is INSTALLED."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "2."

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService2xInstalled() : HEALTH SERVICE VERSION = 2.x"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService2xInstalled() : HEALTH SERVICE VERSION is NOT 2.x"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthService2xInstalled() : HEALTH SERVICE 2.x is NOT installed !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isShealthServiceInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.service.health"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthServiceInstalled() : HealthService is Installed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isShealthServiceInstalled() : HealthService is NOT Installed !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isSleepMonitorInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.sleepmonitor"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isSleepMonitorInstalled() : Sleep Monitor is Installed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v1, "SHealthUpgrade(SHealthUpgradeUtil)"

    const-string v2, "isSleepMonitorInstalled() : Sleep Monitor is NOT Installed !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setMigrationStatus(Landroid/content/Context;I)V
    .locals 5

    const-string v0, "MigrationSharedPreference"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->EDITOR_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "MigrationState"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.app.shealth"

    const-string v2, "ERR_USTATUS"

    const-string v3, "FAIL8"

    const/4 v4, 0x0

    invoke-static {p0, v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
