.class Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewVersionAndModel(JLjava/util/EnumSet;III)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;III)V"
        }
    .end annotation

    const/16 v3, 0x7d8

    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewVersionAndModel - hardwareVersion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", softwareVersion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", modelNumber: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->manufacture:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->manufacture:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)I

    move-result v0

    invoke-static {v0, p6}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->getProductString(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$1400(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$1500(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$1600(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$1700(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyStateChanged(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$1800(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyStateChanged(I)V

    goto :goto_0
.end method
