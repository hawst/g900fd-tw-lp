.class public Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;
    }
.end annotation


# static fields
.field private static final _a:D = 6378245.0

.field private static final _ee:D = 0.006693421622965943


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GCJtoWGS(DD)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;-><init>(DD)V

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->GCJtoWGS(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    move-result-object v0

    return-object v0
.end method

.method public static GCJtoWGS(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;
    .locals 7

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->outOfChina(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->WGSToGCJ(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    mul-double/2addr v1, v5

    iget-wide v3, v0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    sub-double/2addr v1, v3

    iget-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    mul-double/2addr v3, v5

    iget-wide v5, v0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    sub-double/2addr v3, v5

    new-instance p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;-><init>(DD)V

    goto :goto_0
.end method

.method public static WGSToGCJ(DD)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;-><init>(DD)V

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->WGSToGCJ(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    move-result-object v0

    return-object v0
.end method

.method public static WGSToGCJ(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;
    .locals 12

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->outOfChina(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide v4, 0x3f7b6a8faf80ef0bL    # 0.006693421622965943

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iget-wide v4, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    const-wide v6, 0x405a400000000000L    # 105.0

    sub-double/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    const-wide v8, 0x4041800000000000L    # 35.0

    sub-double/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->tLat(DD)D

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    const-wide v8, 0x405a400000000000L    # 105.0

    sub-double/2addr v6, v8

    iget-wide v8, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    const-wide v10, 0x4041800000000000L    # 35.0

    sub-double/2addr v8, v10

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->tLon(DD)D

    move-result-wide v6

    const-wide v8, 0x4066800000000000L    # 180.0

    mul-double/2addr v4, v8

    const-wide v8, 0x41582b102de355c1L    # 6335552.717000426

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    mul-double/2addr v10, v2

    div-double/2addr v8, v10

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v8, v10

    div-double/2addr v4, v8

    const-wide v8, 0x4066800000000000L    # 180.0

    mul-double/2addr v6, v8

    const-wide v8, 0x415854c140000000L    # 6378245.0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double v2, v8, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    div-double v1, v6, v0

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    iget-wide v6, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    add-double v3, v6, v4

    iget-wide v5, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    add-double/2addr v1, v5

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;-><init>(DD)V

    move-object p0, v0

    goto/16 :goto_0
.end method

.method static outOfChina(Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;)Z
    .locals 5

    const/4 v0, 0x1

    invoke-static {}, Lcom/samsung/android/sdk/health/Shealth;->isChinaModel()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    const-wide v3, 0x4052004189374bc7L    # 72.004

    cmpg-double v1, v1, v3

    if-ltz v1, :cond_0

    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    const-wide v3, 0x40613ab5dcc63f14L    # 137.8347

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_0

    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    const-wide v3, 0x3fea89a027525461L    # 0.8293

    cmpg-double v1, v1, v3

    if-ltz v1, :cond_0

    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    const-wide v3, 0x404be9de69ad42c4L    # 55.8271

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static tLat(DD)D
    .locals 10

    const-wide/high16 v0, -0x3fa7000000000000L    # -100.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, p0

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    const-wide v2, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v2, p2

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    const-wide v2, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v2, p0

    mul-double/2addr v2, p2

    add-double/2addr v2, v0

    const-wide v4, 0x3fc999999999999aL    # 0.2

    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_0

    move-wide v0, p0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    mul-double/2addr v4, p0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, p0

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4044000000000000L    # 40.0

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double v6, p2, v6

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4064000000000000L    # 160.0

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    div-double v4, p2, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4074000000000000L    # 320.0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, p2

    const-wide/high16 v8, 0x403e000000000000L    # 30.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0

    :cond_0
    neg-double v0, p0

    goto :goto_0
.end method

.method private static tLon(DD)D
    .locals 10

    const-wide v0, 0x4072c00000000000L    # 300.0

    add-double/2addr v0, p0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    const-wide v2, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v2, p0

    mul-double/2addr v2, p0

    add-double/2addr v0, v2

    const-wide v2, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v2, p0

    mul-double/2addr v2, p2

    add-double/2addr v2, v0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_0

    move-wide v0, p0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    mul-double/2addr v4, p0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, p0

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, p0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4044000000000000L    # 40.0

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double v6, p0, v6

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x4062c00000000000L    # 150.0

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    div-double v4, p0, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide v4, 0x4072c00000000000L    # 300.0

    const-wide/high16 v6, 0x403e000000000000L    # 30.0

    div-double v6, p0, v6

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0

    :cond_0
    neg-double v0, p0

    goto/16 :goto_0
.end method
