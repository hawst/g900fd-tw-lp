.class final enum Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/database/validator/DataParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "NameSet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum CODE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum CONSTANT:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum CONSTANTS:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum DATA:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum DATA_TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum DESCRIPTION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum GREATER_THAN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum MANDATORY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum MAX:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum MIN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum NAME:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum PROPERTY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum SHEALTH:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum SUM:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

.field public static final enum VALIDATION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "SHEALTH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->SHEALTH:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "DATA"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DATA:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "DATA_TYPE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DATA_TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "CONSTANTS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CONSTANTS:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "CONSTANT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CONSTANT:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "PROPERTY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->PROPERTY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "VALIDATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->VALIDATION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "DESCRIPTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DESCRIPTION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "NAME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->NAME:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "TYPE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "MANDATORY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MANDATORY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "MIN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MIN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "MAX"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MAX:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "SUM"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->SUM:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "GREATER_THAN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->GREATER_THAN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const-string v1, "CODE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CODE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->SHEALTH:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DATA:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DATA_TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CONSTANTS:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CONSTANT:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->PROPERTY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->VALIDATION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DESCRIPTION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->NAME:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MANDATORY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MIN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MAX:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->SUM:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->GREATER_THAN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CODE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->$VALUES:[Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->$VALUES:[Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    return-object v0
.end method
