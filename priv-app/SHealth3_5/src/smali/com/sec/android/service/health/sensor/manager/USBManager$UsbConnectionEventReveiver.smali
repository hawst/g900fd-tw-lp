.class Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/USBManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbConnectionEventReveiver"
.end annotation


# instance fields
.field private isRegistered:Z

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/manager/USBManager;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->isRegistered:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/sec/android/service/health/sensor/manager/USBManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;-><init>(Lcom/sec/android/service/health/sensor/manager/USBManager;)V

    return-void
.end method


# virtual methods
.method public isRegistered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->isRegistered:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const-string v0, "[HealthSensor]USBManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onReceive="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # invokes: Lcom/sec/android/service/health/sensor/manager/USBManager;->usbDisconnected()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$100(Lcom/sec/android/service/health/sensor/manager/USBManager;)V

    :cond_0
    return-void
.end method

.method public setRegistered(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->isRegistered:Z

    return-void
.end method
