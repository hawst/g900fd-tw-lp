.class final Lcom/sec/android/service/health/DataRequestAdapter$1;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/DataRequestAdapter;->registerDataObserver(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/health/IHealthDataObserver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$dataType:Ljava/lang/String;

.field final synthetic val$observer:Lcom/samsung/android/health/IHealthDataObserver;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/samsung/android/health/IHealthDataObserver;Ljava/lang/String;)V
    .locals 0

    iput-object p2, p0, Lcom/sec/android/service/health/DataRequestAdapter$1;->val$observer:Lcom/samsung/android/health/IHealthDataObserver;

    iput-object p3, p0, Lcom/sec/android/service/health/DataRequestAdapter$1;->val$dataType:Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/DataRequestAdapter$1;->onChange(ZLandroid/net/Uri;)V

    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/DataRequestAdapter$1;->val$observer:Lcom/samsung/android/health/IHealthDataObserver;

    iget-object v1, p0, Lcom/sec/android/service/health/DataRequestAdapter$1;->val$dataType:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/samsung/android/health/IHealthDataObserver;->onChange(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Health.DataRequestAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No callback to invoke : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
