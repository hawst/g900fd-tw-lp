.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

.field private mUnit:I


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mUnit:I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method


# virtual methods
.method public doAction()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_UNIT_SETTING:[B

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method protected getUnit()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mUnit:I

    return v0
.end method

.method public responseReceived([B)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x3

    aget-byte v0, p1, v0

    const/4 v1, 0x2

    new-array v1, v1, [B

    array-length v2, v1

    invoke-static {p1, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_UNIT_SETTING:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "[HealthSensor]JNJ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[responseReceived] unit="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    const v0, 0x222e1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mUnit:I

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendAck()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readRecordCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const v0, 0x222e2

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mUnit:I

    goto :goto_1

    :cond_2
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->mUnit:I

    goto :goto_1
.end method
