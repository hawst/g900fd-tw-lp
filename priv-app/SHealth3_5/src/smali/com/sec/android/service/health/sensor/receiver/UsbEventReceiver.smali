.class public Lcom/sec/android/service/health/sensor/receiver/UsbEventReceiver;
.super Landroid/app/Activity;


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor][HEALTH]"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private findInterface(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbInterface;
    .locals 5

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v3

    const/16 v4, 0xff

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getInterfaceSubclass()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "[HealthSensor][HEALTH]"

    const-string v1, "[USBHiddenActivity] onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "[HealthSensor][HEALTH]"

    const-string v1, "[USBHiddenActivity] onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "usb"

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/receiver/UsbEventReceiver;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/sensor/manager/USBManager;

    if-nez v1, :cond_0

    const-string v0, "[HealthSensor][HEALTH]"

    const-string v1, "[USBHiddenActivity] mUsbManager is null, return"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/receiver/UsbEventReceiver;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbDevice;

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/receiver/UsbEventReceiver;->findInterface(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbInterface;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbConnected(Landroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;)V

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/receiver/UsbEventReceiver;->finish()V

    goto :goto_0
.end method
