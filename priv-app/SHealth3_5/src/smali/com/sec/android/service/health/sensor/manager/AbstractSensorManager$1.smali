.class Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager$1;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->access$000()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->executeCommand(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "mCommandExecutor interrupted"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
