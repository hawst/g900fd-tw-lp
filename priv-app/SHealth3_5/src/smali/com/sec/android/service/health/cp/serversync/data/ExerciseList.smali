.class public Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;
.super Ljava/lang/Object;


# instance fields
.field public accessaryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/AccessaryItem;",
            ">;"
        }
    .end annotation
.end field

.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public distanceUnit:Ljava/lang/String;

.field public durationMilli:Ljava/lang/Long;

.field public endTime:Ljava/lang/String;

.field public exerciseId:J

.field public exerciseList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/ActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field public exerciseName:Ljava/lang/String;

.field public exerciseTypeCode:Ljava/lang/String;

.field public fatBurnTime:Ljava/lang/Long;

.field public firstBeatCoachingData:Lcom/sec/android/service/health/cp/serversync/data/FirstBeatDataItem;

.field public hdid:Ljava/lang/String;

.field public heartRate:Ljava/lang/Double;

.field public inputType:Ljava/lang/String;

.field public level:Ljava/lang/Integer;

.field public locationDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/LocationItem;",
            ">;"
        }
    .end annotation
.end field

.field public missionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/ExerciseMissionItem;",
            ">;"
        }
    .end annotation
.end field

.field public photoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/ExerciseImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field public realTimeDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/RealTimeItem;",
            ">;"
        }
    .end annotation
.end field

.field public requestType:Ljava/lang/String;

.field public responseCode:Ljava/lang/String;

.field public startTime:Ljava/lang/String;

.field public totalCadence:Ljava/lang/Double;

.field public totalCalorie:Ljava/lang/Double;

.field public totalDistance:Ljava/lang/Double;

.field public totalDuration:Ljava/lang/Long;

.field public valid:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;->exerciseList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;->realTimeDataList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;->locationDataList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/data/FirstBeatDataItem;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/serversync/data/FirstBeatDataItem;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;->firstBeatCoachingData:Lcom/sec/android/service/health/cp/serversync/data/FirstBeatDataItem;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;->photoList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;->accessaryList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ExerciseList;->missionList:Ljava/util/ArrayList;

    return-void
.end method
