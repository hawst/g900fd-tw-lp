.class public abstract Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;


# static fields
.field protected static final LOG_FEATURE_ANT_WEIGHT_SCALE:Ljava/lang/String; = "SC01"

.field protected static final LOG_FEATURE_SAP_HRM:Ljava/lang/String; = "SC03"

.field protected static final LOG_FEATURE_SAP_WEIGHT_SCALE:Ljava/lang/String; = "SC02"

.field protected static final STATE_JOINED:I = 0x2

.field protected static final STATE_JOINING:I = 0x1

.field protected static final STATE_LEAVING:I = 0x3

.field protected static final TAG:Ljava/lang/String; = "[HealthSensor][HealthSensor]AbstractSensorManager"

.field private static mCommandQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;",
            ">;"
        }
    .end annotation
.end field

.field private static mResponseQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ACTION_DEVICE_CONNECTED:Ljava/lang/String;

.field private DEVICE_ID:Ljava/lang/String;

.field private DEVICE_TYPE:Ljava/lang/String;

.field private deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCommandExecutor:Ljava/lang/Thread;

.field protected mContext:Landroid/content/Context;

.field private mScanningStoppedListner:Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;

.field protected mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

.field private profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mScanningStoppedListner:Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;

    const-string v0, "ACTION_DEVICE_CONNECTED"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;

    const-string v0, "DEVICE_TYPE"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->DEVICE_TYPE:Ljava/lang/String;

    const-string v0, "DEVICE_ID"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->DEVICE_ID:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager$1;-><init>(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandExecutor:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$000()Ljava/util/concurrent/BlockingQueue;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method private addSensorEventListener(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V
    .locals 3

    if-nez p3, :cond_0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "addSensorEventListener listener is null, return without register listener"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "addSensorEventListener - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addSensorEventListener deviceId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-nez v0, :cond_2

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addSensorEventListener RemotecallBackListAdded created with device : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v0, p1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getSensorEventListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregister(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v0, p3, p2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private clearObsoleteListeners(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearObsoleteListeners() devObjId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "devId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearObsoleteListeners() devObjId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getListener(Ljava/lang/Integer;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    :cond_3
    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregister(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private createRemoteCallBack(Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;Ljava/lang/Integer;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method private dispatchRemoteCallBackDied(Ljava/lang/Integer;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Remote callback has died, devId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->remoteCallBackDied(Ljava/lang/Integer;)V

    return-void
.end method

.method private getAppNameByPID(I)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v2, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v2, p1, :cond_0

    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAppNameByPID() - processName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "getAppNameByPID() - null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSensorEventListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getRemoteEventCallBackList(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v3

    if-nez v3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    sget-object v4, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    :try_start_0
    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v0

    if-ge v2, v0, :cond_3

    invoke-virtual {v3, v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_1

    invoke-virtual {v3, v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    :goto_2
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method private logConnectedState(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 2

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v0

    const/16 v1, 0x2712

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    const-string v0, "com.sec.android.service.health"

    const-string v1, "SC01"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "com.sec.android.service.health"

    const-string v1, "SC02"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v0

    const/16 v1, 0x2718

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const-string v0, "com.sec.android.service.health"

    const-string v1, "SC03"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeDeviceState(Ljava/lang/String;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removeDeviceState deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private removeSensorEventListener(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "removeSensorEventListener :  remoteCallBackList instance is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v4, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v4

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v1

    if-ge v3, v1, :cond_3

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v1, v5, :cond_2

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    :goto_2
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregister(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method

.method private setState(Ljava/lang/String;I)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setState deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceStatePresent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected addProfileHandlerController(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addProfileHandlerController deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected addSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method protected final addToCommandQueue(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandExecutor:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandExecutor:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected cleanDeviceHandler()V
    .locals 2

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "cleanDeviceHandler is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :cond_3
    return-void
.end method

.method protected cleanupOnDied(Ljava/lang/Integer;)V
    .locals 0

    return-void
.end method

.method protected createDeviceHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)V
    .locals 2

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getProfileHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-direct {v1, p1, v0, p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->addProfileHandlerController(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->initiallize(Landroid/content/Context;Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "createDeviceHandler: profileHandlerInterfaced is null !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->initiallize(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public abstract create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end method

.method public deinitialize()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->cleanDeviceHandler()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :cond_0
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandExecutor:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method

.method protected deviceAppDied(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->killContentUiActivity(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)V

    :cond_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, p1, v0, v2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchLeave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "deviceAppDied: device is null !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_2

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "deviceAppDied: process id is null !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "deviceAppDied: calling dispatchLeave"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, p1, v0, v2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchLeave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V

    goto :goto_0
.end method

.method public final dispatchJoin(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;ILcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "dispatchJoin - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceStatePresent(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->setState(Ljava/lang/String;I)V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->addSensorEventListener(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceState(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->addSensorEventListener(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceState(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->addSensorEventListener(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, p3, v3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->createRemoteCallBack(Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;Ljava/lang/Integer;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onJoined(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceState(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/16 v1, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, p3, v3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->createRemoteCallBack(Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;Ljava/lang/Integer;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onJoined(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_0
.end method

.method public final dispatchLeave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V
    .locals 5

    const/4 v3, 0x1

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "dispatchLeave - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceStatePresent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceState(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceState(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "dispatchLeave return without doing anything due to invalid state"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v2, "There is no controller leaving device !!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_4
    if-eqz v0, :cond_5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->isContollingDevice(Ljava/lang/Integer;)Z

    move-result v1

    if-nez v1, :cond_6

    if-eqz p2, :cond_6

    :cond_5
    if-eqz p3, :cond_8

    :cond_6
    if-eqz v0, :cond_7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData(Ljava/lang/Integer;)I

    :cond_7
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_0

    :cond_8
    if-eqz v0, :cond_9

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopListeningData(Ljava/lang/Integer;)I

    :cond_9
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getRemoteEventCallBackList(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    if-nez v0, :cond_a

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "remoteCallBackList is null, don\'t need to call removeEventListener. Return."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v0

    if-eq v0, v3, :cond_b

    if-eqz p3, :cond_c

    :cond_b
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto/16 :goto_0

    :cond_c
    if-le v0, v3, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getSensorEventListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->removeSensorEventListener(Ljava/lang/String;Ljava/lang/Integer;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->createRemoteCallBack(Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;Ljava/lang/Integer;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0, p1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onLeft(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto/16 :goto_0
.end method

.method public final dispatchRequest(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "dispatchRequest - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x7

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    if-eqz v0, :cond_1

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CLEAR_OBSOLETE_CALLBACKS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->clearObsoleteListeners(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->removeDataListener(Ljava/lang/Integer;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    if-eqz p4, :cond_2

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x9

    goto :goto_0
.end method

.method public final declared-synchronized dispatchStartScanning(Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mScanningStoppedListner:Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;

    invoke-virtual {p0, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->startScanning(Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized dispatchStopScanning()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->stopScanning()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public executeCommand(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not implemented : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public abstract getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    return-object v0
.end method

.method protected getRemoteEventCallBackList(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    return-object v0
.end method

.method public getSHealthSensorDevices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method protected getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 0

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method insertLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "app_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "feature"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.samsung.android.providers.context"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public abstract isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
.end method

.method public isDataTypeSupported(I)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected isDeviceState(Ljava/lang/String;I)Z
    .locals 5

    const/4 v1, 0x0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDeviceState deviceId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceStatePresent(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v2, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isDeviceState stateInteger.intValue() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected isDeviceStatePresent(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceStateHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isPluginApp(I)Z
    .locals 4

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getAppNameByPID(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.service.health"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "access from healthservice"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPluginApp isPlugin : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method

.method protected abstract join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
.end method

.method protected killContentUiActivity(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "killing is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.service.health.sensor.force.close"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "device"

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "bindingPid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void
.end method

.method protected abstract leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    move/from16 v7, p8

    move/from16 v8, p9

    move-object/from16 v9, p10

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "[",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    move/from16 v7, p8

    move/from16 v8, p9

    move-object/from16 v9, p10

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onDataRecordStarted(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZ)V
    .locals 8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    move v1, p1

    move-object v2, p2

    move-wide v3, p3

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataRecordStarted(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZ)V

    :cond_0
    return-void
.end method

.method public onDataStarted(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide/from16 v5, p5

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-interface/range {v0 .. v10}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataStarted(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onDataStopped(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide/from16 v5, p5

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-interface/range {v0 .. v10}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataStopped(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V
    .locals 3

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "onDeviceFound - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceFound : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->addSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    if-eqz p2, :cond_2

    invoke-virtual {p2, p1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/util/RenameDataBaseHelper;->isDevicePresent(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/util/RenameDataBaseHelper;->getName(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setName(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-interface {v0, p1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_0
.end method

.method public final onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V

    return-void
.end method

.method public final onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V
    .locals 3

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "onDeviceJoined - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceJoined errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sendConnectedBroadcast : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->setState(Ljava/lang/String;I)V

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->logConnectedState(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p2, v2, v0, p1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onJoined(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_2
    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->DEVICE_ID:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->DEVICE_TYPE:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->removeDeviceState(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceLeft errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "onDeviceLeft - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->removeDeviceState(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->removerRemoteEventCallBackList(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p2, v2, v0, p1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onLeft(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_0
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V
    .locals 6

    if-nez p3, :cond_1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "onResponseReceived : Device is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getRemoteEventCallBackList(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v3

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Ljava/lang/Integer;I)V

    goto :goto_0
.end method

.method public onScanningStopped()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mScanningStoppedListner:Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mScanningStoppedListner:Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;->scanningStopped()V

    :cond_0
    return-void
.end method

.method public declared-synchronized onStateChanged(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-interface {v2, p1, v0}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onStateChanged(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized onStateChanged(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 2

    monitor-enter p0

    if-nez p2, :cond_1

    :try_start_0
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "onStateChanged - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-interface {v1, p1, v0}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onStateChanged(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final pollCurrentResponse(JLjava/util/concurrent/TimeUnit;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    return-object v0
.end method

.method public record(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;J)I
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "record callingPid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Exercise Id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->record(Ljava/lang/Integer;J)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected abstract remoteCallBackDied(Ljava/lang/Integer;)V
.end method

.method protected final removeFromCommandQueue(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected removeProfileHandlerController(Ljava/lang/String;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removeProfileHandlerController deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected removeSensorDevice(Ljava/lang/String;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removeSensorDevice deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->removeDeviceState(Ljava/lang/String;)V

    return-void
.end method

.method protected removerRemoteEventCallBackList(Ljava/lang/String;)V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removerRemoteEventCallBackList deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->eventListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public renameDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getSensorEventListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isDeviceState(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/service/health/sensor/manager/util/RenameDataBaseHelper;->isDevicePresent(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v4

    invoke-static {v2, v3, v4, p3}, Lcom/sec/android/service/health/sensor/manager/util/RenameDataBaseHelper;->updateName(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, p3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setName(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v4

    invoke-static {v2, v3, v4, p3}, Lcom/sec/android/service/health/sensor/manager/util/RenameDataBaseHelper;->insert(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, p3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setName(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v2, "RenameDevice Failure because device is not yet joined"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method protected abstract request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
.end method

.method protected final setCurrentResponse(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)Z
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setSensorListner(Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    return-void
.end method

.method public startListeningData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startListeningData callingPid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->startListeningData(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
.end method

.method protected abstract startScanning(Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
.end method

.method public stopListeningData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/Integer;)I
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stopListeningData callingPid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopListeningData(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)I
.end method

.method protected abstract stopScanning()Z
.end method

.method protected final takeCurrentResponse()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    return-object v0
.end method
