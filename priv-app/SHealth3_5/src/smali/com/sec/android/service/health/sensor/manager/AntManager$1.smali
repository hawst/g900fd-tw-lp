.class Lcom/sec/android/service/health/sensor/manager/AntManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/manager/AntManager;->startSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/manager/AntManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewListReceived(Lcom/dsi/ant/backgroundscan/antplus/DeviceType;[I)V
    .locals 9

    const/4 v6, -0x1

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "onNewListReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->getIntValue()I

    move-result v2

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    array-length v0, p2

    if-ge v8, v0, :cond_0

    aget v3, p2, v8

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IAlreadyConnectedDeviceListReceiver "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->getUID(II)Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    const/4 v5, 0x1

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/manager/AntManager;->notifyDeviceSearchUpdated(Ljava/lang/String;IILjava/lang/String;ZII)V

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_0
    return-void
.end method
