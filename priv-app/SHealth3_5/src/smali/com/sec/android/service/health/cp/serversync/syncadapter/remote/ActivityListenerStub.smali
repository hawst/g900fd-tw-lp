.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;
.super Ljava/lang/Object;


# static fields
.field public static final SYNC_DETAIL_TYPE_ONGOING:I = -0x65


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mDataType:I

.field public mFlagStopTask:Z

.field private mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mFlagStopTask:Z

    iput-object p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    iput p3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mDataType:I

    return-void
.end method


# virtual methods
.method public getListenerObject()Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    return-object v0
.end method

.method public onErrorDuringExerciseSync()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onErrorDuringExerciseSync(): Error occurred during exercise sync"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    invoke-interface {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;->onErrorDuringExerciseSync()V

    :cond_0
    return-void
.end method

.method public onFinished(II)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SyncListenerStub onFinished requestType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mDataType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    iget v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mDataType:I

    invoke-interface {v0, v1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;->onFinished(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mFlagStopTask:Z

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    goto :goto_0
.end method
