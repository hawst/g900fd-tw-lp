.class public Lcom/sec/android/service/health/cp/serversync/data/ECGList;
.super Ljava/lang/Object;


# instance fields
.field public accId:Ljava/lang/String;

.field public analysisList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/ECGAnalysisItem;",
            ">;"
        }
    .end annotation
.end field

.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceBioPKId:J

.field public deviceCreateDate:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceFilePath:Ljava/lang/String;

.field public deviceModifyDate:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public maxHeartRate:Ljava/lang/Integer;

.field public meanHeartRate:D

.field public minHeartRate:Ljava/lang/Integer;

.field public requestType:Ljava/lang/String;

.field public sampleCount:I

.field public sampleEndTime:Ljava/lang/String;

.field public sampleStartTime:Ljava/lang/String;

.field public sampleTime:Ljava/lang/String;

.field public samplingFrequency:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/ECGList;->analysisList:Ljava/util/ArrayList;

    return-void
.end method
