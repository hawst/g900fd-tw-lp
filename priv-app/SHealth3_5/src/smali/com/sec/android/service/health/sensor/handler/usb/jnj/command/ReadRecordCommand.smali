.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# static fields
.field private static dataCnt:I

.field private static dateFormat:Ljava/lang/String;

.field private static totalNum:I

.field private static unit:I


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dataCnt:I

    const-string v0, ""

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dateFormat:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->totalNum:I

    const v0, 0x7fffffff

    sput v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->unit:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private createGetDataResponse(Landroid/text/format/Time;F)V
    .locals 3

    const-string v0, "[HealthSensor]JNJ"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ReadRecordCommand] createGetDataResponse="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->time:J

    iput p2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucose:F

    sget v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->unit:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method

.method private getData([B)F
    .locals 4

    const-string v1, ""

    const-string v0, ""

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    move-object v2, v1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    aget-byte v0, p1, v1

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    aget-byte v0, p1, v1

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/16 v0, 0x10

    invoke-static {v2, v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method private getDate([B)Landroid/text/format/Time;
    .locals 5

    const-string v1, ""

    const/4 v0, 0x3

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    if-ltz v1, :cond_1

    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v0, ""

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy:MM:dd:HH:mm:ss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dateFormat:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->setDate(Ljava/lang/String;)Landroid/text/format/Time;

    move-result-object v0

    return-object v0
.end method

.method private parseRecordCommand([B)V
    .locals 5

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v4, 0x0

    new-array v0, v2, [B

    array-length v1, v0

    invoke-static {p1, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_GLUCOSE_RECORD:[B

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-array v0, v3, [B

    array-length v1, v0

    invoke-static {p1, v2, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-array v1, v3, [B

    const/4 v2, 0x6

    array-length v3, v1

    invoke-static {p1, v2, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->getDate([B)Landroid/text/format/Time;

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->getData([B)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->createGetDataResponse(Landroid/text/format/Time;F)V

    goto :goto_0
.end method

.method private setDate(Ljava/lang/String;)Landroid/text/format/Time;
    .locals 4

    const/4 v0, 0x0

    const-string v1, "[HealthSensor]JNJ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setDate() date: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    :cond_0
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Landroid/text/format/Time;->year:I

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/text/format/Time;->month:I

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Landroid/text/format/Time;->monthDay:I

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Landroid/text/format/Time;->hour:I

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Landroid/text/format/Time;->minute:I

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Landroid/text/format/Time;->second:I

    return-object v0
.end method


# virtual methods
.method public doAction()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readDateFormatCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;->getDateFormat()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dateFormat:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readTotalNumberCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->getTotalNumber()I

    move-result v0

    sput v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->totalNum:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readUnitCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;->getUnit()I

    move-result v0

    sput v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->unit:I

    const/4 v0, 0x4

    new-array v0, v0, [B

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_GLUCOSE_RECORD:[B

    const/4 v1, 0x2

    sget v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dataCnt:I

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const-string v1, "[HealthSensor]JNJ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[ReadRecordCommand] gluRecCmd[3]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x3

    aget-byte v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method public responseReceived([B)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->parseRecordCommand([B)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendAck()V

    sget v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dataCnt:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dataCnt:I

    sget v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dataCnt:I

    sget v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->totalNum:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;->dataCnt:I

    goto :goto_0
.end method
