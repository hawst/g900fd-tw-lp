.class public Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$3;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]HRMAntProfileHandler"


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private manufacture:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->manufacture:I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->manufacture:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->manufacture:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method


# virtual methods
.method public getAntDeviceType()I
    .locals 1

    const/16 v0, 0x78

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 1

    const/16 v0, 0x7d2

    return v0
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected requestAccess()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mAntDeviceID:I

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method
