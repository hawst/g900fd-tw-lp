.class public abstract Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
.implements Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;


# static fields
.field protected static final TAG:Ljava/lang/String; = "[HealthSensor]BTManager"


# instance fields
.field private ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method


# virtual methods
.method public abstract deinitialize()V
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-object v0
.end method

.method public abstract initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
.end method

.method public onFinishSession()V
    .locals 3

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTProfileHandler onFinishSession"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    return-void
.end method

.method public onHealthServiceError(ILjava/lang/String;)V
    .locals 3

    const-string v0, "[HealthSensor]BTManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthServiceError code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " desc: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setEvent(I)V

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setResponse(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    return-void
.end method

.method public onMeasuredDataReceived(Lcom/sec/android/service/health/sensor/data/MeasurementData;)V
    .locals 9

    const/4 v8, -0x1

    const/4 v0, 0x0

    const/4 v7, 0x0

    const-string v1, "[HealthSensor]BTManager"

    const-string/jumbo v2, "onMeasuredDataReceived"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getDataSet()[Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;

    move-result-object v1

    const-string v2, "[HealthSensor]BTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onMeasuredDataReceived type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getDeviceType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getDeviceType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    const-string v0, "[HealthSensor]BTManager"

    const-string/jumbo v1, "onMeasuredDataReceived wrong device type"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x4a05

    if-ne v5, v6, :cond_1

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x4a06

    if-ne v5, v6, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    goto :goto_2

    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x482a

    if-ne v5, v6, :cond_0

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v2, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->time:J

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v1, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto :goto_0

    :sswitch_3
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    array-length v3, v1

    :goto_3
    if-ge v0, v3, :cond_10

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe140

    if-ne v5, v6, :cond_4

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v5

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string v6, "kg"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const v5, 0x1fbd1

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    :cond_4
    :goto_4
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe144

    if-ne v5, v6, :cond_a

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v5

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->height:F

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string v6, "cm"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    const v4, 0x249f1

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->heightUnit:I

    :cond_5
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string v6, "lb"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    const v5, 0x1fbd2

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    goto :goto_4

    :cond_7
    iput v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    goto :goto_4

    :cond_8
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ft"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    const v4, 0x249f2

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->heightUnit:I

    goto :goto_5

    :cond_9
    iput v8, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->heightUnit:I

    goto :goto_5

    :cond_a
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe150

    if-ne v5, v6, :cond_b

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMassIndex:F

    goto :goto_5

    :cond_b
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xe14c

    if-ne v5, v6, :cond_c

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyFat:F

    goto :goto_5

    :cond_c
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf001

    if-ne v5, v6, :cond_d

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMetabolicRate:F

    goto :goto_5

    :cond_d
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf002

    if-ne v5, v6, :cond_e

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->visceralFat:F

    goto :goto_5

    :cond_e
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf003

    if-ne v5, v6, :cond_f

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyAge:I

    goto/16 :goto_5

    :cond_f
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const v6, 0xf00a

    if-ne v5, v6, :cond_5

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v4

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->skeletalMuscle:F

    goto/16 :goto_5

    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v2, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_4
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/data/MeasurementData;->getTimeStamp()Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->time:J

    array-length v3, v1

    :goto_6
    if-ge v0, v3, :cond_14

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x7114

    if-eq v5, v6, :cond_11

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getType()I

    move-result v5

    const/16 v6, 0x71b8

    if-ne v5, v6, :cond_12

    :cond_11
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getValue()F

    move-result v5

    iput v5, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucose:F

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "mg/dL"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    const v4, 0x222e1

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    :cond_12
    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_13
    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "mmol/L"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    const v4, 0x222e2

    iput v4, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    goto :goto_7

    :cond_14
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v2, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1004 -> :sswitch_0
        0x1007 -> :sswitch_1
        0x1008 -> :sswitch_2
        0x100f -> :sswitch_3
        0x1011 -> :sswitch_4
        0x1029 -> :sswitch_5
        0x102a -> :sswitch_7
        0x1047 -> :sswitch_8
        0x1048 -> :sswitch_6
    .end sparse-switch
.end method

.method public onSensorInfoReceived(Lcom/sec/android/service/health/sensor/data/SensorInfo;)V
    .locals 3

    const-string v0, "[HealthSensor]BTManager"

    const-string/jumbo v1, "onSensorInfoReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setResponse(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    return-void
.end method

.method public onStartSession()V
    .locals 3

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTProfileHandler onStartSession"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    return-void
.end method

.method public sendDataToSensor([B)V
    .locals 0

    return-void
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method

.method public setSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;->ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-void
.end method
