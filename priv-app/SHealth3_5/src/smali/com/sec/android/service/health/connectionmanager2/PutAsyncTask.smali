.class Lcom/sec/android/service/health/connectionmanager2/PutAsyncTask;
.super Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
.source "PutAsyncTask.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 0
    .param p1, "connectionManager"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "requestparameters"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected varargs doInBackground2([Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .locals 14
    .param p1, "params"    # [Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 42
    const/4 v3, 0x0

    .line 43
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    const/4 v6, 0x0

    .line 44
    .local v6, "resCode":I
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 46
    .local v7, "response":Ljava/lang/StringBuffer;
    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-virtual {v11}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getUrl()Ljava/lang/String;

    move-result-object v10

    .line 49
    .local v10, "strURL":Ljava/lang/String;
    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-virtual {v11}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getHttpEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    .line 52
    .local v5, "requestEntity":Lorg/apache/http/HttpEntity;
    iget-object v11, p0, Lcom/sec/android/service/health/connectionmanager2/PutAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    if-nez v11, :cond_0

    .line 54
    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v11, -0x1

    const/4 v12, -0x7

    invoke-direct {v8, v11, v12}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 55
    .local v8, "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-virtual {v11, v8}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 56
    const/4 v11, 0x0

    aget-object v11, p1, v11

    .line 122
    .end local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :goto_0
    return-object v11

    .line 67
    :cond_0
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v4, v10}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 80
    .local v4, "httpput":Lorg/apache/http/client/methods/HttpPut;
    if-eqz v5, :cond_1

    .line 82
    invoke-virtual {v4, v5}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 84
    :cond_1
    iget-object v11, p0, Lcom/sec/android/service/health/connectionmanager2/PutAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v11}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 86
    iget-object v11, p0, Lcom/sec/android/service/health/connectionmanager2/PutAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v11}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/PutAsyncTask;->getHttpContext()Lorg/apache/http/protocol/BasicHttpContext;

    move-result-object v12

    invoke-interface {v11, v4, v12}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 88
    :cond_2
    if-nez v3, :cond_3

    .line 90
    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v11, -0x1

    const/4 v12, -0x7

    invoke-direct {v8, v11, v12}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 91
    .restart local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-virtual {v11, v8}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 92
    const/4 v11, 0x0

    aget-object v11, p1, v11

    goto :goto_0

    .line 94
    .end local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_3
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v11

    const/16 v12, 0xc8

    if-eq v11, v12, :cond_4

    .line 97
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v11

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v8

    .line 98
    .restart local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-virtual {v11, v8}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 99
    const/4 v11, 0x0

    aget-object v11, p1, v11

    goto :goto_0

    .line 101
    .end local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_4
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 102
    .local v2, "httpEntity":Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_6

    .line 107
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v12

    const-string/jumbo v13, "utf-8"

    invoke-direct {v11, v12, v13}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 109
    .local v0, "bufferedReader":Ljava/io/BufferedReader;
    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .local v9, "strLine":Ljava/lang/String;
    if-eqz v9, :cond_5

    .line 111
    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 117
    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v2    # "httpEntity":Lorg/apache/http/HttpEntity;
    .end local v4    # "httpput":Lorg/apache/http/client/methods/HttpPut;
    .end local v9    # "strLine":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 119
    .local v1, "ex":Ljava/lang/Exception;
    invoke-static {v6, v1}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v8

    .line 121
    .end local v1    # "ex":Ljava/lang/Exception;
    :goto_2
    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-virtual {v11, v8}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 122
    const/4 v11, 0x0

    aget-object v11, p1, v11

    goto/16 :goto_0

    .line 113
    .restart local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v2    # "httpEntity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "httpput":Lorg/apache/http/client/methods/HttpPut;
    .restart local v9    # "strLine":Ljava/lang/String;
    :cond_5
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 115
    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v9    # "strLine":Ljava/lang/String;
    :cond_6
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    .local v8, "result":Ljava/lang/String;
    goto :goto_2
.end method
