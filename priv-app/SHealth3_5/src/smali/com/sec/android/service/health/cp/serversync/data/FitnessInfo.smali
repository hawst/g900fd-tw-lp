.class public Lcom/sec/android/service/health/cp/serversync/data/FitnessInfo;
.super Ljava/lang/Object;


# instance fields
.field public accessaryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/AccessaryItem;",
            ">;"
        }
    .end annotation
.end field

.field public appId:Ljava/lang/String;

.field public calorie:D

.field public comment:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public duration:I

.field public durationMilli:Ljava/lang/Long;

.field public endTime:Ljava/lang/String;

.field public exerciseId:J

.field public exerciseName:Ljava/lang/String;

.field public fatBurnTime:Ljava/lang/Long;

.field public fitnessList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/FitnessItem;",
            ">;"
        }
    .end annotation
.end field

.field public hdid:Ljava/lang/String;

.field public heartRate:Ljava/lang/Double;

.field public inputType:Ljava/lang/String;

.field public level:Ljava/lang/Integer;

.field public locationDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/LocationItem;",
            ">;"
        }
    .end annotation
.end field

.field public realTimeDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/RealTimeItem;",
            ">;"
        }
    .end annotation
.end field

.field public requestType:Ljava/lang/String;

.field public responseCode:Ljava/lang/String;

.field public startTime:Ljava/lang/String;

.field public totalDistance:Ljava/lang/Double;

.field public totalRepetitionCount:I

.field public valid:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FitnessInfo;->fitnessList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FitnessInfo;->realTimeDataList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FitnessInfo;->locationDataList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FitnessInfo;->accessaryList:Ljava/util/ArrayList;

    return-void
.end method
