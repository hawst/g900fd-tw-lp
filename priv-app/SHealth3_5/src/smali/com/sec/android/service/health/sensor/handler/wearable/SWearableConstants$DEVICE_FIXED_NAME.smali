.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$DEVICE_FIXED_NAME;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DEVICE_FIXED_NAME"
.end annotation


# static fields
.field public static final ACTIVITY_TRACKER_OLD:Ljava/lang/String; = "Samsung EI-AN900A"

.field public static final GEAR1:Ljava/lang/String; = "GALAXY Gear"

.field public static final GEAR2:Ljava/lang/String; = "Gear 2"

.field public static final GEAR2NEO:Ljava/lang/String; = "Gear 2 Neo"

.field public static final GEAR3:Ljava/lang/String; = "Gear 3"

.field public static final GEARO:Ljava/lang/String; = "Gear O"

.field public static final GEARS:Ljava/lang/String; = "Gear S"

.field public static final GEAR_FIT:Ljava/lang/String; = "Gear Fit"

.field public static final GEAR_FIT_OLD:Ljava/lang/String; = "Wingtip"

.field public static final GEAR_OLD:Ljava/lang/String; = "Gear1"

.field public static final TIZEN_GEAR1:Ljava/lang/String; = "Gear"

.field public static final WINGTIP:Ljava/lang/String; = "WINGTIP"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
