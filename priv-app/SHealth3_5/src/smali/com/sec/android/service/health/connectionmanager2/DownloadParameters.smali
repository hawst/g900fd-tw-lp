.class public Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;
.super Ljava/lang/Object;
.source "DownloadParameters.java"


# instance fields
.field private headerNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private headerValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field listener:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

.field outputStream:Ljava/io/OutputStream;

.field privateId:I

.field requestId:J

.field private response:Ljava/lang/Object;

.field tag:Ljava/lang/Object;

.field url:Ljava/lang/String;


# direct methods
.method public constructor <init>(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "outputStream"    # Ljava/io/OutputStream;
    .param p7, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;",
            "Ljava/lang/String;",
            "Ljava/io/OutputStream;",
            "Ljava/lang/Object;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p8, "headerNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->listener:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    .line 20
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->url:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->outputStream:Ljava/io/OutputStream;

    .line 22
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->tag:Ljava/lang/Object;

    .line 23
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->response:Ljava/lang/Object;

    .line 24
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->headerValues:Ljava/util/HashMap;

    .line 25
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->headerNames:Ljava/util/ArrayList;

    .line 39
    iput-wide p1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->requestId:J

    .line 40
    iput p3, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->privateId:I

    .line 41
    iput-object p4, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->listener:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    .line 42
    iput-object p5, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->url:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->outputStream:Ljava/io/OutputStream;

    .line 44
    iput-object p7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->tag:Ljava/lang/Object;

    .line 45
    iput-object p8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->headerNames:Ljava/util/ArrayList;

    .line 46
    return-void
.end method


# virtual methods
.method public getHeaderNames()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->headerNames:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHeaderValues()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->headerValues:Ljava/util/HashMap;

    return-object v0
.end method

.method public getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->listener:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->outputStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method public getPrivateId()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->privateId:I

    return v0
.end method

.method public getRequestId()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->requestId:J

    return-wide v0
.end method

.method public getResponse()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->response:Ljava/lang/Object;

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->tag:Ljava/lang/Object;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->url:Ljava/lang/String;

    return-object v0
.end method

.method public setHeaderValues(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "headerValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->headerValues:Ljava/util/HashMap;

    .line 128
    return-void
.end method

.method public setResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "response"    # Ljava/lang/Object;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->response:Ljava/lang/Object;

    .line 118
    return-void
.end method
