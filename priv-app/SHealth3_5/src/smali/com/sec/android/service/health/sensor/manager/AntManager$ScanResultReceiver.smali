.class Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/AntManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScanResultReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/manager/AntManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/manager/AntManager;Lcom/sec/android/service/health/sensor/manager/AntManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;-><init>(Lcom/sec/android/service/health/sensor/manager/AntManager;)V

    return-void
.end method


# virtual methods
.method public onAntFsLinkBeacon(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;I)V
    .locals 8

    const/4 v7, -0x1

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v2

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v3

    if-eqz p5, :cond_1

    invoke-virtual {p5}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->getIntValue()I

    move-result v6

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onNewScanResult "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p5}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/manager/AntManager;->notifyDeviceSearchUpdated(Ljava/lang/String;IILjava/lang/String;ZII)V

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onNewScanResult "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    goto :goto_1
.end method

.method public onAntPlusDataMessage(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;)V
    .locals 8

    const/4 v6, -0x1

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v2

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    const/4 v5, 0x0

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/manager/AntManager;->notifyDeviceSearchUpdated(Ljava/lang/String;IILjava/lang/String;ZII)V

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onNewScanResult "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCommonManufacturerInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;II)V
    .locals 4

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v0

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    if-eqz p5, :cond_1

    invoke-virtual {p5}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->getIntValue()I

    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onNewScanResult "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p5}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onNewScanResult "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCommonProductInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;JII)V
    .locals 0

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    goto :goto_0
.end method

.method public onLegacyManufacturerInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;I)V
    .locals 4

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v0

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    if-eqz p5, :cond_1

    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onNewScanResult "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p5}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onNewScanResult "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onScanStopped(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager$3;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$RequestAccessResult:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "Notify - onSearchStopped: USER_CANCELLED"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "Notify - onSearchStopped: CHANNEL_NOT_AVAILABLE"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->notifyHealthServiceError(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "Notify - onSearchStopped: DEPENDENCY_NOT_INSTALLED"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->notifyHealthServiceError(I)V

    goto :goto_0

    :pswitch_3
    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "Notify - onSearchStopped: OTHER_FAILURE"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/AntManager;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->notifyHealthServiceError(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
