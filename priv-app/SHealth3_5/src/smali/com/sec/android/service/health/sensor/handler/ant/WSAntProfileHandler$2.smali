.class Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestBasicMeasurement()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBasicMeasurementFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;Ljava/math/BigDecimal;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$11;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusWeightScalePcc$WeightScaleRequestStatus:[I

    invoke-virtual {p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error. Unrecognized status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onBasicMeasurementFinished - bodyWeight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;-><init>()V

    invoke-virtual {p5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    const v1, 0x1fbd1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasTime:J
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->access$802(J)J

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->access$1000(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)I

    move-result v0

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasDeviceId:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->access$902(I)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_1
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "BasicMeasurement: Fail: Busy"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "BasicMeasurement: Fail: Comm Err"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_3
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "BasicMeasurement: Fail: Trans Lost"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
