.class public Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;
.super Ljava/lang/Object;


# instance fields
.field public appId:Ljava/lang/String;

.field public calcium:Ljava/lang/String;

.field public calorie:Ljava/lang/String;

.field public caloriePerUnit:Ljava/lang/String;

.field public carbohydrate:Ljava/lang/String;

.field public cholesterol:Ljava/lang/String;

.field public defaultNumberOfServingUnit:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceFatsecretNutrientPKId:Ljava/lang/String;

.field public deviceFatsecretPKId:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public deviceWelstoryNutrientPKId:Ljava/lang/String;

.field public deviceWelstoryPKId:Ljava/lang/String;

.field public favoriteYN:Ljava/lang/String;

.field public fiber:Ljava/lang/String;

.field public foodName:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public iron:Ljava/lang/String;

.field public metricServingAmount:Ljava/lang/String;

.field public metricServingUnit:Ljava/lang/String;

.field public monounsaturatedFat:Ljava/lang/String;

.field public nutrientHdid:Ljava/lang/String;

.field public polyunsaturatedFat:Ljava/lang/String;

.field public potassium:Ljava/lang/String;

.field public protein:Ljava/lang/String;

.field public rootCategoryId:Ljava/lang/String;

.field public rootCategoryName:Ljava/lang/String;

.field public saturatedFat:Ljava/lang/String;

.field public serverFoodId:Ljava/lang/String;

.field public serverLocale:Ljava/lang/String;

.field public serverSourceType:Ljava/lang/String;

.field public servingDescription:Ljava/lang/String;

.field public sodium:Ljava/lang/String;

.field public sorting1:Ljava/lang/String;

.field public sorting2:Ljava/lang/String;

.field public subCategoryId:Ljava/lang/String;

.field public subCategoryName:Ljava/lang/String;

.field public sugar:Ljava/lang/String;

.field public totalFat:Ljava/lang/String;

.field public transFat:Ljava/lang/String;

.field public vitaminA:Ljava/lang/String;

.field public vitaminC:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
