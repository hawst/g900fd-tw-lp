.class Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->setAccessToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->runTask(Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;)V

    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    return-void
.end method

.method public setNetworkFailure()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    return-void
.end method
