.class public Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitFEListener;
.super Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;

# interfaces
.implements Lcom/garmin/fit/BikeProfileMesgListener;
.implements Lcom/garmin/fit/DeviceSettingsMesgListener;
.implements Lcom/garmin/fit/HrmProfileMesgListener;
.implements Lcom/garmin/fit/SdmProfileMesgListener;


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onMesg(Lcom/garmin/fit/BikeProfileMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/DeviceSettingsMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/HrmProfileMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/SdmProfileMesg;)V
    .locals 0

    return-void
.end method
