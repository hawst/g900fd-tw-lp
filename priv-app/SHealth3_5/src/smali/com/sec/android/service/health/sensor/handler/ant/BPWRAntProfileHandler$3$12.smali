.class Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$12;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$12;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewPedalSmoothness(JLjava/util/EnumSet;JZLjava/math/BigDecimal;Ljava/math/BigDecimal;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;JZ",
            "Ljava/math/BigDecimal;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation

    const-string v0, "[HealthSensor]BPWRSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewPedalSmoothness - powerOnlyUpdateEventCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", separatePedalSmoothnessSupport: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", leftOrCombinedPedalSmoothness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", rightPedalSmoothness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "ant_powerOnlyUpdateEventCount"

    invoke-virtual {v0, v1, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "ant_separatePedalSmoothnessSupport"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "ant_leftOrCombinedPedalSmoothness"

    invoke-virtual {p7}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "ant_rightPedalSmoothness"

    invoke-virtual {p8}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3$12;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;-><init>()V

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;->notifyResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method
