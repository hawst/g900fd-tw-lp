.class public Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;
.super Ljava/lang/Object;
.source "ConnectionManagerFactory.java"


# static fields
.field private static DEFAULT_TAG:Ljava/lang/String;

.field private static connectionHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->connectionHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 16
    const-string v0, "Adv. Con. Mgr"

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->DEFAULT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized createInstance()Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .locals 2

    .prologue
    .line 20
    const-class v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->DEFAULT_TAG:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .locals 3
    .param p0, "TAG"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v2, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->connectionHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-static {p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->destoryInstance(Ljava/lang/String;)V

    .line 50
    :cond_0
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;-><init>(Ljava/lang/String;)V

    .line 51
    .local v0, "conman":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->connectionHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    monitor-exit v2

    return-object v0

    .line 46
    .end local v0    # "conman":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized destoryInstance()V
    .locals 2

    .prologue
    .line 31
    const-class v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->DEFAULT_TAG:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->destoryInstance(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    monitor-exit v1

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized destoryInstance(Ljava/lang/String;)V
    .locals 3
    .param p0, "TAG"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v2, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->connectionHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 65
    .local v0, "conman":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->cleanupSession()V

    .line 69
    :cond_0
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->connectionHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit v2

    return-void

    .line 64
    .end local v0    # "conman":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .locals 2

    .prologue
    .line 40
    const-class v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->DEFAULT_TAG:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .locals 3
    .param p0, "TAG"    # Ljava/lang/String;

    .prologue
    .line 80
    const-class v2, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->connectionHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    .local v0, "conman":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    monitor-exit v2

    return-object v0

    .line 80
    .end local v0    # "conman":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
