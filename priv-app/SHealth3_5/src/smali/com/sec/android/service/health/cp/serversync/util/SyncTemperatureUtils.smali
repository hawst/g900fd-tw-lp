.class public Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$DeletedTHItemsList;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$RequestDeleteTHHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$RequestGetTHHistory;
    }
.end annotation


# static fields
.field private static final MAX_TEMPERATURE_HUMIDITY_RECORDS:I = 0xa

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTemperatureHumidity(Ljava/lang/Object;)V
    .locals 7

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;->TAG:Ljava/lang/String;

    const-string v1, "addTemperatureHumidity"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;->temperatureHumidityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;->temperatureHumidityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;->temperatureHumidityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "_id"

    iget-wide v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceTHPKId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "application__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->appId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "user_device__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->accessoryId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "temperature"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->temperature:Ljava/lang/Double;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v4, "humidity"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->humidity:Ljava/lang/Double;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v4, "comment"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->comment:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "sample_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->measuredTime:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->daylightSaving:Ljava/lang/String;

    if-eqz v4, :cond_0

    const-string v4, "daylight_saving"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->daylightSaving:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string v4, "hdid"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->hdid:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "create_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "update_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceUpdateTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "time_zone"

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceTimeZone:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "sync_status"

    const v4, 0x29811

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string/jumbo v0, "temperature_humidity"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v1, v0, v4, v3, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error inserting temperature_humidity data. Invalid values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :cond_0
    :try_start_3
    const-string v4, "daylight_saving"

    const v5, 0x445c2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    :cond_2
    return-void
.end method

.method public static getDeletedTemperatureHumidity(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDeletedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$RequestDeleteTHHistory;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$RequestDeleteTHHistory;-><init>()V

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$DeletedTHItemsList;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$DeletedTHItemsList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "deleted_info"

    const-string/jumbo v3, "table_name = \"temperature_humidity\""

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v10, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$DeletedTHItemsList;->deviceCreateTimes:Ljava/util/ArrayList;

    const-string v3, "create_time"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, v10, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$DeletedTHItemsList;->deviceCreateTimes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return-object v2

    :cond_2
    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils$RequestDeleteTHHistory;->deleteList:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_TEMPERATURE_HUMIDITY_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v8

    goto :goto_1
.end method

.method public static getModifiedTemperatureHumidity(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/CursorWindowAllocationException;
        }
    .end annotation

    const/4 v10, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;->TAG:Ljava/lang/String;

    const-string v1, "getUnsyncedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v3, "sync_status = ? or sync_status = ? "

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const v1, 0x29812

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v10

    const/4 v1, 0x1

    const v5, 0x29813

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v1, "temperature_humidity"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v6, v8

    move-object v7, v9

    move v8, v10

    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;-><init>()V

    const-string v0, "_id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceTHPKId:J

    const-string v0, "application__id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->appId:Ljava/lang/String;

    const-string/jumbo v0, "user_device__id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->accessoryId:Ljava/lang/String;

    const-string/jumbo v0, "time_zone"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceTimeZone:Ljava/lang/String;

    const-string/jumbo v0, "temperature"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->temperature:Ljava/lang/Double;

    const-string v0, "humidity"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->humidity:Ljava/lang/Double;

    const-string v0, "comment"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "comment"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->comment:Ljava/lang/String;

    :goto_1
    const-string/jumbo v0, "sample_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v2, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->measuredTime:Ljava/lang/String;

    const-string v0, "daylight_saving"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "daylight_saving"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->daylightSaving:Ljava/lang/String;

    :goto_2
    const-string v0, "hdid"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->hdid:Ljava/lang/String;

    const-string v0, "create_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceCreateTime:Ljava/lang/String;

    const-string/jumbo v0, "update_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceUpdateTime:Ljava/lang/String;

    const-string/jumbo v0, "sync_status"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->requestType:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v1, "temperature_humidity"

    iget-wide v2, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->deviceTHPKId:J

    const-string/jumbo v4, "update_time"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v7, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;->temperatureHumidityList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v8, 0x1

    const/16 v0, 0xa

    if-ne v2, v0, :cond_6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_TEMPERATURE_HUMIDITY:Ljava/lang/String;

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;

    invoke-direct {v1}, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move v2, v10

    :goto_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-object v6, v0

    move-object v7, v1

    move v8, v2

    goto/16 :goto_0

    :cond_0
    const-string v0, ""

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->comment:Ljava/lang/String;

    goto/16 :goto_1

    :cond_1
    const v0, 0x445c2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/TemperatureHumidityItem;->daylightSaving:Ljava/lang/String;

    goto/16 :goto_2

    :cond_2
    move-object v9, v7

    move v10, v8

    move-object v8, v6

    :cond_3
    if-eqz v12, :cond_4

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    if-lez v10, :cond_5

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_TEMPERATURE_HUMIDITY:Ljava/lang/String;

    invoke-direct {v0, v1, v9, v8}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    return-object v11

    :cond_6
    move-object v0, v6

    move-object v1, v7

    goto :goto_3
.end method

.method public static getTemperatureHumidityDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/16 v7, 0xa

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDetailHistoryData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncTemperatureUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " items for temperature humidity"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v0, v2

    :goto_0
    if-lez v1, :cond_1

    new-instance v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;

    invoke-direct {v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;-><init>()V

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;->startTime:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;->endTime:Ljava/lang/String;

    iput v0, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;->offset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;->limit:Ljava/lang/Integer;

    if-lt v1, v7, :cond_0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;->limit:Ljava/lang/Integer;

    add-int/lit8 v0, v0, 0xa

    add-int/lit8 v1, v1, -0xa

    :goto_1
    new-instance v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v6, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_TEMPERATURE_HUMIDITY_HISTORY:Ljava/lang/String;

    invoke-direct {v5, v6, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils$RequestGetUVHistory;->limit:Ljava/lang/Integer;

    add-int/2addr v0, v1

    move v1, v2

    goto :goto_1

    :cond_1
    return-object v3
.end method
