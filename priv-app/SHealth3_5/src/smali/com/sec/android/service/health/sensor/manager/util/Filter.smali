.class public Lcom/sec/android/service/health/sensor/manager/util/Filter;
.super Ljava/lang/Object;


# instance fields
.field private mRequiredDataType:I

.field private mRequiredDeviceId:Ljava/lang/String;

.field private mRequiredDeviceType:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceType:I

    iput p2, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFilterDataType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    return v0
.end method

.method public getFilterDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getFilterDeviceType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceType:I

    return v0
.end method

.method public verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v2

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceType:I

    const/16 v3, 0x2711

    if-eq v0, v3, :cond_6

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceType:I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_1
    iget v3, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :goto_3
    move v0, v1

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_1
.end method
