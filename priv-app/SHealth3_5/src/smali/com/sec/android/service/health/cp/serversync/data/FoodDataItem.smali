.class public Lcom/sec/android/service/health/cp/serversync/data/FoodDataItem;
.super Ljava/lang/Object;


# instance fields
.field public appId:Ljava/lang/String;

.field public calorie:Ljava/lang/String;

.field public carbohydrate:Ljava/lang/Float;

.field public cholesterol:Ljava/lang/Float;

.field public deviceCreateTimeFoodItem:Ljava/lang/String;

.field public deviceFoodItemPKId:Ljava/lang/String;

.field public deviceModifyTimeFoodItem:Ljava/lang/String;

.field public deviceTimeZoneFoodItem:Ljava/lang/String;

.field public fat:Ljava/lang/Float;

.field public foodDataType:Ljava/lang/String;

.field public foodId:Ljava/lang/String;

.field public foodName:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public protein:Ljava/lang/Float;

.field public requestType:Ljava/lang/String;

.field public sodium:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
