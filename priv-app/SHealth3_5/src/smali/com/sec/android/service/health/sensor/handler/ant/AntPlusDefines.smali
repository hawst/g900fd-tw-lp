.class public Lcom/sec/android/service/health/sensor/handler/ant/AntPlusDefines;
.super Ljava/lang/Object;


# static fields
.field public static final DEVICE_TYPE_ALL_DEVICE:I = 0x0

.field public static final DEVICE_TYPE_BIKE_CADENCE:I = 0x7a

.field public static final DEVICE_TYPE_BIKE_POWER:I = 0xb

.field public static final DEVICE_TYPE_BIKE_SPEED:I = 0x7b

.field public static final DEVICE_TYPE_BIKE_SPEED_CADENCE:I = 0x79

.field public static final DEVICE_TYPE_BLOOD_PRESSURE:I = 0x12

.field public static final DEVICE_TYPE_FITNESS_EQUIPMENT:I = 0x11

.field public static final DEVICE_TYPE_HEART_RATE:I = 0x78

.field public static final DEVICE_TYPE_STRIDE_SDM:I = 0x7c

.field public static final DEVICE_TYPE_WATCH:I = -0x1

.field public static final DEVICE_TYPE_WEIGHT_SCALE:I = 0x77

.field public static final FIELD_FE_BIKE_INSTANTANEOUS_CADENCE:Ljava/lang/String; = "Cadence"

.field public static final FIELD_FE_BIKE_INSTANTANEOUS_POWER:Ljava/lang/String; = "Instantaneous Power"

.field public static final FIELD_FE_CLIMBER_CUMULATIVE_STRIDES:Ljava/lang/String; = "Cycles(Strides)"

.field public static final FIELD_FE_CLIMBER_INSTANTANEOUS_CADENCE:Ljava/lang/String; = "Cadence"

.field public static final FIELD_FE_CLIMBER_INSTANTANEOUS_POWER:Ljava/lang/String; = "Instantaneous Power"

.field public static final FIELD_FE_EP_CUMULATIVE_POS_VERT_DISTANCE:Ljava/lang/String; = "Positive vertical distance"

.field public static final FIELD_FE_EP_CUMULATIVE_STRIDES:Ljava/lang/String; = "Stride Count"

.field public static final FIELD_FE_EP_INSTANTANEOUS_CADENCE:Ljava/lang/String; = "Cadence"

.field public static final FIELD_FE_EP_INSTANTANEOUS_POWER:Ljava/lang/String; = "Instantaneous Power"

.field public static final FIELD_FE_EQUIPMENT_TYPE:Ljava/lang/String; = "Equipment Type"

.field public static final FIELD_FE_GEN_CUMULATIVE_CALORIES:Ljava/lang/String; = "Calories"

.field public static final FIELD_FE_GEN_CUMULATIVE_DISTANCE:Ljava/lang/String; = "Distance Traveled"

.field public static final FIELD_FE_GEN_CYCLE_LENGTH:Ljava/lang/String; = "Cycle length"

.field public static final FIELD_FE_GEN_ELAPSED_TIME:Ljava/lang/String; = "Elapsed Time"

.field public static final FIELD_FE_GEN_HR_DATA_SOURCE:Ljava/lang/String; = "HR Data Source"

.field public static final FIELD_FE_GEN_INCLINE_PERCENTAGE:Ljava/lang/String; = "Incline"

.field public static final FIELD_FE_GEN_INSTANTANEOUS_CALORICBURN:Ljava/lang/String; = "Caloric burn rate"

.field public static final FIELD_FE_GEN_INSTANTANEOUS_HEARTRATE:Ljava/lang/String; = "Heart Rate"

.field public static final FIELD_FE_GEN_INSTANTANEOUS_METS:Ljava/lang/String; = "METs"

.field public static final FIELD_FE_GEN_INSTANTANEOUS_SPEED:Ljava/lang/String; = "Speed"

.field public static final FIELD_FE_GEN_LAP_COUNT:Ljava/lang/String; = "Lap Count"

.field public static final FIELD_FE_GEN_RESISTANCE_LEVEL:Ljava/lang/String; = "Resistance Level"

.field public static final FIELD_FE_MSG_COUNT:Ljava/lang/String; = "Current MessageCount"

.field public static final FIELD_FE_ROWER_CUMULATIVE_STROKERS:Ljava/lang/String; = "Stroke count"

.field public static final FIELD_FE_ROWER_INSTANTANEOUS_CADENCE:Ljava/lang/String; = "Cadence"

.field public static final FIELD_FE_ROWER_INSTANTANEOUS_POWER:Ljava/lang/String; = "Instantaneous Power"

.field public static final FIELD_FE_SKIER_CUMULATIVE_STRIDES:Ljava/lang/String; = "Stride Count"

.field public static final FIELD_FE_SKIER_INSTANTANEOUS_CADENCE:Ljava/lang/String; = "Cadence"

.field public static final FIELD_FE_SKIER_INSTANTANEOUS_POWER:Ljava/lang/String; = "Instantaneous Power"

.field public static final FIELD_FE_STATE:Ljava/lang/String; = "FE state"

.field public static final FIELD_FE_TM_CUMULATIVE_NEG_VERT_DISTANCE:Ljava/lang/String; = "Negative vertical distance"

.field public static final FIELD_FE_TM_CUMULATIVE_POS_VERT_DISTANCE:Ljava/lang/String; = "Positive vertical distance"

.field public static final FIELD_FE_TM_INSTANTANEOUS_CADENCE:Ljava/lang/String; = "Cadence"

.field public static final FITNESS_EQUIPMENT_STATE_ASLEEP_OFF:I = 0x1

.field public static final FITNESS_EQUIPMENT_STATE_FINISHED_PAUSED:I = 0x4

.field public static final FITNESS_EQUIPMENT_STATE_IN_USE:I = 0x3

.field public static final FITNESS_EQUIPMENT_STATE_READY:I = 0x2

.field public static final FITNESS_EQUIPMENT_TYPE_BIKE:I = 0x15

.field public static final FITNESS_EQUIPMENT_TYPE_CLIMBER:I = 0x17

.field public static final FITNESS_EQUIPMENT_TYPE_ELLIPTICAL:I = 0x14

.field public static final FITNESS_EQUIPMENT_TYPE_GENERAL:I = 0x10

.field public static final FITNESS_EQUIPMENT_TYPE_NORDICSKIER:I = 0x18

.field public static final FITNESS_EQUIPMENT_TYPE_ROWER:I = 0x16

.field public static final FITNESS_EQUIPMENT_TYPE_TREADMILL:I = 0x13

.field public static final GARMIN_PRODUCT_ALF04:I = 0x53d

.field public static final GARMIN_PRODUCT_AMX:I = 0x5b5

.field public static final GARMIN_PRODUCT_AXB01:I = 0x3

.field public static final GARMIN_PRODUCT_AXB02:I = 0x4

.field public static final GARMIN_PRODUCT_AXH01:I = 0x2

.field public static final GARMIN_PRODUCT_CHIRP:I = 0x4e5

.field public static final GARMIN_PRODUCT_CONNECT:I = 0xfffe

.field public static final GARMIN_PRODUCT_DSI_ALF01:I = 0x3f3

.field public static final GARMIN_PRODUCT_DSI_ALF02:I = 0x6

.field public static final GARMIN_PRODUCT_EDGE200:I = 0x52d

.field public static final GARMIN_PRODUCT_EDGE500:I = 0x40c

.field public static final GARMIN_PRODUCT_EDGE800:I = 0x491

.field public static final GARMIN_PRODUCT_FR110:I = 0x464

.field public static final GARMIN_PRODUCT_FR310XT:I = 0x3fa

.field public static final GARMIN_PRODUCT_FR310XT_4T:I = 0x5a6

.field public static final GARMIN_PRODUCT_FR405:I = 0x2cd

.field public static final GARMIN_PRODUCT_FR50:I = 0x30e

.field public static final GARMIN_PRODUCT_FR60:I = 0x3dc

.field public static final GARMIN_PRODUCT_FR610:I = 0x541

.field public static final GARMIN_PRODUCT_FR70:I = 0x59c

.field public static final GARMIN_PRODUCT_FR910XT:I = 0x530

.field public static final GARMIN_PRODUCT_HRM1:I = 0x1

.field public static final GARMIN_PRODUCT_HRM2SS:I = 0x5

.field public static final GARMIN_PRODUCT_SDM4:I = 0x2717

.field public static final GARMIN_PRODUCT_TRANING_CENTER:I = 0x4e97

.field public static final MANUFACTURER_ID_4IIIIS:I = 0x33

.field public static final MANUFACTURER_ID_ACE_SENSOR:I = 0x2b

.field public static final MANUFACTURER_ID_ACTIGRAPHCORP:I = 0x167f

.field public static final MANUFACTURER_ID_ALATECH_TECHNOLOGY_LTD:I = 0x3a

.field public static final MANUFACTURER_ID_ARCHINOETICS:I = 0x22

.field public static final MANUFACTURER_ID_A_AND_D:I = 0x15

.field public static final MANUFACTURER_ID_BEURER:I = 0x13

.field public static final MANUFACTURER_ID_BF1SYSTEMS:I = 0x2f

.field public static final MANUFACTURER_ID_BKOOL:I = 0x43

.field public static final MANUFACTURER_ID_BREAKAWAY:I = 0x39

.field public static final MANUFACTURER_ID_BRIM_BROTHERS:I = 0x2c

.field public static final MANUFACTURER_ID_CARDIOSPORT:I = 0x14

.field public static final MANUFACTURER_ID_CATEYE:I = 0x44

.field public static final MANUFACTURER_ID_CITIZEN_SYSTEMS:I = 0x24

.field public static final MANUFACTURER_ID_CLEAN_MOBILE:I = 0x1a

.field public static final MANUFACTURER_ID_CONCEPT2:I = 0x28

.field public static final MANUFACTURER_ID_DAYTON:I = 0x4

.field public static final MANUFACTURER_ID_DEVELOPMENT:I = 0xff

.field public static final MANUFACTURER_ID_DEXCOM:I = 0x1f

.field public static final MANUFACTURER_ID_DYNASTREAM:I = 0xf

.field public static final MANUFACTURER_ID_DYNASTREAM_OEM:I = 0xd

.field public static final MANUFACTURER_ID_ECHOWELL:I = 0xc

.field public static final MANUFACTURER_ID_GARMIN:I = 0x1

.field public static final MANUFACTURER_ID_GARMIN_FR405_ANTFS:I = 0x2

.field public static final MANUFACTURER_ID_GEONAUTE:I = 0x3d

.field public static final MANUFACTURER_ID_GPULSE:I = 0x19

.field public static final MANUFACTURER_ID_HMM:I = 0x16

.field public static final MANUFACTURER_ID_HOLUX:I = 0x27

.field public static final MANUFACTURER_ID_IBIKE:I = 0x8

.field public static final MANUFACTURER_ID_IDT:I = 0x5

.field public static final MANUFACTURER_ID_ID_BIKE:I = 0x3e

.field public static final MANUFACTURER_ID_IFOR_POWELL:I = 0x36

.field public static final MANUFACTURER_ID_LEMOND_FITNESS:I = 0x1e

.field public static final MANUFACTURER_ID_MAGELLAN:I = 0x25

.field public static final MANUFACTURER_ID_MAXWELL_GUIDER:I = 0x37

.field public static final MANUFACTURER_ID_METALOGICS:I = 0x32

.field public static final MANUFACTURER_ID_METRIGEAR:I = 0x11

.field public static final MANUFACTURER_ID_MIO_TECHNOLOGY_EUROPE:I = 0x3b

.field public static final MANUFACTURER_ID_NAUTILUS:I = 0xe

.field public static final MANUFACTURER_ID_NORTH_POLE_ENGINEERING:I = 0x42

.field public static final MANUFACTURER_ID_OCTANE_FITNESS:I = 0x21

.field public static final MANUFACTURER_ID_ONE_GIANT_LEAP:I = 0x2a

.field public static final MANUFACTURER_ID_OSYNCE:I = 0x26

.field public static final MANUFACTURER_ID_PEAKSWARE:I = 0x1c

.field public static final MANUFACTURER_ID_PEDAL_BRAIN:I = 0x1b

.field public static final MANUFACTURER_ID_PERCEPTION_DIGITAL:I = 0x2e

.field public static final MANUFACTURER_ID_PHYSICAL_ENTERPRISES:I = 0x41

.field public static final MANUFACTURER_ID_PIONEER:I = 0x30

.field public static final MANUFACTURER_ID_QUARQ:I = 0x7

.field public static final MANUFACTURER_ID_ROTOR:I = 0x3c

.field public static final MANUFACTURER_ID_SARIS:I = 0x9

.field public static final MANUFACTURER_ID_SAXONAR:I = 0x1d

.field public static final MANUFACTURER_ID_SEIKO_EPSON:I = 0x34

.field public static final MANUFACTURER_ID_SEIKO_EPSON_OEM:I = 0x35

.field public static final MANUFACTURER_ID_SPANTEX:I = 0x31

.field public static final MANUFACTURER_ID_SPARK_HK:I = 0xa

.field public static final MANUFACTURER_ID_SPECIALIZED:I = 0x3f

.field public static final MANUFACTURER_ID_SRM:I = 0x6

.field public static final MANUFACTURER_ID_STAGES_CYCLING:I = 0x45

.field public static final MANUFACTURER_ID_STAR_TRAC:I = 0x38

.field public static final MANUFACTURER_ID_SUUNTO:I = 0x17

.field public static final MANUFACTURER_ID_TANITA:I = 0xb

.field public static final MANUFACTURER_ID_THE_HURT_BOX:I = 0x23

.field public static final MANUFACTURER_ID_THITA_ELEKTRONIK:I = 0x18

.field public static final MANUFACTURER_ID_TIMEX:I = 0x10

.field public static final MANUFACTURER_ID_WAHOO_FITNESS:I = 0x20

.field public static final MANUFACTURER_ID_WTEK:I = 0x40

.field public static final MANUFACTURER_ID_XELIC:I = 0x12

.field public static final MANUFACTURER_ID_XPLOVA:I = 0x2d

.field public static final MANUFACTURER_ID_ZEPHYR:I = 0x3

.field public static final STRING_GARMIN_PRODUCT_ALF04:Ljava/lang/String; = "alf04"

.field public static final STRING_GARMIN_PRODUCT_AMX:Ljava/lang/String; = "amx"

.field public static final STRING_GARMIN_PRODUCT_AXB01:Ljava/lang/String; = "axb01"

.field public static final STRING_GARMIN_PRODUCT_AXB02:Ljava/lang/String; = "axb02"

.field public static final STRING_GARMIN_PRODUCT_AXH01:Ljava/lang/String; = "axh01"

.field public static final STRING_GARMIN_PRODUCT_CHIRP:Ljava/lang/String; = "chirp"

.field public static final STRING_GARMIN_PRODUCT_CONNECT:Ljava/lang/String; = "connect"

.field public static final STRING_GARMIN_PRODUCT_DSI_ALF01:Ljava/lang/String; = "dsi_alf01"

.field public static final STRING_GARMIN_PRODUCT_DSI_ALF02:Ljava/lang/String; = "dsi_alf02"

.field public static final STRING_GARMIN_PRODUCT_EDGE200:Ljava/lang/String; = "edge200"

.field public static final STRING_GARMIN_PRODUCT_EDGE500:Ljava/lang/String; = "edge500"

.field public static final STRING_GARMIN_PRODUCT_EDGE800:Ljava/lang/String; = "edge800"

.field public static final STRING_GARMIN_PRODUCT_FR110:Ljava/lang/String; = "fr110"

.field public static final STRING_GARMIN_PRODUCT_FR310XT:Ljava/lang/String; = "fr310xt"

.field public static final STRING_GARMIN_PRODUCT_FR310XT_4T:Ljava/lang/String; = "fr310xt_4t"

.field public static final STRING_GARMIN_PRODUCT_FR405:Ljava/lang/String; = "fr405"

.field public static final STRING_GARMIN_PRODUCT_FR50:Ljava/lang/String; = "fr50"

.field public static final STRING_GARMIN_PRODUCT_FR60:Ljava/lang/String; = "fr60"

.field public static final STRING_GARMIN_PRODUCT_FR610:Ljava/lang/String; = "fr610"

.field public static final STRING_GARMIN_PRODUCT_FR70:Ljava/lang/String; = "fr70"

.field public static final STRING_GARMIN_PRODUCT_FR910XT:Ljava/lang/String; = "fr910xt"

.field public static final STRING_GARMIN_PRODUCT_HRM1:Ljava/lang/String; = "hrm1"

.field public static final STRING_GARMIN_PRODUCT_HRM2SS:Ljava/lang/String; = "hrm2ss"

.field public static final STRING_GARMIN_PRODUCT_SDM4:Ljava/lang/String; = "sdm4"

.field public static final STRING_GARMIN_PRODUCT_TRANING_CENTER:Ljava/lang/String; = "training_center"

.field public static final STRING_MANUFACTURER_ID_4IIIIS:Ljava/lang/String; = "4iiii Innovations Inc"

.field public static final STRING_MANUFACTURER_ID_ACE_SENSOR:Ljava/lang/String; = "ace_sensor"

.field public static final STRING_MANUFACTURER_ID_ACTIGRAPHCORP:Ljava/lang/String; = "ActiGraph"

.field public static final STRING_MANUFACTURER_ID_ALATECH_TECHNOLOGY_LTD:Ljava/lang/String; = "ALATECH"

.field public static final STRING_MANUFACTURER_ID_ARCHINOETICS:Ljava/lang/String; = "Archinoetics"

.field public static final STRING_MANUFACTURER_ID_A_AND_D:Ljava/lang/String; = "A&D"

.field public static final STRING_MANUFACTURER_ID_BEURER:Ljava/lang/String; = "Beurer"

.field public static final STRING_MANUFACTURER_ID_BF1SYSTEMS:Ljava/lang/String; = "Factor Cycle"

.field public static final STRING_MANUFACTURER_ID_BKOOL:Ljava/lang/String; = "BKOOL"

.field public static final STRING_MANUFACTURER_ID_BREAKAWAY:Ljava/lang/String; = "Breakaway Sports"

.field public static final STRING_MANUFACTURER_ID_BRIM_BROTHERS:Ljava/lang/String; = "Brim Brothers"

.field public static final STRING_MANUFACTURER_ID_CARDIOSPORT:Ljava/lang/String; = "Cardiosport"

.field public static final STRING_MANUFACTURER_ID_CATEYE:Ljava/lang/String; = "CatEye"

.field public static final STRING_MANUFACTURER_ID_CITIZEN_SYSTEMS:Ljava/lang/String; = "Citizen Systems"

.field public static final STRING_MANUFACTURER_ID_CLEAN_MOBILE:Ljava/lang/String; = "clean_mobile"

.field public static final STRING_MANUFACTURER_ID_CONCEPT2:Ljava/lang/String; = "Concept2"

.field public static final STRING_MANUFACTURER_ID_DAYTON:Ljava/lang/String; = "dayton"

.field public static final STRING_MANUFACTURER_ID_DEVELOPMENT:Ljava/lang/String; = "development"

.field public static final STRING_MANUFACTURER_ID_DEXCOM:Ljava/lang/String; = "Dexcom"

.field public static final STRING_MANUFACTURER_ID_DYNASTREAM:Ljava/lang/String; = "Dynastream"

.field public static final STRING_MANUFACTURER_ID_DYNASTREAM_OEM:Ljava/lang/String; = "Dynastream OEM"

.field public static final STRING_MANUFACTURER_ID_ECHOWELL:Ljava/lang/String; = "Echowell Electronic Co., Ltd."

.field public static final STRING_MANUFACTURER_ID_GARMIN:Ljava/lang/String; = "Garmin"

.field public static final STRING_MANUFACTURER_ID_GARMIN_FR405_ANTFS:Ljava/lang/String; = "Garmin"

.field public static final STRING_MANUFACTURER_ID_GEONAUTE:Ljava/lang/String; = "Geonaute"

.field public static final STRING_MANUFACTURER_ID_GPULSE:Ljava/lang/String; = "GPULSE"

.field public static final STRING_MANUFACTURER_ID_HMM:Ljava/lang/String; = "HMM"

.field public static final STRING_MANUFACTURER_ID_HOLUX:Ljava/lang/String; = "Holux"

.field public static final STRING_MANUFACTURER_ID_IBIKE:Ljava/lang/String; = "iBike"

.field public static final STRING_MANUFACTURER_ID_IDT:Ljava/lang/String; = "idt"

.field public static final STRING_MANUFACTURER_ID_ID_BIKE:Ljava/lang/String; = "idBike"

.field public static final STRING_MANUFACTURER_ID_IFOR_POWELL:Ljava/lang/String; = "Ifor Powell"

.field public static final STRING_MANUFACTURER_ID_LEMOND_FITNESS:Ljava/lang/String; = "Lemond Fitness"

.field public static final STRING_MANUFACTURER_ID_MAGELLAN:Ljava/lang/String; = "Magellan"

.field public static final STRING_MANUFACTURER_ID_MAXWELL_GUIDER:Ljava/lang/String; = "maxwell_guider"

.field public static final STRING_MANUFACTURER_ID_METALOGICS:Ljava/lang/String; = "Metalogics"

.field public static final STRING_MANUFACTURER_ID_METRIGEAR:Ljava/lang/String; = "metrigear"

.field public static final STRING_MANUFACTURER_ID_MIO_TECHNOLOGY_EUROPE:Ljava/lang/String; = "Mio Technology"

.field public static final STRING_MANUFACTURER_ID_NAUTILUS:Ljava/lang/String; = "nautilus"

.field public static final STRING_MANUFACTURER_ID_NORTH_POLE_ENGINEERING:Ljava/lang/String; = "NPE"

.field public static final STRING_MANUFACTURER_ID_OCTANE_FITNESS:Ljava/lang/String; = "Octane Fitness"

.field public static final STRING_MANUFACTURER_ID_ONE_GIANT_LEAP:Ljava/lang/String; = "One Giant Leap"

.field public static final STRING_MANUFACTURER_ID_OSYNCE:Ljava/lang/String; = "o-synce"

.field public static final STRING_MANUFACTURER_ID_PEAKSWARE:Ljava/lang/String; = "Training Peaks"

.field public static final STRING_MANUFACTURER_ID_PEDAL_BRAIN:Ljava/lang/String; = "Pedal Brain"

.field public static final STRING_MANUFACTURER_ID_PERCEPTION_DIGITAL:Ljava/lang/String; = "perception_digital"

.field public static final STRING_MANUFACTURER_ID_PHYSICAL_ENTERPRISES:Ljava/lang/String; = "Mio"

.field public static final STRING_MANUFACTURER_ID_PIONEER:Ljava/lang/String; = "Pioneer"

.field public static final STRING_MANUFACTURER_ID_QUARQ:Ljava/lang/String; = "Quarq"

.field public static final STRING_MANUFACTURER_ID_ROTOR:Ljava/lang/String; = "ROTOR Bike Components"

.field public static final STRING_MANUFACTURER_ID_SARIS:Ljava/lang/String; = "PowerTap"

.field public static final STRING_MANUFACTURER_ID_SAXONAR:Ljava/lang/String; = "power2max"

.field public static final STRING_MANUFACTURER_ID_SEIKO_EPSON:Ljava/lang/String; = "Seiko Epson"

.field public static final STRING_MANUFACTURER_ID_SEIKO_EPSON_OEM:Ljava/lang/String; = "Seiko Epson"

.field public static final STRING_MANUFACTURER_ID_SPANTEX:Ljava/lang/String; = "Spantec Computer"

.field public static final STRING_MANUFACTURER_ID_SPARK_HK:Ljava/lang/String; = "spark_hk"

.field public static final STRING_MANUFACTURER_ID_SPECIALIZED:Ljava/lang/String; = "Specialized"

.field public static final STRING_MANUFACTURER_ID_SRM:Ljava/lang/String; = "SRM"

.field public static final STRING_MANUFACTURER_ID_STAGES_CYCLING:Ljava/lang/String; = "Stages Cycling"

.field public static final STRING_MANUFACTURER_ID_STAR_TRAC:Ljava/lang/String; = "StarTrac"

.field public static final STRING_MANUFACTURER_ID_SUUNTO:Ljava/lang/String; = "SUUNTO"

.field public static final STRING_MANUFACTURER_ID_TANITA:Ljava/lang/String; = "TANITA"

.field public static final STRING_MANUFACTURER_ID_THE_HURT_BOX:Ljava/lang/String; = "the_hurt_box"

.field public static final STRING_MANUFACTURER_ID_THITA_ELEKTRONIK:Ljava/lang/String; = "Thita Elektronik"

.field public static final STRING_MANUFACTURER_ID_TIMEX:Ljava/lang/String; = "Timex"

.field public static final STRING_MANUFACTURER_ID_WAHOO_FITNESS:Ljava/lang/String; = "Wahoo Fitness"

.field public static final STRING_MANUFACTURER_ID_WTEK:Ljava/lang/String; = "WTEK"

.field public static final STRING_MANUFACTURER_ID_XELIC:Ljava/lang/String; = "Xelic"

.field public static final STRING_MANUFACTURER_ID_XPLOVA:Ljava/lang/String; = "Xplova"

.field public static final STRING_MANUFACTURER_ID_ZEPHYR:Ljava/lang/String; = "Zephyr"

.field public static final STRING_TANITA_PRODUCT_BC1000:Ljava/lang/String; = "BC-1000"

.field public static final STRING_TANITA_PRODUCT_BC1100F:Ljava/lang/String; = "BC-1100F"

.field public static final STRING_TANITA_PRODUCT_BC1500:Ljava/lang/String; = "BC-1500"

.field public static final TANITA_PRODUCT_BC1000:I = 0x0

.field public static final TANITA_PRODUCT_BC1100F:I = 0x3

.field public static final TANITA_PRODUCT_BC1500:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
