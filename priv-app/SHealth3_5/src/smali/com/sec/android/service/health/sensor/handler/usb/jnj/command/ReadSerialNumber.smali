.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private createResponse(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "RES_GET_SERIAL_NUMBER"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setEvent(I)V

    const-string/jumbo v2, "serial_number"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setResponse(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]JNJ"

    const-string v1, "Protocol listener is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSerialNumber([B)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    new-array v0, v3, [B

    array-length v1, v0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_SERIAL_NUMBER:[B

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    array-length v0, p1

    add-int/lit8 v0, v0, -0x2

    new-array v1, v0, [B

    array-length v0, v1

    invoke-static {p1, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method


# virtual methods
.method public doAction()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_SERIAL_NUMBER:[B

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method public responseReceived([B)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->getSerialNumber([B)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "ReadSerialNumber"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "JNJ Device serial number = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->setDeviceSerialNumber(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendAck()V

    return-void

    :cond_0
    const-string v0, "ReadSerialNumber"

    const-string v1, "JNJ Device serial number is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
