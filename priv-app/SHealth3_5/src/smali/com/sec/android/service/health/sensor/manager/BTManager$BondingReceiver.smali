.class Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/BTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BondingReceiver"
.end annotation


# instance fields
.field private _ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private isRegistered:Z

.field private mFilter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->_ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->isRegistered:Z

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->_ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->_ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->isRegistered:Z

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->mFilter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    return-void
.end method


# virtual methods
.method public isRegistered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->isRegistered:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTManager BroadcastReceiver onReceive() action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager BroadcastReceiver onReceive() DEVICE FOUND"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[HealthSensor]BTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BTManager BroadcastReceiver onReceive() deviceFound name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " class= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->mFilter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager BondingReceiver onReceive()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->_ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTManager BondingReceiver onReceive device : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "android.bluetooth.device.extra.BOND_STATE"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "[HealthSensor]BTManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BTManager BondingReceiver onReceive previousBondState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "[HealthSensor]BTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BTManager BondingReceiver onReceive currentBondState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xc

    if-eq v2, v1, :cond_2

    if-nez v2, :cond_4

    :cond_2
    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager BondingReceiver onReceive success"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager bondStateChangedEvent() health profile, Device Bonded "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTManager initialize() controller not there for the BTAddress "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", so Creating it"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->initProfileController(Landroid/bluetooth/BluetoothDevice;)V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->_ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    const/16 v0, 0xa

    if-ne v2, v0, :cond_0

    :try_start_1
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager BondingReceiver onReceive BONE NONE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->_ShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->setRegistered(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public setRegistered(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->isRegistered:Z

    return-void
.end method
