.class public Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;,
        Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;,
        Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;
    }
.end annotation


# instance fields
.field public mealList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;->mealList:Ljava/util/ArrayList;

    return-void
.end method
