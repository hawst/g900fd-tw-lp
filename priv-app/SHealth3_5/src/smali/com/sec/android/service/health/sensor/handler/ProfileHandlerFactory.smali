.class public Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]ProfileHandlerFactory"

.field private static mInstance:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;


# instance fields
.field mCapabilityMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;",
            ">;>;"
        }
    .end annotation
.end field

.field sensorDeviceClasses:[Ljava/lang/Class;

.field private sensorDeviceHandlerClass:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceHandlerClass:Ljava/util/ArrayList;

    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SAPHRMProfileHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/sec/android/service/health/sensor/handler/UsbHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/BPWRAntProfileHandler;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/FEAntProfileHandler;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-class v2, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-class v2, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-class v2, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-class v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-class v2, Lcom/sec/android/service/health/sensor/handler/temperature/TemperatureHandler;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-class v2, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-class v2, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceClasses:[Ljava/lang/Class;

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->initilize()V

    return-void
.end method

.method private addToCapabilityMapper(Ljava/lang/Class;)V
    .locals 10

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getConnectivityType()I

    move-result v9

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProfileName()Ljava/lang/String;

    move-result-object v6

    move-object v2, p0

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;-><init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v9, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/ArrayList;

    move-object v8, v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProfileName()Ljava/lang/String;

    move-result-object v6

    move-object v2, p0

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;-><init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v9, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkAvailability(Ljava/util/List;Ljava/util/List;II)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;II)Z"
        }
    .end annotation

    const/16 v6, 0x2711

    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne p3, v6, :cond_0

    if-nez p4, :cond_0

    :goto_0
    return v3

    :cond_0
    if-nez p1, :cond_4

    move v1, v2

    :goto_1
    move v5, v4

    :goto_2
    if-ge v5, v1, :cond_8

    if-eq p3, v6, :cond_1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p3, :cond_5

    :cond_1
    move v5, v3

    :goto_3
    if-ne v5, v3, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    :cond_2
    move v1, v4

    :goto_4
    if-ge v1, v2, :cond_7

    if-eqz p4, :cond_3

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p4, :cond_6

    :cond_3
    move v0, v3

    :goto_5
    and-int v3, v5, v0

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_7
    move v0, v4

    goto :goto_5

    :cond_8
    move v5, v4

    goto :goto_3
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mInstance:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mInstance:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mInstance:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    return-object v0
.end method

.method private initilize()V
    .locals 12

    const/4 v10, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->prepopulateListOfClasses()V

    move v9, v10

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceHandlerClass:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v9, v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceHandlerClass:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getConnectivityType()I

    move-result v11

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProfileName()Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceHandlerClass:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Class;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;-><init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :goto_1
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/ArrayList;

    move-object v8, v0

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProfileName()Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceHandlerClass:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Class;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;-><init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InstantiationException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v10

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[HealthSensor]ProfileHandlerFactory"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v10

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[HealthSensor]ProfileHandlerFactory"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_1
    return-void
.end method

.method private prepopulateListOfClasses()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceHandlerClass:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceClasses:[Ljava/lang/Class;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceHandlerClass:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->sensorDeviceClasses:[Ljava/lang/Class;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private searchProfileHandlerByDeviceName(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    .locals 9

    const/4 v3, 0x0

    if-nez p1, :cond_0

    move-object v1, v3

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_1

    move-object v1, v3

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_4

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "GALAXY Gear"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "GALAXY Gear"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getHandlerClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :cond_4
    if-eqz v2, :cond_3

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    :try_start_1
    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getHandlerClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-object v1, v0
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_0

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :cond_5
    move-object v1, v3

    goto/16 :goto_0
.end method

.method private searchProfileHandlerByDeviceTypeDataType(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    .locals 6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v5

    invoke-direct {p0, v3, v4, v5, v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->checkAvailability(Ljava/util/List;Ljava/util/List;II)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getHandlerClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    return-object v0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private searchProfileHandlerByProtocol(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    .locals 5

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->getInstance()Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->searchProtocol(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProfileName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getProfileName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getProfileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getHandlerClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public checkAvailability(IIILjava/lang/String;Z)Z
    .locals 5

    const/4 v3, 0x7

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p5, :cond_1

    if-eq p1, v3, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    if-eqz p4, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->getInstance()Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->checkProtocolAvailability(IIILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_2
    if-eq p1, v2, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x6

    if-eq p1, v0, :cond_3

    if-ne p1, v3, :cond_4

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->getInstance()Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->checkProtocolAvailability(III)Z

    move-result v0

    if-ne v0, v2, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v4, v0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->checkAvailability(Ljava/util/List;Ljava/util/List;II)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public checkSensorAvailability(IIILjava/lang/String;Z)Z
    .locals 7

    const/4 v6, 0x1

    if-nez p1, :cond_1

    move v1, v6

    :goto_0
    const/16 v0, 0x8

    if-ge v1, v0, :cond_2

    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->checkAvailability(IIILjava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->checkAvailability(IIILjava/lang/String;Z)Z

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getProfileHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    const-string v1, "[HealthSensor]ProfileHandlerFactory"

    const-string v2, "ProfileHandlerFactory getProfileHandler() :   device is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->searchProfileHandlerByProtocol(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v0

    if-nez v0, :cond_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDeviceOriginalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->searchProfileHandlerByDeviceName(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->searchProfileHandlerByDeviceTypeDataType(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getProfileHandler(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProfileHandlerList(I)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->getHandlerClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "[HealthSensor]ProfileHandlerFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "() Exception :"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "[HealthSensor]ProfileHandlerFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "() Exception :"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getProfileNames(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
