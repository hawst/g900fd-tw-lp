.class public abstract Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
.super Ljava/lang/Thread;

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;,
        Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;,
        Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;,
        Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;,
        Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;
    }
.end annotation


# static fields
.field private static final DEFAULT_WEIGHT:I = 0x5

.field public static final DELETE_USER_BACKUP_DATA:I = 0x2715

.field private static final DUPLICATEDKEYERROR:Ljava/lang/String; = "duplicatedkeyerror"

.field protected static final PRIFIX_SYNC_LAST_TIME:Ljava/lang/String; = "SYNC_TIME_"

.field public static final SYNC_STATUS_DELETE_DATA:I = 0x2712

.field public static final SYNC_STATUS_GET_DETAIL_HISTORY:I = 0x2714

.field public static final SYNC_STATUS_GET_SYNC_HISTORY:I = 0x2713

.field public static final SYNC_STATUS_SET_DATA:I = 0x2711

.field public static final SYNC_STATUS_SET_IMAGE:I = 0x2716

.field private static mBaseSyncTaskObject:Ljava/lang/Object;

.field protected static mGson:Lcom/google/gson/Gson;


# instance fields
.field private MAX_PARALLEL_IMAGES_ALLOWED:I

.field private final TAG:Ljava/lang/String;

.field private mDataSyncType:I

.field private mDeletedLocalData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadImageUrl:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mErrorWhileBackup:I

.field private mFinishedImageCount:I

.field private mGetDetailHistoryRequestor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation
.end field

.field protected mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

.field protected mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

.field private mMaxImageToDownload:I

.field protected final mRequestType:I

.field private mStartedImageCount:I

.field private mSyncEndTime:J

.field private mSyncType:Ljava/lang/String;

.field private mTaskWeight:J

.field protected mUnsyncedImageData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation
.end field

.field protected mUnsyncedLocalData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mBaseSyncTaskObject:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const-class v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mErrorWhileBackup:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mTaskWeight:J

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->MAX_PARALLEL_IMAGES_ALLOWED:I

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mMaxImageToDownload:I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaseSyncTask(): requestType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " dataSyncType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iput p3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDataSyncType:I

    iput p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mRequestType:I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGson:Lcom/google/gson/Gson;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mMaxImageToDownload:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->downloadImages()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    return v0
.end method

.method static synthetic access$608(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I
    .locals 2

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    return v0
.end method

.method static synthetic access$708(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I
    .locals 2

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->uploadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->modifySyncStatus(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    return-void
.end method

.method private deleteUserData()V
    .locals 4

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v0

    const/16 v1, 0x2715

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_USER_DATA_IN_SERVER:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->execute(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)I

    return-void
.end method

.method private downloadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;)V
    .locals 7

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadImageItem() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    sget-object v6, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mBaseSyncTaskObject:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v0

    const/16 v1, 0x2714

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->imageUrl:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;

    invoke-direct {v5, p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$5;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->download(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setServerData(): SYNC_STATUS_SET_IMAGE: download file failed. File: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " download failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleImageDownloadDoneStopped()V

    :cond_0
    monitor-exit v6

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Stopped Downloading..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleImageDownloadDoneStopped()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Stopped Downloading..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleImageDownloadDoneStopped()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/NetException;->printStackTrace()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;->filePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Stopped Downloading..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleImageDownloadDoneStopped()V

    goto/16 :goto_0
.end method

.method private downloadImages()V
    .locals 4

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mBaseSyncTaskObject:Ljava/lang/Object;

    monitor-enter v3

    const/4 v1, 0x0

    :try_start_0
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    move v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->MAX_PARALLEL_IMAGES_ALLOWED:I

    if-ne v2, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setServerData(): Reached max images to download parallelly"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v3

    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->downloadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;)V

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private handleDeleteUserData(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleDeleteUserData : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    return-void
.end method

.method private handleGetDetailHistory(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "handleGetDetailHistory()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Ljava/lang/Object;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private handleGetSyncHistory(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonSyntaxException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "handleDeleteServerData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGson:Lcom/google/gson/Gson;

    check-cast p1, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->serverTime:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncEndTime:J

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    const-string v3, "D"

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->historyStatus:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v3, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v1, v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getDetailHistoryData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getDetailHistory()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_1
.end method

.method private isExerciseRelatedTasks()Z
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseActivityTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseExtendedWalkInfoTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseFitnessTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseImageTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseInfoTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseLocationTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseManualTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseRealtimeTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExerciseWalkingTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FirstBeatCoachingResultTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FirstBeatCoachingTask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeLatestWalkInfoRowAsUpdated(JJ)V
    .locals 8

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-ltz v0, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v2, "Walkinfo record belongs to present day, so make latest record as updated"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select _id from walk_info where exercise__id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " order by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "start_time"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " desc limit 1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "sync_status"

    const v5, 0x29813

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "walk_info"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "Walkinfo record doesnt belongs to present day"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method private modifySyncStatus(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "modifySyncStatus()"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "sync_status"

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_EXERCISE_IMAGE:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error uploading exercise image with file path: \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "exercise_photo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file_path = \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_IMAGE:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error uploading food image with file path: \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, "meal_image"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file_path = \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    goto :goto_0
.end method

.method private uploadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    .locals 9

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "uploadImageItem()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v8, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mBaseSyncTaskObject:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v0

    const/16 v1, 0x2716

    iget-object v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->params:Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    iget-object v5, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    iget-object v6, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->entityName:Ljava/lang/String;

    new-instance v7, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;

    invoke-direct {v7, p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->upload(ILjava/lang/Object;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setServerData(): SYNC_STATUS_SET_IMAGE: upload file failed. File: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " upload file failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->modifySyncStatus(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->uploadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "uploadImageItem(): No images remaining to upload"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V

    :cond_1
    monitor-exit v8

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private uploadImages()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setServerData(): SYNC_STATUS_SET_IMAGE"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mBaseSyncTaskObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    iget v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->MAX_PARALLEL_IMAGES_ALLOWED:I

    if-ne v3, v4, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setServerData(): Reached max images to upload parallelly"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    monitor-exit v1

    return-void

    :cond_1
    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->uploadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setServerData(): SYNC_STATUS_SET_IMAGE: no modified data"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cancelTask()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "cancelTask()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->cancelRequest()V

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    return-void
.end method

.method public finishTask(I)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishTask(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->isExerciseRelatedTasks()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishTask(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " during exercise related tasks"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->onErrorDuringExerciseSync()V

    :cond_0
    if-nez p1, :cond_1

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncEndTime:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setLastSyncTime(J)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    iget v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDataSyncType:I

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->onFinished(II)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishTask(): erroCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "finishTask(): erroCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    return-void
.end method

.method protected abstract getDeletedLocalData()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation
.end method

.method protected getDetailHistory()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "getDetailHistory()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v1

    const/16 v2, 0x2714

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v3, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->uri:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->body:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0, p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->execute(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)I

    return-void
.end method

.method protected abstract getDetailHistoryData()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation
.end method

.method protected getSyncHistory()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "getSyncHistory()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetSyncHistory;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetSyncHistory;-><init>()V

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetSyncHistory;->startDate:Ljava/lang/String;

    const-string v1, "Y"

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetSyncHistory;->isReturningAll:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getSyncTypeString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetSyncHistory;->type:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v1

    const/16 v2, 0x2713

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_SYNC_HISTORY:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGson:Lcom/google/gson/Gson;

    invoke-virtual {v4, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0, p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->execute(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)I

    return-void
.end method

.method protected abstract getSyncTypeString()Ljava/lang/String;
.end method

.method protected abstract getTableName()Ljava/lang/String;
.end method

.method protected abstract getUnsyncedLocalData()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation
.end method

.method public getWeight()J
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "getWeight()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mTaskWeight:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x5

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mTaskWeight:J

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Weight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mTaskWeight:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mTaskWeight:J

    return-wide v0
.end method

.method protected handleDeleteServerData()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "handleDeleteServerData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getTableName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleDeleteServerData(Ljava/lang/String;)V

    return-void
.end method

.method protected handleDeleteServerData(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleDeleteServerData(): tableName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$3;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$3;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public handleImageDownloadDoneStopped()V
    .locals 2

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mMaxImageToDownload:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mMaxImageToDownload:I

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "downloadImageItem(): No images remainng to download"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mMaxImageToDownload:I

    iget v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->MAX_PARALLEL_IMAGES_ALLOWED:I

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$7;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$7;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method protected handleSetServerData(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "handleSetServerData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$2;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    const/4 v1, 0x0

    const/16 v8, 0x191

    const/4 v7, 0x5

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onExceptionReceived : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onExceptionReceived : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->isExerciseRelatedTasks()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onExceptionReceived(): Error occurred during exercise sync"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->onErrorDuringExerciseSync()V

    :cond_0
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGson:Lcom/google/gson/Gson;

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v1, v0

    :cond_1
    :goto_0
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mRequestType:I

    const v2, 0x1869f

    if-ne v0, v2, :cond_3

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v0

    const/16 v2, -0xe

    if-eq v0, v2, :cond_3

    if-eqz v1, :cond_2

    iget v0, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->httpStatus:I

    if-ne v0, v8, :cond_2

    const-string v0, "LIC_4104"

    iget-object v2, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/16 v0, 0x2711

    if-ne p3, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    :goto_1
    if-eqz v1, :cond_5

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth"

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->httpStatus:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->message:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v4, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_2
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v0

    const/4 v2, -0x3

    if-ne v0, v2, :cond_6

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    :goto_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->uri:Ljava/lang/String;

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth"

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    invoke-static {v2, v3, v4, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v0

    const/16 v2, 0x1f4

    if-ne v0, v2, :cond_7

    if-eqz v1, :cond_7

    iget-object v0, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->message:Ljava/lang/String;

    const-string v2, "\\W"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "duplicatedkeyerror"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, p3, v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->procedure(II)V

    goto :goto_3

    :cond_7
    if-eqz v1, :cond_8

    iget v0, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->httpStatus:I

    if-ne v0, v8, :cond_8

    const-string v0, "LIC_4104"

    iget-object v2, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "Samsung Account Token is not validate. request again"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Samsung Account Token is not validate. request again"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$1;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    goto :goto_3

    :cond_8
    if-eqz v1, :cond_b

    const-string v0, "PRO_5412"

    iget-object v2, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    :try_start_1
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_a

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getCountry()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    const-string v2, "XXX"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setCountry(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setCountry(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getHttpResCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getHttpResCode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :cond_b
    const/16 v0, 0x2712

    if-ne p3, v0, :cond_c

    invoke-virtual {p0, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_3

    :cond_c
    if-eqz v1, :cond_d

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->NEGATIVE:Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ExceptionObject;->code:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->isStatusCode(Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_3

    :cond_d
    invoke-virtual {p0, p3, v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->procedure(II)V

    goto/16 :goto_3

    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onRequestCancelled()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v5, 0x5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResponseReceived()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p4, :cond_0

    invoke-virtual {p0, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v2, "API-Response"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "response received, requestId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->end(J)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->dump()V

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SetDataTask : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v2, "eng"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->PrintLongString(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_1
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    :try_start_1
    invoke-virtual {p0, p4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleSetServerData(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/gson/JsonSyntaxException;->printStackTrace()V

    invoke-virtual {p0, p3, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->procedure(II)V

    goto :goto_0

    :catch_1
    move-exception v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v2, "Got out of memory error while printing response"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleDeleteServerData()V
    :try_end_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x7

    invoke-virtual {p0, p3, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->procedure(II)V

    goto/16 :goto_0

    :pswitch_2
    if-nez v0, :cond_3

    :try_start_3
    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleGetSyncHistory(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_0

    :pswitch_3
    if-nez v0, :cond_4

    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleGetDetailHistory(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getDetailHistory()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->downloadImages()V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_0

    :pswitch_4
    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleDeleteUserData(Ljava/lang/Object;)V
    :try_end_3
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected procedure(II)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "procedure(): privateId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". errorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "procedure(): remove first modified data in the list"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "procedure(): call setServerData(SYNC_STATUS_SET_DATA)"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iput p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mErrorWhileBackup:I

    const/16 v0, 0x2711

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "procedure(): remove first deleted data in the list"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "procedure(): call setServerData(SYNC_STATUS_DELETE_DATA)"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iput p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mErrorWhileBackup:I

    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public run()V
    .locals 1

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->runTask(Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;)V

    return-void
.end method

.method public runTask(Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;)V
    .locals 5

    const v4, 0x1869f

    const/16 v3, 0xd

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "runTask()"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mRequestType:I

    if-ne v0, v4, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getUnsyncedLocalData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-boolean v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->containFile:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getDeletedLocalData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :try_start_1
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mRequestType:I

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->getListenerObject()Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->getListenerObject()Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->getListenerObject()Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mListener:Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;->getListenerObject()Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/_ActivityListener;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "Shealth_SDK_Is_Caller"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ERR_SYNC_MANUAL"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;

    :goto_2
    const/16 v0, 0x2711

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :goto_3
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "Error occurred while doing db operations"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string v1, "Error occurred while doing getUnsyncedLocalData() or getDeletedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_3

    :cond_2
    :try_start_2
    const-string v0, "ERR_SYNC_AUTO"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_3

    :cond_3
    :try_start_3
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mRequestType:I

    const v1, 0x1869e

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getSyncHistory()V

    goto :goto_3

    :cond_4
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mRequestType:I

    const v1, 0x1869c

    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->deleteUserData()V

    goto :goto_3

    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method protected setLastSyncTime(J)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SET SYNC_TIME_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getSyncTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SYNC_TIME_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getSyncTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method

.method protected setServerData(I)V
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setServerData(): status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v1

    const/16 v2, 0x2711

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v3, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->uri:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->body:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0, p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->execute(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setServerData(): SYNC_STATUS_SET_DATA: upload data failed"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->uploadImages()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setServerData(): SYNC_STATUS_DELETE_DATA: deleted data size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    move-result-object v1

    const/16 v2, 0x2712

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v3, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->uri:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDeletedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->body:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0, p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->execute(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setServerData(): SYNC_STATUS_DELETE_DATA: deleted data failed"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v0, "profile"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getSyncTypeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "in_sync"

    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setProfileData(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mErrorWhileBackup:I

    if-nez v0, :cond_4

    invoke-virtual {p0, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setServerData(): SYNC_STATUS_DELETE_DATA: call finish with error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mErrorWhileBackup:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mErrorWhileBackup:I

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected skipGetSyncHistory()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "skipGetSyncHistory()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getDetailHistoryData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getDetailHistory()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_0
.end method

.method protected abstract updateItemsLocally(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected updateSyncStatusField(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;)V
    .locals 10

    const/4 v8, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    iget-boolean v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->isLongId:Z

    if-eqz v1, :cond_7

    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    const-string/jumbo v2, "realtime_data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    const-string v2, "location_data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    const-string/jumbo v2, "walk_info"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    :try_start_1
    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "exercise__id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->rowId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    if-eqz v6, :cond_1

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "exercise__id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const-string/jumbo v1, "sync_status"

    const v2, 0x29811

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exercise__id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v2, v9

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    iget-object v0, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    const-string/jumbo v1, "walk_info"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->updateTime:J

    invoke-direct {p0, v0, v1, v7, v8}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->makeLatestWalkInfoRowAsUpdated(JJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    if-eqz v6, :cond_2

    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_4
    :try_start_4
    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "update_time"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->rowId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v1, "update_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iget-wide v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->updateTime:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_6

    const-string/jumbo v1, "sync_status"

    const v2, 0x29811

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_2
    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->rowId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v2, v9

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    :cond_5
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v1, "sync_status"

    const v2, 0x29813

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    :cond_7
    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "update_time"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->rowStringId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string/jumbo v1, "update_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iget-wide v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->updateTime:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_9

    const-string/jumbo v1, "sync_status"

    const v2, 0x29811

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_3
    iget-object v1, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->tableName:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->rowStringId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v2, v9

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    :cond_8
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v1, "sync_status"

    const v2, 0x29813

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto/16 :goto_1
.end method
