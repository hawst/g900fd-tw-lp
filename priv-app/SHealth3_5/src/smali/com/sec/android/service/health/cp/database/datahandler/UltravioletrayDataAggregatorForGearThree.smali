.class public Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;
.super Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    return-void
.end method


# virtual methods
.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 7

    const/4 v1, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "addData[] for UltravioletRays"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    :goto_0
    :try_start_0
    array-length v0, p1

    if-ge v2, v0, :cond_3

    aget-object v0, p1, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " _UvRay "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " time "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->checkMandatoryFields()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->getContentValues()Landroid/content/ContentValues;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v3, :cond_2

    const-string v3, "application__id"

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "hdid = \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->devicePkId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while inserting into db"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_2
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_2
.end method

.method protected checkMandatoryFields()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getContentValues()Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    const-string/jumbo v1, "uv_index"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    const-string/jumbo v1, "uv_index"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "sample_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "hdid"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->devicePkId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method getTableUri()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 4

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    if-eqz v0, :cond_0

    const-string v0, "UvRay_DATA"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
