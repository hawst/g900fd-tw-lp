.class public Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
.super Ljava/lang/Object;
.source "ConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;
    }
.end annotation


# static fields
.field public static CONNECTION_MANAGER_DEBUG:Ljava/lang/String; = null

.field private static final CORE_POOL_SIZE:I = 0x10

.field private static final KEEP_ALIVE:I = 0x1e

.field private static final MAXIMUM_POOL_SIZE:I = 0x10


# instance fields
.field private AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private TAG:Ljava/lang/String;

.field private downloadSynchronization:Ljava/lang/Object;

.field private downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

.field private isOneTimeDemoMode:Z

.field rejectExecutionHandler:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;

.field public final requestExecutor:Ljava/util/concurrent/Executor;

.field private requestId:J

.field private requestSynchronization:Ljava/lang/Object;

.field private final sPoolWorkQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final sThreadFactory:Ljava/util/concurrent/ThreadFactory;

.field private uploadSynchronization:Ljava/lang/Object;

.field private uploaderMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-string v0, "CON_MGR"

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 8
    .param p1, "TAG"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x10

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-wide/16 v2, 0x1

    iput-wide v2, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestId:J

    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestSynchronization:Ljava/lang/Object;

    .line 48
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadSynchronization:Ljava/lang/Object;

    .line 53
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploadSynchronization:Ljava/lang/Object;

    .line 59
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 66
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$1;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    .line 75
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->sPoolWorkQueue:Ljava/util/concurrent/BlockingQueue;

    .line 80
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x1e

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->sPoolWorkQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestExecutor:Ljava/util/concurrent/Executor;

    .line 103
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;

    invoke-direct {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->rejectExecutionHandler:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;

    .line 112
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->TAG:Ljava/lang/String;

    .line 113
    return-void
.end method

.method private execute(Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 3
    .param p1, "AAT"    # Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    .param p2, "requestparamameters"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 369
    if-eqz p1, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequestId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestExecutor:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {p1, v1, v0}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 374
    :cond_0
    return-void
.end method

.method private getAbstractASyncTaskwithMethodType(Lcom/sec/android/service/health/connectionmanager2/MethodType;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    .locals 1
    .param p1, "type"    # Lcom/sec/android/service/health/connectionmanager2/MethodType;
    .param p2, "requestParamameters"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 414
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    if-ne p1, v0, :cond_0

    .line 416
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    .line 430
    :goto_0
    return-object v0

    .line 418
    :cond_0
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->DELETE:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    if-ne p1, v0, :cond_1

    .line 420
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/DeleteAsyncTask;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/service/health/connectionmanager2/DeleteAsyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    goto :goto_0

    .line 422
    :cond_1
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    if-ne p1, v0, :cond_2

    .line 424
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/service/health/connectionmanager2/PostAsyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    goto :goto_0

    .line 426
    :cond_2
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/MethodType;->PUT:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    if-ne p1, v0, :cond_3

    .line 428
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/PutAsyncTask;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/service/health/connectionmanager2/PutAsyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    goto :goto_0

    .line 430
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized getRequestId()J
    .locals 4

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestId:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestId:J

    .line 125
    iget-wide v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public cancelRequest(Ljava/lang/Object;)V
    .locals 6
    .param p1, "requester"    # Ljava/lang/Object;

    .prologue
    .line 473
    iget-object v5, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestSynchronization:Ljava/lang/Object;

    monitor-enter v5

    .line 475
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    .line 476
    .local v3, "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 477
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 479
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;

    .line 480
    .local v0, "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->getRequestParamameters()Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    move-result-object v2

    .line 481
    .local v2, "requestparameters":Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequester()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 483
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->cancel(Z)Z

    goto :goto_0

    .line 486
    .end local v0    # "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    .end local v2    # "requestparameters":Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .end local v3    # "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    .restart local v3    # "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487
    return-void
.end method

.method public cancelRequest(Ljava/lang/Object;I)Z
    .locals 7
    .param p1, "requester"    # Ljava/lang/Object;
    .param p2, "privateId"    # I

    .prologue
    const/4 v4, 0x0

    .line 445
    if-nez p1, :cond_0

    .line 462
    :goto_0
    return v4

    .line 448
    :cond_0
    iget-object v5, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestSynchronization:Ljava/lang/Object;

    monitor-enter v5

    .line 449
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    .line 450
    .local v3, "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 451
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 452
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;

    .line 453
    .local v0, "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->getRequestParamameters()Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    move-result-object v2

    .line 455
    .local v2, "requestparameters":Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getPrivateId()I

    move-result v6

    if-ne v6, p2, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getRequester()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 458
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->cancel(Z)Z

    move-result v4

    monitor-exit v5

    goto :goto_0

    .line 461
    .end local v0    # "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    .end local v2    # "requestparameters":Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .end local v3    # "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    .restart local v3    # "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :cond_2
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method cleanupSession()V
    .locals 0

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->clearAllRequest()V

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->stopAllDownload()V

    .line 517
    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->stopAllUpload()V

    .line 526
    return-void
.end method

.method public clearAllRequest()V
    .locals 5

    .prologue
    .line 494
    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestSynchronization:Ljava/lang/Object;

    monitor-enter v4

    .line 496
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    .line 497
    .local v2, "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 498
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 500
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;

    .line 501
    .local v0, "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;->cancel(Z)Z

    goto :goto_0

    .line 504
    .end local v0    # "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    .end local v2    # "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 503
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    .restart local v2    # "tasks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;>;"
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 504
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 505
    return-void
.end method

.method public download(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;)J
    .locals 7
    .param p1, "privateId"    # I
    .param p2, "listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;
    .param p3, "apiWithParams"    # Ljava/lang/String;
    .param p4, "outputStream"    # Ljava/io/OutputStream;
    .param p5, "tag"    # Ljava/lang/Object;

    .prologue
    .line 559
    if-nez p3, :cond_0

    .line 561
    const-wide/16 v0, 0x0

    .line 563
    :goto_0
    return-wide v0

    :cond_0
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->download(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public download(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)J
    .locals 10
    .param p1, "privateId"    # I
    .param p2, "listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;
    .param p3, "apiWithParams"    # Ljava/lang/String;
    .param p4, "outputStream"    # Ljava/io/OutputStream;
    .param p5, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;",
            "Ljava/lang/String;",
            "Ljava/io/OutputStream;",
            "Ljava/lang/Object;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 587
    .local p6, "headerNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 589
    :cond_0
    const-wide/16 v1, 0x0

    .line 600
    :goto_0
    return-wide v1

    .line 591
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getRequestId()J

    move-result-wide v1

    .line 592
    .local v1, "newId":J
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getServerIp()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v4}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getPort()I

    move-result v4

    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v6}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getIsHttpsEnabled()Z

    move-result v6

    invoke-static {v3, v4, v6, p3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerUtils;->makeUrl(Ljava/lang/String;IZLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 593
    .local v5, "Url":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    move v3, p1

    move-object v4, p2

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;-><init>(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 594
    .local v0, "DP":Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;
    new-instance v9, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)V

    .line 595
    .local v9, "DAT":Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadSynchronization:Ljava/lang/Object;

    monitor-enter v4

    .line 597
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 599
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestExecutor:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    invoke-virtual {v9, v3, v4}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 598
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public downloadPublicUrl(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)J
    .locals 10
    .param p1, "privateId"    # I
    .param p2, "listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;
    .param p3, "apiWithParams"    # Ljava/lang/String;
    .param p4, "outputStream"    # Ljava/io/OutputStream;
    .param p5, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;",
            "Ljava/lang/String;",
            "Ljava/io/OutputStream;",
            "Ljava/lang/Object;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 625
    .local p6, "headerNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 627
    :cond_0
    const-wide/16 v1, 0x0

    .line 637
    :goto_0
    return-wide v1

    .line 629
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getRequestId()J

    move-result-wide v1

    .line 630
    .local v1, "newId":J
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;-><init>(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 631
    .local v0, "DP":Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;
    new-instance v9, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)V

    .line 632
    .local v9, "DAT":Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadSynchronization:Ljava/lang/Object;

    monitor-enter v4

    .line 634
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 635
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestExecutor:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v9, v3, v4}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 635
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method protected downloadTaskCompleted(J)V
    .locals 2
    .param p1, "RequestId"    # J

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 531
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->releaseConnection()V

    .line 536
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 537
    return-void
.end method

.method protected getHttpClientManager()Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    return-object v0
.end method

.method public initConnectionManager(IIZLjava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "connectionTimeout"    # I
    .param p2, "readTimeout"    # I
    .param p3, "httpsenabled"    # Z
    .param p4, "keyName"    # Ljava/lang/String;
    .param p5, "keyStorePassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 206
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->initHttpClientManager(IIZLjava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestExecutor:Ljava/util/concurrent/Executor;

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->rejectExecutionHandler:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->setRejectedExecutionHandler(Ljava/util/concurrent/RejectedExecutionHandler;)V
    :try_end_0
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    const/4 v0, 0x1

    .line 213
    :goto_0
    return v0

    .line 210
    :catch_0
    move-exception v6

    .line 212
    .local v6, "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->TAG:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initConnectionManager(ZLjava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "httpsenabled"    # Z
    .param p2, "keyName"    # Ljava/lang/String;
    .param p3, "keyStorePassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 157
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    if-nez v1, :cond_0

    .line 159
    new-instance v1, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-direct {v1}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 163
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->initHttpClientManager(ZLjava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestExecutor:Ljava/util/concurrent/Executor;

    check-cast v1, Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->rejectExecutionHandler:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager$ImpRejectedExecutionHandler;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->setRejectedExecutionHandler(Ljava/util/concurrent/RejectedExecutionHandler;)V
    :try_end_0
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    const/4 v1, 0x1

    .line 170
    :goto_0
    return v1

    .line 167
    :catch_0
    move-exception v0

    .line 169
    .local v0, "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J
    .locals 17
    .param p1, "requester"    # Ljava/lang/Object;
    .param p2, "privateId"    # I
    .param p3, "type"    # Lcom/sec/android/service/health/connectionmanager2/MethodType;
    .param p4, "api"    # Ljava/lang/String;
    .param p5, "params"    # Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    .param p6, "data"    # Ljava/lang/String;
    .param p7, "handler"    # Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .param p8, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I",
            "Lcom/sec/android/service/health/connectionmanager2/MethodType;",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/connectionmanager2/RequestParam;",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;",
            "Ljava/lang/Object;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 327
    .local p9, "headervalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    if-eqz p4, :cond_0

    if-nez p7, :cond_1

    .line 329
    :cond_0
    const-wide/16 v3, 0x0

    .line 356
    :goto_0
    return-wide v3

    .line 332
    :cond_1
    const/4 v10, 0x0

    .line 333
    .local v10, "stringEntity":Lorg/apache/http/entity/StringEntity;
    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    move-object/from16 v0, p3

    if-eq v0, v5, :cond_2

    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/MethodType;->PUT:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    move-object/from16 v0, p3

    if-ne v0, v5, :cond_3

    .line 335
    :cond_2
    if-eqz p6, :cond_3

    .line 339
    :try_start_0
    new-instance v16, Lorg/apache/http/entity/StringEntity;

    const-string v5, "UTF-8"

    move-object/from16 v0, v16

    move-object/from16 v1, p6

    invoke-direct {v0, v1, v5}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    .end local v10    # "stringEntity":Lorg/apache/http/entity/StringEntity;
    .local v16, "stringEntity":Lorg/apache/http/entity/StringEntity;
    :try_start_1
    const-string v5, "application/json;charset=utf-8"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v10, v16

    .line 350
    .end local v16    # "stringEntity":Lorg/apache/http/entity/StringEntity;
    .restart local v10    # "stringEntity":Lorg/apache/http/entity/StringEntity;
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getRequestId()J

    move-result-wide v3

    .line 351
    .local v3, "newId":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v5}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getServerIp()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v6}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getPort()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getIsHttpsEnabled()Z

    move-result v7

    invoke-static/range {p4 .. p5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->makeApiWithParam(Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v6, v7, v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerUtils;->makeUrl(Ljava/lang/String;IZLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 352
    .local v8, "Url":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->isOneTimeDemoMode:Z

    move/from16 v5, p2

    move-object/from16 v6, p1

    move-object/from16 v7, p7

    move-object/from16 v9, p3

    move-object/from16 v11, p8

    move-object/from16 v13, p9

    invoke-direct/range {v2 .. v13}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;-><init>(JILjava/lang/Object;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/MethodType;Lorg/apache/http/HttpEntity;Ljava/lang/Object;ZLjava/util/HashMap;)V

    .line 353
    .local v2, "requestparamameters":Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->isOneTimeDemoMode:Z

    .line 354
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getAbstractASyncTaskwithMethodType(Lcom/sec/android/service/health/connectionmanager2/MethodType;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;

    move-result-object v14

    .line 355
    .local v14, "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->execute(Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    goto :goto_0

    .line 342
    .end local v2    # "requestparamameters":Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .end local v3    # "newId":J
    .end local v8    # "Url":Ljava/lang/String;
    .end local v14    # "AAT":Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
    :catch_0
    move-exception v15

    .line 344
    .local v15, "ex":Ljava/io/UnsupportedEncodingException;
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->TAG:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const-wide/16 v3, 0x0

    goto :goto_0

    .line 342
    .end local v10    # "stringEntity":Lorg/apache/http/entity/StringEntity;
    .end local v15    # "ex":Ljava/io/UnsupportedEncodingException;
    .restart local v16    # "stringEntity":Lorg/apache/http/entity/StringEntity;
    :catch_1
    move-exception v15

    move-object/from16 v10, v16

    .end local v16    # "stringEntity":Lorg/apache/http/entity/StringEntity;
    .restart local v10    # "stringEntity":Lorg/apache/http/entity/StringEntity;
    goto :goto_1
.end method

.method public setAddress(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "aAddress"    # Ljava/lang/String;
    .param p2, "aPort"    # I

    .prologue
    .line 233
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddressWithoutLookUp(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public setAddressWithoutLookUp(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "aAddress"    # Ljava/lang/String;
    .param p2, "aPort"    # I

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->setServerIp(Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->setPort(I)V

    .line 299
    const/4 v0, 0x1

    .line 301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopAllDownload()V
    .locals 5

    .prologue
    .line 651
    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadSynchronization:Ljava/lang/Object;

    monitor-enter v4

    .line 653
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 654
    .local v1, "downloaders":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 655
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 657
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;

    .line 658
    .local v0, "downloader":Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->stopDownload(Lcom/sec/android/service/health/connectionmanager2/NetException;)V

    goto :goto_0

    .line 662
    .end local v0    # "downloader":Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
    .end local v1    # "downloaders":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;>;"
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 661
    .restart local v1    # "downloaders":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;>;"
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;>;"
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 662
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 663
    return-void
.end method

.method public stopAllUpload()V
    .locals 5

    .prologue
    .line 751
    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploadSynchronization:Ljava/lang/Object;

    monitor-enter v4

    .line 753
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    .line 754
    .local v2, "uploaders":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 755
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 757
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;

    .line 758
    .local v1, "uploader":Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->stopUpload(Lcom/sec/android/service/health/connectionmanager2/NetException;)V

    goto :goto_0

    .line 761
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;>;"
    .end local v1    # "uploader":Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
    .end local v2    # "uploaders":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 760
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;>;"
    .restart local v2    # "uploaders":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;>;"
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 761
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 762
    return-void
.end method

.method public stopDownload(J)Z
    .locals 1
    .param p1, "requestId"    # J

    .prologue
    .line 676
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->stopDownload(JLcom/sec/android/service/health/connectionmanager2/NetException;)Z

    move-result v0

    return v0
.end method

.method public stopDownload(JLcom/sec/android/service/health/connectionmanager2/NetException;)Z
    .locals 4
    .param p1, "requestId"    # J
    .param p3, "e"    # Lcom/sec/android/service/health/connectionmanager2/NetException;

    .prologue
    .line 693
    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadSynchronization:Ljava/lang/Object;

    monitor-enter v2

    .line 695
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;

    .line 696
    .local v0, "downloader":Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
    monitor-exit v2

    .line 698
    if-nez v0, :cond_0

    .line 700
    const/4 v1, 0x0

    .line 704
    :goto_0
    return v1

    .line 696
    .end local v0    # "downloader":Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 703
    .restart local v0    # "downloader":Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
    :cond_0
    invoke-virtual {v0, p3}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->stopDownload(Lcom/sec/android/service/health/connectionmanager2/NetException;)V

    .line 704
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public stopUpload(J)Z
    .locals 1
    .param p1, "requestId"    # J

    .prologue
    .line 775
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->stopUpload(JLcom/sec/android/service/health/connectionmanager2/NetException;)Z

    move-result v0

    return v0
.end method

.method public stopUpload(JLcom/sec/android/service/health/connectionmanager2/NetException;)Z
    .locals 4
    .param p1, "requestId"    # J
    .param p3, "e"    # Lcom/sec/android/service/health/connectionmanager2/NetException;

    .prologue
    .line 792
    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploadSynchronization:Ljava/lang/Object;

    monitor-enter v2

    .line 794
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;

    .line 795
    .local v0, "uploader":Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
    monitor-exit v2

    .line 797
    if-nez v0, :cond_0

    .line 799
    const/4 v1, 0x0

    .line 803
    :goto_0
    return v1

    .line 795
    .end local v0    # "uploader":Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 802
    .restart local v0    # "uploader":Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
    :cond_0
    invoke-virtual {v0, p3}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->stopUpload(Lcom/sec/android/service/health/connectionmanager2/NetException;)V

    .line 803
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected taskCompleted(J)V
    .locals 2
    .param p1, "RequestId"    # J

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->AsyncTaskHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    return-void
.end method

.method public upload(ILcom/sec/android/service/health/connectionmanager2/IOnUploadListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)J
    .locals 12
    .param p1, "privateId"    # I
    .param p2, "listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;
    .param p3, "apiWithParams"    # Ljava/lang/String;
    .param p4, "completefilepath"    # Ljava/lang/String;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "fileName"    # Ljava/lang/String;
    .param p7, "entityName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 728
    .local p8, "headervalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 730
    :cond_0
    const-wide/16 v1, 0x0

    .line 741
    :goto_0
    return-wide v1

    .line 732
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getRequestId()J

    move-result-wide v1

    .line 733
    .local v1, "newId":J
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getServerIp()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v4}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getPort()I

    move-result v4

    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v6}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getIsHttpsEnabled()Z

    move-result v6

    invoke-static {v3, v4, v6, p3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerUtils;->makeUrl(Ljava/lang/String;IZLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 734
    .local v5, "Url":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move v3, p1

    move-object v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;-><init>(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 735
    .local v0, "UpParams":Lcom/sec/android/service/health/connectionmanager2/UploadParameters;
    new-instance v11, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;

    invoke-direct {v11, p0, v0}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)V

    .line 736
    .local v11, "UAT":Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
    iget-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploadSynchronization:Ljava/lang/Object;

    monitor-enter v4

    .line 738
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6, v11}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 739
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 740
    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->requestExecutor:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    invoke-virtual {v11, v3, v4}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 739
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method protected uploadTaskCompleted(J)V
    .locals 2
    .param p1, "RequestId"    # J

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploaderMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    return-void
.end method
