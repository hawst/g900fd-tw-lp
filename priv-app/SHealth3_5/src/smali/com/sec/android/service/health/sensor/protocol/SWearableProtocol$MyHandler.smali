.class final Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MyHandler"
.end annotation


# instance fields
.field mWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/os/Looper;Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;->mWeakRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14

    const-wide/16 v1, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x1

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;->mWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v2, "RECEIVE_DATA"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    const-string v1, "STRESS"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "SLEEP"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    const-string v3, "PEDO"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    const-string v4, "COACHINGVAR"

    invoke-virtual {v11, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    const-string v5, "EXERCISE"

    invoke-virtual {v11, v5}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v5

    const-string v6, "HRM"

    invoke-virtual {v11, v6}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    const-string v7, "UVRAY"

    invoke-virtual {v11, v7}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v7

    iget-object v12, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v12, :cond_13

    sget-object v12, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v13, "RECEIVE_DATA : use aggregator"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_12

    array-length v12, v1

    if-lez v12, :cond_12

    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v12, "RECEIVE_DATA : stress_data"

    invoke-static {v9, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v9, :cond_11

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v9

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    iget-object v12, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v1, v8, v12}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_11

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v9, "error happen while inserting DB : stress_data"

    invoke-static {v1, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    const-string/jumbo v1, "stress_data"

    :goto_1
    move-object v9, v1

    move v1, v10

    :goto_2
    if-eqz v2, :cond_10

    array-length v12, v2

    if-lez v12, :cond_10

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v12, "RECEIVE_DATA : sleep_data"

    invoke-static {v1, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_f

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v12

    move-object v1, v2

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v1, v8, v2}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "error happen while inserting DB : sleep_data"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    const-string/jumbo v1, "sleep_data"

    :goto_3
    move-object v2, v1

    move v9, v10

    :goto_4
    if-eqz v3, :cond_2

    array-length v1, v3

    if-lez v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v12, "RECEIVE_DATA : pedo_data"

    invoke-static {v1, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v12

    move-object v1, v3

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v1, v8, v3}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "error happen while inserting DB : pedo_data"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    const-string/jumbo v2, "pedo_data"

    :cond_2
    if-eqz v4, :cond_3

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v3, "RECEIVE_DATA : coaching_data"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v3

    move-object v1, v4

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v8, v4}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "error happen while inserting DB : coaching_data"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    const-string v2, "coaching_data"

    :cond_3
    if-eqz v5, :cond_e

    array-length v1, v5

    if-lez v1, :cond_e

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v3, "RECEIVE_DATA : rec_data"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_d

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v3

    move-object v1, v5

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v8, v4}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "error happen while inserting DB : rec_data"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    const-string/jumbo v1, "rec_data"

    :goto_5
    move-object v2, v1

    move v1, v10

    :goto_6
    if-eqz v6, :cond_4

    array-length v3, v6

    if-lez v3, :cond_4

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v3, "RECEIVE_DATA : hrm_data"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_c

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v3

    move-object v1, v6

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v8, v4}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "error happen while inserting DB : hrm_data"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    const-string v1, "hrm_data"

    :goto_7
    move-object v2, v1

    move v1, v10

    :cond_4
    if-eqz v7, :cond_5

    array-length v3, v7

    if-lez v3, :cond_5

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v3, "RECEIVE_DATA : uv_ray"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_b

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v3

    move-object v1, v7

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    check-cast v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v8, v4}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->addDataToDb([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "error happen while inserting DB : uv_ray"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    const-string/jumbo v1, "uv_ray"

    :goto_8
    move-object v2, v1

    move v1, v10

    :cond_5
    :goto_9
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const/16 v4, 0x1389

    iput v4, v3, Landroid/os/Message;->what:I

    const-string v4, "VERSION"

    invoke-virtual {v11, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "VERSION"

    const-string v6, "VERSION"

    invoke-virtual {v11, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    const-string v5, "DEVICE"

    const-string v6, "DEVICE"

    invoke-virtual {v11, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "DB_INSERT_ERROR_DATA_NAME"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "NOTIFICATION_FLAG"

    invoke-virtual {v4, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :cond_6
    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$100(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;->sendMessage(Landroid/os/Message;)Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SYNC_ERROR in handler send message"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "action_gear : DB insert finished"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "[HS_Wearable_sync]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "insert in send_response message"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v3, "VERSION"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "VERSION"

    invoke-virtual {v5, v3, v1, v2}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v3

    const-string v1, "DEVICE"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "DB_INSERT_ERROR_DATA_NAME"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_a
    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->coaching_response(DLjava/lang/String;)V
    invoke-static {v0, v3, v4, v1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$300(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;DLjava/lang/String;)V

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$400(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Landroid/os/Looper;

    move-result-object v1

    if-eqz v1, :cond_8

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$100(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    move-result-object v1

    if-eqz v1, :cond_8

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mThread quit"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$400(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    :cond_8
    iget-object v3, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-object v1, v8

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-interface {v3, v1, v8}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$000(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)I

    move-result v1

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sendDataUpdatedToApp(II)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$500(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;II)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "[HS_Wearable_sync]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "action_gear : SEND_RESPONSE_END_SYNC"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$000(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)I

    move-result v1

    if-nez v1, :cond_9

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v9}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    goto/16 :goto_0

    :cond_9
    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$000(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)I

    move-result v1

    if-ne v1, v10, :cond_0

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v10}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error happen doing insertDB : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v0, v9}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I

    goto/16 :goto_0

    :cond_a
    move-wide v3, v1

    move-object v2, v8

    move-object v1, v8

    goto/16 :goto_a

    :cond_b
    move-object v1, v2

    goto/16 :goto_8

    :cond_c
    move-object v1, v2

    goto/16 :goto_7

    :cond_d
    move-object v1, v2

    goto/16 :goto_5

    :cond_e
    move v1, v9

    goto/16 :goto_6

    :cond_f
    move-object v1, v9

    goto/16 :goto_3

    :cond_10
    move-object v2, v9

    move v9, v1

    goto/16 :goto_4

    :cond_11
    move-object v1, v8

    goto/16 :goto_1

    :cond_12
    move v1, v9

    move-object v9, v8

    goto/16 :goto_2

    :cond_13
    move-object v2, v8

    move v1, v9

    goto/16 :goto_9

    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
