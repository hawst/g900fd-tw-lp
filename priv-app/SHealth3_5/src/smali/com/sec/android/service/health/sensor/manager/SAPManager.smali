.class public Lcom/sec/android/service/health/sensor/manager/SAPManager;
.super Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

# interfaces
.implements Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;
    }
.end annotation


# static fields
.field public static final SAP_CLOSE_SESSION_TIME_OUT_ERROR:I = 0x6c

.field public static final SAP_CONNECTION_ERROR:I = 0x66

.field public static final SAP_CONNECTION_NO_RESPONSE_ERROR:I = 0x6e

.field public static final SAP_OPEN_SESSION_IN_PROGRESS_ERROR:I = 0x6a

.field public static final SAP_OPEN_SESSION_TIME_OUT_ERROR:I = 0x6b

.field public static final SAP_SENSOR_NOT_PAIRED_ERROR:I = 0x68

.field public static final SAP_SENSOR_SEARCH_FAILED_ERROR:I = 0x69

.field public static final SAP_UNKNOWN_ERROR:I = 0x64

.field public static final SAP_WRITE_MESSAGE_ERROR:I = 0x6d

.field public static final SAP_WRONG_DATA_FORMAT:I = 0x67

.field private static final TRANSPORT_BLE_STRING:Ljava/lang/String; = "TRANSPORT_BLE"

.field private static final TRANSPORT_BT_STRING:Ljava/lang/String; = "TRANSPORT_BT"

.field private static final TRANSPORT_NFC_STRING:Ljava/lang/String; = "TRANSPORT_NFC"

.field private static final TRANSPORT_WIFI_STRING:Ljava/lang/String; = "TRANSPORT_WIFI"

.field private static mSapManager:Lcom/sec/android/service/health/sensor/manager/SAPManager;


# instance fields
.field private TAG:Ljava/lang/String;

.field private appcessoryDeviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/SAPManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSapManager:Lcom/sec/android/service/health/sensor/manager/SAPManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;-><init>()V

    const-string v0, "[HealthSensor]SAPManager"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->appcessoryDeviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/manager/SAPManager;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private addSAPManagerAppcessoryDevice(Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->appcessoryDeviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v1, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->appcessoryId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private convertToDevice(Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 4

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-wide v2, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->appcessoryId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->address:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->address:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->name:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setName(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setDeviceOriginalName(Ljava/lang/String;)V

    const-string v0, "Samsung"

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    :cond_2
    iget v0, p1, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->transportId:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setConnectivityType(I)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getProfileHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setType(I)V

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setDataType(Ljava/util/List;)V

    :cond_3
    move-object v0, v1

    goto :goto_0

    :pswitch_1
    const-string v0, "TRANSPORT_BT"

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setProtocol(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    const-string v0, "TRANSPORT_BLE"

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setProtocol(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    const-string v0, "TRANSPORT_NFC"

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setProtocol(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    const-string v0, "TRANSPORT_WIFI"

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setProtocol(Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/manager/SAPManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSapManager:Lcom/sec/android/service/health/sensor/manager/SAPManager;

    return-object v0
.end method

.method private getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->appcessoryDeviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    return-object v0
.end method

.method private removeSAPManagerAppcessoryDevice(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->appcessoryDeviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public deinitialize()V
    .locals 1

    invoke-super {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deinitialize()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->appcessoryDeviceHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    return-void
.end method

.method public getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSHealthSensorDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDeviceOriginalName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDeviceOriginalName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "HRM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 4

    const/16 v3, 0x12

    invoke-super {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init android.os.Build.VERSION.SDK_INT = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-wide/16 v1, 0x6

    invoke-static {v0, v1, v2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->getDefaultManager(Landroid/content/Context;J)Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "initialize mSAPAccManager  getDefaultManager with TRANSPORT_BLE|TRANSPORT_BT"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const/4 v1, 0x4

    invoke-virtual {v0, p0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->registerEventListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;I)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-wide/16 v1, 0x2

    invoke-static {v0, v1, v2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->getDefaultManager(Landroid/content/Context;J)Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "initialize mSAPAccManager  getDefaultManager with TRANSPORT_BT"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialize mSAPAccManager  getDefaultManager registerEventListener is skipped for android.os.Build.VERSION.SDK_INT = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->getRejoin()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->isConnected(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataTypeSupported(I)Z
    .locals 1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_0
    .end sparse-switch
.end method

.method public join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 4

    const/4 v3, 0x1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "join no Sensor Device Found in the AbstractSensorManager"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "join called - device name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->isConnected(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v1, v0, v3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->openConnection(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "join - openConnection returned:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "join - openConnection called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "join - Appcessory device not found"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto :goto_0
.end method

.method public leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 5

    const/4 v4, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "leave called no Sensor Device Found in the AbstractSensorManager"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "leave called - device name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "leave called - : mSAPAccManager.isConnected() status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v3, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->isConnected(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->getSessionId()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->setRejoin(Z)V

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->addSAPManagerAppcessoryDevice(Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->closeSession(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v2, "leave - closeSession Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->closeConnection(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "leave - closeConnection Failed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->removeProfileHandlerController(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->removeSensorDevice(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->removeSAPManagerAppcessoryDevice(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "leave called no AppcessoryDevice Found in the AbstractSensorManager "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto/16 :goto_0
.end method

.method public onAccessoryAttached(J)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onAccessoryAttached"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    sget-byte v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->PAYLOAD_BINARY:B

    invoke-virtual {v1, v0, v2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->openSession(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;B)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onAccessoryAttached openSession called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onAccessoryDetached(J)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onAccessoryDetached - accessoryId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->getRejoin()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onAccessoryDetached reJoin True So returning without deleting the profileHandler Controller and SensorDevice"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onCloseSessionRequest(JJ)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCloseSessionRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDeviceFound(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)V
    .locals 4

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    const-string v1, "HRM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "[SAP][CON][SAPEventHandler] onDeviceFound"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceFound myDevice.appcessoryId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceFound myDevice.address = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceFound myDevice.name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceFound myDevice.transportId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->transportId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDeviceFound mDiscoveredDevicesList.add(myDevice)"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-wide v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-wide v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->getRejoin()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SAP onDeviceFound myDevice.appcessoryId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Already AutoConnection Enabled So not sending to Application"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    invoke-direct {v0, p1}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;-><init>(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)V

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->convertToDevice(Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->addSAPManagerAppcessoryDevice(Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->addSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    goto :goto_0
.end method

.method public onError(III)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onError - transportId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", accID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", errCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    int-to-long v0, p2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onError - Device is NULL so returning"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    packed-switch p3, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onError - Unknown Error So Returning "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onError - SENSOR_SEARCH_FAILED_ERROR : now stop searching"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->stopScanning()Z

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onError - SAP_CLOSE_SESSION_TIME_OUT_ERROR "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x66
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onOpenSessionAccepted(JJ)V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onOpenSessionAccepted"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSAPManagerAppcessoryDevice(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->getRejoin()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onOpenSessionAccepted ReJoin is true So returning without Deleting the profile Handler and sensorDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->setSessionId(J)V

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->addSAPManagerAppcessoryDevice(Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onOpenSessionAccepted SAP HRM profile handler"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "MANAGER"

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "DEVICE"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "SESSION"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->createDeviceHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onOpenSessionAccepted sessionID  after createDeviceHandler"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    long-to-int v5, p3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/sec/android/service/health/sensor/manager/SAPManager$1DelayThread;

    invoke-direct {v2, p0}, Lcom/sec/android/service/health/sensor/manager/SAPManager$1DelayThread;-><init>(Lcom/sec/android/service/health/sensor/manager/SAPManager;)V

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/manager/SAPManager$1DelayThread;->start()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->setRejoin(Z)V

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->setSessionId(J)V

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->addSAPManagerAppcessoryDevice(Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto :goto_0
.end method

.method public onOpenSessionRequest(JJ)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onOpenSessionRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onPairedStatus(IJZ)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onPairedStatus - isPaired : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSessionClosed(JJ)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onSessionClosed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onTransportStatusChanged(II)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onTransportStatusChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected remoteCallBackDied(Ljava/lang/Integer;)V
    .locals 0

    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "request called "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->request(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    return v0
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startReceivingData .. "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v4, p5

    move-wide v5, p6

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->startReceivingData(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public startScanning(Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "SAPManager startScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "BT is Not Enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startScanning mDiscoveredDevicesList.clear()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->startDiscovery(I)Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startScanning mSAPAccManager.startDiscoveryrtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)I
    .locals 1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopScanning()Z
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    const-string v1, "SAPManager stopScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->stopDiscovery(I)Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopScanning for SAP Devices success? : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method
