.class public Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;
.super Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final PROFILE_NAME:Ljava/lang/String; = "usb"


# instance fields
.field private infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

.field private isPingCommandMode:Z

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

.field private mUsbDevice:Landroid/hardware/usb/UsbDevice;

.field pingReplied:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;-><init>()V

    iput-object v8, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, ""

    const-string v2, "Healthpro"

    const/4 v3, 0x2

    const/16 v4, 0x2714

    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->getProtocolName()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v8, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    iput-boolean v7, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->isPingCommandMode:Z

    iput-boolean v7, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->pingReplied:Z

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol$1;-><init>(Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->LOG_TAG:Ljava/lang/String;

    const-string v1, "InfopiaProtocol deinitialize called!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public getBaudRate()I
    .locals 1

    const/16 v0, 0x12c0

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    return-object v0
.end method

.method public getProtocolName()Ljava/lang/String;
    .locals 1

    const-string v0, "infopia"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 3

    const/4 v2, 0x0

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->setProtocolListener(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;)V

    if-eqz p4, :cond_0

    check-cast p4, Landroid/hardware/usb/UsbDevice;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setConnectedState(Z)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "INFOPIA_GLUCOSE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "Infopia"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    :cond_1
    return v2
.end method

.method public isCommandMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->isPingCommandMode:Z

    return v0
.end method

.method public notifyRawDataReceived([B)I
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    array-length v3, p1

    if-lez v3, :cond_3

    add-int/2addr v2, v3

    aget-byte v2, p1, v1

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->S:[B

    aget-byte v3, v3, v1

    if-eq v2, v3, :cond_0

    aget-byte v2, p1, v1

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->T:[B

    aget-byte v3, v3, v1

    if-eq v2, v3, :cond_0

    aget-byte v2, p1, v1

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->PDT:[B

    aget-byte v3, v3, v1

    if-eq v2, v3, :cond_0

    aget-byte v2, p1, v1

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->ERS:[B

    aget-byte v3, v3, v1

    if-eq v2, v3, :cond_0

    aget-byte v2, p1, v1

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->GDT:[B

    aget-byte v3, v3, v1

    if-ne v2, v3, :cond_7

    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    const-string/jumbo v3, "ping_response"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->pingReplied:Z

    if-nez v3, :cond_1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->pingReplied:Z

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->LOG_TAG:Ljava/lang/String;

    const-string v3, "[InfopiaProtocol] mUsbManager is INFOPIA, responseReceived will call"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-nez v2, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->LOG_TAG:Ljava/lang/String;

    const-string v2, "[InfopiaProtocol] meterListener is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    instance-of v0, v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;->responseReceived([B)V

    :cond_3
    :goto_2
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    instance-of v0, v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/GetTimeCommand;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/GetTimeCommand;

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/GetTimeCommand;->responseReceived([B)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    instance-of v0, v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/ReadGlucoseCommand;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/ReadGlucoseCommand;

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/ReadGlucoseCommand;->responseReceived([B)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    instance-of v0, v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/SetTimeCommand;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/SetTimeCommand;

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/SetTimeCommand;->responseReceived([B)V

    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_1
.end method

.method public notifyStart()I
    .locals 2

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/ReadGlucoseCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/ReadGlucoseCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->doAction(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStarted(I)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public notifyStop()I
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    :cond_0
    return v2
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "send_ping"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "command_request_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->setCommandMode(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->sendPing()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    invoke-interface {v0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->doAction(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CMD_GET_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/GetTimeCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/GetTimeCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CMD_SET_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/SetTimeCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/SetTimeCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    const-string v2, "PARAM_TIME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Date;->setTime(J)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/SetTimeCommand;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/SetTimeCommand;->setTime(Ljava/util/Date;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CMD_CLEAR_DATA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/DeleteAllDataCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v1, "Not supported command"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    const/16 v0, 0x3ea

    goto :goto_1
.end method

.method public sendPing()V
    .locals 2

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/GetTimeCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/GetTimeCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->infopiaCommand:Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->doAction(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V

    return-void
.end method

.method public setCommandMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;->isPingCommandMode:Z

    return-void
.end method
