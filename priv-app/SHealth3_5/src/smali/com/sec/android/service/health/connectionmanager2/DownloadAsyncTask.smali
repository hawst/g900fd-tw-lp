.class Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;
.super Landroid/os/AsyncTask;
.source "DownloadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;",
        "Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;",
        "Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;",
        ">;"
    }
.end annotation


# static fields
.field private static final ChunkSize:I = 0x1000


# instance fields
.field private PauseSyncObject:Ljava/lang/Object;

.field private TAG:Ljava/lang/String;

.field private cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

.field private connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private context:Lorg/apache/http/protocol/BasicHttpContext;

.field downloadBufferedInputStream:Ljava/io/BufferedInputStream;

.field private downloadContentLength:J

.field private downloadedBytes:J

.field downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

.field private httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

.field httpEntity:Lorg/apache/http/HttpEntity;

.field private httpget:Lorg/apache/http/client/methods/HttpGet;

.field private isPause:Z


# direct methods
.method protected constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)V
    .locals 4
    .param p1, "connectionmanager"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "downloadparameters"    # Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    const-class v0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->TAG:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpget:Lorg/apache/http/client/methods/HttpGet;

    .line 32
    iput-wide v2, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    .line 33
    iput-wide v2, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadedBytes:J

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->isPause:Z

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->PauseSyncObject:Ljava/lang/Object;

    .line 36
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    .line 37
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .line 38
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 39
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 40
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    .line 41
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    .line 42
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpEntity:Lorg/apache/http/HttpEntity;

    .line 52
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 53
    iput-object p2, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .line 54
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getHttpClientManager()Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 55
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    .line 56
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;
    .locals 12
    .param p1, "params"    # [Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .prologue
    const/16 v7, 0x1000

    const/4 v11, -0x1

    .line 66
    const/4 v5, -0x1

    .line 67
    .local v5, "responseCode":I
    new-array v1, v7, [B

    .line 68
    .local v1, "data":[B
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    if-nez v7, :cond_0

    .line 70
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " DownloadASyncTask HttpClientManager NULL Id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v7, -0x7

    invoke-direct {v6, v11, v7}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 72
    .local v6, "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7, v6}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 73
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .line 197
    .end local v6    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :goto_0
    return-object v7

    .line 77
    :cond_0
    :try_start_0
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " DownloadASyncTask  doInBackground Started = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getUrl()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpget:Lorg/apache/http/client/methods/HttpGet;

    .line 83
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpget:Lorg/apache/http/client/methods/HttpGet;

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    invoke-interface {v7, v8, v9}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 84
    .local v4, "httpresponse":Lorg/apache/http/HttpResponse;
    if-nez v4, :cond_1

    .line 86
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " DownloadASyncTask HttpResponse NULL Id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v3, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v7, -0x1e

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v3, v5, v7, v8, v9}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    .line 88
    .local v3, "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7, v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 89
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    goto :goto_0

    .line 91
    .end local v3    # "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_1
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    const/16 v8, 0xc8

    if-eq v7, v8, :cond_2

    .line 93
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " DownloadASyncTask HttpResponse != 200 Id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    const/4 v10, -0x1

    invoke-direct {v8, v9, v10}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    invoke-virtual {v7, v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 95
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    goto/16 :goto_0

    .line 97
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->setHeaders(Lorg/apache/http/HttpResponse;)V

    .line 98
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpEntity:Lorg/apache/http/HttpEntity;

    .line 99
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpEntity:Lorg/apache/http/HttpEntity;

    if-nez v7, :cond_3

    .line 101
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " DownloadASyncTask HttpEntity is NULL Id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    new-instance v3, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v7, -0x1e

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v3, v5, v7, v8, v9}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    .line 103
    .restart local v3    # "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7, v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 104
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    goto/16 :goto_0

    .line 107
    .end local v3    # "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpEntity:Lorg/apache/http/HttpEntity;

    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    .line 108
    iget-wide v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-nez v7, :cond_4

    .line 110
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " DownloadASyncTask Contentlength = 0, Id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    new-instance v3, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v7, -0x1e

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v3, v5, v7, v8, v9}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;Ljava/lang/Exception;)V

    .line 112
    .restart local v3    # "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7, v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 113
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    goto/16 :goto_0

    .line 115
    .end local v3    # "ex":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_4
    new-instance v7, Ljava/io/BufferedInputStream;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpEntity:Lorg/apache/http/HttpEntity;

    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v8

    const/16 v9, 0x1000

    invoke-direct {v7, v8, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    .line 116
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 117
    :goto_1
    iget-wide v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadedBytes:J

    iget-wide v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    cmp-long v7, v7, v9

    if-gez v7, :cond_9

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 121
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v9, 0x0

    invoke-direct {v8, v5, v9}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    invoke-virtual {v7, v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 122
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    if-eqz v7, :cond_5

    .line 124
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 126
    :cond_5
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 128
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    .line 129
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 131
    :cond_6
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    goto/16 :goto_0

    .line 134
    :cond_7
    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->PauseSyncObject:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 136
    :try_start_1
    iget-boolean v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->isPause:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v7, :cond_8

    .line 140
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->PauseSyncObject:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 158
    :cond_8
    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 159
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    const/4 v8, 0x0

    const/16 v9, 0x1000

    invoke-virtual {v7, v1, v8, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v0

    .line 160
    .local v0, "bytesRead":I
    if-ne v0, v11, :cond_e

    .line 162
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 163
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 165
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    .line 166
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 174
    .end local v0    # "bytesRead":I
    :cond_9
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v9, 0x0

    invoke-direct {v8, v5, v9}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    invoke-virtual {v7, v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 175
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 142
    :catch_0
    move-exception v3

    .line 144
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_5
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " DownloadASyncTask pause waiting error, Id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v10}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-static {v5, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 146
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    if-eqz v7, :cond_a

    .line 148
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 150
    :cond_a
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    if-eqz v7, :cond_b

    .line 152
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    .line 153
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 155
    :cond_b
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    monitor-exit v8

    goto/16 :goto_0

    .line 158
    .end local v3    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v7
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 177
    .end local v4    # "httpresponse":Lorg/apache/http/HttpResponse;
    :catch_1
    move-exception v2

    .line 179
    .local v2, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " DownloadASyncTask read error, Id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-static {v5, v2}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setResponse(Ljava/lang/Object;)V

    .line 183
    :try_start_7
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    if-eqz v7, :cond_c

    .line 185
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 187
    :cond_c
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    if-eqz v7, :cond_d

    .line 189
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    .line 190
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 197
    :cond_d
    :goto_2
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    goto/16 :goto_0

    .line 170
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "bytesRead":I
    .restart local v4    # "httpresponse":Lorg/apache/http/HttpResponse;
    :cond_e
    :try_start_8
    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v1, v8, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 171
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    const/4 v8, 0x0

    sget-object v9, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->ONGOING:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 172
    iget-wide v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadedBytes:J

    int-to-long v9, v0

    add-long/2addr v7, v9

    iput-wide v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadedBytes:J
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_1

    .line 193
    .end local v0    # "bytesRead":I
    .end local v4    # "httpresponse":Lorg/apache/http/HttpResponse;
    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v3

    .line 195
    .restart local v3    # "ex":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->doInBackground([Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)V
    .locals 11
    .param p1, "result"    # Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadTaskCompleted(J)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpEntity:Lorg/apache/http/HttpEntity;

    if-eqz v0, :cond_1

    .line 269
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadBufferedInputStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 276
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpget:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->httpget:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 289
    :cond_2
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " DownloadASyncTask onCancelled Id =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v2}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    if-eqz v0, :cond_3

    .line 292
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getHeaderValues()Ljava/util/HashMap;

    move-result-object v9

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;->onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V

    .line 296
    :goto_1
    return-void

    .line 279
    :catch_0
    move-exception v10

    .line 281
    .local v10, "ex":Ljava/io/IOException;
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->TAG:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 295
    .end local v10    # "ex":Ljava/io/IOException;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/service/health/connectionmanager2/NetException;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getHeaderValues()Ljava/util/HashMap;

    move-result-object v9

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;->onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V

    goto :goto_1
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->onCancelled(Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)V

    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)V
    .locals 11
    .param p1, "result"    # Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadTaskCompleted(J)V

    .line 237
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " DownloadASyncTask onPostExecute Id =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v2}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getResponse()Ljava/lang/Object;

    move-result-object v10

    .line 239
    .local v10, "obj":Ljava/lang/Object;
    instance-of v0, v10, Lcom/sec/android/service/health/connectionmanager2/NetException;

    if-eqz v0, :cond_0

    move-object v7, v10

    .line 241
    check-cast v7, Lcom/sec/android/service/health/connectionmanager2/NetException;

    .line 242
    .local v7, "NE":Lcom/sec/android/service/health/connectionmanager2/NetException;
    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v0

    if-nez v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getHeaderValues()Ljava/util/HashMap;

    move-result-object v9

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;->onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V

    .line 251
    .end local v7    # "NE":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_0
    :goto_0
    return-void

    .line 248
    .restart local v7    # "NE":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getHeaderValues()Ljava/util/HashMap;

    move-result-object v9

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;->onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->onPostExecute(Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;)V
    .locals 10
    .param p1, "status"    # [Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    .prologue
    const/4 v4, 0x0

    .line 211
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 213
    aget-object v0, p1, v4

    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne v0, v1, :cond_1

    .line 215
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getPrivateId()I

    move-result v3

    aget-object v4, p1, v4

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadContentLength:J

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/service/health/connectionmanager2/NetException;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getHeaderValues()Ljava/util/HashMap;

    move-result-object v9

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;->onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getPrivateId()I

    move-result v3

    aget-object v4, p1, v4

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadedBytes:J

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/service/health/connectionmanager2/NetException;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v9}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getHeaderValues()Ljava/util/HashMap;

    move-result-object v9

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;->onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->onProgressUpdate([Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;)V

    return-void
.end method

.method protected setHeaders(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "httpresponse"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 316
    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v6}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getHeaderNames()Ljava/util/ArrayList;

    move-result-object v1

    .line 317
    .local v1, "headernames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 319
    .local v0, "headerValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 321
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 323
    const/4 v2, 0x0

    .line 324
    .local v2, "headers":[Lorg/apache/http/Header;
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {p1, v6}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v2

    .line 325
    if-eqz v2, :cond_1

    array-length v6, v2

    if-lez v6, :cond_1

    .line 327
    array-length v6, v2

    new-array v3, v6, [Ljava/lang/String;

    .line 328
    .local v3, "headervals":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_1
    array-length v6, v2

    if-ge v5, v6, :cond_0

    .line 330
    aget-object v6, v2, v5

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    .line 328
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 332
    :cond_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    .end local v3    # "headervals":[Ljava/lang/String;
    .end local v5    # "k":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 335
    .end local v2    # "headers":[Lorg/apache/http/Header;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v6, v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->setHeaderValues(Ljava/util/HashMap;)V

    .line 337
    .end local v4    # "i":I
    :cond_3
    return-void
.end method

.method protected stopDownload(Lcom/sec/android/service/health/connectionmanager2/NetException;)V
    .locals 4
    .param p1, "e"    # Lcom/sec/android/service/health/connectionmanager2/NetException;

    .prologue
    .line 308
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->CONNECTION_MANAGER_DEBUG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " DownloadASyncTask stopDownload Id =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->downloadparameters:Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;

    invoke-virtual {v2}, Lcom/sec/android/service/health/connectionmanager2/DownloadParameters;->getRequestId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    .line 310
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/connectionmanager2/DownloadAsyncTask;->cancel(Z)Z

    .line 311
    return-void
.end method
