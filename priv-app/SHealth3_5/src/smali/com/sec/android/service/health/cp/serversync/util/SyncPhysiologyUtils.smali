.class public Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addModifySleep(Ljava/lang/Object;)V
    .locals 9

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;->TAG:Ljava/lang/String;

    const-string v1, "addModifySleep"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;

    if-eqz v0, :cond_b

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;->sleepInfoList:Ljava/util/ArrayList;

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;->sleepInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_b

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;->sleepInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "_id"

    iget-wide v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceSleepPKId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "user_device__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->accId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "application__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->appId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "bed_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->bedtime:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "rise_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->risetime:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->awakeCount:Ljava/lang/String;

    if-eqz v4, :cond_1

    const-string v4, "awake_count"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->awakeCount:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->quality:Ljava/lang/String;

    if-eqz v4, :cond_2

    const-string/jumbo v4, "quality"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->quality:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->movement:Ljava/lang/String;

    if-eqz v4, :cond_3

    const-string/jumbo v4, "movement"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->movement:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_3
    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->efficiency:Ljava/lang/Double;

    if-eqz v4, :cond_4

    const-string v4, "efficiency"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->efficiency:Ljava/lang/Double;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    :cond_4
    const-string v4, "input_source_type"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->inputType:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getInputType(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v4, "noise"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->noise:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "comment"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->comment:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "create_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceCreateDate:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "update_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceModifyDate:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "time_zone"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "daylight_saving"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "hdid"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->hdid:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "sync_status"

    const v5, 0x29811

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v4, "3437xv63xb"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->appId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->accId:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->accId:Ljava/lang/String;

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_5
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->insertNewUserDeviceRow(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    const-string/jumbo v5, "user_device__id"

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string/jumbo v4, "sleep"

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v1, v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->accId:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->insertUserDeviceRow(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string/jumbo v4, "sleep"

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v1, v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_7
    :goto_1
    :try_start_2
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->sleepDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "_id"

    iget-wide v7, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceSleepDataPKId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "application__id"

    iget-object v7, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->appId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v6, "sleep__id"

    iget-wide v7, v0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceSleepPKId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v6, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->sleepStatusCode:Ljava/lang/String;

    if-eqz v6, :cond_8

    const-string/jumbo v6, "sleep_status"

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->sleepStatusCode:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_8
    const-string/jumbo v6, "sample_time"

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->sampleTime:Ljava/lang/String;

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "create_time"

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceCreateDate:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v6, "update_time"

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceModifyDate:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v6, "time_zone"

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "daylight_saving"

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "hdid"

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->hdid:Ljava/lang/String;

    invoke-virtual {v5, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sync_status"

    const v6, 0x29811

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    const-string/jumbo v1, "sleep_data"

    const/4 v6, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v7

    invoke-virtual {v2, v1, v6, v5, v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_3
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    :catch_0
    move-exception v1

    :try_start_4
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error inserting sleep_data data. Invalid values: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :cond_9
    :try_start_5
    const-string/jumbo v4, "sleep"

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v1, v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_5
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    :catch_1
    move-exception v0

    :try_start_6
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error inserting sleep data. Invalid values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    :cond_b
    return-void
.end method

.method public static addModifyStress(Ljava/lang/Object;)V
    .locals 7

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;->TAG:Ljava/lang/String;

    const-string v1, "addModifyStress"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;

    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;->stressHistoryList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;->stressHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetPhysiologyHistory;->stressHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "_id"

    iget-wide v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceStressPKId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "application__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->appId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->stressStatus:Ljava/lang/String;

    if-eqz v4, :cond_0

    const-string/jumbo v4, "state"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->stressStatus:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    const-string/jumbo v4, "score"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->score:Ljava/lang/Float;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string/jumbo v4, "user_device__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->accId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "sample_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->startTime:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "create_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceCreateDate:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "update_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceModifyDate:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "time_zone"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "comment"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->comment:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceDaylightSaving:Ljava/lang/String;

    if-eqz v4, :cond_1

    const-string v4, "daylight_saving"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string v4, "hdid"

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->hdid:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "sync_status"

    const v4, 0x29811

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string/jumbo v0, "stress"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v1, v0, v4, v3, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error inserting stress data. Invalid values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :cond_1
    :try_start_3
    const-string v4, "daylight_saving"

    const v5, 0x445c2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    :cond_3
    return-void
.end method

.method public static getDeletedPhysiology(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "deleted_info"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "table_name = \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    const-string v1, "1y90e30264"

    iput-object v1, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory;->appId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory;->itemList:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory$Item;

    const-string v4, "create_time"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils$RequestDeletePhysiologyHistory;->itemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return-object v2

    :cond_2
    if-eqz p1, :cond_3

    const-string/jumbo v0, "sleep"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_SLEEP_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    move-object v2, v8

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "stress"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_STRESS_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static getModifiedSleep(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/CursorWindowAllocationException;
        }
    .end annotation

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetSleep;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestSetSleep;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v3, "sync_status = ? or sync_status = ? "

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x1

    const v2, 0x29813

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    const-string/jumbo v1, "sleep"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;

    invoke-direct {v11}, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;-><init>()V

    const-string/jumbo v1, "sync_status"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->requestType:Ljava/lang/String;

    const-string/jumbo v1, "time_zone"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceTimeZone:Ljava/lang/String;

    const-string v1, "bed_time"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-string/jumbo v3, "rise_time"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-lez v5, :cond_6

    :goto_1
    iget-object v5, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->bedtime:Ljava/lang/String;

    iget-object v3, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->risetime:Ljava/lang/String;

    const-string v1, "awake_count"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->awakeCount:Ljava/lang/String;

    const-string v1, "efficiency"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->efficiency:Ljava/lang/Double;

    const-string/jumbo v1, "quality"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->quality:Ljava/lang/String;

    const-string/jumbo v1, "movement"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->movement:Ljava/lang/String;

    const-string/jumbo v1, "noise"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->noise:Ljava/lang/String;

    const-string v1, "comment"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "comment"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->comment:Ljava/lang/String;

    :goto_2
    const-string v1, "create_time"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceCreateDate:Ljava/lang/String;

    const-string/jumbo v1, "update_time"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceModifyDate:Ljava/lang/String;

    const-string v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceSleepPKId:J

    const-string v1, "application__id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->appId:Ljava/lang/String;

    const-string v1, "input_source_type"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getInputType(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->inputType:Ljava/lang/String;

    const-string v1, "daylight_saving"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceDaylightSaving:Ljava/lang/String;

    const-string/jumbo v1, "user_device__id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->accId:Ljava/lang/String;

    const-string v1, "hdid"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->hdid:Ljava/lang/String;

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v2, "sleep"

    iget-wide v3, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceSleepPKId:J

    const-string/jumbo v5, "update_time"

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v3, "sleep__id = ? "

    const-string/jumbo v1, "sleep_data"

    const/4 v2, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->deviceSleepPKId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_3
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;

    invoke-direct {v12}, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;-><init>()V

    const-string/jumbo v1, "time_zone"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceTimeZone:Ljava/lang/String;

    const-string/jumbo v1, "sample_time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iget-object v3, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->sampleTime:Ljava/lang/String;

    const-string/jumbo v1, "sleep_status"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->sleepStatusCode:Ljava/lang/String;

    const-string v1, "create_time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceCreateDate:Ljava/lang/String;

    const-string/jumbo v1, "update_time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceModifyDate:Ljava/lang/String;

    const-string v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceSleepDataPKId:J

    const-string v1, "daylight_saving"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceDaylightSaving:Ljava/lang/String;

    const-string v1, "hdid"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->hdid:Ljava/lang/String;

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v2, "sleep_data"

    iget-wide v3, v12, Lcom/sec/android/service/health/cp/serversync/data/SleepItem;->deviceSleepDataPKId:J

    const-string/jumbo v5, "update_time"

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->sleepDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_3

    :cond_0
    const-string v1, ""

    iput-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->comment:Ljava/lang/String;

    goto/16 :goto_2

    :cond_1
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    iget-object v1, v11, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->sleepDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_SLEEP_INFO:Ljava/lang/String;

    invoke-direct {v1, v2, v11, v10}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    :cond_4
    if-eqz v9, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_5
    return-object v8

    :cond_6
    move-wide v13, v3

    move-wide v3, v1

    move-wide v1, v13

    goto/16 :goto_1
.end method

.method public static getModifiedStress(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v3, "sync_status = ? or sync_status = ? "

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v5, 0x29812

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const v5, 0x29813

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const-string/jumbo v1, "stress"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;-><init>()V

    const-string/jumbo v0, "sync_status"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->requestType:Ljava/lang/String;

    const-string/jumbo v0, "time_zone"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceTimeZone:Ljava/lang/String;

    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v2, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->startTime:Ljava/lang/String;

    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v2, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->endTime:Ljava/lang/String;

    const-string/jumbo v0, "state"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->stressStatus:Ljava/lang/String;

    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->accId:Ljava/lang/String;

    const-string/jumbo v0, "sensor"

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->inputType:Ljava/lang/String;

    const-string v0, "create_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceCreateDate:Ljava/lang/String;

    const-string/jumbo v0, "update_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceModifyDate:Ljava/lang/String;

    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceStressPKId:J

    const-string v0, "application__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->appId:Ljava/lang/String;

    const-string v0, "daylight_saving"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceDaylightSaving:Ljava/lang/String;

    const-string v0, "comment"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "comment"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->comment:Ljava/lang/String;

    :goto_1
    const-string/jumbo v0, "score"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "score"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->score:Ljava/lang/Float;

    :cond_0
    const-string v0, "hdid"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->hdid:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v1, "stress"

    iget-wide v2, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->deviceStressPKId:J

    const-string/jumbo v4, "update_time"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_STRESS_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v9, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    :cond_1
    const-string v0, ""

    iput-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/StressInfo;->comment:Ljava/lang/String;

    goto :goto_1

    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v8
.end method

.method public static getPhysiologyDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;

    invoke-direct {v3}, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;-><init>()V

    if-eqz p2, :cond_0

    const-string/jumbo v0, "sleep"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "sleep"

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;->type:Ljava/lang/String;

    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncPhysiologyUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " items for "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;->itemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v4, 0x3e7

    if-ne v0, v4, :cond_2

    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;->itemList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory$Item;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_PHYSIOLOGY_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v4, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;->itemList:Ljava/util/ArrayList;

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "stress"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "stress"

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;->type:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;->itemList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory$Item;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/RequestGetPhysiologyHistory;->itemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_PHYSIOLOGY_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v2
.end method
