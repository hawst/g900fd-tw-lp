.class public Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Landroid/location/LocationListener;
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;
    }
.end annotation


# static fields
.field private static DataUpdate:Z = false

.field private static final GPS_DATA:I = 0x7d4

.field private static final GPS_INIT:I = 0x7d1

.field private static final GPS_START:I = 0x7d2

.field private static final GPS_STOP:I = 0x7d3

.field protected static final STATE_DISCONNECTED:I = 0x3e9

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field protected mConnectState:I

.field mContext:Landroid/content/Context;

.field mExerciseId:J

.field mGpsDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field protected mGpsManager:Lcom/sec/android/service/health/sensor/manager/GPSManager;

.field private mHandler:Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

.field private mLocationManager:Landroid/location/LocationManager;

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field protected pairingCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

.field protected state:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->DataUpdate:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mExerciseId:J

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mGpsDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->pairingCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const/16 v0, 0x3e9

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->state:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mConnectState:I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;-><init>(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mHandler:Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->gpsinit()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)Z
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->registerGpsListener()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->gpsStart()V

    return-void
.end method

.method static synthetic access$402(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->DataUpdate:Z

    return p0
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->gpsStop()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->unregisterGpsListener()V

    return-void
.end method

.method private gpsStart()V
    .locals 6

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gpsStart : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x3e8

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    :cond_0
    return-void
.end method

.method private gpsStop()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gpsStop : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    :cond_0
    return-void
.end method

.method private gpsinit()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string v1, "gpsinit "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string v1, "GPS Disable!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string v1, "GPS Ensable!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerGpsListener()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v2, 0x6

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private unregisterGpsListener()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterGpsListener : mProfileHandlerListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x7d3

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mHandler:Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mGpsDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 2

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mGpsDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/16 v1, 0x7d1

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mHandler:Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x0

    return v0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onGpsStatusChanged(I)V
    .locals 0

    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;-><init>()V

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->latitude:D

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->longitude:D

    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->altitude:D

    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->accuracy:F

    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->bearing:F

    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->speed:F

    :cond_3
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Altitude:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Latitude:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Longitude:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Accuracy:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v2, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->DataUpdate:Z

    if-eqz v2, :cond_4

    const/16 v2, 0x7d4

    iput v2, v0, Landroid/os/Message;->what:I

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mHandler:Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_4
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onProviderDisabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onProviderEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startReceivingData called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x7d2

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mHandler:Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x0

    return v0
.end method

.method public stopReceivingData()I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopReceivingData called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x7d3

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mHandler:Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x0

    return v0
.end method
