.class public Lcom/sec/android/service/health/cp/serversync/data/WalkingItem;
.super Ljava/lang/Object;


# instance fields
.field public accessaryId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public calorie:D

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public distance:D

.field public distanceUnit:Ljava/lang/String;

.field public downStep:I

.field public endTime:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public runStep:I

.field public samplePosition:Ljava/lang/Integer;

.field public startTime:Ljava/lang/String;

.field public totalStep:I

.field public upDownStep:I

.field public upStep:I

.field public walkSpeed:D

.field public walkSpeedUnit:Ljava/lang/String;

.field public walkStep:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
