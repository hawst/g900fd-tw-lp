.class Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->access$000(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
