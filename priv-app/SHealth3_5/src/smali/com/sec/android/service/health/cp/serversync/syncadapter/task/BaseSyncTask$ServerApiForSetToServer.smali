.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;
.super Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServerApiForSetToServer"
.end annotation


# instance fields
.field public syncedRowIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->syncedRowIdList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;",
            ">;",
            "Lcom/sec/android/service/health/connectionmanager2/RequestParam;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->syncedRowIdList:Ljava/util/ArrayList;

    return-void
.end method
