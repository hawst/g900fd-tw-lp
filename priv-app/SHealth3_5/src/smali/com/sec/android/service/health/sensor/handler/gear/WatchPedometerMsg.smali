.class public Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;
.super Ljava/lang/Object;


# static fields
.field public static final CODE_CMD_REPLACE_SYNCED_DATA:Ljava/lang/String; = "shealth-replacesynceddata-cmd"

.field public static final CODE_CMD_SYNC_FROM_GEAR:Ljava/lang/String; = "pedometer-sync-cmd"

.field public static final CODE_REQ_SETSTEP_FROM_SHEALTH:Ljava/lang/String; = "shealth-setstep-req"

.field public static final CODE_REQ_SYNC_FROM_SHEALTH:Ljava/lang/String; = "shealth-get-req"

.field public static final INCH_UNIT:Ljava/lang/String; = "inch"

.field public static final KG_UNIT:Ljava/lang/String; = "kg"

.field public static final KM_UNIT:Ljava/lang/String; = "km"

.field public static final LB_UNIT:Ljava/lang/String; = "lb"

.field private static final PEDOMETER_ACTION_REQ_SYNC:Ljava/lang/String; = "com.sec.android.app.pedometer.REQ_SYNC"

.field public static final SYNC_REV:I = 0x2

.field private static final TAG:Ljava/lang/String; = "GearHandler"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final ackMsgForSyncReqFromGear(Ljava/lang/String;Ljava/lang/Long;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string/jumbo v1, "responseCode"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "responseMessage"

    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "gearMessageRevision"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method private static getCurrentScontextGoal(Landroid/content/Context;)I
    .locals 8

    const/16 v7, 0x2710

    const/4 v6, 0x0

    :try_start_0
    const-string v3, "SELECT value FROM goal AS G, user_device AS UD WHERE G.user_device__id = UD._id AND UD.device_type = 10009 ORDER BY G.set_time DESC LIMIT 1"

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static final getCurrentWalkingMateGoal(Landroid/content/Context;JLjava/lang/String;)I
    .locals 7

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v6, 0x0

    const-string v3, "goal_type=? AND set_time<? AND user_device__id = ?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v6

    const-string v0, ""

    aput-object v0, v4, v2

    const-string v0, ""

    aput-object v0, v4, v5

    const v0, 0x9c41

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {p1, p2}, Lcom/sec/android/service/health/sensor/SensorService;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    aput-object p3, v4, v5

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v5, "value"

    aput-object v5, v2, v6

    const-string/jumbo v5, "set_time DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getCurrentScontextGoal(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getCurrentScontextGoal(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method private static getProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .locals 1

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static final makeCurrentState(Landroid/content/Context;Ljava/lang/Long;)Lorg/json/JSONObject;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    const-string/jumbo v3, "start_time>?  AND start_time<? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v6

    const-string v0, ""

    aput-object v0, v4, v2

    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->getStartOfDay()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/SensorService;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v5, "_id DESC "

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;-><init>()V

    const-string v1, "exercise__id"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setExerciseId(J)V

    const-string v1, "create_time"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setCreateTime(J)V

    const-string/jumbo v1, "total_step"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setTotalStep(I)V

    const-string/jumbo v1, "run_step"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setRunSteps(I)V

    const-string/jumbo v1, "walk_step"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setWalkSteps(I)V

    const-string/jumbo v1, "updown_step"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setUpDownSteps(I)V

    const-string v1, "calorie"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setKcal(F)V

    const-string/jumbo v1, "speed"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setSpeed(F)V

    const-string v1, "distance"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setDistance(F)V

    const-string/jumbo v1, "user_device__id"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setUserDeviceId(Ljava/lang/String;)V

    const-string/jumbo v1, "time_zone"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->setTimeZone(I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    move v1, v6

    move v2, v7

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->getTotalStep()I

    move-result v5

    add-int/2addr v6, v5

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->getDistance()F

    move-result v5

    add-float/2addr v7, v5

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->getKcal()F

    move-result v0

    add-float/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v2, v7

    :cond_2
    :goto_2
    if-eqz v3, :cond_3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "lastStepTimestamp"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "totalStep"

    invoke-virtual {v0, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "calorie"

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "distance"

    float-to-long v2, v7

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-object v0

    :cond_4
    move v2, v7

    goto :goto_2
.end method

.method private static final makeProfileObject(Landroid/content/Context;)Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v1

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v2

    const-string v3, "height"

    float-to-double v4, v1

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string/jumbo v1, "weight"

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v1

    const v2, 0x2e635

    if-ne v1, v2, :cond_0

    const-string v1, "gender"

    const-string v2, "M"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_0
    const-string/jumbo v1, "unit"

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->makeUnitObject(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0

    :cond_0
    const-string v1, "gender"

    const-string v2, "F"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0
.end method

.method public static final makeSyncReqToGear(Landroid/content/Context;JJZLjava/lang/String;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string/jumbo v2, "requestCode"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p5, :cond_0

    const-string/jumbo v2, "requestMessage"

    const-string/jumbo v3, "shealth-setstep-req"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_0
    const-string v2, "gearMessageRevision"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v2, "profile"

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->makeProfileObject(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v2, "stepGoal"

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {p0, v3, v4, p6}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getCurrentWalkingMateGoal(Landroid/content/Context;JLjava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "currentState"

    invoke-static {p0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->makeCurrentState(Landroid/content/Context;Ljava/lang/Long;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string/jumbo v3, "startTimestamp"

    invoke-virtual {v2, v3, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "endTimestamp"

    invoke-virtual {v2, v3, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "duration"

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v2, "requestParam"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v2, "requestMessage"

    const-string/jumbo v3, "shealth-get-req"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0
.end method

.method private static final makeUnitObject(Landroid/content/Context;)Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getDistanceUnit(Landroid/content/Context;)I

    move-result v1

    const v2, 0x29811

    if-ne v1, v2, :cond_0

    const-string v1, "distance"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :goto_0
    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v1

    const v2, 0x1fbd1

    if-ne v1, v2, :cond_1

    const-string/jumbo v1, "weight"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :goto_1
    invoke-static {p0}, Lcom/sec/android/service/health/sensor/handler/gear/WatchPedometerMsg;->getProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v1

    const v2, 0x249f1

    if-ne v1, v2, :cond_2

    const-string v1, "height"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :goto_2
    return-object v0

    :cond_0
    const-string v1, "distance"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "weight"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_1

    :cond_2
    const-string v1, "height"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_2
.end method
