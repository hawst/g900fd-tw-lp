.class Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/UsbHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReadThread"
.end annotation


# instance fields
.field bInterrupted:Z

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/UsbHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->this$0:Lcom/sec/android/service/health/sensor/handler/UsbHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->bInterrupted:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/handler/UsbHandler;Lcom/sec/android/service/health/sensor/handler/UsbHandler$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;-><init>(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->bInterrupted:Z

    if-nez v0, :cond_1

    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->sleep(J)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->this$0:Lcom/sec/android/service/health/sensor/handler/UsbHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->this$0:Lcom/sec/android/service/health/sensor/handler/UsbHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)I

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->data:[B

    const/16 v2, 0x12c

    # invokes: Lcom/sec/android/service/health/sensor/handler/UsbHandler;->nativeUsbSppRead(I[BI)[B
    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->access$200(I[BI)[B

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->data:[B

    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->this$0:Lcom/sec/android/service/health/sensor/handler/UsbHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->this$0:Lcom/sec/android/service/health/sensor/handler/UsbHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->data:[B

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->notifyRawDataReceived([B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "[HealthSensor]UsbHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[UsbHandler] exception occured ReadThread/run e= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]UsbHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[UsbHandler] out of thread while loop this="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method shutdown()V
    .locals 3

    const-string v0, "[HealthSensor]UsbHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[UsbHandler] shutdown iscalled this="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->bInterrupted:Z

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->interrupt()V

    return-void
.end method
