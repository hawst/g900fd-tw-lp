.class Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$DummyDataThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DummyDataThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Generating dummy data"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, 0xc8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    invoke-static {v2, v1}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    # getter for: Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Sending of dummy data is interrupted"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    return-void
.end method
