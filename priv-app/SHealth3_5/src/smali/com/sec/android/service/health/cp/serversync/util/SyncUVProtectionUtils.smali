.class public Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$DeletedUVProtectionItemsList;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestDeleteUVProtectionHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final UV_PROTECTION_GET_INDEX_MAX:I = 0xa

.field private static final UV_PROTECTION_SET_INDEX_MAX:I = 0xa


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addUVProtection(Ljava/lang/Object;)V
    .locals 9

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;->TAG:Ljava/lang/String;

    const-string v2, "addUVProtection"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    check-cast p0, Ljava/lang/String;

    const-class v2, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;

    invoke-virtual {v1, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;

    if-eqz v2, :cond_3

    iget-object v1, v2, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;->spfList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, v2, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;->spfList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;->spfList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;

    move-object v7, v0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "application__id"

    iget-object v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->appId:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "sun_protection"

    iget v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->spf:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "recommended_duration"

    iget v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->recommendedDuration:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "sample_time"

    iget-object v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->measuredTime:Ljava/lang/String;

    iget-object v5, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceTimeZone:Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "create_time"

    iget-object v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v2, "update_time"

    iget-object v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceUpdateTime:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v2, "time_zone"

    iget-object v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceTimeZone:Ljava/lang/Integer;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "hdid"

    iget-object v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->hdid:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "sync_status"

    const v4, 0x29811

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v2, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->daylightSaving:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    const-string v2, "daylight_saving"

    iget-object v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->daylightSaving:Ljava/lang/Integer;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    const-string/jumbo v2, "uv_protection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceUVPRTPKId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "_id"

    iget-wide v4, v7, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceUVPRTPKId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v2, "uv_protection"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v1, v2, v4, v3, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    sget-object v2, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error inserting/updating uv_protection data. Invalid values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v2

    :cond_1
    :try_start_3
    const-string v2, "daylight_saving"

    const v4, 0x445c2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    :cond_3
    return-void
.end method

.method public static getDeletedUVProtection(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDeletedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestDeleteUVProtectionHistory;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestDeleteUVProtectionHistory;-><init>()V

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$DeletedUVProtectionItemsList;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$DeletedUVProtectionItemsList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "deleted_info"

    const-string/jumbo v3, "table_name = \"uv_protection\""

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v10, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$DeletedUVProtectionItemsList;->deviceCreateTimes:Ljava/util/ArrayList;

    const-string v3, "create_time"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, v10, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$DeletedUVProtectionItemsList;->deviceCreateTimes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return-object v2

    :cond_2
    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestDeleteUVProtectionHistory;->deleteList:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_UV_PROTECTION_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v8

    goto :goto_1
.end method

.method public static getModifiedUVProtection(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/CursorWindowAllocationException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;->TAG:Ljava/lang/String;

    const-string v1, "getUnsyncedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v3, "sync_status = ? or sync_status = ? "

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const v1, 0x29812

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    const/4 v1, 0x1

    const v5, 0x29813

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v1, "uv_protection"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v6, v8

    move-object v7, v9

    move-object v9, v10

    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;-><init>()V

    const-string/jumbo v0, "sync_status"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->requestType:Ljava/lang/String;

    const-string/jumbo v0, "time_zone"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceTimeZone:Ljava/lang/Integer;

    const-string v0, "_id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceUVPRTPKId:J

    const-string v0, "application__id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->appId:Ljava/lang/String;

    const-string/jumbo v0, "sun_protection"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->spf:I

    const-string/jumbo v0, "recommended_duration"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->recommendedDuration:I

    const-string/jumbo v0, "sample_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v2, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceTimeZone:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->measuredTime:Ljava/lang/String;

    const-string v0, "daylight_saving"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "daylight_saving"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->daylightSaving:Ljava/lang/Integer;

    :goto_1
    const-string v0, "create_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceCreateTime:Ljava/lang/String;

    const-string/jumbo v0, "update_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceUpdateTime:Ljava/lang/String;

    const-string v0, "hdid"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->hdid:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v1, "uv_protection"

    iget-wide v2, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->deviceUVPRTPKId:J

    const-string/jumbo v4, "update_time"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;->spfList:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v6, 0x1

    const/16 v1, 0xa

    if-ne v0, v1, :cond_5

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_UV_PROTECTION:Ljava/lang/String;

    invoke-direct {v0, v1, v9, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;

    invoke-direct {v2}, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move v0, v8

    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move v6, v0

    move-object v7, v1

    move-object v9, v2

    goto/16 :goto_0

    :cond_0
    const v0, 0x445c2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/UVProtectionItem;->daylightSaving:Ljava/lang/Integer;

    goto :goto_1

    :cond_1
    move v8, v6

    move-object v10, v9

    move-object v9, v7

    :cond_2
    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    if-lez v8, :cond_4

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_UV_PROTECTION:Ljava/lang/String;

    invoke-direct {v0, v1, v10, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v11

    :cond_5
    move-object v1, v7

    move-object v2, v9

    goto :goto_2
.end method

.method public static getUVProtectionDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/16 v7, 0xa

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDetailHistoryData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " items for uv_protection"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v0, v2

    :goto_0
    if-lez v1, :cond_1

    new-instance v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;

    invoke-direct {v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;-><init>()V

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;->startTime:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;->endTime:Ljava/lang/String;

    iput v0, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;->offset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;->limit:Ljava/lang/Integer;

    if-lt v1, v7, :cond_0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;->limit:Ljava/lang/Integer;

    add-int/lit8 v0, v0, 0xa

    add-int/lit8 v1, v1, -0xa

    :goto_1
    new-instance v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v6, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_UV_PROTECTION_HISTORY:Ljava/lang/String;

    invoke-direct {v5, v6, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncUVProtectionUtils$RequestGetUVProtectionHistory;->limit:Ljava/lang/Integer;

    add-int/2addr v0, v1

    move v1, v2

    goto :goto_1

    :cond_1
    return-object v3
.end method
