.class public Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MealImage"
.end annotation


# instance fields
.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceImagePKId:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public foodDataType:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public imageDevicePath:Ljava/lang/String;

.field public imageFileName:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
