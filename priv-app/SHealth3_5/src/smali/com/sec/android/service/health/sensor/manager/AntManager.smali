.class public Lcom/sec/android/service/health/sensor/manager/AntManager;
.super Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/manager/AntManager$3;,
        Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;,
        Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;
    }
.end annotation


# static fields
.field protected static final MSG_REQ_SEARCH_START:I = 0x3ea

.field protected static final MSG_REQ_SEARCH_STOP:I = 0x3eb

.field public static final TEMP_EMPTY_INT_STRING:Ljava/lang/String; = "-1"

.field public static final TEMP_EMPTY_STRING:Ljava/lang/String; = "FEdevice"

.field public static final TEMP_FE_DEVICE_TYPE_STRING:Ljava/lang/String; = "17_0"

.field private static backgroundScanController:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

.field private static filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

.field private static mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

.field protected static volatile mAntSearchHandler:Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;

.field private static requestor:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;


# instance fields
.field private WatchNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

.field protected listDevice:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field protected volatile mServiceLooper:Landroid/os/Looper;

.field searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

.field protected searchlistDevice:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/AntManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    sput-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    sput-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->backgroundScanController:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    sput-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->requestor:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/AntManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/manager/AntManager$2;-><init>(Lcom/sec/android/service/health/sensor/manager/AntManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->WatchNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mServiceLooper:Landroid/os/Looper;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;-><init>(Landroid/os/Looper;Lcom/sec/android/service/health/sensor/manager/AntManager;)V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mAntSearchHandler:Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/manager/AntManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->stopSearch()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/service/health/sensor/manager/AntManager;
    .locals 2

    const-class v0, Lcom/sec/android/service/health/sensor/manager/AntManager;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static setTempFEdevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 2

    const-string v0, "17_0"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setId(Ljava/lang/String;)V

    const/16 v0, 0x271b

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setType(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setDataType(Ljava/util/List;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setConnectivityType(I)V

    const-string v0, "FEdevice"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setName(Ljava/lang/String;)V

    const-string v0, "-1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setProtocol(Ljava/lang/String;)V

    const-string v0, "-1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    return-object p0
.end method

.method private stopSearch()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "stopSearch()"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchlistDevice:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchlistDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->backgroundScanController:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->backgroundScanController:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    sput-object v2, Lcom/sec/android/service/health/sensor/manager/AntManager;->backgroundScanController:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    sput-object v2, Lcom/sec/android/service/health/sensor/manager/AntManager;->requestor:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->StopSearchWatch()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    :cond_2
    return-void
.end method


# virtual methods
.method public create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public deinitialize()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeProfileHandlerController(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeSensorDevice(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-super {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deinitialize()V

    return-void
.end method

.method public getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLooper()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mServiceLooper:Landroid/os/Looper;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method public isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectedState()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataTypeSupported(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    invoke-virtual {p0, p1, p0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->createDeviceHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :cond_1
    return-void
.end method

.method public leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeProfileHandlerController(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeSensorDevice(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public notifyDeviceSearchUpdated(Ljava/lang/String;IILjava/lang/String;ZII)V
    .locals 6

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "notifyDeviceSearchUpdated"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchlistDevice:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchlistDevice:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchlistDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->getUID(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {p2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->checkSupportDeviceType(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, p2

    move v1, p3

    move-object v2, p4

    move v3, p6

    move v4, p7

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->setANTDeviceInfo(IILjava/lang/String;IIZ)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSubType(I)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchlistDevice:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    goto :goto_0
.end method

.method public notifyHealthServiceError(I)V
    .locals 4

    const-string v0, "Failed to search ANT devices"

    const-string v1, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onHealthServiceError code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " desc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setEvent(I)V

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setResponse(Landroid/os/Bundle;)V

    return-void
.end method

.method public notifyWatchDeviceListUpdated([Ljava/lang/String;[I[I[Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "notifyWatchDeviceListUpdated"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_1

    const/4 v0, -0x1

    aget-object v2, p1, v1

    aget v3, p3, v1

    aget v4, p2, v1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->setANTDeviceInfo(IILjava/lang/String;IIZ)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {v2, v0}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/service/health/sensor/manager/AntManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onStateChanged(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getRemoteEventCallBackList(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-interface {v1, p2, v0}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onStateChanged(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V

    goto :goto_0
.end method

.method protected remoteCallBackDied(Ljava/lang/Integer;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "remoteCallBackDied() processId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->cleanupOnDied(Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getRemoteEventCallBackList(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeProfileHandlerController(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeSensorDevice(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method

.method public removeDeviceInfoForJoinFailed(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removerRemoteEventCallBackList(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeProfileHandlerController(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeSensorDevice(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->listDevice:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->request(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 8

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v4, p5

    move-wide v5, p6

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->startReceivingData(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startScanning(Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "AntManager startScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p1, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mAntSearchHandler:Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->mAntSearchHandler:Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;

    const/16 v2, 0x3ea

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x0

    return v0
.end method

.method public startSearch()V
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string/jumbo v1, "startSearch()"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchlistDevice:Ljava/util/ArrayList;

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->getFilterDeviceType()I

    move-result v0

    const/16 v1, 0x271d

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->setContext(Landroid/content/Context;Landroid/os/Looper;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->WatchNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->StartSearchWatch(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->getFilterDeviceType()I

    move-result v0

    const/16 v1, 0x2711

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->setContext(Landroid/content/Context;Landroid/os/Looper;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->searchAntWatch:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->WatchNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->StartSearchWatch(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;)V

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "17_0"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->setTempFEdevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->backgroundScanController:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->backgroundScanController:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/service/health/sensor/manager/AntManager$ScanResultReceiver;-><init>(Lcom/sec/android/service/health/sensor/manager/AntManager;Lcom/sec/android/service/health/sensor/manager/AntManager$1;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->startBackgroundScan(Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;)V

    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mServiceLooper:Landroid/os/Looper;

    new-instance v2, Lcom/sec/android/service/health/sensor/manager/AntManager$1;

    invoke-direct {v2, p0}, Lcom/sec/android/service/health/sensor/manager/AntManager$1;-><init>(Lcom/sec/android/service/health/sensor/manager/AntManager;)V

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;-><init>(Landroid/os/Looper;Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;)V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->requestor:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->requestor:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->broadcastAlreadyConnectedDeviceListRequest(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)I
    .locals 1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopScanning()Z
    .locals 3

    const-string v0, "[HealthSensor][HealthSensor]AbstractSensorManager"

    const-string v1, "AntManager stopScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/AntManager;->mAntSearchHandler:Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/AntManager;->mAntSearchHandler:Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;

    const/16 v2, 0x3eb

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager$AntSearchHandler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x1

    return v0
.end method
