.class public Lcom/sec/android/service/health/connectionmanager2/RequestParam;
.super Ljava/lang/Object;
.source "RequestParam.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private params:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->params:Ljava/util/ArrayList;

    .line 53
    return-void
.end method

.method public static makeApiWithParam(Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;)Ljava/lang/String;
    .locals 7
    .param p0, "api"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    .prologue
    const/16 v6, 0x3f

    .line 103
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 105
    .local v3, "url":Ljava/lang/StringBuffer;
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->size()I

    move-result v4

    if-eqz v4, :cond_4

    .line 107
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    .line 108
    .local v2, "length":I
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    const/16 v5, 0x2f

    if-ne v4, v5, :cond_2

    .line 110
    add-int/lit8 v4, v2, -0x1

    const-string v5, "?"

    invoke-virtual {v3, v4, v2, v5}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 117
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 121
    :try_start_0
    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->getKey(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->getValue(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-nez v4, :cond_3

    .line 117
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 112
    .end local v1    # "i":I
    :cond_2
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    if-eq v4, v6, :cond_0

    .line 114
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 126
    .restart local v1    # "i":I
    :cond_3
    :try_start_1
    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->getKey(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 128
    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->getValue(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_1

    .line 131
    const/16 v4, 0x26

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 134
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v4, 0x0

    .line 142
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v1    # "i":I
    .end local v2    # "length":I
    :goto_3
    return-object v4

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->params:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method

.method public getKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->params:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;

    # getter for: Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->key:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->access$000(Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->params:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;

    # getter for: Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->value:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->access$100(Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->params:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
