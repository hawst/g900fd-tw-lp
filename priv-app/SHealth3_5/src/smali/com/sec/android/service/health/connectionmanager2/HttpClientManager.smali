.class Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;
.super Ljava/lang/Object;
.source "HttpClientManager.java"


# static fields
.field private static final SSLCTX_PROTO_NAME:Ljava/lang/String; = "TLS"

.field private static final TAG:Ljava/lang/String; = "Adv.Con.Manager"

.field private static final default_connectionTimeout:I = 0x7530

.field private static final default_readTimeout:I = 0x7530


# instance fields
.field private MAX_CON_PER_ROUTE:I

.field private MAX_TOTAL_CON:I

.field private ServerIp:Ljava/lang/String;

.field private bufferSize:I

.field private cm:Lorg/apache/http/conn/ClientConnectionManager;

.field private connectionTimeout:I

.field private httpClient:Lorg/apache/http/client/HttpClient;

.field private isHttpsEnabled:Z

.field mAppCtx:Landroid/content/Context;

.field private mClientKeyStore:Ljava/security/KeyStore;

.field private mCustomSF:Lcom/sec/android/service/health/connectionmanager2/NewSSLSocketFactory;

.field private mSSLContext:Ljavax/net/ssl/SSLContext;

.field private mTrustStore:Ljava/security/KeyStore;

.field private port:I

.field private readTimeout:I


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/16 v2, 0x7530

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->isHttpsEnabled:Z

    .line 54
    iput v2, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->connectionTimeout:I

    .line 56
    iput v2, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->readTimeout:I

    .line 58
    const/16 v0, 0x2000

    iput v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->bufferSize:I

    .line 63
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 67
    const/16 v0, 0x50

    iput v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->port:I

    .line 69
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->cm:Lorg/apache/http/conn/ClientConnectionManager;

    .line 71
    iput v3, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->MAX_CON_PER_ROUTE:I

    .line 72
    iput v3, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->MAX_TOTAL_CON:I

    .line 78
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mTrustStore:Ljava/security/KeyStore;

    .line 79
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mClientKeyStore:Ljava/security/KeyStore;

    .line 80
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mSSLContext:Ljavax/net/ssl/SSLContext;

    .line 81
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mCustomSF:Lcom/sec/android/service/health/connectionmanager2/NewSSLSocketFactory;

    .line 91
    iput v2, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->connectionTimeout:I

    .line 92
    iput v2, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->readTimeout:I

    .line 93
    return-void
.end method


# virtual methods
.method protected getBufferSize()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->bufferSize:I

    return v0
.end method

.method protected getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->cm:Lorg/apache/http/conn/ClientConnectionManager;

    return-object v0
.end method

.method protected getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->httpClient:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method protected getIsHttpsEnabled()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->isHttpsEnabled:Z

    return v0
.end method

.method protected getPort()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->port:I

    return v0
.end method

.method protected getServerIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->ServerIp:Ljava/lang/String;

    return-object v0
.end method

.method initHttpClientManager(IIZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "connectionTimeout"    # I
    .param p2, "readTimeout"    # I
    .param p3, "httpsenabled"    # Z
    .param p4, "keyName"    # Ljava/lang/String;
    .param p5, "keyStorePassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->connectionTimeout:I

    .line 98
    iput p2, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->readTimeout:I

    .line 99
    invoke-virtual {p0, p3, p4, p5}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->initHttpClientManager(ZLjava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method initHttpClientManager(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "httpsenabled"    # Z
    .param p2, "keyName"    # Ljava/lang/String;
    .param p3, "keyStorePassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 104
    new-instance v3, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v3}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 105
    .local v3, "params":Lorg/apache/http/params/HttpParams;
    const-string v5, "http.socket.timeout"

    iget v6, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->readTimeout:I

    invoke-interface {v3, v5, v6}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 106
    const-string v5, "http.connection.timeout"

    iget v6, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->connectionTimeout:I

    invoke-interface {v3, v5, v6}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 107
    const-string v5, "http.socket.buffer-size"

    iget v6, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->bufferSize:I

    invoke-interface {v3, v5, v6}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 108
    const-string v5, "http.protocol.cookie-policy"

    const-string v6, "best-match"

    invoke-interface {v3, v5, v6}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 109
    new-instance v0, Lorg/apache/http/conn/params/ConnPerRouteBean;

    iget v5, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->MAX_CON_PER_ROUTE:I

    invoke-direct {v0, v5}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    .line 110
    .local v0, "connPerRoute":Lorg/apache/http/conn/params/ConnPerRoute;
    invoke-static {v3, v0}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 111
    iget v5, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->MAX_TOTAL_CON:I

    invoke-static {v3, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 112
    new-instance v4, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 113
    .local v4, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    iput-boolean p1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->isHttpsEnabled:Z

    .line 115
    const-string v5, "Adv.Con.Manager"

    const-string v6, "initialize HttpClient Manager"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    if-eqz p1, :cond_0

    .line 118
    const-string v5, "Adv.Con.Manager"

    const-string v6, "getSecureHttpConnection : SSL Socket Factory is not setup."

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->setupSSLSocketFactory(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    iget-object v6, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mCustomSF:Lcom/sec/android/service/health/connectionmanager2/NewSSLSocketFactory;

    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getPort()I

    move-result v7

    invoke-direct {v2, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    .line 121
    .local v2, "httpsScheme":Lorg/apache/http/conn/scheme/Scheme;
    invoke-virtual {v4, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 123
    .end local v2    # "httpsScheme":Lorg/apache/http/conn/scheme/Scheme;
    :cond_0
    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getPort()I

    move-result v7

    invoke-direct {v1, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    .line 124
    .local v1, "http":Lorg/apache/http/conn/scheme/Scheme;
    invoke-virtual {v4, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 125
    new-instance v5, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v5, v3, v4}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->setClientConnectionManager(Lorg/apache/http/conn/ClientConnectionManager;)V

    .line 126
    new-instance v5, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v6

    invoke-direct {v5, v6, v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->setHttpClient(Lorg/apache/http/client/HttpClient;)V

    .line 127
    return-void
.end method

.method protected releaseConnection()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 150
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->setHttpClient(Lorg/apache/http/client/HttpClient;)V

    .line 151
    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->setClientConnectionManager(Lorg/apache/http/conn/ClientConnectionManager;)V

    .line 153
    return-void
.end method

.method protected setClientConnectionManager(Lorg/apache/http/conn/ClientConnectionManager;)V
    .locals 0
    .param p1, "cm"    # Lorg/apache/http/conn/ClientConnectionManager;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->cm:Lorg/apache/http/conn/ClientConnectionManager;

    .line 162
    return-void
.end method

.method protected setHttpClient(Lorg/apache/http/client/HttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 180
    return-void
.end method

.method protected setPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 234
    iput p1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->port:I

    .line 235
    return-void
.end method

.method protected setServerIp(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverIp"    # Ljava/lang/String;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->ServerIp:Ljava/lang/String;

    .line 217
    return-void
.end method

.method public setupSSLSocketFactory(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "keyName"    # Ljava/lang/String;
    .param p2, "keyStorePassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 239
    const/4 v5, 0x0

    .line 240
    .local v5, "tm":[Ljavax/net/ssl/TrustManager;
    const/4 v2, 0x0

    .line 242
    .local v2, "km":[Ljavax/net/ssl/KeyManager;
    const-string v8, "Adv.Con.Manager"

    const-string/jumbo v9, "setupSSLSocketFactory : Try to create SSL Socket Factory"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :try_start_0
    const-string v8, "TLS"

    invoke-static {v8}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mSSLContext:Ljavax/net/ssl/SSLContext;

    .line 246
    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mTrustStore:Ljava/security/KeyStore;

    if-eqz v8, :cond_2

    .line 248
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v7

    .line 251
    .local v7, "tmf":Ljavax/net/ssl/TrustManagerFactory;
    const-string v8, "Adv.Con.Manager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Default Trust Manager Factory Alg is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mTrustStore:Ljava/security/KeyStore;

    invoke-virtual {v7, v8}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 253
    invoke-virtual {v7}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v5

    .line 254
    const-string v8, "Adv.Con.Manager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "X509TrustManager size is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v5

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    .end local v7    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    :goto_0
    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mClientKeyStore:Ljava/security/KeyStore;

    if-eqz v8, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 262
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v4

    .line 264
    .local v4, "kmf":Ljavax/net/ssl/KeyManagerFactory;
    const-string v8, "Adv.Con.Manager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Default Key Manager Factory Alg is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mClientKeyStore:Ljava/security/KeyStore;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V

    .line 267
    invoke-virtual {v4}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object v0

    .line 268
    .local v0, "defKM":[Ljavax/net/ssl/KeyManager;
    const/4 v8, 0x1

    new-array v3, v8, [Ljavax/net/ssl/KeyManager;

    const/4 v8, 0x0

    new-instance v9, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;

    iget-object v10, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mClientKeyStore:Ljava/security/KeyStore;

    invoke-direct {v9, v0, p1, v10, p2}, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;-><init>([Ljavax/net/ssl/KeyManager;Ljava/lang/String;Ljava/security/KeyStore;Ljava/lang/String;)V

    aput-object v9, v3, v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    .end local v2    # "km":[Ljavax/net/ssl/KeyManager;
    .local v3, "km":[Ljavax/net/ssl/KeyManager;
    :try_start_1
    const-string v8, "Adv.Con.Manager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "X509KeyManager size is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 271
    .end local v0    # "defKM":[Ljavax/net/ssl/KeyManager;
    .end local v3    # "km":[Ljavax/net/ssl/KeyManager;
    .end local v4    # "kmf":Ljavax/net/ssl/KeyManagerFactory;
    .restart local v2    # "km":[Ljavax/net/ssl/KeyManager;
    :cond_0
    :try_start_2
    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mSSLContext:Ljavax/net/ssl/SSLContext;

    const/4 v9, 0x0

    invoke-virtual {v8, v2, v5, v9}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 272
    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mCustomSF:Lcom/sec/android/service/health/connectionmanager2/NewSSLSocketFactory;

    if-nez v8, :cond_1

    .line 274
    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NewSSLSocketFactory;

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mSSLContext:Ljavax/net/ssl/SSLContext;

    invoke-direct {v8, v9}, Lcom/sec/android/service/health/connectionmanager2/NewSSLSocketFactory;-><init>(Ljavax/net/ssl/SSLContext;)V

    iput-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->mCustomSF:Lcom/sec/android/service/health/connectionmanager2/NewSSLSocketFactory;

    .line 281
    :cond_1
    return-void

    .line 258
    :cond_2
    const/4 v8, 0x1

    new-array v6, v8, [Ljavax/net/ssl/TrustManager;

    const/4 v8, 0x0

    new-instance v9, Lcom/sec/android/service/health/connectionmanager2/ConnectionTrustManager;

    invoke-direct {v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionTrustManager;-><init>()V

    aput-object v9, v6, v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .end local v5    # "tm":[Ljavax/net/ssl/TrustManager;
    .local v6, "tm":[Ljavax/net/ssl/TrustManager;
    move-object v5, v6

    .end local v6    # "tm":[Ljavax/net/ssl/TrustManager;
    .restart local v5    # "tm":[Ljavax/net/ssl/TrustManager;
    goto :goto_0

    .line 277
    :catch_0
    move-exception v1

    .line 279
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v9, -0x1

    const/16 v10, -0x1b

    invoke-direct {v8, v9, v10}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    throw v8

    .line 277
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "km":[Ljavax/net/ssl/KeyManager;
    .restart local v0    # "defKM":[Ljavax/net/ssl/KeyManager;
    .restart local v3    # "km":[Ljavax/net/ssl/KeyManager;
    .restart local v4    # "kmf":Ljavax/net/ssl/KeyManagerFactory;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "km":[Ljavax/net/ssl/KeyManager;
    .restart local v2    # "km":[Ljavax/net/ssl/KeyManager;
    goto :goto_1
.end method
