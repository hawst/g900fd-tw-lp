.class Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$3;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onReceived dataType= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ;extra "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onReceived dataType= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStarted(II)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted dataType= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ;error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStopped(II)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStopped dataType= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService$3;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearSensorService;->deinitialize()V

    return-void
.end method
