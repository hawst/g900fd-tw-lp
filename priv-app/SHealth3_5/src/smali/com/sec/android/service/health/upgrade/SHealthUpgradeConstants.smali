.class public Lcom/sec/android/service/health/upgrade/SHealthUpgradeConstants;
.super Ljava/lang/Object;


# static fields
.field public static final ACTION_CONNECTIVITY_CHANGE:Ljava/lang/String; = "android.net.conn.CONNECTIVITY_CHANGE"

.field public static final ACTION_PACKAGE_INSTALL:Ljava/lang/String; = "com.sec.android.service.health.upgrade.PackageInstall"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_APPID:Ljava/lang/String; = "appId"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_CONTENT_SIZE:Ljava/lang/String; = "contentSize"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_DOWNLOAD_URI:Ljava/lang/String; = "downloadURI"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_PRODUCT_ID:Ljava/lang/String; = "productId"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_PRODUCT_NAME:Ljava/lang/String; = "productName"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_RESULT_CODE:Ljava/lang/String; = "resultCode"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_RESULT_MESSAGE:Ljava/lang/String; = "resultMsg"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_VERSION_CODE:Ljava/lang/String; = "versionCode"

.field public static final ACTIVATION_STUB_DOWNLOAD_PARAMETER_VERSION_NAME:Ljava/lang/String; = "versionName"

.field public static final APK_DOWNLOAD_DONE:I = 0x0

.field public static final APK_DOWNLOAD_FAIL:I = 0x1

.field public static final APK_NAME_HEALTH_SERVICE:Ljava/lang/String; = "HealthService.apk"

.field public static final APK_NAME_SHEALTH:Ljava/lang/String; = "SHealth3.apk"

.field public static final APK_NAME_SHELATH_FOR_GEAR:Ljava/lang/String; = "SHealth3-For-Gear.apk"

.field public static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field public static final DONT_DELETE_DATA:I = 0x1

.field public static final DOWNLOAD_CONNECTION_TIMEOUT:I = 0x2710

.field public static final DOWNLOAD_SERVER_URL:Ljava/lang/String; = "https://vas.samsungapps.com/stub/stubDownload.as"

.field public static final DOWNLOAD_SOCKET_TIMEOUT:I = 0x7530

.field public static final EXTERNEL_STORAGE_PATH:Ljava/lang/String;

.field public static final FITNESS_WITH_GEAR_UPGRADE_FLAG:Ljava/lang/String; = "true"

.field public static final FITNESS_WITH_GEAR_UPGRADE_FLAG_FILE:Ljava/lang/String; = "FitnessWithGearVersionFlag.xml"

.field public static final FITNESS_WITH_GEAR_UPGRADE_FLAG_FILE_PATH:Ljava/lang/String;

.field public static final FITNESS_WITH_GEAR_UPGRADE_MARK_FLAG:Ljava/lang/String; = "true"

.field public static final FITNESS_WITH_GEAR_UPGRADE_MARK_FLAG_FILE:Ljava/lang/String; = "FitnessWithGearUpgradeMarkFlag.xml"

.field public static final INSTALL_FAILED_ALREADY_EXISTS:I = -0x1

.field public static final INSTALL_FAILED_CONFLICTING_PROVIDER:I = -0xd

.field public static final INSTALL_FAILED_CONTAINER_ERROR:I = -0x12

.field public static final INSTALL_FAILED_CPU_ABI_INCOMPATIBLE:I = -0x10

.field public static final INSTALL_FAILED_DEXOPT:I = -0xb

.field public static final INSTALL_FAILED_DUPLICATE_PACKAGE:I = -0x5

.field public static final INSTALL_FAILED_INSUFFICIENT_STORAGE:I = -0x4

.field public static final INSTALL_FAILED_INTERNAL_ERROR:I = -0x6e

.field public static final INSTALL_FAILED_INVALID_APK:I = -0x2

.field public static final INSTALL_FAILED_INVALID_INSTALL_LOCATION:I = -0x13

.field public static final INSTALL_FAILED_INVALID_URI:I = -0x3

.field public static final INSTALL_FAILED_MEDIA_UNAVAILABLE:I = -0x14

.field public static final INSTALL_FAILED_MISSING_FEATURE:I = -0x11

.field public static final INSTALL_FAILED_MISSING_SHARED_LIBRARY:I = -0x9

.field public static final INSTALL_FAILED_NEWER_SDK:I = -0xe

.field public static final INSTALL_FAILED_NO_SHARED_USER:I = -0x6

.field public static final INSTALL_FAILED_OLDER_SDK:I = -0xc

.field public static final INSTALL_FAILED_REPLACE_COULDNT_DELETE:I = -0xa

.field public static final INSTALL_FAILED_SHARED_USER_INCOMPATIBLE:I = -0x8

.field public static final INSTALL_FAILED_TEST_ONLY:I = -0xf

.field public static final INSTALL_FAILED_UPDATE_INCOMPATIBLE:I = -0x7

.field public static final INSTALL_PARSE_FAILED_BAD_MANIFEST:I = -0x65

.field public static final INSTALL_PARSE_FAILED_BAD_PACKAGE_NAME:I = -0x6a

.field public static final INSTALL_PARSE_FAILED_BAD_SHARED_USER_ID:I = -0x6b

.field public static final INSTALL_PARSE_FAILED_CERTIFICATE_ENCODING:I = -0x69

.field public static final INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES:I = -0x68

.field public static final INSTALL_PARSE_FAILED_MANIFEST_EMPTY:I = -0x6d

.field public static final INSTALL_PARSE_FAILED_MANIFEST_MALFORMED:I = -0x6c

.field public static final INSTALL_PARSE_FAILED_NOT_APK:I = -0x64

.field public static final INSTALL_PARSE_FAILED_NO_CERTIFICATES:I = -0x67

.field public static final INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION:I = -0x66

.field public static final INSTALL_REPLACE_EXISTING:I = 0x2

.field public static final INSTALL_SUCCEEDED:I = 0x1

.field public static final MESSAGE_UPGRADE_HEALTH_SERVICE_REQUIRED:I = 0x163

.field public static final MESSAGE_UPGRADE_NOT_REQUIRED:I = 0xff

.field public static final METHOD_NAME_DELETE:Ljava/lang/String; = "deletePackage"

.field public static final METHOD_NAME_INSTALL:Ljava/lang/String; = "installPackage"

.field public static final MIGRATION_FROM_FITNESS_WITH_GEAR:I = 0x1

.field public static final MIGRATION_FROM_SHEALTH_2_5:I = 0x3

.field public static final MIGRATION_FROM_SHEALTH_2_5_WITH_FITNESS_WITH_GEAR:I = 0x2

.field public static final MIGRATION_FROM_SHEALTH_3_x:I = 0x4

.field public static final MIGRATION_NOT_REQUIRED:I = 0x0

.field public static final MIGRATION_PREF_FILE:Ljava/lang/String; = "MigrationSharedPreference"

.field public static final MIGRATION_PREF_FLAG:Ljava/lang/String; = "MigrationState"

.field public static final NULL_STRING:Ljava/lang/String; = ""

.field public static final PACKAGE_NAME_FITNESS_WITH_GEAR:Ljava/lang/String; = "com.sec.android.app.shealthlite"

.field public static final PACKAGE_NAME_HEALTH_SERVICE:Ljava/lang/String; = "com.sec.android.service.health"

.field public static final PACKAGE_NAME_SAMSUNGAPPS:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final PACKAGE_NAME_SAMSUNGAPPS_MAIN:Ljava/lang/String; = "com.sec.android.app.samsungapps.Main"

.field public static final PACKAGE_NAME_SENSOR_SERVICE:Ljava/lang/String; = "com.sec.android.service.health.sensor"

.field public static final PACKAGE_NAME_SHEALTH:Ljava/lang/String; = "com.sec.android.app.shealth"

.field public static final PACKAGE_NAME_SLEEP_MONITOR:Ljava/lang/String; = "com.sec.android.app.shealth.plugins.sleepmonitor"

.field public static final READ_BUFFER_LEN:I = 0x400

.field public static final SHARED_PREF_FILED_GENDER:Ljava/lang/String; = "gender"

.field public static final SHARED_PREF_HEALTH_INFO_2_5:Ljava/lang/String; = "com.sec.android.app.shealth_preferences"

.field public static final SHEALTH_FOR_GEAR_FLAG:Ljava/lang/String; = "true"

.field public static final SHEALTH_FOR_GEAR_FLAG_FILE:Ljava/lang/String; = "ShealthForGearVersionFlag.xml"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeConstants;->EXTERNEL_STORAGE_PATH:Ljava/lang/String;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeConstants;->FITNESS_WITH_GEAR_UPGRADE_FLAG_FILE_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
