.class public Lcom/sec/android/service/health/contentservice/ContentServiceReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static REQUEST_HEALTH_ANDROID_ID:Ljava/lang/String;

.field private static RESPONSE_HEALTH_ANDROID_ID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/sec/android/service/health/contentservice/ContentServiceReceiver;->eCC()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/contentservice/ContentServiceReceiver;->RESPONSE_HEALTH_ANDROID_ID:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/service/health/contentservice/ContentServiceReceiver;->qlmowoO3r9()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/contentservice/ContentServiceReceiver;->REQUEST_HEALTH_ANDROID_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static eCC()Ljava/lang/String;
    .locals 60

    const/16 v0, 0x39

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, -0x4b

    aput v58, v1, v57

    const/16 v57, 0x185d

    aput v57, v1, v56

    const/16 v56, 0x6e47

    aput v56, v1, v55

    const/16 v55, 0x5a2a

    aput v55, v1, v54

    const/16 v54, 0x4113

    aput v54, v1, v53

    const/16 v53, -0x1ef2

    aput v53, v1, v52

    const/16 v52, -0x4d

    aput v52, v1, v51

    const/16 v51, -0x6f

    aput v51, v1, v50

    const/16 v50, -0x45

    aput v50, v1, v49

    const/16 v49, -0x10

    aput v49, v1, v48

    const/16 v48, -0x75b9

    aput v48, v1, v47

    const/16 v47, -0x3e

    aput v47, v1, v46

    const/16 v46, -0x5d

    aput v46, v1, v45

    const/16 v45, 0x615c

    aput v45, v1, v44

    const/16 v44, 0x720

    aput v44, v1, v43

    const/16 v43, 0x1b42

    aput v43, v1, v42

    const/16 v42, 0x3b53

    aput v42, v1, v41

    const/16 v41, 0xc64

    aput v41, v1, v40

    const/16 v40, -0x5bb7

    aput v40, v1, v39

    const/16 v39, -0x9

    aput v39, v1, v38

    const/16 v38, -0x27

    aput v38, v1, v37

    const/16 v37, -0x47

    aput v37, v1, v36

    const/16 v36, -0x73

    aput v36, v1, v35

    const/16 v35, -0x22

    aput v35, v1, v34

    const/16 v34, -0x37

    aput v34, v1, v33

    const/16 v33, -0x70

    aput v33, v1, v32

    const/16 v32, -0x35

    aput v32, v1, v31

    const/16 v31, -0x8d9

    aput v31, v1, v30

    const/16 v30, -0x7d

    aput v30, v1, v29

    const/16 v29, -0x5b

    aput v29, v1, v28

    const/16 v28, -0x27ba

    aput v28, v1, v27

    const/16 v27, -0x43

    aput v27, v1, v26

    const/16 v26, -0x3b

    aput v26, v1, v25

    const/16 v25, -0x42

    aput v25, v1, v24

    const/16 v24, 0x7844

    aput v24, v1, v23

    const/16 v23, 0x1b1c

    aput v23, v1, v22

    const/16 v22, -0x6698

    aput v22, v1, v21

    const/16 v21, -0x49

    aput v21, v1, v20

    const/16 v20, -0x18

    aput v20, v1, v19

    const/16 v19, -0x7f

    aput v19, v1, v18

    const/16 v18, -0x71

    aput v18, v1, v17

    const/16 v17, -0x2b

    aput v17, v1, v16

    const/16 v16, -0x52

    aput v16, v1, v15

    const/16 v15, -0x69

    aput v15, v1, v14

    const/16 v14, -0x5a

    aput v14, v1, v13

    const/16 v13, -0xb

    aput v13, v1, v12

    const/16 v12, -0x80

    aput v12, v1, v11

    const/16 v11, -0x62f4

    aput v11, v1, v10

    const/16 v10, -0x18

    aput v10, v1, v9

    const/16 v9, 0x5751

    aput v9, v1, v8

    const/16 v8, 0x73a

    aput v8, v1, v7

    const/16 v7, -0x2b9a

    aput v7, v1, v6

    const/16 v6, -0x59

    aput v6, v1, v5

    const/16 v5, 0x631d

    aput v5, v1, v4

    const/16 v4, 0x350e

    aput v4, v1, v3

    const/16 v3, -0x53a6

    aput v3, v1, v2

    const/16 v2, -0x31

    aput v2, v1, v0

    const/16 v0, 0x39

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, -0xf

    aput v59, v0, v58

    const/16 v58, 0x1814

    aput v58, v0, v57

    const/16 v57, 0x6e18

    aput v57, v0, v56

    const/16 v56, 0x5a6e

    aput v56, v0, v55

    const/16 v55, 0x415a

    aput v55, v0, v54

    const/16 v54, -0x1ebf

    aput v54, v0, v53

    const/16 v53, -0x1f

    aput v53, v0, v52

    const/16 v52, -0x2b

    aput v52, v0, v51

    const/16 v51, -0xb

    aput v51, v0, v50

    const/16 v50, -0x4f

    aput v50, v0, v49

    const/16 v49, -0x75e8

    aput v49, v0, v48

    const/16 v48, -0x76

    aput v48, v0, v47

    const/16 v47, -0x9

    aput v47, v0, v46

    const/16 v46, 0x6110

    aput v46, v0, v45

    const/16 v45, 0x761

    aput v45, v0, v44

    const/16 v44, 0x1b07

    aput v44, v0, v43

    const/16 v43, 0x3b1b

    aput v43, v0, v42

    const/16 v42, 0xc3b

    aput v42, v0, v41

    const/16 v41, -0x5bf4

    aput v41, v0, v40

    const/16 v40, -0x5c

    aput v40, v0, v39

    const/16 v39, -0x69

    aput v39, v0, v38

    const/16 v38, -0xa

    aput v38, v0, v37

    const/16 v37, -0x23

    aput v37, v0, v36

    const/16 v36, -0x73

    aput v36, v0, v35

    const/16 v35, -0x74

    aput v35, v0, v34

    const/16 v34, -0x3e

    aput v34, v0, v33

    const/16 v33, -0x1b

    aput v33, v0, v32

    const/16 v32, -0x8b1

    aput v32, v0, v31

    const/16 v31, -0x9

    aput v31, v0, v30

    const/16 v30, -0x37

    aput v30, v0, v29

    const/16 v29, -0x27d9

    aput v29, v0, v28

    const/16 v28, -0x28

    aput v28, v0, v27

    const/16 v27, -0x53

    aput v27, v0, v26

    const/16 v26, -0x70

    aput v26, v0, v25

    const/16 v25, 0x782f

    aput v25, v0, v24

    const/16 v24, 0x1b78

    aput v24, v0, v23

    const/16 v23, -0x66e5

    aput v23, v0, v22

    const/16 v22, -0x67

    aput v22, v0, v21

    const/16 v21, -0x74

    aput v21, v0, v20

    const/16 v20, -0x18

    aput v20, v0, v19

    const/16 v19, -0x20

    aput v19, v0, v18

    const/16 v18, -0x59

    aput v18, v0, v17

    const/16 v17, -0x36

    aput v17, v0, v16

    const/16 v16, -0x7

    aput v16, v0, v15

    const/16 v15, -0x39

    aput v15, v0, v14

    const/16 v14, -0x25

    aput v14, v0, v13

    const/16 v13, -0x19

    aput v13, v0, v12

    const/16 v12, -0x629e

    aput v12, v0, v11

    const/16 v11, -0x63

    aput v11, v0, v10

    const/16 v10, 0x5722

    aput v10, v0, v9

    const/16 v9, 0x757

    aput v9, v0, v8

    const/16 v8, -0x2bf9

    aput v8, v0, v7

    const/16 v7, -0x2c

    aput v7, v0, v6

    const/16 v6, 0x6333

    aput v6, v0, v5

    const/16 v5, 0x3563

    aput v5, v0, v4

    const/16 v4, -0x53cb

    aput v4, v0, v3

    const/16 v3, -0x54

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static hmmuVkr4Lu()Ljava/lang/String;
    .locals 60

    const/16 v0, 0x39

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, -0x65

    aput v58, v1, v57

    const/16 v57, -0x43a6

    aput v57, v1, v56

    const/16 v56, -0x1d

    aput v56, v1, v55

    const/16 v55, -0x7c

    aput v55, v1, v54

    const/16 v54, -0x22

    aput v54, v1, v53

    const/16 v53, -0x3c

    aput v53, v1, v52

    const/16 v52, -0x7e

    aput v52, v1, v51

    const/16 v51, -0x2

    aput v51, v1, v50

    const/16 v50, -0x20

    aput v50, v1, v49

    const/16 v49, 0x7359

    aput v49, v1, v48

    const/16 v48, -0x6dd4

    aput v48, v1, v47

    const/16 v47, -0x26

    aput v47, v1, v46

    const/16 v46, -0x2292

    aput v46, v1, v45

    const/16 v45, -0x6f

    aput v45, v1, v44

    const/16 v44, -0x7d

    aput v44, v1, v43

    const/16 v43, -0x44

    aput v43, v1, v42

    const/16 v42, -0x1f9a

    aput v42, v1, v41

    const/16 v41, -0x41

    aput v41, v1, v40

    const/16 v40, 0x6374

    aput v40, v1, v39

    const/16 v39, 0x4630

    aput v39, v1, v38

    const/16 v38, 0x4808

    aput v38, v1, v37

    const/16 v37, -0x79f9

    aput v37, v1, v36

    const/16 v36, -0x2a

    aput v36, v1, v35

    const/16 v35, -0x5f

    aput v35, v1, v34

    const/16 v34, -0x2f

    aput v34, v1, v33

    const/16 v33, -0x32

    aput v33, v1, v32

    const/16 v32, -0x56

    aput v32, v1, v31

    const/16 v31, -0x1ab2

    aput v31, v1, v30

    const/16 v30, -0x6f

    aput v30, v1, v29

    const/16 v29, -0x52

    aput v29, v1, v28

    const/16 v28, -0x14

    aput v28, v1, v27

    const/16 v27, 0x397c

    aput v27, v1, v26

    const/16 v26, -0x5af

    aput v26, v1, v25

    const/16 v25, -0x2c

    aput v25, v1, v24

    const/16 v24, -0x6a

    aput v24, v1, v23

    const/16 v23, 0x1f67

    aput v23, v1, v22

    const/16 v22, -0x5a94

    aput v22, v1, v21

    const/16 v21, -0x75

    aput v21, v1, v20

    const/16 v20, 0x909

    aput v20, v1, v19

    const/16 v19, -0x67a0

    aput v19, v1, v18

    const/16 v18, -0x9

    aput v18, v1, v17

    const/16 v17, -0x3c

    aput v17, v1, v16

    const/16 v16, -0x65

    aput v16, v1, v15

    const/16 v15, -0x5f

    aput v15, v1, v14

    const/16 v14, 0x1211

    aput v14, v1, v13

    const/16 v13, -0x5bc4

    aput v13, v1, v12

    const/16 v12, -0x3d

    aput v12, v1, v11

    const/16 v11, -0x73

    aput v11, v1, v10

    const/16 v10, -0x6a2

    aput v10, v1, v9

    const/16 v9, -0x76

    aput v9, v1, v8

    const/16 v8, -0x7b

    aput v8, v1, v7

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, -0x10

    aput v6, v1, v5

    const/16 v5, -0x68ac

    aput v5, v1, v4

    const/4 v4, -0x6

    aput v4, v1, v3

    const/16 v3, -0x50

    aput v3, v1, v2

    const/16 v2, 0x772f

    aput v2, v1, v0

    const/16 v0, 0x39

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, -0x21

    aput v59, v0, v58

    const/16 v58, -0x43ed

    aput v58, v0, v57

    const/16 v57, -0x44

    aput v57, v0, v56

    const/16 v56, -0x40

    aput v56, v0, v55

    const/16 v55, -0x69

    aput v55, v0, v54

    const/16 v54, -0x75

    aput v54, v0, v53

    const/16 v53, -0x30

    aput v53, v0, v52

    const/16 v52, -0x46

    aput v52, v0, v51

    const/16 v51, -0x52

    aput v51, v0, v50

    const/16 v50, 0x7318

    aput v50, v0, v49

    const/16 v49, -0x6d8d

    aput v49, v0, v48

    const/16 v48, -0x6e

    aput v48, v0, v47

    const/16 v47, -0x22c6

    aput v47, v0, v46

    const/16 v46, -0x23

    aput v46, v0, v45

    const/16 v45, -0x3e

    aput v45, v0, v44

    const/16 v44, -0x7

    aput v44, v0, v43

    const/16 v43, -0x1fd2

    aput v43, v0, v42

    const/16 v42, -0x20

    aput v42, v0, v41

    const/16 v41, 0x6331

    aput v41, v0, v40

    const/16 v40, 0x4663

    aput v40, v0, v39

    const/16 v39, 0x4846

    aput v39, v0, v38

    const/16 v38, -0x79b8

    aput v38, v0, v37

    const/16 v37, -0x7a

    aput v37, v0, v36

    const/16 v36, -0xe

    aput v36, v0, v35

    const/16 v35, -0x6c

    aput v35, v0, v34

    const/16 v34, -0x64

    aput v34, v0, v33

    const/16 v33, -0x7c

    aput v33, v0, v32

    const/16 v32, -0x1ada

    aput v32, v0, v31

    const/16 v31, -0x1b

    aput v31, v0, v30

    const/16 v30, -0x3e

    aput v30, v0, v29

    const/16 v29, -0x73

    aput v29, v0, v28

    const/16 v28, 0x3919

    aput v28, v0, v27

    const/16 v27, -0x5c7

    aput v27, v0, v26

    const/16 v26, -0x6

    aput v26, v0, v25

    const/16 v25, -0x3

    aput v25, v0, v24

    const/16 v24, 0x1f03

    aput v24, v0, v23

    const/16 v23, -0x5ae1

    aput v23, v0, v22

    const/16 v22, -0x5b

    aput v22, v0, v21

    const/16 v21, 0x96d

    aput v21, v0, v20

    const/16 v20, -0x67f7

    aput v20, v0, v19

    const/16 v19, -0x68

    aput v19, v0, v18

    const/16 v18, -0x4a

    aput v18, v0, v17

    const/16 v17, -0x1

    aput v17, v0, v16

    const/16 v16, -0x31

    aput v16, v0, v15

    const/16 v15, 0x1270

    aput v15, v0, v14

    const/16 v14, -0x5bee

    aput v14, v0, v13

    const/16 v13, -0x5c

    aput v13, v0, v12

    const/16 v12, -0x1d

    aput v12, v0, v11

    const/16 v11, -0x6d5

    aput v11, v0, v10

    const/4 v10, -0x7

    aput v10, v0, v9

    const/16 v9, -0x18

    aput v9, v0, v8

    const/16 v8, -0xb

    aput v8, v0, v7

    const/16 v7, -0x7d

    aput v7, v0, v6

    const/16 v6, -0x6886

    aput v6, v0, v5

    const/16 v5, -0x69

    aput v5, v0, v4

    const/16 v4, -0x21

    aput v4, v0, v3

    const/16 v3, 0x774c

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static qTTSI3L()Ljava/lang/String;
    .locals 59

    const/16 v0, 0x38

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, -0x47e8

    aput v57, v1, v56

    const/16 v56, -0xf

    aput v56, v1, v55

    const/16 v55, -0x3a

    aput v55, v1, v54

    const/16 v54, -0x4b

    aput v54, v1, v53

    const/16 v53, -0x25f5

    aput v53, v1, v52

    const/16 v52, -0x6b

    aput v52, v1, v51

    const/16 v51, -0x55

    aput v51, v1, v50

    const/16 v50, -0x67

    aput v50, v1, v49

    const/16 v49, -0x49

    aput v49, v1, v48

    const/16 v48, -0x4

    aput v48, v1, v47

    const/16 v47, -0x79

    aput v47, v1, v46

    const/16 v46, -0x69bb

    aput v46, v1, v45

    const/16 v45, -0x3e

    aput v45, v1, v44

    const/16 v44, -0x26

    aput v44, v1, v43

    const/16 v43, 0x3e0f

    aput v43, v1, v42

    const/16 v42, 0x177b

    aput v42, v1, v41

    const/16 v41, -0x24a1

    aput v41, v1, v40

    const/16 v40, -0x7c

    aput v40, v1, v39

    const/16 v39, 0x1115

    aput v39, v1, v38

    const/16 v38, -0x56be

    aput v38, v1, v37

    const/16 v37, -0x14

    aput v37, v1, v36

    const/16 v36, 0x6a26

    aput v36, v1, v35

    const/16 v35, 0x563b

    aput v35, v1, v34

    const/16 v34, 0x5313

    aput v34, v1, v33

    const/16 v33, -0x62ff

    aput v33, v1, v32

    const/16 v32, -0x4d

    aput v32, v1, v31

    const/16 v31, -0x3dd6

    aput v31, v1, v30

    const/16 v30, -0x4a

    aput v30, v1, v29

    const/16 v29, -0x64a6

    aput v29, v1, v28

    const/16 v28, -0x6

    aput v28, v1, v27

    const/16 v27, -0x25d1

    aput v27, v1, v26

    const/16 v26, -0x4e

    aput v26, v1, v25

    const/16 v25, -0x32

    aput v25, v1, v24

    const/16 v24, -0x5c

    aput v24, v1, v23

    const/16 v23, -0x54d8

    aput v23, v1, v22

    const/16 v22, -0x28

    aput v22, v1, v21

    const/16 v21, -0x2d7

    aput v21, v1, v20

    const/16 v20, -0x67

    aput v20, v1, v19

    const/16 v19, -0x6bf

    aput v19, v1, v18

    const/16 v18, -0x6a

    aput v18, v1, v17

    const/16 v17, -0x25

    aput v17, v1, v16

    const/16 v16, 0x3179

    aput v16, v1, v15

    const/16 v15, 0x435f

    aput v15, v1, v14

    const/16 v14, -0x10de

    aput v14, v1, v13

    const/16 v13, -0x3f

    aput v13, v1, v12

    const/16 v12, -0x6df3

    aput v12, v1, v11

    const/4 v11, -0x4

    aput v11, v1, v10

    const/16 v10, -0xa

    aput v10, v1, v9

    const/16 v9, 0x4817

    aput v9, v1, v8

    const/16 v8, -0x6db

    aput v8, v1, v7

    const/16 v7, -0x68

    aput v7, v1, v6

    const/4 v6, -0x5

    aput v6, v1, v5

    const/16 v5, -0xaaa

    aput v5, v1, v4

    const/16 v4, -0x68

    aput v4, v1, v3

    const/16 v3, 0xa53

    aput v3, v1, v2

    const/16 v2, 0x4e69

    aput v2, v1, v0

    const/16 v0, 0x38

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, -0x47a4

    aput v58, v0, v57

    const/16 v57, -0x48

    aput v57, v0, v56

    const/16 v56, -0x67

    aput v56, v0, v55

    const/16 v55, -0xf

    aput v55, v0, v54

    const/16 v54, -0x25be

    aput v54, v0, v53

    const/16 v53, -0x26

    aput v53, v0, v52

    const/16 v52, -0x7

    aput v52, v0, v51

    const/16 v51, -0x23

    aput v51, v0, v50

    const/16 v50, -0x7

    aput v50, v0, v49

    const/16 v49, -0x43

    aput v49, v0, v48

    const/16 v48, -0x28

    aput v48, v0, v47

    const/16 v47, -0x69f3

    aput v47, v0, v46

    const/16 v46, -0x6a

    aput v46, v0, v45

    const/16 v45, -0x6a

    aput v45, v0, v44

    const/16 v44, 0x3e4e

    aput v44, v0, v43

    const/16 v43, 0x173e

    aput v43, v0, v42

    const/16 v42, -0x24e9

    aput v42, v0, v41

    const/16 v41, -0x25

    aput v41, v0, v40

    const/16 v40, 0x1141

    aput v40, v0, v39

    const/16 v39, -0x56ef

    aput v39, v0, v38

    const/16 v38, -0x57

    aput v38, v0, v37

    const/16 v37, 0x6a73

    aput v37, v0, v36

    const/16 v36, 0x566a    # 3.1E-41f

    aput v36, v0, v35

    const/16 v35, 0x5356

    aput v35, v0, v34

    const/16 v34, -0x62ad

    aput v34, v0, v33

    const/16 v33, -0x63

    aput v33, v0, v32

    const/16 v32, -0x3dbe

    aput v32, v0, v31

    const/16 v31, -0x3e

    aput v31, v0, v30

    const/16 v30, -0x64ca

    aput v30, v0, v29

    const/16 v29, -0x65

    aput v29, v0, v28

    const/16 v28, -0x25b6

    aput v28, v0, v27

    const/16 v27, -0x26

    aput v27, v0, v26

    const/16 v26, -0x20

    aput v26, v0, v25

    const/16 v25, -0x31

    aput v25, v0, v24

    const/16 v24, -0x54b4

    aput v24, v0, v23

    const/16 v23, -0x55

    aput v23, v0, v22

    const/16 v22, -0x2f9

    aput v22, v0, v21

    const/16 v21, -0x3

    aput v21, v0, v20

    const/16 v20, -0x6d8

    aput v20, v0, v19

    const/16 v19, -0x7

    aput v19, v0, v18

    const/16 v18, -0x57

    aput v18, v0, v17

    const/16 v17, 0x311d

    aput v17, v0, v16

    const/16 v16, 0x4331

    aput v16, v0, v15

    const/16 v15, -0x10bd

    aput v15, v0, v14

    const/16 v14, -0x11

    aput v14, v0, v13

    const/16 v13, -0x6d96

    aput v13, v0, v12

    const/16 v12, -0x6e

    aput v12, v0, v11

    const/16 v11, -0x7d

    aput v11, v0, v10

    const/16 v10, 0x4864

    aput v10, v0, v9

    const/16 v9, -0x6b8

    aput v9, v0, v8

    const/4 v8, -0x7

    aput v8, v0, v7

    const/16 v7, -0x78

    aput v7, v0, v6

    const/16 v6, -0xa88

    aput v6, v0, v5

    const/16 v5, -0xb

    aput v5, v0, v4

    const/16 v4, 0xa3c

    aput v4, v0, v3

    const/16 v3, 0x4e0a

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static qlmowoO3r9()Ljava/lang/String;
    .locals 59

    const/16 v0, 0x38

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, -0x63

    aput v57, v1, v56

    const/16 v56, -0x4ddd

    aput v56, v1, v55

    const/16 v55, -0x13

    aput v55, v1, v54

    const/16 v54, -0x5f

    aput v54, v1, v53

    const/16 v53, -0x9

    aput v53, v1, v52

    const/16 v52, -0x79bc

    aput v52, v1, v51

    const/16 v51, -0x2c

    aput v51, v1, v50

    const/16 v50, -0x4f9d

    aput v50, v1, v49

    const/16 v49, -0x2

    aput v49, v1, v48

    const/16 v48, -0x1288

    aput v48, v1, v47

    const/16 v47, -0x4e

    aput v47, v1, v46

    const/16 v46, 0x620e

    aput v46, v1, v45

    const/16 v45, -0x5aca

    aput v45, v1, v44

    const/16 v44, -0x17

    aput v44, v1, v43

    const/16 v43, -0x1

    aput v43, v1, v42

    const/16 v42, -0x1e9f

    aput v42, v1, v41

    const/16 v41, -0x57

    aput v41, v1, v40

    const/16 v40, 0x5836

    aput v40, v1, v39

    const/16 v39, -0x1ff4

    aput v39, v1, v38

    const/16 v38, -0x4d

    aput v38, v1, v37

    const/16 v37, 0x2e57

    aput v37, v1, v36

    const/16 v36, 0x5c7b

    aput v36, v1, v35

    const/16 v35, 0x520d

    aput v35, v1, v34

    const/16 v34, -0x2ce9

    aput v34, v1, v33

    const/16 v33, -0x7f

    aput v33, v1, v32

    const/16 v32, -0x72ce

    aput v32, v1, v31

    const/16 v31, -0x1b

    aput v31, v1, v30

    const/16 v30, -0x789d

    aput v30, v1, v29

    const/16 v29, -0x15

    aput v29, v1, v28

    const/16 v28, 0x4148

    aput v28, v1, v27

    const/16 v27, -0x58dc

    aput v27, v1, v26

    const/16 v26, -0x31

    aput v26, v1, v25

    const/16 v25, -0x1a

    aput v25, v1, v24

    const/16 v24, 0x2474

    aput v24, v1, v23

    const/16 v23, -0x75c0

    aput v23, v1, v22

    const/16 v22, -0x7

    aput v22, v1, v21

    const/16 v21, 0x6001

    aput v21, v1, v20

    const/16 v20, 0x4f04

    aput v20, v1, v19

    const/16 v19, -0x30da

    aput v19, v1, v18

    const/16 v18, -0x60

    aput v18, v1, v17

    const/16 v17, 0x3a6d

    aput v17, v1, v16

    const/16 v16, -0x22a2

    aput v16, v1, v15

    const/16 v15, -0x4d

    aput v15, v1, v14

    const/16 v14, -0x53

    aput v14, v1, v13

    const/16 v13, -0x58bf

    aput v13, v1, v12

    const/16 v12, -0x40

    aput v12, v1, v11

    const/16 v11, 0x4b58

    aput v11, v1, v10

    const/16 v10, 0x6f3e

    aput v10, v1, v9

    const/16 v9, 0x1f1c

    aput v9, v1, v8

    const/16 v8, 0x7272

    aput v8, v1, v7

    const/16 v7, 0x13

    aput v7, v1, v6

    const/16 v6, -0x3e8d

    aput v6, v1, v5

    const/16 v5, -0x11

    aput v5, v1, v4

    const/16 v4, 0x452e

    aput v4, v1, v3

    const/16 v3, -0x53d6

    aput v3, v1, v2

    const/16 v2, -0x31

    aput v2, v1, v0

    const/16 v0, 0x38

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, -0x27

    aput v58, v0, v57

    const/16 v57, -0x4d96

    aput v57, v0, v56

    const/16 v56, -0x4e

    aput v56, v0, v55

    const/16 v55, -0x1b

    aput v55, v0, v54

    const/16 v54, -0x42

    aput v54, v0, v53

    const/16 v53, -0x79f5

    aput v53, v0, v52

    const/16 v52, -0x7a

    aput v52, v0, v51

    const/16 v51, -0x4fd9

    aput v51, v0, v50

    const/16 v50, -0x50

    aput v50, v0, v49

    const/16 v49, -0x12c7

    aput v49, v0, v48

    const/16 v48, -0x13

    aput v48, v0, v47

    const/16 v47, 0x6246

    aput v47, v0, v46

    const/16 v46, -0x5a9e

    aput v46, v0, v45

    const/16 v45, -0x5b

    aput v45, v0, v44

    const/16 v44, -0x42

    aput v44, v0, v43

    const/16 v43, -0x1edc

    aput v43, v0, v42

    const/16 v42, -0x1f

    aput v42, v0, v41

    const/16 v41, 0x5869

    aput v41, v0, v40

    const/16 v40, -0x1fa8

    aput v40, v0, v39

    const/16 v39, -0x20

    aput v39, v0, v38

    const/16 v38, 0x2e12

    aput v38, v0, v37

    const/16 v37, 0x5c2e

    aput v37, v0, v36

    const/16 v36, 0x525c

    aput v36, v0, v35

    const/16 v35, -0x2cae

    aput v35, v0, v34

    const/16 v34, -0x2d

    aput v34, v0, v33

    const/16 v33, -0x72e4

    aput v33, v0, v32

    const/16 v32, -0x73

    aput v32, v0, v31

    const/16 v31, -0x78e9

    aput v31, v0, v30

    const/16 v30, -0x79

    aput v30, v0, v29

    const/16 v29, 0x4129

    aput v29, v0, v28

    const/16 v28, -0x58bf

    aput v28, v0, v27

    const/16 v27, -0x59

    aput v27, v0, v26

    const/16 v26, -0x38

    aput v26, v0, v25

    const/16 v25, 0x241f

    aput v25, v0, v24

    const/16 v24, -0x75dc

    aput v24, v0, v23

    const/16 v23, -0x76

    aput v23, v0, v22

    const/16 v22, 0x602f

    aput v22, v0, v21

    const/16 v21, 0x4f60

    aput v21, v0, v20

    const/16 v20, -0x30b1

    aput v20, v0, v19

    const/16 v19, -0x31

    aput v19, v0, v18

    const/16 v18, 0x3a1f

    aput v18, v0, v17

    const/16 v17, -0x22c6

    aput v17, v0, v16

    const/16 v16, -0x23

    aput v16, v0, v15

    const/16 v15, -0x34

    aput v15, v0, v14

    const/16 v14, -0x5891

    aput v14, v0, v13

    const/16 v13, -0x59

    aput v13, v0, v12

    const/16 v12, 0x4b36

    aput v12, v0, v11

    const/16 v11, 0x6f4b

    aput v11, v0, v10

    const/16 v10, 0x1f6f

    aput v10, v0, v9

    const/16 v9, 0x721f

    aput v9, v0, v8

    const/16 v8, 0x72

    aput v8, v0, v7

    const/16 v7, -0x3f00

    aput v7, v0, v6

    const/16 v6, -0x3f

    aput v6, v0, v5

    const/16 v5, 0x4543

    aput v5, v0, v4

    const/16 v4, -0x53bb

    aput v4, v0, v3

    const/16 v3, -0x54

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lcom/sec/android/service/health/contentservice/ContentServiceReceiver;->qTTSI3L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/service/health/contentservice/ContentServiceReceiver;->hmmuVkr4Lu()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xd6a

    aput v12, v1, v11

    const/16 v11, -0x149c

    aput v11, v1, v10

    const/16 v10, -0x4c

    aput v10, v1, v9

    const/16 v9, 0x527b

    aput v9, v1, v8

    const/16 v8, -0x11c5

    aput v8, v1, v7

    const/16 v7, -0x7f

    aput v7, v1, v6

    const/16 v6, -0x63

    aput v6, v1, v5

    const/16 v5, -0x46

    aput v5, v1, v4

    const/16 v4, 0x5a43

    aput v4, v1, v2

    const/16 v2, -0x1ac5

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xd0e

    aput v13, v0, v12

    const/16 v12, -0x14f3

    aput v12, v0, v11

    const/16 v11, -0x15

    aput v11, v0, v10

    const/16 v10, 0x521f

    aput v10, v0, v9

    const/16 v9, -0x11ae

    aput v9, v0, v8

    const/16 v8, -0x12

    aput v8, v0, v7

    const/16 v7, -0x11

    aput v7, v0, v6

    const/16 v6, -0x22

    aput v6, v0, v5

    const/16 v5, 0x5a2d

    aput v5, v0, v4

    const/16 v4, -0x1aa6

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_1

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method
