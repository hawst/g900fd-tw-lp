.class public final Lcom/sec/android/service/health/cp/common/DBConstants$GOAL_TYPE;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GOAL_TYPE"
.end annotation


# static fields
.field public static final BLOOD_GLUCOSE_AFTER_MEAL:I = 0x3

.field public static final BLOOD_GLUCOSE_EMPTY:I = 0x2

.field public static final BLOOD_PRESSURE_DIASTOLIC:I = 0x9

.field public static final BLOOD_PRESSURE_PULSE:I = 0xa

.field public static final BLOOD_PRESSURE_SYSTOLIC:I = 0x8

.field public static final COMFORT_ZONE:I = 0xe

.field public static final EXERCISE:I = 0x0

.field public static final EXERCISE_GOAL_VALUE:I = 0x9c4

.field public static final FOOD_BREAKFAST:I = 0x4

.field public static final FOOD_DINNER:I = 0x6

.field public static final FOOD_FOR_ALL_DAY:I = 0xf

.field public static final FOOD_LUNCH:I = 0x5

.field public static final FOOD_MORE_FAT:I = 0xf

.field public static final FOOD_MORE_TINY:I = 0x10

.field public static final ID:I = 0xd

.field public static final MEAL_GOAL_VALUE:I = 0x9c4

.field public static final WALKING_GOAL_VALUE:I = 0x2710

.field public static final WALK_RUNNING:I = 0xb

.field public static final WALK_UPDOWN:I = 0xd

.field public static final WALK_WALKING:I = 0xc

.field public static final WEIGHT:I = 0x1

.field public static final WEIGHT_HALF_KG_ONE_WEEK:I = 0x15

.field public static final WEIGHT_HALF_KG_TWO_WEEKS:I = 0x17

.field public static final WEIGHT_ONE_KG_ONE_WEEK:I = 0x16

.field public static final WEIGHT_ONE_KG_TWO_WEEKS:I = 0x18


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
