.class Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WearableReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->closeSocketConnection()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$600(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "intent is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v5}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onReceive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "com.samsung.android.shealth.SYNC_ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.samsung.android.shealth.GEAR_SYNC_ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.samsung.android.shealth.SBAND_SYNC_ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.samsung.android.shealth.GEAR_SYNC_ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    invoke-static {p2, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getDevieTypeFromIntent(Landroid/content/Intent;Landroid/content/Context;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    if-ne v1, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC_ERROR : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v5}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$000(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sendDataUpdatedToApp(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$500(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;II)V

    goto/16 :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "received SYNC_ERROR but not right device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\ndevice name & type"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->getInstance()Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;

    move-result-object v1

    const-string v2, "VERSION"

    const-wide/16 v3, 0x0

    invoke-virtual {p2, v2, v3, v4}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->protocolNegotication(D)D

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->getDataTranslatorFromProtocolVersion(DZ)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    move-result-object v1

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$702(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$700(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Protocol version is wrong"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v5}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$700(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/16 v3, 0x1388

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->makeMessageFromIntent(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Landroid/content/Intent;I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$800(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$100(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Handler sendMessage error"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v5}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$902(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;[B)[B

    goto/16 :goto_0

    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "send_data or msg is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # getter for: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$800(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;->this$0:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    # invokes: Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    invoke-static {v0, v5}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V

    goto :goto_1
.end method
