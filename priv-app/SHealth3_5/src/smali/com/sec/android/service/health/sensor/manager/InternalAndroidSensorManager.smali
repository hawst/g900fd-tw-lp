.class public Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;
.super Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;


# instance fields
.field private mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    return-object v0
.end method


# virtual methods
.method public create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->isPluginApp(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 8

    const/4 v3, 0x4

    invoke-super {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/hardware/SensorManager;

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Humidity - Device is initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    if-eqz v7, :cond_0

    const/16 v0, 0xc

    invoke-virtual {v7, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Humidity - Device is initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "HUMIDITY"

    const-string v2, "HUMIDITY"

    const/16 v4, 0x272b

    const/16 v5, 0x12

    const-string v6, "HUMIDITY_PROFILE"

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Temperature - Device is initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_1

    if-eqz v7, :cond_1

    const/16 v0, 0xd

    invoke-virtual {v7, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Temperature - Device is initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "TEMPERATURE"

    const-string v2, "TEMPERATURE"

    const/16 v4, 0x272c

    const/16 v5, 0x13

    const-string v6, "TEMPERATURE_PROFILE"

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_2

    if-eqz v7, :cond_2

    const v0, 0x1001d

    invoke-virtual {v7, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "UV RAY - Device is initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "UV-RAY"

    const-string v2, "UV-RAY"

    const/16 v4, 0x272d

    const/16 v5, 0x14

    const-string v6, "UVRAY_PROFILE"

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataTypeSupported(I)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_5
        0x4 -> :sswitch_0
        0x10 -> :sswitch_4
        0x11 -> :sswitch_0
        0x12 -> :sswitch_2
        0x13 -> :sswitch_1
        0x14 -> :sswitch_3
    .end sparse-switch
.end method

.method protected join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 2

    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "join is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->createDeviceHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Join Device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    return-void
.end method

.method protected remoteCallBackDied(Ljava/lang/Integer;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->cleanupOnDied(Ljava/lang/Integer;)V

    return-void
.end method

.method protected request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->request(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    return v0
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 8

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v4, p5

    move-wide v5, p6

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->startReceivingData(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public startScanning(Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "InternalSamsungSensorManager startScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Temperature device being returned"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Humidity device being returned"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHumidityDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "Body Temperature device being returned"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "UV device being returned"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mUvRayDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "HRM - startScanning is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->isPluginApp(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "SPO2 device being returned"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "ECG device being returned"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_6
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "BloodGlucose device being returned"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->onScanningStopped()V

    const/4 v0, 0x0

    return v0
.end method

.method public stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)I
    .locals 1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public stopScanning()Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "InternalSamsungSensorManager stopScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method
