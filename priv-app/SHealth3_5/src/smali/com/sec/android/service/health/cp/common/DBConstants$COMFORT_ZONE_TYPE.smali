.class public final Lcom/sec/android/service/health/cp/common/DBConstants$COMFORT_ZONE_TYPE;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "COMFORT_ZONE_TYPE"
.end annotation


# static fields
.field public static final ID:I = 0xb

.field public static final TEMPERATURE_UNIT_MARK:Ljava/lang/String; = "\ufffd\ufffd"

.field public static final UNIT_CELSIUS:Ljava/lang/String; = "C"

.field public static final UNIT_FAHRENHEIT:Ljava/lang/String; = "F"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
