.class public Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;
.super Ljava/lang/Object;


# static fields
.field private static final DEFAULT_BIRTH_DATE:Ljava/lang/String; = "19700101"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addProfile(Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v2, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;

    invoke-virtual {v0, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setProfileRestoreContents(Lcom/sec/android/service/health/cp/serversync/data/ProfileData;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/io/File;

    const-string v2, "/storage/emulated/0/SHealth3/cache/img/user_photo.jpg"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private static checkValidity(Lcom/sec/android/service/health/cp/serversync/data/ProfileData;)Z
    .locals 9

    const/4 v0, 0x1

    const v8, 0x461c4000    # 10000.0f

    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->activityType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntActivityType(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->gender:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntGenderType(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->heightUnit:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntUnit(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weightUnit:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntUnit(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->temperatureUnit:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntUnit(Ljava/lang/String;)I

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->country:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->country:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->name:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v0, :cond_3

    iget-object v5, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x32

    if-gt v5, v6, :cond_3

    const v5, 0x2e635

    if-eq v2, v5, :cond_0

    const v5, 0x2e636

    if-ne v2, v5, :cond_3

    :cond_0
    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->height:F

    cmpg-float v2, v2, v7

    if-ltz v2, :cond_3

    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->height:F

    cmpl-float v2, v2, v8

    if-gtz v2, :cond_3

    const v2, 0x249f1

    if-eq v3, v2, :cond_1

    const v2, 0x249f2

    if-ne v3, v2, :cond_3

    :cond_1
    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weight:F

    cmpg-float v2, v2, v7

    if-ltz v2, :cond_3

    iget v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weight:F

    cmpl-float v2, v2, v8

    if-gtz v2, :cond_3

    const v2, 0x1fbd1

    if-eq v4, v2, :cond_2

    const v2, 0x1fbd2

    if-ne v4, v2, :cond_3

    :cond_2
    const v2, 0x2bf24

    if-eq v1, v2, :cond_4

    const v2, 0x2bf22

    if-eq v1, v2, :cond_4

    const v2, 0x2bf21

    if-eq v1, v2, :cond_4

    const v2, 0x2bf23

    if-eq v1, v2, :cond_4

    const v2, 0x2bf25

    if-eq v1, v2, :cond_4

    :cond_3
    const/4 v0, 0x0

    :cond_4
    return v0
.end method

.method public static getDeletedProfileData(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/io/File;

    const-string v2, "/storage/emulated/0/SHealth3/cache/img/user_photo.jpg"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncPreference;->getLastProfileImageSyncedTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_USER_PROFILE_IMAGE:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public static getModifiedProfile(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfile()Lcom/sec/android/service/health/cp/serversync/data/ProfileData;

    move-result-object v6

    const-string v0, "1y90e30264"

    iput-object v0, v6, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->appId:Ljava/lang/String;

    const-string v0, "Y"

    iput-object v0, v6, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->syncYN:Ljava/lang/String;

    iget-object v0, v6, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_PROFILE:Ljava/lang/String;

    invoke-direct {v0, v4, v6, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v0, Ljava/io/File;

    const-string v4, "/storage/emulated/0/SHealth3/cache/img/user_photo.jpg"

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "profile image exists"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncPreference;->getLastProfileImageSyncedTime()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-wide/16 v6, 0x1

    move-object v5, v1

    move-wide v8, v2

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_PROFILE_IMAGE:Ljava/lang/String;

    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v6}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "profileImage"

    move-object v4, v1

    move-object v5, v11

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    return-object v10

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->TAG:Ljava/lang/String;

    const-string v4, "Cant backup profile since name is null"

    invoke-static {v0, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "already synced. modified : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", lastSyncedTime : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncPreference;->getLastProfileImageSyncedTime()J

    move-result-wide v3

    invoke-static {v3, v4, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "profile image does not exist"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getProfile()Lcom/sec/android/service/health/cp/serversync/data/ProfileData;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;-><init>()V

    const-string v1, "country"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->country:Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->country:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "XXX"

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->country:Ljava/lang/String;

    :cond_0
    const-string/jumbo v1, "name"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->name:Ljava/lang/String;

    const-string v1, "gender"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const v3, 0x2e635

    if-ne v2, v3, :cond_2

    const-string v1, "0"

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->gender:Ljava/lang/String;

    :cond_1
    :goto_0
    const-string v1, "height"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->height:F

    :goto_1
    const-string v1, "height_unit"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getStringUnit(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->heightUnit:Ljava/lang/String;

    :goto_2
    const-string/jumbo v1, "weight"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weight:F

    :goto_3
    const-string/jumbo v1, "weight_unit"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getStringUnit(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weightUnit:Ljava/lang/String;

    :goto_4
    const-string v1, "birth_date"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->birthDate:Ljava/lang/String;

    :goto_5
    const-string v1, "activity_type"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getStringActivityType(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->activityType:Ljava/lang/String;

    :goto_6
    const-string v1, "Y"

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->syncYN:Ljava/lang/String;

    const-string v1, "activity_type"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getUpdateTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->deviceUpdateTime:Ljava/lang/String;

    :goto_7
    const-string v1, "distance_unit"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->distanceUnit:Ljava/lang/String;

    :goto_8
    const-string/jumbo v1, "profile_disclose_yn"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileDiscloseYN:Ljava/lang/String;

    :goto_9
    const-string/jumbo v1, "profile_create_time"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileCreateTime:Ljava/lang/String;

    :goto_a
    const-string/jumbo v1, "temperature_unit"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getStringUnit(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->temperatureUnit:Ljava/lang/String;

    :goto_b
    return-object v0

    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const v2, 0x2e636

    if-ne v1, v2, :cond_3

    const-string v1, "1"

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->gender:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->gender:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    iput v5, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->height:F

    goto/16 :goto_1

    :cond_5
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->heightUnit:Ljava/lang/String;

    goto/16 :goto_2

    :cond_6
    iput v5, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weight:F

    goto/16 :goto_3

    :cond_7
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weightUnit:Ljava/lang/String;

    goto/16 :goto_4

    :cond_8
    const-string v1, "19700101"

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->birthDate:Ljava/lang/String;

    goto/16 :goto_5

    :cond_9
    invoke-static {v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getStringActivityType(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->activityType:Ljava/lang/String;

    goto/16 :goto_6

    :cond_a
    invoke-static {v6, v7}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->deviceUpdateTime:Ljava/lang/String;

    goto :goto_7

    :cond_b
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->distanceUnit:Ljava/lang/String;

    goto :goto_8

    :cond_c
    const-string v1, "N"

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileDiscloseYN:Ljava/lang/String;

    goto :goto_9

    :cond_d
    invoke-static {v6, v7}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileCreateTime:Ljava/lang/String;

    goto :goto_a

    :cond_e
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->temperatureUnit:Ljava/lang/String;

    goto :goto_b
.end method

.method public static getProfileData(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const/4 v2, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key = \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v1, "shared_pref"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getProfileDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USER_PROFILE:Ljava/lang/String;

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static getUpdateTime()J
    .locals 2

    const-string/jumbo v0, "update_time"

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static isModified()Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "in_sync"

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "false"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static setProfileData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "key"

    invoke-virtual {v2, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "value"

    invoke-virtual {v2, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key = \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    const-string/jumbo v1, "shared_pref"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error updating shared_preferences data. Invalid values: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    :try_start_1
    const-string/jumbo v1, "shared_pref"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error inserting shared_preferences data. Invalid values: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setProfileRestoreContents(Lcom/sec/android/service/health/cp/serversync/data/ProfileData;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/data/ProfileData;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->checkValidity(Lcom/sec/android/service/health/cp/serversync/data/ProfileData;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "country"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->country:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "name"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "gender"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->gender:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntGenderType(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "height"

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->height:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "height_unit"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->heightUnit:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntUnit(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "weight"

    iget v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weight:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "weight_unit"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->weightUnit:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntUnit(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "birth_date"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->birthDate:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "activity_type"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->activityType:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntActivityType(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "distance_unit"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->distanceUnit:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "profile_disclose_yn"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileDiscloseYN:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "temperature_unit"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->temperatureUnit:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntUnit(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "profile_create_time"

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    :cond_0
    const-string/jumbo v2, "profile_create_time"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileCreateTime:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v2, "in_sync"

    const-string/jumbo v3, "true"

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileImageURL:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileImageURL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/data/ProfileData;->profileImageURL:Ljava/lang/String;

    const-string v3, "/storage/emulated/0/SHealth3/cache/img/user_photo.jpg"

    invoke-direct {v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    return-object v0

    :cond_1
    const-string/jumbo v3, "profile_create_time"

    invoke-static {v3, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static setRestoreProfileData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v2, "value"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "key"

    invoke-virtual {v1, v2, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "shared_pref"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    return-void
.end method
