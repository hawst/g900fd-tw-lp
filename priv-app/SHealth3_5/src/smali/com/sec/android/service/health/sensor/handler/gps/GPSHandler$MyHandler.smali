.class Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyHandler"
.end annotation


# instance fields
.field mEnclosingClassWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;->mEnclosingClassWeakRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler$MyHandler;->mEnclosingClassWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage msg.what : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GPS_INIT"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # invokes: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->gpsinit()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V

    goto :goto_0

    :pswitch_1
    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GPS_START"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # invokes: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->registerGpsListener()Z
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)Z

    # invokes: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->gpsStart()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V

    const/4 v0, 0x1

    # setter for: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->DataUpdate:Z
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$402(Z)Z

    goto :goto_0

    :pswitch_2
    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GPS_STOP"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->DataUpdate:Z
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$402(Z)Z

    # invokes: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->gpsStop()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$500(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V

    # invokes: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->unregisterGpsListener()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;)V

    goto :goto_0

    :pswitch_3
    # getter for: Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GPS_DATA "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/gps/GPSHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
