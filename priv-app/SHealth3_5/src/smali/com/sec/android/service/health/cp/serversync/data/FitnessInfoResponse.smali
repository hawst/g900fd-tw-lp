.class public Lcom/sec/android/service/health/cp/serversync/data/FitnessInfoResponse;
.super Ljava/lang/Object;


# instance fields
.field public accessaryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/AccessaryItem;",
            ">;"
        }
    .end annotation
.end field

.field public appId:Ljava/lang/String;

.field public calorie:F

.field public comment:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public duration:I

.field public durationMilli:Ljava/lang/Long;

.field public endTime:Ljava/lang/String;

.field public exerciseId:J

.field public exerciseName:Ljava/lang/String;

.field public exerciseTypeCode:Ljava/lang/String;

.field public fatBurnTime:Ljava/lang/String;

.field public fitnessList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/FitnessList;",
            ">;"
        }
    .end annotation
.end field

.field public hdid:Ljava/lang/String;

.field public heartRate:Ljava/lang/Float;

.field public inputType:Ljava/lang/String;

.field public itemCount:I

.field public level:Ljava/lang/Integer;

.field public photoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/ExerciseImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field public startTime:Ljava/lang/String;

.field public totalDistance:Ljava/lang/Float;

.field public totalRepetitionCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FitnessInfoResponse;->fitnessList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FitnessInfoResponse;->photoList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FitnessInfoResponse;->accessaryList:Ljava/util/ArrayList;

    return-void
.end method
