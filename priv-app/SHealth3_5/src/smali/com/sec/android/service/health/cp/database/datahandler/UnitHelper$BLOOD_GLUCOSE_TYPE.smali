.class public final Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper$BLOOD_GLUCOSE_TYPE;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BLOOD_GLUCOSE_TYPE"
.end annotation


# static fields
.field public static final MGDL_UNIT:Ljava/lang/String; = "mg/dL"

.field public static final MMOL_UNIT:Ljava/lang/String; = "mmol/L"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
