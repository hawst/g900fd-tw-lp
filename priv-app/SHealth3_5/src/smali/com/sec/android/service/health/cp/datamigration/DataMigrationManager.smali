.class public Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$2;,
        Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TASK_2xTo35:Ljava/lang/String; = "migrateLocalDataFrom2xTo35"

.field private static final TASK_2xto25:Ljava/lang/String; = "migrateLocalDataFrom2xto25"

.field private static final TASK_3xto35:Ljava/lang/String; = "migrateLocalDataFrom3xto35"

.field private static final TASK_NONE:Ljava/lang/String; = "NONE"

.field private static final TASK_NORAM_INIT:Ljava/lang/String; = "doNormalInitialization"

.field private static mInstance:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;


# instance fields
.field private isTaskInProgress:Z

.field private mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

.field private mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

.field private mMigrationErrorCode:I

.field private mMigrationState:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

.field private mMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

.field private mRunningAPITask:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mInstance:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    const-string v0, "NONE"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;->STATE_NONE:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationState:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;-><init>(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;)Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationState:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationErrorCode:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    return-object p1
.end method

.method public static getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mInstance:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    return-object v0
.end method

.method private resetState()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[+] resetState"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    const-string v0, "NONE"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[-] resetState"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public declared-synchronized cancel()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public check2XDBFileExist(Landroid/content/Context;)Z
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[+] check2XDBFileExist"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "secure_shealth2.db"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->getBackupPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "secure_shealth2.db"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[-] check2XDBFileExist : isExists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[-] check2XDBFileExist : isBackupDbExists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    or-int/2addr v0, v1

    return v0
.end method

.method public declared-synchronized check2xAnd3xDBFilesExist(Landroid/content/Context;)Z
    .locals 5

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[+] check2xAnd3xDBFilesExist"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "secure_shealth2.db"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const-string/jumbo v1, "platform.db"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[-] check2xAnd3xDBFilesExist : is2xExists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[-] check2xAnd3xDBFilesExist : is3xExists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized check35DBFileExist(Landroid/content/Context;)Z
    .locals 4

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[+] check35DBFileExist"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "platform.db"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[-] check35DBFileExist : is35Exists"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public checkDbStatus(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/ICheckDbStatusListener;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized cleanUp(Landroid/content/Context;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[+] cleanUp"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->cleanUpOldDatabase(Landroid/content/Context;)V

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[-] cleanUp"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized doNormalInitialization(Landroid/content/Context;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v3, "[+] doNormalInitialization"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    if-ne v2, v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " task is running. So task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "doNormalInitialization"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] doNormalInitialization"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    const-string v0, "doNormalInitialization"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    invoke-direct {v0, p1, p0, v2}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] doNormalInitialization"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized doNormalInitialization(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v3, "[+] doNormalInitialization with callback"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    if-ne v2, v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " task is running. So task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "doNormalInitialization"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] doNormalInitialization with callback"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$2;->$SwitchMap$com$sec$android$service$health$cp$datamigration$DataMigrationManager$MigrationState:[I

    iget-object v2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationState:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    :goto_0
    monitor-exit p0

    return v0

    :pswitch_0
    :try_start_1
    invoke-interface {p2}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onCancelled()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    :try_start_2
    iget v1, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationErrorCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onFinished(Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationErrorCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onProgressUpdate(Ljava/lang/Integer;)V

    iput-object p2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    goto :goto_0

    :pswitch_3
    iput-object p2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    const-string v0, "doNormalInitialization"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;

    invoke-direct {v0, p1, p0, p2}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] doNormalInitialization with callback"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized migrateLocalDataFrom2xTo35(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v3, "[+] migrateLocalDataFrom2xTo35"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    if-ne v2, v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " task is running. So task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "migrateLocalDataFrom2xTo35"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] migrateLocalDataFrom2xto25"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    const-string/jumbo v0, "migrateLocalDataFrom2xTo35"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;

    invoke-direct {v0, p1, p0, p2}, Lcom/sec/android/service/health/cp/datamigration/versionh/JtoTMigrationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] migrateLocalDataFrom2xTo35"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized migrateLocalDataFrom2xto25(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v3, "[+] migrateLocalDataFrom2xto25"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    if-ne v2, v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " task is running. So task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "migrateLocalDataFrom2xto25"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] migrateLocalDataFrom2xto25"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    const-string/jumbo v0, "migrateLocalDataFrom2xto25"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;

    invoke-direct {v0, p1, p0, p2}, Lcom/sec/android/service/health/cp/datamigration/versionj/JtoHMigrationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] migrateLocalDataFrom2xto25"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized migrateLocalDataFrom3xto35(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v3, "[+] migrateLocalDataFrom3xto35"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    if-ne v2, v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " task is running. So task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "migrateLocalDataFrom3xto35"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] migrateLocalDataFrom3xto35"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->isTaskInProgress:Z

    const-string/jumbo v0, "migrateLocalDataFrom3xto35"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mRunningAPITask:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionk/KtoTMigrationTask;

    invoke-direct {v0, p1, p0, p2}, Lcom/sec/android/service/health/cp/datamigration/versionk/KtoTMigrationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentRunningTask:Lcom/sec/android/service/health/cp/BaseInitializationTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v2, "[-] migrateLocalDataFrom3xto35"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCancelled(Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[+] onCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->resetState()V

    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "calling the app call back. status ="

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onCancelled()V

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[-] onCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onFinished(Ljava/lang/Integer;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[+] onFinished"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->resetState()V

    if-eqz p2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "calling the app call back."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onFinished(Ljava/lang/Integer;)V

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "[-] onFinished"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onProgressUpdate(Ljava/lang/Integer;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V
    .locals 2

    if-eqz p2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->TAG:Ljava/lang/String;

    const-string v1, "calling onProgressUpdate "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onProgressUpdate(Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method
