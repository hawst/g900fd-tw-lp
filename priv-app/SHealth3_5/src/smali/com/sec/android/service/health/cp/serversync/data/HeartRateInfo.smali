.class public Lcom/sec/android/service/health/cp/serversync/data/HeartRateInfo;
.super Ljava/lang/Object;


# instance fields
.field public accId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceBioPKId:J

.field public deviceCreateDate:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyDate:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public endTime:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public heartRateDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/HeartRateItem;",
            ">;"
        }
    .end annotation
.end field

.field public inputType:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public sampleConditionType:Ljava/lang/String;

.field public sampleMethodType:Ljava/lang/String;

.field public startTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/HeartRateInfo;->heartRateDataList:Ljava/util/ArrayList;

    return-void
.end method
