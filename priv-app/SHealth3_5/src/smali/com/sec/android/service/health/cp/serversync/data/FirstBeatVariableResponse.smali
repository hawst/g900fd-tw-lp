.class public Lcom/sec/android/service/health/cp/serversync/data/FirstBeatVariableResponse;
.super Ljava/lang/Object;


# instance fields
.field public activityClass:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceFirstBeatVarPKId:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public latestExerciseTime:Ljava/lang/String;

.field public latestFeedbackPhraseNumber:Ljava/lang/String;

.field public maximumHeartRate:Ljava/lang/String;

.field public maximumMet:Ljava/lang/String;

.field public previousToTrainingLevel:Ljava/lang/String;

.field public previousTrainingLevel:Ljava/lang/String;

.field public recoveryResource:Ljava/lang/String;

.field public startTime:Ljava/lang/String;

.field public trainingLevel:Ljava/lang/String;

.field public trainingLevelUpdate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
