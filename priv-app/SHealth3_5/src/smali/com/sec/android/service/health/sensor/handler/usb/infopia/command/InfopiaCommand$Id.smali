.class public final Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand$Id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Id"
.end annotation


# static fields
.field public static final CMD_CLEAR_DATA:Ljava/lang/String; = "CMD_CLEAR_DATA"

.field public static final CMD_GET_TIME:Ljava/lang/String; = "CMD_GET_TIME"

.field public static final CMD_SET_TIME:Ljava/lang/String; = "CMD_SET_TIME"

.field public static final RES_CLEAR_DATA:Ljava/lang/String; = "RES_CLEAR_DATA"

.field public static final RES_GET_TIME:Ljava/lang/String; = "RES_GET_TIME"

.field public static final RES_SET_TIME:Ljava/lang/String; = "RES_SET_TIME"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
