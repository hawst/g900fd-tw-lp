.class public Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mEndOfDay:J

.field private mIsFirstDataInBulkArray:Z

.field private mLastDataStartTime:J

.field private mPrevExerciseId:J

.field private mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorBase;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mIsFirstDataInBulkArray:Z

    return-void
.end method

.method private getContentValuesForBulkData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Landroid/content/ContentValues;
    .locals 14

    const-wide/32 v12, 0x927c0

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const v3, 0x7fffffff

    const/4 v6, 0x0

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->insertIntoExerciseDeviceTableForBulkData(J)J

    const-string v0, "exercise__id"

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "user_device__id"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distanceUnit:I

    const v1, 0x29812

    if-ne v0, v1, :cond_a

    const-string v0, "distance"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertYardsToMeters(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_1

    const-string/jumbo v0, "speed"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_2

    const-string v0, "calorie"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    :cond_2
    const-string/jumbo v0, "total_step"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    if-eq v0, v3, :cond_3

    const-string/jumbo v0, "walk_step"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    if-eq v0, v3, :cond_4

    const-string/jumbo v0, "run_step"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-eq v0, v3, :cond_5

    const-string/jumbo v0, "updown_step"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    if-eq v0, v3, :cond_6

    const-string/jumbo v0, "sample_position"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_6
    const-string/jumbo v0, "start_time"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "end_time"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    add-long/2addr v1, v12

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mIsFirstDataInBulkArray:Z

    if-eqz v0, :cond_d

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mIsFirstDataInBulkArray True"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    if-eqz v8, :cond_b

    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mIsFirstDataInBulkArray True - there is a row conflict"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v0, "distance"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    const-string/jumbo v0, "total_step"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v2, "calorie"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "_id = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v9, v7, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "distance"

    invoke-virtual {v7, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    sub-float v1, v2, v1

    :cond_7
    const-string v2, "calorie"

    invoke-virtual {v7, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    sub-float v3, v2, v3

    :cond_8
    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    sub-int/2addr v2, v0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    add-long/2addr v4, v12

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->processExerciseData(FIFJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v8, :cond_9

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_9
    move-object v0, v6

    :goto_1
    return-object v0

    :cond_a
    const-string v0, "distance"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    goto/16 :goto_0

    :cond_b
    if-eqz v8, :cond_c

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mIsFirstDataInBulkArray:Z

    :cond_d
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mlastPedometerDataBulk:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    add-long/2addr v4, v12

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->processExerciseData(FIFJ)V

    move-object v0, v7

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_e

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v0

    :catchall_1
    move-exception v0

    move-object v6, v8

    goto :goto_2
.end method

.method private getNewExerciseId(J)J
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise_info__id"

    const/16 v2, 0x4651

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "start_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "duration_min"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "duration_millisecond"

    const v2, 0x927c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "end_time"

    const-wide/32 v2, 0x927c0

    add-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "exercise_type"

    const/16 v2, 0x4e23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "input_source_type"

    const v2, 0x3f7a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-wide p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseStartTime:J

    iput v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExercisePrevDistance:F

    iput v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExercisePrevCadence:F

    iput v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExercisePrevEnergy:F

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private insertBulkData([Landroid/content/ContentValues;)I
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    const-string v1, "WalkInfoAggregator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Bulk Data insert count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method private insertIntoExerciseDeviceTableForBulkData(J)J
    .locals 8

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mIsFirstDataInBulkArray:Z

    if-eqz v0, :cond_7

    invoke-static {p1, p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getEndOfDayInMilliSeconds(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mEndOfDay:J

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select E.[_id] from exercise as E inner join exercise_device_info as ED on E.[_id] = ED.[exercise__id]  and E.[exercise_info__id] = 18001 and ED.[user_device__id] = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' and E.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] between "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getStartOfDayInMilliSeconds(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getEndOfDayInMilliSeconds(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertIntoExerciseDeviceTable  query : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "insertIntoExerciseDeviceTableForBulkData mIsFirstDataInBulkArray True exercise exists for the day for the specific device"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x1

    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    iput-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mPrevExerciseId:J

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->getExerciseDetails(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    move v0, v7

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mIsFirstDataInBulkArray:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mEndOfDay:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_5

    :cond_2
    invoke-static {p1, p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getEndOfDayInMilliSeconds(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mEndOfDay:J

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mPrevExerciseId:J

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mPrevExerciseId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "insertIntoExerciseDeviceTableForBulkData updating last exercise summery"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mLastDataStartTime:J

    const-wide/32 v2, 0x927c0

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->updateExerciseSummery(J)V

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "insertIntoExerciseDeviceTableForBulkData getting new exercise and adding exercise_device_info"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->getNewExerciseId(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    :cond_5
    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mExerciseId:J

    return-wide v0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_7
    move v0, v7

    goto :goto_0
.end method


# virtual methods
.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 10

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addData[] : Come from Companion Device. Count "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v4, p1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mIsFirstDataInBulkArray:Z

    array-length v0, p1

    new-array v6, v0, [Landroid/content/ContentValues;

    :try_start_0
    array-length v7, p1

    move v5, v3

    move v1, v3

    :goto_0
    if-ge v5, v7, :cond_3

    aget-object v0, p1, v5

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->getContentValuesForBulkData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Landroid/content/ContentValues;

    move-result-object v8

    if-eqz v8, :cond_1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v4, :cond_0

    const-string v4, "application__id"

    iget-object v9, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v8, v4, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v4, v1, 0x1

    aput-object v8, v6, v1

    move v1, v4

    :cond_1
    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v8, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    iput-wide v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->mLastDataStartTime:J

    :cond_2
    move v0, v1

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_0

    :cond_3
    array-length v0, p1

    if-lez v0, :cond_4

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    const-wide/32 v7, 0x927c0

    add-long/2addr v4, v7

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->updateExerciseSummery(J)V

    :cond_4
    if-nez v1, :cond_5

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "addData[] : data array didnt contain any valid data "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    :goto_1
    return v0

    :cond_5
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addData[] : data array - valid data count"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v6}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->insertBulkData([Landroid/content/ContentValues;)I

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Error occured in between .. transaction cancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v3

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Error occured while updating db"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Data is out of range"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_1
.end method
