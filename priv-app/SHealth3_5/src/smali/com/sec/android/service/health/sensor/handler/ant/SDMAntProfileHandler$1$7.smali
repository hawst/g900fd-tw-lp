.class Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$7;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$ISensorStatusReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$7;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewSensorStatus(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;",
            ")V"
        }
    .end annotation

    const-string v0, "[HealthSensor]SDMSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewSensorStatus - SensorLocation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorLocation;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", BatteryStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", SensorHealth: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorHealth;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", SensorUseState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$SensorUseState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
