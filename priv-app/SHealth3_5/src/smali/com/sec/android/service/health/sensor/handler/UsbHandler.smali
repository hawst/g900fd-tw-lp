.class public Lcom/sec/android/service/health/sensor/handler/UsbHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "[HealthSensor]UsbHandler"

.field private static final PROFILE_NAME:Ljava/lang/String; = "usb"

.field static data:[B


# instance fields
.field private mContext:Landroid/content/Context;

.field mProfileHandlerCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

.field private mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

.field private mUsbDevice:Landroid/hardware/usb/UsbDevice;

.field private usbSppHandle:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x400

    new-array v0, v0, [B

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->data:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    return v0
.end method

.method static synthetic access$200(I[BI)[B
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->nativeUsbSppRead(I[BI)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/UsbHandler;)Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    return-object v0
.end method

.method private static native nativeUsbSppClose(I)V
.end method

.method private static native nativeUsbSppOpen(I)I
.end method

.method private static native nativeUsbSppRead(I[BI)[B
.end method

.method private static native nativeUsbSppWrite(I[BI)I
.end method


# virtual methods
.method public deinitialize()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->shutdown()V

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->nativeUsbSppClose(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->deinitialize()V

    :cond_1
    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 3

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz p4, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/hardware/usb/UsbDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v2

    invoke-virtual {v0, v1, p0, v2, p4}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->nativeUsbSppClose(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->getBaudRate()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->nativeUsbSppOpen(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    const-string v0, "[HealthSensor]UsbHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Usb handler initialize!! usbSppHandle = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " protocol baud rate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->getBaudRate()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->readData()V

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onDataStarted(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :cond_0
    return-void
.end method

.method public onDataStopped(II)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_0
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    return-void
.end method

.method public onStateChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onStateChanged(I)V

    :cond_0
    return-void
.end method

.method public printByteArray(Ljava/lang/String;[BI)V
    .locals 5

    const-string v0, "[HealthSensor]UsbHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==========="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "==[start]==================="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p3, :cond_3

    array-length v0, p2

    if-ge v1, v0, :cond_3

    aget-byte v0, p2, v1

    if-gez v0, :cond_0

    add-int/lit16 v0, v0, 0x100

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[HealthSensor]UsbHandler"

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public readData()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "[HealthSensor]UsbHandler"

    const-string v1, "[UsbHandler] readData() "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]UsbHandler"

    const-string v1, "[UsbHandler] mReadThread!=null && mReadThread.isAlive()"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;-><init>(Lcom/sec/android/service/health/sensor/handler/UsbHandler;Lcom/sec/android/service/health/sensor/handler/UsbHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler$ReadThread;->start()V

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    const/4 v0, 0x0

    const-string/jumbo v1, "send_ping"

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->sendPing()V

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    goto :goto_0
.end method

.method public sendRawData([B)I
    .locals 3

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    array-length v1, p1

    invoke-static {v0, p1, v1}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->nativeUsbSppWrite(I[BI)I

    :cond_0
    const-string v0, "[HealthSensor]UsbHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[UsbHandler] sendRawData() called!  usbSppHandle = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->usbSppHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    check-cast p1, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->mProtocol:Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;->notifyStart()I

    move-result v0

    return v0
.end method

.method public stopReceivingData()I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/UsbHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    return v2
.end method
