.class public Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitBloodPressureListener;
.super Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;

# interfaces
.implements Lcom/garmin/fit/BloodPressureMesgListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onMesg(Lcom/garmin/fit/BloodPressureMesg;)V
    .locals 6

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;-><init>()V

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    const-wide v4, 0x92ee70e000L

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    :cond_0
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getSystolicPressure()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getSystolicPressure()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->floatValue()F

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    :cond_1
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getDiastolicPressure()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getDiastolicPressure()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->floatValue()F

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    :cond_2
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMeanArterialPressure()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMeanArterialPressure()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->floatValue()F

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->mean:F

    :cond_3
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMap3SampleMean()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v2, "ant_map3_sample_mean"

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMap3SampleMean()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_4
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMapMorningValues()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v2, "ant_map_morning"

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMapMorningValues()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_5
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMapEveningValues()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_6

    const-string v2, "ant_map_evening"

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getMapEveningValues()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_6
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getHeartRate()Ljava/lang/Short;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getHeartRate()Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Short;->intValue()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    :cond_7
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getHeartRateType()Lcom/garmin/fit/HrType;

    move-result-object v2

    if-eqz v2, :cond_8

    :cond_8
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getStatus()Lcom/garmin/fit/BpStatus;

    move-result-object v2

    if-eqz v2, :cond_9

    :cond_9
    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getUserProfileIndex()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_a

    const-string v2, "ant_userprofile_id_number"

    invoke-virtual {p1}, Lcom/garmin/fit/BloodPressureMesg;->getUserProfileIndex()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_a
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitBloodPressureListener;->fitnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    invoke-interface {v2, v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;->notifyMeasuredData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method
