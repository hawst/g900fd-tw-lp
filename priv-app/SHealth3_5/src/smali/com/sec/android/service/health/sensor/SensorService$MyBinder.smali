.class final Lcom/sec/android/service/health/sensor/SensorService$MyBinder;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_SensorService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/SensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/SensorService;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/SensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/SensorService$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;-><init>(Lcom/sec/android/service/health/sensor/SensorService;)V

    return-void
.end method


# virtual methods
.method public checkAvailability(IIILjava/lang/String;)I
    .locals 7

    const/4 v6, 0x0

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->isFeatureEnabled(II)Z

    move-result v0

    if-eqz v0, :cond_1

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z
    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z

    move-result v5

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->checkSensorAvailability(IIILjava/lang/String;Z)Z

    move-result v0

    :goto_0
    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkAvailability connectivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deviceType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dataType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " protocol : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkAvailability bAvialable : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    :goto_1
    return v6

    :cond_0
    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    move v0, v6

    goto :goto_0
.end method

.method public getApplicationContextPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getCallingPackage()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$1500(Lcom/sec/android/service/health/sensor/SensorService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConnectedDevices(III)Ljava/util/List;
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v0, 0x1

    const/4 v2, 0x0

    const-string v1, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] getConnectedDevices() connectivityType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deviceType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " dataType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-direct {v4, p2, p3, v2}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    if-lt p1, v0, :cond_6

    if-ge p1, v9, :cond_6

    if-ne p1, v8, :cond_4

    if-nez p3, :cond_4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManagerList(I)Ljava/util/HashMap;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_d

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_3

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/service/health/sensor/manager/util/RenameDataBaseHelper;->isDevicePresent(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/service/health/sensor/manager/util/RenameDataBaseHelper;->getName(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setName(Ljava/lang/String;)V

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    :cond_5
    move-object v0, v2

    :goto_3
    return-object v0

    :cond_6
    move v3, v0

    :goto_4
    if-ge v3, v9, :cond_f

    if-eq v3, v8, :cond_7

    const/4 v0, 0x7

    if-ne v3, v0, :cond_a

    :cond_7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManagerList(I)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    if-eqz v0, :cond_e

    const-string v5, "[HealthSensor]SensorService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "absMngr = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_9
    move-object v0, v1

    :goto_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_4

    :cond_a
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v3, p3}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_c

    if-nez v1, :cond_b

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v0

    goto :goto_7

    :cond_b
    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_c
    move-object v0, v1

    goto :goto_7

    :cond_d
    move-object v0, v2

    goto/16 :goto_3

    :cond_e
    move-object v0, v2

    goto :goto_5

    :cond_f
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public isApplicationPlugin()Z
    .locals 2

    const-string v0, "com.sec.android.service.health"

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getCallingPackage()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$1500(Lcom/sec/android/service/health/sensor/SensorService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "access from healthservice"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getCallingPackage()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$1500(Lcom/sec/android/service/health/sensor/SensorService;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
    .locals 4

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] isConnected() bConnected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v0, 0x4

    if-eq v2, v0, :cond_1

    const/4 v0, 0x7

    if-ne v2, v0, :cond_3

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x5

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V
    .locals 5

    const/4 v2, 0x0

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] join() process Id : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] join() - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$1100(Lcom/sec/android/service/health/sensor/SensorService;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_2

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v0, v2}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkPermission(Landroid/content/Context;Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getCallerPackage()Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1200(Lcom/sec/android/service/health/sensor/SensorService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : Doesn\'t have permission to access the device "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v0, 0x4

    if-eq v1, v0, :cond_4

    const/4 v0, 0x7

    if-ne v1, v0, :cond_6

    :cond_4
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_5
    invoke-virtual {v3, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchJoin(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;ILcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_2
.end method

.method public leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] leave() - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] leave() devId"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "process Id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    const/4 v2, 0x7

    if-ne v1, v2, :cond_4

    :cond_2
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_3
    invoke-virtual {v2, v1, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z
    invoke-static {v3, v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z

    move-result v1

    invoke-virtual {v0, p1, v2, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchLeave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_1
.end method

.method public record(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;J)I
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] record()  process Id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Exercise Id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] record() with exercise - device is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const/4 v3, 0x7

    if-ne v2, v3, :cond_3

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] record() DataType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->record(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;J)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] record() ERROR_FAILURE"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public rename(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/String;)I
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] rename() name : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " process Id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_1

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] rename() - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    const/4 v0, 0x4

    if-ne v3, v0, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    invoke-virtual {v4, v3, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v2, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->renameDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] rename() ERROR_NOT_JOINED"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x7

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_2

    :cond_4
    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] rename() ERROR_FAILURE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p2, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "request - command is null "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "command is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v2, "request() - device is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_1
    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "request - command : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " process Id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_4

    :cond_2
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_3
    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->dispatchRequest(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_1

    :cond_5
    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] request() ERROR_FAILURE"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public showConfirmation(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)V
    .locals 3

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] showConfirmation"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] showConfirmation() - device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->isWearableDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] showConfirmation() - isWearableDevice"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    const-class v2, Lcom/sec/android/service/health/contentservice/HealthContentShow;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "streaming"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "deviceobjectid"

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "deviceid"

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "uniqueKey"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # setter for: Lcom/sec/android/service/health/sensor/SensorService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    invoke-static {v0, p2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1302(Lcom/sec/android/service/health/sensor/SensorService;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    goto :goto_0
.end method

.method public startListeningData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] startListeningData() process Id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] startListeningData() - device is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const/4 v3, 0x7

    if-ne v2, v3, :cond_3

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->startListeningData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] startListeningData() ERROR_FAILURE"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public startReceiving(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 9

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] startReceivingData() process Id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] startReceivingData() - device is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const/4 v3, 0x7

    if-ne v2, v3, :cond_3

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] startReceivingData() ERROR_FAILURE"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I
    .locals 7

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->startReceiving(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    return v0
.end method

.method public startScan(IIIILcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;)I
    .locals 9

    const/4 v0, 0x3

    const/4 v7, 0x0

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] startScan() connectivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deviceType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dataType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " durationSeconds : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/4 v1, 0x0

    invoke-direct {v5, p2, p3, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask$Status;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    # setter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$202(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iget-object v8, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1000(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z
    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z

    move-result v6

    move v2, p1

    move v3, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;-><init>(Landroid/content/Context;IILcom/sec/android/service/health/sensor/manager/ISensorListener;Lcom/sec/android/service/health/sensor/manager/util/Filter;Z)V

    # setter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v8, v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$902(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/manager/ScanningManager;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    invoke-virtual {v0, p5, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v7

    :goto_0
    return v0

    :cond_1
    const-string v1, "[HealthSensor]SensorService"

    const-string/jumbo v2, "startScan() Scan already in progress"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    invoke-virtual {v1, p5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1000(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->onScanningStopped(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V

    goto :goto_0
.end method

.method public startScanByDeviceId(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;)I
    .locals 9

    const/4 v0, 0x3

    const/4 v7, 0x0

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] startScanByDeviceId() connectivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deviceId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " durationSeconds : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v1, 0x2711

    invoke-direct {v5, v1, v7, p2}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask$Status;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    # setter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$202(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iget-object v8, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1000(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z
    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z

    move-result v6

    move v2, p1

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;-><init>(Landroid/content/Context;IILcom/sec/android/service/health/sensor/manager/ISensorListener;Lcom/sec/android/service/health/sensor/manager/util/Filter;Z)V

    # setter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v8, v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$902(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/manager/ScanningManager;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v1

    invoke-virtual {v0, p4, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v7

    :goto_0
    return v0

    :cond_1
    const-string v1, "[HealthSensor]SensorService"

    const-string/jumbo v2, "startScanByDeviceId() Scan already in progress"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    invoke-virtual {v1, p4}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1000(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->onScanningStopped(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V

    goto :goto_0
.end method

.method public stopListeningData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)I
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] stopListeningData() process Id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] stopListeningData() - device is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const/4 v3, 0x7

    if-ne v2, v3, :cond_3

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->stopListeningData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/Integer;)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] stopListeningData() ERROR_FAILURE"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)I
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] stopReceivingData() process Id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] stopReceivingData() - device is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const/4 v3, 0x7

    if-ne v2, v3, :cond_3

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] stopReceivingData() ERROR_FAILURE"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public stopScan()V
    .locals 2

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] stopScan()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->cancelScan()Z

    :cond_0
    return-void
.end method

.method public updateConfirmation(ZJ)V
    .locals 4

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] updateConfirmation acceptStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requestTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "status"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "requestTime"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    new-instance v2, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$1300(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;)Z

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbContentServiceHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1400()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
