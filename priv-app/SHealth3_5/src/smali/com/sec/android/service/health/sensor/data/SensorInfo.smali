.class public Lcom/sec/android/service/health/sensor/data/SensorInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/service/health/sensor/data/SensorInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEVICE_TYPE:I = 0x3

.field private static final MANUFACTURER:I = 0x50

.field private static final MODEL_NUMBER:I = 0x51

.field private static final SENSOR_INFO_DATA:I = 0x4

.field private static final TAG:Ljava/lang/String; = "HealthService"


# instance fields
.field private mDeviceTypes:[I

.field private mFEState:Ljava/lang/String;

.field private mFEType:Ljava/lang/String;

.field private mHardwareVersion:Ljava/lang/String;

.field private mManufacture:Ljava/lang/String;

.field private mManufacturerID:Ljava/lang/String;

.field private mModelName:Ljava/lang/String;

.field private mModelNumber:Ljava/lang/String;

.field private mSDM_Battery:Ljava/lang/String;

.field private mSDM_Health:Ljava/lang/String;

.field private mSDM_Location:Ljava/lang/String;

.field private mSDM_UseState:Ljava/lang/String;

.field private mSerialNumber:Ljava/lang/String;

.field private mSoftwareVer:Ljava/lang/String;

.field private mSoftwareVersion:Ljava/lang/String;

.field private mTotalSupportedDeviceType:I

.field private mUid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/data/SensorInfo$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/data/SensorInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/data/SensorInfo;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mUid:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v0, p6

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    array-length v1, v1

    invoke-static {p6, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSerialNumber:Ljava/lang/String;

    const-string v0, "HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorInfo] created. mManufacture:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mModelName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSoftwareVer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSerialNumber:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSerialNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    goto :goto_0
.end method

.method private parseSensorInfo(Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v0, 0x0

    const-string v1, "\\|"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v2, v1, v6

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v3, v2, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    iget v3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    if-lez v3, :cond_0

    iget v3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    :goto_0
    iget v3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-array v3, v5, [I

    iput-object v3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    goto :goto_0

    :cond_1
    aget-object v0, v1, v7

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v1, v0, v5

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x50

    if-ne v1, v2, :cond_2

    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    :goto_1
    aget-object v1, v0, v6

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x51

    if-ne v1, v2, :cond_3

    aget-object v0, v0, v7

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    :goto_2
    const-string v0, "HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorInformation][parseSensorInfo()] mManufacture:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mModelName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    iput-object v8, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iput-object v8, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    goto :goto_2
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mUid:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSerialNumber:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacturerID:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelNumber:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mHardwareVersion:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVersion:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Location:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Battery:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Health:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_UseState:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEState:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDeviceTypes()[I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    return-object v0
.end method

.method public getFEState()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEState:Ljava/lang/String;

    return-object v0
.end method

.method public getFEType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEType:Ljava/lang/String;

    return-object v0
.end method

.method public getHardwareVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mHardwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getManufactureID()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacturerID:Ljava/lang/String;

    return-object v0
.end method

.method public getManufactureName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSDM_Battery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Battery:Ljava/lang/String;

    return-object v0
.end method

.method public getSDM_Health()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Health:Ljava/lang/String;

    return-object v0
.end method

.method public getSDM_Location()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Location:Ljava/lang/String;

    return-object v0
.end method

.method public getSDM_UseState()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_UseState:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSoftwareVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mUid:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceTypes([I)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    return-void
.end method

.method public setFEState(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEState:Ljava/lang/String;

    return-void
.end method

.method public setFEType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEType:Ljava/lang/String;

    return-void
.end method

.method public setHardwareVersion(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mHardwareVersion:Ljava/lang/String;

    return-void
.end method

.method public setManufactureID(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacturerID:Ljava/lang/String;

    return-void
.end method

.method public setManufactureName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    return-void
.end method

.method public setModelName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    return-void
.end method

.method public setModelNumber(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelNumber:Ljava/lang/String;

    return-void
.end method

.method public setSDM_Battery(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Battery:Ljava/lang/String;

    return-void
.end method

.method public setSDM_Health(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Health:Ljava/lang/String;

    return-void
.end method

.method public setSDM_Location(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Location:Ljava/lang/String;

    return-void
.end method

.method public setSDM_UseState(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_UseState:Ljava/lang/String;

    return-void
.end method

.method public setSensorInfoField(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/data/SensorInfo;->parseSensorInfo(Ljava/lang/String;)V

    return-void
.end method

.method public setSensorInfoField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 3

    const/4 v2, 0x0

    array-length v0, p4

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    array-length v1, v1

    invoke-static {p4, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    const-string v0, "HealthService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorInfo] setSensorInfoField. mManufacture:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mModelName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSoftwareVer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    goto :goto_0
.end method

.method public setSerialNumber(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSerialNumber:Ljava/lang/String;

    return-void
.end method

.method public setSoftwareVersion(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mUid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mTotalSupportedDeviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mDeviceTypes:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacture:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSerialNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mManufacturerID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mModelNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mHardwareVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSoftwareVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Location:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Battery:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_Health:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mSDM_UseState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/SensorInfo;->mFEType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
