.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# static fields
.field protected static final hexArray:[C


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->hexArray:[C

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method public static bytesToHex([B)Ljava/lang/String;
    .locals 6

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    new-array v1, v0, [C

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    mul-int/lit8 v3, v0, 0x2

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->hexArray:[C

    ushr-int/lit8 v5, v2, 0x4

    aget-char v4, v4, v5

    aput-char v4, v1, v3

    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->hexArray:[C

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v4, v2

    aput-char v2, v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method private getDate([B)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    new-array v1, v4, [B

    array-length v2, v1

    invoke-static {p1, v5, v1, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_READ_RTC:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x2

    new-array v1, v1, [B

    array-length v2, v1

    invoke-static {p1, v4, v1, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v2, 0x4

    new-array v2, v2, [B

    array-length v3, v1

    if-le v3, v6, :cond_1

    aget-byte v0, v1, v6

    aput-byte v0, v2, v5

    aget-byte v0, v1, v4

    aput-byte v0, v2, v7

    aget-byte v0, v1, v7

    aput-byte v0, v2, v4

    aget-byte v0, v1, v5

    aput-byte v0, v2, v6

    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->bytesToHex([B)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    const-string v4, "RES_GET_TIME"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "PARAM_TIME"

    invoke-virtual {v4, v5, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setResponse(Landroid/os/Bundle;)V

    const-string v1, "device time"

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getResposeHandler()Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method


# virtual methods
.method public doAction()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_RTC:[B

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method public responseReceived([B)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->getDate([B)Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendAck()V

    return-void
.end method
