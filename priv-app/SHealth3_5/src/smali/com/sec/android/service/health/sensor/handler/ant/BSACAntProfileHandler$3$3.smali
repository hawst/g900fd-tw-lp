.class Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$3;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewCumulativeOperatingTime(JLjava/util/EnumSet;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;J)V"
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "ant_cumulative_operating_time"

    invoke-virtual {v0, v1, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$3;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;-><init>()V

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->notifyResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method
