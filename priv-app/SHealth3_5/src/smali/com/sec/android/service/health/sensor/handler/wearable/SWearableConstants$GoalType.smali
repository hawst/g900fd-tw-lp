.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$GoalType;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GoalType"
.end annotation


# static fields
.field public static final CALORIES:I = 0x4

.field public static final DISTANCE:I = 0x2

.field public static final NOGOAL:I = 0x8

.field public static final TIME:I = 0x3

.field public static final TRAININGLEVEL:I = 0x5

.field public static final TRAINING_VALUE:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
