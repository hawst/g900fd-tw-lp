.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;
.super Ljava/lang/Thread;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final bHdpTest:Z

.field private mClosed:Z

.field private mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

.field private mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

.field private mThreadFd:Landroid/os/ParcelFileDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->bHdpTest:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mThreadFd:Landroid/os/ParcelFileDescriptor;

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mClosed:Z

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mThreadFd:Landroid/os/ParcelFileDescriptor;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    const/4 v14, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "run called"

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mThreadFd:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mThreadFd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "run readFd:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " valid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->valid()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    const v0, 0x186a0

    new-array v8, v0, [B

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mClosed:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move v0, v1

    :cond_0
    :goto_0
    iget-boolean v5, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mClosed:Z

    if-nez v5, :cond_3

    :try_start_0
    invoke-virtual {v7, v8}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v10, v5, v3

    const-wide/32 v12, 0x14438

    cmp-long v10, v10, v12

    if-lez v10, :cond_2

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "run() timeout "

    invoke-static {v0, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v10, -0x1

    const/4 v11, 0x1

    invoke-interface {v0, v10, v11}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    :cond_1
    move v0, v1

    :cond_2
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->interrupted()Z

    move-result v10

    if-eqz v10, :cond_4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    const-string v1, "Reader Thread Stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    :try_start_1
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "run() IOException e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "run() e.getMessage()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, "read failed: ETIMEDOUT (Connection timed out)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    const-string v1, "[HealthService] timeout case2 "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v14, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    goto :goto_1

    :cond_4
    if-lez v9, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "run() Got data:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v9, [B

    invoke-static {v8, v1, v0, v1, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v3, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyRawDataReceived([B)I

    move v0, v2

    move-wide v3, v5

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    :cond_5
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "run() mFd is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method shutdown()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->TAG:Ljava/lang/String;

    const-string v1, "ShutDown Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->interrupt()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->mClosed:Z

    return-void
.end method
