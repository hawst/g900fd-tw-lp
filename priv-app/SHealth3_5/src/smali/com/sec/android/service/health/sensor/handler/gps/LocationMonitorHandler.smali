.class public Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
.implements Lcom/samsung/location/monitor/LocationMonitorCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;,
        Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;
    }
.end annotation


# static fields
.field private static final LOCATION_CREATE:I = 0x7d1

.field private static final PARAM_CHECK_WEARABLE_DEVICE_CONNECTED:Ljava/lang/String; = "PARAM_CHECK_WEARABLE_DEVICE_CONNECTED"

.field private static final PARAM_HEIGHT:Ljava/lang/String; = "PARAM_HEIGHT"

.field private static final PARAM_PSERVICE:Ljava/lang/String; = "PARAM_PSERVICE"

.field private static final PARAM_STATE:Ljava/lang/String; = "PARAM_STATE"

.field private static final PARAM_TYPE:Ljava/lang/String; = "PARAM_TYPE"

.field private static final PARAM_WEIGHT:Ljava/lang/String; = "PARAM_WEIGHT"

.field protected static final STATE_DISCONNECTED:I = 0x3e9

.field private static final TAG:Ljava/lang/String;

.field private static final command:Ljava/lang/String; = "CMD_REQUEST_EXTRA"


# instance fields
.field private isAvailableBarometer:Z

.field private locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mContext:Landroid/content/Context;

.field private mLocationMonitorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

.field private mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field protected pairingCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

.field protected state:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLocationMonitorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->pairingCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const/16 v0, 0x3e9

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->state:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Lcom/samsung/location/monitor/LocationMonitor;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z

    return p1
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/LocationMonitor;->stopMonitoring()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->getLocLooper()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x7d1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->destroyLooper()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_2
    return-void
.end method

.method public finalize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string v1, "finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLocationMonitorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLocationMonitorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/4 v0, 0x0

    return v0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDataChanged(Lcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 6

    const/4 v5, 0x1

    if-nez p1, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDataChanged result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;-><init>()V

    iget-wide v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    iget-wide v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->latitude:D

    iget-wide v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->longitude:D

    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z

    if-ne v1, v5, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onDataChanged hasAltitude"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    float-to-double v1, v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->altitude:D

    :cond_2
    iget v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->accuracy:F

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->accuracy:F

    iget v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->speed:F

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onDataChanged main data"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_3

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onDataChanged => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Extra;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Extra;-><init>()V

    iput-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatDistance:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatTime:J

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->isAvailableBarometer:Z

    if-ne v2, v5, :cond_4

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineTime:J

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineTime:J

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->declineTime:J

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineTime:J

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->maxAltitude:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->minAltitude:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    :cond_4
    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->maxSpeed:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->consumedCalorie:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->pace:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->pace:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->maxPace:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxPace:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->averagePace:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averagePace:F

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->stepCount:I

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onDataChanged extra data"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v2, :cond_5

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v2}, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v2, v0, v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method public onMonitoringResumed()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResumeMonitoring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onMonitoringStarted()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onStartMonitoring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onMonitoringUnavailable()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onStopMonitoring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->stopReceivingData(I)I

    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 9

    const/4 v0, 0x0

    const/16 v1, 0x3e9

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    const-string v2, "CMD_REQUEST_EXTRA"

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_7

    const/4 v3, 0x1

    const-string v4, "PARAM_TYPE"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-gt v3, v4, :cond_4

    const-string v3, "PARAM_TYPE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x4

    if-gt v3, v4, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    if-eqz v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->getLocLooper()Landroid/os/Handler;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    const-wide/16 v7, 0x1

    cmp-long v1, v5, v7

    if-lez v1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string v1, " ERROR_FAILURE : Looper isnt yet prepared"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x3e8

    :goto_0
    return v1

    :cond_1
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x7d1

    iput v3, v1, Landroid/os/Message;->what:I

    iput-object p0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->getLocLooper()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    :goto_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Request Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LocationMonitorHandler - Request Error :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_3
    move v1, v0

    goto :goto_0

    :cond_4
    const-string v3, "PARAM_STATE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "LocationMonitorPause"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "request LocationMonitorPause is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v1}, Lcom/samsung/location/monitor/LocationMonitor;->pauseMonitoring()V

    goto :goto_1

    :cond_5
    const-string v3, "PARAM_STATE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "LocationMonitorResume"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :try_start_0
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "request LocationMonitorResume is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v2}, Lcom/samsung/location/monitor/LocationMonitor;->resumeMonitoring()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "resumeMonitoring is called but UnsupportedOperationException is occurred"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_1

    :cond_6
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "request return case 1 with ERROR_WRONG_REQUEST"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "request return case 2 with ERROR_WRONG_REQUEST"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "request return case 3 with ERROR_WRONG_REQUEST"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startReceivingData called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;-><init>(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mLoctionMointerThread:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->start()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x6

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :cond_1
    return v2
.end method

.method public stopReceivingData()I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->stopReceivingData(I)I

    move-result v0

    return v0
.end method

.method public stopReceivingData(I)I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopReceivingData called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/LocationMonitor;->stopMonitoring()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x6

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_1
    return p1
.end method
