.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;
    }
.end annotation


# static fields
.field public static final BLUETOOTH_HEALTH:Ljava/lang/String; = "bluetooth_health"

.field private static final CREATE_CHANNEL_DONE:I = 0x3e9

.field private static final DESTROY_CHANNEL_DONE:I = 0x3ea

.field private static final FAILURE:I = 0x0

.field private static final PROFILE_NAME:Ljava/lang/String; = "hdp"

.field private static final SEND_DATA:I = 0x64

.field private static STATE_CONNECTED:I = 0x0

.field private static STATE_DISCONNECTED:I = 0x0

.field private static STATE_RECEIVING_DATA:I = 0x0

.field private static final SUCCESS:I = 0x1

.field protected static final TAG:Ljava/lang/String;

.field private static error_flag:Z

.field private static final mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;


# instance fields
.field private appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

.field private appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

.field private bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private errorCode:I

.field private mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

.field private mChannelId:I

.field private mDoesDataListenerExist:Z

.field private mHandler:Landroid/os/Handler;

.field private mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

.field private mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

.field private mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mstateofBTCommunication:I

.field private uniqueId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    sput-boolean v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->error_flag:Z

    sput v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_DISCONNECTED:I

    const/4 v0, 0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_CONNECTED:I

    const/4 v0, 0x2

    sput v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_RECEIVING_DATA:I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$2;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$2;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->errorCode:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mChannelId:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mDoesDataListenerExist:Z

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Landroid/os/ParcelFileDescriptor;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Landroid/os/ParcelFileDescriptor;)Landroid/os/ParcelFileDescriptor;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->createChannel()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mstateofBTCommunication:I

    return v0
.end method

.method static synthetic access$1100()I
    .locals 1

    sget v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_DISCONNECTED:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->destroyChannel()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mDoesDataListenerExist:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mChannelId:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->sendMessage(II)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-object v0
.end method

.method private static convertProfile2DataType(I)I
    .locals 4

    const/4 v0, 0x0

    sparse-switch p0, :sswitch_data_0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v2, "convertProfile2DataType() invalid dataType"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "convertProfile2DataType() convertProfile2DataType return dataType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :sswitch_0
    const/16 v0, 0x1007

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x1011

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1004

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1008

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x100f

    goto :goto_0

    :sswitch_5
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "convertProfile2DataType() no dataType for Profile("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x900 -> :sswitch_5
        0x904 -> :sswitch_0
        0x908 -> :sswitch_3
        0x90c -> :sswitch_4
        0x910 -> :sswitch_1
        0x914 -> :sswitch_2
        0x918 -> :sswitch_5
        0x91c -> :sswitch_5
    .end sparse-switch
.end method

.method private createChannel()V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "createChannel and Start Bluetooth HDP Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;-><init>(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    const-string v1, "BluetoothHDPReadThread"

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->start()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyStart()I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "createChannel and Start Bluetooth HDP Thread done!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private destroyChannel()V
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "destroyChannel"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->shutdown()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;->join()V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v2, "handleMessage DESTROY_CHANNEL_DONE Connection cancelled twice?"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private registerSinkAppConfiguration()V
    .locals 4

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v0

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "registerSinkAppConfiguration() profile = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "registerSinkAppConfiguration() mBluetoothHealth is null, so return"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->convertProfile2DataType(I)I

    move-result v0

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/bluetooth/BluetoothHealth;->registerSinkAppConfiguration(Ljava/lang/String;ILandroid/bluetooth/BluetoothHealthCallback;)Z

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "registerSinkAppConfiguration Return value  is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sendMessage(II)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p2, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private unregisterHealthApplication()V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterHealthApplication() profile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unregisterHealthApplication()  mBluetoothHealth || appConfiguration is null, so return."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterHealthApplication() BluetoothAddress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterHealthApplication() name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getDataType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHealth;->unregisterAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unregisterHealthApplication() returnValue = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->unregisterHealthApplication()V

    :cond_0
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appFileDiscriptor:Landroid/os/ParcelFileDescriptor;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mChannelId:I

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mReadThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPReadThread;

    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->prepare()V

    :cond_0
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$MessageHandler;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler$1;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mHandler:Landroid/os/Handler;

    :cond_1
    if-eqz p4, :cond_2

    check-cast p4, Landroid/bluetooth/BluetoothHealth;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    :cond_2
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectedDevice()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_4

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "initiallize() bluetoothHealth or BluetoothDevice is null, so returning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x3ef

    :goto_0
    return v0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-interface {v0, p1, p0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "initiallize()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->registerSinkAppConfiguration()V

    :cond_5
    sget v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_DISCONNECTED:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mstateofBTCommunication:I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDataReceived(_Health data, Bundle extra)"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_RECEIVING_DATA:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mstateofBTCommunication:I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDataReceived(_Health[] data, Bundle[] extra)"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDataStarted(I)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDataStarted"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_CONNECTED:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mstateofBTCommunication:I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    return-void
.end method

.method public onDataStopped(II)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDataStopped"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->STATE_DISCONNECTED:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mstateofBTCommunication:I

    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mDoesDataListenerExist:Z

    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->error_flag:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    sput-boolean v3, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->error_flag:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    invoke-interface {v0, v1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    goto :goto_0
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 0

    return-void
.end method

.method public onStateChanged(I)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStateChanged() state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3e8

    goto :goto_0
.end method

.method public sendRawData([B)I
    .locals 3

    const/4 v2, 0x0

    aget-byte v0, p1, v2

    const/16 v1, -0x1a

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sendDataToSensor rawData has Error flag"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->error_flag:Z

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return v2
.end method

.method public setBluetoothHealth(Landroid/bluetooth/BluetoothHealth;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    return-void
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mDoesDataListenerExist:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "connectChannelToSource bluetoothDevice || AppConfiguration || mBluetoothHealth is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public stopReceivingData()I
    .locals 5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mDoesDataListenerExist:Z

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopReceivingData()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mChannelId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v2, "disconnectChannel() calling onDataStopped"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string v1, "disconnectChannel() btDevice || appConfiguration || appID is null, return"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disconnectChannel() btAddress="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->uniqueId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->appConfiguration:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    iget v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->mChannelId:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/bluetooth/BluetoothHealth;->disconnectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/continua/BluetoothHDPHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopReceivingData() disconnectChannel() return true!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
