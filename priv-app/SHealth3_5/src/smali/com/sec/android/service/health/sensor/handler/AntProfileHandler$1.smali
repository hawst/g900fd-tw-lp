.class Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceStateChange(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string/jumbo v1, "onDeviceStateChange() mAntPluginPcc null"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDeviceStateChange status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->getIntValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$4;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$DeviceState:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->setConnectState(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyChannelStateChanged()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->setConnectState(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyChannelStateChanged()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->setConnectState(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyChannelStateChanged()V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->setConnectState(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyChannelStateChanged()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
