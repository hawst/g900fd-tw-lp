.class Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onDataStarted(I)V
    .locals 3

    const/4 v2, 0x0

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onDataStarted is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v2, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :cond_0
    return-void
.end method

.method public onDataStopped(II)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataStopped - dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    invoke-interface {v0, v1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->sendErrorMessage(I)V
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.CONNECTION_TYPE"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.DEVICE_TYPE"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.DATA_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/gear/GearHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStateChanged() - state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public sendRawData([B)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
