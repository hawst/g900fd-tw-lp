.class public Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;
.super Lcom/samsung/android/sdk/health/content/ShealthProfile;


# static fields
.field private static final TAG:Ljava/lang/String; = "MigrateShealthProfile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v0, "value"

    invoke-virtual {v2, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "shared_pref"

    const-string v3, "key=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "key"

    invoke-virtual {v2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "shared_pref"

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    :cond_0
    return-void
.end method


# virtual methods
.method public load()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    const-string v0, "MigrateShealthProfile"

    const-string v1, "dummy load api used only for migration"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public save()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, "Profile has been updated"

    invoke-static {v4}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v4

    const-string/jumbo v5, "name"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "activity_type"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getActivityType()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "birth_date"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "distance_unit"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getDistanceUnit()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "gender"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getGender()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "height"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getHeight()F

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "height_unit"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getHeightUnit()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "update_time"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "temperature_unit"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getTemperatureUnit()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getWeight()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "weight_unit"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getWeightUnit()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "profile_disclose_yn"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->getProfileDisclose()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-static {v4, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "profile_create_time"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "in_sync"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method
