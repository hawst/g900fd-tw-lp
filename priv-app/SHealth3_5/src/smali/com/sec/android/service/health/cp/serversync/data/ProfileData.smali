.class public Lcom/sec/android/service/health/cp/serversync/data/ProfileData;
.super Ljava/lang/Object;


# instance fields
.field public activityType:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public birthDate:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public deviceUpdateTime:Ljava/lang/String;

.field public distanceUnit:Ljava/lang/String;

.field public gender:Ljava/lang/String;

.field public height:F

.field public heightUnit:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public profileCreateTime:Ljava/lang/String;

.field public profileDiscloseYN:Ljava/lang/String;

.field public profileImageURL:Ljava/lang/String;

.field public syncYN:Ljava/lang/String;

.field public temperatureUnit:Ljava/lang/String;

.field public weight:F

.field public weightUnit:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
