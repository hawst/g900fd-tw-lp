.class Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;
.super Lorg/apache/http/entity/mime/MultipartEntity;
.source "UploadMultipartEntity.java"


# instance fields
.field private COS:Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

.field private final listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->COS:Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .line 32
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;)V
    .locals 1
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "listener"    # Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->COS:Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    .line 43
    iput-object p2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .line 44
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;)V
    .locals 1
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "boundary"    # Ljava/lang/String;
    .param p3, "charset"    # Ljava/nio/charset/Charset;
    .param p4, "listener"    # Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->COS:Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    .line 59
    iput-object p4, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .line 60
    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "outstream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->COS:Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;-><init>(Ljava/io/OutputStream;Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->COS:Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->COS:Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    invoke-super {p0, v0}, Lorg/apache/http/entity/mime/MultipartEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 80
    return-void
.end method
