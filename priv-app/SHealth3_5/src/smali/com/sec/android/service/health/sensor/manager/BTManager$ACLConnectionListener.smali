.class Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/BTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ACLConnectionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/BTManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_ACL_CONNECTED, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    const/16 v2, 0x7d6

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onStateChanged(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_ACL_DISCONNECTED, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    const/16 v2, 0x7d5

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onStateChanged(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
