.class Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;
.super Ljava/lang/Object;
.source "SamsungAccountUserTokenManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 13
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 104
    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$100()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ServiceConnection onServiceConnected "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v8

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v7, v8}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$202(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;

    .line 109
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$300(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string/jumbo v8, "registration_code"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 110
    .local v6, "registrationCode":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 112
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z

    .line 115
    :cond_0
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    iget-object v8, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v8}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppId:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$500(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppSecret:Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$600(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPackageName:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$700(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;

    invoke-direct {v12, p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;-><init>(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;)V

    invoke-interface {v8, v9, v10, v11, v12}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v8

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$402(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;

    .line 208
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    .line 210
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # invokes: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->connectToSamsungAccount()V
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1600(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V

    .line 292
    .end local v6    # "registrationCode":Ljava/lang/String;
    :goto_0
    return-void

    .line 215
    .restart local v6    # "registrationCode":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$300(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string/jumbo v8, "registration_code"

    iget-object v9, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 218
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1700(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    .line 219
    .local v5, "manager":Landroid/accounts/AccountManager;
    if-nez v5, :cond_5

    .line 221
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # invokes: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->unbindSAService()V
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1500(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V

    .line 222
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :cond_2
    :goto_1
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Queue;->size()I

    move-result v7

    if-eqz v7, :cond_4

    .line 226
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .line 227
    .local v4, "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    if-eqz v4, :cond_2

    .line 229
    const-string v7, "Samsung Account is not registered."

    invoke-interface {v4, v7}, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;->setFailureMessage(Ljava/lang/String;)V

    goto :goto_1

    .line 233
    .end local v4    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v7
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 274
    .end local v5    # "manager":Landroid/accounts/AccountManager;
    .end local v6    # "registrationCode":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 276
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    .line 277
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1700(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mServiceConnection:Landroid/content/ServiceConnection;
    invoke-static {v8}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1800(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/ServiceConnection;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 278
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 280
    :cond_3
    :goto_2
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Queue;->size()I

    move-result v7

    if-eqz v7, :cond_b

    .line 282
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .line 283
    .restart local v4    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    if-eqz v4, :cond_3

    .line 285
    invoke-virtual {v3}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;->setFailureMessage(Ljava/lang/String;)V

    goto :goto_2

    .line 289
    .end local v4    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v7

    .line 232
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local v5    # "manager":Landroid/accounts/AccountManager;
    .restart local v6    # "registrationCode":Ljava/lang/String;
    :cond_4
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    sget-object v9, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v7, v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 233
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 236
    :cond_5
    :try_start_5
    const-string v7, "com.osp.app.signin"

    invoke-virtual {v5, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 238
    .local v0, "accountArr":[Landroid/accounts/Account;
    if-eqz v0, :cond_8

    array-length v7, v0

    if-lez v7, :cond_8

    .line 240
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 241
    .local v2, "data":Landroid/os/Bundle;
    const/4 v7, 0x4

    new-array v1, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v8, "user_id"

    aput-object v8, v1, v7

    const/4 v7, 0x1

    const-string v8, "birthday"

    aput-object v8, v1, v7

    const/4 v7, 0x2

    const-string v8, "login_id"

    aput-object v8, v1, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "mcc"

    aput-object v8, v1, v7

    .line 242
    .local v1, "additionalData":[Ljava/lang/String;
    const-string v7, "additional"

    invoke-virtual {v2, v7, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 246
    :try_start_6
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1300(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    move-result-object v7

    sget-object v9, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_OFFERED_WHILE_NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    if-eq v7, v9, :cond_6

    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1300(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    move-result-object v7

    sget-object v9, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    if-ne v7, v9, :cond_7

    .line 248
    :cond_6
    const-string v7, "expired_access_token"

    iget-object v9, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungToken:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$800(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v7, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    sget-object v9, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v7, v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 251
    :cond_7
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 252
    :try_start_7
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v7

    const/16 v8, 0x4d3

    iget-object v9, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9, v2}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_0

    .line 251
    :catchall_2
    move-exception v7

    :try_start_8
    monitor-exit v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v7

    .line 256
    .end local v1    # "additionalData":[Ljava/lang/String;
    .end local v2    # "data":Landroid/os/Bundle;
    :cond_8
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # invokes: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->unbindSAService()V
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1500(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V

    .line 257
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 259
    :cond_9
    :goto_3
    :try_start_a
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Queue;->size()I

    move-result v7

    if-eqz v7, :cond_a

    .line 261
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .line 262
    .restart local v4    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    if-eqz v4, :cond_9

    .line 264
    const-string v7, "Samsung Account is not registered."

    invoke-interface {v4, v7}, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;->setFailureMessage(Ljava/lang/String;)V

    goto :goto_3

    .line 268
    .end local v4    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    :catchall_3
    move-exception v7

    monitor-exit v8
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :try_start_b
    throw v7
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0

    .line 267
    :cond_a
    :try_start_c
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    sget-object v9, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v7, v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 268
    monitor-exit v8
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto/16 :goto_0

    .line 288
    .end local v0    # "accountArr":[Landroid/accounts/Account;
    .end local v5    # "manager":Landroid/accounts/AccountManager;
    .end local v6    # "registrationCode":Ljava/lang/String;
    .restart local v3    # "e":Landroid/os/RemoteException;
    :cond_b
    :try_start_d
    iget-object v7, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    sget-object v9, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v7, v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 289
    monitor-exit v8
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 297
    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onServiceDisconnected : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    return-void
.end method
