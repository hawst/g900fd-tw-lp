.class public final enum Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

.field public static final enum NEGATIVE:Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

.field public static final enum POSITOVE:Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    const-string v1, "NEGATIVE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->NEGATIVE:Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    const-string v1, "POSITOVE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->POSITOVE:Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->NEGATIVE:Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->POSITOVE:Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->$VALUES:[Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->$VALUES:[Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;

    return-object v0
.end method
