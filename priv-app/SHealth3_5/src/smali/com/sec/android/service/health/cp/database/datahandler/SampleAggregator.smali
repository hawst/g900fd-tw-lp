.class public Lcom/sec/android/service/health/cp/database/datahandler/SampleAggregator;
.super Ljava/lang/Object;


# static fields
.field public static MILLI_SECS_PER_HOUR:J

.field public static SAMPLE_TIME_INTERVAL:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/32 v0, 0x927c0

    sput-wide v0, Lcom/sec/android/service/health/cp/database/datahandler/SampleAggregator;->SAMPLE_TIME_INTERVAL:J

    const-wide/32 v0, 0x36ee80

    sput-wide v0, Lcom/sec/android/service/health/cp/database/datahandler/SampleAggregator;->MILLI_SECS_PER_HOUR:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AggregatePedoData(JLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .locals 5

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v2, p2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    sget-wide v0, Lcom/sec/android/service/health/cp/database/datahandler/SampleAggregator;->MILLI_SECS_PER_HOUR:J

    rem-long v0, v2, v0

    sub-long v0, v2, v0

    cmp-long v4, p0, v0

    if-lez v4, :cond_1

    :goto_1
    invoke-static {p2, p0, p1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/SampleAggregator;->getSampleData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;JJ)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-wide p0, v0

    goto :goto_1
.end method

.method private static getSampleData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;JJ)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .locals 15

    sub-long v0, p3, p1

    sget-wide v2, Lcom/sec/android/service/health/cp/database/datahandler/SampleAggregator;->SAMPLE_TIME_INTERVAL:J

    div-long/2addr v0, v2

    long-to-int v7, v0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-gt v7, v0, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    iget v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-lez v4, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    int-to-float v0, v0

    div-float v0, v2, v0

    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-lez v2, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    int-to-float v1, v1

    div-float v1, v3, v1

    :cond_3
    new-array v3, v7, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    div-int v8, v2, v7

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    rem-int v6, v2, v7

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    div-int v9, v2, v7

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    rem-int v5, v2, v7

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    div-int v10, v2, v7

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    rem-int v4, v2, v7

    const/4 v2, 0x0

    move v14, v2

    move v2, v4

    move v4, v5

    move v5, v6

    move v6, v14

    :goto_1
    if-ge v6, v7, :cond_7

    new-instance v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v11}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    aput-object v11, v3, v6

    aget-object v11, v3, v6

    iput v8, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    aget-object v11, v3, v6

    iput v9, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    aget-object v11, v3, v6

    iput v10, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-lez v5, :cond_4

    aget-object v11, v3, v6

    iget v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/lit8 v12, v12, 0x1

    iput v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/lit8 v5, v5, -0x1

    :cond_4
    if-lez v4, :cond_5

    aget-object v11, v3, v6

    iget v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    add-int/lit8 v12, v12, 0x1

    iput v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    add-int/lit8 v4, v4, -0x1

    :cond_5
    if-lez v2, :cond_6

    aget-object v11, v3, v6

    iget v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/lit8 v12, v12, 0x1

    iput v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/lit8 v2, v2, -0x1

    :cond_6
    aget-object v11, v3, v6

    mul-int/lit8 v12, v6, 0x3c

    mul-int/lit16 v12, v12, 0x3e8

    int-to-long v12, v12

    add-long v12, v12, p1

    iput-wide v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    aget-object v11, v3, v6

    aget-object v12, v3, v6

    iget v12, v12, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    aget-object v13, v3, v6

    iget v13, v13, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    add-int/2addr v12, v13

    aget-object v13, v3, v6

    iget v13, v13, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v12, v13

    iput v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    aget-object v11, v3, v6

    aget-object v12, v3, v6

    iget v12, v12, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    int-to-float v12, v12

    mul-float/2addr v12, v0

    iput v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    aget-object v11, v3, v6

    aget-object v12, v3, v6

    iget v12, v12, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    int-to-float v12, v12

    mul-float/2addr v12, v1

    iput v12, v11, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_7
    move-object v0, v3

    goto/16 :goto_0
.end method
