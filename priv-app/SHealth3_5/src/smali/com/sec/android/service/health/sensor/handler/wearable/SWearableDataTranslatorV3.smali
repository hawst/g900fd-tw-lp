.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;
.super Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;-><init>()V

    return-void
.end method

.method private addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/16 v1, 0x20

    const/16 v2, 0x2d

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x14

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addHDIDHyphen_changed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addHDIDHyphen_fromGear : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addHDIDHyphen_Error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    goto :goto_0
.end method

.method private addPedometerGoalHistory([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Landroid/content/Context;Ljava/lang/String;)V
    .locals 9

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    move v8, v3

    :goto_0
    array-length v0, p1

    if-ge v8, v0, :cond_2

    aget-object v0, p1, v8

    if-eqz v0, :cond_0

    iget-wide v1, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-eqz v1, :cond_0

    const v2, 0x9c41

    iget v4, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    move-object v0, p0

    move-object v1, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setGoalDataValueWithUserDeviceId(Landroid/content/Context;IIIJLjava/lang/String;)I

    :cond_0
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "History <<SGoal>> : goal is null. "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private getJCoachingProfile(Landroid/content/Context;)[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;
    .locals 7

    const/4 v6, 0x4

    new-array v1, v6, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

    new-array v2, v6, [I

    fill-array-data v2, :array_0

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->lastDBtime:J

    new-instance v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    aget v4, v2, v0

    iput v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->type:I

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x3

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeGoal:J

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x2

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->distanceGoal:F

    aget-object v3, v1, v0

    aget v4, v2, v0

    invoke-virtual {p0, p1, v4, v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->caloryGoal:F

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x5

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->trainingEffectIntensityGoal:F

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/4 v5, 0x7

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->trainingEffectTimeGoal:J

    aget-object v3, v1, v0

    aget v4, v2, v0

    const/16 v5, 0x8

    invoke-virtual {p0, p1, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->noGoal:I

    aget-object v3, v1, v0

    iget-wide v4, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->lastDBtime:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeStamp:J

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :array_0
    .array-data 4
        0x2712
        0x2afa
        0x36b2
        0x2ee1
    .end array-data
.end method

.method private getJCoachingVar(Landroid/content/Context;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;
    .locals 11

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, -0x1

    new-instance v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;

    invoke-direct {v6}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "create_time DESC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ac"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-char v0, v0

    iput-char v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->ac:C

    const-string/jumbo v0, "maximum_heart_rate"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-char v0, v0

    iput-char v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->maxHeartRate:C

    const-string/jumbo v0, "maximum_met"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->maxMET:J

    const-string/jumbo v0, "recovery_resource"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->recourceRecovery:I

    const-string/jumbo v0, "start_time"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->startDate:J

    const-string/jumbo v0, "training_level"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->trainingLevel:I

    const-string/jumbo v0, "training_level_update"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->lastTrainingLevelUpdate:J

    const-string/jumbo v0, "previous_training_level"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->previousTrainingLevel:I

    const-string/jumbo v0, "previous_to_previous_training_level"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->previousToPreviousTrainingLevel:I

    const-string v0, "latest_feedback_phrase_number"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->latestFeedbackPhraseNumber:I

    const-string v0, "latest_exercise_time"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->latestExerciseTime:J

    :goto_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v6

    :cond_2
    const/16 v0, 0x32

    iput-char v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->ac:C

    iput-char v10, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->maxHeartRate:C

    iput-wide v8, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->maxMET:J

    iput v10, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->recourceRecovery:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->startDate:J

    iput v7, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->trainingLevel:I

    iput-wide v8, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->lastTrainingLevelUpdate:J

    iput v7, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->previousTrainingLevel:I

    iput v7, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->previousToPreviousTrainingLevel:I

    iput v7, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->latestFeedbackPhraseNumber:I

    iput-wide v8, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->latestExerciseTime:J

    goto :goto_0
.end method

.method private getJPedometerGoal(Landroid/content/Context;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;
    .locals 8

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "set_time"

    aput-object v0, v2, v5

    const-string/jumbo v0, "value"

    aput-object v0, v2, v4

    const/4 v0, 0x2

    const-string v1, "goal_type"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "goal_subtype"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "update_time"

    aput-object v1, v2, v0

    const-string v3, "goal_type=? "

    new-array v4, v4, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v5

    const v0, 0x9c41

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    if-eqz p1, :cond_5

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v5, "set_time DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStepCount(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-int v0, v0

    if-ge v3, v0, :cond_1

    const/4 v0, 0x0

    iput v0, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->isAcheived:I

    :goto_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "pedometer goal= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "set_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    const-string v0, "goal_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->type:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v0, v2

    :goto_2
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_3
    return-object v0

    :cond_1
    const/4 v0, 0x1

    :try_start_3
    iput v0, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->isAcheived:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v7, v0

    move-object v0, v2

    move-object v2, v1

    move-object v1, v7

    :goto_4
    :try_start_4
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v4, "Goal error"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :cond_2
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v1, v6

    move-object v0, v6

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_5
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    move-object v0, v6

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    :cond_4
    move-object v0, v6

    goto :goto_2

    :cond_5
    move-object v1, v6

    goto/16 :goto_0
.end method

.method private getJProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;
    .locals 4

    new-instance v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, -0x1

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->age:I

    :goto_0
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->gender:I

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->heightUnit:I

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->height:F

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weightUnit:I

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->activityClass:I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getProfileUpdatedTime(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->time:J

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getSystemTimeMillisForBirthday(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->birthday:J

    invoke-static {p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getDistanceUnit(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->distanceUnit:I

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->name:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivity()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->activity:I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getProfile:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getSystemTimeMillisForBirthday(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getAgeValue(J)I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->age:I

    goto :goto_0
.end method

.method private getJUVProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;
    .locals 8

    const/4 v6, 0x0

    new-instance v7, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;

    invoke-direct {v7}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;-><init>()V

    if-eqz p1, :cond_4

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "create_time DESC limit 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "create_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v7, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;->time:J

    const-string/jumbo v0, "skin_tone"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;->skinTone:I

    const-string/jumbo v0, "skin_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;->skinType:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v7

    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v1, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_4
    move-object v1, v6

    goto :goto_0
.end method

.method private getLastGoalFromDB(Landroid/content/Context;ILjava/lang/String;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;
    .locals 8

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v0, "value"

    aput-object v0, v2, v1

    const-string/jumbo v0, "set_time"

    aput-object v0, v2, v5

    const-string v3, "goal_type=? AND user_device__id=? AND set_time<=?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    aput-object p3, v4, v5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v5, "set_time DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    invoke-direct {v6}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;-><init>()V

    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    const-string/jumbo v0, "set_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v6

    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v1, v6

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private getWearableAppRegistryData(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "connected"

    const-string v0, "bt_id"

    const-string/jumbo v0, "package_name"

    const-string v0, "device_name"

    const-string v0, "last_launch"

    const-string v0, "device_fixed_name"

    const-string v0, "com.samsung.android.uhm.framework.appregistry.BaseContentProvider.provider"

    const-string v0, "content://com.samsung.android.uhm.framework.appregistry.BaseContentProvider.provider/Device"

    const-string v0, "content://com.samsung.android.uhm.framework.appregistry.BaseContentProvider.provider/Device"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "package_name"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, "device_name"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "bt_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "last_launch"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const-string v0, "connected"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v0, "device_fixed_name"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    sget-object v9, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "wearablePckgName = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " wearableName = "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wearableId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " lastLaunch = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " connectionStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wearableFixedName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v7
.end method

.method private isExistHistoryGoalData(Landroid/content/Context;IIJILjava/lang/String;)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "value"

    aput-object v1, v2, v0

    const-string v3, "goal_type=? AND value=? AND user_device__id=? AND set_time=?"

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object p7, v4, v0

    const/4 v0, 0x3

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isExistGoalData() goalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " activityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    move v0, v7

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_0
.end method

.method private isTodaysGoal(J)Z
    .locals 4

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetData(Landroid/content/Context;JI)V
    .locals 14

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "resetData() pedo"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-eqz v2, :cond_1

    move-wide/from16 v0, p2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v12

    const-wide/16 v10, -0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT EX._id, UD._id FROM exercise AS EX, user_device AS UD, exercise_device_info AS EDI  WHERE EX.start_time BETWEEN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/32 v3, 0x5265c00

    add-long/2addr v3, v12

    const-wide/16 v5, 0x1

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND EX.exercise_type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x4e23

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND EX._id = EDI.exercise__id AND EDI.user_device__id = UD._id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND UD.device_type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-eqz v3, :cond_4

    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v2, 0x1

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    :goto_0
    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_0
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    const-string v3, "_id = ?"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v9

    invoke-virtual {v6, v7, v3, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time BETWEEN "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND user_device__id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v3, 0x0

    move-object v2, v9

    move-wide v4, v10

    goto :goto_0

    :catchall_0
    move-exception v2

    move-object v3, v8

    :goto_1
    if-eqz v3, :cond_3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    :catchall_1
    move-exception v2

    goto :goto_1

    :cond_4
    move-object v2, v9

    move-wide v4, v10

    goto :goto_0
.end method

.method private resetSleepData(Landroid/content/Context;JILjava/lang/String;)V
    .locals 9

    const-wide/16 v6, 0x1

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x2932e00

    invoke-virtual {p0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v2

    sub-long v2, p2, v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v2

    add-long/2addr v2, v0

    invoke-virtual {p0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getEndOfDay(J)J

    move-result-wide v4

    add-long/2addr v0, v4

    :goto_0
    sget-object v4, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "startTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " endTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "bed_time>=? AND bed_time<=? AND user_device__id=?"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v0

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v2

    sub-long/2addr v2, v6

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v2

    add-long/2addr v2, v0

    invoke-virtual {p0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v4

    sub-long/2addr v4, v6

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getEndOfDay(J)J

    move-result-wide v4

    add-long/2addr v0, v4

    goto :goto_0
.end method

.method private setCoachingResponseData(Landroid/content/Context;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;)V
    .locals 18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v3

    const-wide v5, 0x8b0bb400L

    sub-long/2addr v3, v5

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "start_time > "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "start_time"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " < "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    if-eqz p1, :cond_d

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v7, v1

    :goto_0
    if-eqz v7, :cond_7

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "exercise_type"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v2, "distance"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v2, "duration_millisecond"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v11, v2

    const-wide/32 v2, 0xea60

    div-long v2, v11, v2

    long-to-int v13, v2

    const-string/jumbo v2, "start_time"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-string v4, "end_time"

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const-string v4, "_id"

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v16, 0x0

    cmp-long v6, v11, v16

    if-lez v6, :cond_0

    const-wide/16 v16, 0x0

    cmp-long v6, v14, v16

    if-eqz v6, :cond_0

    cmp-long v2, v2, v14

    if-nez v2, :cond_2

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "durationMin <= 0 || endTime == 0 || startTime == endTime = TRUE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    :cond_2
    const/16 v2, 0x4e21

    if-eq v1, v2, :cond_3

    const/16 v2, 0x4e22

    if-ne v1, v2, :cond_6

    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "exercise__id = "

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;-><init>()V

    iput-wide v14, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->endTime:J

    int-to-double v3, v10

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->distance:D

    const-string/jumbo v3, "training_load_peak"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->eteTrainingLoadPeak:I

    const-string/jumbo v3, "maximal_met"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->eteMaxMET:I

    const-string/jumbo v3, "resource_recovery"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->eteResourceRecovery:I

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCoachingResult with TLP : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_3
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_5
    int-to-double v2, v10

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_4

    new-instance v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;-><init>()V

    iput-wide v14, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->endTime:J

    iput v13, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->duration:I

    int-to-double v3, v10

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->distance:D

    iput-wide v11, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->durationMilliSecond:J

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCoachingResult temp : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_6
    int-to-double v1, v10

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_1

    new-instance v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;-><init>()V

    iput-wide v14, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->endTime:J

    iput v13, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->duration:I

    iput-wide v11, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->durationMilliSecond:J

    int-to-double v2, v10

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;->distance:D

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SCoachingResult temp : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_7
    if-eqz v7, :cond_8

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_8
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    const/4 v1, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v4, v2, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

    add-int/lit8 v3, v2, 0x1

    aput-object v1, v4, v2

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EXERCISE Add data"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto :goto_4

    :cond_9
    move-object/from16 v0, p2

    iput-object v4, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

    :goto_5
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_c

    const/4 v1, 0x0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v4, v2, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;

    add-int/lit8 v3, v2, 0x1

    aput-object v1, v4, v2

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RUNNING Add data"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto :goto_6

    :cond_a
    const/4 v1, 0x0

    move-object/from16 v0, p2

    iput-object v1, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

    goto :goto_5

    :cond_b
    move-object/from16 v0, p2

    iput-object v4, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingRunningExercise:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;

    :goto_7
    return-void

    :cond_c
    const/4 v1, 0x0

    move-object/from16 v0, p2

    iput-object v1, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingRunningExercise:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;

    goto :goto_7

    :cond_d
    move-object v7, v1

    goto/16 :goto_0
.end method

.method private setDistanceUnit(ILandroid/content/Context;)V
    .locals 4

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-static {p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getDistanceUnit(Landroid/content/Context;)I

    move-result v0

    :goto_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    const-string v2, "distance_unit"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_1
    const v0, 0x29811

    goto :goto_0

    :pswitch_2
    const v0, 0x29813

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x29811
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private setGoalDataValueWithUserDeviceId(Landroid/content/Context;IIIJLjava/lang/String;)I
    .locals 11

    const/4 v10, -0x1

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p4

    move-wide/from16 v6, p5

    move v8, p3

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->isExistHistoryGoalData(Landroid/content/Context;IIJILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v3, "Goal already exists!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setGoalDataValueWithUserDeviceId() goalType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " activityType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vlaue : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " time : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ret : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v10

    :cond_1
    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->isTodaysGoal(J)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p7

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getLastGoalFromDB(Landroid/content/Context;ILjava/lang/String;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    move-result-object v2

    if-eqz v2, :cond_3

    iget v3, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    if-eq v3, p4, :cond_0

    move-wide/from16 v0, p5

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v3

    iget-wide v5, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v5

    cmp-long v2, v3, v5

    if-nez v2, :cond_2

    new-instance v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;-><init>()V

    iput p2, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->type:I

    iput p4, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    move-wide/from16 v0, p5

    iput-wide v0, v2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    move-object/from16 v0, p7

    invoke-direct {p0, p1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->updateTodaysGoal(Landroid/content/Context;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Ljava/lang/String;)I

    move-result v10

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "set_time"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v3, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v2, v2

    :goto_1
    move v10, v2

    goto/16 :goto_0

    :catch_0
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setGoalDataValueWithUserDeviceId() Exception occured while Goal insert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v10

    goto :goto_1

    :cond_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "set_time"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v3, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    long-to-int v10, v2

    goto/16 :goto_0

    :catch_1
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setGoalDataValueWithUserDeviceId() Exception occured while Goal insert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "set_time"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v3, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide v2

    long-to-int v10, v2

    goto/16 :goto_0

    :catch_2
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setGoalDataValueWithUserDeviceId() Exception occured while Goal insert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private toParcelableCoaching(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;-><init>()V

    iget-char v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->ac:C

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->ac:C

    iget-char v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->maxHeartRate:C

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxHeartRate:C

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->maxMET:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxMET:J

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->recourceRecovery:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->recourceRecovery:I

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->startDate:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->startDate:J

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->trainingLevel:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->trainingLevel:I

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->lastTrainingLevelUpdate:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->lastTrainingLevelUpdate:J

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->previousTrainingLevel:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousTrainingLevel:I

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->previousToPreviousTrainingLevel:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousToPreviousTrainingLevel:I

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->latestFeedbackPhraseNumber:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestFeedbackPhraseNumber:I

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;->latestExerciseTime:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestExerciseTime:J

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method private toParcelableExercise([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;
    .locals 10

    const/4 v0, 0x0

    if-eqz p1, :cond_10

    array-length v0, p1

    new-array v2, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<EXERCISE>> : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v3, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "ea received"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_f

    array-length v1, v2

    new-array v3, v1, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;

    aget-object v1, p1, v0

    aput-object v1, v3, v0

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;-><init>()V

    aput-object v1, v2, v0

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->time:J

    iput-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    aget-object v1, v3, v0

    iget v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->type:I

    const/16 v4, 0x2712

    if-ne v1, v4, :cond_0

    aget-object v1, v2, v0

    const/16 v4, 0x4652

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    :goto_1
    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->duration:J

    iput-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->calorie:D

    iput-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->heartRate:D

    iput-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->distance:D

    iput-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->fitnessLevel:I

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    aget-object v1, p1, v0

    iget-object v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

    if-eqz v1, :cond_4

    aget-object v1, v2, v0

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;->averageSpeed:F

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->averageSpeed:F

    aget-object v1, v2, v0

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;->maxSpeed:F

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxSpeed:F

    aget-object v1, v2, v0

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;->maxHeartRate:F

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxHeartRate:F

    aget-object v1, v2, v0

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;->maxAltitude:F

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxAltitude:F

    aget-object v1, v2, v0

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;->minAltitude:F

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->minAltitude:F

    aget-object v1, p1, v0

    iget-object v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

    iget-object v4, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;->heartRateRawData:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateRawData;

    if-eqz v4, :cond_4

    aget-object v1, v2, v0

    array-length v5, v4

    new-array v5, v5, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    iput-object v5, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    const/4 v1, 0x0

    :goto_2
    array-length v5, v4

    if-ge v1, v5, :cond_4

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    new-instance v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    invoke-direct {v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;-><init>()V

    aput-object v6, v5, v1

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v5, v5, v1

    aget-object v6, v4, v1

    iget v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateRawData;->heartRate:I

    iput v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->heartRate:I

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v5, v5, v1

    aget-object v6, v4, v1

    iget-wide v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateRawData;->samplingTime:J

    iput-wide v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->samplingTime:J

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_0
    aget-object v1, v3, v0

    iget v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->type:I

    const/16 v4, 0x2afa

    if-ne v1, v4, :cond_1

    aget-object v1, v2, v0

    const/16 v4, 0x4653

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_1
    aget-object v1, v3, v0

    iget v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->type:I

    const/16 v4, 0x36b2

    if-ne v1, v4, :cond_2

    aget-object v1, v2, v0

    const/16 v4, 0x4654

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_2
    aget-object v1, v3, v0

    iget v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->type:I

    const/16 v4, 0x2ee1

    if-ne v1, v4, :cond_3

    aget-object v1, v2, v0

    const/16 v4, 0x4655

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_3
    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->type:I

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_4
    aget-object v1, v3, v0

    iget-object v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    if-eqz v1, :cond_7

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    array-length v4, v4

    new-array v4, v4, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iput-object v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    const/4 v1, 0x0

    :goto_3
    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    array-length v4, v4

    if-ge v1, v4, :cond_7

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    new-instance v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    invoke-direct {v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;-><init>()V

    aput-object v5, v4, v1

    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v4, v4, v1

    if-eqz v4, :cond_5

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->time:J

    iput-wide v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {}, Lcom/samsung/android/sdk/health/Shealth;->isChinaModel()Z

    move-result v4

    if-nez v4, :cond_6

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->latitude:D

    iput-wide v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->longitude:D

    iput-wide v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    :goto_4
    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->altitude:D

    iput-wide v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->altitude:D

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->accuracy:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->speed:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->bearing:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->bearing:F

    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    if-eqz v4, :cond_5

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    new-instance v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    invoke-direct {v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;-><init>()V

    iput-object v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->averageSpeed:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->totalDistance:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->inclineDistance:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->declineDistance:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->flatDistance:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatDistance:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->inclineTime:J

    iput-wide v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineTime:J

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->declineTime:J

    iput-wide v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineTime:J

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->flatTime:J

    iput-wide v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatTime:J

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->consumedCalorie:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;->stepCount:I

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->stepCount:I

    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extraLocation:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocationBundle;

    if-eqz v4, :cond_5

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->extraLocation:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocationBundle;

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocationBundle;->pace:F

    iput v5, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->pace:F

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    :cond_6
    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v4, v4, v1

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->latitude:D

    aget-object v6, v3, v0

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

    aget-object v6, v6, v1

    iget-wide v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;->longitude:D

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert;->WGSToGCJ(DD)Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;

    move-result-object v4

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v5, v5, v1

    iget-wide v6, v4, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->latitude:D

    iput-wide v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v5, v5, v1

    iget-wide v6, v4, Lcom/sec/android/service/health/sensor/handler/wearable/LocationConvert$LatLng;->longitude:D

    iput-wide v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    goto/16 :goto_4

    :cond_7
    aget-object v1, v2, v0

    if-eqz v1, :cond_c

    if-eqz p2, :cond_c

    const/4 v1, 0x0

    :goto_5
    array-length v4, p2

    if-ge v1, v4, :cond_9

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<<COCHING RESULT>> : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ea received"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v4, p2, v1

    aget-object v5, v2, v0

    new-instance v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;

    invoke-direct {v6}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;-><init>()V

    iput-object v6, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v6, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->endTime:J

    iput-wide v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v6, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->distance:D

    iput-wide v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->distance:D

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v6, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->eteTrainingLoadPeak:I

    iput v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteTrainingLoadPeak:I

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v6, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->eteMaxMET:I

    iput v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteMaxMET:I

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v6, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->eteResourceRecovery:I

    iput v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteResourceRecovery:I

    const-wide v5, 0x4008147ae147ae14L    # 3.01

    cmpl-double v5, p3, v5

    if-ltz v5, :cond_8

    aget-object v5, v2, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->devicePkId:Ljava/lang/String;

    :cond_8
    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    aget-object v6, v2, v0

    iget-wide v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    aget-object v8, v2, v0

    iget-wide v8, v8, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    add-long/2addr v6, v8

    sub-long/2addr v4, v6

    const-wide/16 v6, -0x64

    cmp-long v6, v4, v6

    if-lez v6, :cond_b

    const-wide/16 v6, 0x64

    cmp-long v4, v4, v6

    if-gez v4, :cond_b

    :cond_9
    :goto_6
    aget-object v1, v2, v0

    if-eqz v1, :cond_e

    const-wide v4, 0x4008147ae147ae14L    # 3.01

    cmpl-double v1, p3, v4

    if-ltz v1, :cond_e

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->recoveryTime:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iput-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->recoveryTime:J

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->crud:I

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->crud:I

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->trainingEffect:F

    iput v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->trainingEffect:F

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->fatBurnTime:J

    iput-wide v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fatBurnTime:J

    aget-object v1, v2, v0

    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    aget-object v1, v3, v0

    iget-object v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

    if-eqz v1, :cond_e

    aget-object v1, v3, v0

    iget-object v1, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

    array-length v1, v1

    new-array v4, v1, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;

    const/4 v1, 0x0

    :goto_7
    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

    array-length v5, v5

    if-ge v1, v5, :cond_d

    aget-object v5, v3, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

    aget-object v5, v5, v1

    if-eqz v5, :cond_a

    new-instance v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;

    invoke-direct {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;-><init>()V

    aput-object v5, v4, v1

    aget-object v5, v4, v1

    aget-object v6, v3, v0

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

    aget-object v6, v6, v1

    iget v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;->achivedType:I

    iput v6, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;->achievedType:I

    aget-object v5, v4, v1

    aget-object v6, v3, v0

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

    aget-object v6, v6, v1

    iget-wide v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;->achivedValue:D

    iput-wide v6, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;->achievedValue:D

    aget-object v5, v4, v1

    aget-object v6, v3, v0

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

    aget-object v6, v6, v1

    iget v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;->achievedBadge:I

    iput v6, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;->achievedBadge:I

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_b
    aget-object v4, v2, v0

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    :cond_c
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v4, "<<EXERCISE>> :  exerciseData[i] is null "

    invoke-static {v1, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_d
    aget-object v1, v2, v0

    iput-object v4, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    :cond_e
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    aget-object v3, v2, v0

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_f
    move-object v0, v2

    :goto_8
    return-object v0

    :cond_10
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "exerciseData is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method

.method private toParcelableHeartRateMonitor([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    array-length v0, p1

    new-array v1, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<<HeartRateMonitor DATA>> : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "ea received."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v0, p1

    new-array v2, v0, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    aget-object v3, p1, v0

    aput-object v3, v2, v0

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->time:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->heartRate:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->eventTime:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->interval:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->interval:J

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->SNR:F

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNR:F

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->SNRUnit:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNRUnit:I

    const-wide v3, 0x4008147ae147ae14L    # 3.01

    cmpl-double v3, p2, v3

    if-ltz v3, :cond_0

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->devicePkId:Ljava/lang/String;

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->crud:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->crud:I

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->tagging:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagging:Ljava/lang/String;

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;->tagIndex:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagIndex:I

    :cond_0
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "<<STRESS DATA>> is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private toParcelablePedometer([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .locals 8

    const/4 v1, 0x0

    const v7, 0x7fffffff

    const/4 v0, 0x0

    if-eqz p1, :cond_6

    array-length v0, p1

    new-array v2, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    array-length v0, p1

    new-array v4, v0, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_5

    aget-object v3, p1, v0

    aput-object v3, v4, v0

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->time:J

    iput-wide v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->distance:F

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->calories:F

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->speed:F

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->totalStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->runStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->walkStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->updownStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    aget-object v3, v2, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-lez v3, :cond_2

    aget-object v3, v2, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    if-nez v3, :cond_0

    aget-object v3, v2, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    if-nez v3, :cond_0

    aget-object v3, v2, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-eqz v3, :cond_1

    :cond_0
    aget-object v3, v2, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    if-ne v3, v7, :cond_4

    aget-object v3, v2, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    if-ne v3, v7, :cond_4

    aget-object v3, v2, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-ne v3, v7, :cond_4

    :cond_1
    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_2

    aget-object v3, v2, v0

    aget-object v5, v2, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    :cond_2
    const-wide v5, 0x4008147ae147ae14L    # 3.01

    cmpl-double v3, p2, v5

    if-ltz v3, :cond_3

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->crud:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->crud:I

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->walkTime:J

    iput-wide v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkTime:J

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->runTime:J

    iput-wide v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runTime:J

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->healthyStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->healthyStep:I

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->activeTime:J

    iput-wide v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->activeTime:J

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->devicePkId:Ljava/lang/String;

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->upStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->upStep:I

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->downStep:I

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->downStep:I

    aget-object v3, v2, v0

    aget-object v5, v4, v0

    iget v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->upStep:I

    aget-object v6, v4, v0

    iget v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;->downStep:I

    add-int/2addr v5, v6

    iput v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    :cond_3
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    aget-object v5, v2, v0

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    move-object v0, v2

    :goto_2
    return-object v0

    :cond_6
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "<<PEDOMETER DATA>> is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private toParcelableSleep([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    array-length v0, p1

    new-array v1, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    array-length v0, p1

    new-array v2, v0, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    aget-object v3, p1, v0

    aput-object v3, v2, v0

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->startTime:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->startTime:J

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->endTime:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->endTime:J

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->efficiency:D

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->efficiency:D

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->status:[I

    iput-object v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->status:[I

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->time:[J

    iput-object v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->time:[J

    const-wide v3, 0x4008147ae147ae14L    # 3.01

    cmpl-double v3, p2, v3

    if-ltz v3, :cond_0

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->crud:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->crud:I

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->rating:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->rating:I

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->tagging:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->tagging:Ljava/lang/String;

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->tagIndex:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->tagIndex:I

    aget-object v3, v1, v0

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->devicePkId:Ljava/lang/String;

    :cond_0
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "<<SLEEP DATA>> is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private toParcelableStress([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;
    .locals 11

    const/4 v10, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_7

    array-length v4, p1

    new-array v5, v4, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<<STRESS DATA>> : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "ea received."

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v0, p1

    new-array v6, v0, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;

    move v0, v1

    move v2, v1

    :goto_0
    array-length v7, v5

    if-ge v0, v7, :cond_3

    aget-object v7, p1, v0

    aput-object v7, v6, v0

    new-instance v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    invoke-direct {v7}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;-><init>()V

    aput-object v7, v5, v0

    aget-object v7, v5, v0

    aget-object v8, v6, v0

    iget v8, v8, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;->state:I

    iput v8, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    aget-object v7, v5, v0

    aget-object v8, v6, v0

    iget-wide v8, v8, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;->time:J

    iput-wide v8, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->time:J

    aget-object v7, v5, v0

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-eqz v7, :cond_0

    aget-object v7, v5, v0

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-ne v7, v10, :cond_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    :cond_1
    const-wide v7, 0x4008147ae147ae14L    # 3.01

    cmpl-double v7, p2, v7

    if-ltz v7, :cond_2

    aget-object v7, v5, v0

    aget-object v8, v6, v0

    iget v8, v8, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;->crud:I

    iput v8, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->crud:I

    aget-object v7, v5, v0

    aget-object v8, v6, v0

    iget v8, v8, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;->stressValue:F

    iput v8, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->stressValue:F

    aget-object v7, v5, v0

    aget-object v8, v6, v0

    iget-object v8, v8, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->devicePkId:Ljava/lang/String;

    :cond_2
    sget-object v7, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    aget-object v8, v5, v0

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    if-ne v1, v4, :cond_4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "<<STRESS DATA>> all value includes error status. it will not deleiver to application."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    :goto_1
    return-object v0

    :cond_4
    sub-int v0, v4, v2

    new-array v2, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    move v0, v1

    :goto_2
    if-ge v1, v4, :cond_8

    aget-object v3, v5, v1

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-eqz v3, :cond_5

    aget-object v3, v5, v1

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-ne v3, v10, :cond_6

    :cond_5
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    aget-object v3, v5, v1

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "<<STRESS DATA>> is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    goto :goto_1

    :cond_8
    move-object v0, v2

    goto :goto_1
.end method

.method private toParcelableUvRay([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;
    .locals 6

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "toParcelableUvRay() uvRayJData = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " version = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_2

    const-wide v0, 0x4008147ae147ae14L    # 3.01

    cmpl-double v0, p2, v0

    if-ltz v0, :cond_2

    array-length v0, p1

    new-array v1, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    aget-object v3, p1, v0

    if-eqz v3, :cond_0

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;-><init>()V

    aget-object v4, p1, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;->index:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    aget-object v4, p1, v0

    iget v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;->crud:I

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->crud:I

    aget-object v4, p1, v0

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;->time:J

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addHDIDHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->devicePkId:Ljava/lang/String;

    aput-object v3, v1, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    aput-object v2, v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_2
    return-object v0

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method private updateCoachingProfile([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;Landroid/content/Context;)V
    .locals 9

    if-eqz p1, :cond_1

    array-length v0, p1

    new-array v8, v0, [Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    array-length v0, p1

    if-ge v7, v0, :cond_2

    aget-object v0, p1, v7

    aput-object v0, v8, v7

    aget-object v0, v8, v7

    if-eqz v0, :cond_0

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->type:I

    const/4 v3, 0x4

    aget-object v0, v8, v7

    iget v0, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->caloryGoal:F

    float-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->type:I

    const/4 v3, 0x2

    aget-object v0, v8, v7

    iget v0, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->distanceGoal:F

    float-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->type:I

    const/4 v3, 0x3

    aget-object v0, v8, v7

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeGoal:J

    long-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->type:I

    const/4 v3, 0x5

    aget-object v0, v8, v7

    iget v0, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->trainingEffectIntensityGoal:F

    float-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->type:I

    const/4 v3, 0x7

    aget-object v0, v8, v7

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->trainingEffectTimeGoal:J

    long-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->type:I

    const/16 v3, 0x8

    aget-object v0, v8, v7

    iget v4, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->noGoal:I

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "<<SCoachingProfile>> : profile is null. "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private updatePedometerGoal(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    const-wide/16 v3, 0x0

    const v6, 0x9c41

    const/4 v5, 0x0

    if-eqz p1, :cond_7

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updatePedometerGoal() pedometer goal= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p2, v6, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getLastGoalFromDB(Landroid/content/Context;ILjava/lang/String;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    move-result-object v0

    if-eqz v0, :cond_5

    iget v1, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    if-eq v1, v2, :cond_3

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v0

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-direct {p0, p2, p1, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->updateTodaysGoal(Landroid/content/Context;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "period"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "goal_type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "goal_subtype"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "value"

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "set_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "user_device__id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePedometerGoal() Exception occured while Goal insert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "Goal time is 0"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget v1, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    if-ne v1, v2, :cond_0

    iget-wide v1, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    iget-wide v3, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v0

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    invoke-direct {p0, p2, p1, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->updateTodaysGoal(Landroid/content/Context;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "period"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "goal_type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "goal_subtype"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "value"

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "set_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "user_device__id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePedometerGoal() Exception occured while Goal insert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-wide v0, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_6

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "period"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "goal_type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "goal_subtype"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "value"

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "set_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "user_device__id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_2
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePedometerGoal() Exception occured while Goal insert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "Goal time is 0"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "<<SGoal>> : goal is null. "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private updateProfile(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;Landroid/content/Context;D)V
    .locals 9

    const v8, 0x29813

    if-nez p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "<<PROFILE>> : Profile is null. "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<PROFILE>> : Profile is received. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getProfileUpdatedTime(Landroid/content/Context;)J

    move-result-wide v1

    iget-wide v3, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->time:J

    :try_start_0
    sget-object v5, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Shealth profile time ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Wearable time ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v5, v5, v3

    if-ltz v5, :cond_4

    cmp-long v1, v1, v3

    if-gez v1, :cond_4

    const-string/jumbo v1, "sync with wearable profile"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->height:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->gender:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setGender(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weightUnit:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->heightUnit:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->distanceUnit:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setDistanceUnit(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->activityClass:I

    const v2, 0x2bf21

    if-lt v1, v2, :cond_1

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->activityClass:I

    const v2, 0x2bf25

    if-gt v1, v2, :cond_1

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->activityClass:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivityType(I)V

    :cond_1
    iget-wide v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->birthday:J

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getyyyyMMddForBirthday(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setBirthDate(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-wide v1, 0x4008147ae147ae14L    # 3.01

    cmpl-double v1, p3, v1

    if-ltz v1, :cond_2

    iget-object v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setName(Ljava/lang/String;)V

    :cond_2
    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->activity:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivity(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->distanceUnit:I

    invoke-direct {p0, v1, p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setDistanceUnit(ILandroid/content/Context;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " send unit ====>>>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->distanceUnit:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->distanceUnit:I

    if-ne v2, v8, :cond_3

    const-string v2, "DISTANCE_UNIT"

    const v3, 0x29813

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V

    invoke-virtual {p2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "IllegalArgumentException, profile will not be updated."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    :cond_3
    :try_start_1
    const-string v2, "DISTANCE_UNIT"

    const v3, 0x29811

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v1, "<<PROFILE>> : Profile does not need to update"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private updateTodaysGoal(Landroid/content/Context;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Ljava/lang/String;)I
    .locals 7

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v1

    iget-wide v3, p2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const-string/jumbo v1, "user_device__id=? AND goal_type>=? AND set_time>=? AND set_time<=?"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    aput-object p3, v2, v0

    const/4 v3, 0x1

    const v4, 0x9c41

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getStartOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v4, "value"

    iget v5, p2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->goal:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v4, "set_time"

    iget-wide v5, p2, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;->time:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateTodaysGoal() Exception occured while Goal update: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "Not Today\'s Goal!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public makeMessageFromIntent(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Landroid/content/Intent;I)Landroid/os/Message;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public makeMessageFromJWearableData(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;I)Landroid/os/Message;
    .locals 10

    const/4 v6, 0x0

    if-eqz p3, :cond_8

    iget-wide v0, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->resetTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    if-eqz p2, :cond_0

    iget-wide v0, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->resetTime:J

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->resetData(Landroid/content/Context;JI)V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v0

    const/16 v1, 0x2727

    if-ne v0, v1, :cond_0

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->resetTime:J

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->resetSleepData(Landroid/content/Context;JILjava/lang/String;)V

    :cond_0
    iget-object v0, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->sleep:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;

    if-eqz v0, :cond_c

    iget-object v0, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->sleep:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;

    iget-wide v1, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->toParcelableSleep([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    move-result-object v0

    :goto_0
    iget-object v1, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->pedometerGoalHistory:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    iget-object v1, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->pedometerGoalHistory:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, p1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->addPedometerGoalHistory([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->pedometer:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;

    if-eqz v1, :cond_6

    iget-object v1, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->pedometer:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->toParcelablePedometer([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-result-object v1

    :goto_1
    iget-object v2, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->stress:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;

    if-eqz v2, :cond_b

    iget-object v2, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->stress:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;

    iget-wide v3, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->toParcelableStress([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    move-result-object v2

    :goto_2
    iget-object v3, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingVar:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;

    if-eqz v3, :cond_a

    iget-object v3, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingVar:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->toParcelableCoaching(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    move-result-object v3

    :goto_3
    iget-object v4, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->exerciseResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;

    if-eqz v4, :cond_7

    iget-object v4, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

    if-eqz v4, :cond_7

    iget-object v4, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->exerciseResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;

    iget-object v5, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

    iget-wide v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, v4, v5, v7, v8}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->toParcelableExercise([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    move-result-object v4

    :goto_4
    iget-object v5, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->heartMonitor:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;

    if-eqz v5, :cond_9

    iget-object v5, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->heartMonitor:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;

    iget-wide v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, v5, v7, v8}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->toParcelableHeartRateMonitor([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    move-result-object v5

    :goto_5
    iget-object v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->pedometerGoal:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    if-eqz v7, :cond_2

    if-eqz p2, :cond_2

    iget-object v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->pedometerGoal:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, p1, v8}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->updatePedometerGoal(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    iget-object v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->userProfile:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;

    if-eqz v7, :cond_3

    iget-object v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->userProfile:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;

    iget-wide v8, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, v7, p1, v8, v9}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->updateProfile(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;Landroid/content/Context;D)V

    :cond_3
    iget-object v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingProfile:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

    if-eqz v7, :cond_4

    iget-object v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingProfile:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

    invoke-direct {p0, v7, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->updateCoachingProfile([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;Landroid/content/Context;)V

    :cond_4
    iget-object v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->uvRay:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;

    if-eqz v7, :cond_5

    iget-object v6, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->uvRay:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;

    iget-wide v7, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, v6, v7, v8}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->toParcelableUvRay([Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;D)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    move-result-object v6

    :cond_5
    new-instance v7, Landroid/os/Message;

    invoke-direct {v7}, Landroid/os/Message;-><init>()V

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    iput p4, v7, Landroid/os/Message;->what:I

    const-string v9, "STRESS"

    invoke-virtual {v8, v9, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v2, "SLEEP"

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "PEDO"

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "COACHINGVAR"

    invoke-virtual {v8, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "EXERCISE"

    invoke-virtual {v8, v0, v4}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "HRM"

    invoke-virtual {v8, v0, v5}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "UVRAY"

    invoke-virtual {v8, v0, v6}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "VERSION"

    iget-wide v1, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-virtual {v8, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    const-string v0, "DEVICE"

    iget-object v1, p3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->device:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    move-object v6, v7

    :goto_6
    return-object v6

    :cond_6
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "wearableData.pedometer is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v6

    goto/16 :goto_1

    :cond_7
    sget-object v4, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "wearableData.exerciseResult && wearableData.coachingResult is null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    goto/16 :goto_4

    :cond_8
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "wearableData is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_9
    move-object v5, v6

    goto/16 :goto_5

    :cond_a
    move-object v3, v6

    goto/16 :goto_3

    :cond_b
    move-object v2, v6

    goto/16 :goto_2

    :cond_c
    move-object v0, v6

    goto/16 :goto_0
.end method

.method public sendCoachingResponse(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/String;DJ)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-super/range {p0 .. p7}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->sendCoachingResponse(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/String;DJ)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "super.sendCoachingResponse() is false"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :cond_0
    invoke-static {p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getCoachingResponseActionName(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    cmpl-double v2, p4, v4

    if-nez v2, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "This is Gear_tizen, Gear2 device."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_1
    const-wide v4, 0x4008147ae147ae14L    # 3.01

    cmpl-double v2, p4, v4

    if-ltz v2, :cond_2

    new-instance v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;-><init>()V

    iput-object p3, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->device:Ljava/lang/String;

    iput-wide p4, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-direct {p0, p1, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->setCoachingResponseData(Landroid/content/Context;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;)V

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getJCoachingVar(Landroid/content/Context;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingVar:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getJProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->changeProfileWeight(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->userProfile:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getJCoachingProfile(Landroid/content/Context;)[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->coachingProfile:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->getJUVProfile(Landroid/content/Context;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->uvProfile:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;

    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "EXTRA_COACHING_RESPONSE"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;->TAG:Ljava/lang/String;

    const-string v2, "coaching_response, Old version GearS~"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
