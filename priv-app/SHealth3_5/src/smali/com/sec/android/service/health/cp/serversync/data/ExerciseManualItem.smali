.class public Lcom/sec/android/service/health/cp/serversync/data/ExerciseManualItem;
.super Ljava/lang/Object;


# instance fields
.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public distanceUnit:Ljava/lang/String;

.field public durationMilli:Ljava/lang/Long;

.field public endTime:Ljava/lang/String;

.field public exerciseId:J

.field public exerciseName:Ljava/lang/String;

.field public exerciseTypeCode:Ljava/lang/String;

.field public fatBurnTime:Ljava/lang/Long;

.field public hdid:Ljava/lang/String;

.field public heartRate:Ljava/lang/Double;

.field public inputType:Ljava/lang/String;

.field public level:Ljava/lang/Integer;

.field public requestType:Ljava/lang/String;

.field public startTime:Ljava/lang/String;

.field public totalCadence:Ljava/lang/Double;

.field public totalCalorie:Ljava/lang/Double;

.field public totalDistance:Ljava/lang/Double;

.field public totalDuration:Ljava/lang/Integer;

.field public totalRunStep:Ljava/lang/Double;

.field public totalUpDownStep:Ljava/lang/Double;

.field public totalWalkStep:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
