.class public Lcom/sec/android/service/health/cp/serversync/data/LocationItem;
.super Ljava/lang/Object;


# instance fields
.field public accessaryId:Ljava/lang/String;

.field public accuracy:Ljava/lang/Double;

.field public altitude:Ljava/lang/Double;

.field public appId:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceLocationDataPKId:Ljava/lang/Long;

.field public deviceLogPKId:J

.field public devicePKId:Ljava/lang/Long;

.field public deviceTimeZone:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public latitude:D

.field public longitude:D

.field public responseCode:Ljava/lang/String;

.field public sampleTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
