.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;
.super Ljava/lang/Object;


# static fields
.field public static final ACTION_CHANNEL_CHANGED:I = 0xc9

.field public static final ACTION_DATA_TRANSFER_FINISH:I = 0xce

.field public static final ACTION_DATA_TRANSFER_START:I = 0xcd

.field public static final ACTION_HEALTH_SERVICE_ERROR:I = 0xcc

.field public static final ACTION_MEASUREMENTS_RECEIVED:I = 0xcb

.field public static final ACTION_SCAN_RESULT:I = 0xd0

.field public static final ACTION_SENSOR_INFO_RECEIVED:I = 0xca

.field public static final ACTION_WATCH_DEVICE_LIST_UPDATED:I = 0xcf

.field public static final CMD_NAME:[Ljava/lang/String;

.field public static final COM_END:I = 0x1

.field public static final COM_START:I = 0x0

.field public static final DATA_ANT_FS_DEVICE_TYPE:Ljava/lang/String; = "AntfsDeviceType"

.field public static final DATA_ANT_FS_MANUFACTURER_ID:Ljava/lang/String; = "AntfsManufacturerId"

.field public static final DATA_ANT_SEARCH_ALREADY_CONNECTED:Ljava/lang/String; = "AlreadyConnected"

.field public static final DATA_ANT_SEARCH_DEVICE_NUMBER:Ljava/lang/String; = "SearchDeviceNumber"

.field public static final DATA_ANT_SEARCH_DEVICE_TYPE:Ljava/lang/String; = "SearchDeviceType"

.field public static final DATA_ANT_SEARCH_DISPLAY_NAME:Ljava/lang/String; = "SearchDisplayName"

.field public static final DATA_ANT_SEARCH_MANUFACTURER_ID:Ljava/lang/String; = "SearchManufacturerId"

.field public static final DATA_ANT_SEARCH_PRODUCT_ID:Ljava/lang/String; = "SearchProductId"

.field public static final DATA_MEASUREMENTS:Ljava/lang/String; = "measurements"

.field public static final DATA_SENSOR_INFO:Ljava/lang/String; = "sensor_info"

.field public static final DATA_WATCH_SEARCH_RESULT:Ljava/lang/String; = "DisplayName"

.field public static final DATA_WATCH_UUID:Ljava/lang/String; = "UUID"

.field public static final ERASE_DATA:I = 0x26

.field public static final ERROR_LIST:I = 0x27

.field public static final FIRMWARE_UPDATE:I = 0x29

.field public static final GET_ALARM:I = 0x20

.field public static final GET_ATUO_TRANS:I = 0x1c

.field public static final GET_AUTO_CONNECT_SYNC_TIME:I = 0x1a

.field public static final GET_BATTERY:I = 0x16

.field public static final GET_BLUETOOTH_ID:I = 0x17

.field public static final GET_CONNECTION_TIME:I = 0x7

.field public static final GET_DEVICE_TIME:I = 0x5

.field public static final GET_DEVICE_TYPE:I = 0x3

.field public static final GET_FIRMWARE_VERSION:I = 0x15

.field public static final GET_MEASUREMENT_DATA:I = 0x4

.field public static final GET_MODEL_NAME:I = 0x2

.field public static final GET_PROTOCOL_VERSION:I = 0x9

.field public static final GET_SERIAL_NUM:I = 0x14

.field public static final GET_TRANS_MODE:I = 0x1e

.field public static final GET_UNIT:I = 0x18

.field public static final GET_USER:I = 0x24

.field public static final INVALID:I = 0x64

.field public static final MEASURE_START:I = 0x22

.field public static final MEASURE_STOP:I = 0x23

.field public static final POWER_OFF:I = 0x28

.field public static final SET_ALARM:I = 0x21

.field public static final SET_AUTO_CONNECT_SYNC_TIME:I = 0x1b

.field public static final SET_AUTO_TRANS:I = 0x1d

.field public static final SET_CONNECTION_TIME:I = 0x8

.field public static final SET_DEVICE_TIME:I = 0x6

.field public static final SET_TRANS_MODE:I = 0x1f

.field public static final SET_UNIT:I = 0x19

.field public static final SET_USER:I = 0x25


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x33

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "START"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "END"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "GMOD"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "GDTYE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "GMDAT"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "GDTIM"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SDTIM"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GCONT"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SCONT"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "GPVER"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "GSERI"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "GFVER"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "GBATT"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "GBLUI"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "GUNIT"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "SUNIT"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "GSYNC"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "SSYNC"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "GATRA"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "SATRA"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "GTMOD"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "STMOD"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "GALAM"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "SALAM"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "MSTAR"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "MSTOP"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "GUSER"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "SUSER"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "ERASE"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "GERR"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "PWOFF"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "UPDAT"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, ""

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/CommandSet;->CMD_NAME:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
