.class Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
.super Landroid/os/AsyncTask;
.source "UploadAsyncTask.java"

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/service/health/connectionmanager2/UploadParameters;",
        "Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;",
        "Lcom/sec/android/service/health/connectionmanager2/UploadParameters;",
        ">;",
        "Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;"
    }
.end annotation


# instance fields
.field private cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

.field private connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private context:Lorg/apache/http/protocol/BasicHttpContext;

.field private entity:Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;

.field private httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

.field private httppost:Lorg/apache/http/client/methods/HttpPost;

.field private httpresponse:Lorg/apache/http/HttpResponse;

.field private responseBody:Ljava/lang/String;

.field private totalSize:J

.field private uploadComplete:Z

.field private uploadedBytes:J

.field private uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;


# direct methods
.method protected constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)V
    .locals 4
    .param p1, "connectionmanager"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "uploadparameters"    # Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httppost:Lorg/apache/http/client/methods/HttpPost;

    .line 34
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->entity:Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;

    .line 35
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    .line 36
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpresponse:Lorg/apache/http/HttpResponse;

    .line 37
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .line 38
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 39
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 40
    iput-wide v2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadedBytes:J

    .line 41
    iput-wide v2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->totalSize:J

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadComplete:Z

    .line 43
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    .line 44
    iput-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 55
    iput-object p2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .line 56
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->getHttpClientManager()Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    .line 57
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;)Lorg/apache/http/client/methods/HttpPost;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httppost:Lorg/apache/http/client/methods/HttpPost;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;Lcom/sec/android/service/health/connectionmanager2/NetException;)Lcom/sec/android/service/health/connectionmanager2/NetException;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;
    .param p1, "x1"    # Lcom/sec/android/service/health/connectionmanager2/NetException;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    return-object p1
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)Lcom/sec/android/service/health/connectionmanager2/UploadParameters;
    .locals 23
    .param p1, "params"    # [Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .prologue
    .line 67
    const/4 v14, -0x1

    .line 69
    .local v14, "responseCode":I
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    .line 71
    .local v13, "response":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getCompleteFilePath()Ljava/lang/String;

    move-result-object v19

    if-nez v19, :cond_0

    .line 73
    const/16 v19, 0x0

    .line 202
    :goto_0
    return-object v19

    .line 75
    :cond_0
    const/16 v19, 0x0

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getSetHeadervalues()Ljava/util/HashMap;

    move-result-object v16

    .line 76
    .local v16, "setHeaderValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v19, v0

    if-nez v19, :cond_1

    .line 78
    new-instance v15, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v19, -0x1

    const/16 v20, -0x7

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v15, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 79
    .local v15, "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/16 v19, 0x0

    aget-object v19, p1, v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setResponse(Ljava/lang/Object;)V

    .line 80
    const/16 v19, 0x0

    aget-object v19, p1, v19

    goto :goto_0

    .line 83
    .end local v15    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_1
    new-instance v6, Ljava/io/File;

    const/16 v19, 0x0

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getCompleteFilePath()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 84
    .local v6, "f":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v19

    move-wide/from16 v0, v19

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->totalSize:J

    .line 87
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getCompleteFilePath()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getCompleteFilePath()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_3

    .line 89
    :cond_2
    const/16 v19, 0x0

    goto :goto_0

    .line 91
    :cond_3
    new-instance v19, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getUrl()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httppost:Lorg/apache/http/client/methods/HttpPost;

    .line 122
    if-eqz v16, :cond_6

    .line 124
    const/16 v18, 0x0

    .line 125
    .local v18, "value":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 126
    .local v11, "key":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 127
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "key":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 128
    .restart local v11    # "key":Ljava/lang/String;
    if-eqz v11, :cond_4

    .line 130
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "value":[Ljava/lang/String;
    check-cast v18, [Ljava/lang/String;

    .line 131
    .restart local v18    # "value":[Ljava/lang/String;
    if-eqz v18, :cond_5

    .line 133
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_2
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v10, v0, :cond_4

    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httppost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v19, v0

    aget-object v20, v18, v10

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v11, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 138
    .end local v10    # "k":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httppost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v11, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 190
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "key":Ljava/lang/String;
    .end local v18    # "value":[Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 192
    .local v4, "e":Lorg/apache/http/client/ClientProtocolException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-static {v14, v4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setResponse(Ljava/lang/Object;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    goto/16 :goto_0

    .line 147
    .end local v4    # "e":Lorg/apache/http/client/ClientProtocolException;
    :cond_6
    :try_start_1
    new-instance v19, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;-><init>(Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->entity:Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;

    .line 148
    const/4 v7, 0x0

    .line 150
    .local v7, "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getFileName()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_7

    .line 151
    new-instance v7, Lorg/apache/http/entity/mime/content/FileBody;

    .end local v7    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getFileName()Ljava/lang/String;

    move-result-object v19

    const-string v20, "application/octet-stream"

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v7, v6, v0, v1, v2}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .restart local v7    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getEntityName()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_8

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->entity:Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getEntityName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 160
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httppost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->entity:Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 161
    new-instance v12, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;

    invoke-direct {v12}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;-><init>()V

    .line 162
    .local v12, "progressstatus":Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;
    sget-object v19, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    move-object/from16 v0, v19

    iput-object v0, v12, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;->status:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    .line 163
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->totalSize:J

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    iput-wide v0, v12, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;->sizeUploaded:J

    .line 164
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v12, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httppost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->context:Lorg/apache/http/protocol/BasicHttpContext;

    move-object/from16 v21, v0

    invoke-interface/range {v19 .. v21}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpresponse:Lorg/apache/http/HttpResponse;

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpresponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v14

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpresponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v8

    .line 169
    .local v8, "httpEntity":Lorg/apache/http/HttpEntity;
    if-eqz v8, :cond_a

    .line 174
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v19, Ljava/io/InputStreamReader;

    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v20

    const-string/jumbo v21, "utf-8"

    invoke-direct/range {v19 .. v21}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 176
    .local v3, "bufferedReader":Ljava/io/BufferedReader;
    :goto_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v17

    .local v17, "strLine":Ljava/lang/String;
    if-eqz v17, :cond_9

    .line 178
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    .line 195
    .end local v3    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v7    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    .end local v8    # "httpEntity":Lorg/apache/http/HttpEntity;
    .end local v12    # "progressstatus":Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;
    .end local v17    # "strLine":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 197
    .local v4, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    invoke-static {v14, v4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setResponse(Ljava/lang/Object;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    goto/16 :goto_0

    .line 153
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v7    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    :cond_7
    :try_start_2
    new-instance v7, Lorg/apache/http/entity/mime/content/FileBody;

    .end local v7    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    invoke-direct {v7, v6}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;)V

    .restart local v7    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    goto/16 :goto_3

    .line 158
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->entity:Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;

    move-object/from16 v19, v0

    const-string v20, "file"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/service/health/connectionmanager2/UploadMultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    goto/16 :goto_4

    .line 180
    .restart local v3    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v8    # "httpEntity":Lorg/apache/http/HttpEntity;
    .restart local v12    # "progressstatus":Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;
    .restart local v17    # "strLine":Ljava/lang/String;
    :cond_9
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 181
    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    .line 184
    .end local v3    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v17    # "strLine":Ljava/lang/String;
    :cond_a
    const/16 v19, 0xc8

    move/from16 v0, v19

    if-eq v14, v0, :cond_b

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/16 v21, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v14, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/String;)V

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setResponse(Ljava/lang/Object;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 201
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->httpresponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v21

    const/16 v22, 0x0

    invoke-direct/range {v20 .. v22}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setResponse(Ljava/lang/Object;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-object/from16 v19, v0

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, [Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->doInBackground([Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    move-result-object v0

    return-object v0
.end method

.method protected getUploadparameters()Lcom/sec/android/service/health/connectionmanager2/UploadParameters;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    return-object v0
.end method

.method protected onCancelled(Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)V
    .locals 10
    .param p1, "result"    # Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploadTaskCompleted(J)V

    .line 267
    iget-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadComplete:Z

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->totalSize:J

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/service/health/connectionmanager2/NetException;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 280
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadedBytes:J

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->cancelledException:Lcom/sec/android/service/health/connectionmanager2/NetException;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadedBytes:J

    iget-object v7, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/service/health/connectionmanager2/NetException;

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->onCancelled(Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)V

    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)V
    .locals 11
    .param p1, "result"    # Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->connectionmanager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->uploadTaskCompleted(J)V

    .line 239
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getResponse()Ljava/lang/Object;

    move-result-object v10

    .line 240
    .local v10, "obj":Ljava/lang/Object;
    instance-of v0, v10, Lcom/sec/android/service/health/connectionmanager2/NetException;

    if-eqz v0, :cond_0

    move-object v7, v10

    .line 242
    check-cast v7, Lcom/sec/android/service/health/connectionmanager2/NetException;

    .line 243
    .local v7, "NE":Lcom/sec/android/service/health/connectionmanager2/NetException;
    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadComplete:Z

    if-eqz v0, :cond_1

    .line 245
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->totalSize:J

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 252
    .end local v7    # "NE":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_0
    :goto_0
    return-void

    .line 249
    .restart local v7    # "NE":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadedBytes:J

    invoke-virtual {p1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->onPostExecute(Lcom/sec/android/service/health/connectionmanager2/UploadParameters;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;)V
    .locals 10
    .param p1, "uploadprogressstatus"    # [Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 214
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 216
    aget-object v0, p1, v5

    iget-object v0, v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;->status:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    if-ne v0, v1, :cond_1

    .line 218
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    iget-wide v5, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->totalSize:J

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getRequestId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v3}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getPrivateId()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->ONGOING:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    aget-object v5, p1, v5

    iget-wide v5, v5, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;->sizeUploaded:J

    iget-object v8, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadparameters:Lcom/sec/android/service/health/connectionmanager2/UploadParameters;

    invoke-virtual {v8}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->getTag()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->responseBody:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, [Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->onProgressUpdate([Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;)V

    return-void
.end method

.method protected stopUpload(Lcom/sec/android/service/health/connectionmanager2/NetException;)V
    .locals 2
    .param p1, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;

    .prologue
    .line 320
    move-object v0, p1

    .line 321
    .local v0, "temp":Lcom/sec/android/service/health/connectionmanager2/NetException;
    new-instance v1, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask$1;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask$1;-><init>(Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;Lcom/sec/android/service/health/connectionmanager2/NetException;)V

    invoke-virtual {v1}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask$1;->start()V

    .line 334
    return-void
.end method

.method public transferred(J)V
    .locals 4
    .param p1, "num"    # J

    .prologue
    const/4 v3, 0x1

    .line 300
    iget-wide v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->totalSize:J

    cmp-long v1, p1, v1

    if-ltz v1, :cond_0

    .line 302
    iput-boolean v3, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadComplete:Z

    .line 310
    :goto_0
    return-void

    .line 305
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->uploadedBytes:J

    .line 306
    new-instance v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;

    invoke-direct {v0}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;-><init>()V

    .line 307
    .local v0, "progressstatus":Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->ONGOING:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    iput-object v1, v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;->status:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    .line 308
    iput-wide p1, v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;->sizeUploaded:J

    .line 309
    new-array v1, v3, [Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$UploadProgressStatus;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/connectionmanager2/UploadAsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0
.end method
