.class public Lcom/sec/android/service/health/cp/common/DBTables$ExerciseInfoTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExerciseInfoTable"
.end annotation


# static fields
.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_DELYN:Ljava/lang/String; = "deleted"

.field public static final KEY_FAVORITE:Ljava/lang/String; = "favorite"

.field public static final KEY_IMAGE:Ljava/lang/String; = "image"

.field public static final KEY_KCAL:Ljava/lang/String; = "kcal"

.field public static final KEY_LANG:Ljava/lang/String; = "lang"

.field public static final KEY_MY:Ljava/lang/String; = "my"

.field public static final KEY_NAME:Ljava/lang/String; = "name"

.field public static final KEY_PINYIN:Ljava/lang/String; = "pinyin"

.field public static final KEY_PINYIN_SORT:Ljava/lang/String; = "pinyin_sort"

.field public static final TABLE_NAME:Ljava/lang/String; = "exercise_info"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
