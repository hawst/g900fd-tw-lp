.class public Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetCustomItem$ItemList;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetCustomItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemList"
.end annotation


# instance fields
.field public activityType:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public calorie:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public customYN:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceItemPKId:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public favoriteYN:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public itemId:Ljava/lang/String;

.field public metabolic:Ljava/lang/String;

.field public metabolicHigh:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public partsOfBody:Ljava/lang/String;

.field public repetitionNumber:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public rootCategoryId:Ljava/lang/String;

.field public serverLocale:Ljava/lang/String;

.field public serverSourceType:Ljava/lang/String;

.field public setNumber:Ljava/lang/String;

.field public sorting1:Ljava/lang/String;

.field public sorting2:Ljava/lang/String;

.field public subCategoryId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
