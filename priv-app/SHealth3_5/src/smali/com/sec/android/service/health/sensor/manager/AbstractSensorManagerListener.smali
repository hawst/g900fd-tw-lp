.class public interface abstract Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "[",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onDataRecordStarted(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZ)V
.end method

.method public abstract onDataStarted(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onDataStopped(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V
.end method

.method public abstract onStateChanged(I)V
.end method

.method public abstract onStateChanged(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
.end method
