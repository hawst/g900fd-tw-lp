.class public Lcom/sec/android/service/health/connectionmanager2/UploadParameters;
.super Ljava/lang/Object;
.source "UploadParameters.java"


# instance fields
.field Listener:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

.field PrivateId:I

.field RequestId:J

.field private Response:Ljava/lang/Object;

.field Tag:Ljava/lang/Object;

.field Url:Ljava/lang/String;

.field completeFilePath:Ljava/lang/String;

.field entityName:Ljava/lang/String;

.field fileName:Ljava/lang/String;

.field private setHeadervalues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "RequestId"    # J
    .param p3, "PrivateId"    # I
    .param p4, "Listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;
    .param p5, "Url"    # Ljava/lang/String;
    .param p6, "completeFilePath"    # Ljava/lang/String;
    .param p7, "Tag"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Listener:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    .line 18
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Url:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->completeFilePath:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Tag:Ljava/lang/Object;

    .line 22
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setHeadervalues:Ljava/util/HashMap;

    .line 23
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->fileName:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->entityName:Ljava/lang/String;

    .line 39
    iput-wide p1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->RequestId:J

    .line 40
    iput p3, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->PrivateId:I

    .line 41
    iput-object p4, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Listener:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    .line 42
    iput-object p5, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Url:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->completeFilePath:Ljava/lang/String;

    .line 44
    iput-object p7, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Tag:Ljava/lang/Object;

    .line 45
    return-void
.end method

.method public constructor <init>(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 0
    .param p1, "RequestId"    # J
    .param p3, "PrivateId"    # I
    .param p4, "Listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;
    .param p5, "Url"    # Ljava/lang/String;
    .param p6, "Filename"    # Ljava/lang/String;
    .param p7, "Tag"    # Ljava/lang/Object;
    .param p8, "fileName"    # Ljava/lang/String;
    .param p9, "entityName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p10, "setHeadervalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p7}, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;-><init>(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    iput-object p10, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setHeadervalues:Ljava/util/HashMap;

    .line 62
    iput-object p8, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->fileName:Ljava/lang/String;

    .line 63
    iput-object p9, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->entityName:Ljava/lang/String;

    .line 64
    return-void
.end method


# virtual methods
.method public getCompleteFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->completeFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getEntityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->entityName:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getListener()Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Listener:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;

    return-object v0
.end method

.method public getPrivateId()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->PrivateId:I

    return v0
.end method

.method public getRequestId()J
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->RequestId:J

    return-wide v0
.end method

.method public getResponse()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Response:Ljava/lang/Object;

    return-object v0
.end method

.method public getSetHeadervalues()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->setHeadervalues:Ljava/util/HashMap;

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Tag:Ljava/lang/Object;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Url:Ljava/lang/String;

    return-object v0
.end method

.method public setResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "response"    # Ljava/lang/Object;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadParameters;->Response:Ljava/lang/Object;

    .line 102
    return-void
.end method
