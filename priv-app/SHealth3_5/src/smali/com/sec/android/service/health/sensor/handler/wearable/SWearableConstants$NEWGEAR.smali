.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$NEWGEAR;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NEWGEAR"
.end annotation


# static fields
.field public static final ACTION_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_GEAR_SYNC"

.field public static final COACHING_RESPONSE:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_SHEALTH_RESPONSE"

.field public static final REQUEST_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_GEAR_REQUEST_SYNC"

.field public static final SERVICE_STATUS:Ljava/lang/String; = "com.samsung.android.shealth.GEAR_SERVICE_STATUS"

.field public static final START_SERVICE:Ljava/lang/String; = "com.samsung.android.shealth.GEAR_START_SERVICE"

.field public static final SYNC_ERROR:Ljava/lang/String; = "com.samsung.android.shealth.GEAR_SYNC_ERROR"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
