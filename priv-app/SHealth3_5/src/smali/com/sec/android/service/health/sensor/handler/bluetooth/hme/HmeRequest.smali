.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;
.super Ljava/lang/Object;


# instance fields
.field mCmd:I

.field mPacket:[B

.field mRetryCount:I

.field mSetTimer:Z


# direct methods
.method public constructor <init>(I[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mCmd:I

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mPacket:[B

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mRetryCount:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mSetTimer:Z

    return-void
.end method

.method public constructor <init>(I[BZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mCmd:I

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mPacket:[B

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mRetryCount:I

    iput-boolean p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mSetTimer:Z

    return-void
.end method


# virtual methods
.method public getCmd()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mCmd:I

    return v0
.end method

.method public getPacket()[B
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mPacket:[B

    return-object v0
.end method

.method public increaseRetryCount()V
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mRetryCount:I

    return-void
.end method

.method public isMaxRetry()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mRetryCount:I

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isSetTimer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeRequest;->mSetTimer:Z

    return v0
.end method
