.class Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;
.super Landroid/os/CountDownTimer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;JJ)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->getMaxValuedataCollection()Ljava/lang/Integer;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    return-void
.end method

.method public onTick(J)V
    .locals 0

    return-void
.end method
