.class public Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;
.super Lsamsung/database/sqlite/SecSQLiteOpenHelper;


# static fields
.field static final CREATE_K_FULL:Ljava/lang/String; = "CREATE_K_FULL"

.field static final INSERT_DEFAULT_DATA_K:Ljava/lang/String; = "INSERT_DEFAULT_DATA_K"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string/jumbo v0, "platform_new.db"

    const/4 v1, 0x0

    const/16 v2, 0xb

    invoke-direct {p0, p1, v0, v1, v2}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->TAG:Ljava/lang/String;

    const-string v1, "initialisation "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onCreate(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate SecSQLiteDatabase db"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v0, "PRAGMA foreign_keys=ON;"

    invoke-virtual {p1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE_K_FULL"

    new-instance v1, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {v1, p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/database/utils/SqlParser;->execSqlFile(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Z

    const-string v0, "INSERT_DEFAULT_DATA_K"

    new-instance v1, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {v1, p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/database/utils/SqlParser;->execSqlFile(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Z

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getDSTSaving(J)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSERT INTO user_device(_id, device_id, model, connectivity_type, device_type, create_time, update_time, time_zone, application__id, sync_status, device_group_type , daylight_saving)  VALUES (\'10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\', \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\', \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\', "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2719

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "1y90e30264"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\', "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x29812

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x57e41

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x445c2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x445c1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->deleteBackupDatabase()V

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Creation of K db failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onUpgrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onUpgrade"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
