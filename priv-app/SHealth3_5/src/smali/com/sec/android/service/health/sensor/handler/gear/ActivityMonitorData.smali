.class public Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private createTime:J

.field private distance:F

.field private exerciseId:J

.field private id:J

.field private kcal:F

.field private kindOfWalking:I

.field private mUserDeviceId:Ljava/lang/String;

.field private runSteps:I

.field private speed:F

.field private timeZone:I

.field private totalStep:I

.field private upDownSteps:I

.field private walkSteps:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->id:J

    return-void
.end method

.method public constructor <init>(JJJIIIIFFFILjava/lang/String;I)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->id:J

    const-wide/16 v1, -0x1

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id should not be "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->id:J

    iput-wide p3, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->exerciseId:J

    iput-wide p5, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->createTime:J

    iput p7, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->totalStep:I

    iput p8, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->runSteps:I

    iput p9, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->walkSteps:I

    iput p10, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->upDownSteps:I

    iput p11, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kcal:F

    move/from16 v0, p12

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->speed:F

    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->distance:F

    move/from16 v0, p14

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kindOfWalking:I

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    move/from16 v0, p16

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->timeZone:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->createTime:J

    return-wide v0
.end method

.method public getDistance()F
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->distance:F

    return v0
.end method

.method public getExerciseId()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->exerciseId:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->id:J

    return-wide v0
.end method

.method public getKcal()F
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kcal:F

    return v0
.end method

.method public getKindOfWalking()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kindOfWalking:I

    return v0
.end method

.method public getRunSteps()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->runSteps:I

    return v0
.end method

.method public getSpeed()F
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->speed:F

    return v0
.end method

.method public getTimeZone()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->timeZone:I

    return v0
.end method

.method public getTotalStep()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->totalStep:I

    return v0
.end method

.method public getUpDownSteps()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->upDownSteps:I

    return v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getWalkSteps()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->walkSteps:I

    return v0
.end method

.method public setCreateTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->createTime:J

    return-void
.end method

.method public setDistance(F)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->distance:F

    return-void
.end method

.method public setExerciseId(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->exerciseId:J

    return-void
.end method

.method public setId(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->id:J

    return-void
.end method

.method public setKcal(F)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kcal:F

    return-void
.end method

.method public setKindOfWalking(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kindOfWalking:I

    return-void
.end method

.method public setRunSteps(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->runSteps:I

    return-void
.end method

.method public setSpeed(F)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->speed:F

    return-void
.end method

.method public setTimeZone(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->timeZone:I

    return-void
.end method

.method public setTotalStep(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->totalStep:I

    return-void
.end method

.method public setUpDownSteps(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->upDownSteps:I

    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    return-void
.end method

.method public setWalkSteps(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->walkSteps:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->exerciseId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->createTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->totalStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->runSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->walkSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->upDownSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->speed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->distance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->kindOfWalking:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/ActivityMonitorData;->timeZone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
