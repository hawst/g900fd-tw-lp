.class public Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
.super Ljava/lang/Object;
.source "SamsungAccountUserTokenManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;,
        Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    }
.end annotation


# static fields
.field private static final ADD_SAMSUNG_ACCOUNT:Ljava/lang/String; = "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

.field private static final KEY_REGISTRATION_CODE:Ljava/lang/String; = "registration_code"

.field private static final OSP_VER:Ljava/lang/String; = "OSP_02"

.field private static final SAMSUNG_ACCOUNT_PREFS:Ljava/lang/String; = "com.sec.android.app.shealth_preferences_samsung_account"

.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

.field public static mLoginId:Ljava/lang/String;


# instance fields
.field private final ID_REQUEST_ACCESSTOKEN:I

.field private REQUEST_LOCK:Ljava/lang/Object;

.field private REQUEST_STATE_LOCK:Ljava/lang/Object;

.field private mAppId:Ljava/lang/String;

.field private mAppSecret:Ljava/lang/String;

.field private mBirthday:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mISaService:Lcom/msc/sa/aidl/ISAService;

.field private mMCC:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mRegistrationCode:Ljava/lang/String;

.field private mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

.field private mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;",
            ">;"
        }
    .end annotation
.end field

.field private mSamsungToken:Ljava/lang/String;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->TAG:Ljava/lang/String;

    .line 61
    new-instance v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mInstance:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mLoginId:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/16 v0, 0x4d3

    iput v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->ID_REQUEST_ACCESSTOKEN:I

    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_LOCK:Ljava/lang/Object;

    .line 64
    iput-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungToken:Ljava/lang/String;

    .line 65
    iput-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mMCC:Ljava/lang/String;

    .line 72
    sget-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 99
    new-instance v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$ArrayUniqueQueue;-><init>(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;

    .line 100
    new-instance v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;-><init>(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 300
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mBirthday:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mMCC:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mMCC:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .param p1, "x1"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->unbindSAService()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->connectToSamsungAccount()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mServiceConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/msc/sa/aidl/ISAService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .param p1, "x1"    # Lcom/msc/sa/aidl/ISAService;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppSecret:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mUserId:Ljava/lang/String;

    return-object p1
.end method

.method private connectToSamsungAccount()V
    .locals 5

    .prologue
    .line 540
    sget-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->TAG:Ljava/lang/String;

    const-string v2, "Try to bind SA service"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    invoke-direct {p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->unbindSAService()V

    .line 542
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 556
    sget-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed to bind service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    iget-object v2, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 559
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 561
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .line 562
    .local v0, "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    if-eqz v0, :cond_0

    .line 564
    const-string v1, "Failed to bind service"

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;->setFailureMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 568
    .end local v0    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 567
    :cond_1
    :try_start_1
    sget-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    iput-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 568
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 570
    :cond_2
    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mInstance:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    return-object v0
.end method

.method public static isDeviceSignInSamsungAccount(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 573
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 574
    .local v1, "manager":Landroid/accounts/AccountManager;
    if-nez v1, :cond_1

    .line 577
    :cond_0
    :goto_0
    return v2

    .line 576
    :cond_1
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 577
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v3, v0

    if-lez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private unbindSAService()V
    .locals 3

    .prologue
    .line 511
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 515
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mISaService:Lcom/msc/sa/aidl/ISAService;

    iget-object v2, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z

    .line 516
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "registration_code"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 517
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRegistrationCode:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    :cond_0
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 536
    :goto_1
    return-void

    .line 519
    :catch_0
    move-exception v0

    .line 521
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 532
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 534
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 528
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public getSamsungAccountBirthday()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mBirthday:Ljava/lang/String;

    return-object v0
.end method

.method public sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .prologue
    .line 487
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 490
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    sget-object v2, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    if-ne v0, v2, :cond_2

    .line 497
    sget-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 498
    invoke-direct {p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->connectToSamsungAccount()V

    .line 505
    :cond_1
    :goto_0
    monitor-exit v1

    .line 506
    return-void

    .line 500
    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    sget-object v2, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    if-ne v0, v2, :cond_1

    .line 502
    sget-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_OFFERED_WHILE_NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    goto :goto_0

    .line 505
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "appSecret"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .prologue
    .line 459
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 461
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mContext:Landroid/content/Context;

    .line 462
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.shealth_preferences_samsung_account"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPrefs:Landroid/content/SharedPreferences;

    .line 466
    :cond_0
    iput-object p2, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppId:Ljava/lang/String;

    .line 467
    iput-object p3, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppSecret:Ljava/lang/String;

    .line 469
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mPackageName:Ljava/lang/String;

    .line 471
    iget-object v2, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 473
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;

    invoke-interface {v0, p4}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 474
    iget-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    sget-object v3, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    if-ne v0, v3, :cond_1

    .line 476
    sget-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    iput-object v0, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 477
    invoke-direct {p0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->connectToSamsungAccount()V

    .line 479
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 482
    return-void

    .line 479
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public startSamsungAccountSignInActivity(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 581
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 582
    .local v1, "packageName":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 583
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "client_id"

    iget-object v3, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 584
    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mAppSecret:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 585
    const-string/jumbo v2, "mypackage"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 586
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    const-string v2, "MODE"

    const-string v3, "ADD_ACCOUNT"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 589
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 590
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 591
    return-void
.end method
