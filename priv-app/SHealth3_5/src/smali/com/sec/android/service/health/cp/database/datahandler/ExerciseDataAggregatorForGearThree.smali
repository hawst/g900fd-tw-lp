.class public Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field private static final MAX_HEARTRATE:I = 0x12c

.field private static final MAX_LATI_LONGITUDE:I = 0xb4

.field private static final MIN_HEARTRATE:I = 0xf

.field private static final MIN_LATI_LONGITUDE:I = -0xb4

.field private static final ONE_HOUR_IN_SEC:I = 0xe10

.field private static final ONE_MINUTE_IN_MILLISEC:I = 0xea60

.field private static final TAG:Ljava/lang/String;

.field private static final THIRD_PARTY_IDENTIFIER:Ljava/lang/String; = "0000"

.field private static mIncrementer:I


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

.field private mExerciseId:J

.field private mMaxSpeed:F

.field mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

.field private mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mIncrementer:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    return-void
.end method

.method private bulkInsertLocationData([Landroid/content/ContentValues;I)Z
    .locals 6

    const/4 v2, 0x0

    const-string v0, " Insert into location_data ( [_id]                        , [application__id]            , [user_device__id]            , [exercise__id]               , [latitude]                   , [longitude]                  , [altitude]                   , [accuracy]                   , [sample_time]                , [daylight_saving]            , [create_time]                , [update_time]                , [time_zone]                  , [sync_status]                  ) values                        ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->getSQLiteDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->compileStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v3

    move v1, v2

    :goto_0
    if-ge v1, p2, :cond_d

    const/4 v4, 0x1

    aget-object v0, p1, v1

    const-string v5, "_id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x2

    aget-object v0, p1, v1

    const-string v5, "application__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_2
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x3

    aget-object v0, p1, v1

    const-string/jumbo v5, "user_device__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_3
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x4

    aget-object v0, p1, v1

    const-string v5, "exercise__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_4
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x5

    aget-object v0, p1, v1

    const-string v5, "latitude"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_5
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x6

    aget-object v0, p1, v1

    const-string v5, "longitude"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, ""

    :goto_6
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x7

    aget-object v0, p1, v1

    const-string v5, "altitude"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v0, ""

    :goto_7
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x8

    aget-object v0, p1, v1

    const-string v5, "accuracy"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v0, ""

    :goto_8
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x9

    aget-object v0, p1, v1

    const-string/jumbo v5, "sample_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    const-string v0, ""

    :goto_9
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xa

    aget-object v0, p1, v1

    const-string v5, "daylight_saving"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    const-string v0, ""

    :goto_a
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xb

    aget-object v0, p1, v1

    const-string v5, "create_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    const-string v0, ""

    :goto_b
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xc

    aget-object v0, p1, v1

    const-string/jumbo v5, "update_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_b

    const-string v0, ""

    :goto_c
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xd

    aget-object v0, p1, v1

    const-string/jumbo v5, "time_zone"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    const-string v0, ""

    :goto_d
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v0, 0xe

    const-wide/32 v4, 0x29812

    invoke-virtual {v3, v0, v4, v5}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindLong(IJ)V

    invoke-virtual {v3}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeInsert()J

    invoke-virtual {v3}, Lsamsung/database/sqlite/SecSQLiteStatement;->clearBindings()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_0
    aget-object v0, p1, v1

    const-string v5, "_id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_1
    aget-object v0, p1, v1

    const-string v5, "application__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_2
    aget-object v0, p1, v1

    const-string/jumbo v5, "user_device__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_3
    aget-object v0, p1, v1

    const-string v5, "exercise__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :cond_4
    aget-object v0, p1, v1

    const-string v5, "latitude"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_5
    aget-object v0, p1, v1

    const-string v5, "longitude"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    :cond_6
    aget-object v0, p1, v1

    const-string v5, "altitude"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :cond_7
    aget-object v0, p1, v1

    const-string v5, "accuracy"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    :cond_8
    aget-object v0, p1, v1

    const-string/jumbo v5, "sample_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    :cond_9
    aget-object v0, p1, v1

    const-string v5, "daylight_saving"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_a

    :cond_a
    aget-object v0, p1, v1

    const-string v5, "create_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_b

    :cond_b
    aget-object v0, p1, v1

    const-string/jumbo v5, "update_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_c

    :cond_c
    aget-object v0, p1, v1

    const-string/jumbo v5, "time_zone"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_d

    :cond_d
    return v2
.end method

.method private bulkInsertRealTimeData([Landroid/content/ContentValues;I)Z
    .locals 6

    const/4 v2, 0x0

    const-string v0, " Insert into realtime_data (  [_id],                      [application__id],          [user_device__id],          [exercise__id],             [data_type],                [speed_per_hour],           [heart_rate_per_min],       [calorie_burn_per_min],     [cadence_rate_per_min],     [stride_length],            [met],                      [incline],                  [resistance],               [power_watt],               [last_heart_beat_time],     [sample_time],              [daylight_saving],          [create_time],              [update_time],              [time_zone],                [sync_status]               ) values                    (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->getSQLiteDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->compileStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v3

    move v1, v2

    :goto_0
    if-ge v1, p2, :cond_15

    const/4 v4, 0x1

    aget-object v0, p1, v1

    const-string v5, "_id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x2

    aget-object v0, p1, v1

    const-string v5, "application__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x3

    aget-object v0, p1, v1

    const-string/jumbo v5, "user_device__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x4

    aget-object v0, p1, v1

    const-string v5, "exercise__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_4
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x5

    aget-object v0, p1, v1

    const-string v5, "data_type"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, ""

    :goto_5
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x6

    aget-object v0, p1, v1

    const-string/jumbo v5, "speed_per_hour"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v0, ""

    :goto_6
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/4 v4, 0x7

    aget-object v0, p1, v1

    const-string v5, "heart_rate_per_min"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v0, ""

    :goto_7
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x8

    aget-object v0, p1, v1

    const-string v5, "calorie_burn_per_min"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    const-string v0, ""

    :goto_8
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x9

    aget-object v0, p1, v1

    const-string v5, "cadence_rate_per_min"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    const-string v0, "0"

    :goto_9
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xa

    aget-object v0, p1, v1

    const-string/jumbo v5, "stride_length"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    const-string v0, ""

    :goto_a
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xb

    aget-object v0, p1, v1

    const-string/jumbo v5, "met"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_b

    const-string v0, ""

    :goto_b
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xc

    aget-object v0, p1, v1

    const-string v5, "incline"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    const-string v0, ""

    :goto_c
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xd

    aget-object v0, p1, v1

    const-string/jumbo v5, "resistance"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_d

    const-string v0, "0"

    :goto_d
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xe

    aget-object v0, p1, v1

    const-string/jumbo v5, "power_watt"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    const-string v0, ""

    :goto_e
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0xf

    aget-object v0, p1, v1

    const-string v5, "last_heart_beat_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_f

    const-string v0, ""

    :goto_f
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x10

    aget-object v0, p1, v1

    const-string/jumbo v5, "sample_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_10

    const-string v0, ""

    :goto_10
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x11

    aget-object v0, p1, v1

    const-string v5, "daylight_saving"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_11

    const-string v0, ""

    :goto_11
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x12

    aget-object v0, p1, v1

    const-string v5, "create_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_12

    const-string v0, ""

    :goto_12
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x13

    aget-object v0, p1, v1

    const-string/jumbo v5, "update_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_13

    const-string v0, ""

    :goto_13
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v4, 0x14

    aget-object v0, p1, v1

    const-string/jumbo v5, "time_zone"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_14

    const-string v0, ""

    :goto_14
    invoke-virtual {v3, v4, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    const/16 v0, 0x15

    const-wide/32 v4, 0x29812

    invoke-virtual {v3, v0, v4, v5}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindLong(IJ)V

    invoke-virtual {v3}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeInsert()J

    invoke-virtual {v3}, Lsamsung/database/sqlite/SecSQLiteStatement;->clearBindings()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_0
    aget-object v0, p1, v1

    const-string v5, "_id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    goto/16 :goto_1

    :cond_1
    aget-object v0, p1, v1

    const-string v5, "_id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_2
    aget-object v0, p1, v1

    const-string v5, "application__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_3
    aget-object v0, p1, v1

    const-string/jumbo v5, "user_device__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_4
    aget-object v0, p1, v1

    const-string v5, "exercise__id"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :cond_5
    aget-object v0, p1, v1

    const-string v5, "data_type"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_6
    aget-object v0, p1, v1

    const-string/jumbo v5, "speed_per_hour"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    :cond_7
    aget-object v0, p1, v1

    const-string v5, "heart_rate_per_min"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :cond_8
    aget-object v0, p1, v1

    const-string v5, "calorie_burn_per_min"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    :cond_9
    aget-object v0, p1, v1

    const-string v5, "cadence_rate_per_min"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    :cond_a
    aget-object v0, p1, v1

    const-string/jumbo v5, "stride_length"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_a

    :cond_b
    aget-object v0, p1, v1

    const-string/jumbo v5, "met"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_b

    :cond_c
    aget-object v0, p1, v1

    const-string v5, "incline"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_c

    :cond_d
    aget-object v0, p1, v1

    const-string/jumbo v5, "resistance"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_d

    :cond_e
    aget-object v0, p1, v1

    const-string/jumbo v5, "power_watt"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_e

    :cond_f
    aget-object v0, p1, v1

    const-string v5, "last_heart_beat_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_f

    :cond_10
    aget-object v0, p1, v1

    const-string/jumbo v5, "sample_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_10

    :cond_11
    aget-object v0, p1, v1

    const-string v5, "daylight_saving"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_11

    :cond_12
    aget-object v0, p1, v1

    const-string v5, "create_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_12

    :cond_13
    aget-object v0, p1, v1

    const-string/jumbo v5, "update_time"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_13

    :cond_14
    aget-object v0, p1, v1

    const-string/jumbo v5, "time_zone"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_14

    :cond_15
    return v2
.end method

.method private checkMandatoryFieldsLocation(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sensor issue - not all mandatory fields are assigned"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "NullPointerException at checkMandatoryFieldsLocation."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getIdTimeStamp(J)J
    .locals 4

    sget v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mIncrementer:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mIncrementer:I

    rem-int/lit8 v0, v0, 0x64

    sput v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mIncrementer:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    sget v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mIncrementer:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private getLocationContentValues(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;JLandroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 6

    const-wide v4, 0x4066800000000000L    # 180.0

    const-wide v2, -0x3f99800000000000L    # -180.0

    iget-wide v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_0

    iget-wide v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_0

    iget-wide v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "sample_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "exercise__id"

    invoke-static {v0, v1, p2, p3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "latitude"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "longitude"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "altitude"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->altitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "accuracy"

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getLocationContentValuesForRealTime(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;JLandroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 4

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mMaxSpeed:F

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    :goto_0
    iput v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mMaxSpeed:F

    const-string/jumbo v0, "sample_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {p4, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v0, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    invoke-static {p4, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-static {p4, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v0, "data_type"

    const v2, 0x493e1

    invoke-static {p4, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v0, "speed_per_hour"

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    const/high16 v3, 0x45610000    # 3600.0f

    mul-float/2addr v2, v3

    invoke-static {p4, v0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;F)V

    return-object v1

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mMaxSpeed:F

    goto :goto_0
.end method

.method private handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;
    .locals 3

    const-string v0, "_id"

    invoke-direct {p0, p2, p3}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->getIdTimeStamp(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "create_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "update_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "sync_status"

    const v1, 0x29812

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    const-string/jumbo v1, "time_zone"

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p2, p3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getDSTSaving(J)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "daylight_saving"

    const v1, 0x445c2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    return-object p1

    :cond_0
    const-string v0, "daylight_saving"

    const v1, 0x445c1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private handleTimeRelatedColumnsForUpdate(Landroid/content/ContentValues;J)Landroid/content/ContentValues;
    .locals 2

    const-string/jumbo v0, "update_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "sync_status"

    const v1, 0x29813

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    const-string/jumbo v1, "time_zone"

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p2, p3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getDSTSaving(J)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "daylight_saving"

    const v1, 0x445c2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    return-object p1

    :cond_0
    const-string v0, "daylight_saving"

    const v1, 0x445c1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private insertCoachingResultData(JLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;)V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertCoachingResultData with exercise ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->distance:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "exercise__id"

    invoke-static {v0, v1, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "maximal_met"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteMaxMET:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "resource_recovery"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteResourceRecovery:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "training_load_peak"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteTrainingLoadPeak:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "end_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "hdid"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->devicePkId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v2, "first_beat_coaching_result"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private insertExerciseActivityData(JLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;)V
    .locals 7

    const/high16 v6, 0x45610000    # 3600.0f

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertExerciseActivityData with exercise ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise__id"

    invoke-static {v0, v1, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "calorie"

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "distance"

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    iget-wide v1, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    const-wide/high16 v3, 0x402e000000000000L    # 15.0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_5

    iget-wide v1, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    const-wide v3, 0x4072c00000000000L    # 300.0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_5

    const-string/jumbo v1, "mean_heart_rate_per_min"

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string/jumbo v1, "max_heart_rate_per_min"

    iget v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxHeartRate:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_0
    iget-wide v1, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "duration_min"

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "duration_millisecond"

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string/jumbo v1, "start_time"

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "end_time"

    iget-wide v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    iget-wide v4, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "max_speed_per_hour"

    iget v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxSpeed:F

    mul-float/2addr v2, v6

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "mean_speed_per_hour"

    iget v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->averageSpeed:F

    mul-float/2addr v2, v6

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_altitude"

    iget v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxAltitude:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "min_altitude"

    iget v2, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->minAltitude:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "hdid"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v1, :cond_1

    const-string v1, "incline_distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "decline_distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v2, "exercise_activity"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_3

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    :cond_3
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void

    :cond_5
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HeartRate is not correct : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private insertExerciseData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "insertExerciseData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise_info__id"

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "start_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "end_time"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    const-wide/high16 v3, 0x402e000000000000L    # 15.0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_2

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    const-wide v3, 0x4072c00000000000L    # 300.0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_2

    const-string v1, "heart_rate"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    :goto_0
    const-string v1, "input_source_type"

    const v2, 0x3f7a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "hdid"

    iget-object v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    const/16 v2, 0x4651

    if-ne v1, v2, :cond_3

    const-string v1, "exercise_type"

    const/16 v2, 0x4e23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_1
    const-string/jumbo v1, "total_calorie"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "distance"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "duration_min"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "duration_millisecond"

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumnsForUpdate(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v2

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v1, "exercise"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hdid = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    cmp-long v3, v0, v6

    if-lez v3, :cond_4

    :goto_2
    return-void

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HeartRate is not correct : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    const-string v1, "exercise_type"

    const/16 v2, 0x4e21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "Data is out of range"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    cmp-long v3, v0, v6

    if-nez v3, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v2, "exercise"

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v3

    invoke-virtual {v1, v2, v8, v0, v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v0

    :cond_5
    cmp-long v2, v0, v6

    if-eqz v2, :cond_6

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_7

    :cond_6
    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid data, Insertion or Updation failed rowId is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_7
    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    iget-object v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertIntoExerciseDeviceTable(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertLocationData([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;J)V

    :cond_8
    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertExerciseActivityData(JLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertHeartRateRawData()V

    :cond_9
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    if-eqz v0, :cond_a

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertExerciseMissionData(J)V

    :cond_a
    iget-object v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    if-eqz v0, :cond_b

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertCoachingResultData(JLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;)V

    :cond_b
    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertFirstBeatCoachingData(J)V

    goto/16 :goto_2
.end method

.method private insertExerciseMissionData(J)V
    .locals 6

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertExerciseMissionData with exercise ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "exercise__id"

    invoke-static {v1, v2, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v2, "mission_achievement_type"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;->achievedBadge:I

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v2, "mission_type"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;->achievedType:I

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v2, "mission_value"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    aget-object v3, v3, v0

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;->achievedValue:D

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v2, "hdid"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "application__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v3, "exercise_mission"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    :cond_1
    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMission;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " values: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method private insertFirstBeatCoachingData(J)V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertFirstBeatCoachingData with exercise ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "exercise__id"

    invoke-static {v0, v1, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v1, "training_effect"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->trainingEffect:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "recovery_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->recoveryTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "fitness_level"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "hdid"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "application__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v2, "first_beat_coaching_data"

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private insertHeartRateRawData()V
    .locals 6

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, " insertHeartRateRawData "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "heart_rate_per_min"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->heartRate:I

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v2, "sample_time"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v3, v3, v0

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->samplingTime:J

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v2, "data_type"

    const v3, 0x493e2

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v2, "exercise__id"

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string/jumbo v2, "user_device__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "hdid"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "application__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string/jumbo v3, "realtime_data"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    :cond_1
    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid data, Insertion failed for URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " values: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method private insertIntoExerciseDeviceTable(Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x0

    const-string v3, "exercise__id = ? and user_device__id = ? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    aput-object v1, v4, v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertIntoExerciseDeviceTable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v5, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v1, "exercise_device_info"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "exercise__id"

    iget-wide v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v2, "user_device__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "hdid"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertIntoExerciseDeviceTable : insert : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "application__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v3, "exercise_device_info"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_0
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private insertLocationData([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;J)V
    .locals 12

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertLocationData with exercise ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v1, p1

    new-array v4, v1, [Landroid/content/ContentValues;

    new-array v5, v1, [Landroid/content/ContentValues;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    array-length v6, p1

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v6, :cond_7

    aget-object v1, p1, v3

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->checkMandatoryFieldsLocation(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Z

    move-result v7

    if-nez v7, :cond_0

    move v1, v0

    move v0, v2

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    :cond_0
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    invoke-direct {p0, v1, p2, p3, v7}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->getLocationContentValues(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;JLandroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v8

    if-nez v8, :cond_1

    sget-object v8, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "contentValues data is not correct"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    invoke-direct {p0, v1, p2, p3, v7}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->getLocationContentValuesForRealTime(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;JLandroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v8

    if-nez v8, :cond_3

    sget-object v7, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "contentValuesForRealTime data is not correct"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    aput-object v8, v4, v0

    iget-object v8, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v8, :cond_2

    aget-object v8, v4, v0

    const-string v9, "application__id"

    iget-object v10, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    aget-object v8, v4, v0

    aget-object v9, v4, v0

    const-string/jumbo v10, "sample_time"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v8

    aput-object v8, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    aput-object v7, v5, v2

    iget-object v7, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v7, :cond_4

    aget-object v7, v5, v2

    const-string v8, "application__id"

    iget-object v9, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v7, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v7, :cond_5

    iget-object v7, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iput-object v7, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    :cond_5
    iget v7, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mMaxSpeed:F

    iget v8, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    cmpg-float v7, v7, v8

    if-gez v7, :cond_6

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    :goto_3
    iput v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mMaxSpeed:F

    aget-object v1, v5, v2

    aget-object v7, v5, v2

    const-string/jumbo v8, "sample_time"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct {p0, v1, v7, v8}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->handleTimeRelatedColumns(Landroid/content/ContentValues;J)Landroid/content/ContentValues;

    move-result-object v1

    aput-object v1, v5, v2

    add-int/lit8 v1, v2, 0x1

    move v11, v1

    move v1, v0

    move v0, v11

    goto/16 :goto_1

    :cond_6
    iget v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mMaxSpeed:F

    goto :goto_3

    :cond_7
    if-gtz v0, :cond_8

    if-lez v2, :cond_9

    :cond_8
    invoke-direct {p0, v4, v0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->bulkInsertLocationData([Landroid/content/ContentValues;I)Z

    invoke-direct {p0, v5, v2}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->bulkInsertRealTimeData([Landroid/content/ContentValues;I)Z

    :cond_9
    return-void
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "addData not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "addData[] called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    :goto_0
    array-length v0, p1

    if-ge v2, v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    aget-object v0, p1, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    aget-object v1, p1, v2

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->insertExerciseData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    invoke-static {}, Ljava/lang/Thread;->yield()V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    :try_start_1
    const-string v0, "ExerciseDataAggregator"

    const-string v1, "Nothing to process"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "Data is out of range"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    invoke-static {}, Ljava/lang/Thread;->yield()V

    :cond_1
    :goto_2
    return v3

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "Error occurred while inserting into db"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    invoke-static {}, Ljava/lang/Thread;->yield()V

    throw v0

    :cond_2
    array-length v0, p1

    if-ne v2, v0, :cond_1

    const/4 v3, 0x1

    goto :goto_2
.end method

.method protected checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public finalizeAggregation()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "Nothing to be done in finalization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mDB:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    return-void
.end method

.method public isRunning(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mExerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;->mExerciseId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "matched"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
