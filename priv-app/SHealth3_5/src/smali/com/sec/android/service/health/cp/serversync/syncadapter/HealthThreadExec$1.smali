.class Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->getMaintenanceNotice(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    const/4 v1, 0x0

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onExceptionReceived : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onExceptionReceived : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mGson:Lcom/google/gson/Gson;

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ExceptionObject;

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ExceptionObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v2

    const/4 v3, -0x3

    if-ne v2, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    const/4 v2, 0x3

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v0, v2, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    iget v2, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ExceptionObject;->httpStatus:I

    const/16 v3, 0x191

    if-ne v2, v3, :cond_1

    const-string v2, "LIC_4104"

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Account Token is not validate. request again"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Samsung Account Token is not validate. request again"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1$1;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    const/4 v2, 0x5

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v0, v2, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onRequestCancelled()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x5

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onResponseReceived()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p4, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v0, v4, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "API-Response"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "response received, requestId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->end(J)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->dump()V

    :cond_1
    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onResponseReceived() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->PrintLongString(Ljava/lang/String;)V

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mGson:Lcom/google/gson/Gson;

    check-cast p4, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ResponseGetMaintenanceNotice;

    invoke-virtual {v0, p4, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ResponseGetMaintenanceNotice;

    if-eqz v0, :cond_5

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ResponseGetMaintenanceNotice;->statusCode:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, "AV"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ResponseGetMaintenanceNotice;->statusCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestType:I
    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->mRequestedDataTypebyApp:I
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;)I

    move-result v2

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->addTempTasks(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$400(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;II)V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    const/16 v2, 0x12

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$ResponseGetMaintenanceNotice;->notification:Ljava/lang/String;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v0, v4, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->sendStartedCallBack(ILjava/lang/String;)V
    invoke-static {v0, v4, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthThreadExec;ILjava/lang/String;)V

    goto/16 :goto_0
.end method
