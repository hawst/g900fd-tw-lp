.class public Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;
.super Ljava/lang/Object;


# static fields
.field public static final CON_MGR_DOWNLOAD_BS:Ljava/lang/String; = "CON_MGR_DOWNLOAD_BS"

.field private static final CON_TIME_OUT:I = 0x493e0

.field private static final EXTRA_KEY_APP_VERSION:Ljava/lang/String; = "AV"

.field private static final EXTRA_KEY_DATASPEC_VERSION:Ljava/lang/String; = "DS"

.field private static final EXTRA_KEY_USER_TOKEN:Ljava/lang/String; = "SU"

.field private static final EXTRA_VALUE_OPENAPI_SPEC_VER:Ljava/lang/String; = "3.36"

.field private static final PACKAGE_NAME_SHEALTH:Ljava/lang/String; = "com.sec.android.app.shealth"

.field public static final STATE_CANCELLED:I = 0x2

.field public static final STATE_ERROR:I = 0x1

.field public static final STATE_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_CONNECTION_PHR:Ljava/lang/String; = "TAG_CONNECTION_PHR"

.field private static httpConnector:Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;


# instance fields
.field private mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private mCurrentDownloadRequestId:J

.field private mCurrentUploadRequestId:J

.field private mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->httpConnector:Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    return-void
.end method

.method private getDefaultHeader()Ljava/util/HashMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v8, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "1y90e30264"

    aput-object v2, v1, v8

    new-array v2, v5, [Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    new-array v3, v5, [Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    new-array v4, v5, [Ljava/lang/String;

    const-string v5, "application/json"

    aput-object v5, v4, v8

    const-string v5, "Accept"

    invoke-virtual {v0, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "access_token"

    invoke-virtual {v0, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v5, "reqAppId"

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "deviceId"

    invoke-virtual {v0, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v5, "x-osp-appId"

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "header : Accept: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v4, v4, v8

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "; access_token: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v2, v2, v8

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; reqAppId: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v4, v1, v8

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; deviceId: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; x-osp-appId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v1, v1, v8

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private getExtraParam()Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.sec.android.app.shealth"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "AV"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "DS"

    const-string v2, "3.36"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SU"

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    const-string v0, "AV"

    const-string v2, "Unknown"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->httpConnector:Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;

    return-object v0
.end method


# virtual methods
.method public cancelRequest()V
    .locals 3

    :try_start_0
    const-string v0, "TAG_CONNECTION_PHR"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->cancelRequest(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    const-string v0, "TAG_CONNECTION_PHR"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentUploadRequestId:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->stopUpload(J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    :try_start_2
    const-string v0, "CON_MGR_DOWNLOAD_BS"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentDownloadRequestId:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->stopDownload(J)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v1, "Error in cancel request"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v1, "Error in stop upload"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v1, "Error in stop download"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public deInitialize()V
    .locals 1

    const-string v0, "TAG_CONNECTION_PHR"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->destoryInstance(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    return-void
.end method

.method public download(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;)I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;,
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    const-string v2, "/"

    invoke-virtual {p4, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    if-nez p3, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Image Url "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v6

    :goto_0
    return v0

    :cond_1
    const-string v0, "//"

    invoke-virtual {p3, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-eqz v0, :cond_2

    const-string v0, "CON_MGR_DOWNLOAD_BS"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "CON_MGR_DOWNLOAD_BS"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const-string v0, "CON_MGR_DOWNLOAD_BS"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {v0, v6, v5, v5}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(ZLjava/lang/String;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const/16 v4, 0x1bb

    invoke-virtual {v0, v2, v4}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddressWithoutLookUp(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v2, "Eror in setAddressWithoutLookUp(server, 443)"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v5, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v2, "download"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "download placed, requestId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v8, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentDownloadRequestId:J

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", filePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mDownLoadconnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v5, "CON_MGR_DOWNLOAD_BS"

    move v1, p1

    move-object v2, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->download(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentDownloadRequestId:J

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentDownloadRequestId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    move v0, v6

    goto/16 :goto_0

    :cond_5
    move v0, v7

    goto/16 :goto_0

    :cond_6
    move v0, v6

    goto/16 :goto_0
.end method

.method public execute(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)I
    .locals 11

    const/4 v10, 0x1

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getExtraParam()Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    move-result-object v5

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "body : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->PrintLongString(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-nez v0, :cond_1

    move v0, v10

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const/4 v8, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move v2, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v3, "API-Request"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "request placed, requestId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", httpapi : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->start(J)V

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, v10

    goto :goto_0
.end method

.method public initialize()Z
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    const-string v0, "TAG_CONNECTION_PHR"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const-string v0, "TAG_CONNECTION_PHR"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const v1, 0x493e0

    const v2, 0x493e0

    sget-boolean v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTPS_ENABLE:Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(IIZLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    sget v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddress(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->deInitialize()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->deInitialize()V

    move v0, v6

    goto :goto_0
.end method

.method public upload(ILjava/lang/Object;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;)I
    .locals 11

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    invoke-static {v1, p3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/File;

    move-object/from16 v0, p5

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v2, "File is missed!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x0

    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v1, p7

    move v4, p1

    move-object v9, p2

    invoke-interface/range {v1 .. v10}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :try_start_1
    instance-of v1, p2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    if-eqz v1, :cond_4

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "body : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v0, p2

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    move-object v1, v0

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->body:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "eng"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    move-object v1, v0

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->body:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->PrintLongString(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    :goto_1
    :try_start_2
    invoke-static/range {p5 .. p5}, Lcom/sec/android/service/health/cp/serversync/util/ImageResizer;->isImageTooBig(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/ImageResizer;->resizeImage(Ljava/lang/String;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v1, v0}, Lcom/sec/android/service/health/cp/serversync/util/ImageResizer;->updateImage(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    :try_start_3
    invoke-static {p3, p4}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->makeApiWithParam(Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;)Ljava/lang/String;

    move-result-object v4

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "body data : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move v2, p1

    move-object/from16 v3, p7

    move-object/from16 v5, p5

    move-object v6, p2

    move-object/from16 v8, p6

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->upload(ILcom/sec/android/service/health/connectionmanager2/IOnUploadListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentUploadRequestId:J

    iget-wide v1, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentUploadRequestId:J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_4
    :try_start_4
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v1

    :try_start_5
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string v3, "Body data is not an instance of ServerApiForSetToServer"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    const-wide/16 v2, 0x0

    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v1, p7

    move v4, p1

    move-object v9, p2

    invoke-interface/range {v1 .. v10}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :catch_2
    move-exception v1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    const-wide/16 v2, 0x0

    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v1, p7

    move v4, p1

    move-object v9, p2

    invoke-interface/range {v1 .. v10}, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;->onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_5
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "upload"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "upload placed, requestId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/serversync/network/HttpConnector;->mCurrentUploadRequestId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", filePath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
