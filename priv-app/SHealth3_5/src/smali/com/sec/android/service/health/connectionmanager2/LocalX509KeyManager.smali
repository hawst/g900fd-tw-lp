.class public Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;
.super Ljava/lang/Object;
.source "LocalX509KeyManager.java"

# interfaces
.implements Ljavax/net/ssl/X509KeyManager;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mAlias:Ljava/lang/String;

.field private mClientKeyStore:Ljava/security/KeyStore;

.field private mDefX509KM:Ljavax/net/ssl/X509KeyManager;

.field private mKeyStorePassword:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljavax/net/ssl/KeyManager;Ljava/lang/String;Ljava/security/KeyStore;Ljava/lang/String;)V
    .locals 6
    .param p1, "km"    # [Ljavax/net/ssl/KeyManager;
    .param p2, "keyName"    # Ljava/lang/String;
    .param p3, "clientKS"    # Ljava/security/KeyStore;
    .param p4, "keyStorePassword"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v4, "Adv.Con.Mgr"

    iput-object v4, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->TAG:Ljava/lang/String;

    .line 27
    iput-object v5, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mDefX509KM:Ljavax/net/ssl/X509KeyManager;

    .line 28
    iput-object v5, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mAlias:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mAlias:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mClientKeyStore:Ljava/security/KeyStore;

    .line 35
    iput-object p4, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mKeyStorePassword:Ljava/lang/String;

    .line 36
    move-object v0, p1

    .local v0, "arr$":[Ljavax/net/ssl/KeyManager;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 38
    .local v2, "k":Ljavax/net/ssl/KeyManager;
    instance-of v4, v2, Ljavax/net/ssl/X509KeyManager;

    if-eqz v4, :cond_1

    .line 40
    check-cast v2, Ljavax/net/ssl/X509KeyManager;

    .end local v2    # "k":Ljavax/net/ssl/KeyManager;
    iput-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mDefX509KM:Ljavax/net/ssl/X509KeyManager;

    .line 44
    :cond_0
    return-void

    .line 36
    .restart local v2    # "k":Ljavax/net/ssl/KeyManager;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 2
    .param p1, "keyType"    # [Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;
    .param p3, "socket"    # Ljava/net/Socket;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->TAG:Ljava/lang/String;

    const-string v1, "X509KeyManager: chooseClientAlias"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mAlias:Ljava/lang/String;

    return-object v0
.end method

.method public chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 2
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;
    .param p3, "socket"    # Ljava/net/Socket;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->TAG:Ljava/lang/String;

    const-string v1, "X509KeyManager: chooseServerAlias"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mDefX509KM:Ljavax/net/ssl/X509KeyManager;

    invoke-interface {v0, p1, p2, p3}, Ljavax/net/ssl/X509KeyManager;->chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->TAG:Ljava/lang/String;

    const-string v1, "X509KeyManager: getCertificateChain"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mDefX509KM:Ljavax/net/ssl/X509KeyManager;

    invoke-interface {v0, p1}, Ljavax/net/ssl/X509KeyManager;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    return-object v0
.end method

.method public getClientAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 2
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->TAG:Ljava/lang/String;

    const-string v1, "X509KeyManager: getClientAliases"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mDefX509KM:Ljavax/net/ssl/X509KeyManager;

    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509KeyManager;->getClientAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "X509KeyManager: getPrivateKey "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mClientKeyStore:Ljava/security/KeyStore;

    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mKeyStorePassword:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v1

    check-cast v1, Ljava/security/PrivateKey;
    :try_end_0
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2

    .line 74
    :goto_0
    return-object v1

    .line 59
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/security/UnrecoverableKeyException;
    invoke-virtual {v0}, Ljava/security/UnrecoverableKeyException;->toString()Ljava/lang/String;

    .line 62
    invoke-virtual {v0}, Ljava/security/UnrecoverableKeyException;->printStackTrace()V

    .line 74
    .end local v0    # "e":Ljava/security/UnrecoverableKeyException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    .line 66
    .local v0, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v0}, Ljava/security/KeyStoreException;->toString()Ljava/lang/String;

    .line 67
    invoke-virtual {v0}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_1

    .line 69
    .end local v0    # "e":Ljava/security/KeyStoreException;
    :catch_2
    move-exception v0

    .line 71
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    .line 72
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1
.end method

.method public getServerAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 2
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->TAG:Ljava/lang/String;

    const-string v1, "X509KeyManager: getServerAliases"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/LocalX509KeyManager;->mDefX509KM:Ljavax/net/ssl/X509KeyManager;

    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509KeyManager;->getServerAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
