.class public final enum Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/BaseInitializationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VERSION_UPGRADE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

.field public static final enum H:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

.field public static final enum J:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

.field public static final enum K:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

.field public static final enum T:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    const-string v1, "J"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->J:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    new-instance v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    const-string v1, "H"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->H:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    new-instance v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    const-string v1, "K"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->K:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    new-instance v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    const-string v1, "T"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->T:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->J:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->H:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->K:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->T:Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->$VALUES:[Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->$VALUES:[Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;

    return-object v0
.end method
