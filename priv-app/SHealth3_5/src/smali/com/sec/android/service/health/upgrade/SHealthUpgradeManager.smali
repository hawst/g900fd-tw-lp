.class public Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "SHealthUpgrade(SHealthLiteUpgradeManager)"


# instance fields
.field private mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

.field mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field private mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

.field private mUpgradeHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_NONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$1;-><init>(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager$2;-><init>(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;)Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mService:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public startUpgrade(Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;Landroid/os/Handler;)V
    .locals 6

    const/16 v5, 0x163

    const/16 v4, 0xff

    const/4 v1, 0x1

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v2, "startUpgrade()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    iput-object p2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_0

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "startUpgrade() : CURRENTLY INSTALLED PACKAGE [Fitness With Gear, SHealth For Gear]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_1

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "startUpgrade() : CURRENTLY INSTALLED PACKAGE [SHealth For Gear]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_2

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v2, "startUpgrade() : CURRENTLY INSTALLED PACKAGE [S-Health For Gear Upgrade Version, S-Health 3.5]"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.upgrade.PackageInstall"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_3

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v2, "startUpgrade() : CURRENTLY INSTALLED PACKAGE [S-Health For Gear Upgrade Version, S-Health 3.5, HealthService 2.x]"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.upgrade.PackageInstall"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_3_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_5

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v2, "startUpgrade() : CURRENTLY INSTALLED PACKAGE [S-Health 3.5]"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isShealthService2xInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_6

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "startUpgrade() : CURRENTLY INSTALLED PACKAGE [S-Health 3.5, Health Service 1.x]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_7

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string v2, "[ILLEGAL STATE] [3XPRECEDENCE] startUpgrade() UPGRADE_STATE_4_2 : "

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.upgrade.PackageInstall"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_8

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string v2, "[ILLEGAL STATE] [3XPRECEDENCE] startUpgrade() UPGRADE_STATE_4_3 : "

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.upgrade.PackageInstall"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_9

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string v1, "[3XPRECEDENCE] [FOTACASE] startUpgrade() UPGRADE_STATE_4_4 : "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mCurrentState:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v0, v2, :cond_c

    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v2, "startUpgrade() : CURRENTLY INSTALLED PACKAGE [S-Health 3.5, Health Service 2.x]"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSleepMonitorInstalled(Landroid/content/Context;)Z

    move-result v2

    if-ne v2, v1, :cond_a

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.upgrade.PackageInstall"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move v0, v1

    :cond_a
    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSensorServiceInstalled(Landroid/content/Context;)Z

    move-result v2

    if-ne v2, v1, :cond_b

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->isSensorServiceEnabled(Landroid/content/Context;)Z

    move-result v2

    if-ne v2, v1, :cond_b

    if-eq v0, v1, :cond_b

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.upgrade.PackageInstall"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_b
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->mUpgradeHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_c
    const-string v0, "SHealthUpgrade(SHealthLiteUpgradeManager)"

    const-string/jumbo v1, "startUpgrade() : ILLEGAL STATE. UPGRADE SCENARIO DOES NOT TREAT THIS SCENARIO."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
