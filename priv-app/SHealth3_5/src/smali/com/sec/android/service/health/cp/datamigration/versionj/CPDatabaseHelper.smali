.class public Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;
.super Lsamsung/database/sqlite/SecSQLiteOpenHelper;


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "secure_sec_health.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static dbWriteInstance:Lsamsung/database/sqlite/SecSQLiteDatabase;

.field private static instance:Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

.field private static mPassword:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->TAG:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->mPassword:[B

    sput-object v1, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->instance:Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

    sput-object v1, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->dbWriteInstance:Lsamsung/database/sqlite/SecSQLiteDatabase;

    return-void

    :array_0
    .array-data 1
        0x41t
        0x6ct
        0x65t
        0x58t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string/jumbo v0, "secure_sec_health.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->instance:Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->instance:Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->instance:Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

    return-object v0
.end method


# virtual methods
.method public getDBInstance()Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->dbWriteInstance:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->instance:Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->mPassword:[B

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->dbWriteInstance:Lsamsung/database/sqlite/SecSQLiteDatabase;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPDatabaseHelper;->dbWriteInstance:Lsamsung/database/sqlite/SecSQLiteDatabase;

    return-object v0
.end method

.method public onCreate(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 0

    return-void
.end method

.method public onUpgrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    .locals 0

    return-void
.end method
