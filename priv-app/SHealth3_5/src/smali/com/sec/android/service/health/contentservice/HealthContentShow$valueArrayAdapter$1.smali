.class Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter$1;->this$1:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 24

    const/4 v1, 0x2

    new-array v5, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v5, v1

    sget v1, Lcom/sec/android/service/health/R$id;->checkBox1:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const/4 v2, 0x0

    const/4 v3, 0x0

    array-length v4, v5

    add-int/lit8 v4, v4, -0x1

    aget-wide v6, v5, v4

    long-to-int v4, v6

    if-gtz v4, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v6, 0x20

    shl-long/2addr v2, v6

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    aget-wide v2, v5, v4

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_1

    const-wide v8, 0x6e514ce3d51c8c9bL    # 2.5014494196966123E223

    xor-long/2addr v2, v8

    :cond_1
    const/16 v8, 0x20

    ushr-long/2addr v2, v8

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    xor-long/2addr v2, v6

    const-wide v6, 0x6e514ce3d51c8c9bL    # 2.5014494196966123E223

    xor-long/2addr v2, v6

    aput-wide v2, v5, v4

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    array-length v4, v5

    add-int/lit8 v4, v4, -0x1

    aget-wide v6, v5, v4

    long-to-int v4, v6

    if-gtz v4, :cond_2

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v4, 0x0

    int-to-long v2, v2

    const/16 v6, 0x20

    shl-long/2addr v2, v6

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    aget-wide v2, v5, v4

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_3

    const-wide v8, 0x6e514ce3d51c8c9bL    # 2.5014494196966123E223

    xor-long/2addr v2, v8

    :cond_3
    const/16 v8, 0x20

    ushr-long/2addr v2, v8

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    xor-long/2addr v2, v6

    const-wide v6, 0x6e514ce3d51c8c9bL    # 2.5014494196966123E223

    xor-long/2addr v2, v6

    aput-wide v2, v5, v4

    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x6

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/16 v12, -0x46

    aput v12, v3, v11

    const/16 v11, -0x31

    aput v11, v3, v10

    const/16 v10, -0x33

    aput v10, v3, v9

    const/16 v9, -0x32ba

    aput v9, v3, v8

    const/16 v8, -0x5e

    aput v8, v3, v4

    const/16 v4, -0x3ba7

    aput v4, v3, v2

    const/4 v2, 0x6

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/16 v13, -0x66

    aput v13, v2, v12

    const/16 v12, -0x1c

    aput v12, v2, v11

    const/16 v11, -0x13

    aput v11, v2, v10

    const/16 v10, -0x32cb

    aput v10, v2, v9

    const/16 v9, -0x33

    aput v9, v2, v8

    const/16 v8, -0x3bd7

    aput v8, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v8, v2

    if-lt v4, v8, :cond_4

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v8, v2

    if-lt v4, v8, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x1323

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x1313

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v8, v2, v4

    aget v9, v3, v4

    xor-int/2addr v8, v9

    aput v8, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    aget v8, v3, v4

    int-to-char v8, v8

    aput-char v8, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-eqz v7, :cond_9

    const-wide v7, 0x6e514ce3d51c8c9bL    # 2.5014494196966123E223

    xor-long/2addr v2, v7

    :cond_9
    const/16 v7, 0x20

    shl-long/2addr v2, v7

    const/16 v7, 0x20

    shr-long/2addr v2, v7

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, -0x2b

    aput v22, v3, v21

    const/16 v21, -0x21

    aput v21, v3, v20

    const/16 v20, -0x23

    aput v20, v3, v19

    const/16 v19, 0x63f

    aput v19, v3, v18

    const/16 v18, -0x2f9d

    aput v18, v3, v17

    const/16 v17, -0x45

    aput v17, v3, v16

    const/16 v16, -0x2fe6

    aput v16, v3, v15

    const/16 v15, -0x4b

    aput v15, v3, v14

    const/16 v14, -0x2a

    aput v14, v3, v13

    const/16 v13, -0x32

    aput v13, v3, v12

    const/16 v12, 0x5f73

    aput v12, v3, v11

    const/16 v11, -0x76ca

    aput v11, v3, v10

    const/16 v10, -0x59

    aput v10, v3, v9

    const/4 v9, -0x6

    aput v9, v3, v8

    const/16 v8, 0x6446

    aput v8, v3, v4

    const/16 v4, -0xbc

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, -0xb

    aput v23, v2, v22

    const/16 v22, -0xa

    aput v22, v2, v21

    const/16 v21, -0xb

    aput v21, v2, v20

    const/16 v20, 0x65b

    aput v20, v2, v19

    const/16 v19, -0x2ffa

    aput v19, v2, v18

    const/16 v18, -0x30

    aput v18, v2, v17

    const/16 v17, -0x2f87

    aput v17, v2, v16

    const/16 v16, -0x30

    aput v16, v2, v15

    const/16 v15, -0x42

    aput v15, v2, v14

    const/16 v14, -0x73

    aput v14, v2, v13

    const/16 v13, 0x5f00

    aput v13, v2, v12

    const/16 v12, -0x76a1

    aput v12, v2, v11

    const/16 v11, -0x77

    aput v11, v2, v10

    const/16 v10, -0x68

    aput v10, v2, v9

    const/16 v9, 0x6425

    aput v9, v2, v8

    const/16 v8, -0x9c

    aput v8, v2, v4

    const/4 v4, 0x0

    :goto_4
    array-length v8, v2

    if-lt v4, v8, :cond_a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5
    array-length v8, v2

    if-lt v4, v8, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter$1;->this$1:Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->itemChecked:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;->access$000(Lcom/sec/android/service/health/contentservice/HealthContentShow$valueArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x7af9

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x7ac9

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_a
    aget v8, v2, v4

    aget v9, v3, v4

    xor-int/2addr v8, v9

    aput v8, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_b
    aget v8, v3, v4

    int-to-char v8, v8

    aput-char v8, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_e
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-eqz v5, :cond_f

    const-wide v5, 0x6e514ce3d51c8c9bL    # 2.5014494196966123E223

    xor-long/2addr v2, v5

    :cond_f
    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    shr-long/2addr v2, v5

    long-to-int v2, v2

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_8
    return-void

    :catch_0
    move-exception v1

    goto :goto_8
.end method
