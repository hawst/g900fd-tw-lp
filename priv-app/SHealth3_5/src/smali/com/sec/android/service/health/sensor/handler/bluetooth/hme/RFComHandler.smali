.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;
.implements Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;
    }
.end annotation


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

.field private mContext:Landroid/content/Context;

.field private mData:Ljava/lang/Object;

.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field private mInStream:Ljava/io/InputStream;

.field private mOutStream:Ljava/io/OutputStream;

.field private mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private mRFComProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

.field private mSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mSocket:Landroid/bluetooth/BluetoothSocket;

.field private mTAG:Ljava/lang/String;

.field private runCommunicationThread:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "HME"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSocket:Landroid/bluetooth/BluetoothSocket;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mData:Ljava/lang/Object;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->runCommunicationThread:Z

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mRFComProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->runCommunicationThread:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->shutdownCommunicationThread()V

    return-void
.end method

.method private shutdownCommunicationThread()V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandlershutdownCommunicationThreadd"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->interrupt()V

    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->runCommunicationThread:Z

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;

    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startCommunicationThread()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler startCommunicationThread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->interrupt()V

    :cond_0
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->runCommunicationThread:Z

    return-void
.end method


# virtual methods
.method public connected(Ljava/lang/String;Landroid/bluetooth/BluetoothSocket;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler connected Socket"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler connected Socket NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSocket:Landroid/bluetooth/BluetoothSocket;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mData:Ljava/lang/Object;

    invoke-interface {v0, v1, p0, v2, v3}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I

    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-wide/16 v0, 0x5dc

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler connected Starting Communication Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->startCommunicationThread()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler getProtocol NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v2, "RFComHandler connected Opening inputStream/Output Stream Error"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v4

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v2, "RFComHandler connected Error While Thread Is Sleeping"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v4

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public deinitialize()V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->stopReceivingData()I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->shutdownCommunicationThread()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->deinitialize()V

    :cond_0
    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mRFComProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 1

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mData:Ljava/lang/Object;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    return v0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler onDataReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onDataStarted(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler onDataStarted"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    return-void
.end method

.method public onDataStopped(II)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler onDataStopped"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler onDataStopped Interuption of the Commmunication Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->interrupt()V

    :cond_0
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mCommunicationThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mSocket:Landroid/bluetooth/BluetoothSocket;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyStop()I

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_2
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    return-void
.end method

.method public onStateChanged(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler onStateChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onStateChanged(I)V

    :cond_0
    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->addDeviceForConnecting(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x3e8

    goto :goto_0
.end method

.method public sendRawData([B)I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RFComHandler sendRawData "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v0, v2, v0

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "() Exception :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->shutdownCommunicationThread()V

    const/4 v0, -0x1

    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->onDataStopped(II)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mRFComProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler startReceivingData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->addDeviceForConnecting(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;)V

    const/4 v0, 0x0

    return v0
.end method

.method public stopReceivingData()I
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;

    const-string v1, "RFComHandler stopReceivingData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->removeDeviceFromConnecting(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->shutdownCommunicationThread()V

    const/4 v0, 0x0

    return v0
.end method
