.class public Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/data/MeasurementData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataSet"
.end annotation


# instance fields
.field private mSpecialTypeValue:I

.field private mType:I

.field private mUnit:Ljava/lang/String;

.field private mValue:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mUnit:Ljava/lang/String;

    iput v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mSpecialTypeValue:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private checkSpecialType(IF)I
    .locals 7

    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    float-to-int v6, p2

    const/4 v5, 0x0

    if-gez v6, :cond_0

    mul-int/lit8 v6, v6, -0x1

    :cond_0
    packed-switch p1, :pswitch_data_0

    packed-switch v6, :pswitch_data_1

    :goto_0
    move v0, v5

    :goto_1
    :pswitch_0
    return v0

    :pswitch_1
    packed-switch v6, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    move v0, v2

    goto :goto_1

    :pswitch_3
    move v0, v1

    goto :goto_1

    :pswitch_4
    move v0, v3

    goto :goto_1

    :pswitch_5
    move v0, v4

    goto :goto_1

    :pswitch_6
    move v0, v1

    goto :goto_1

    :pswitch_7
    move v0, v2

    goto :goto_1

    :pswitch_8
    move v0, v3

    goto :goto_1

    :pswitch_9
    move v0, v4

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0xe140
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7fe
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_9
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7ffffe
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private getUnit(I)Ljava/lang/String;
    .locals 1

    const-string v0, ""

    sparse-switch p1, :sswitch_data_0

    const-string v0, "-"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "-"

    goto :goto_0

    :sswitch_1
    const-string v0, "%"

    goto :goto_0

    :sswitch_2
    const-string v0, "degree"

    goto :goto_0

    :sswitch_3
    const-string v0, "m"

    goto :goto_0

    :sswitch_4
    const-string v0, "cm"

    goto :goto_0

    :sswitch_5
    const-string v0, "ft"

    goto :goto_0

    :sswitch_6
    const-string v0, "in"

    goto :goto_0

    :sswitch_7
    const-string/jumbo v0, "mL"

    goto :goto_0

    :sswitch_8
    const-string v0, "g"

    goto :goto_0

    :sswitch_9
    const-string v0, "kg"

    goto :goto_0

    :sswitch_a
    const-string/jumbo v0, "mg"

    goto :goto_0

    :sswitch_b
    const-string v0, "lb"

    goto :goto_0

    :sswitch_c
    const-string v0, "kg/m2"

    goto :goto_0

    :sswitch_d
    const-string/jumbo v0, "mg/dL"

    goto :goto_0

    :sswitch_e
    const-string/jumbo v0, "min"

    goto :goto_0

    :sswitch_f
    const-string v0, "h"

    goto :goto_0

    :sswitch_10
    const-string v0, "d"

    goto :goto_0

    :sswitch_11
    const-string/jumbo v0, "y"

    goto :goto_0

    :sswitch_12
    const-string v0, "bpm"

    goto :goto_0

    :sswitch_13
    const-string/jumbo v0, "resp/min"

    goto :goto_0

    :sswitch_14
    const-string v0, "kPa"

    goto :goto_0

    :sswitch_15
    const-string/jumbo v0, "mmHg"

    goto :goto_0

    :sswitch_16
    const-string v0, "J"

    goto :goto_0

    :sswitch_17
    const-string v0, "W"

    goto :goto_0

    :sswitch_18
    const-string/jumbo v0, "mmol/L"

    goto :goto_0

    :sswitch_19
    const-string/jumbo v0, "oC"

    goto :goto_0

    :sswitch_1a
    const-string v0, "m/min"

    goto :goto_0

    :sswitch_1b
    const-string/jumbo v0, "steps"

    goto :goto_0

    :sswitch_1c
    const-string v0, "ft/min"

    goto :goto_0

    :sswitch_1d
    const-string v0, "inch/min"

    goto :goto_0

    :sswitch_1e
    const-string/jumbo v0, "step/min"

    goto :goto_0

    :sswitch_1f
    const-string v0, "calories "

    goto :goto_0

    :sswitch_20
    const-string/jumbo v0, "rpm"

    goto :goto_0

    :sswitch_21
    const-string/jumbo v0, "oF"

    goto :goto_0

    :sswitch_22
    const-string/jumbo v0, "strides"

    goto :goto_0

    :sswitch_23
    const-string/jumbo v0, "s"

    goto :goto_0

    :sswitch_24
    const-string v0, "kcal"

    goto :goto_0

    :sswitch_25
    const-string v0, "beats"

    goto/16 :goto_0

    :sswitch_26
    const-string v0, "METs"

    goto/16 :goto_0

    :sswitch_27
    const-string v0, "kcal/h"

    goto/16 :goto_0

    :sswitch_28
    const-string/jumbo v0, "strokes/min"

    goto/16 :goto_0

    :sswitch_29
    const-string v0, "kcal/day"

    goto/16 :goto_0

    :sswitch_2a
    const-string v0, "V"

    goto/16 :goto_0

    :sswitch_2b
    const-string v0, "cycle"

    goto/16 :goto_0

    :sswitch_2c
    const-string v0, "m/s"

    goto/16 :goto_0

    :sswitch_2d
    const-string/jumbo v0, "strides/min"

    goto/16 :goto_0

    :sswitch_2e
    const-string v0, "-"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2e
        0x200 -> :sswitch_0
        0x220 -> :sswitch_1
        0x2e0 -> :sswitch_2
        0x500 -> :sswitch_3
        0x511 -> :sswitch_4
        0x540 -> :sswitch_5
        0x560 -> :sswitch_6
        0x652 -> :sswitch_7
        0x6c0 -> :sswitch_8
        0x6c3 -> :sswitch_9
        0x6d2 -> :sswitch_a
        0x6e0 -> :sswitch_b
        0x7a0 -> :sswitch_c
        0x852 -> :sswitch_d
        0x8a0 -> :sswitch_e
        0x8c0 -> :sswitch_f
        0x8e0 -> :sswitch_10
        0x940 -> :sswitch_11
        0xaa0 -> :sswitch_12
        0xae0 -> :sswitch_13
        0xf03 -> :sswitch_14
        0xf20 -> :sswitch_15
        0xf80 -> :sswitch_16
        0xfc0 -> :sswitch_17
        0x1140 -> :sswitch_21
        0x1195 -> :sswitch_22
        0x1196 -> :sswitch_23
        0x1197 -> :sswitch_24
        0x119a -> :sswitch_25
        0x119b -> :sswitch_26
        0x119c -> :sswitch_27
        0x119d -> :sswitch_28
        0x119e -> :sswitch_2d
        0x119f -> :sswitch_29
        0x11a0 -> :sswitch_2a
        0x11a1 -> :sswitch_2b
        0x11a2 -> :sswitch_2c
        0x1272 -> :sswitch_18
        0x17a0 -> :sswitch_19
        0x19a0 -> :sswitch_1a
        0x1a00 -> :sswitch_1b
        0x1a20 -> :sswitch_1c
        0x1a40 -> :sswitch_1d
        0x1a60 -> :sswitch_1e
        0x1a80 -> :sswitch_1f
        0x1aa0 -> :sswitch_20
    .end sparse-switch
.end method

.method private set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    invoke-static {p2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mUnit:Ljava/lang/String;

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    iget v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->checkSpecialType(IF)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mSpecialTypeValue:I

    return-void
.end method


# virtual methods
.method public getSpecialTypeValue()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mSpecialTypeValue:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    return v0
.end method

.method public getUnit()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    return v0
.end method

.method set(IFI)V
    .locals 2

    iput p1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    iput p2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    invoke-direct {p0, p3}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->getUnit(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mUnit:Ljava/lang/String;

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    iget v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->checkSpecialType(IF)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mSpecialTypeValue:I

    return-void
.end method

.method set(IFLjava/lang/String;)V
    .locals 2

    iput p1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    iput p2, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mUnit:Ljava/lang/String;

    iget v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mType:I

    iget v1, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mValue:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->checkSpecialType(IF)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/data/MeasurementData$DataSet;->mSpecialTypeValue:I

    return-void
.end method
