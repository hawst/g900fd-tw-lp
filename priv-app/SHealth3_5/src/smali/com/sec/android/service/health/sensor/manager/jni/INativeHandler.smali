.class public interface abstract Lcom/sec/android/service/health/sensor/manager/jni/INativeHandler;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onFinishSession()V
.end method

.method public abstract onHealthServiceError(ILjava/lang/String;)V
.end method

.method public abstract onMeasuredDataReceived(Lcom/sec/android/service/health/sensor/data/MeasurementData;)V
.end method

.method public abstract onSensorInfoReceived(Lcom/sec/android/service/health/sensor/data/SensorInfo;)V
.end method

.method public abstract onStartSession()V
.end method

.method public abstract sendDataToSensor([B)V
.end method
