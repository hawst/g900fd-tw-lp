.class public Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;
.super Ljava/lang/Object;


# instance fields
.field public accId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public awakeCount:Ljava/lang/String;

.field public bedtime:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceCreateDate:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyDate:Ljava/lang/String;

.field public deviceSleepPKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public duration:Ljava/lang/String;

.field public efficiency:Ljava/lang/Double;

.field public hdid:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public movement:Ljava/lang/String;

.field public noise:Ljava/lang/String;

.field public quality:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public risetime:Ljava/lang/String;

.field public sleepDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/SleepItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/SleepInfo;->sleepDataList:Ljava/util/ArrayList;

    return-void
.end method
