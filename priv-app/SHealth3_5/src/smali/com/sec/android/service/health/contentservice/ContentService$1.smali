.class Lcom/sec/android/service/health/contentservice/ContentService$1;
.super Lcom/samsung/android/sdk/health/content/_ContentService$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/contentservice/ContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/contentservice/ContentService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/contentservice/ContentService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/_ContentService$Stub;-><init>()V

    return-void
.end method

.method private getImportantColumns(Landroid/net/Uri;Landroid/content/ContentValues;)Ljava/util/ArrayList;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/content/ContentValues;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/contentservice/_ContentValueData;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x2

    new-array v5, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v5, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    if-gtz v3, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    ushr-long v6, v1, v4

    aget-wide v1, v5, v3

    const-wide/16 v8, 0x0

    cmp-long v4, v1, v8

    if-eqz v4, :cond_1

    const-wide v8, 0xc8b907bb595d61L

    xor-long/2addr v1, v8

    :cond_1
    const/16 v4, 0x20

    ushr-long/2addr v1, v4

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    xor-long/2addr v1, v6

    const-wide v6, 0xc8b907bb595d61L

    xor-long/2addr v1, v6

    aput-wide v1, v5, v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    # getter for: Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/contentservice/ContentService;->access$200()Ljava/util/HashMap;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    # getter for: Lcom/sec/android/service/health/contentservice/ContentService;->mWatchDogColumns:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/contentservice/ContentService;->access$200()Ljava/util/HashMap;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    new-instance v10, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v10}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    iput-object v8, v10, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_6

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x6929

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x6919

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v1

    new-instance v5, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v5}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, -0x3

    aput v11, v2, v10

    const/16 v10, -0x58

    aput v10, v2, v9

    const/16 v9, 0x5201

    aput v9, v2, v8

    const/16 v8, -0x21c2

    aput v8, v2, v7

    const/16 v7, -0x41

    aput v7, v2, v6

    const/16 v6, 0x2a72

    aput v6, v2, v3

    const/16 v3, 0x1375

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/16 v12, -0x5e

    aput v12, v1, v11

    const/16 v11, -0x9

    aput v11, v1, v10

    const/16 v10, 0x526d

    aput v10, v1, v9

    const/16 v9, -0x21ae

    aput v9, v1, v8

    const/16 v8, -0x22

    aput v8, v1, v7

    const/16 v7, 0x2a2d

    aput v7, v1, v6

    const/16 v6, 0x132a

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v4

    :goto_5
    return-object v1

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_6
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v11, 0x0

    cmp-long v3, v1, v11

    if-eqz v3, :cond_7

    const-wide v11, 0xc8b907bb595d61L

    xor-long/2addr v1, v11

    :cond_7
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    iput v1, v10, Lcom/sec/android/service/health/contentservice/_ContentValueData;->index:I

    invoke-interface {v6, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v11, 0x2

    const/4 v12, 0x3

    const/4 v13, 0x4

    const/4 v14, 0x5

    const/4 v15, 0x6

    const/16 v16, 0x7

    const/16 v17, 0x8

    const/16 v18, 0x9

    const/16 v19, -0x5

    aput v19, v2, v18

    const/16 v18, -0x7e

    aput v18, v2, v17

    const/16 v17, 0x2568

    aput v17, v2, v16

    const/16 v16, -0x47ad

    aput v16, v2, v15

    const/16 v15, -0x36

    aput v15, v2, v14

    const/16 v14, -0x43a8

    aput v14, v2, v13

    const/16 v13, -0x11

    aput v13, v2, v12

    const/16 v12, 0x5a3c

    aput v12, v2, v11

    const/16 v11, -0x74cd

    aput v11, v2, v3

    const/16 v3, -0x19

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x2

    const/4 v13, 0x3

    const/4 v14, 0x4

    const/4 v15, 0x5

    const/16 v16, 0x6

    const/16 v17, 0x7

    const/16 v18, 0x8

    const/16 v19, 0x9

    const/16 v20, -0x62

    aput v20, v1, v19

    const/16 v19, -0x1f

    aput v19, v1, v18

    const/16 v18, 0x2501

    aput v18, v1, v17

    const/16 v17, -0x47db

    aput v17, v1, v16

    const/16 v16, -0x48

    aput v16, v1, v15

    const/16 v15, -0x43c3

    aput v15, v1, v14

    const/16 v14, -0x44

    aput v14, v1, v13

    const/16 v13, 0x5a5e

    aput v13, v1, v12

    const/16 v12, -0x74a6

    aput v12, v1, v11

    const/16 v11, -0x75

    aput v11, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v11, v1

    if-lt v3, v11, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v11, v1

    if-lt v3, v11, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xd

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v13, 0x2

    const/4 v14, 0x3

    const/4 v15, 0x4

    const/16 v16, 0x5

    const/16 v17, 0x6

    const/16 v18, 0x7

    const/16 v19, 0x8

    const/16 v20, 0x9

    const/16 v21, 0xa

    const/16 v22, 0xb

    const/16 v23, 0xc

    const/16 v24, -0x59e

    aput v24, v2, v23

    const/16 v23, -0x6c

    aput v23, v2, v22

    const/16 v22, -0x28

    aput v22, v2, v21

    const/16 v21, 0x7a20

    aput v21, v2, v20

    const/16 v20, 0x7016

    aput v20, v2, v19

    const/16 v19, -0x10e1

    aput v19, v2, v18

    const/16 v18, -0x74

    aput v18, v2, v17

    const/16 v17, -0x20

    aput v17, v2, v16

    const/16 v16, 0x2976

    aput v16, v2, v15

    const/16 v15, 0x154c

    aput v15, v2, v14

    const/16 v14, -0x7d8f

    aput v14, v2, v13

    const/16 v13, -0x1a

    aput v13, v2, v3

    const/16 v3, 0x5759

    aput v3, v2, v1

    const/16 v1, 0xd

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x2

    const/4 v15, 0x3

    const/16 v16, 0x4

    const/16 v17, 0x5

    const/16 v18, 0x6

    const/16 v19, 0x7

    const/16 v20, 0x8

    const/16 v21, 0x9

    const/16 v22, 0xa

    const/16 v23, 0xb

    const/16 v24, 0xc

    const/16 v25, -0x5a1

    aput v25, v1, v24

    const/16 v24, -0x6

    aput v24, v1, v23

    const/16 v23, -0x4b

    aput v23, v1, v22

    const/16 v22, 0x7a55

    aput v22, v1, v21

    const/16 v21, 0x707a

    aput v21, v1, v20

    const/16 v20, -0x1090

    aput v20, v1, v19

    const/16 v19, -0x11

    aput v19, v1, v18

    const/16 v18, -0x40

    aput v18, v1, v17

    const/16 v17, 0x2912

    aput v17, v1, v16

    const/16 v16, 0x1529

    aput v16, v1, v15

    const/16 v15, -0x7deb

    aput v15, v1, v14

    const/16 v14, -0x7e

    aput v14, v1, v13

    const/16 v13, 0x5738

    aput v13, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v13, v1

    if-lt v3, v13, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v13, v1

    if-lt v3, v13, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v12, 0x2

    const/4 v13, 0x3

    const/4 v14, 0x4

    const/4 v15, 0x5

    const/16 v16, 0x6

    const/16 v17, -0x77

    aput v17, v2, v16

    const/16 v16, -0x79ce

    aput v16, v2, v15

    const/16 v15, -0xd

    aput v15, v2, v14

    const/16 v14, -0x22

    aput v14, v2, v13

    const/16 v13, -0x4b

    aput v13, v2, v12

    const/16 v12, -0x2cf6

    aput v12, v2, v3

    const/4 v3, -0x1

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x2

    const/4 v14, 0x3

    const/4 v15, 0x4

    const/16 v16, 0x5

    const/16 v17, 0x6

    const/16 v18, -0x4c

    aput v18, v1, v17

    const/16 v17, -0x79a9

    aput v17, v1, v16

    const/16 v16, -0x7a

    aput v16, v1, v15

    const/16 v15, -0x4e

    aput v15, v1, v14

    const/16 v14, -0x2c

    aput v14, v1, v13

    const/16 v13, -0x2c84

    aput v13, v1, v12

    const/16 v12, -0x2d

    aput v12, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v12, v1

    if-lt v3, v12, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v12, v1

    if-lt v3, v12, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    const/4 v3, 0x0

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_f

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    aget v11, v1, v3

    aget v12, v2, v3

    xor-int/2addr v11, v12

    aput v11, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_a
    aget v11, v2, v3

    int-to-char v11, v11

    aput-char v11, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_b
    aget v13, v1, v3

    aget v14, v2, v3

    xor-int/2addr v13, v14

    aput v13, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_c
    aget v13, v2, v3

    int-to-char v13, v13

    aput-char v13, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_d
    aget v12, v1, v3

    aget v13, v2, v3

    xor-int/2addr v12, v13

    aput v12, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_e
    aget v12, v2, v3

    int-to-char v12, v12

    aput-char v12, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_f
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_10

    const-wide v8, 0xc8b907bb595d61L

    xor-long/2addr v1, v8

    :cond_10
    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    shr-long/2addr v1, v8

    long-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v8, v5, v2

    long-to-int v2, v8

    if-gtz v2, :cond_11

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_11
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    ushr-long v8, v1, v8

    aget-wide v1, v5, v3

    const-wide/16 v10, 0x0

    cmp-long v10, v1, v10

    if-eqz v10, :cond_12

    const-wide v10, 0xc8b907bb595d61L

    xor-long/2addr v1, v10

    :cond_12
    const/16 v10, 0x20

    ushr-long/2addr v1, v10

    const/16 v10, 0x20

    shl-long/2addr v1, v10

    xor-long/2addr v1, v8

    const-wide v8, 0xc8b907bb595d61L

    xor-long/2addr v1, v8

    aput-wide v1, v5, v3

    goto/16 :goto_0

    :cond_13
    new-instance v5, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v5}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, 0x5233

    aput v11, v2, v10

    const/16 v10, 0x170d

    aput v10, v2, v9

    const/16 v9, -0x4c85

    aput v9, v2, v8

    const/16 v8, -0x21

    aput v8, v2, v7

    const/16 v7, 0x6d7d

    aput v7, v2, v6

    const/16 v6, -0x7ce

    aput v6, v2, v3

    const/16 v3, -0x59

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/16 v12, 0x526c

    aput v12, v1, v11

    const/16 v11, 0x1752

    aput v11, v1, v10

    const/16 v10, -0x4ce9

    aput v10, v1, v9

    const/16 v9, -0x4d

    aput v9, v1, v8

    const/16 v8, 0x6d1c

    aput v8, v1, v7

    const/16 v7, -0x793

    aput v7, v1, v6

    const/4 v6, -0x8

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v6, v1

    if-lt v3, v6, :cond_16

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v6, v1

    if-lt v3, v6, :cond_17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_14
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_15

    :goto_e
    new-instance v5, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v5}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, -0x3096

    aput v10, v2, v9

    const/16 v9, -0x5d

    aput v9, v2, v8

    const/16 v8, -0x25

    aput v8, v2, v7

    const/16 v7, -0x6ff3

    aput v7, v2, v6

    const/16 v6, -0x31

    aput v6, v2, v3

    const/16 v3, -0x6a

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/16 v12, -0x30

    aput v12, v1, v11

    const/16 v11, -0x30cb

    aput v11, v1, v10

    const/16 v10, -0x31

    aput v10, v1, v9

    const/16 v9, -0x49

    aput v9, v1, v8

    const/16 v8, -0x6f94

    aput v8, v1, v7

    const/16 v7, -0x70

    aput v7, v1, v6

    const/16 v6, -0x37

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_f
    array-length v6, v1

    if-lt v3, v6, :cond_18

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_10
    array-length v6, v1

    if-lt v3, v6, :cond_19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_15
    move-object v1, v4

    goto/16 :goto_5

    :cond_16
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_17
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_18
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_19
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :catch_1
    move-exception v1

    goto/16 :goto_e
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/contentservice/ContentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/contentservice/ContentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public showConfirmation(JILandroid/net/Uri;Landroid/content/ContentValues;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x2

    new-array v6, v3, [J

    const/4 v3, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v6, v3

    const/4 v3, 0x0

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v6, v4

    long-to-int v4, v4

    if-gtz v4, :cond_0

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v5, Ljava/lang/Integer;

    invoke-direct {v5, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/4 v5, 0x0

    move/from16 v0, p3

    int-to-long v3, v0

    const/16 v7, 0x20

    shl-long/2addr v3, v7

    const/16 v7, 0x20

    ushr-long v7, v3, v7

    aget-wide v3, v6, v5

    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-eqz v9, :cond_1

    const-wide v9, -0x404d1582dc7bf3dcL    # -0.07389051549039088

    xor-long/2addr v3, v9

    :cond_1
    const/16 v9, 0x20

    ushr-long/2addr v3, v9

    const/16 v9, 0x20

    shl-long/2addr v3, v9

    xor-long/2addr v3, v7

    const-wide v7, -0x404d1582dc7bf3dcL    # -0.07389051549039088

    xor-long/2addr v3, v7

    aput-wide v3, v6, v5

    new-instance v7, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    const-class v4, Lcom/sec/android/service/health/contentservice/HealthContentShow;

    invoke-direct {v7, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v7, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v6, v3

    long-to-int v3, v3

    if-gtz v3, :cond_4

    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v3, 0x1

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/16 v5, -0x61

    aput v5, v4, v3

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/16 v7, -0x51

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_0
    array-length v7, v3

    if-lt v5, v7, :cond_2

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_1
    array-length v7, v3

    if-lt v5, v7, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_2
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    aget-wide v3, v6, v3

    const-wide/16 v8, 0x0

    cmp-long v5, v3, v8

    if-eqz v5, :cond_5

    const-wide v8, -0x404d1582dc7bf3dcL    # -0.07389051549039088

    xor-long/2addr v3, v8

    :cond_5
    const/16 v5, 0x20

    shl-long/2addr v3, v5

    const/16 v5, 0x20

    shr-long/2addr v3, v5

    long-to-int v3, v3

    const/16 v4, 0x3ec

    if-eq v3, v4, :cond_6

    const/16 v3, 0xc

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, -0x44c9

    aput v18, v4, v17

    const/16 v17, -0x31

    aput v17, v4, v16

    const/16 v16, 0xb4e

    aput v16, v4, v15

    const/16 v15, 0x166f

    aput v15, v4, v14

    const/16 v14, 0x3449

    aput v14, v4, v13

    const/16 v13, 0x3440

    aput v13, v4, v12

    const/16 v12, -0x7aa6

    aput v12, v4, v11

    const/16 v11, -0x20

    aput v11, v4, v10

    const/16 v10, -0x76de

    aput v10, v4, v9

    const/16 v9, -0x19

    aput v9, v4, v8

    const/16 v8, 0x5f44

    aput v8, v4, v5

    const/16 v5, -0x54c4

    aput v5, v4, v3

    const/16 v3, 0xc

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, -0x44aa

    aput v19, v3, v18

    const/16 v18, -0x45

    aput v18, v3, v17

    const/16 v17, 0xb2f

    aput v17, v3, v16

    const/16 v16, 0x160b

    aput v16, v3, v15

    const/16 v15, 0x3416

    aput v15, v3, v14

    const/16 v14, 0x3434

    aput v14, v3, v13

    const/16 v13, -0x7acc

    aput v13, v3, v12

    const/16 v12, -0x7b

    aput v12, v3, v11

    const/16 v11, -0x76aa

    aput v11, v3, v10

    const/16 v10, -0x77

    aput v10, v3, v9

    const/16 v9, 0x5f2b

    aput v9, v3, v8

    const/16 v8, -0x54a1

    aput v8, v3, v5

    const/4 v5, 0x0

    :goto_2
    array-length v8, v3

    if-lt v5, v8, :cond_7

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_3
    array-length v8, v3

    if-lt v5, v8, :cond_8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/contentservice/ContentService$1;->getImportantColumns(Landroid/net/Uri;Landroid/content/ContentValues;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_6
    const/16 v3, 0xb

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, -0x6d9c

    aput v17, v4, v16

    const/16 v16, -0x1

    aput v16, v4, v15

    const/16 v15, -0x4e

    aput v15, v4, v14

    const/16 v14, -0x3f

    aput v14, v4, v13

    const/16 v13, -0x2a

    aput v13, v4, v12

    const/16 v12, 0x2c1c

    aput v12, v4, v11

    const/16 v11, -0x66b7

    aput v11, v4, v10

    const/16 v10, -0x14

    aput v10, v4, v9

    const/16 v9, -0x1bfc

    aput v9, v4, v8

    const/16 v8, -0x7f

    aput v8, v4, v5

    const/16 v5, -0x33

    aput v5, v4, v3

    const/16 v3, 0xb

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, -0x6dff

    aput v18, v3, v17

    const/16 v17, -0x6e

    aput v17, v3, v16

    const/16 v16, -0x25

    aput v16, v3, v15

    const/16 v15, -0x6b

    aput v15, v3, v14

    const/16 v14, -0x5e

    aput v14, v3, v13

    const/16 v13, 0x2c6f

    aput v13, v3, v12

    const/16 v12, -0x66d4

    aput v12, v3, v11

    const/16 v11, -0x67

    aput v11, v3, v10

    const/16 v10, -0x1b8b

    aput v10, v3, v9

    const/16 v9, -0x1c

    aput v9, v3, v8

    const/16 v8, -0x41

    aput v8, v3, v5

    const/4 v5, 0x0

    :goto_4
    array-length v8, v3

    if-lt v5, v8, :cond_9

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_5
    array-length v8, v3

    if-lt v5, v8, :cond_a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v7, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v3, 0x6

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/16 v12, -0x4a

    aput v12, v4, v11

    const/16 v11, -0x54

    aput v11, v4, v10

    const/16 v10, 0x1f59

    aput v10, v4, v9

    const/16 v9, 0xc6b

    aput v9, v4, v8

    const/16 v8, 0x456f

    aput v8, v4, v5

    const/16 v5, -0x5dc

    aput v5, v4, v3

    const/4 v3, 0x6

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/16 v13, -0x28

    aput v13, v3, v12

    const/16 v12, -0x3d

    aput v12, v3, v11

    const/16 v11, 0x1f30

    aput v11, v3, v10

    const/16 v10, 0xc1f

    aput v10, v3, v9

    const/16 v9, 0x450c

    aput v9, v3, v8

    const/16 v8, -0x5bb

    aput v8, v3, v5

    const/4 v5, 0x0

    :goto_6
    array-length v8, v3

    if-lt v5, v8, :cond_b

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_7
    array-length v8, v3

    if-lt v5, v8, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v6, v3

    long-to-int v3, v3

    if-gtz v3, :cond_f

    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v3, 0x1

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/16 v5, -0x23

    aput v5, v4, v3

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/16 v7, -0x13

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_8
    array-length v7, v3

    if-lt v5, v7, :cond_d

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_9
    array-length v7, v3

    if-lt v5, v7, :cond_e

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_7
    aget v8, v3, v5

    aget v9, v4, v5

    xor-int/2addr v8, v9

    aput v8, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    :cond_8
    aget v8, v4, v5

    int-to-char v8, v8

    aput-char v8, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    :cond_9
    aget v8, v3, v5

    aget v9, v4, v5

    xor-int/2addr v8, v9

    aput v8, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    :cond_a
    aget v8, v4, v5

    int-to-char v8, v8

    aput-char v8, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5

    :cond_b
    aget v8, v3, v5

    aget v9, v4, v5

    xor-int/2addr v8, v9

    aput v8, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_6

    :cond_c
    aget v8, v4, v5

    int-to-char v8, v8

    aput-char v8, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_7

    :cond_d
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    :cond_e
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    :cond_f
    const/4 v3, 0x0

    aget-wide v3, v6, v3

    const-wide/16 v8, 0x0

    cmp-long v6, v3, v8

    if-eqz v6, :cond_10

    const-wide v8, -0x404d1582dc7bf3dcL    # -0.07389051549039088

    xor-long/2addr v3, v8

    :cond_10
    const/16 v6, 0x20

    shl-long/2addr v3, v6

    const/16 v6, 0x20

    shr-long/2addr v3, v6

    long-to-int v3, v3

    invoke-virtual {v7, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    invoke-virtual {v3, v7}, Lcom/sec/android/service/health/contentservice/ContentService;->startActivity(Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    move-object/from16 v0, p6

    # setter for: Lcom/sec/android/service/health/contentservice/ContentService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    invoke-static {v3, v0}, Lcom/sec/android/service/health/contentservice/ContentService;->access$002(Lcom/sec/android/service/health/contentservice/ContentService;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/contentservice/ContentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public updateConfirmation(J[Z[Ljava/lang/String;)V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    # getter for: Lcom/sec/android/service/health/contentservice/ContentService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    invoke-static {v2}, Lcom/sec/android/service/health/contentservice/ContentService;->access$000(Lcom/sec/android/service/health/contentservice/ContentService;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const/16 v2, 0xb

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, -0x6e

    aput v16, v3, v15

    const/16 v15, -0x1bef

    aput v15, v3, v14

    const/16 v14, -0x73

    aput v14, v3, v13

    const/16 v13, -0x5c88

    aput v13, v3, v12

    const/16 v12, -0x29

    aput v12, v3, v11

    const/16 v11, -0x7b

    aput v11, v3, v10

    const/16 v10, -0x70

    aput v10, v3, v9

    const/16 v9, -0x5e

    aput v9, v3, v8

    const/16 v8, -0x2fd7

    aput v8, v3, v7

    const/16 v7, -0x4b

    aput v7, v3, v4

    const/16 v4, 0x6d24

    aput v4, v3, v2

    const/16 v2, 0xb

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, -0x9

    aput v17, v2, v16

    const/16 v16, -0x1b84

    aput v16, v2, v15

    const/16 v15, -0x1c

    aput v15, v2, v14

    const/16 v14, -0x5cd4

    aput v14, v2, v13

    const/16 v13, -0x5d

    aput v13, v2, v12

    const/16 v12, -0xa

    aput v12, v2, v11

    const/16 v11, -0xb

    aput v11, v2, v10

    const/16 v10, -0x29

    aput v10, v2, v9

    const/16 v9, -0x2fa8

    aput v9, v2, v8

    const/16 v8, -0x30

    aput v8, v2, v7

    const/16 v7, 0x6d56

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v7, v2

    if-lt v4, v7, :cond_0

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v7, v2

    if-lt v4, v7, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v6, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const/4 v2, 0x6

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/16 v11, 0x2d00

    aput v11, v3, v10

    const/16 v10, -0x48a8

    aput v10, v3, v9

    const/16 v9, -0x3d

    aput v9, v3, v8

    const/16 v8, -0x6a

    aput v8, v3, v7

    const/16 v7, -0x6ce3

    aput v7, v3, v4

    const/16 v4, -0x20

    aput v4, v3, v2

    const/4 v2, 0x6

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/16 v12, 0x2d73

    aput v12, v2, v11

    const/16 v11, -0x48d3

    aput v11, v2, v10

    const/16 v10, -0x49

    aput v10, v2, v9

    const/16 v9, -0x9

    aput v9, v2, v8

    const/16 v8, -0x6c97

    aput v8, v2, v7

    const/16 v7, -0x6d

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_2
    array-length v7, v2

    if-lt v4, v7, :cond_2

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3
    array-length v7, v2

    if-lt v4, v7, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v6, v2, v0}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    const/16 v2, 0xe

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, -0x61

    aput v19, v3, v18

    const/16 v18, -0x12

    aput v18, v3, v17

    const/16 v17, 0x405a

    aput v17, v3, v16

    const/16 v16, 0x172c

    aput v16, v3, v15

    const/16 v15, -0x6f88

    aput v15, v3, v14

    const/16 v14, -0x2d

    aput v14, v3, v13

    const/16 v13, -0x7ef7

    aput v13, v3, v12

    const/16 v12, -0x1c

    aput v12, v3, v11

    const/16 v11, -0x57

    aput v11, v3, v10

    const/16 v10, -0x54a7

    aput v10, v3, v9

    const/16 v9, -0x27

    aput v9, v3, v8

    const/16 v8, -0xe

    aput v8, v3, v7

    const/4 v7, -0x7

    aput v7, v3, v4

    const/4 v4, -0x1

    aput v4, v3, v2

    const/16 v2, 0xe

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, -0xf

    aput v20, v2, v19

    const/16 v19, -0x7d

    aput v19, v2, v18

    const/16 v18, 0x402f

    aput v18, v2, v17

    const/16 v17, 0x1740

    aput v17, v2, v16

    const/16 v16, -0x6fe9

    aput v16, v2, v15

    const/16 v15, -0x70

    aput v15, v2, v14

    const/16 v14, -0x7e93

    aput v14, v2, v13

    const/16 v13, -0x7f

    aput v13, v2, v12

    const/16 v12, -0x21

    aput v12, v2, v11

    const/16 v11, -0x54ca

    aput v11, v2, v10

    const/16 v10, -0x55

    aput v10, v2, v9

    const/16 v9, -0x7e

    aput v9, v2, v8

    const/16 v8, -0x77

    aput v8, v2, v7

    const/16 v7, -0x62

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_4
    array-length v7, v2

    if-lt v4, v7, :cond_4

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5
    array-length v7, v2

    if-lt v4, v7, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v6, v2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    new-instance v2, Landroid/os/RemoteCallbackList;

    invoke-direct {v2}, Landroid/os/RemoteCallbackList;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/contentservice/ContentService$1;->this$0:Lcom/sec/android/service/health/contentservice/ContentService;

    # getter for: Lcom/sec/android/service/health/contentservice/ContentService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    invoke-static {v3}, Lcom/sec/android/service/health/contentservice/ContentService;->access$000(Lcom/sec/android/service/health/contentservice/ContentService;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    iput-object v2, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    # getter for: Lcom/sec/android/service/health/contentservice/ContentService;->mCbContentServiceHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/contentservice/ContentService;->access$100()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_6
    return-void

    :cond_0
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_1
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_2
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_3
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    :cond_4
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_5
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :catch_0
    move-exception v2

    goto :goto_6
.end method
