.class public Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;


# static fields
.field private static final STATE_DATA_AND_REQUEST_PROCESSING:I = 0x3

.field private static final STATE_DATA_RECEIVING:I = 0x1

.field private static final STATE_NONE:I = 0x0

.field private static final STATE_REQUEST_PROCESSING:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

.field private device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private exerciseId:J

.field private handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

.field private listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

.field private mCommandExecutor:Ljava/lang/Thread;

.field private mCommandQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;",
            ">;"
        }
    .end annotation
.end field

.field private mDataRecord:Z

.field private mDataType:I

.field private mProcessId:I

.field private mResponseQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;",
            ">;"
        }
    .end annotation
.end field

.field private mState:I

.field private mcontrollingDevObjId:Ljava/lang/Integer;

.field private remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private uniqueKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->currentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    iput-boolean v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    iput v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mCommandExecutor:Ljava/lang/Thread;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)Ljava/util/concurrent/BlockingQueue;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private isStreamingData(I)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    const/16 v1, 0xa

    if-eq p1, v1, :cond_0

    const/4 v1, 0x6

    if-eq p1, v1, :cond_0

    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] isSreamData() bStream : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dataType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method


# virtual methods
.method public addDataListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addDataListener() devObjId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addingDataListener() binderId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->register(Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void
.end method

.method protected final addToCommandQueue(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)Z
    .locals 3

    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "acquireControl() Command : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mCommandExecutor:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mCommandExecutor:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public deinitialize()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->deinitialize()V

    :cond_0
    return-void
.end method

.method public getExerciseId()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    return-wide v0
.end method

.method public getProcessId()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    return v0
.end method

.method public getRecordStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    return v0
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v0, p1, p0, v1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I

    :cond_0
    return-void
.end method

.method public isContollingDevice(Ljava/lang/Integer;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDataListenerPresent()Z
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v0

    if-lez v0, :cond_0

    :cond_0
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDataListenerPresent() bReturn"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public isDataListenerPresent(Ljava/lang/Integer;)Z
    .locals 4

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDataListenerPresent() proceeeId : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    sget-object v3, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v3

    if-nez v0, :cond_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v0, v3, :cond_0

    sget-object v3, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;

    monitor-exit v3

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "isDataListenerPresent() DataListener is not present"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isOnlyDataListenerPresent(Ljava/lang/Integer;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v3

    if-le v3, v2, :cond_0

    move v0, v1

    :goto_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isOnlyDataListenerPresent() bReturn : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " count : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v0, v4, :cond_1

    sget-object v4, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;

    monitor-exit v4

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 11

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v4

    iget-wide v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget v8, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    iget-boolean v9, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v10}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
    .locals 11

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v4

    iget-wide v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget v8, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v10}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDataStarted(II)V
    .locals 11

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataStarted() dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v4

    iget-wide v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget v8, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    iget-boolean v9, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    move v1, p1

    move v2, p2

    invoke-interface/range {v0 .. v10}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;->onDataStarted(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDataStopped(II)V
    .locals 12

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataStopped() dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    iget-wide v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDataStopped() this.mState is changed to STATE_REQUEST_PROCESSING"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v4

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget v8, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    iget-boolean v9, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    iget-object v10, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    move v1, p1

    move v2, p2

    invoke-interface/range {v0 .. v10}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;->onDataStopped(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    invoke-virtual {p0, v11}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->removeDataListener(Ljava/lang/Integer;)V

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDataStopped() this.mState is changed to STATE_NONE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->currentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iput-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->currentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResponseReceived() this.mState is changed to STATE_DATA_RECEIVING"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResponseReceived()"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v3

    iget v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResponseReceived() this.mState is changed to STATE_NONE"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    goto :goto_0
.end method

.method public onStateChanged(I)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStateChanged() state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;->onStateChanged(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_0
    return-void
.end method

.method protected final pollCurrentResponse(JLjava/util/concurrent/TimeUnit;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "timeout : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " TimeUnit : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    return-object v0
.end method

.method public record(Ljava/lang/Integer;J)I
    .locals 8

    const/4 v4, 0x1

    const/4 v0, 0x2

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "record() exerciseID :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deviceObjectId :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-eq v1, v4, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "record() Failure: not in STATE_DATA_RECEIVING state"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->isStreamingData(I)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "record() Failure: not streaming Data type"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x8

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "record() Failure: record already in progress"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v1, v2, :cond_4

    :cond_3
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "record() Failure: not a controlling pid"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    iput-wide p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-wide v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->exerciseId:J

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget v6, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    iget-boolean v7, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;->onDataRecordStarted(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZ)V

    :cond_5
    const/4 v0, 0x0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public removeDataListener(Ljava/lang/Integer;)V
    .locals 5

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removeDataListener() processId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    sget-object v3, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->remoteCallBackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregister(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public request(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-eq v1, v3, :cond_0

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-ne v1, v4, :cond_2

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "request() Device is BUSY, startRecivingData/request has already been called - this.mState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_8

    :cond_4
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "request() device Id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->currentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    iput p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-nez v0, :cond_7

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "request() this.mState is changed to STATE_REQUEST_PROCESSING"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    if-eqz v0, :cond_6

    iput-object v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->currentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    :cond_5
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    :cond_6
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "request() error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_7
    iput v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "request() this.mState is changed to STATE_DATA_AND_REQUEST_PROCESSING"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_8
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "request() handler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method protected final setCurrentResponse(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public startListeningData(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I
    .locals 4

    const/4 v0, 0x2

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startListeningData() devObjectId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startListeningData() listener or processId is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x7

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {p1, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startListeningData() controllingProcessId is not equal"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->isDataListenerPresent(Ljava/lang/Integer;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startListeningData() DataListener is not existed"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->addDataListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized startReceivingData(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 5

    const/4 v4, 0x3

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startReceivingData() device Id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " exerciseId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " record "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startReceivingData() exerciseID listener || processId is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x7

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-eq v1, v0, :cond_2

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-ne v1, v4, :cond_3

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startReceivingData() Device is BUSY, startRecivingData has already been called - this.mState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->isDataListenerPresent(Ljava/lang/Integer;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startReceivingData() exerciseID DataListener Alreay Present for the ProcessID"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v1, :cond_8

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startReceivingData() exerciseID Invoking Handler\'s start Receving Data"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, p3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->addDataListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    iput p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0, p7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    if-nez v0, :cond_7

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData() Invoking Handler\'s Success"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-nez v1, :cond_6

    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData() this.mState is changed to STATE_DATA_RECEIVING"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    if-eqz p4, :cond_5

    invoke-virtual {p0, p1, p5, p6}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->record(Ljava/lang/Integer;J)I

    move-result v1

    if-nez v1, :cond_5

    :cond_5
    :goto_2
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startReceivingData() error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    const/4 v1, 0x3

    :try_start_2
    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData() this.mState is changed to STATE_DATA_AND_REQUEST_PROCESSING"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->removeDataListener(Ljava/lang/Integer;)V

    goto :goto_2

    :cond_8
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData() exerciseID returning ERROR_FAILURE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public stopListeningData(Ljava/lang/Integer;)I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stopListeningData() device Object Id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopListeningData() processId is null or not equal to controllingProcessId"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->removeDataListener(Ljava/lang/Integer;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized stopReceivingData(Ljava/lang/Integer;)I
    .locals 4

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopReceivingData() devObjectId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopReceivingData() processId is NULL so returning "

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-eq v1, v0, :cond_1

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stopReceivingData() is Not BUSY, so stopRecivingData should not be called - this.mState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v1, :cond_4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopReceivingData() Invoking Handler\'s stop Receving Data"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->stopReceivingData()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    const/16 v1, 0x2719

    if-ne v0, v1, :cond_3

    const-string v0, "PEDOSTART"

    const-string v1, "PEDOMETER is force closed"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->stopReceivingData()I

    move-result v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopReceivingData() processId not Equal to controlling processId"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopReceivingData() returning ERROR_FAILURE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final takeCurrentResponse()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    return-object v0
.end method
