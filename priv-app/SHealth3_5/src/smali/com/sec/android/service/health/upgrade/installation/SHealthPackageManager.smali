.class public Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageDeleteObserver;,
        Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SHealthUpgrade(SHealthPackageManager)"


# instance fields
.field private mDeleteMethod:Ljava/lang/reflect/Method;

.field private mDeleteObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageDeleteObserver;

.field private mInstallMethod:Ljava/lang/reflect/Method;

.field private mInstallObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;

.field private mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mInstallObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mDeleteObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageDeleteObserver;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mPackageManager:Landroid/content/pm/PackageManager;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mInstallMethod:Ljava/lang/reflect/Method;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mDeleteMethod:Ljava/lang/reflect/Method;

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;

    new-instance v0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;-><init>(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mInstallObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;

    new-instance v0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageDeleteObserver;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageDeleteObserver;-><init>(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mDeleteObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageDeleteObserver;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/net/Uri;

    aput-object v1, v0, v3

    const-class v1, Landroid/content/pm/IPackageInstallObserver;

    aput-object v1, v0, v4

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v5

    const-class v1, Ljava/lang/String;

    aput-object v1, v0, v2

    new-array v1, v2, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    const-class v2, Landroid/content/pm/IPackageDeleteObserver;

    aput-object v2, v1, v4

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v1, v5

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "installPackage"

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mInstallMethod:Ljava/lang/reflect/Method;

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "deletePackage"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mDeleteMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;)Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;

    return-object v0
.end method


# virtual methods
.method public deletePackage(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    const/4 v5, 0x0

    const-string v0, "SHealthUpgrade(SHealthPackageManager)"

    const-string v1, "deletePackage()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mDeleteMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mDeleteObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageDeleteObserver;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public installPackage(Landroid/net/Uri;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    const/4 v5, 0x2

    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mInstallMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mInstallObserver:Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager$PackageInstallObserver;

    aput-object v4, v2, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public installPackage(Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    const-string v0, "SHealthUpgrade(SHealthPackageManager)"

    const-string v1, "installPackage()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "SHealthUpgrade(SHealthPackageManager)"

    const-string v1, "installPackage() : apkFile is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const-string v0, "SHealthUpgrade(SHealthPackageManager)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "installPackage() - apkFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const-string v0, "SHealthUpgrade(SHealthPackageManager)"

    const-string v1, "installPackage() : apkFile does not exist."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->installPackage(Landroid/net/Uri;)V

    return-void
.end method

.method public setOnInstalledPackaged(Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;)V
    .locals 2

    const-string v0, "SHealthUpgrade(SHealthPackageManager)"

    const-string/jumbo v1, "setOnInstalledPackaged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/installation/SHealthPackageManager;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/ISHealthUpgradeEventListener;

    return-void
.end method
