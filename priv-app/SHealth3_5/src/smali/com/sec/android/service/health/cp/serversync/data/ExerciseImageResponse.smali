.class public Lcom/sec/android/service/health/cp/serversync/data/ExerciseImageResponse;
.super Ljava/lang/Object;


# instance fields
.field public appId:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceFilePath:Ljava/lang/String;

.field public deviceLogPKId:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePhotoPKId:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public exercisePhotoURL:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public imageFileName:Ljava/lang/String;

.field public latitude:Ljava/lang/Float;

.field public longitude:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
