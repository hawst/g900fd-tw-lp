.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;
.super Ljava/lang/Object;


# instance fields
.field private mDaySetting:[Z

.field private mIndex:I

.field private mIsAlarmSetting:Z

.field private mTime:Landroid/text/format/Time;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x7

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mDaySetting:[Z

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public getAlarmDays()[Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mDaySetting:[Z

    return-object v0
.end method

.method public getAlarmStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mIsAlarmSetting:Z

    return v0
.end method

.method public getIndex()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mIndex:I

    return v0
.end method

.method public getTime()Landroid/text/format/Time;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mTime:Landroid/text/format/Time;

    return-object v0
.end method

.method public setAlarmDays([Z)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mDaySetting:[Z

    return-void
.end method

.method public setAlarmStatus(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mIsAlarmSetting:Z

    return-void
.end method

.method public setIndex(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mIndex:I

    return-void
.end method

.method public setTime(Landroid/text/format/Time;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/datatype/AlarmData;->mTime:Landroid/text/format/Time;

    return-void
.end method
