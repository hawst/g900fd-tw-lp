.class public Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Meal"
.end annotation


# instance fields
.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceCreateTimeMeal:Ljava/lang/String;

.field public deviceDaylightSavingMeal:Ljava/lang/String;

.field public deviceMealPKId:J

.field public deviceModifyTimeMeal:Ljava/lang/String;

.field public deviceTimeZoneMeal:Ljava/lang/String;

.field public favoriteYN:Ljava/lang/String;

.field public foodList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;",
            ">;"
        }
    .end annotation
.end field

.field public hdid:Ljava/lang/String;

.field public intakeTime:Ljava/lang/String;

.field public meal:Ljava/lang/String;

.field public mealCategory:Ljava/lang/String;

.field public mealName:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public totalCalorie:D


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->foodList:Ljava/util/ArrayList;

    return-void
.end method
