.class public Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MealItem"
.end annotation


# instance fields
.field public amount:D

.field public appId:Ljava/lang/String;

.field public calorie:D

.field public deviceCreateTimeFoodItem:Ljava/lang/String;

.field public deviceDaylightSavingFoodItem:Ljava/lang/String;

.field public deviceFoodItemPKId:Ljava/lang/String;

.field public deviceModifyTimeFoodItem:Ljava/lang/String;

.field public deviceTimeZoneFoodItem:Ljava/lang/String;

.field public foodDataType:Ljava/lang/String;

.field public foodId:Ljava/lang/String;

.field public foodItemUnit:Ljava/lang/String;

.field public foodName:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public requestType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
