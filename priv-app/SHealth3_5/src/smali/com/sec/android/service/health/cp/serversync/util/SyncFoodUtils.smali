.class public Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodImage;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$WelstoryFoodHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetWelstoryFoodHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodHistory;
    }
.end annotation


# static fields
.field private static final MAX_FOOD_RECORDS:I = 0x1f4

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addFoodNutrient(Ljava/lang/Object;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    move-object v0, p0

    check-cast v0, Ljava/lang/String;

    const-class v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;

    invoke-virtual {v1, v0, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;

    check-cast p0, Ljava/lang/String;

    const-class v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetWelstoryFoodHistory;

    invoke-virtual {v1, p0, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetWelstoryFoodHistory;

    if-eqz v0, :cond_5

    iget-object v3, v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;->fatsecretFoodList:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;->fatsecretFoodList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;->fatsecretFoodList:Ljava/util/ArrayList;

    :goto_0
    if-eqz v0, :cond_c

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "application__id"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->appId:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "update_time"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceModifyTime:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "create_time"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "description"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->description:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->favoriteYN:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->favoriteYN:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "N"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->favoriteYN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "favorite"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    :goto_2
    const-string/jumbo v1, "name"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->foodName:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "server_food_id"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->serverFoodId:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "kcal"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->calorie:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->serverSourceType:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string/jumbo v1, "server_source_type"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->serverSourceType:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_3
    const-string/jumbo v1, "server_locale"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->serverLocale:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "server_root_category"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->rootCategoryName:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "server_root_category_id"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->rootCategoryId:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "server_sub_category"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->subCategoryName:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "server_sub_category_id"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->subCategoryId:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting1:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting1:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "sorting1"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting1:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting2:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting2:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "sorting2"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting2:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const-string/jumbo v1, "time_zone"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "daylight_saving"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "hdid"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->hdid:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sync_status"

    const v2, 0x29811

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretPKId:Ljava/lang/String;

    if-nez v1, :cond_8

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryPKId:Ljava/lang/String;

    move-object v2, v1

    :goto_4
    const-string v1, "_id"

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v1, "food_info"

    const/4 v6, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v7

    invoke-virtual {v3, v1, v6, v5, v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->caloriePerUnit:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretNutrientPKId:Ljava/lang/String;

    if-nez v1, :cond_9

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryNutrientPKId:Ljava/lang/String;

    :goto_5
    if-nez v1, :cond_4

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v6, 0x1

    add-long/2addr v1, v6

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "foodNutrientId is null, so correcting to : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v2, "_id"

    invoke-virtual {v5, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretPKId:Ljava/lang/String;

    if-nez v1, :cond_a

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryPKId:Ljava/lang/String;

    :goto_6
    const-string v2, "food_info__id"

    invoke-virtual {v5, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "kcal_in_unit"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->caloriePerUnit:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "metric_serving_amount"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->metricServingAmount:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "metric_serving_unit"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->metricServingUnit:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "serving_description"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->servingDescription:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "default_number_of_serving_unit"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->defaultNumberOfServingUnit:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "protein"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->protein:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "total_fat"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->totalFat:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "saturated_fat"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->saturatedFat:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "polysaturated_fat"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->polyunsaturatedFat:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "monosaturated_fat"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->monounsaturatedFat:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "trans_fat"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->transFat:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "carbohydrate"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->carbohydrate:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sugar"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sugar:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "cholesterol"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->cholesterol:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sodium"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sodium:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "potassium"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->potassium:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "vitamin_c"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->vitaminC:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "vitamin_a"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->vitaminA:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "calcium"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->calcium:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "iron"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->iron:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "dietary_fiber"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->fiber:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_time"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "update_time"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceModifyTime:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "time_zone"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceTimeZone:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "hdid"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->hdid:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sync_status"

    const v2, 0x29811

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "daylight_saving"

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "hdid"

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->nutrientHdid:Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    const-string v0, "food_nutrient"

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v2

    invoke-virtual {v3, v0, v1, v5, v2}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_3
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error inserting food_nutrient data. Invalid values: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :cond_5
    if-eqz v1, :cond_d

    iget-object v0, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetWelstoryFoodHistory;->welstoryFoodList:Ljava/util/ArrayList;

    if-eqz v0, :cond_d

    iget-object v0, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetWelstoryFoodHistory;->welstoryFoodList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_d

    iget-object v0, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetWelstoryFoodHistory;->welstoryFoodList:Ljava/util/ArrayList;

    goto/16 :goto_0

    :cond_6
    :try_start_5
    const-string v1, "favorite"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2

    :cond_7
    const-string/jumbo v1, "server_source_type"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    :cond_8
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretPKId:Ljava/lang/String;

    move-object v2, v1

    goto/16 :goto_4

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error inserting food_info data. Invalid values: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_9
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretNutrientPKId:Ljava/lang/String;

    goto/16 :goto_5

    :cond_a
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretPKId:Ljava/lang/String;

    goto/16 :goto_6

    :cond_b
    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    :cond_c
    return-void

    :cond_d
    move-object v0, v2

    goto/16 :goto_0
.end method

.method public static addModifyMeal(Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    const-string v2, "addModifyMeal"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v2, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;

    invoke-virtual {v0, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;

    if-eqz v0, :cond_9

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;->mealList:Ljava/util/ArrayList;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;->mealList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_9

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;->mealList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "_id"

    iget-wide v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceMealPKId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "application__id"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->appId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->mealName:Ljava/lang/String;

    if-eqz v5, :cond_3

    const-string/jumbo v5, "name"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->mealName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string/jumbo v5, "total_kilo_calorie"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->totalCalorie:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "type"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->meal:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getIntMealType(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "category"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->mealCategory:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "comment"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->comment:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "sample_time"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->intakeTime:Ljava/lang/String;

    iget-object v7, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceTimeZoneMeal:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "create_time"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceCreateTimeMeal:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v5, "update_time"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceModifyTimeMeal:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v5, "time_zone"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceTimeZoneMeal:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "hdid"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->hdid:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceDaylightSavingMeal:Ljava/lang/String;

    if-eqz v5, :cond_4

    const-string v5, "daylight_saving"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceDaylightSavingMeal:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->favoriteYN:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->favoriteYN:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "N"

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->favoriteYN:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "favorite"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    :goto_3
    const-string/jumbo v5, "sync_status"

    const v6, 0x29811

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string/jumbo v5, "meal"

    const/4 v6, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v7

    invoke-virtual {v3, v5, v6, v1, v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->foodList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "_id"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->deviceFoodItemPKId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->foodId:Ljava/lang/String;

    if-eqz v7, :cond_2

    const-string v7, "food_info__id"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->foodId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v7, "application__id"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->appId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "amount"

    iget v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->amount:F

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string/jumbo v7, "unit"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->foodItemUnit:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v7, "meal__id"

    iget-wide v8, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceMealPKId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "create_time"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->deviceCreateTimeFoodItem:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v7, "update_time"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->deviceModifyTimeFoodItem:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v7, "time_zone"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->deviceTimeZoneFoodItem:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "daylight_saving"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->deviceDaylightSavingFoodItem:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "hdid"

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;->hdid:Ljava/lang/String;

    invoke-virtual {v6, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "sync_status"

    const v7, 0x29811

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    const-string/jumbo v1, "meal_item"

    const/4 v7, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v8

    invoke-virtual {v3, v1, v7, v6, v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_3
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    :catch_0
    move-exception v1

    :try_start_4
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error inserting meal_item data. Invalid values: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    :catch_1
    move-exception v1

    :try_start_5
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    const-string v6, "inserting data into meal_item table failed."

    invoke-static {v1, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_4

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :cond_3
    :try_start_6
    const-string/jumbo v5, "name"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    const-string v5, "daylight_saving"

    const v6, 0x445c2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2

    :cond_5
    const-string v5, "favorite"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    :catch_2
    move-exception v0

    :try_start_7
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error inserting meal data. Invalid values: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_8
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    const-string v1, "inserting data into meal table failed."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "_id"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->deviceImagePKId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "application__id"

    iget-object v8, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->appId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v7, "meal__id"

    iget-wide v8, v0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->deviceMealPKId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "file_path"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->imageDevicePath:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "create_time"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v7, "update_time"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->deviceModifyTime:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v7, "time_zone"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->deviceTimeZone:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "daylight_saving"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "hdid"

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->hdid:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v7, "sync_status"

    const v8, 0x29811

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    const-string/jumbo v7, "meal_image"

    const/4 v8, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v9

    invoke-virtual {v3, v7, v8, v6, v9}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v7

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-eqz v7, :cond_7

    iget-object v7, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->imageUrl:Ljava/lang/String;

    if-eqz v7, :cond_7

    new-instance v7, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;

    iget-object v8, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->imageUrl:Ljava/lang/String;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;->imageDevicePath:Ljava/lang/String;

    invoke-direct {v7, v8, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_5

    :catch_4
    move-exception v1

    :try_start_a
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error inserting meal_image data. Invalid values: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_5

    :catch_5
    move-exception v1

    :try_start_b
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    const-string v6, "inserting data into meal_item table failed."

    invoke-static {v1, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_8
    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    move-object v0, v2

    :goto_6
    return-object v0

    :cond_9
    move-object v0, v1

    goto :goto_6
.end method

.method public static getDeletedFoodNutrient(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodHistory;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodHistory;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "deleted_info"

    const-string/jumbo v3, "table_name = \"food_info\""

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodHistory$Item;

    const-string v3, "create_time"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_FOOD_NUTRIENT_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_WELSTORY_NUTRIENT_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v8
.end method

.method public static getDeletedMeal(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "deleted_info"

    const-string/jumbo v3, "table_name = \"meal\""

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const-string v3, "1y90e30264"

    iput-object v3, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory;->appId:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory;->mealList:Ljava/util/ArrayList;

    new-instance v4, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory$Item;

    const-string v5, "create_time"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v1, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteMealHistory;->mealList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v1, v3, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const-string v1, "deleted_info"

    const-string/jumbo v3, "table_name = \"meal_image\""

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodImage;

    invoke-direct {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodImage;-><init>()V

    const-string v2, "1y90e30264"

    iput-object v2, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodImage;->appId:Ljava/lang/String;

    const-string v2, "F"

    iput-object v2, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodImage;->dataType:Ljava/lang/String;

    const-string v2, "create_time"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestDeleteFoodImage;->deviceCreateTime:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_FOOD_IMAGE_INFO:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    return-object v8
.end method

.method public static getFoodNutrientDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/16 v6, 0x1f4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requesting "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " items for food info"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;

    invoke-direct {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;-><init>()V

    new-instance v4, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;

    invoke-direct {v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_0

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v5, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v0, v5, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v5, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v0, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_1

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v5, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_WELSTORY_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v0, v5, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v5, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v5, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, v4, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetFoodHistory;->deviceCreateTimeArr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_WELSTORY_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v2
.end method

.method public static getMealDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDetailHistoryData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;

    invoke-direct {v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;-><init>()V

    const-string v0, "food"

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;->dataType:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " items for meal"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v4, 0x3e7

    if-ne v0, v4, :cond_0

    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory$Item;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FOOD_INFO_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v4, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory$Item;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$RequestGetMealHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FOOD_INFO_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v2
.end method

.method public static getModifiedFoodNutrient(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/CursorWindowAllocationException;
        }
    .end annotation

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    const-string/jumbo v4, "sync_status = ? or sync_status = ? "

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v2, 0x1

    const v3, 0x29813

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    const/16 v16, 0x0

    const/4 v15, 0x0

    new-instance v14, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;

    invoke-direct {v14}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;-><init>()V

    new-instance v13, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$WelstoryFoodHistory;

    invoke-direct {v13}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$WelstoryFoodHistory;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v10, 0x0

    const/4 v9, 0x0

    :try_start_0
    const-string v2, "food_info"

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v10

    if-eqz v10, :cond_26

    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :goto_0
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_20

    const/4 v2, 0x1

    const-string/jumbo v3, "server_source_type"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const v4, 0x46cd2

    if-eq v3, v4, :cond_0

    const-string/jumbo v3, "server_source_type"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const v4, 0x46cd4

    if-ne v3, v4, :cond_25

    :cond_0
    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_1
    new-instance v19, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;-><init>()V

    const-string v2, "application__id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->appId:Ljava/lang/String;

    if-nez v17, :cond_16

    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretPKId:Ljava/lang/String;

    :goto_2
    const-string/jumbo v2, "update_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceModifyTime:Ljava/lang/String;

    const-string v2, "create_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceCreateTime:Ljava/lang/String;

    const-string v2, "description"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->description:Ljava/lang/String;

    const-string v2, "favorite"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "N"

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->favoriteYN:Ljava/lang/String;

    :goto_3
    const-string/jumbo v2, "name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->foodName:Ljava/lang/String;

    const-string/jumbo v2, "server_food_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->serverFoodId:Ljava/lang/String;

    const-string v2, "kcal"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->calorie:Ljava/lang/String;

    const-string/jumbo v2, "server_source_type"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    const-string/jumbo v2, "server_source_type"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->serverSourceType:Ljava/lang/String;

    :cond_1
    const-string/jumbo v2, "server_locale"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->serverLocale:Ljava/lang/String;

    const-string/jumbo v2, "server_root_category"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->rootCategoryName:Ljava/lang/String;

    const-string/jumbo v2, "server_root_category_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->rootCategoryId:Ljava/lang/String;

    const-string/jumbo v2, "server_sub_category"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->subCategoryName:Ljava/lang/String;

    const-string/jumbo v2, "server_sub_category_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->subCategoryId:Ljava/lang/String;

    const-string/jumbo v2, "sorting1"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting1:Ljava/lang/String;

    const-string/jumbo v2, "sorting2"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sorting2:Ljava/lang/String;

    const-string/jumbo v2, "time_zone"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceTimeZone:Ljava/lang/String;

    const-string v2, "daylight_saving"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceDaylightSaving:Ljava/lang/String;

    const-string v2, "hdid"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->hdid:Ljava/lang/String;

    if-nez v17, :cond_18

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string v3, "food_info"

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretPKId:Ljava/lang/String;

    const-string/jumbo v5, "update_time"

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    const-string v4, "food_info__id = ? "

    if-nez v17, :cond_19

    const-string v2, "food_nutrient"

    const/4 v3, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretPKId:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    :goto_5
    if-eqz v2, :cond_1c

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1c

    :goto_6
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_1c

    if-nez v17, :cond_1a

    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretNutrientPKId:Ljava/lang/String;

    :goto_7
    const-string v3, "calcium"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    const-string v3, "calcium"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->calcium:Ljava/lang/String;

    :cond_2
    const-string v3, "carbohydrate"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_3

    const-string v3, "carbohydrate"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->carbohydrate:Ljava/lang/String;

    :cond_3
    const-string v3, "cholesterol"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_4

    const-string v3, "cholesterol"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->cholesterol:Ljava/lang/String;

    :cond_4
    const-string v3, "default_number_of_serving_unit"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->defaultNumberOfServingUnit:Ljava/lang/String;

    const-string v3, "dietary_fiber"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_5

    const-string v3, "dietary_fiber"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->fiber:Ljava/lang/String;

    :cond_5
    const-string v3, "iron"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_6

    const-string v3, "iron"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->iron:Ljava/lang/String;

    :cond_6
    const-string v3, "kcal_in_unit"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_7

    const-string v3, "kcal_in_unit"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->caloriePerUnit:Ljava/lang/String;

    :cond_7
    const-string/jumbo v3, "metric_serving_amount"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_8

    const-string/jumbo v3, "metric_serving_amount"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->metricServingAmount:Ljava/lang/String;

    :cond_8
    const-string/jumbo v3, "metric_serving_unit"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->metricServingUnit:Ljava/lang/String;

    const-string/jumbo v3, "monosaturated_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_9

    const-string/jumbo v3, "monosaturated_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->monounsaturatedFat:Ljava/lang/String;

    :cond_9
    const-string/jumbo v3, "polysaturated_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_a

    const-string/jumbo v3, "polysaturated_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->polyunsaturatedFat:Ljava/lang/String;

    :cond_a
    const-string/jumbo v3, "potassium"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_b

    const-string/jumbo v3, "potassium"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->potassium:Ljava/lang/String;

    :cond_b
    const-string/jumbo v3, "protein"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_c

    const-string/jumbo v3, "protein"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->protein:Ljava/lang/String;

    :cond_c
    const-string/jumbo v3, "saturated_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_d

    const-string/jumbo v3, "saturated_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->saturatedFat:Ljava/lang/String;

    :cond_d
    const-string/jumbo v3, "serving_description"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->servingDescription:Ljava/lang/String;

    const-string/jumbo v3, "sodium"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_e

    const-string/jumbo v3, "sodium"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sodium:Ljava/lang/String;

    :cond_e
    const-string/jumbo v3, "sugar"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_f

    const-string/jumbo v3, "sugar"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->sugar:Ljava/lang/String;

    :cond_f
    const-string/jumbo v3, "total_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_10

    const-string/jumbo v3, "total_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->totalFat:Ljava/lang/String;

    :cond_10
    const-string/jumbo v3, "trans_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_11

    const-string/jumbo v3, "trans_fat"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->transFat:Ljava/lang/String;

    :cond_11
    const-string/jumbo v3, "vitamin_a"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_12

    const-string/jumbo v3, "vitamin_a"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->vitaminA:Ljava/lang/String;

    :cond_12
    const-string/jumbo v3, "vitamin_c"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_13

    const-string/jumbo v3, "vitamin_c"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->vitaminC:Ljava/lang/String;

    :cond_13
    const-string v3, "hdid"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->nutrientHdid:Ljava/lang/String;

    if-nez v17, :cond_1b

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string v4, "food_nutrient"

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceFatsecretNutrientPKId:Ljava/lang/String;

    const-string/jumbo v6, "update_time"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_8
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_6

    :catchall_0
    move-exception v1

    move-object v3, v10

    :goto_9
    if-eqz v3, :cond_14

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_14
    if-eqz v2, :cond_15

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_15

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_15
    throw v1

    :cond_16
    :try_start_4
    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryPKId:Ljava/lang/String;

    goto/16 :goto_2

    :catchall_1
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    goto :goto_9

    :cond_17
    const-string v2, "Y"

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->favoriteYN:Ljava/lang/String;

    goto/16 :goto_3

    :cond_18
    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string v3, "food_info"

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryPKId:Ljava/lang/String;

    const-string/jumbo v5, "update_time"

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_19
    const-string v2, "food_nutrient"

    const/4 v3, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryPKId:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v2

    goto/16 :goto_5

    :cond_1a
    :try_start_5
    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryNutrientPKId:Ljava/lang/String;

    goto/16 :goto_7

    :cond_1b
    new-instance v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string v4, "food_nutrient"

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/FoodNutrientInfo;->deviceWelstoryNutrientPKId:Ljava/lang/String;

    const-string/jumbo v6, "update_time"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    :cond_1c
    if-eqz v2, :cond_1d

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1d
    if-nez v17, :cond_1f

    iget-object v3, v14, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;->fatsecretFoodList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v16, v16, 0x1

    const/16 v3, 0x1f4

    move/from16 v0, v16

    if-ne v0, v3, :cond_1e

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v3, v4, v14, v12}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v14, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;

    invoke-direct {v14}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$ResponseGetFoodHistory;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const/16 v16, 0x0

    :cond_1e
    :goto_a
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-object v9, v2

    goto/16 :goto_0

    :cond_1f
    iget-object v3, v13, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$WelstoryFoodHistory;->welstoryFoodList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v15, v15, 0x1

    const/16 v3, 0x1f4

    if-ne v15, v3, :cond_1e

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_WELSTORY_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v3, v4, v13, v11}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v13, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$WelstoryFoodHistory;

    invoke-direct {v13}, Lcom/sec/android/service/health/cp/serversync/util/SyncFoodUtils$WelstoryFoodHistory;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v15, 0x0

    goto :goto_a

    :cond_20
    move-object v1, v11

    move-object v2, v12

    move-object v3, v13

    move-object v4, v14

    move v5, v15

    move/from16 v6, v16

    :goto_b
    if-eqz v10, :cond_21

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_21
    if-eqz v9, :cond_22

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_22

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_22
    if-lez v6, :cond_23

    new-instance v6, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v7, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v6, v7, v4, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_23
    if-lez v5, :cond_24

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_WELSTORY_FOOD_INFO:Ljava/lang/String;

    invoke-direct {v2, v4, v3, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_24
    return-object v18

    :catchall_2
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    goto/16 :goto_9

    :catchall_3
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    goto/16 :goto_9

    :cond_25
    move/from16 v17, v2

    goto/16 :goto_1

    :cond_26
    move-object v1, v11

    move-object v2, v12

    move-object v3, v13

    move-object v4, v14

    move v5, v15

    move/from16 v6, v16

    goto :goto_b
.end method

.method public static getModifiedMeal(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    const-string/jumbo v4, "sync_status = ? or sync_status = ? "

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v2, 0x1

    const v3, 0x29813

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood;

    invoke-direct {v13}, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood;-><init>()V

    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    :try_start_0
    const-string/jumbo v2, "meal"

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-result-object v10

    if-eqz v10, :cond_1c

    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    :goto_0
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_e

    new-instance v17, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;-><init>()V

    const-string v2, "application__id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->appId:Ljava/lang/String;

    const-string/jumbo v2, "sync_status"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->requestType:Ljava/lang/String;

    const-string/jumbo v2, "time_zone"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceTimeZoneMeal:Ljava/lang/String;

    const-string/jumbo v2, "sample_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceTimeZoneMeal:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->intakeTime:Ljava/lang/String;

    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, v17

    iput-wide v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceMealPKId:J

    const-string v2, "create_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceCreateTimeMeal:Ljava/lang/String;

    const-string/jumbo v2, "update_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceModifyTimeMeal:Ljava/lang/String;

    const-string v2, "favorite"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "N"

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->favoriteYN:Ljava/lang/String;

    :goto_1
    const-string v2, "comment"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v2, "comment"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->comment:Ljava/lang/String;

    :goto_2
    const-string/jumbo v2, "total_kilo_calorie"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    float-to-double v2, v2

    move-object/from16 v0, v17

    iput-wide v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->totalCalorie:D

    const-string/jumbo v2, "type"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getStringMealType(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->meal:Ljava/lang/String;

    const-string/jumbo v2, "name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->mealName:Ljava/lang/String;

    :cond_0
    const-string v2, "category"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->mealCategory:Ljava/lang/String;

    const-string v2, "daylight_saving"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceDaylightSavingMeal:Ljava/lang/String;

    const-string v2, "hdid"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->hdid:Ljava/lang/String;

    iget-object v2, v13, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood;->mealList:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v3, "meal"

    move-object/from16 v0, v17

    iget-wide v4, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceMealPKId:J

    const-string/jumbo v6, "update_time"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "meal__id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v17

    iget-wide v3, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->deviceMealPKId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v2, "meal_item"

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_b

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_b

    :goto_3
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v18, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;-><init>()V

    const-string v2, "application__id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->appId:Ljava/lang/String;

    const-string/jumbo v2, "sync_status"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->requestType:Ljava/lang/String;

    const-string v2, "amount"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    move-object/from16 v0, v18

    iput-wide v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->amount:D

    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->deviceFoodItemPKId:Ljava/lang/String;

    const-string v2, "create_time"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->deviceCreateTimeFoodItem:Ljava/lang/String;

    const-string/jumbo v2, "update_time"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->deviceModifyTimeFoodItem:Ljava/lang/String;

    const-string/jumbo v2, "time_zone"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->deviceTimeZoneFoodItem:Ljava/lang/String;

    const-string v2, "food_info__id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodId:Ljava/lang/String;

    const-string/jumbo v2, "unit"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodItemUnit:Ljava/lang/String;

    const-string v2, "daylight_saving"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->deviceDaylightSavingFoodItem:Ljava/lang/String;

    const-string/jumbo v2, "name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    const-string/jumbo v2, "name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    const-string/jumbo v2, "name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodName:Ljava/lang/String;

    :goto_4
    const-string v2, "hdid"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->hdid:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v2, "food_info"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "server_source_type"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "server_food_id"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    const-string/jumbo v3, "server_source_type"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const v4, 0x46cd2

    if-ne v3, v4, :cond_7

    const-string v3, "F"

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodDataType:Ljava/lang/String;

    :goto_5
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->foodList:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v3, "meal_item"

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->deviceFoodItemPKId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string/jumbo v6, "update_time"

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    :goto_6
    if-eqz v3, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1

    :cond_4
    :try_start_3
    const-string v2, "Y"

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->favoriteYN:Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    const-string v2, ""

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$Meal;->comment:Ljava/lang/String;

    goto/16 :goto_2

    :cond_6
    const-string v2, "F"

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodName:Ljava/lang/String;

    goto/16 :goto_4

    :cond_7
    const v4, 0x46cd4

    if-ne v3, v4, :cond_8

    const-string v3, "Z"

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodDataType:Ljava/lang/String;

    goto :goto_5

    :cond_8
    const v4, 0x46cd3

    if-ne v3, v4, :cond_9

    const-string v3, "C"

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodDataType:Ljava/lang/String;

    goto :goto_5

    :cond_9
    const-string v3, "C"

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodDataType:Ljava/lang/String;

    goto :goto_5

    :cond_a
    const-string v3, "F"

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood$MealItem;->foodDataType:Ljava/lang/String;

    goto/16 :goto_5

    :cond_b
    if-eqz v9, :cond_c

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_c
    add-int/lit8 v12, v12, 0x1

    const/16 v2, 0x1f4

    if-ne v12, v2, :cond_d

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_INGESTED_FOOD:Ljava/lang/String;

    invoke-direct {v2, v3, v13, v14}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood;

    invoke-direct {v13}, Lcom/sec/android/service/health/cp/serversync/data/RequestSetIngestedFood;-><init>()V

    const/4 v12, 0x0

    :cond_d
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :cond_e
    move-object v2, v9

    move v3, v12

    move-object v4, v13

    move-object v5, v14

    :goto_7
    if-lez v3, :cond_f

    :try_start_4
    new-instance v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v6, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_INGESTED_FOOD:Ljava/lang/String;

    invoke-direct {v3, v6, v4, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_6

    :cond_f
    if-eqz v10, :cond_10

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_10
    if-eqz v2, :cond_11

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_11

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_11
    const-string/jumbo v4, "sync_status = ? or sync_status = ? "

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v2, 0x1

    const v3, 0x29813

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    :try_start_5
    const-string/jumbo v2, "meal_image"

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_18

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v2

    if-eqz v2, :cond_18

    move-object v9, v11

    :goto_8
    :try_start_6
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_17

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;

    invoke-direct {v12}, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;-><init>()V

    const-string/jumbo v2, "sync_status"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSyncStatus(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->requestType:Ljava/lang/String;

    const-string v2, "F"

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->dataType:Ljava/lang/String;

    const-string v2, "file_path"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    if-lez v3, :cond_12

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->fileName:Ljava/lang/String;

    :cond_12
    const-string/jumbo v2, "meal__id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceLogPKId:Ljava/lang/String;

    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceLogIMGPKId:Ljava/lang/String;

    const-string v2, "file_path"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceFilePath:Ljava/lang/String;

    const-string v2, "application__id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->appId:Ljava/lang/String;

    const-string v2, "create_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceCreateTime:Ljava/lang/String;

    const-string/jumbo v2, "update_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceModifyTime:Ljava/lang/String;

    const-string/jumbo v2, "time_zone"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceTimeZone:Ljava/lang/String;

    const-string v2, "daylight_saving"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceDaylightSaving:Ljava/lang/String;

    const-string v2, "hdid"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->hdid:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceLogPKId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v2, "meal"

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-object v8

    if-eqz v8, :cond_15

    :try_start_7
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_15

    :goto_9
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, "create_time"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceLogCreateTime:Ljava/lang/String;

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_9

    :catchall_1
    move-exception v1

    move-object v2, v8

    :goto_a
    if-eqz v10, :cond_13

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_13
    if-eqz v2, :cond_14

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_14

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_14
    throw v1

    :cond_15
    if-eqz v8, :cond_16

    :try_start_8
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_16
    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string/jumbo v3, "meal_image"

    iget-object v4, v12, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceLogIMGPKId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string/jumbo v6, "update_time"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_IMAGE_INFO:Ljava/lang/String;

    invoke-direct {v2, v3, v12, v11}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-object v9, v8

    goto/16 :goto_8

    :cond_17
    move-object v11, v9

    :cond_18
    if-eqz v10, :cond_19

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_19
    if-eqz v11, :cond_1a

    invoke-interface {v11}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1a

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_1a
    if-eqz v16, :cond_1b

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1b

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;

    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    const-string/jumbo v1, "requestType"

    iget-object v2, v6, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->requestType:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "dataType"

    iget-object v2, v6, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->dataType:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "deviceCreateTime"

    iget-object v2, v6, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceCreateTime:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "userImage"

    iget-object v2, v6, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceFilePath:Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_IMAGE:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v6, v6, Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;->deviceFilePath:Ljava/lang/String;

    const-string/jumbo v7, "userImage"

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_1b
    return-object v15

    :catchall_2
    move-exception v1

    move-object v2, v11

    goto/16 :goto_a

    :catchall_3
    move-exception v1

    move-object v2, v9

    goto/16 :goto_a

    :catchall_4
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    goto/16 :goto_6

    :catchall_5
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    goto/16 :goto_6

    :catchall_6
    move-exception v1

    move-object v3, v10

    goto/16 :goto_6

    :cond_1c
    move-object v2, v9

    move v3, v12

    move-object v4, v13

    move-object v5, v14

    goto/16 :goto_7
.end method
