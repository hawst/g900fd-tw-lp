.class final enum Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
.super Ljava/lang/Enum;
.source "SamsungAccountUserTokenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RequestState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

.field public static final enum EXPIRED_REQUEST_OFFERED_WHILE_NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

.field public static final enum EXPIRED_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

.field public static final enum IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

.field public static final enum NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 55
    new-instance v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    const-string v1, "NORMAL_REQUEST_ONGOING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 56
    new-instance v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    const-string v1, "EXPIRED_REQUEST_OFFERED_WHILE_NORMAL_REQUEST_ONGOING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_OFFERED_WHILE_NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 57
    new-instance v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    const-string v1, "EXPIRED_REQUEST_ONGOING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 52
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    sget-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_OFFERED_WHILE_NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->$VALUES:[Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 52
    const-class v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->$VALUES:[Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    return-object v0
.end method
