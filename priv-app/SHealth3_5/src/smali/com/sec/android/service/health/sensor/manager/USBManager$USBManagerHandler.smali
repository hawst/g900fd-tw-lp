.class Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/USBManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "USBManagerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/manager/USBManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/sec/android/service/health/sensor/manager/USBManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;-><init>(Lcom/sec/android/service/health/sensor/manager/USBManager;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mShealthProtocolInterfaceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$300(Lcom/sec/android/service/health/sensor/manager/USBManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$400(Lcom/sec/android/service/health/sensor/manager/USBManager;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mShealthProtocolInterfaceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$300(Lcom/sec/android/service/health/sensor/manager/USBManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$500()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$600(Lcom/sec/android/service/health/sensor/manager/USBManager;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$600(Lcom/sec/android/service/health/sensor/manager/USBManager;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBProfileHandlerList:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "Handler list is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return v7

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBProfileHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBProfileHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->isProtocolSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBProfileHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    # setter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$702(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mShealthProtocolInterfaceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$300(Lcom/sec/android/service/health/sensor/manager/USBManager;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$400(Lcom/sec/android/service/health/sensor/manager/USBManager;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    # invokes: Lcom/sec/android/service/health/sensor/manager/USBManager;->setUsbProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$800(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$700(Lcom/sec/android/service/health/sensor/manager/USBManager;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->createAndAddProfileHandlerController(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$700(Lcom/sec/android/service/health/sensor/manager/USBManager;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const-string/jumbo v4, "send_ping"

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->request(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$400(Lcom/sec/android/service/health/sensor/manager/USBManager;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    # setter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$402(Lcom/sec/android/service/health/sensor/manager/USBManager;I)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;->this$0:Lcom/sec/android/service/health/sensor/manager/USBManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$900(Lcom/sec/android/service/health/sensor/manager/USBManager;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, v6, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_3
    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "ProfileHandlerController controller is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "Usb device not identified!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$500()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    # getter for: Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBManager:Lcom/sec/android/service/health/sensor/manager/USBManager;
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/USBManager;->access$1000()Lcom/sec/android/service/health/sensor/manager/USBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->deinitialize()V

    goto/16 :goto_0
.end method
