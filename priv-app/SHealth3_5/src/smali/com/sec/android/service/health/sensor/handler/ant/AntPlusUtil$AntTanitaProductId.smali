.class public Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AntTanitaProductId"
.end annotation


# static fields
.field private static instance:Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;


# instance fields
.field private hash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->instance:Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->hash:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->hash:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "BC-1000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->hash:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "BC-1100F"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->hash:Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "BC-1500"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->instance:Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;

    return-object v0
.end method


# virtual methods
.method public getProductString(I)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->getInstance()Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil$AntTanitaProductId;->hash:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
