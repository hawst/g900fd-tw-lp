.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;
.super Landroid/app/Service;


# static fields
.field private static final TAG:Ljava/lang/String; = "HealthSyncService"

.field private static mHealthSyncAdapter:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

.field private static final sSyncAdapterLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;->sSyncAdapterLock:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;->mHealthSyncAdapter:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;->mHealthSyncAdapter:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "HealthSyncService"

    const-string v1, "Service created"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;->sSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;->mHealthSyncAdapter:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthSyncService;->mHealthSyncAdapter:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "HealthSyncService"

    const-string v1, "Service destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
