.class public Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;


# instance fields
.field private isSyncing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->isSyncing:Z

    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->instance:Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->instance:Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->instance:Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    return-object v0
.end method


# virtual methods
.method public getIsSycning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->isSyncing:Z

    return v0
.end method

.method public setIsSyncing(Z)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Status Changed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->isSyncing:Z

    return-void
.end method
