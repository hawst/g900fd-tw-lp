.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;
.super Ljava/lang/Thread;


# static fields
.field private static OBJ_LOCK:Ljava/lang/Object; = null

.field private static final mNAME:Ljava/lang/String; = "PWAccessP"

.field private static final mTAG:Ljava/lang/String; = "HME"

.field private static rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;


# instance fields
.field private final mSPP_UUID:Ljava/util/UUID;

.field private mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

.field private searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->OBJ_LOCK:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v0, "00001101-0000-1000-8000-00805F9B34FB"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->mSPP_UUID:Ljava/util/UUID;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v0, "HME"

    const-string v1, "Creation of the HMESocketListenerThread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v0, "HME"

    const-string v1, "HMESocketListenerThread listenUsingRfcommWithServiceRecord"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const-string v1, "PWAccessP"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->mSPP_UUID:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->listenUsingRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static addDeviceForConnecting(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;)V
    .locals 3

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string v0, "HME"

    const-string v1, "addDeviceForConnecting address or Listener Null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->OBJ_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "HME"

    const-string v2, "addDeviceForConnecting address Creating Listener Thread"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->addDeviceToSearchList(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->start()V

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->addDeviceToSearchList(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private addDeviceToSearchList(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;)V
    .locals 3

    const-string v0, "HME"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addDeviceToSearchList "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static removeDeviceFromConnecting(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->removeDeviceFromSearchList(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private removeDeviceFromSearchList(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    const-string v0, "HME"

    const-string v1, "addDeviceToSearchList address null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "HME"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addDeviceToSearchList "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->stopHmeSocketListenerThread()V

    goto :goto_0
.end method

.method private stopHmeSocketListenerThread()V
    .locals 2

    const-string v0, "HME"

    const-string/jumbo v1, "stopHmeSocketListenerThread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "HME"

    const-string/jumbo v1, "stopHmeSocketListenerThread Still Devices are there,  so not stopping rfcomSocketListenerThread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    if-eqz v0, :cond_1

    const-string v0, "HME"

    const-string/jumbo v1, "stopHmeSocketListenerThread Calling Interrupt/stoping the thread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->interrupt()V

    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v1, 0x0

    const-string v0, "HME"

    const-string/jumbo v2, "rfcomSocketListenerThread() run()"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v2

    if-lez v2, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    if-eqz v2, :cond_5

    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    const-string v0, "HME"

    const-string/jumbo v3, "rfcomSocketListenerThread run() connected !!"

    invoke-static {v0, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->searchDevicesList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, v3, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSessionStartedListener;->connected(Ljava/lang/String;Landroid/bluetooth/BluetoothSocket;)V

    :cond_0
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->removeDeviceFromConnecting(Ljava/lang/String;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v2

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    :goto_2
    const-string v2, "HME"

    const-string v3, "End rfcomSocketListenerThread"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v0

    :goto_3
    :try_start_6
    const-string v3, "HME"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "rfcomSocketListenerThread Exception :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v2, :cond_3

    :try_start_7
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_3
    :goto_4
    const-string v0, "HME"

    const-string/jumbo v2, "rfcomSocketListenerThread Exiting"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sput-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;->rfcomSocketListenerThread:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComSocketListenerThread;

    return-void

    :catch_1
    move-exception v2

    const-string v3, "HME"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "rfcomSocketListenerThread Exception :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_2
    move-exception v0

    const-string v2, "HME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "rfcomSocketListenerThread Exception :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catchall_1
    move-exception v0

    :goto_5
    if-eqz v2, :cond_4

    :try_start_8
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_4
    :goto_6
    throw v0

    :catch_3
    move-exception v1

    const-string v2, "HME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "rfcomSocketListenerThread Exception :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catch_4
    move-exception v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    goto/16 :goto_3

    :cond_5
    move-object v0, v2

    goto/16 :goto_1
.end method
