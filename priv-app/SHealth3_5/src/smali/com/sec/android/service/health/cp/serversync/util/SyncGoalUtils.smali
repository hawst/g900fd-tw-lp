.class public Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestDeleteGoalHistory;,
        Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestGetGoalHistory;
    }
.end annotation


# static fields
.field public static final MAX_GOAL_RECORDS:I = 0x64

.field public static final PEDOMETER_GOAL:Ljava/lang/String; = "10009_SCONTEXT_PEDOMETER"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addModifyGoal(Ljava/lang/Object;)V
    .locals 8

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->TAG:Ljava/lang/String;

    const-string v1, "addModifyGoal"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    check-cast p0, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/data/GoalList;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/GoalList;

    if-eqz v0, :cond_7

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalList;->goalList:Ljava/util/ArrayList;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalList;->goalList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_7

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->beginTransaction()V

    :try_start_0
    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalList;->goalList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "_id"

    iget-wide v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceGoalPKId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "application__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->appId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->period:Ljava/lang/String;

    if-eqz v4, :cond_1

    const-string/jumbo v4, "period"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->period:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    const-string v4, "goal_type"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalType:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->convertGoalType(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "goal_subtype"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalType:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->convertGoalType(Ljava/lang/String;)I

    move-result v5

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalSubType:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->convertGoalSubType(ILjava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v4, "value"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalValue:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    const-string/jumbo v4, "user_device__id"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->accId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "daylight_saving"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceDaylightSaving:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalSampleDate:Ljava/lang/String;

    if-eqz v4, :cond_2

    const-string/jumbo v4, "set_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalSampleDate:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillisWithTimeZone(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_2
    const-string v4, "create_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceCreateTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "update_time"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceModifyTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v4, "time_zone"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "current_value"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->currentValue:Ljava/lang/Double;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v4, "hdid"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->hdid:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "sync_status"

    const v5, 0x29811

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v4, "3437xv63xb"

    iget-object v5, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->appId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->accId:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->accId:Ljava/lang/String;

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    invoke-static {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->insertNewUserDeviceRow(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    const-string/jumbo v5, "user_device__id"

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v4, "goal"

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v3, v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->accId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->insertUserDeviceRow(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "goal"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v1, v0, v4, v3, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error inserting goal data. Invalid values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    throw v0

    :cond_5
    :try_start_3
    const-string v0, "goal"

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    invoke-virtual {v1, v0, v4, v3, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J
    :try_end_3
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :cond_6
    :try_start_4
    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->endTransaction()V

    :cond_7
    return-void
.end method

.method public static convertGoalSubType(ILjava/lang/String;)I
    .locals 1

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x9c41
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static convertGoalSubType(II)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const-string v0, "1"

    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const-string v0, "1"

    goto :goto_0

    :pswitch_3
    const-string v0, "1"

    goto :goto_0

    :pswitch_4
    const-string v0, "3"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x9c41
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static convertGoalType(Ljava/lang/String;)I
    .locals 2

    const v0, 0x9c44

    const-string v1, "1"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "2"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x9c42

    goto :goto_0

    :cond_2
    const-string v1, "3"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v0, 0x9c46

    goto :goto_0

    :cond_3
    const-string v1, "4"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v0, 0x9c45

    goto :goto_0

    :cond_4
    const-string v1, "5"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const v0, 0x9c41

    goto :goto_0

    :cond_5
    const-string v1, "6"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const v0, 0x9c43

    goto :goto_0

    :cond_6
    const-string v1, "7"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const v0, 0x9c47

    goto :goto_0

    :cond_7
    const-string v1, "8"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const v0, 0x9c48

    goto :goto_0

    :cond_8
    const-string v1, "9"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const v0, 0x9c49

    goto :goto_0

    :cond_9
    const-string v1, "10"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const v0, 0x9c4a

    goto :goto_0

    :cond_a
    const-string v1, "11"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x9c4b

    goto :goto_0
.end method

.method public static convertGoalType(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "1"

    goto :goto_0

    :pswitch_1
    const-string v0, "2"

    goto :goto_0

    :pswitch_2
    const-string v0, "3"

    goto :goto_0

    :pswitch_3
    const-string v0, "4"

    goto :goto_0

    :pswitch_4
    const-string v0, "5"

    goto :goto_0

    :pswitch_5
    const-string v0, "6"

    goto :goto_0

    :pswitch_6
    const-string v0, "7"

    goto :goto_0

    :pswitch_7
    const-string v0, "8"

    goto :goto_0

    :pswitch_8
    const-string v0, "9"

    goto :goto_0

    :pswitch_9
    const-string v0, "10"

    goto :goto_0

    :pswitch_a
    const-string v0, "11"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9c41
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static getDeletedGoalData(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDeletedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestDeleteGoalHistory;

    invoke-direct {v9}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestDeleteGoalHistory;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "deleted_info"

    const-string/jumbo v3, "table_name = \"goal\""

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestDeleteGoalHistory;->goalList:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestDeleteGoalHistory$Item;

    const-string v4, "create_time"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestDeleteGoalHistory$Item;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestDeleteGoalHistory;->goalList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return-object v2

    :cond_2
    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_USER_GOAL:Ljava/lang/String;

    invoke-direct {v0, v1, v9}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v8

    goto :goto_1
.end method

.method public static getGoalDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->TAG:Ljava/lang/String;

    const-string v1, "getDetailHistoryData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestGetGoalHistory;

    invoke-direct {v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestGetGoalHistory;-><init>()V

    const-string v2, "1y90e30264"

    iput-object v2, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestGetGoalHistory;->appId:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMdd(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestGetGoalHistory;->startDate:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMdd(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils$RequestGetGoalHistory;->endDate:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v3, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USER_GOAL:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static getModifiedGoal(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/CursorWindowAllocationException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v2, 0x0

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/data/GoalList;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/data/GoalList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v3, "sync_status = ? or sync_status = ? "

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const v1, 0x29812

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    const/4 v1, 0x1

    const v5, 0x29813

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const-string v1, "goal"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move v6, v8

    move-object v7, v9

    move-object v9, v10

    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;

    invoke-direct {v10}, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;-><init>()V

    const-string v0, "application__id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->appId:Ljava/lang/String;

    const-string v0, "goal_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->convertGoalType(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalType:Ljava/lang/String;

    const-string/jumbo v0, "value"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalValue:Ljava/lang/String;

    const-string/jumbo v0, "period"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->period:Ljava/lang/String;

    const-string v0, "goal_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v1, "goal_subtype"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncGoalUtils;->convertGoalSubType(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalSubType:Ljava/lang/String;

    const-string v0, "goal_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const v1, 0x9c41

    if-ne v0, v1, :cond_2

    const-string/jumbo v0, "user_device__id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "10009_SCONTEXT_PEDOMETER"

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->accId:Ljava/lang/String;

    :goto_1
    const-string v0, "create_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceCreateTime:Ljava/lang/String;

    const-string/jumbo v0, "update_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceModifyTime:Ljava/lang/String;

    const-string/jumbo v0, "time_zone"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceTimeZone:Ljava/lang/String;

    const-string v0, "_id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceGoalPKId:J

    const-string v0, "daylight_saving"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceDaylightSaving:Ljava/lang/String;

    const-string/jumbo v0, "set_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "set_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v2, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceTimeZone:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUtil;->getyyyyMMddHHmmssSSSWithTimeZone(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->goalSampleDate:Ljava/lang/String;

    :cond_0
    const-string v0, "current_value"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "current_value"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->currentValue:Ljava/lang/Double;

    :cond_1
    const-string v0, "hdid"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->hdid:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;

    const-string v1, "goal"

    iget-wide v2, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->deviceGoalPKId:J

    const-string/jumbo v4, "update_time"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v9, Lcom/sec/android/service/health/cp/serversync/data/GoalList;->goalList:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v6, 0x1

    const/16 v1, 0x64

    if-ne v0, v1, :cond_7

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_GOAL:Ljava/lang/String;

    invoke-direct {v0, v1, v9, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/android/service/health/cp/serversync/data/GoalList;

    invoke-direct {v2}, Lcom/sec/android/service/health/cp/serversync/data/GoalList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move v0, v8

    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move v6, v0

    move-object v7, v1

    move-object v9, v2

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/sec/android/service/health/cp/serversync/data/GoalItem;->accId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    move v6, v8

    move-object v7, v9

    move-object v9, v10

    :cond_4
    if-eqz v12, :cond_5

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_5
    if-lez v6, :cond_6

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_GOAL:Ljava/lang/String;

    invoke-direct {v0, v1, v9, v7}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    return-object v11

    :cond_7
    move-object v1, v7

    move-object v2, v9

    goto :goto_2
.end method
