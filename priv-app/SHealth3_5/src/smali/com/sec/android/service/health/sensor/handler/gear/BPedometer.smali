.class public Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/gear/DBStorable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/service/health/sensor/handler/gear/DBStorable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dateTime:J

.field private distance:F

.field private exId:I

.field private id:I

.field private kcal:F

.field private kindOfWalking:I

.field private runSteps:I

.field private speed:F

.field private totalStep:I

.field private upDownSteps:I

.field private walkSteps:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIJIIIIFFFI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->id:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->exId:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->dateTime:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->totalStep:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->runSteps:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->walkSteps:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->upDownSteps:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kcal:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->speed:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->distance:F

    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kindOfWalking:I

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->id:I

    iput p2, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->exId:I

    iput-wide p3, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->dateTime:J

    iput p5, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->totalStep:I

    iput p9, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kcal:F

    iput p10, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->speed:F

    iput p11, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->distance:F

    iput p12, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kindOfWalking:I

    iput p6, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->runSteps:I

    iput p7, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->walkSteps:I

    iput p8, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->upDownSteps:I

    return-void
.end method

.method public constructor <init>(JIIIIFFFI)V
    .locals 4

    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->id:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->exId:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->dateTime:J

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->totalStep:I

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->runSteps:I

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->walkSteps:I

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->upDownSteps:I

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kcal:F

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->speed:F

    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->distance:F

    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kindOfWalking:I

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->dateTime:J

    iput p3, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->totalStep:I

    iput p7, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kcal:F

    iput p8, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->speed:F

    iput p9, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->distance:F

    iput p10, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kindOfWalking:I

    iput p4, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->runSteps:I

    iput p5, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->walkSteps:I

    iput p6, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->upDownSteps:I

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;)I
    .locals 4

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getTime()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->compareTo(Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Lcom/sec/android/service/health/sensor/handler/gear/DBStorable;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getTypeId()I

    move-result v0

    invoke-interface {p1}, Lcom/sec/android/service/health/sensor/handler/gear/DBStorable;->getTypeId()I

    move-result v2

    if-eq v0, v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getDateTime()J

    move-result-wide v2

    move-object v0, p1

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getDateTime()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getKindOfWalking()I

    move-result v0

    check-cast p1, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;

    invoke-virtual {p1}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getKindOfWalking()I

    move-result v2

    if-eq v0, v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getContentValue()Landroid/content/ContentValues;
    .locals 1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    return-object v0
.end method

.method public getDateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->dateTime:J

    return-wide v0
.end method

.method public getDistance()F
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->distance:F

    return v0
.end method

.method public getExId()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->exId:I

    return v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->id:I

    return v0
.end method

.method public getInputType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getKcal()F
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kcal:F

    return v0
.end method

.method public getKindOfWalking()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kindOfWalking:I

    return v0
.end method

.method public getRowId()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->id:I

    return v0
.end method

.method public getRunSteps()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->runSteps:I

    return v0
.end method

.method public getSpeed()F
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->speed:F

    return v0
.end method

.method public getStep()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->totalStep:I

    return v0
.end method

.method public getTime()J
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->getDateTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTypeId()I
    .locals 1

    const/16 v0, 0x32

    return v0
.end method

.method public getUpDownSteps()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->upDownSteps:I

    return v0
.end method

.method public getWalkSteps()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->walkSteps:I

    return v0
.end method

.method public setDateTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->dateTime:J

    return-void
.end method

.method public setDistance(F)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->distance:F

    return-void
.end method

.method public setExId(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->exId:I

    return-void
.end method

.method public setId(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->id:I

    return-void
.end method

.method public setKcal(F)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kcal:F

    return-void
.end method

.method public setKindOfWalking(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kindOfWalking:I

    return-void
.end method

.method public setRunSteps(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->runSteps:I

    return-void
.end method

.method public setSpeed(F)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->speed:F

    return-void
.end method

.method public setStep(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->totalStep:I

    return-void
.end method

.method public setUpDownSteps(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->upDownSteps:I

    return-void
.end method

.method public setWalkSteps(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->walkSteps:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->dateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->totalStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->runSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->walkSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->upDownSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->speed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->distance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/BPedometer;->kindOfWalking:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
