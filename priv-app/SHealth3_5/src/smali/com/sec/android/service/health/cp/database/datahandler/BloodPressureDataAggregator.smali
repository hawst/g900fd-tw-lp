.class public Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    return-void
.end method


# virtual methods
.method protected checkMandatoryFields()Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Data will be skipped. mandatory fields empty"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method getContentValues()Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "systolic"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "diastolic"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "pulse"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "mean"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->mean:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "sample_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->time:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "input_source_type"

    const v2, 0x3f7a2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    return-object v0
.end method

.method getTableUri()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 3

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    if-eqz v0, :cond_0

    const-string v0, "BloodPressure_DATA"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
