.class public Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/garmin/fit/DeviceInfoMesgListener;
.implements Lcom/garmin/fit/FileIdMesgListener;
.implements Lcom/garmin/fit/UserProfileMesgListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;
    }
.end annotation


# instance fields
.field public fitnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMesg(Lcom/garmin/fit/DeviceInfoMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/FileIdMesg;)V
    .locals 0

    return-void
.end method

.method public onMesg(Lcom/garmin/fit/UserProfileMesg;)V
    .locals 0

    return-void
.end method

.method public setNotiListener(Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener;->fitnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    return-void
.end method
