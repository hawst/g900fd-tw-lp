.class public interface abstract Lcom/sec/android/service/health/sensor/handler/gear/DBStorable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# virtual methods
.method public abstract equals(Lcom/sec/android/service/health/sensor/handler/gear/DBStorable;)Z
.end method

.method public abstract getContentValue()Landroid/content/ContentValues;
.end method

.method public abstract getInputType()I
.end method

.method public abstract getRowId()I
.end method

.method public abstract getTime()J
.end method

.method public abstract getTypeId()I
.end method
