.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;
.super Ljava/lang/Object;


# static fields
.field static ReceiveFlow:B = 0x0t

.field private static final Tag:Ljava/lang/String; = "hmeCH"

.field static flow:B = 0x0t

.field static final nCommandPos:I = 0x6

.field static final nCommandSize:I = 0x5

.field static final nCrcSize:I = 0x4

.field static final nFlow:I = 0x1

.field static final nHeaderSize:I = 0xa

.field static final nPacketSize:I = 0x4

.field static stx:B


# instance fields
.field Command:[B

.field PacketSize:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x51

    sput-byte v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->flow:B

    const/16 v0, 0x52

    sput-byte v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->ReceiveFlow:B

    const/16 v0, -0x80

    sput-byte v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->stx:B

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    const/4 v0, 0x5

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    return-void
.end method

.method public constructor <init>(BLjava/lang/String;I)V
    .locals 5

    const/4 v2, 0x5

    const/4 v1, 0x4

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sput-byte p1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->flow:B

    new-array v0, v1, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    new-array v0, v2, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    const/4 v3, -0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([BB)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v0, v3

    if-le v0, v2, :cond_0

    move v0, v2

    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    invoke-static {v3, v4, v2, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v0, v2

    if-le v0, v1, :cond_2

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    invoke-static {v2, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 6

    const/4 v2, 0x5

    const/4 v1, 0x4

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    new-array v0, v2, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    const/4 v3, -0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([BB)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v0, v3

    if-le v0, v2, :cond_0

    move v0, v2

    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    invoke-static {v3, v5, v2, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v2, "%04d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v0, v2

    if-le v0, v1, :cond_2

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    invoke-static {v2, v5, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public constructor <init>([B[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    return-void
.end method

.method public static getCommand([B)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method public static getPackeSize([B)I
    .locals 5

    const/4 v4, 0x0

    const/4 v0, -0x1

    :try_start_0
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-direct {v1, p0, v2, v3}, Ljava/lang/String;-><init>([BII)V

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hmeCH"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static toByteHeader(Ljava/lang/String;I)[B
    .locals 11

    const/4 v10, 0x4

    const/4 v6, 0x1

    const/4 v9, -0x1

    const/4 v1, 0x5

    const/4 v8, 0x0

    const/16 v0, 0xb

    new-array v2, v0, [B

    invoke-static {v2, v9}, Ljava/util/Arrays;->fill([BB)V

    const/4 v3, 0x6

    const/4 v0, 0x2

    sget-byte v4, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->stx:B

    aput-byte v4, v2, v8

    sget-byte v4, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->flow:B

    aput-byte v4, v2, v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "%04x"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v5, v4

    if-le v5, v10, :cond_0

    :cond_0
    invoke-static {v4, v8, v2, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v0, v4

    if-le v0, v1, :cond_1

    move v0, v1

    :cond_1
    array-length v5, v4

    invoke-static {v4, v8, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_1
    if-ge v0, v1, :cond_3

    add-int v4, v0, v3

    aput-byte v9, v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_3
    return-object v2
.end method


# virtual methods
.method public getCmd()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method public getPacketSize()I
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getflow()B
    .locals 1

    sget-byte v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->flow:B

    return v0
.end method

.method public setCmd(Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x5

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v0, v2

    if-le v0, v1, :cond_0

    move v0, v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public setPacketSize(I)V
    .locals 4

    const/4 v0, 0x4

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v1, v2

    if-le v1, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public setflow(B)V
    .locals 0

    sput-byte p1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->flow:B

    return-void
.end method

.method public toByte()[B
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x0

    const/16 v0, 0xa

    new-array v0, v0, [B

    sget-byte v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->flow:B

    aput-byte v1, v0, v4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->PacketSize:[B

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/HmeHeader;->Command:[B

    invoke-static {v1, v4, v0, v5, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method
