.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVTask;
.super Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;


# static fields
.field private static final UV_CODE:Ljava/lang/String; = "uvi"


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    return-void
.end method


# virtual methods
.method protected getDeletedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils;->getDeletedUV(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getDetailHistoryData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/UVTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    invoke-static {v0, p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils;->getUVDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getSyncTypeString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "uvi"

    return-object v0
.end method

.method protected getTableName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "ultraviolet_rays"

    return-object v0
.end method

.method protected getUnsyncedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils;->getModifiedUV(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected updateItemsLocally(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    invoke-static {p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncUVUtils;->addUV(Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method
