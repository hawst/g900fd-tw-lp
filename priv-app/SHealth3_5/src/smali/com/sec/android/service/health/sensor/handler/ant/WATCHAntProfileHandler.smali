.class public Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$13;,
        Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]WATCHSensor"

.field private static final TESTFILE:Ljava/lang/String; = "/mnt/sdcard/WatchTest/Watch.fit"
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation
.end field

.field private static final TESTFLAGFILE:Ljava/lang/String; = "/mnt/sdcard/WatchTest/watch.txt"
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation
.end field

.field private static final TESTPATHFILE:Ljava/lang/String; = "/mnt/sdcard/WatchTest"
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation
.end field


# instance fields
.field private AntfsDeviceType:[I

.field private AntfsManufacturerId:[I

.field private DisplayName:[Ljava/lang/String;

.field private FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

.field private UUIDtoString:[Ljava/lang/String;

.field private bDevicesInList:Z

.field private deviceInfoArray:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

.field listData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            ">;"
        }
    .end annotation
.end field

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private final mCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field mTimer:Ljava/util/Timer;

.field mdata:Landroid/os/Bundle;

.field public watchnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mCount:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mTimer:Ljava/util/Timer;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->watchnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mdata:Landroid/os/Bundle;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$11;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$11;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$12;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$12;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method private FitFileWrite([B)V
    .locals 4

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$10;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$10;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[B)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WATCHAntProfileHandler FitFileWrite #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mFisrtConnect:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mFisrtConnect:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;)[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->deviceInfoArray:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->bDevicesInList:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsDeviceType:[I

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[I)[I
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsDeviceType:[I

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsManufacturerId:[I

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[I)[I
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsManufacturerId:[I

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->UUIDtoString:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->UUIDtoString:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mFisrtConnect:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->stopTimer()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Z
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->checkFlagfile()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->FitFileWrite([B)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->readFitFromData([B)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->writeFile([B)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->startTimer()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method private checkFlagfile()Z
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/sdcard/WatchTest/watch.txt"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readFitFromData([B)V
    .locals 5

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Lcom/garmin/fit/Decode;

    invoke-direct {v0}, Lcom/garmin/fit/Decode;-><init>()V

    new-instance v2, Lcom/garmin/fit/MesgBroadcaster;

    invoke-direct {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;-><init>(Lcom/garmin/fit/Decode;)V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWatchListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->getUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->getAntDeviceType()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWatchListener;-><init>(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWatchListener;->setNotiListener(Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/FileIdMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/DeviceInfoMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/ActivityMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/SessionMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/LapMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/LengthMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/RecordMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/EventMesgListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/HrvMesgListener;)V

    :try_start_0
    invoke-virtual {v2, v1}, Lcom/garmin/fit/MesgBroadcaster;->run(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/garmin/fit/FitRuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string/jumbo v1, "readFitFromData - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v0, "[HealthSensor]WATCHSensor"

    const-string/jumbo v2, "readFitFromData - FitRuntimeException"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string/jumbo v1, "readFitFromData - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_1
    throw v0

    :catch_3
    move-exception v1

    const-string v1, "[HealthSensor]WATCHSensor"

    const-string/jumbo v2, "readFitFromData - IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private startTimer()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mFisrtConnect:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$1;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_0
    return-void
.end method

.method private stopTimer()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mTimer:Ljava/util/Timer;

    :cond_0
    return-void
.end method

.method private writeFile([B)V
    .locals 5

    new-instance v1, Ljava/io/File;

    const-string v0, "/mnt/sdcard/WatchTest/Watch.fit"

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    const-string v0, "/mnt/sdcard/WatchTest"

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    :cond_1
    new-instance v1, Ljava/io/FileOutputStream;

    const-string v2, "/mnt/sdcard/WatchTest/Watch.fit"

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "fos.close() - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_1
    :try_start_3
    const-string v1, "[HealthSensor]WATCHSensor"

    const-string/jumbo v2, "writeFile - RuntimeException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "fos.close() - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_3
    move-exception v1

    :goto_2
    :try_start_5
    const-string v1, "[HealthSensor]WATCHSensor"

    const-string/jumbo v2, "writeFile - Exception"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v0, :cond_2

    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "fos.close() - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_3
    if-eqz v1, :cond_3

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :cond_3
    :goto_4
    throw v0

    :catch_5
    move-exception v1

    const-string v1, "[HealthSensor]WATCHSensor"

    const-string v2, "fos.close() - IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_4
    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "FileLoadErrer"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    :catch_6
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :catch_7
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public StartSearchWatch(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;)V
    .locals 2

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "StartSearchWatch"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->watchnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->requestAccess()V

    return-void
.end method

.method public StopSearchWatch()V
    .locals 2

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "StopSearchWatch"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->watchnotilisten:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$WatchNotiListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->unregisterDevice()V

    return-void
.end method

.method public getAntDeviceType()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 1

    const/16 v0, 0x7d2

    return v0
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected requestAccess()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$2;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$3;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    invoke-static {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->requestDeviceListAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void
.end method

.method public requestDownloadActivities(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;Z)Z
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->listData:Ljava/util/ArrayList;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mdata:Landroid/os/Bundle;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v1

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;

    invoke-direct {v2, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$5;

    invoke-direct {v3, p0, p2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$5;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;Z)V

    new-instance v4, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$6;

    invoke-direct {v4, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$6;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->requestDownloadAllActivities(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v1

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$7;

    invoke-direct {v2, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$7;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;

    invoke-direct {v3, p0, p2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$8;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;Z)V

    new-instance v4, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$9;

    invoke-direct {v4, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$9;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->requestDownloadNewActivities(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z

    move-result v0

    goto :goto_0
.end method

.method public requestWATCHData(Ljava/lang/String;IILjava/lang/String;Z)Z
    .locals 3

    const-string v0, "[HealthSensor]WATCHSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "requestWATCHData - DisplayName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", AntfsDeviceType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", AntfsManufacturerId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", UUIDtoString:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    new-instance v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-direct {v1, v0, p3, p2, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;-><init>(Ljava/util/UUID;IILjava/lang/String;)V

    invoke-virtual {p0, v1, p5}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->requestDownloadActivities(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;Z)Z

    const/4 v0, 0x1

    return v0
.end method

.method public requestWATCHData(Ljava/lang/String;Z)Z
    .locals 6

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->getAntDeviceType()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v2, v1, v3}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->convertANTname(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->UUIDtoString:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    new-instance v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsManufacturerId:[I

    aget v3, v3, v0

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->AntfsDeviceType:[I

    aget v4, v4, v0

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->DisplayName:[Ljava/lang/String;

    aget-object v0, v5, v0

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;-><init>(Ljava/util/UUID;IILjava/lang/String;)V

    invoke-virtual {p0, v2, p2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->requestDownloadActivities(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;Z)Z

    move-result v1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    invoke-direct {v0, p2, p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;-><init>(Landroid/os/Looper;Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->requestWATCHData(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyOnDataStarted(I)V

    :cond_0
    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public unregisterDevice()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->stopTimer()V

    invoke-super {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->unregisterDevice()V

    return-void
.end method
