.class Lcom/sec/android/service/health/sensor/manager/SAPManager$1DelayThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/manager/SAPManager;->onOpenSessionAccepted(JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DelayThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/SAPManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/manager/SAPManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$1DelayThread;->this$0:Lcom/sec/android/service/health/sensor/manager/SAPManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$1DelayThread;->this$0:Lcom/sec/android/service/health/sensor/manager/SAPManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->access$000(Lcom/sec/android/service/health/sensor/manager/SAPManager;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onOpenSessionAccepted Thread sleep for security check time"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x258

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$1DelayThread;->this$0:Lcom/sec/android/service/health/sensor/manager/SAPManager;

    # getter for: Lcom/sec/android/service/health/sensor/manager/SAPManager;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/SAPManager;->access$000(Lcom/sec/android/service/health/sensor/manager/SAPManager;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onOpenSessionAccepted Thread wake up after security check time"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
