.class public Lcom/sec/android/service/health/cp/database/validator/DataParser;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/database/validator/DataParser$1;,
        Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;,
        Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;
    }
.end annotation


# static fields
.field private static final ATTR_CODE:Ljava/lang/String; = "code"

.field private static final ATTR_DEFAULT:Ljava/lang/String; = "default"

.field private static final ATTR_GREATER_THAN:Ljava/lang/String; = "greaterthan"

.field private static final ATTR_MANDATORY:Ljava/lang/String; = "mandatory"

.field private static final ATTR_MAX:Ljava/lang/String; = "max"

.field private static final ATTR_MIN:Ljava/lang/String; = "min"

.field private static final ATTR_NAME:Ljava/lang/String; = "name"

.field private static final ATTR_SUM:Ljava/lang/String; = "sum"

.field private static final ATTR_TYPE:Ljava/lang/String; = "type"

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_CONSTANT:Ljava/lang/String; = "constant"

.field private static final TAG_CONSTANTS:Ljava/lang/String; = "constants"

.field private static final TAG_DATA:Ljava/lang/String; = "data"

.field private static final TAG_DATA_TYPE:Ljava/lang/String; = "data-type"

.field private static final TAG_DESCRIPTION:Ljava/lang/String; = "description"

.field private static final TAG_PROPERTY:Ljava/lang/String; = "property"

.field private static final TAG_SHEALTH:Ljava/lang/String; = "SHealth"

.field private static final TAG_VALIDATION:Ljava/lang/String; = "validation"

.field private static final VALUE_TYPE_BOOLEAN:Ljava/lang/String; = "boolean"

.field private static final VALUE_TYPE_CODE:Ljava/lang/String; = "code"

.field private static final VALUE_TYPE_NUMBER:Ljava/lang/String; = "number"

.field private static final VALUE_TYPE_STRING:Ljava/lang/String; = "string"

.field private static dataParser:Lcom/sec/android/service/health/cp/database/validator/DataParser;


# instance fields
.field private mConstants:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/database/validator/FieldSpec;",
            ">;>;"
        }
    .end annotation
.end field

.field private mIsInitialized:Z

.field private mNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;",
            ">;"
        }
    .end annotation
.end field

.field private mSharedPrefMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/database/validator/FieldSpec;",
            ">;"
        }
    .end annotation
.end field

.field private mTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;",
            ">;"
        }
    .end annotation
.end field

.field private mXmlParser:Lorg/xmlpull/v1/XmlPullParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mIsInitialized:Z

    return-void
.end method

.method private checkRangeOfColumn(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/validator/FieldSpec;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 8

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$1;->$SwitchMap$com$sec$android$service$health$cp$database$validator$DataParser$TypeSet:[I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    iget-object v2, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->type:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;

    move-object v0, p0

    move-object v1, p4

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkRuleOfColomn(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Lcom/sec/android/service/health/cp/database/validator/FieldSpec;Landroid/content/ContentValues;DLcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not registered as a type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-double v5, v0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;

    move-object v0, p0

    move-object v1, p4

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkRuleOfColomn(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Lcom/sec/android/service/health/cp/database/validator/FieldSpec;Landroid/content/ContentValues;DLcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " is not boolean value. yours : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p4, p1, v0, v1}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->correctionValue:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->correctionValue:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is corrected as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->correctionValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, p3}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is corrected as NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->constant:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mConstants:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->constant:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;->fields:Ljava/util/HashSet;

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " is not a value of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->constant:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". yours : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p3, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p4, p1, v0, v1}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mConstants:Ljava/util/HashMap;

    iget-object v2, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->constant:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;

    iget v0, v0, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;->defaultValue:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is corrected as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mConstants:Ljava/util/HashMap;

    iget-object v2, p2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->constant:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;

    iget v0, v0, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;->defaultValue:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p3}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private checkRuleOfColomn(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Lcom/sec/android/service/health/cp/database/validator/FieldSpec;Landroid/content/ContentValues;DLcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;)V
    .locals 8

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->min:Ljava/lang/Double;

    if-eqz v1, :cond_0

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->min:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    cmpl-double v1, v1, p5

    if-lez v1, :cond_0

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " cannot be smaller than "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v3, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->min:Ljava/lang/Double;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". yours : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5, p6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$1;->$SwitchMap$com$sec$android$service$health$cp$database$validator$DataParser$TypeSet:[I

    invoke-virtual {p7}, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    if-eqz v1, :cond_1

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    cmpg-double v1, v1, p5

    if-gez v1, :cond_1

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " cannot be bigger than "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v3, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". yours : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5, p6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser$1;->$SwitchMap$com$sec$android$service$health$cp$database$validator$DataParser$TypeSet:[I

    invoke-virtual {p7}, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :cond_1
    :goto_1
    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->sum:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    move v1, v0

    move-wide v2, v4

    :goto_2
    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->sum:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->sum:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->sum:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    add-double/2addr v2, v6

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :pswitch_0
    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->min:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p5

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->min:Ljava/lang/Double;

    invoke-virtual {p4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is corrected as "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->min:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p5

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {p4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is corrected as "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p5

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p4, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v3, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->intValue()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is corrected as "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p4, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v3, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->intValue()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V

    goto/16 :goto_1

    :cond_3
    cmpl-double v0, v2, p5

    if-eqz v0, :cond_4

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " is not matched with sum of "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v6, v6, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->sum:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ". yours : "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is corrected as the sum value : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V

    move-wide p5, v2

    :cond_4
    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpg-double v0, p5, v0

    if-gez v0, :cond_5

    cmpl-double v0, p5, v4

    if-eqz v0, :cond_5

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " cannot be greater than "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {p4, v2}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). yours : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {p4, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {p4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is corrected as "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {p4, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is corrected as "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V

    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private checkSyncStatus(Landroid/content/ContentValues;)V
    .locals 2

    const-string/jumbo v0, "sync_status"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "sync_status"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, 0x29811

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "sync_status"

    const v1, 0x29813

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method private generateHdid([B)Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    add-int/lit16 v2, v2, 0x100

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_1
    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getInstance()Lcom/sec/android/service/health/cp/database/validator/DataParser;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->dataParser:Lcom/sec/android/service/health/cp/database/validator/DataParser;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->dataParser:Lcom/sec/android/service/health/cp/database/validator/DataParser;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->dataParser:Lcom/sec/android/service/health/cp/database/validator/DataParser;

    return-object v0
.end method

.method private getNewHdid(B)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->generateHdid([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNewHdid([B)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->generateHdid([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseXML(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    move-object v1, v7

    move-object v2, v7

    move-object v3, v7

    move-object v5, v7

    move-object v6, v7

    :goto_0
    if-eq v0, v9, :cond_7

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mData:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mConstants:Ljava/util/HashMap;

    goto :goto_1

    :pswitch_2
    :try_start_0
    sget-object v4, Lcom/sec/android/service/health/cp/database/validator/DataParser$1;->$SwitchMap$com$sec$android$service$health$cp$database$validator$DataParser$NameSet:[I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_1

    :cond_1
    move-object v0, v1

    move-object v4, v6

    move-object v1, v2

    move-object v2, v3

    move-object v3, v5

    :goto_2
    move-object v5, v3

    move-object v6, v4

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_1

    :pswitch_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    :try_start_1
    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    move-object v2, v3

    move-object v3, v5

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_2

    :pswitch_4
    :try_start_2
    new-instance v4, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;

    invoke-direct {v4}, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;-><init>()V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v0, 0x0

    :try_start_3
    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v6

    goto :goto_2

    :pswitch_5
    if-eqz v6, :cond_3

    :try_start_4
    new-instance v4, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;

    invoke-direct {v4}, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;-><init>()V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1

    move v3, v8

    :goto_3
    :try_start_5
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v0

    if-ge v3, v0, :cond_2

    sget-object v10, Lcom/sec/android/service/health/cp/database/validator/DataParser$1;->$SwitchMap$com$sec$android$service$health$cp$database$validator$DataParser$NameSet:[I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    invoke-interface {p1, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_2

    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :pswitch_6
    invoke-interface {p1, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v3, v4

    :goto_5
    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "START_TAG is null : "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :pswitch_7
    :try_start_6
    invoke-interface {p1, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->type:Ljava/lang/String;

    goto :goto_4

    :pswitch_8
    invoke-interface {p1, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->mandatory:Ljava/lang/String;

    goto :goto_4

    :cond_2
    iget-object v0, v4, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->type:Ljava/lang/String;

    if-nez v0, :cond_a

    new-instance v0, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v4, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, "\'s type field should not be null"

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_3
    if-eqz v5, :cond_1

    move v0, v8

    :goto_6
    :try_start_7
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v4

    if-ge v0, v4, :cond_9

    const-string v4, "default"

    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    move v0, v9

    :goto_7
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v0, :cond_4

    iput v4, v5, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;->defaultValue:I

    :cond_4
    iget-object v0, v5, Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;->fields:Ljava/util/HashSet;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v4, v6

    move-object v1, v2

    move-object v2, v3

    move-object v3, v5

    goto/16 :goto_2

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :pswitch_9
    move v4, v8

    :goto_8
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v0

    if-ge v4, v0, :cond_6

    sget-object v10, Lcom/sec/android/service/health/cp/database/validator/DataParser$1;->$SwitchMap$com$sec$android$service$health$cp$database$validator$DataParser$NameSet:[I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    invoke-interface {p1, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_3

    :goto_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_8

    :pswitch_a
    iget-object v0, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    invoke-interface {p1, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    iput-object v10, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->min:Ljava/lang/Double;

    goto :goto_9

    :catch_1
    move-exception v0

    goto/16 :goto_5

    :pswitch_b
    iget-object v0, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    invoke-interface {p1, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    iput-object v10, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->max:Ljava/lang/Double;

    goto :goto_9

    :pswitch_c
    iget-object v0, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    invoke-interface {p1, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, ","

    invoke-static {v10, v11}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->splitDataList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    iput-object v10, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->sum:Ljava/util/ArrayList;

    goto :goto_9

    :pswitch_d
    iget-object v0, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    invoke-interface {p1, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->greaterthan:Ljava/lang/String;

    goto :goto_9

    :pswitch_e
    iget-object v0, v3, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    invoke-interface {p1, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->constant:Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_9

    :cond_6
    move-object v0, v1

    move-object v4, v6

    move-object v1, v2

    move-object v2, v3

    move-object v3, v5

    goto/16 :goto_2

    :pswitch_f
    :try_start_8
    sget-object v4, Lcom/sec/android/service/health/cp/database/validator/DataParser$1;->$SwitchMap$com$sec$android$service$health$cp$database$validator$DataParser$NameSet:[I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_1

    :pswitch_10
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mData:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v7

    move-object v6, v7

    goto/16 :goto_1

    :pswitch_11
    if-eqz v6, :cond_0

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v3, v7

    goto/16 :goto_1

    :pswitch_12
    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mConstants:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_2

    move-object v1, v7

    move-object v5, v7

    goto/16 :goto_1

    :catch_2
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "END_TAG null : "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mSharedPrefMap:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mData:Ljava/util/HashMap;

    const-string/jumbo v1, "shared_pref"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move v2, v8

    :goto_a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_8

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mSharedPrefMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_a

    :cond_8
    return-void

    :catch_3
    move-exception v0

    move-object v6, v4

    goto/16 :goto_5

    :catch_4
    move-exception v0

    move-object v5, v4

    goto/16 :goto_5

    :cond_9
    move v0, v8

    goto/16 :goto_7

    :cond_a
    move-object v0, v1

    move-object v3, v5

    move-object v1, v2

    move-object v2, v4

    move-object v4, v6

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x9
        :pswitch_10
        :pswitch_12
        :pswitch_11
    .end packed-switch
.end method

.method private static splitDataList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/StringTokenizer;

    invoke-direct {v1, p0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    const-string v0, "hdid"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->reportValidationError(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoLogging()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoThrowingException()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-boolean v1, Landroid/util/secutil/LogSwitcher;->isShowingGlobalLog:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_2
    new-instance v1, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;

    invoke-direct {v1, p2, p3, v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to report invalid data due to exception occurred : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method public checkValidationAndCorrection(Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 5

    const-string/jumbo v0, "shared_pref"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mSharedPrefMap:Ljava/util/HashMap;

    const-string v1, "key"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "key"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "value"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1, p4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkRangeOfColumn(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/validator/FieldSpec;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mData:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;

    iget-object v3, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_4

    :cond_2
    iget-object v3, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->mandatory:Ljava/lang/String;

    if-eqz v3, :cond_4

    const-string/jumbo v3, "yes"

    iget-object v4, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->mandatory:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->INSERT:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    if-eq p3, v3, :cond_3

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->REPLACE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    if-ne p3, v3, :cond_4

    :cond_3
    iget-object v3, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    const-string v4, " is not nullable"

    invoke-direct {p0, p4, p1, v3, v4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->writeLogAndthrowException(Lcom/sec/android/service/health/cp/database/ValidationPolicy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->isDoCorretion()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "hdid"

    iget-object v4, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string/jumbo v3, "user_device"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "hdid"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->getNewHdid([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, p2}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkSyncStatus(Landroid/content/ContentValues;)V

    :cond_4
    iget-object v3, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->validationRule:Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/sec/android/service/health/cp/database/validator/FieldSpec;->name:Ljava/lang/String;

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-direct {p0, p1, v1, p2, p4}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->checkRangeOfColumn(Ljava/lang/String;Lcom/sec/android/service/health/cp/database/validator/FieldSpec;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    :cond_6
    const-string v3, "_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "hdid"

    invoke-virtual {v3}, Ljava/lang/Long;->byteValue()B

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->getNewHdid(B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getConstantsSpec()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/database/validator/ConstantSpec;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mConstants:Ljava/util/HashMap;

    return-object v0
.end method

.method public getDataSpec()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/database/validator/FieldSpec;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mData:Ljava/util/HashMap;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mIsInitialized:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mIsInitialized:Z

    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->TAG:Ljava/lang/String;

    const-string v2, "initialize DataParser"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "SHealth"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->SHEALTH:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "data"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DATA:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "data-type"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DATA_TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "constants"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CONSTANTS:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "constant"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CONSTANT:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "property"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->PROPERTY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "validation"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->VALIDATION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "description"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->DESCRIPTION:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "name"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->NAME:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "type"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->TYPE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "mandatory"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MANDATORY:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "min"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MIN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "max"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->MAX:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string/jumbo v2, "sum"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->SUM:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "greaterthan"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->GREATER_THAN:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mNameMap:Ljava/util/HashMap;

    const-string v2, "code"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;->CODE:Lcom/sec/android/service/health/cp/database/validator/DataParser$NameSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "number"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;->NUMBER:Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    const-string/jumbo v2, "string"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;->STRING:Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    const-string v2, "boolean"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;->BOOLEAN:Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mTypeMap:Ljava/util/HashMap;

    const-string v2, "code"

    sget-object v3, Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;->CODE:Lcom/sec/android/service/health/cp/database/validator/DataParser$TypeSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v2, "data_spec.xml"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mXmlParser:Lorg/xmlpull/v1/XmlPullParser;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mXmlParser:Lorg/xmlpull/v1/XmlPullParser;

    const-string v2, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mXmlParser:Lorg/xmlpull/v1/XmlPullParser;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/DataParser;->mXmlParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->parseXML(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_2
    :goto_1
    throw v0

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
