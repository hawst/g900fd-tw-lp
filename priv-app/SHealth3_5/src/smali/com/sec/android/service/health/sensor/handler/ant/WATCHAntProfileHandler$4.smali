.class Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->requestDownloadActivities(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewDownloadActivitiesFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "[HealthSensor]WATCHSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDownloadHistoryFinished arg0:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$13;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$AntFsRequestStatus:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->listData:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->listData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyResponseReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "DownloadAllHistory finished successfully."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyOnDataStopped(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :pswitch_1
    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "DownloadAllHistory failed, device busy."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "DownloadAllHistory failed, communication error."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_3
    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "DownloadAllHistory failed, authentication rejected."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_4
    const-string v0, "[HealthSensor]WATCHSensor"

    const-string v1, "DownloadAllHistory failed, transmission lost."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler$4;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WATCHAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
