.class public abstract Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$4;,
        Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    }
.end annotation


# static fields
.field protected static final MSG_REQ_PCC_END:I = 0x3e9

.field protected static final MSG_REQ_PCC_START:I = 0x3e8

.field protected static final MSG_REQ_RESP:I = 0x3ea

.field protected static final NOTI_ALL:Z = false

.field private static final PCC_CONNECT_TYPE:I = 0x3

.field protected static final STATE_CONNECTED:I = 0x3

.field protected static final STATE_CONNECTING:I = 0x2

.field protected static final STATE_DISCONNECTED:I = 0x1

.field protected static final STATE_DISCONNECTING:I = 0x4

.field protected static final STATE_PROCESSING:I = 0x5

.field protected static final STATE_WAIT_DISCONNECT:I = 0x6

.field protected static final TAG:Ljava/lang/String; = "[HealthSensor]AntProfileHandler"


# instance fields
.field protected device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private lost_state:Z

.field protected mAntDeviceID:I

.field protected mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

.field protected mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

.field protected mAntPluginSubPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

.field protected mConnectState:I

.field protected mContext:Landroid/content/Context;

.field protected final mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

.field protected mFisrtConnect:Z

.field protected mLooper:Landroid/os/Looper;

.field protected volatile mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

.field protected mSensorInfo:Lcom/sec/android/service/health/sensor/data/SensorInfo;

.field protected mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

.field protected profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field protected send_flag:Z

.field protected state:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginSubPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntDeviceID:I

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mConnectState:I

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mFisrtConnect:Z

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->send_flag:Z

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->lost_state:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->stopDevice()V

    return-void
.end method

.method private stopDevice()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntDeviceID:I

    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mSensorInfo:Lcom/sec/android/service/health/sensor/data/SensorInfo;

    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginSubPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginSubPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->releaseAccess()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginSubPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->releaseAccess()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iput-object v4, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HealthSensor]AntProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error releaseAccess() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "[HealthSensor]AntProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error releaseAccess() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deinitialize "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getAntDeviceType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntDeviceID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public abstract getAntDeviceType()I
.end method

.method public getConnectState()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mConnectState:I

    return v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public abstract getSubState()I
.end method

.method public getUID()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getAntDeviceType()I

    move-result v0

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntDeviceID:I

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->getUID(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract handleMessageSub(Landroid/os/Message;)V
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 4

    const/4 v0, 0x0

    if-nez p4, :cond_0

    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string v1, "data is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v1, "[HealthSensor]AntProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initiallize "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mContext:Landroid/content/Context;

    check-cast p4, Lcom/sec/android/service/health/sensor/manager/AntManager;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/manager/AntManager;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;-><init>(Landroid/os/Looper;Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mConnectState:I

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->getDeviceType(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->getDeviceID(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntDeviceID:I

    new-instance v1, Lcom/sec/android/service/health/sensor/data/SensorInfo;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getUID()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/service/health/sensor/data/SensorInfo;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mSensorInfo:Lcom/sec/android/service/health/sensor/data/SensorInfo;

    new-instance v1, Lcom/sec/android/service/health/sensor/data/UserProfileData;

    invoke-direct {v1}, Lcom/sec/android/service/health/sensor/data/UserProfileData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-eqz v1, :cond_1

    const-string v1, "[HealthSensor]AntProfileHandler"

    const-string v2, "mAntPluginPcc before releaseAccess()"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->releaseAccess()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const-string v1, "[HealthSensor]AntProfileHandler"

    const-string v2, "mAntPluginPcc after releaseAccess()"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_1
    const-string v1, "[HealthSensor]AntProfileHandler"

    const-string v2, "mAntPluginPcc null"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public notifyChannelStateChanged()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getConnectState()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setConnectedState(Z)V

    :cond_0
    :goto_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mFisrtConnect:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mFisrtConnect:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/AntManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setConnectedState(Z)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->lost_state:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->lost_state:Z

    const/16 v0, 0x7d6

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyStateChanged(I)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getSubState()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyStateChanged(I)V

    goto :goto_1

    :cond_5
    if-ne v0, v3, :cond_6

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mFisrtConnect:Z

    if-nez v0, :cond_1

    const/16 v0, 0x7d5

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyStateChanged(I)V

    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->lost_state:Z

    goto :goto_1

    :cond_6
    const/16 v1, 0x12c

    if-ne v0, v1, :cond_7

    const/16 v0, 0x7d2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyStateChanged(I)V

    goto :goto_1

    :cond_7
    const/16 v0, 0x7d7

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyStateChanged(I)V

    goto :goto_1
.end method

.method public notifyHealthServiceError(I)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_1

    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthServiceError code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deviceID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string/jumbo v0, "notifyHealthServiceError"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setEvent(I)V

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setResponse(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHealthServiceError code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " device is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public notifyOnDataStarted(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getAntDeviceType()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->antDataTypeConvert(I)I

    move-result v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->send_flag:Z

    return-void
.end method

.method public notifyOnDataStopped(I)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->send_flag:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getAntDeviceType()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->antDataTypeConvert(I)I

    move-result v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_0
    return-void
.end method

.method public notifyOnJoinedError(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntManager:Lcom/sec/android/service/health/sensor/manager/AntManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AntManager;->removeDeviceInfoForJoinFailed(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_0
    return-void
.end method

.method public notifyResponse(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    return-void
.end method

.method public notifyResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->send_flag:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public notifyResponseReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->send_flag:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public notifyStateChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->profileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onStateChanged(I)V

    :cond_0
    return-void
.end method

.method public registerDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    .locals 4

    const/4 v3, 0x6

    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "registerDevice "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getAntDeviceType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    if-eq v0, v3, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mConnectState:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntDeviceID:I

    new-instance v0, Lcom/sec/android/service/health/sensor/data/SensorInfo;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/data/SensorInfo;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mSensorInfo:Lcom/sec/android/service/health/sensor/data/SensorInfo;

    new-instance v0, Lcom/sec/android/service/health/sensor/data/UserProfileData;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/data/UserProfileData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-eqz v0, :cond_1

    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string v1, "mAntPluginPcc before releaseAccess()"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->releaseAccess()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string v1, "mAntPluginPcc after releaseAccess()"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string v1, "mAntPluginPcc null"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    if-ne v0, v3, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    goto :goto_1
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/16 v0, 0x3ea

    return v0
.end method

.method protected abstract requestAccess()V
.end method

.method public sendRequest(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    .locals 0

    return-void
.end method

.method public setConnectState(I)Z
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mConnectState:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mConnectState:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyOnDataStarted(I)V

    return v0
.end method

.method public stopReceivingData()I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyOnDataStopped(I)V

    return v0
.end method

.method public subscribeCommonToEvents()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    instance-of v0, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string/jumbo v1, "subscribeCommonToEvents()"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$2;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->subscribeManufacturerIdentificationEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IManufacturerIdentificationReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;-><init>(Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->subscribeProductInformationEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;)V

    goto :goto_0
.end method

.method public unregisterDevice()V
    .locals 3

    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregistDevice "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->getAntDeviceType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntDeviceID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->state:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
