.class Lcom/sec/android/service/health/HealthService$1;
.super Lcom/samsung/android/health/IHealth$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/HealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/HealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/HealthService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/HealthService$1;->this$0:Lcom/sec/android/service/health/HealthService;

    invoke-direct {p0}, Lcom/samsung/android/health/IHealth$Stub;-><init>()V

    return-void
.end method

.method private getCallingPackageName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/HealthService$1;->this$0:Lcom/sec/android/service/health/HealthService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/HealthService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public isHealthDataPermissionAcquired(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "Invalid argument"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/HealthService$1;->getCallingPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    # invokes: Lcom/sec/android/service/health/HealthService;->checkValidCaller(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/sec/android/service/health/HealthService;->access$000(Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/sec/android/service/health/PermissionRequestAdapter;->processIsPermissionAcquired(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public readData(Lcom/samsung/android/internal/ReadRequestImpl;)Lcom/samsung/android/health/HealthDataResolver$ReadResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "Invalid argument"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/HealthService$1;->getCallingPackageName()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/sec/android/service/health/HealthService;->checkValidCaller(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/sec/android/service/health/HealthService;->access$000(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v1, p0, Lcom/sec/android/service/health/HealthService$1;->this$0:Lcom/sec/android/service/health/HealthService;

    invoke-static {v1, v0, p1}, Lcom/sec/android/service/health/DataRequestAdapter;->processReadRequest(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/internal/ReadRequestImpl;)Lcom/samsung/android/health/HealthDataResolver$ReadResult;

    move-result-object v0

    return-object v0
.end method

.method public registerDataObserver(Ljava/lang/String;Lcom/samsung/android/health/IHealthDataObserver;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "Empty data type"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/HealthService$1;->getCallingPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v1, p0, Lcom/sec/android/service/health/HealthService$1;->this$0:Lcom/sec/android/service/health/HealthService;

    invoke-static {v1, v0, p1, p2}, Lcom/sec/android/service/health/DataRequestAdapter;->registerDataObserver(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/health/IHealthDataObserver;)V

    return-void
.end method

.method public requestHealthDataPermissions(Landroid/os/Bundle;)Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "Invalid argument"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/HealthService$1;->getCallingPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    # invokes: Lcom/sec/android/service/health/HealthService;->checkValidCaller(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/sec/android/service/health/HealthService;->access$000(Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/sec/android/service/health/PermissionRequestAdapter;->processPermissionRequest(Ljava/lang/String;Landroid/os/Bundle;)Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;

    move-result-object v0

    return-object v0
.end method

.method public unregisterDataObserver(Lcom/samsung/android/health/IHealthDataObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/sec/android/service/health/HealthService$1;->this$0:Lcom/sec/android/service/health/HealthService;

    invoke-static {v0, p1}, Lcom/sec/android/service/health/DataRequestAdapter;->unregisterDataObserver(Landroid/content/Context;Lcom/samsung/android/health/IHealthDataObserver;)V

    return-void
.end method
