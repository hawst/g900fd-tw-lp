.class Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;->STATE_CANCELLED:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    # setter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationState:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$002(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;)Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    # getter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$200(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    # getter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$200(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onCancelled()V

    :cond_0
    return-void
.end method

.method public onFinished(Ljava/lang/Integer;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;->STATE_FINISHED:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    # setter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationState:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$002(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;)Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationErrorCode:I
    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$102(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;I)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    # getter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$200(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    # getter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$200(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onFinished(Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$202(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    :cond_0
    return-void
.end method

.method public onProgressUpdate(Ljava/lang/Integer;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;->STATE_ONGOING:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    # setter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationState:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$002(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;)Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$MigrationState;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mMigrationErrorCode:I
    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$102(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;I)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    # getter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$200(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager$1;->this$0:Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    # getter for: Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->mCurrentListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->access$200(Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;)Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;->onProgressUpdate(Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method
