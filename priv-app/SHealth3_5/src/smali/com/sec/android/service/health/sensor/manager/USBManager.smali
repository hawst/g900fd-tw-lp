.class public Lcom/sec/android/service/health/sensor/manager/USBManager;
.super Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/manager/USBManager$1;,
        Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;,
        Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;,
        Lcom/sec/android/service/health/sensor/manager/USBManager$UsbHandlerListener;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "[HealthSensor]USBManager"

.field private static mUSBManager:Lcom/sec/android/service/health/sensor/manager/USBManager;

.field private static mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private static tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private static usbProfileName:Ljava/lang/String;


# instance fields
.field private filter:Landroid/content/IntentFilter;

.field private handler:Landroid/os/Handler;

.field private isUsbDisconnected:Z

.field private mContext:Landroid/content/Context;

.field private mDevice:Landroid/hardware/usb/UsbDevice;

.field private mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

.field private mProtocolIndex:I

.field private mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

.field private mShealthProtocolInterfaceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;",
            ">;"
        }
    .end annotation
.end field

.field mUSBProfileHandlerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;",
            ">;"
        }
    .end annotation
.end field

.field private mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

.field private mUsbProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/USBManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBManager:Lcom/sec/android/service/health/sensor/manager/USBManager;

    sput-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    sput-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    sput-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->isUsbDisconnected:Z

    iput v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mShealthProtocolInterfaceList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBProfileHandlerList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/manager/USBManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbDisconnected()V

    return-void
.end method

.method static synthetic access$1000()Lcom/sec/android/service/health/sensor/manager/USBManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBManager:Lcom/sec/android/service/health/sensor/manager/USBManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/manager/USBManager;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mShealthProtocolInterfaceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/manager/USBManager;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/service/health/sensor/manager/USBManager;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I

    return p1
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/manager/USBManager;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/manager/USBManager;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->setUsbProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/sensor/manager/USBManager;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/manager/USBManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBManager:Lcom/sec/android/service/health/sensor/manager/USBManager;

    return-object v0
.end method

.method private getUsbProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-object v0
.end method

.method private printDeviceInfo(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[HealthSensor]USBManager"

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private setUsbProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-void
.end method

.method private usbDisconnected()V
    .locals 2

    const/16 v0, 0x7d7

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onStateChanged(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->deinitialize()V

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->removeProfileHandlerController(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->deinitialize()V

    return-void
.end method


# virtual methods
.method public createAndAddProfileHandlerController(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;)V
    .locals 3

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->getUsbProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V

    invoke-interface {p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->removeProfileHandlerController(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    :cond_0
    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "=============createAndAddProfileHandlerController=============="

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBManager:Lcom/sec/android/service/health/sensor/manager/USBManager;

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;Lcom/sec/android/service/health/sensor/manager/AbstractSensorManagerListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->initiallize(Landroid/content/Context;Ljava/lang/Object;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->addProfileHandlerController(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->printDeviceInfo(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    return-void
.end method

.method public create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    instance-of v0, p1, Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    check-cast p1, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setId(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public deinitialize()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "UsbManager: deinitialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->deinitialize()V

    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->isUsbDisconnected:Z

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProfileHandler:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProfileController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    sput-object v2, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;

    sput-object v2, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    sput-object v2, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mDevice:Landroid/hardware/usb/UsbDevice;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    iput v3, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mProtocolIndex:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->isRegistered()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->setRegistered(Z)V

    :cond_1
    return-void
.end method

.method public getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "[HealthSensor]USBManager"

    const-string v2, "getConnectedDevices"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "[HealthSensor]USBManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getConnectedDevices usb device found ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;->getNativeEventManager()Lcom/sec/android/service/health/sensor/manager/jni/NativeEventManager;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;-><init>(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/sec/android/service/health/sensor/manager/USBManager$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->filter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->setRegistered(Z)V

    return-void
.end method

.method public isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectedState()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataTypeSupported(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "[HealthSensor]USBManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "join() mUsbDevice = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setConnectedState(Z)V

    goto :goto_0
.end method

.method public leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto :goto_0
.end method

.method public final onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V
    .locals 2

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ping_response"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->getCommandId()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->tempUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    goto :goto_0

    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V

    goto :goto_0
.end method

.method protected remoteCallBackDied(Ljava/lang/Integer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->deinitialize()V

    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->request(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "ProfileHandlerController controller is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 8

    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "=============startReceivingData=============="

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->printDeviceInfo(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v4, p5

    move-wide v5, p6

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->startReceivingData(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "[HealthSensor]USBManager"

    const-string/jumbo v1, "startReceivingData() profileController is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startScanning(Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
    .locals 2

    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "USBManager startScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->isUsbDisconnected:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUsbDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->addSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onScanningStopped()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->onScanningStopped()V

    const/4 v0, 0x2

    goto :goto_0
.end method

.method public stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)I
    .locals 2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/USBManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "[HealthSensor]USBManager"

    const-string/jumbo v1, "stopReceivingData() profileController is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopScanning()Z
    .locals 2

    const-string v0, "[HealthSensor]USBManager"

    const-string v1, "USBManager stopScanning"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public usbConnected(Landroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->isRegistered()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;-><init>(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/sec/android/service/health/sensor/manager/USBManager$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mReceiver:Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/manager/USBManager$UsbConnectionEventReveiver;->setRegistered(Z)V

    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->isUsbDisconnected:Z

    sput-object v3, Lcom/sec/android/service/health/sensor/manager/USBManager;->usbProfileName:Ljava/lang/String;

    const-string v0, "[HealthSensor]USBManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "USB device connected device.getVendorId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " device.getProductId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    const/16 v1, 0x2047

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/16 v1, 0x30a

    if-eq v0, v1, :cond_2

    :cond_1
    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    const/16 v1, 0x67b

    if-ne v0, v1, :cond_3

    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/16 v1, 0x2303

    if-ne v0, v1, :cond_3

    :cond_2
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mDevice:Landroid/hardware/usb/UsbDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getProfileHandlerList(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mUSBProfileHandlerList:Ljava/util/ArrayList;

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->getInstance()Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->getProtocolByConnectivityType(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->mShealthProtocolInterfaceList:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/service/health/sensor/manager/USBManager$USBManagerHandler;-><init>(Lcom/sec/android/service/health/sensor/manager/USBManager;Lcom/sec/android/service/health/sensor/manager/USBManager$1;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->handler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/USBManager;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    return-void
.end method
