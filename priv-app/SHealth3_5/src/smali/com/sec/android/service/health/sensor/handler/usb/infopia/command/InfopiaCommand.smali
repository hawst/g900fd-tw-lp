.class public interface abstract Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand$Id;
    }
.end annotation


# static fields
.field public static final ERS:[B

.field public static final GDT:[B

.field public static final PDT:[B

.field public static final S:[B

.field public static final T:[B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    new-array v0, v4, [B

    const/16 v1, 0x53

    aput-byte v1, v0, v3

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->S:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->ERS:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->PDT:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->GDT:[B

    new-array v0, v4, [B

    const/16 v1, 0x54

    aput-byte v1, v0, v3

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->T:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x45t
        0x52t
        0x53t
    .end array-data

    :array_1
    .array-data 1
        0x50t
        0x44t
        0x54t
    .end array-data

    :array_2
    .array-data 1
        0x47t
        0x44t
        0x54t
    .end array-data
.end method


# virtual methods
.method public abstract doAction(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
.end method
