.class Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$1;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v7, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->DATA_RECEIVER_ACTION:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SWeightHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->DATA_RECEIVER_ACTION:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->KEY_WEIGHT_DATA:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    if-eqz v2, :cond_3

    aget-byte v0, v2, v3

    if-gez v0, :cond_1

    aget-byte v0, v2, v3

    add-int/lit16 v0, v0, 0x100

    :goto_0
    mul-int/lit16 v3, v0, 0x100

    aget-byte v0, v2, v4

    if-gez v0, :cond_2

    aget-byte v0, v2, v4

    add-int/lit16 v0, v0, 0x100

    :goto_1
    add-int/2addr v0, v3

    int-to-double v3, v0

    const-wide/high16 v5, 0x4024000000000000L    # 10.0

    div-double/2addr v3, v5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;

    double-to-float v3, v3

    aget-byte v2, v2, v7

    # invokes: Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->sendMeasuredDataReport(Landroid/text/format/Time;FI)V
    invoke-static {v0, v1, v3, v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;Landroid/text/format/Time;FI)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    aget-byte v0, v2, v3

    goto :goto_0

    :cond_2
    aget-byte v0, v2, v4

    goto :goto_1

    :cond_3
    const-string v0, "[HealthSensor]SWeightHandler"

    const-string v1, "getByteArrayExtra returned null - buffer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v7, v7}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    goto :goto_2
.end method
