.class Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CommnunicationThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "RFComHandler run -> Communication Thread Started"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyStart()I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->runCommunicationThread:Z
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    const v0, 0x186a0

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    new-array v2, v1, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyRawDataReceived([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "RFComHandler run -> Communication Thread Error While Notifing the recived Data"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v5

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mTAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "RFComHandler run -> Communication Thread Ending"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # invokes: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->shutdownCommunicationThread()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)V

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mInStream:Ljava/io/InputStream;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler$CommnunicationThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->mOutStream:Ljava/io/OutputStream;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComHandler;)Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
