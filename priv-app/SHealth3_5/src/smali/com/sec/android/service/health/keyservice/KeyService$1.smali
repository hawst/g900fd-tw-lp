.class final Lcom/sec/android/service/health/keyservice/KeyService$1;
.super Lcom/sec/android/service/health/keyservice/aidl/IKeyService$Stub;
.source "KeyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/keyservice/KeyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/service/health/keyservice/aidl/IKeyService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public ServiceKey(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "VALUE"    # Ljava/lang/String;
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 30
    const/4 v0, 0x0

    .line 31
    .local v0, "str":Ljava/lang/String;
    packed-switch p3, :pswitch_data_0

    .line 58
    :cond_0
    :goto_0
    return-object v0

    .line 33
    :pswitch_0
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 34
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 35
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$100()Z

    move-result v1

    if-eqz v1, :cond_1

    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$200()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KeyService, KeyManager KeyService set : it\'s already set : (X) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    :cond_1
    :goto_1
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KeyService, KeyManager KeyService set : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " // "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 37
    :cond_2
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "str":Ljava/lang/String;
    goto :goto_1

    .line 44
    :pswitch_1
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 45
    .restart local v0    # "str":Ljava/lang/String;
    goto :goto_0

    .line 47
    :pswitch_2
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$100()Z

    move-result v1

    if-eqz v1, :cond_3

    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KeyService, KeyManager KeyService reset : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " // "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_3
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 50
    .restart local v0    # "str":Ljava/lang/String;
    goto/16 :goto_0

    .line 52
    :pswitch_3
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$100()Z

    move-result v1

    if-eqz v1, :cond_4

    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KeyService, KeyManagerKeyService reset : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " // "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_4
    # getter for: Lcom/sec/android/service/health/keyservice/KeyService;->DB_Key:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/keyservice/KeyService;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "str":Ljava/lang/String;
    goto/16 :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
