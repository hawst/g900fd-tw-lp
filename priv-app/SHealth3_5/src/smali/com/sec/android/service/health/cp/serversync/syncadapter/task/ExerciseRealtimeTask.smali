.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseRealtimeTask;
.super Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;


# static fields
.field private static final REALTIME_CODE:Ljava/lang/String; = "realtime"


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;III)V
    .locals 0

    invoke-direct {p0, p1, p3, p4}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    return-void
.end method


# virtual methods
.method protected getDeletedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDetailHistoryData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseRealtimeTask;->getTableName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/service/health/cp/serversync/util/SyncExerciseUtils;->getRealtimeLocationDetailData(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getSyncHistory()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseRealtimeTask;->skipGetSyncHistory()V

    return-void
.end method

.method protected getSyncTypeString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "realtime"

    return-object v0
.end method

.method protected getTableName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "realtime_data"

    return-object v0
.end method

.method protected getUnsyncedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/SyncExerciseUtils;->getModifiedRealTime()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected updateItemsLocally(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    invoke-static {p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncExerciseUtils;->addModifyRealTime(Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method
