.class public Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$HServer;
.super Ljava/lang/Object;
.source "HttpConnectionConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HServer"
.end annotation


# static fields
.field public static HTTPS_PORT_NO_H:I = 0x0

.field public static final IS_HTTPS_ENABLED_H:Z = true

.field public static SERVER_URL_BASE_H:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "api.samsungosp.com"

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$HServer;->SERVER_URL_BASE_H:Ljava/lang/String;

    .line 18
    const/16 v0, 0x1bb

    sput v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$HServer;->HTTPS_PORT_NO_H:I

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
