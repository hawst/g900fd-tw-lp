.class public Lcom/sec/android/service/health/cp/database/datahandler/DataManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static aggregatorList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;",
            ">;"
        }
    .end annotation
.end field

.field private static instance:Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

.field private static mContext:Landroid/content/Context;


# instance fields
.field private keyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->instance:Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->keyList:Ljava/util/ArrayList;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DataManager() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->instance:Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    return-object v0
.end method

.method private getKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "data is null, doesn\'t need to save data"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v1

    invoke-direct {p0, p2, v1}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[DataManager] There is no aggregator for key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;[Landroid/os/Bundle;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    aget-object v1, p1, v0

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v1

    invoke-direct {p0, p2, v1}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;->addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[DataManager] There is no aggregator for key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isRunning(J)Z
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " [DataManager]Checking running aggregator  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->keyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;->isRunning(J)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " [DataManager]There is running aggregator _id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " [DataManager]There is no running aggregator _id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    return-void
.end method

.method public start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;JI)V"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Data Manager start - exerciseID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Package Name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p1, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->mContext:Landroid/content/Context;

    move-wide v2, p5

    move v4, p7

    move-object v5, p2

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->joinSession(Landroid/content/Context;JILjava/lang/String;Ljava/util/List;)V

    sparse-switch p3, :sswitch_data_0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getUIPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v0

    move-object v6, v0

    :goto_0
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, p2, v1}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->keyList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_2
    :pswitch_0
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, p1

    move-wide v3, p5

    move v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;->initialize(Landroid/content/Context;Ljava/lang/String;JI)V

    goto :goto_1

    :sswitch_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGear2FitPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :sswitch_1
    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getGearSPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/BloodPressureDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_BlOODPRESSURE Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_2
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/BloodGlucoseDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/BloodGlucoseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_BLOODGLUCOSE Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_3
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_WEIGHT Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_4
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/BodyTemperatureDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/BodyTemperatureDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_BODYTEMPERATURE Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_5
    const/16 v0, 0x272e

    if-eq p3, v0, :cond_0

    const/16 v0, 0x2730

    if-ne p3, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/HRMBioDataAggregatorForGearThree;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/HRMBioDataAggregatorForGearThree;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_HEARTRATEMONITOR(EXR) HRMDataAggregatorForGearThree Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v0, p5, v2

    if-eqz v0, :cond_2

    const-wide/16 v2, -0x1

    cmp-long v0, p5, v2

    if-nez v0, :cond_3

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/HRMBioDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/HRMBioDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_HEARTRATEMONITOR(BIO) Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/HRMDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_HEARTRATEMONITOR(EXR) Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_6
    const/16 v0, 0x272e

    if-eq p3, v0, :cond_4

    const/16 v0, 0x2730

    if-ne p3, v0, :cond_5

    :cond_4
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForGearThree;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForGearThree;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_PEDOMETER PedometerDataAggregatorForGearThree Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_5
    const/16 v0, 0x2719

    if-ne p3, v0, :cond_6

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_PEDOMETER PedometerDataAggregatorForSContext Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_6
    const/16 v0, 0x2724

    if-ne p3, v0, :cond_7

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForGearOne;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForGearOne;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_PEDOMETER PedometerDataAggregatorForGearOne Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_7
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_PEDOMETER PedometerDataAggregator Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_7
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/FitnessDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_FITNESS Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_8
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_LOCATION Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_9
    const/16 v0, 0x272e

    if-eq p3, v0, :cond_8

    const/16 v0, 0x2730

    if-ne p3, v0, :cond_9

    :cond_8
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregatorForGearThree;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_SLEEP SleepDataAggregatorForGearThree Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_9
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/SleepDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_SLEEP Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_a
    const/16 v0, 0x272e

    if-eq p3, v0, :cond_a

    const/16 v0, 0x2730

    if-ne p3, v0, :cond_b

    :cond_a
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/StressDataAggregatorForGearThree;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/StressDataAggregatorForGearThree;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_STRESS StressDataAggregatorForGearThree Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_b
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/StressDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/StressDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_STRESS Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_b
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/CoachingVariableDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/CoachingVariableDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "[DataManager] DATA_TYPE_COACHING Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_c
    const/16 v0, 0x272e

    if-eq p3, v0, :cond_c

    const/16 v0, 0x2730

    if-ne p3, v0, :cond_d

    :cond_c
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregatorForGearThree;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "DATA_TYPE_EXERCISE Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_d
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/ExerciseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "DATA_TYPE_EXERCISE Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_d
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/UltravioletrayDataAggregatorForGearThree;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "DATA_TYPE_UV_RAY Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_e
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    new-instance v2, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;

    invoke-direct {v2, v6}, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v2, "DATA_TYPE_UVRAY_PROFILE Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_e
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[DataManager] [Initailize] There is no aggregator for key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_f
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x2723 -> :sswitch_0
        0x2724 -> :sswitch_0
        0x2726 -> :sswitch_0
        0x272e -> :sswitch_1
        0x2730 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_4
        :pswitch_7
        :pswitch_c
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public stop(Ljava/lang/String;ILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    const-string v1, "[DataManager] Stop is called "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->leaveSession(Ljava/lang/String;)V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;->finalizeAggregation()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->aggregatorList:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->keyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[DataManager] Aggregator is removed KEY : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    return-void
.end method
