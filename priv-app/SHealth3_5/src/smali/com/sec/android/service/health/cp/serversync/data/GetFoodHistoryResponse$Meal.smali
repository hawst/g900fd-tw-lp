.class public Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Meal"
.end annotation


# instance fields
.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceCreateTimeMeal:Ljava/lang/String;

.field public deviceDaylightSavingMeal:Ljava/lang/String;

.field public deviceMealPKId:J

.field public deviceModifyTimeMeal:Ljava/lang/String;

.field public deviceTimeZoneMeal:Ljava/lang/String;

.field public favoriteYN:Ljava/lang/String;

.field public foodList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealItem;",
            ">;"
        }
    .end annotation
.end field

.field public hdid:Ljava/lang/String;

.field public imageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$MealImage;",
            ">;"
        }
    .end annotation
.end field

.field public inputType:Ljava/lang/String;

.field public intakeTime:Ljava/lang/String;

.field public meal:Ljava/lang/String;

.field public mealCategory:Ljava/lang/String;

.field public mealName:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public totalCalorie:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->foodList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/GetFoodHistoryResponse$Meal;->imageList:Ljava/util/ArrayList;

    return-void
.end method
