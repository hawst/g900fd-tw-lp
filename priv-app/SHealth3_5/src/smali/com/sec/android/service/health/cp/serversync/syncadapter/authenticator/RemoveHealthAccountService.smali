.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;
.super Landroid/app/IntentService;


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "RemoveHealthAccountService"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private removeHealthAccount(I)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;->TAG:Ljava/lang/String;

    const-string v1, "Inside removeHealthAccount"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Inside removeHealthAccount - Database Module has to initialised before doing any action - check OOBE"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;->TAG:Ljava/lang/String;

    const-string v1, "Inside removeHealthAccount - Database Module has to initialised before doing any action - check OOBE."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x1869f

    if-eq p1, v0, :cond_2

    const v0, 0x1869e

    if-ne p1, v0, :cond_3

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;->TAG:Ljava/lang/String;

    const-string v1, "Changing all record sync status to Added"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/DatabaseUtility;->updatePHRSyncStatusToAdded()V

    invoke-static {}, Lcom/sec/android/service/health/cp/DatabaseUtility;->updateProfileSyncStatusToFalse()V

    invoke-static {}, Lcom/sec/android/service/health/cp/DatabaseUtility;->updateProfileImageSyncStatusToAdded()V

    goto :goto_0

    :cond_3
    const v0, 0x1869c

    if-ne p1, v0, :cond_0

    const-string v0, "Deleting Db when erase and reset ..."

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->deleteDatabase()Z

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onHandleIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "REQUEST_TYPE"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/RemoveHealthAccountService;->removeHealthAccount(I)V

    return-void
.end method
