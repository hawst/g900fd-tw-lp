.class public Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;


# instance fields
.field private mCapabilityMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;",
            ">;>;"
        }
    .end annotation
.end field

.field private sensorDeviceProtocolClass:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mInstance:Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    return-void
.end method

.method private checkAvailability(Ljava/util/List;Ljava/util/List;II)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;II)Z"
        }
    .end annotation

    const/16 v6, 0x2711

    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne p3, v6, :cond_0

    if-nez p4, :cond_0

    :goto_0
    return v3

    :cond_0
    if-nez p1, :cond_4

    move v1, v2

    :goto_1
    move v5, v4

    :goto_2
    if-ge v5, v1, :cond_8

    if-eq p3, v6, :cond_1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p3, :cond_5

    :cond_1
    move v5, v3

    :goto_3
    if-ne v5, v3, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    :cond_2
    move v1, v4

    :goto_4
    if-ge v1, v2, :cond_7

    if-eqz p4, :cond_3

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p4, :cond_6

    :cond_3
    move v0, v3

    :goto_5
    and-int v3, v5, v0

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_7
    move v0, v4

    goto :goto_5

    :cond_8
    move v5, v4

    goto :goto_3
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mInstance:Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    return-object v0
.end method

.method private initilize()V
    .locals 12

    const/4 v10, 0x0

    move v9, v10

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v9, v1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_1
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getConnectivityType()I

    move-result v11

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProtocol()Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Class;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;-><init>(Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InstantiationException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v10

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/ArrayList;

    move-object v8, v0

    new-instance v1, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProtocol()Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Class;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;-><init>(Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v11, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v10

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "() Exception :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public augmentNewProtocol(Ljava/lang/Class;Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->initilize()V

    :cond_1
    return-void
.end method

.method public checkProtocolAvailability(III)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDeviceTypes()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDataTypes()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v3, v0, p2, p3}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->checkAvailability(Ljava/util/List;Ljava/util/List;II)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public checkProtocolAvailability(IIILjava/lang/String;)Z
    .locals 9

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    :goto_0
    return v3

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getProtocol()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v1, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDeviceTypes()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v5, v3

    :goto_2
    if-ge v5, v8, :cond_2

    const/16 v1, 0x2711

    if-eq p2, v1, :cond_1

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p2, :cond_4

    :cond_1
    move v2, v4

    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDataTypes()Ljava/util/List;

    move-result-object v5

    if-ne v2, v4, :cond_5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    :goto_3
    move v2, v3

    :goto_4
    if-ge v2, v1, :cond_8

    if-eqz p3, :cond_3

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p3, :cond_6

    :cond_3
    move v0, v4

    :goto_5
    move v2, v0

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    :cond_5
    const/4 v0, -0x1

    move v1, v0

    goto :goto_3

    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_7
    move v3, v2

    goto :goto_0

    :cond_8
    move v0, v3

    goto :goto_5

    :cond_9
    move v0, v2

    goto :goto_5
.end method

.method public finalize()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getProtocolByConnectivityType(I)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getProtocolClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public intiProtocolFactory()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->TAG:Ljava/lang/String;

    const-string v1, "[intiProtocolFactory] Initialize of Protocol Factory"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/ContinuaProtocol;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    const-class v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/RFComProtocol;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/InfopiaProtocol;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->sensorDeviceProtocolClass:Ljava/util/ArrayList;

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/GearProtocol;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->initilize()V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->TAG:Ljava/lang/String;

    const-string v1, "[intiProtocolFactory] End Initialize of Protocol Factory"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public searchProtocol(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 2

    if-nez p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->TAG:Ljava/lang/String;

    const-string v1, "ProtocolFactory searchProtocol() :   device is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDeviceOriginalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->searchProtocolByDeviceName(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v0

    goto :goto_0
.end method

.method public searchProtocolByDeviceName(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 8

    const/4 v3, 0x0

    if-nez p1, :cond_0

    move-object v1, v3

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_1

    move-object v1, v3

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDeviceNames()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_3

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getProtocolClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :cond_4
    move-object v1, v3

    goto :goto_0
.end method

.method public searchProtocolByDeviceTypeDataType(III)Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->mCapabilityMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDeviceTypes()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getDataTypes()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v3, v4, p2, p3}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->checkAvailability(Ljava/util/List;Ljava/util/List;II)Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory$ProtocolCapability;->getProtocolClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
