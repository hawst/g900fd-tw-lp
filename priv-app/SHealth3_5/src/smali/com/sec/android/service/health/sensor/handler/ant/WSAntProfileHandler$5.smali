.class Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestDownloadAllhistory()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadAllHistoryFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$11;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$AntFsRequestStatus:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error. Unrecognized status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "DownloadAllHistory finished successfully."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_1
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "DownloadAllHistory failed, device busy."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "DownloadAllHistory failed, communication error."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_3
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "DownloadAllHistory failed, feature not supported in weight scale."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_4
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "DownloadAllHistory failed, authentication rejected."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_5
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "DownloadAllHistory failed, transmission lost."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStopped(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
