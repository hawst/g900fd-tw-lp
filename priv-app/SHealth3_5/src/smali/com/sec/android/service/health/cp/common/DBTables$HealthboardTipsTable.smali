.class public Lcom/sec/android/service/health/cp/common/DBTables$HealthboardTipsTable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HealthboardTipsTable"
.end annotation


# static fields
.field public static final HB_RULE_ID:Ljava/lang/String; = "rule_id"

.field public static final HB_TIP_ARTICLE:Ljava/lang/String; = "article"

.field public static final HB_TIP_BOARD_CONTENT_ID:Ljava/lang/String; = "boardcontentid"

.field public static final HB_TIP_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final HB_TIP_DATA_ONE:Ljava/lang/String; = "hb_data_one"

.field public static final HB_TIP_DATA_TWO:Ljava/lang/String; = "hb_data_two"

.field public static final HB_TIP_ID:Ljava/lang/String; = "_id"

.field public static final HB_TIP_META_DATA:Ljava/lang/String; = "meta_data"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final TABLE_NAME:Ljava/lang/String; = "healthboard_tips"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
