.class public final Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "PCCServiceHandler"
.end annotation


# instance fields
.field weakAntProfHandlerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->weakAntProfHandlerRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->weakAntProfHandlerRef:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string/jumbo v1, "weakAntProfHandlerRef is null"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->weakAntProfHandlerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    if-nez v0, :cond_1

    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string v1, "enclosingClassRef is null"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "[HealthSensor]AntProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage() :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->handleMessageSub(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_0
    const-string v1, "[HealthSensor]AntProfileHandler"

    const-string v2, "mAntPluginPcc MSG_REQ_PCC_START --"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->requestAccess()V

    goto :goto_0

    :pswitch_1
    const-string v1, "[HealthSensor]AntProfileHandler"

    const-string v2, "mAntPluginPcc MSG_REQ_PCC_END --"

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    # invokes: Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->stopDevice()V
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyResponse(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
