.class Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->uploadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

.field final synthetic val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpload(JILcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/16 v5, 0x2712

    const/4 v4, 0x0

    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    if-ne p4, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start Uploading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iput-wide p5, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->mTotalFileSize:J

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onUpload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "upload STARTED, requestId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->start(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->ONGOING:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    if-ne p4, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ongoing Uploading... size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-wide v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->mTotalFileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    if-ne p4, v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Done Uploading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # operator++ for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$608(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onUpload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "upload DONE, requestId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedLocalData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->end(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->dump()V

    :cond_3
    const-string v0, "/storage/emulated/0/SHealth3/cache/img/user_photo.jpg"

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uploadImageItem(): Profile image uploaded successfully"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/common/SyncPreference;->setLastProfileImageSyncedTime(J)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$700(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # operator++ for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$708(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->uploadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    invoke-static {v1, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$800(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$600(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uploadImageItem(): No images remaining to upload"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V

    goto/16 :goto_0

    :cond_6
    sget-object v0, Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnUploadListener$Status;

    if-ne p4, v0, :cond_0

    if-eqz p7, :cond_8

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopped Uploading... error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$500(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v4, v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Stopped Uploading... error : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$500(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v3, v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->modifySyncStatus(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$900(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # operator++ for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$608(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$700(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # operator++ for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mStartedImageCount:I
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$708(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->uploadImageItem(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V
    invoke-static {v1, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$800(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;)V

    :cond_7
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mFinishedImageCount:I
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$600(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v1, v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mUnsyncedImageData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uploadImageItem(): No images remaining to upload"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->setServerData(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopped Uploading... error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v2, v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mSyncType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$500(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$6;->val$imageItem:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;

    iget-object v4, v4, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Stopped Uploading..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
