.class public interface abstract Lcom/sec/android/service/health/cp/common/DBConstants;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/common/DBConstants$B_PEDOMETER_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$ANT_DEVICE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$TRAINING_LOAD_PEAK_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$REAL_TIME_SPEED_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$REAL_TIME_HEART_BEAT_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$MAP_PATH_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$HEARTRATE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$SENSOR_CONSTANTS;,
        Lcom/sec/android/service/health/cp/common/DBConstants$HEALTHBOARD_TIPS;,
        Lcom/sec/android/service/health/cp/common/DBConstants$TIP_INSTANCE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$GOAL_INSTANCE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$SLEEP_MONITOR_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$DATA_INPUT_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$USER_PPROFILE_SHARE_DATA_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$USER_PPROFILE_SHARE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$BLUETOOTH_DEVICE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$NEWS_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$WIDGET_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$CARDIO_VASCULAR_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$MEMO_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$GOAL_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$COMFORT_ZONE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$WALKING_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$EXERCISE_INFO_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$RUNNING_PRO_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$EXERCISE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$MEAL_ITEM_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$MEAL_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$EXTENDED_FOOD_INFO_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$FOOD_INFO_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$CIGARETTE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$ALCOHOL_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$WEIGHT_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$PRESSURE_TYPE;,
        Lcom/sec/android/service/health/cp/common/DBConstants$BLOOD_SUGAR_TYPE;
    }
.end annotation


# static fields
.field public static final APPLICATION_NAME:Ljava/lang/String; = "SHealth2"

.field public static final AUTHORITY:Ljava/lang/String; = "SecHealth"

.field public static final COMFORT_ZONE_CARD_UNIQUE_KEY:Ljava/lang/String; = "D_COMFORT_ZONE"

.field public static final DB_FOLDER_NAME:Ljava/lang/String; = "/databases/"

.field public static final DEFAULT_ROW_ID:I = -0x1

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "create_time"

.field public static final PEDOMETER_CARD_UNIQUE_KEY:Ljava/lang/String; = "3926905"

.field public static final TAG:Ljava/lang/String; = "HealthDatabase"

.field public static final allMeasuresId:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/service/health/cp/common/DBConstants;->allMeasuresId:[I

    return-void

    :array_0
    .array-data 4
        0xb
        0xd
        0xe
        0x8
        0x15
        0x17
        0x3ee
        0x3ed
        0x3ea
        0x3f2
        0x3fd
        0x3ef
        0x32
        -0x1
    .end array-data
.end method
