.class Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc$IStrideCountReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewStrideCount(JLjava/util/EnumSet;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;J)V"
        }
    .end annotation

    const-string v0, "[HealthSensor]SDMSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewStrideCount - cumulativeStrides: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->oldTotalStep:J
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->oldTotalStep:J
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    iget-object v3, v3, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->oldTotalStep:J
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)J

    move-result-wide v3

    sub-long v3, v0, v3

    long-to-int v3, v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    iget-object v3, v3, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->notifyResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->oldTotalStep:J
    invoke-static {v2, v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->access$702(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;J)J

    return-void
.end method
