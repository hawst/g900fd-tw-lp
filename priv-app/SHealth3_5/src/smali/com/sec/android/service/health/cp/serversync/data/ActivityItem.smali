.class public Lcom/sec/android/service/health/cp/serversync/data/ActivityItem;
.super Ljava/lang/Object;


# instance fields
.field public altitudeGain:Ljava/lang/Double;

.field public altitudeLoss:Ljava/lang/Double;

.field public appId:Ljava/lang/String;

.field public cadence:Ljava/lang/Double;

.field public cadenceTypeCode:Ljava/lang/String;

.field public calorie:Ljava/lang/Double;

.field public declineDistance:Ljava/lang/Double;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public distance:Ljava/lang/Double;

.field public duration:Ljava/lang/Integer;

.field public durationMilli:Ljava/lang/Long;

.field public endTime:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public incline:Ljava/lang/Float;

.field public inclineDistance:Ljava/lang/Double;

.field public maxAltitude:Ljava/lang/Float;

.field public maxCadenceRate:Ljava/lang/Double;

.field public maxCalorieburnRate:Ljava/lang/Double;

.field public maxHeartRate:Ljava/lang/Double;

.field public maxPace:Ljava/lang/Double;

.field public maxSpeedRate:Ljava/lang/Double;

.field public meanCadenceRate:Ljava/lang/Double;

.field public meanCalorieburnRate:Ljava/lang/Double;

.field public meanHeartRate:Ljava/lang/Double;

.field public meanPace:Ljava/lang/Double;

.field public meanSpeedRate:Ljava/lang/Double;

.field public minAltitude:Ljava/lang/Float;

.field public powerWatt:Ljava/lang/Double;

.field public requestType:Ljava/lang/String;

.field public resistance:Ljava/lang/Integer;

.field public samplePosition:Ljava/lang/Integer;

.field public speedUnit:Ljava/lang/String;

.field public startTime:Ljava/lang/String;

.field public strideLength:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
