.class Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IProductInformationReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->subscribeCommonToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewProductInformation(JLjava/util/EnumSet;IIJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;IIJ)V"
        }
    .end annotation

    const/16 v3, 0x7d8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]AntProfileHandler"

    const-string/jumbo v1, "onNewProductInformation() mAntPluginPcc null"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SoftwareRevision: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "[HealthSensor]AntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SerialNumber: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p6, p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {p6, p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-static {p6, p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyStateChanged(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-static {p6, p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->notifyStateChanged(I)V

    goto :goto_0
.end method
