.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticationService;
.super Landroid/app/Service;


# static fields
.field private static final TAG:Ljava/lang/String; = "AuthenticationService"


# instance fields
.field private mAuthenticator:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthServiceAuthenticator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    const-string v0, "AuthenticationService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AuthenticationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBinder()...  returning the AccountAuthenticator binder for intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticationService;->mAuthenticator:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthServiceAuthenticator;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthServiceAuthenticator;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "AuthenticationService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AuthenticationService"

    const-string v1, "SampleSyncAdapter Authentication Service started."

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthServiceAuthenticator;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthServiceAuthenticator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticationService;->mAuthenticator:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/HealthServiceAuthenticator;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "AuthenticationService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AuthenticationService"

    const-string v1, "SampleSyncAdapter Authentication Service stopped."

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
