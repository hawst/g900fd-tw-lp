.class Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalShealthSensorCapability"
.end annotation


# instance fields
.field dataType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field deviceNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field deviceType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field handlerClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field profileName:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->this$0:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->handlerClass:Ljava/lang/Class;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->profileName:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->deviceType:Ljava/util/List;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->dataType:Ljava/util/List;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->deviceNames:Ljava/util/List;

    iput-object p5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->profileName:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->handlerClass:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public getDataTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->dataType:Ljava/util/List;

    return-object v0
.end method

.method public getDeviceNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->deviceNames:Ljava/util/List;

    return-object v0
.end method

.method public getDeviceTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->deviceType:Ljava/util/List;

    return-object v0
.end method

.method public getHandlerClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->handlerClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getProfileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory$LocalShealthSensorCapability;->profileName:Ljava/lang/String;

    return-object v0
.end method
