.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;
.super Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final TOTAL_REPETITION_COUNT:Ljava/lang/String; = "total_repetition_count"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    return-void
.end method


# virtual methods
.method protected getDeletedLocalData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->TAG:Ljava/lang/String;

    const-string v1, "getDeletedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method protected getDetailHistoryData()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->TAG:Ljava/lang/String;

    const-string v1, "getDetailHistoryData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;

    invoke-direct {v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;-><init>()V

    const-string v0, "A"

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;->exerciseTypeCode:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v4, v4, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " items for exercise manual"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v4, 0x3e7

    if-ne v0, v4, :cond_0

    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory$ExerciseLogId;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory$ExerciseLogId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v4, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_EXERCISE_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v4, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v4, v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory$ExerciseLogId;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->mGetSyncHistoryData:Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory;->historyList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetSyncHistory$HistoryList;->summaryCreateTime:Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory$ExerciseLogId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask$RequestGetExerciseHistory;->deviceCreateTimeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_EXERCISE_HISTORY:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v2
.end method

.method protected getSyncTypeString()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->TAG:Ljava/lang/String;

    const-string v1, "getSyncTypeString()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "exercisemanual"

    return-object v0
.end method

.method protected getTableName()Ljava/lang/String;
    .locals 1

    const-string v0, "exercise"

    return-object v0
.end method

.method protected getUnsyncedLocalData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->TAG:Ljava/lang/String;

    const-string v1, "getUnsyncedLocalData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->getTableName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncExerciseUtils;->getModifiedExercise(Ljava/lang/String;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected updateItemsLocally(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateItemsLocally()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ExerciseManualTask;->mGson:Lcom/google/gson/Gson;

    check-cast p2, Ljava/lang/String;

    const-class v1, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetActivity;

    invoke-virtual {v0, p2, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetActivity;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetActivity;->activityPeriodList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetActivity;->activityPeriodList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/network/gsonobject/ResponseGetActivity;->activityPeriodList:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncExerciseUtils;->addModifyActivity(Ljava/util/ArrayList;Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
