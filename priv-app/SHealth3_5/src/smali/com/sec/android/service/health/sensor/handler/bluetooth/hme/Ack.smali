.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;
.super Ljava/lang/Object;


# static fields
.field public static final ACK:I = 0x1

.field public static final ACK_NAME:[Ljava/lang/String;

.field public static final INVALID:I = 0x3

.field public static final NACK:I = 0x2

.field public static final UNKNOWN:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ACK"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "NACK"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "INVAL"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/Ack;->ACK_NAME:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
