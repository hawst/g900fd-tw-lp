.class Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onReceived()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->setAccessToken(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "onReceived(): inside synchronized"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mIsSamsungTokenRecieved:Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;)Landroid/content/SyncResult;

    move-result-object v0

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setFailureMessage(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "setFailureMessage(): inside synchronized"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mIsSamsungTokenRecieved:Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    const/16 v2, 0xb

    # setter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mErrorGettingSamsungToken:I
    invoke-static {v0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$202(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;I)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->setAccessToken(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setNetworkFailure()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$100(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;)Landroid/content/SyncResult;

    move-result-object v0

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "setNetworkFailure(): I failed.."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "setNetworkFailure(): I failed.. Inside synchronized"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mIsSamsungTokenRecieved:Z

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$1;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    const/4 v2, 0x4

    # setter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mErrorGettingSamsungToken:I
    invoke-static {v0, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$202(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;I)I

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->setAccessToken(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->mSamsungLockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
