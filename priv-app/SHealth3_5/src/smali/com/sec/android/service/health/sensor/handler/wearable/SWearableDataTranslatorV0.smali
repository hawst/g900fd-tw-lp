.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;
.super Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;


# static fields
.field public static final bNewVersion:Z = true


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;-><init>()V

    return-void
.end method

.method private addPedometerGoalHistory([Landroid/os/Parcelable;Landroid/content/Context;Ljava/lang/String;)V
    .locals 9

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    move v8, v3

    :goto_0
    array-length v0, p1

    if-ge v8, v0, :cond_2

    aget-object v0, p1, v8

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SGoal;

    if-eqz v0, :cond_0

    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-eqz v1, :cond_0

    const v2, 0x9c41

    iget v4, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    move-object v0, p0

    move-object v1, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->setGoalDataValueWithUserDeviceId(Landroid/content/Context;IIIJLjava/lang/String;)I

    :cond_0
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "History <<SGoal>> : goal is null. "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private getCoachingResult(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SCoaching;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SCoaching;-><init>()V

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;-><init>()V

    iget-char v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->ac:C

    iget-char v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxHeartRate:C

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxMET:J

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->recourceRecovery:I

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->startDate:J

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->trainingLevel:I

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->lastTrainingLevelUpdate:J

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousTrainingLevel:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousToPreviousTrainingLevel:I

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestFeedbackPhraseNumber:I

    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestExerciseTime:J

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method private getExerciseResult([Landroid/os/Parcelable;[Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;
    .locals 12

    const/4 v5, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_b

    array-length v0, p1

    new-array v4, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<EXERCISE>> : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "ea received"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_a

    array-length v0, v4

    new-array v6, v0, [Lcom/samsung/android/sdk/health/sensor/SExercise;

    aget-object v0, p1, v1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SExercise;

    aput-object v0, v6, v1

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;-><init>()V

    aput-object v0, v4, v1

    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->time:J

    iput-wide v7, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    aget-object v0, v6, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    const/16 v3, 0x2712

    if-ne v0, v3, :cond_0

    aget-object v0, v4, v1

    const/16 v3, 0x4652

    iput v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    :goto_1
    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->duration:J

    iput-wide v7, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->calorie:D

    iput-wide v7, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->heartRate:D

    iput-wide v7, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->distance:D

    iput-wide v7, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->fitnessLevel:I

    iput v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    aget-object v0, v6, v1

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->extra:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    aget-object v0, v6, v1

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->extra:Landroid/os/Bundle;

    aget-object v3, v4, v1

    const-string v7, "AVERAGE_SPEED"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v7

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->averageSpeed:F

    aget-object v3, v4, v1

    const-string v7, "MAX_SPEED"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v7

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxSpeed:F

    aget-object v3, v4, v1

    const-string v7, "MAX_HEARTRATE"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v7

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxHeartRate:F

    aget-object v3, v4, v1

    const-string v7, "MAX_ALTITUDE"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v7

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxAltitude:F

    aget-object v3, v4, v1

    const-string v7, "MIN_ALTITUDE"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v7

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->minAltitude:F

    const-string v3, "HEARTRATE_RAW_DATA"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    check-cast v0, [Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    if-eqz v0, :cond_4

    aget-object v3, v4, v1

    array-length v7, v0

    new-array v7, v7, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    iput-object v7, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    move v3, v2

    :goto_2
    array-length v7, v0

    if-ge v3, v7, :cond_4

    aget-object v7, v4, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    new-instance v8, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    invoke-direct {v8}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;-><init>()V

    aput-object v8, v7, v3

    aget-object v7, v4, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v7, v7, v3

    aget-object v8, v0, v3

    iget v8, v8, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;->heartRate:I

    iput v8, v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->heartRate:I

    aget-object v7, v4, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v7, v7, v3

    aget-object v8, v0, v3

    iget-wide v8, v8, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;->samplingTime:J

    iput-wide v8, v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->samplingTime:J

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_0
    aget-object v0, v6, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    const/16 v3, 0x2afa

    if-ne v0, v3, :cond_1

    aget-object v0, v4, v1

    const/16 v3, 0x4653

    iput v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_1
    aget-object v0, v6, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    const/16 v3, 0x36b2

    if-ne v0, v3, :cond_2

    aget-object v0, v4, v1

    const/16 v3, 0x4654

    iput v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_2
    aget-object v0, v6, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    const/16 v3, 0x2ee1

    if-ne v0, v3, :cond_3

    aget-object v0, v4, v1

    const/16 v3, 0x4655

    iput v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_3
    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    iput v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    goto/16 :goto_1

    :cond_4
    aget-object v0, v6, v1

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    if-eqz v0, :cond_6

    aget-object v0, v4, v1

    aget-object v3, v6, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    array-length v3, v3

    new-array v3, v3, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iput-object v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move v0, v2

    :goto_3
    aget-object v3, v6, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    new-instance v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    invoke-direct {v7}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;-><init>()V

    aput-object v7, v3, v0

    aget-object v3, v6, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v3, v3, v0

    if-eqz v3, :cond_5

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->time:J

    iput-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->latitude:D

    iput-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->longitude:D

    iput-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->altitude:D

    iput-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->altitude:D

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->accuracy:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->speed:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->bearing:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->bearing:F

    aget-object v3, v6, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    if-eqz v3, :cond_5

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    new-instance v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    invoke-direct {v7}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;-><init>()V

    iput-object v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->averageSpeed:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->totalDistance:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineDistance:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineDistance:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatDistance:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatDistance:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineTime:J

    iput-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineTime:J

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineTime:J

    iput-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineTime:J

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatTime:J

    iput-wide v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatTime:J

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->consumedCalorie:F

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    aget-object v3, v4, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    aget-object v7, v6, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget v7, v7, Lcom/samsung/android/sdk/health/sensor/SExtra;->stepCount:I

    iput v7, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->stepCount:I

    aget-object v3, v6, v1

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/SExtra;->extra:Landroid/os/Bundle;

    if-eqz v3, :cond_5

    aget-object v7, v4, v1

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    const-string v8, "PACE"

    const v9, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v3

    iput v3, v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->pace:F

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    :cond_6
    aget-object v0, v4, v1

    if-eqz v0, :cond_9

    if-eqz p2, :cond_9

    move v3, v2

    :goto_4
    array-length v0, p2

    if-ge v3, v0, :cond_7

    aget-object v0, p2, v3

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    aget-object v6, v4, v1

    new-instance v7, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;

    invoke-direct {v7}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;-><init>()V

    iput-object v7, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    aget-object v6, v4, v1

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v7, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    iput-wide v7, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    aget-object v6, v4, v1

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v7, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->distance:D

    iput-wide v7, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->distance:D

    aget-object v6, v4, v1

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v7, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteTrainingLoadPeak:I

    iput v7, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteTrainingLoadPeak:I

    aget-object v6, v4, v1

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v7, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteMaxMET:I

    iput v7, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteMaxMET:I

    aget-object v6, v4, v1

    iget-object v6, v6, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteResourceRecovery:I

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteResourceRecovery:I

    aget-object v0, v4, v1

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iget-wide v6, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    aget-object v0, v4, v1

    iget-wide v8, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    aget-object v0, v4, v1

    iget-wide v10, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    add-long/2addr v8, v10

    sub-long/2addr v6, v8

    const-wide/16 v8, -0x64

    cmp-long v0, v6, v8

    if-lez v0, :cond_8

    const-wide/16 v8, 0x64

    cmp-long v0, v6, v8

    if-gez v0, :cond_8

    :cond_7
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    aget-object v3, v4, v1

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_8
    aget-object v0, v4, v1

    iput-object v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_9
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v3, "<<EXERCISE>> :  exerciseData[i] is null "

    invoke-static {v0, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_a
    move-object v0, v4

    :goto_6
    return-object v0

    :cond_b
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "exerciseData is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    goto :goto_6
.end method

.method private getHeartRateMonitor([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    array-length v0, p1

    new-array v2, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    array-length v0, p1

    new-array v3, v0, [Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    aget-object v0, p1, v1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;

    aput-object v0, v3, v1

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;-><init>()V

    aput-object v0, v2, v1

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->time:J

    iput-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRate:I

    iput v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->eventTime:J

    iput-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->interval:J

    iput-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->interval:J

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNR:F

    iput v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNR:F

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNRUnit:I

    iput v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNRUnit:I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_1
    return-object v0

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v2, "<<STRESS DATA>> is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getLastGoal(Landroid/content/Context;ILjava/lang/String;)Lcom/samsung/android/sdk/health/sensor/SGoal;
    .locals 8

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v0, "value"

    aput-object v0, v2, v1

    const-string/jumbo v0, "set_time"

    aput-object v0, v2, v5

    const-string v3, "goal_type=? AND user_device__id=? AND set_time<=?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    aput-object p3, v4, v5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v5, "set_time DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v6, Lcom/samsung/android/sdk/health/sensor/SGoal;

    invoke-direct {v6}, Lcom/samsung/android/sdk/health/sensor/SGoal;-><init>()V

    const-string/jumbo v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    const-string/jumbo v0, "set_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v6, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v6

    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v1, v6

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private getPedometerResult([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .locals 8

    const/4 v2, 0x0

    const v7, 0x7fffffff

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    array-length v0, p1

    new-array v3, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    array-length v0, p1

    new-array v4, v0, [Lcom/samsung/android/sdk/health/sensor/SPedometer;

    move v1, v2

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_4

    aget-object v0, p1, v1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SPedometer;

    aput-object v0, v4, v1

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    aput-object v0, v3, v1

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->time:J

    iput-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->distance:F

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->calories:F

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->speed:F

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->totalStep:I

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->runStep:I

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->walkStep:I

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    aget-object v0, v3, v1

    aget-object v5, v4, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/SPedometer;->updownStep:I

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    aget-object v0, v3, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-lez v0, :cond_2

    aget-object v0, v3, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    if-nez v0, :cond_0

    aget-object v0, v3, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    if-nez v0, :cond_0

    aget-object v0, v3, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-eqz v0, :cond_1

    :cond_0
    aget-object v0, v3, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    if-ne v0, v7, :cond_3

    aget-object v0, v3, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    if-ne v0, v7, :cond_3

    aget-object v0, v3, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-ne v0, v7, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    aget-object v0, v3, v1

    aget-object v5, v3, v1

    iget v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    iput v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    aget-object v5, v3, v1

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move-object v0, v3

    :goto_2
    return-object v0

    :cond_5
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v2, "<<PEDOMETER DATA>> is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private getSleepResult([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    array-length v0, p1

    new-array v2, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<<SLEEP DATA>> : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v3, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "ea received."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v0, p1

    new-array v3, v0, [Lcom/samsung/android/sdk/health/sensor/SSleep;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    aget-object v0, p1, v1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SSleep;

    aput-object v0, v3, v1

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;-><init>()V

    aput-object v0, v2, v1

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/SSleep;->startTime:J

    iput-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->startTime:J

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/SSleep;->endTime:J

    iput-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->endTime:J

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-wide v4, v4, Lcom/samsung/android/sdk/health/sensor/SSleep;->efficiency:D

    iput-wide v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->efficiency:D

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/SSleep;->status:[I

    iput-object v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->status:[I

    aget-object v0, v2, v1

    aget-object v4, v3, v1

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/SSleep;->time:[J

    iput-object v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->time:[J

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_1
    return-object v0

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v2, "<<SLEEP DATA>> is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getStressResult([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;
    .locals 11

    const/4 v10, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    if-eqz p1, :cond_6

    array-length v5, p1

    new-array v6, v5, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    array-length v0, p1

    new-array v7, v0, [Lcom/samsung/android/sdk/health/sensor/SStress;

    move v1, v2

    move v3, v2

    :goto_0
    array-length v0, v6

    if-ge v1, v0, :cond_2

    aget-object v0, p1, v1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SStress;

    aput-object v0, v7, v1

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;-><init>()V

    aput-object v0, v6, v1

    aget-object v0, v6, v1

    aget-object v8, v7, v1

    iget v8, v8, Lcom/samsung/android/sdk/health/sensor/SStress;->state:I

    iput v8, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    aget-object v0, v6, v1

    aget-object v8, v7, v1

    iget-wide v8, v8, Lcom/samsung/android/sdk/health/sensor/SStress;->time:J

    iput-wide v8, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->time:J

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    aget-object v8, v6, v1

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v6, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-eqz v0, :cond_0

    aget-object v0, v6, v1

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-ne v0, v10, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    if-ne v2, v5, :cond_3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "<<STRESS DATA>> all value includes error status. it will not deleiver to application."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v4

    :goto_1
    return-object v0

    :cond_3
    sub-int v0, v5, v3

    new-array v1, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    move v0, v2

    :goto_2
    if-ge v2, v5, :cond_7

    aget-object v3, v6, v2

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-eqz v3, :cond_4

    aget-object v3, v6, v2

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;->state:I

    if-ne v3, v10, :cond_5

    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    aget-object v3, v6, v2

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "<<STRESS DATA>> is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v4

    goto :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_1
.end method

.method private isExistHistoryGoalData(Landroid/content/Context;IIJILjava/lang/String;)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "value"

    aput-object v1, v2, v0

    const-string v3, "goal_type=? AND value=? AND user_device__id=? AND set_time=?"

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object p7, v4, v0

    const/4 v0, 0x3

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isExistGoalData() goalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " activityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    move v0, v7

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_0
.end method

.method private isTodaysGoal(J)Z
    .locals 4

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetData(Landroid/content/Context;Landroid/os/Bundle;I)V
    .locals 10

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "resetData() pedo"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-string v0, "PEDOMETER"

    const-wide/16 v4, 0x0

    invoke-virtual {p2, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_1
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT EX._id FROM exercise AS EX, user_device AS UD, exercise_device_info AS EDI  WHERE EX.start_time BETWEEN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/32 v3, 0x5265c00

    add-long/2addr v0, v3

    const-wide/16 v3, 0x1

    sub-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND EX.exercise_type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x4e23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND EX._id = EDI.exercise__id AND EDI.user_device__id = UD._id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND UD.device_type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_5

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-wide v2

    :goto_2
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    cmp-long v0, v2, v7

    if-eqz v0, :cond_0

    const-string v0, "_id = ?"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v9

    invoke-virtual {v1, v4, v0, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-wide v0, v2

    goto/16 :goto_1

    :cond_3
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v6

    move-wide v2, v7

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :cond_5
    move-wide v2, v7

    goto :goto_2
.end method

.method private resetSleepData(Landroid/content/Context;Landroid/os/Bundle;ILjava/lang/String;)V
    .locals 9

    const-wide/16 v6, 0x1

    const-wide/16 v2, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "resetData() resetSleepData"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-string v0, "PEDOMETER"

    const-wide/16 v4, 0x0

    invoke-virtual {p2, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_1
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/32 v4, 0x2932e00

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v2

    sub-long v2, v0, v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v2

    add-long/2addr v2, v4

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getEndOfDay(J)J

    move-result-wide v0

    add-long/2addr v0, v4

    :goto_2
    sget-object v4, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "startTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " endTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "bed_time>=? AND bed_time<=? AND user_device__id=?"

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v0

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    move-wide v0, v2

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v2

    sub-long/2addr v2, v6

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v2

    add-long/2addr v2, v4

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v0

    sub-long/2addr v0, v6

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getEndOfDay(J)J

    move-result-wide v0

    add-long/2addr v0, v4

    goto :goto_2
.end method

.method private setDistanceUnit(ILandroid/content/Context;)V
    .locals 4

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-static {p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getDistanceUnit(Landroid/content/Context;)I

    move-result v0

    :goto_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    const-string v2, "distance_unit"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/service/health/cp/HealthContentProvider;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_1
    const v0, 0x29811

    goto :goto_0

    :pswitch_2
    const v0, 0x29813

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x29811
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private setGoalDataValueWithUserDeviceId(Landroid/content/Context;IIIJLjava/lang/String;)I
    .locals 11

    const/4 v10, -0x1

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p4

    move-wide/from16 v6, p5

    move v8, p3

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->isExistHistoryGoalData(Landroid/content/Context;IIJILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v3, "Goal already exists!!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setGoalDataValueWithUserDeviceId() goalType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " activityType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vlaue : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " time : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ret : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v10

    :cond_1
    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->isTodaysGoal(J)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p7

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getLastGoal(Landroid/content/Context;ILjava/lang/String;)Lcom/samsung/android/sdk/health/sensor/SGoal;

    move-result-object v2

    if-eqz v2, :cond_3

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    if-eq v3, p4, :cond_0

    move-wide/from16 v0, p5

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v3

    iget-wide v5, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v5

    cmp-long v2, v3, v5

    if-nez v2, :cond_2

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/SGoal;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/SGoal;-><init>()V

    iput p2, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->type:I

    iput p4, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    move-wide/from16 v0, p5

    iput-wide v0, v2, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    move-object/from16 v0, p7

    invoke-direct {p0, p1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->updateTodaysGoal(Landroid/content/Context;Landroid/os/Parcelable;Ljava/lang/String;)I

    move-result v10

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "set_time"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v3, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v2, v2

    :goto_1
    move v10, v2

    goto/16 :goto_0

    :catch_0
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setGoalDataValueWithUserDeviceId() Exception occured while Goal insert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v10

    goto :goto_1

    :cond_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "set_time"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v3, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    long-to-int v10, v2

    goto/16 :goto_0

    :catch_1
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setGoalDataValueWithUserDeviceId() Exception occured while Goal insert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "set_time"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v3, "value"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide v2

    long-to-int v10, v2

    goto/16 :goto_0

    :catch_2
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setGoalDataValueWithUserDeviceId() Exception occured while Goal insert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private updateCoachingProfile([Landroid/os/Parcelable;Landroid/content/Context;)V
    .locals 9

    if-eqz p1, :cond_1

    array-length v0, p1

    new-array v8, v0, [Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    array-length v0, p1

    if-ge v7, v0, :cond_2

    aget-object v0, p1, v7

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;

    aput-object v0, v8, v7

    aget-object v0, v8, v7

    if-eqz v0, :cond_0

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    const/4 v3, 0x4

    aget-object v0, v8, v7

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->caloryGoal:F

    float-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    const/4 v3, 0x2

    aget-object v0, v8, v7

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->distanceGoal:F

    float-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    const/4 v3, 0x3

    aget-object v0, v8, v7

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeGoal:J

    long-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    const/4 v3, 0x5

    aget-object v0, v8, v7

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectIntensityGoal:F

    float-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    aget-object v0, v8, v7

    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    const/4 v3, 0x7

    aget-object v0, v8, v7

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectTimeGoal:J

    long-to-int v4, v0

    aget-object v0, v8, v7

    iget-wide v5, v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->setActivityTypeGoalValue(Landroid/content/Context;IIIJ)I

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "<<SCoachingProfile>> : profile is null. "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private updatePedometerGoal(Landroid/os/Parcelable;Landroid/content/Context;Ljava/lang/String;)V
    .locals 9

    const-wide/16 v7, 0x0

    const v6, 0x9c41

    const/4 v5, 0x0

    if-eqz p1, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SGoal;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePedometerGoal() pedometer goal= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p2, v6, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getLastGoal(Landroid/content/Context;ILjava/lang/String;)Lcom/samsung/android/sdk/health/sensor/SGoal;

    move-result-object v1

    if-eqz v1, :cond_3

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    if-eq v2, v3, :cond_0

    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    cmp-long v2, v2, v7

    if-eqz v2, :cond_2

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v1

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    invoke-direct {p0, p2, p1, p3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->updateTodaysGoal(Landroid/content/Context;Landroid/os/Parcelable;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v2, "period"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "goal_type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "goal_subtype"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "value"

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "set_time"

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "user_device__id"

    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePedometerGoal() Exception occured while Goal insert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "Goal time is 0"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    cmp-long v1, v1, v7

    if-eqz v1, :cond_4

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v2, "period"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "goal_type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "goal_subtype"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "value"

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "set_time"

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "user_device__id"

    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePedometerGoal() Exception occured while Goal insert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "Goal time is 0"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "<<SGoal>> : goal is null. "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private updateProfile(Landroid/os/Parcelable;Landroid/content/Context;)V
    .locals 9

    const v8, 0x29813

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/SProfile;

    if-nez p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "<<PROFILE>> : Profile is null. "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<PROFILE>> : Profile is received. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/SProfile;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getProfileUpdatedTime(Landroid/content/Context;)J

    move-result-wide v1

    iget-wide v3, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    :try_start_0
    sget-object v5, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Shealth profile time ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Wearable time ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v5, v5, v3

    if-ltz v5, :cond_3

    cmp-long v1, v1, v3

    if-gez v1, :cond_3

    const-string/jumbo v1, "sync with wearable profile"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->height:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->gender:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setGender(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->weightUnit:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->heightUnit:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setDistanceUnit(I)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    const v2, 0x2bf21

    if-lt v1, v2, :cond_1

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    const v2, 0x2bf25

    if-gt v1, v2, :cond_1

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivityType(I)V

    :cond_1
    iget-wide v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->birthday:J

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getyyyyMMddForBirthday(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setBirthDate(Ljava/lang/String;)V

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    invoke-direct {p0, v1, p2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->setDistanceUnit(ILandroid/content/Context;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    if-ne v2, v8, :cond_2

    const-string v2, "DISTANCE_UNIT"

    const v3, 0x29813

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V

    invoke-virtual {p2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v2, "IllegalArgumentException, profile will not be updated."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    :cond_2
    :try_start_1
    const-string v2, "DISTANCE_UNIT"

    const v3, 0x29811

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v1, "<<PROFILE>> : Profile does not need to update"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private updateTodaysGoal(Landroid/content/Context;Landroid/os/Parcelable;Ljava/lang/String;)I
    .locals 7

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    check-cast p2, Lcom/samsung/android/sdk/health/sensor/SGoal;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v1

    iget-wide v3, p2, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const-string/jumbo v1, "user_device__id=? AND goal_type>=? AND set_time>=? AND set_time<=?"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    aput-object p3, v2, v0

    const/4 v3, 0x1

    const v4, 0x9c41

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStartOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v4, "value"

    iget v5, p2, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v4, "set_time"

    iget-wide v5, p2, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateTodaysGoal() Exception occured while Goal update: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v2, "Not Today\'s Goal!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public makeMessageFromIntent(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Landroid/content/Intent;I)Landroid/os/Message;
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "[HS_Wearable_sync]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Received ACTION_DATA_SEND"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "[HS_Wearable_sync]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WearableReceiver action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "com.samsung.android.shealth.ACTION_DATA_SEND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.samsung.android.shealth.ACTION_GEAR2_DATA_SEND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.samsung.android.shealth.ACTION_SBAND_DATA_SEND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_0
    const-string v0, "EXTRA_RESET"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    const-string v0, "EXTRA_RESET"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->resetData(Landroid/content/Context;Landroid/os/Bundle;I)V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v0

    const/16 v2, 0x2727

    if-ne v0, v2, :cond_1

    const-string v0, "EXTRA_RESET"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->resetSleepData(Landroid/content/Context;Landroid/os/Bundle;ILjava/lang/String;)V

    :cond_1
    const-string v0, "EXTRA_SLEEP"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "EXTRA_SLEEP"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getSleepResult([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    move-result-object v0

    :goto_0
    const-string v2, "EXTRA_PEDOMETER_GOAL_HISTORY"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    const-string v2, "EXTRA_PEDOMETER_GOAL_HISTORY"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, p1, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->addPedometerGoalHistory([Landroid/os/Parcelable;Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    const-string v2, "EXTRA_PEDOMETER"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "EXTRA_PEDOMETER"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getPedometerResult([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-result-object v2

    :goto_1
    const-string v3, "EXTRA_STRESS"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "EXTRA_STRESS"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getStressResult([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    move-result-object v3

    :goto_2
    const-string v4, "EXTRA_COACHING_VAR"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "EXTRA_COACHING_VAR"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getCoachingResult(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    move-result-object v4

    :goto_3
    const-string v5, "EXTRA_EXERCISE_RESULT"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "EXTRA_COACHING_RESULT"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "EXTRA_EXERCISE_RESULT"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v5

    const-string v6, "EXTRA_COACHING_RESULT"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getExerciseResult([Landroid/os/Parcelable;[Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    move-result-object v5

    :goto_4
    const-string v6, "EXTRA_HEART_MONITOR"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v1, "EXTRA_HEART_MONITOR"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->getHeartRateMonitor([Landroid/os/Parcelable;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    move-result-object v1

    :cond_3
    const-string v6, "EXTRA_PEDOMETER_GOAL"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    if-eqz p2, :cond_4

    const-string v6, "EXTRA_PEDOMETER_GOAL"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, p1, v7}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->updatePedometerGoal(Landroid/os/Parcelable;Landroid/content/Context;Ljava/lang/String;)V

    :cond_4
    const-string v6, "EXTRA_USER_PROFILE"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "EXTRA_USER_PROFILE"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    invoke-direct {p0, v6, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->updateProfile(Landroid/os/Parcelable;Landroid/content/Context;)V

    :cond_5
    const-string v6, "EXTRA_COACHING_PROFILE"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "EXTRA_COACHING_PROFILE"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    invoke-direct {p0, v6, p1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->updateCoachingProfile([Landroid/os/Parcelable;Landroid/content/Context;)V

    :cond_6
    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iput p4, v6, Landroid/os/Message;->what:I

    const-string v8, "STRESS"

    invoke-virtual {v7, v8, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v3, "SLEEP"

    invoke-virtual {v7, v3, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "PEDO"

    invoke-virtual {v7, v0, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "COACHINGVAR"

    invoke-virtual {v7, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "EXERCISE"

    invoke-virtual {v7, v0, v5}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "HRM"

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    invoke-virtual {v6, v7}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    move-object v1, v6

    :goto_5
    return-object v1

    :cond_7
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    const-string v3, "EXTRA.PEDOMETER is not present"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    goto/16 :goto_1

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "This intent action is not Data_send"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "action is null"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_a
    move-object v5, v1

    goto/16 :goto_4

    :cond_b
    move-object v4, v1

    goto/16 :goto_3

    :cond_c
    move-object v3, v1

    goto/16 :goto_2

    :cond_d
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public makeMessageFromJWearableData(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;I)Landroid/os/Message;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public sendCoachingResponse(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/String;DJ)Z
    .locals 1

    invoke-super/range {p0 .. p7}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->sendCoachingResponse(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/String;DJ)Z

    move-result v0

    return v0
.end method
