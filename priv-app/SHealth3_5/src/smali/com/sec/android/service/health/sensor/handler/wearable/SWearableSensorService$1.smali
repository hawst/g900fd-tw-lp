.class Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 6

    const/4 v5, -0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Insert SWearableSensorService. If after send ACTION_REQUEST, not come here, wait other sync"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onServiceConnected() error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_DEVICETYPE"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "sync_number"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onServiceConnected() deviceType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onServiceConnected() Wearable_sync_Number: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v3

    # invokes: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->isDeviceAvailable(Landroid/content/Intent;)Z
    invoke-static {v1, v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$200(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No Wearable device is available!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.action.SYNC_FAILED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "distance_unit"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " distanceUnit = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->updateDistanceUnit(Ljava/lang/String;Landroid/content/Context;)V

    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    const/4 v2, 0x7

    const/16 v3, 0x2711

    const/16 v4, 0x9

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    :cond_4
    :goto_1
    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v2, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->findDevice(Ljava/util/List;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    # setter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$402(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_5

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$500(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_8

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    :catch_4
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_1

    :catch_5
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    :catch_6
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_0

    :catch_7
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    :catch_8
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceList is null. do nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.samsung.android.shealth.SYNC_ERROR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.samsung.android.shealth.GEAR_SYNC_ERROR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.samsung.android.shealth.SBAND_SYNC_ERROR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$100(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.samsung.android.shealth.GEAR_SYNC_ERROR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "SYNC_ERROR WintipSensorService Error"

    invoke-static {v1, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    const/4 v3, 0x7

    const/16 v4, 0x2711

    const/16 v5, 0xb

    invoke-virtual {v1, v3, v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_c

    move-result-object v0

    :cond_9
    :goto_2
    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3, v0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->findDevice(Ljava/util/List;I)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    # setter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$402(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_10

    :goto_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :catch_9
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    :catch_a
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    :catch_b
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_2

    :catch_c
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_2

    :catch_d
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    :catch_e
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_3

    :catch_f
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_3

    :catch_10
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceList is null. do nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_b
    if-eq v2, v5, :cond_e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "[HS_Wearable_sync]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync data type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_4
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$300(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    const/4 v3, 0x7

    const/16 v4, 0x2711

    const/16 v5, 0xb

    invoke-virtual {v1, v3, v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_13
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_14
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_4 .. :try_end_4} :catch_15

    move-result-object v0

    :cond_c
    :goto_4
    if-eqz v0, :cond_d

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_d

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3, v0, v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->findDevice(Ljava/util/List;I)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    # setter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$402(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_5
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$500(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_11
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_16
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_5 .. :try_end_5} :catch_17
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_18
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_5 .. :try_end_5} :catch_19

    goto/16 :goto_0

    :catch_11
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :catch_12
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    :catch_13
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_4

    :catch_14
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_4

    :catch_15
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_4

    :catch_16
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :catch_17
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :catch_18
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :catch_19
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :cond_d
    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceList is null. do nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0

    :cond_e
    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error doing sync : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto/16 :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onServiceDisconnected() error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    return-void
.end method
