.class public Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry$ConnectionStatus;
    }
.end annotation


# instance fields
.field private mConnectionStatus:I

.field private mLastLaunch:I

.field private mWearableFixedName:Ljava/lang/String;

.field private mWearableId:Ljava/lang/String;

.field private mWearableName:Ljava/lang/String;

.field private mWearablePckgName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearablePckgName:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableName:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableId:Ljava/lang/String;

    iput p4, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mLastLaunch:I

    iput p5, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mConnectionStatus:I

    iput-object p6, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableFixedName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getConnectionStatus()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mConnectionStatus:I

    return v0
.end method

.method public getLastLaunch()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mLastLaunch:I

    return v0
.end method

.method public getWearableFixedName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableFixedName:Ljava/lang/String;

    return-object v0
.end method

.method public getWearableId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableId:Ljava/lang/String;

    return-object v0
.end method

.method public getWearableName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableName:Ljava/lang/String;

    return-object v0
.end method

.method public getWearablePckgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearablePckgName:Ljava/lang/String;

    return-object v0
.end method

.method public setConnectionStatus(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mConnectionStatus:I

    return-void
.end method

.method public setLastLaunch(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mLastLaunch:I

    return-void
.end method

.method public setWearableFixedName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableFixedName:Ljava/lang/String;

    return-void
.end method

.method public setWearableName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableName:Ljava/lang/String;

    return-void
.end method

.method public setWearablePckgName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearablePckgName:Ljava/lang/String;

    return-void
.end method

.method public setmWearableId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/WearableAppRegistry;->mWearableId:Ljava/lang/String;

    return-void
.end method
