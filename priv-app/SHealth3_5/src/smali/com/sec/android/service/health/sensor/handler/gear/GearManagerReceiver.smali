.class public Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final SHEALTH_ACTION_REQ_SYNC_FAIL:Ljava/lang/String; = "com.sec.android.app.shealth.RSP_SYNC_FAIL"

.field private static final SHEALTH_ACTION_RSP_SYNC:Ljava/lang/String; = "com.sec.android.app.shealth.RSP_SYNC"

.field private static final SHEALTH_ACTION_SAP_CONNECT_STATE:Ljava/lang/String; = "com.sec.android.app.shealth.SAP_CONNECT_STATE"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mProtocolListner:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->mProtocolListner:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GearManagerReceiver - onReceive() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "com.sec.android.app.shealth.SAP_CONNECT_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v0, "result"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onReceive() : SHEALTH_ACTION_SAP_CONNECT_STATE - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v1, "com.sec.android.app.shealth.RSP_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "param"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->mProtocolListner:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->mProtocolListner:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyRawDataReceived([B)I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->mProtocolListner:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0, v4}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyRawDataReceived([B)I

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mProtocolListner.notifyRawDataReceived(param.getBytes(SWearableConstants.UTF_8))"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gear/GearManagerReceiver;->mProtocolListner:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0, v4}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyRawDataReceived([B)I

    goto :goto_0
.end method
