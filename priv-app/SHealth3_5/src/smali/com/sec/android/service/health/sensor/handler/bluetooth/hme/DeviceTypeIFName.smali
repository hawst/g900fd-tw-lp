.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/DeviceTypeIFName;
.super Ljava/lang/Object;


# static fields
.field public static final BP:I = 0x2

.field public static final DEVICE_TYPE_FULL_NAME:[Ljava/lang/String;

.field public static final DEVICE_TYPE_NAME:[Ljava/lang/String;

.field public static final EXERCISE:I = 0x6

.field public static final GLUCOSE_METER:I = 0x5

.field public static final MEDICATION:I = 0x7

.field public static final PULSE_OXIMETER:I = 0x1

.field public static final SupportCnt:I = 0x8

.field public static final THERMOMETER:I = 0x3

.field public static final UNKNOWN_DEVICE:I = 0x0

.field public static final WEIGHING_SCALE:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Invalid"

    aput-object v1, v0, v3

    const-string v1, "OXIM"

    aput-object v1, v0, v4

    const-string v1, "BPM"

    aput-object v1, v0, v5

    const-string v1, "THM"

    aput-object v1, v0, v6

    const-string v1, "WS"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "BGM"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "EXE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "MED"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/DeviceTypeIFName;->DEVICE_TYPE_NAME:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "undefined"

    aput-object v1, v0, v3

    const-string v1, "Pulse Oximeter"

    aput-object v1, v0, v4

    const-string v1, "Blood Pressure Monitor"

    aput-object v1, v0, v5

    const-string v1, "Thermometer"

    aput-object v1, v0, v6

    const-string v1, "Weighing Scale"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Glucose Meter"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Exercise Monitor"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Medication Monitor"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/DeviceTypeIFName;->DEVICE_TYPE_FULL_NAME:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
