.class public Lcom/sec/android/service/health/sensor/manager/ScanningManager;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListenerImpl;,
        Lcom/sec/android/service/health/sensor/manager/ScanningManager$scanningStoppedListener;,
        Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final MAX_SLEEP_DURATION:I = 0xa

.field public static final MIN_SLEEP_DURATION:I = 0x5

.field private static final TAG:Ljava/lang/String; = "ScanningManager"


# instance fields
.field IS_PLUGIN_APP:Z

.field filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

.field mDataType:I

.field mDuration:I

.field mErrorFromManager:I

.field mManagerType:I

.field scanningThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;",
            ">;"
        }
    .end annotation
.end field

.field sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/sec/android/service/health/sensor/manager/ISensorListener;Lcom/sec/android/service/health/sensor/manager/util/Filter;IZ)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    iput v1, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mErrorFromManager:I

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->IS_PLUGIN_APP:Z

    iput p2, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    iput p3, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mDuration:I

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    iput-object p5, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->filter:Lcom/sec/android/service/health/sensor/manager/util/Filter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    iput p6, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mDataType:I

    iput-boolean p7, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->IS_PLUGIN_APP:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILcom/sec/android/service/health/sensor/manager/ISensorListener;Lcom/sec/android/service/health/sensor/manager/util/Filter;Z)V
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;-><init>(Landroid/content/Context;IILcom/sec/android/service/health/sensor/manager/ISensorListener;Lcom/sec/android/service/health/sensor/manager/util/Filter;IZ)V

    return-void
.end method


# virtual methods
.method addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V
    .locals 2

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;-><init>(Lcom/sec/android/service/health/sensor/manager/ScanningManager;)V

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->addAbstractManager(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->isThreadScannable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V
    .locals 2

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;-><init>(Lcom/sec/android/service/health/sensor/manager/ScanningManager;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->addAbstractManager(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->addAbstractManager(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {v0, p3}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->addAbstractManager(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->isThreadScannable()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method

.method public cancelScan()Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->interrupt()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->cancel(Z)Z

    :cond_1
    return v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->doInBackground([Landroid/os/Bundle;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/os/Bundle;)Ljava/lang/Void;
    .locals 7

    const/4 v1, 0x1

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v2, 0x0

    const/4 v4, 0x4

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v5, :cond_1

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_1
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v1, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_2
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v6, :cond_3

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_3
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-nez v0, :cond_4

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v1

    invoke-virtual {v1, v5, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v3

    invoke-virtual {v3, v6, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v3

    invoke-virtual {p0, v3, v1, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_4
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    :cond_5
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->IS_PLUGIN_APP:Z

    if-eqz v0, :cond_1a

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_8

    :cond_7
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v4, v6}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_8
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_a

    :cond_9
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_a
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_c

    :cond_b
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_c
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_e

    :cond_d
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/16 v1, 0x13

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_e
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_10

    :cond_f
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_10
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_11

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_12

    :cond_11
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_12
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_13

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_14

    :cond_13
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_14
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_15

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_16

    :cond_15
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_16

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_16
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_17

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_18

    :cond_17
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_18

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_18
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-eqz v0, :cond_19

    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mManagerType:I

    if-ne v0, v4, :cond_1a

    :cond_19
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    if-eqz v0, :cond_1a

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->addToScanThreadList(Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;)V

    :cond_1a
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-interface {v0, v2}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onScanningStarted(I)V

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1b

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->start()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1b
    :goto_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1c

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/ScanningManager$ScanThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v0, "ScanningManager"

    const-string v1, "doInBackground()... InterruptedException received"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_1c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected onCancelled()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onScanningStopped(I)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onScanningStopped(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->scanningThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    iget v1, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->mErrorFromManager:I

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onScanningStopped(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/ScanningManager;->sensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onScanningStopped(I)V

    goto :goto_0
.end method
