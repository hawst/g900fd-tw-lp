.class public Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/database/validator/FieldSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ValidationRule"
.end annotation


# instance fields
.field public constant:Ljava/lang/String;

.field public greaterthan:Ljava/lang/String;

.field public max:Ljava/lang/Double;

.field public min:Ljava/lang/Double;

.field public sum:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/validator/FieldSpec$ValidationRule;->sum:Ljava/util/ArrayList;

    return-void
.end method
