.class Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;
.super Ljava/lang/Object;
.source "RequestParam.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/connectionmanager2/RequestParam;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Pair"
.end annotation


# instance fields
.field private key:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->key:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->value:Ljava/lang/String;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->key:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParam$Pair;->value:Ljava/lang/String;

    return-object v0
.end method
