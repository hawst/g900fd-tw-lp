.class public Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/scontext/SContextListener;
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final gapOfStep:I = 0xa

.field private static mGender:I

.field private static mHeight:F

.field private static mPedometerFeatureLevel:I

.field private static mSContextManager:Landroid/hardware/scontext/SContextManager;

.field private static mWeight:F


# instance fields
.field private apiVersion:I

.field private bListenerRegistered:Z

.field private last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field mContext:Landroid/content/Context;

.field mPedometerDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private mbSContextDiffMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerFeatureLevel:I

    const/4 v0, 0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mGender:I

    const/high16 v0, 0x432a0000    # 170.0f

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mHeight:F

    const/high16 v0, 0x42820000    # 65.0f

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mWeight:F

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mbSContextDiffMode:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->apiVersion:I

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method private detectPedometerMode()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerFeatureLevel:I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "detectPedometerMode - ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerFeatureLevel:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " apiVersion : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->apiVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerFeatureLevel:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    iput-boolean v5, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mbSContextDiffMode:Z

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v1, "StepHandler MODE - Diff value mode"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v4

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "() NoSuchMethodError Exception :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerFeatureLevel:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->apiVersion:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_2

    iput-boolean v5, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mbSContextDiffMode:Z

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v1, "StepHandler MODE - Diff value mode"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mbSContextDiffMode:Z

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v1, "StepHandler MODE - Total value mode"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getStepType(I)I
    .locals 4

    const/4 v0, -0x1

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_8
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private registerPedometerListener()Z
    .locals 8

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "scontext"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->setBasicValue(IFF)V

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/4 v2, 0x2

    sget v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mGender:I

    sget v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mHeight:F

    float-to-double v4, v1

    sget v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mWeight:F

    float-to-double v6, v1

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;IIDD)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "registerWalkForLifeListener bListenerRegistered = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stepsHandler::registerPedometerListener is called bListenerRegistered : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    return v0
.end method

.method private unregisterPedometerListener()I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unregisterPedometerListener : current state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    if-ne v2, v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "unregisterListener is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/4 v2, 0x2

    invoke-virtual {v1, p0, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const-string/jumbo v1, "stepsHandler::unregisterPedometerListener is called"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopReceivingData() onDataStopped() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stepsHandler::onDataStopped is called with errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v2, 0x5

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :goto_1
    return v0

    :catch_0
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "unregisterListener is called but NullPointerException is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v2, "SContext Pedomter is not registered."

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "mSContextManager is null."

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopReceivingData() mProfileHandlerListener is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private updateWflDbByDiffValue(Landroid/hardware/scontext/SContextPedometer;)V
    .locals 21

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getMode()I

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Diff] pedometerContext.getMode() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ## 0 -> Normal_Mode ## 1 -> Logging_Mode ##"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1a

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getArraySize()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTimeStampArray()[J

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistanceDiffArray()[D

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeedArray()[D

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorieDiffArray()[D

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCountDiffArray()[J

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCountDiffArray()[J

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCountDiffArray()[J

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCountDiffArray()[J

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCountDiffArray()[J

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCountDiffArray()[J

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCountDiffArray()[J

    move-result-object v15

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "[Diff] arraySize : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v8, :cond_19

    if-nez v4, :cond_0

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] timestamp is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] timestamp is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_0
    if-nez v5, :cond_1

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] distanceDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] distanceDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_1
    if-nez v6, :cond_2

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] speedArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] speedArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_2
    if-nez v7, :cond_3

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] calorieDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] calorieDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_3
    if-nez v9, :cond_4

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] totalStepDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] totalStepDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_4
    if-nez v10, :cond_5

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] walkFlatStepCntDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] walkFlatStepCntDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_5
    if-nez v11, :cond_6

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] runFlatStepCntDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] runFlatStepCntDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_6
    if-nez v12, :cond_7

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] walkUpStepCntDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] walkUpStepCntDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_7
    if-nez v13, :cond_8

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] walkDownStepCntDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] walkDownStepCntDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_8
    if-nez v14, :cond_9

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] runUpStepCntDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] runUpStepCntDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_9
    if-nez v15, :cond_a

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v16, "[Diff] runDownStepCntDiffArray is null"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "[PEDOMETER_LOGGING_MODE] runDownStepCntDiffArray is null"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_a
    new-array v0, v8, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v16, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v8, :cond_17

    new-instance v17, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    aput-object v17, v16, v3

    aget-object v17, v16, v3

    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, v17

    iput-wide v0, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    if-eqz v4, :cond_b

    aget-object v17, v16, v3

    aget-wide v18, v4, v3

    move-wide/from16 v0, v18

    move-object/from16 v2, v17

    iput-wide v0, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    :cond_b
    if-eqz v5, :cond_c

    aget-object v17, v16, v3

    aget-wide v18, v5, v3

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    :cond_c
    if-eqz v6, :cond_d

    aget-object v17, v16, v3

    aget-wide v18, v6, v3

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    :cond_d
    if-eqz v7, :cond_e

    aget-object v17, v16, v3

    aget-wide v18, v7, v3

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    :cond_e
    if-eqz v9, :cond_f

    aget-object v17, v16, v3

    aget-wide v18, v9, v3

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    :cond_f
    if-eqz v10, :cond_10

    aget-object v17, v16, v3

    aget-wide v18, v10, v3

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    :cond_10
    if-eqz v11, :cond_11

    aget-object v17, v16, v3

    aget-wide v18, v11, v3

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    :cond_11
    if-eqz v12, :cond_12

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move/from16 v18, v0

    aget-wide v19, v12, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    move/from16 v18, v0

    aget-wide v19, v12, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    move/from16 v18, v0

    aget-wide v19, v12, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    :cond_12
    if-eqz v13, :cond_13

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move/from16 v18, v0

    aget-wide v19, v13, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    move/from16 v18, v0

    aget-wide v19, v13, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    move/from16 v18, v0

    aget-wide v19, v13, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    :cond_13
    if-eqz v14, :cond_14

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move/from16 v18, v0

    aget-wide v19, v14, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    move/from16 v18, v0

    aget-wide v19, v14, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    move/from16 v18, v0

    aget-wide v19, v14, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    :cond_14
    if-eqz v15, :cond_15

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move/from16 v18, v0

    aget-wide v19, v15, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    move/from16 v18, v0

    aget-wide v19, v15, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    move/from16 v18, v0

    aget-wide v19, v15, v3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    :cond_15
    aget-object v17, v16, v3

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move/from16 v17, v0

    if-gez v17, :cond_16

    sget-object v17, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v18, "minus updown step is occurred"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v17, v16, v3

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    :cond_16
    sget-object v17, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[Diff] time = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    move-wide/from16 v19, v0

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " distance = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " speed = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " calorie = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " stepStatus = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " totalStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " walkStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " runStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " upDownStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " walkUpStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " walkDownStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " runUpStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " runDownStepCnt = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v16, v3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v3, v0, v4}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v8, :cond_18

    aget-object v9, v16, v3

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/2addr v7, v9

    aget-object v9, v16, v3

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    add-int/2addr v6, v9

    aget-object v9, v16, v3

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v5, v9

    aget-object v9, v16, v3

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v4, v9

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_18
    add-int/lit8 v3, v7, -0xa

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    if-le v3, v4, :cond_19

    const-string v3, "[StepsHandler][logging] step count have gap"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_19
    :goto_2
    return-void

    :cond_1a
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistanceDiff()D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeed()D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorieDiff()D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getUpDownStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->getStepType(I)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-gez v4, :cond_1b

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus updownStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1b
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    if-gez v4, :cond_1c

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus walkUpStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    if-gez v4, :cond_1d

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus walkDownStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1d
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    if-gez v4, :cond_1e

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus runUpStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    if-gez v4, :cond_1f

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus runDownStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1f
    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Diff] time = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " distance = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " speed = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " calorie = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stepStatus = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " totalStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " walkStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " runStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " upDownStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " walkUpStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " walkDownStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " runUpStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " runDownStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v5, 0x0

    invoke-interface {v4, v3, v5}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    :cond_20
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/lit8 v4, v4, -0xa

    iget v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v5, v6

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v3, v5

    if-le v4, v3, :cond_19

    const-string v3, "[StepsHandler] step count have gap"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v3

    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v4, "StepHandler pedometerContext.getMode() NoSuchMethodError"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistanceDiff()D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeed()D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorieDiff()D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getUpDownStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->getStepType(I)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCountDiff()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-gez v4, :cond_21

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus updownStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_21
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    if-gez v4, :cond_22

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus walkUpStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_22
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    if-gez v4, :cond_23

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus walkDownStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_23
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    if-gez v4, :cond_24

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus runUpStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_24
    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    if-gez v4, :cond_25

    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "minus runDownStep is occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_25
    sget-object v4, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Diff] time = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " distance = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " speed = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " calorie = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stepStatus = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " totalStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " walkStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " runStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " upDownStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " walkUpStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " walkDownStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " runUpStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " runDownStepCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v5, 0x0

    invoke-interface {v4, v3, v5}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    iget v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/lit8 v4, v4, -0xa

    iget v5, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    iget v6, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v5, v6

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v3, v5

    if-le v4, v3, :cond_19

    const-string v3, "[StepsHandler] step count have gap"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private updateWlfDbByTotalValue(Landroid/hardware/scontext/SContextPedometer;)V
    .locals 29

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistance()D

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeed()D

    move-result-wide v5

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorie()D

    move-result-wide v7

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCount()J

    move-result-wide v9

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCount()J

    move-result-wide v11

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCount()J

    move-result-wide v13

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getUpDownStepCount()J

    move-result-wide v15

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->getStepType(I)I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCount()J

    move-result-wide v18

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCount()J

    move-result-wide v20

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCount()J

    move-result-wide v22

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCount()J

    move-result-wide v24

    sget-object v26, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "[Total] distance = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " speed = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " calorie = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " stepStatus = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " totalStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " walkStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " runStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " upDownStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " walkUpStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " walkDownStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " runUpStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " runDownStepCnt = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v26, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct/range {v26 .. v26}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    move-wide/from16 v0, v27

    move-object/from16 v2, v26

    iput-wide v0, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    move/from16 v0, v17

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    const-wide/16 v27, 0x0

    cmp-long v27, v9, v27

    if-lez v27, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    if-eqz v27, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    move/from16 v27, v0

    if-lez v27, :cond_3

    double-to-float v0, v3

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    move/from16 v28, v0

    sub-float v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    double-to-float v0, v5

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    move/from16 v28, v0

    sub-float v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    double-to-float v0, v7

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    move/from16 v28, v0

    sub-float v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    long-to-int v0, v9

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    long-to-int v0, v11

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    long-to-int v0, v13

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    long-to-int v0, v15

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v27, v0

    sub-long v27, v18, v27

    move-wide/from16 v0, v27

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v27, v0

    sub-long v27, v20, v27

    move-wide/from16 v0, v27

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v27, v0

    sub-long v27, v22, v27

    move-wide/from16 v0, v27

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v27, v0

    sub-long v27, v24, v27

    move-wide/from16 v0, v27

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    if-eqz v27, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-object/from16 v27, v0

    double-to-float v3, v3

    move-object/from16 v0, v27

    iput v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    double-to-float v4, v5

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    double-to-float v4, v7

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    long-to-int v4, v9

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    long-to-int v4, v11

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    long-to-int v4, v13

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    long-to-int v4, v15

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v18

    long-to-int v4, v0

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v20

    long-to-int v4, v0

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v22

    long-to-int v4, v0

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v24

    long-to-int v4, v0

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    :cond_0
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Total] distance = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " speed = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " calorie = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " stepStatus = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " totalStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " walkStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " runStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " upDownStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " walkUpStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " walkDownStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " runUpStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " runDownStepCnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-interface {v3, v0, v4}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    :cond_1
    move-object/from16 v0, v26

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/lit8 v3, v3, -0xa

    move-object/from16 v0, v26

    iget v4, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v4, v5

    move-object/from16 v0, v26

    iget v5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v4, v5

    if-le v3, v4, :cond_2

    const-string v3, "[StepsHandler] step count have gap"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    double-to-float v0, v3

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    double-to-float v0, v5

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    double-to-float v0, v7

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    long-to-int v0, v9

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    long-to-int v0, v11

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    long-to-int v0, v13

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    long-to-int v0, v15

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    goto/16 :goto_0
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->unregisterPedometerListener()I

    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string v1, "initialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mPedometerDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->detectPedometerMode()V

    const/4 v0, 0x0

    return v0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 4

    iget-object v0, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onSContextChanged type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mbSContextDiffMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mbSContextDiffMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getPedometerContext()Landroid/hardware/scontext/SContextPedometer;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mbSContextDiffMode:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->updateWflDbByDiffValue(Landroid/hardware/scontext/SContextPedometer;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->updateWlfDbByTotalValue(Landroid/hardware/scontext/SContextPedometer;)V

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/16 v0, 0x3ea

    return v0
.end method

.method public setBasicValue(IFF)V
    .locals 3

    const/4 v1, 0x0

    const v0, 0x2e635

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mGender:I

    :goto_0
    cmpg-float v0, p2, v1

    if-gtz v0, :cond_1

    const/high16 v0, 0x432a0000    # 170.0f

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mHeight:F

    :goto_1
    cmpg-float v0, p3, v1

    if-gtz v0, :cond_2

    const/high16 v0, 0x42700000    # 60.0f

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mWeight:F

    :goto_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setBasicValue - Gender : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mGender:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Height : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mHeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Weight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mWeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mGender:I

    goto :goto_0

    :cond_1
    sput p2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mHeight:F

    goto :goto_1

    :cond_2
    sput p3, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mWeight:F

    goto :goto_2
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 4

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    if-ne v1, v0, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData Listener is already registered."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->bListenerRegistered:Z

    if-nez v2, :cond_0

    move v1, v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData() wait for 500 ms"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/4 v2, 0x5

    invoke-interface {v0, v2, v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :goto_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startReceivingData() errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->registerPedometerListener()Z

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "startReceivingData() Exception is occurred"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData() mProfileHandlerListener is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public stopReceivingData()I
    .locals 4

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->unregisterPedometerListener()I

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/scontext/StepsHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopReceivingData() errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method
