.class public Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;
.super Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;


# static fields
.field private static DATA_RECEIVER_ACTION:Ljava/lang/String; = null

.field private static KEY_WEIGHT_DATA:Ljava/lang/String; = null

.field private static final RSP_MEASURED_DATA_SCALE:B = -0x7ft

.field private static final TAG:Ljava/lang/String; = "[HealthSensor]SWeightHandler"


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mContext:Landroid/content/Context;

.field mWeightScaleReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "android.accessory.service.action.SCALE_DATA_IND"

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->DATA_RECEIVER_ACTION:Ljava/lang/String;

    const-string v0, "SCALE_DATA"

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->KEY_WEIGHT_DATA:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;-><init>()V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mWeightScaleReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->DATA_RECEIVER_ACTION:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->KEY_WEIGHT_DATA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;Landroid/text/format/Time;FI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->sendMeasuredDataReport(Landroid/text/format/Time;FI)V

    return-void
.end method

.method private handleReceviedData([B)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x2

    const-string v0, "[HealthSensor]SWeightHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleReceviedData len="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    aget-byte v0, p1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "[HealthSensor]SWeightHandler"

    const-string v1, "handleReceviedData response of user profile"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-byte v0, p1, v3

    if-gez v0, :cond_0

    aget-byte v0, p1, v3

    add-int/lit16 v0, v0, 0x100

    :goto_1
    mul-int/lit16 v1, v0, 0x100

    aget-byte v0, p1, v4

    if-gez v0, :cond_1

    aget-byte v0, p1, v4

    add-int/lit16 v0, v0, 0x100

    :goto_2
    add-int/2addr v0, v1

    int-to-double v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    div-double/2addr v0, v2

    const-string v2, "[HealthSensor]SWeightHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "!!!RESULT!!! weight = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " base??="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, p1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    double-to-float v0, v0

    aget-byte v1, p1, v5

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->sendMeasuredDataReport(Landroid/text/format/Time;FI)V

    goto :goto_0

    :cond_0
    aget-byte v0, p1, v3

    goto :goto_1

    :cond_1
    aget-byte v0, p1, v4

    goto :goto_2

    :pswitch_data_0
    .packed-switch -0x7f
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized sendMeasuredDataReport(Landroid/text/format/Time;FI)V
    .locals 4

    const/4 v3, 0x1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    iput p2, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    if-ne p3, v3, :cond_1

    const v1, 0x1fbd1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const v1, 0x1fbd2

    :try_start_1
    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mWeightScaleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->deinitialize()V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "[HealthSensor]SWeightHandler"

    const-string v1, "Receiver not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 3

    invoke-virtual {p0, p3}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->setSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    invoke-virtual {p0, p2}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->DATA_RECEIVER_ACTION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->mWeightScaleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    invoke-super {p0, p1, p4}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->initiallize(Landroid/content/Context;Ljava/lang/Object;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onError(Ljava/lang/String;IJJ)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onReceive([BJJ)I
    .locals 2

    const-string v0, "[HealthSensor]SWeightHandler"

    const-string v1, "[SAPScaleListener]) onReceive "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v0, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    cmp-long v0, v0, p2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->sessionId:J

    cmp-long v0, v0, p4

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]SWeightHandler"

    const-string/jumbo v1, "onRecieve handleRecevieMData calling"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->handleReceviedData([B)V

    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_0
    const-string v0, "[HealthSensor]SWeightHandler"

    const-string/jumbo v1, "onRecieve handleRecevieMData accesoryId and sessionId are not matching"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/16 v0, 0x3ea

    return v0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    const/4 v2, 0x0

    const-string v0, "[HealthSensor]SWeightHandler"

    const-string v1, " SWeightHandler startReceivingData is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/bluetooth/samsung/SWeightHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :cond_0
    return v2
.end method
