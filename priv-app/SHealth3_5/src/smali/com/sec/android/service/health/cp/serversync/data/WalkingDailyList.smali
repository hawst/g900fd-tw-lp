.class public Lcom/sec/android/service/health/cp/serversync/data/WalkingDailyList;
.super Ljava/lang/Object;


# instance fields
.field public accessaryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/AccessaryItem;",
            ">;"
        }
    .end annotation
.end field

.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public dailyCalorie:D

.field public dailyDistance:D

.field public dailyDuration:I

.field public dailyEndTime:Ljava/lang/String;

.field public dailyRunStep:I

.field public dailyStartTime:Ljava/lang/String;

.field public dailyTotalStep:I

.field public dailyUpDownStep:I

.field public dailyWalkStep:I

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public distanceUnit:Ljava/lang/String;

.field public durationMilli:Ljava/lang/Long;

.field public exerciseId:J

.field public exerciseName:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public heartRate:Ljava/lang/Double;

.field public inputType:Ljava/lang/String;

.field public itemCount:Ljava/lang/Integer;

.field public level:Ljava/lang/Integer;

.field public requestType:Ljava/lang/String;

.field public responseCode:Ljava/lang/String;

.field public valid:Ljava/lang/Boolean;

.field public walkingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/WalkingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/WalkingDailyList;->walkingList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/WalkingDailyList;->accessaryList:Ljava/util/ArrayList;

    return-void
.end method
