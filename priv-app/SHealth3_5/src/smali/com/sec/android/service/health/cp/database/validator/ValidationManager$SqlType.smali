.class public final enum Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/database/validator/ValidationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SqlType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

.field public static final enum INSERT:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

.field public static final enum REPLACE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

.field public static final enum UPDATE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    const-string v1, "INSERT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->INSERT:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->UPDATE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    new-instance v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    const-string v1, "REPLACE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->REPLACE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->INSERT:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->UPDATE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->REPLACE:Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->$VALUES:[Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->$VALUES:[Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/cp/database/validator/ValidationManager$SqlType;

    return-object v0
.end method
