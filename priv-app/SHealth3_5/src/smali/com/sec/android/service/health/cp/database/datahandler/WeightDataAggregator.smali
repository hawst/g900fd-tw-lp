.class public Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    return-void
.end method


# virtual methods
.method protected checkMandatoryFields()Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Data will be skipped. mandatory fields empty"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method getContentValues()Landroid/content/ContentValues;
    .locals 4

    const v3, 0x1fbd2

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "user_device__id"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mUserDeviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    if-ne v1, v3, :cond_0

    const-string/jumbo v1, "weight"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->heightUnit:I

    const v2, 0x249f2

    if-ne v1, v2, :cond_1

    const-string v1, "height"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->height:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertFeetToCm(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_1
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyFatUnit:I

    if-ne v1, v3, :cond_2

    const-string v1, "body_fat"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyFat:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_2
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->visceralFatUnit:I

    if-ne v1, v3, :cond_3

    const-string/jumbo v1, "visceral_fat"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->visceralFat:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_3
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->skeletalMuscleUnit:I

    if-ne v1, v3, :cond_4

    const-string/jumbo v1, "skeletal_muscle"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->skeletalMuscle:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_4
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyWaterUnit:I

    if-ne v1, v3, :cond_5

    const-string v1, "body_water"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyWater:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_5
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->muscleMassUnit:I

    if-ne v1, v3, :cond_6

    const-string/jumbo v1, "muscle_mass"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->muscleMass:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_6
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->boneMassUnit:I

    if-ne v1, v3, :cond_7

    const-string v1, "bone_mass"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->boneMass:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :goto_7
    const-string v1, "activity_metabolic_rate"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->activityMetabolicRate:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "body_age"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyAge:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "body_mass_index"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMassIndex:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "basal_metabolic_rate"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMetabolicRate:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "input_source_type"

    const v2, 0x3f7a2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "sample_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    return-object v0

    :cond_0
    const-string/jumbo v1, "weight"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_0

    :cond_1
    const-string v1, "height"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->height:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_1

    :cond_2
    const-string v1, "body_fat"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyFat:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_2

    :cond_3
    const-string/jumbo v1, "visceral_fat"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->visceralFat:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_3

    :cond_4
    const-string/jumbo v1, "skeletal_muscle"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->skeletalMuscle:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_4

    :cond_5
    const-string v1, "body_water"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyWater:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_5

    :cond_6
    const-string/jumbo v1, "muscle_mass"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->muscleMass:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_6

    :cond_7
    const-string v1, "bone_mass"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->boneMass:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    goto/16 :goto_7
.end method

.method getTableUri()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 3

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    if-eqz v0, :cond_0

    const-string v0, "Weight_DATA"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/WeightDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
