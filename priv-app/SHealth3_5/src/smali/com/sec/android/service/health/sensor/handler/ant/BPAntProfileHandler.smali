.class public Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$7;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]BPSensor"


# instance fields
.field private FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

.field bpmsg:Lcom/garmin/fit/BloodPressureMesg;

.field mBPdata:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field mdata:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->bpmsg:Lcom/garmin/fit/BloodPressureMesg;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mdata:Landroid/os/Bundle;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mBPdata:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$5;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$5;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$6;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$6;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method private readFitFromData([B)V
    .locals 5

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Lcom/garmin/fit/Decode;

    invoke-direct {v0}, Lcom/garmin/fit/Decode;-><init>()V

    new-instance v2, Lcom/garmin/fit/MesgBroadcaster;

    invoke-direct {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;-><init>(Lcom/garmin/fit/Decode;)V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitBloodPressureListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->getUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->getAntDeviceType()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitBloodPressureListener;-><init>(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitBloodPressureListener;->setNotiListener(Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/BloodPressureMesgListener;)V

    :try_start_0
    invoke-virtual {v2, v1}, Lcom/garmin/fit/MesgBroadcaster;->run(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/garmin/fit/FitRuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "[HealthSensor]BPSensor"

    const-string/jumbo v1, "readFitFromData - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v0, "[HealthSensor]BPSensor"

    const-string/jumbo v2, "readFitFromData - FitRuntimeException"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "[HealthSensor]BPSensor"

    const-string/jumbo v1, "readFitFromData - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_1
    throw v0

    :catch_3
    move-exception v1

    const-string v1, "[HealthSensor]BPSensor"

    const-string/jumbo v2, "readFitFromData - IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public getAntDeviceType()I
    .locals 1

    const/16 v0, 0x12

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 1

    const/16 v0, 0x7d1

    return v0
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected requestAccess()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mAntDeviceID:I

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$1;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void
.end method

.method public requestDownloadAllhistory()Z
    .locals 6

    const/4 v3, 0x5

    const/4 v1, 0x1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    if-ne v0, v3, :cond_0

    :goto_0
    return v1

    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->state:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;

    new-instance v2, Lcom/garmin/fit/BloodPressureMesg;

    invoke-direct {v2}, Lcom/garmin/fit/BloodPressureMesg;-><init>()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->bpmsg:Lcom/garmin/fit/BloodPressureMesg;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->mdata:Landroid/os/Bundle;

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)V

    new-instance v4, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$3;

    invoke-direct {v4, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$3;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)V

    new-instance v5, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$4;

    invoke-direct {v5, p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler$4;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;)V

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->requestDownloadMeasurements(ZZLcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStarted(I)V

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->requestDownloadAllhistory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ant/BPAntProfileHandler;->notifyOnDataStarted(I)V

    :cond_0
    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
