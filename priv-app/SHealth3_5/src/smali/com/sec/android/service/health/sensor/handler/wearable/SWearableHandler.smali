.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mContext:Landroid/content/Context;

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

.field public mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private sendErrorMessage(I)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendErrorMessage() errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.shealth.HEALTH_SYNC_ERROR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "EXTRA_ACTION_NAME"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->deinitialize()V

    :cond_0
    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "request() mProtocol = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3e8

    goto :goto_0
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startReceivingData() mProtocol = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyStart()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3e8

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stopReceivingData is called!! mProtocol = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProtocol:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->notifyStop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3e8

    goto :goto_0
.end method
