.class public Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;
.super Lsamsung/database/sqlite/SecSQLiteOpenHelper;


# static fields
.field private static final DATABASE_VERSION:I = 0x5

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string/jumbo v0, "secure_shealth2.db"

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0, p1, v0, v1, v2}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    const-string v1, "DatabaseUpgradeHelper secure_shealth2.db"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onCreate(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 3

    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Old Version ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")  New Version ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    const-string v1, "There is no databse file to upgrade"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onUpgrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onUpgrade oldversion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newversion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;

    new-instance v1, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {v1, p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HDatabaseUpgradeHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;-><init>(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/content/Context;)V

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->upgrade(II)Z

    return-void
.end method
