.class Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onJoined error="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_2

    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "startReceivingData"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$400(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$600(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, " startReceivingData => "

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.shealth.HEALTH_SYNC_ERROR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "EXTRA_ERROR_CODE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "EXTRA_ACTION_NAME"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, " sync is processing, error =com.samsung.android.shealth.HEALTH_SYNC_ERROR"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "device is null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService$2;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->deinitialize()V

    goto :goto_1
.end method

.method public onLeft(I)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onLeft error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onResponseReceived command="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Response ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStateChanged(I)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableSensorService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStateChanged state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
