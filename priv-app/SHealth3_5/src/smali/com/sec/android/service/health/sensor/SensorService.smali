.class public Lcom/sec/android/service/health/sensor/SensorService;
.super Landroid/app/Service;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;,
        Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;,
        Lcom/sec/android/service/health/sensor/SensorService$MyBinder;,
        Lcom/sec/android/service/health/sensor/SensorService$SensorListener;
    }
.end annotation


# static fields
.field public static final ACTION_SENSOR_DATA_ACCEPT_STATUS:Ljava/lang/String; = "com.sec.android.service.health.sensor.contentui"

.field private static final DEBUGING_MODE:Z = true

.field public static final ENABLE_CONTENTUI:Z = true

.field private static final HANDLER_BULK_DATA_RECEIVED:I = 0x6d

.field private static final HANDLER_COMMAND:Ljava/lang/String; = "handler_command"

.field private static final HANDLER_COMMAND_INT:I = 0x65

.field private static final HANDLER_CONTROL_CHANGED:I = 0x71

.field private static final HANDLER_DATA_RECEIVED:I = 0x6c

.field private static final HANDLER_DATA_RECORD:I = 0x6f

.field private static final HANDLER_DATA_STARTED:I = 0x6b

.field private static final HANDLER_DATA_STOPED:I = 0x6e

.field private static final HANDLER_JOINED:I = 0x69

.field private static final HANDLER_LEFT:I = 0x6a

.field private static final HANDLER_RESPONSE:Ljava/lang/String; = "handler_response"

.field private static final HANDLER_SCANNED_DEVICE:Ljava/lang/String; = "handler_scanned_device"

.field private static final HANDLER_SCAN_RESULT_DEVICE_FOUND:I = 0x66

.field private static final HANDLER_SCAN_STARTED:I = 0x67

.field private static final HANDLER_SCAN_STOPPED:I = 0x68

.field private static final HANDLER_STATE_CHANGED:I = 0x70

.field public static final INSERT_EXERCISE_ID:Ljava/lang/String; = "exercise_id"

.field private static final IS_SHEALTH_PLUGIN_APP:Ljava/lang/String; = "isSHealthPlugin"

.field private static final NOTIFICATION_NUM_SENSORSIVCE:I = 0x1388

.field public static SHEALTH_ACTION_DAY_COMPLETE:Ljava/lang/String; = null

.field public static final SHEALTH_DEVICE:Ljava/lang/String; = "shealth_device"

.field private static final SHEALTH_DEVICE_DATA_TYPE:Ljava/lang/String; = "shealth_device_data_type"

.field private static final SHEALTH_DEVICE_ID:Ljava/lang/String; = "shealth_device_ID"

.field private static final SHEALTH_DEVICE_UNIQUE_ID:Ljava/lang/String; = "shealth_unique_id"

.field private static final SHEALTH_RECORD:Ljava/lang/String; = "record"

.field private static final TAG:Ljava/lang/String; = "[HealthSensor]SensorService"

.field private static final TESTNODE:Z

.field public static beginBroadcast:Ljava/lang/Object;

.field private static mCbContentServiceHandler:Landroid/os/Handler;

.field private static mContext:Landroid/content/Context;

.field private static mDBInsertFailureID:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static mProcessId:I


# instance fields
.field private mAm:Landroid/app/IActivityManager;

.field private final mBinder:Lcom/sec/android/service/health/sensor/SensorService$MyBinder;

.field private mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

.field private volatile mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

.field private mDataTypeUriMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final mForegroundToken:Landroid/os/IBinder;

.field private mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

.field private mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;",
            ">;"
        }
    .end annotation
.end field

.field public mSensorDataMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/contentservice/SensorData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mUIConfirmationReceiver:Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "com.sec.shealth.action.DAY_COMPLETE"

    sput-object v0, Lcom/sec/android/service/health/sensor/SensorService;->SHEALTH_ACTION_DAY_COMPLETE:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mDBInsertFailureID:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    new-instance v0, Lcom/sec/android/service/health/sensor/SensorService$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/SensorService$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mCbContentServiceHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mAm:Landroid/app/IActivityManager;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    new-instance v0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;-><init>(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/SensorService$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;-><init>(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/SensorService$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mBinder:Lcom/sec/android/service/health/sensor/SensorService$MyBinder;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .locals 1

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$SensorListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/service/health/sensor/SensorService;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/service/health/sensor/SensorService;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getCallerPackage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/service/health/sensor/SensorService;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/SensorService;->mCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    return-object p1
.end method

.method static synthetic access$1400()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mCbContentServiceHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/service/health/sensor/SensorService;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mDBInsertFailureID:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1800()I
    .locals 1

    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$MyBinder;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mBinder:Lcom/sec/android/service/health/sensor/SensorService$MyBinder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    return-object p1
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/SensorService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/SensorService;->setProcessId(I)V

    return-void
.end method

.method static synthetic access$600(I)Z
    .locals 1

    invoke-static {p0}, Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/manager/ScanningManager;)Lcom/sec/android/service/health/sensor/manager/ScanningManager;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/SensorService;->mScanTask:Lcom/sec/android/service/health/sensor/manager/ScanningManager;

    return-object p1
.end method

.method private static createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<TT;>;)",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<TT;>;"
        }
    .end annotation

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->createDeepCopy()Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] createRemoteCallbackListCopy() originalCallbackList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createUribyDataType()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/4 v1, 0x1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/4 v1, 0x2

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/4 v1, 0x3

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mDataTypeUriMap:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public static generateUniqueID()Ljava/lang/String;
    .locals 3

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "generateUniqueID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getAppNameByPID(I)Ljava/lang/String;
    .locals 4

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/SensorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    if-eqz v0, :cond_0

    iget v2, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v2, p1, :cond_0

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAppNameByPID() - processName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCallerPackage()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCallerPackage() - pkgName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v1, v4

    goto :goto_0
.end method

.method private getCallingPackage()Ljava/lang/String;
    .locals 5

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SensorListener] getCallingPackage pakName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " caller : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static declared-synchronized getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    .locals 3

    const-class v1, Lcom/sec/android/service/health/sensor/SensorService;

    monitor-enter v1

    if-nez p0, :cond_0

    :try_start_0
    const-string v0, "[HealthSensor]SensorService"

    const-string v2, "[SensorListener] getDeviceId() device is null!!"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    sget-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getEndOfDay(J)J
    .locals 4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    const-string v0, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calendar time zone offset "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xf

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter v1

    :try_start_0
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v0, 0xb

    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    const/16 v2, 0x3b

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    const/16 v2, 0x3b

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    const/16 v2, 0x3e7

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfHour(J)J
    .locals 4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    const-string v0, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calendar time zone offset "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xf

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter v1

    :try_start_0
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v0, 0xc

    const/16 v2, 0x3b

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    const/16 v2, 0x3b

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    const/16 v2, 0x3e7

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static getProcessId()I
    .locals 1

    sget v0, Lcom/sec/android/service/health/sensor/SensorService;->mProcessId:I

    return v0
.end method

.method public static getStartOfDay()J
    .locals 4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v0, 0xb

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfDay(J)J
    .locals 4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v0, 0xb

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfHour(J)J
    .locals 4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v0, 0xc

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private isPluginApp(I)Z
    .locals 4

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/SensorService;->getAppNameByPID(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.service.health"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "access from healthservice"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-nez v0, :cond_1

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "app_package == null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_1
    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPluginApp isPlugin : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " processId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "[HealthSensor]SensorService"

    const-string v2, "app_package != null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method

.method private static isStreamingData(I)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/16 v1, 0xf

    if-eq p0, v1, :cond_0

    const/16 v1, 0x15

    if-eq p0, v1, :cond_0

    const/16 v1, 0xa

    if-eq p0, v1, :cond_0

    const/4 v1, 0x6

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    const/16 v1, 0xb

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    const/16 v1, 0xc

    if-eq p0, v1, :cond_0

    const/16 v1, 0xd

    if-eq p0, v1, :cond_0

    const/16 v1, 0xe

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x11

    if-eq p0, v1, :cond_0

    const/16 v1, 0x13

    if-eq p0, v1, :cond_0

    const/16 v1, 0x12

    if-eq p0, v1, :cond_0

    const/16 v1, 0x14

    if-eq p0, v1, :cond_0

    const/16 v1, 0x10

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] isStreamingData() bStream : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dataType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method private setProcessId(I)V
    .locals 0

    sput p1, Lcom/sec/android/service/health/sensor/SensorService;->mProcessId:I

    return-void
.end method


# virtual methods
.method public getConfirmationData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/contentservice/_ContentValueData;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    sget-object v1, Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    instance-of v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    if-eqz v2, :cond_2

    new-instance v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v2}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    const-string/jumbo v3, "weight"

    iput-object v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    iput v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    iput v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->device_unit:I

    iget-wide v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    iput-wide v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weight:F

    invoke-static {v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v2}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMassIndex:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    const-string v3, "body_mass_index"

    iput-object v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v0

    iput v0, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->weightUnit:I

    iput v0, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->device_unit:I

    iget-wide v3, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->time:J

    iput-wide v3, v2, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;->bodyMassIndex:F

    invoke-static {v0}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    instance-of v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    const-string/jumbo v2, "systolic"

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    const-string v2, "diastolic"

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    const-string/jumbo v2, "pulse"

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucose:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    const-string v2, "glucose"

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    iput v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->time:J

    iput-wide v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucose:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    instance-of v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperature:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    const-string/jumbo v2, "systolic"

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureUnit:I

    iput v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->time:J

    iput-wide v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperature:F

    invoke-static {v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    new-instance v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>()V

    const-string v2, "__all__"

    iput-object v2, v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public getContentUri(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Landroid/net/Uri;
    .locals 1

    instance-of v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onBind called--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mBinder:Lcom/sec/android/service/health/sensor/SensorService$MyBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorService] onCreate() DEBUGING_MODE = true"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p0, Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/SensorService$SensorListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->initProfileManagerFactory(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->getInstance()Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/ProtocolFactory;->intiProtocolFactory()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/SensorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->initialize(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/SensorService;->createUribyDataType()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SENSOR_SERVICE"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mServiceLooper:Landroid/os/Looper;

    new-instance v0, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;-><init>(Lcom/sec/android/service/health/sensor/SensorService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    new-instance v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;-><init>(Lcom/sec/android/service/health/sensor/SensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mUIConfirmationReceiver:Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.sec.android.service.health.sensor.contentui"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.sec.android.service.health.sensor.record"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService;->mUIConfirmationReceiver:Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mDBInsertFailureID:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorService] onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SensorService::onDestroy is called"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->deinitialize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;

    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService;->mUIConfirmationReceiver:Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mServiceLooper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorService] onStartCommand() - flags : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAMSUNG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "This device is not SAMSUNG device. finish"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onUnbind called"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
