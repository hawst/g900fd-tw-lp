.class public abstract Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    const-string v0, "USBProtocol"

    const-string v1, "deinitialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public abstract getBaudRate()I
.end method

.method public abstract sendPing()V
.end method
