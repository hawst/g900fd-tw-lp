.class Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBodyWeightBroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1$1;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewBodyWeightBroadcast(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;Ljava/math/BigDecimal;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$BodyWeightStatus;",
            "Ljava/math/BigDecimal;",
            ")V"
        }
    .end annotation

    if-nez p5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p5}, Ljava/math/BigDecimal;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->prevWeight:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->access$700()I

    move-result v1

    if-eq v1, v0, :cond_0

    if-lez v0, :cond_0

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->prevWeight:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->access$702(I)I

    goto :goto_0
.end method
