.class public abstract Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;
.super Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;

# interfaces
.implements Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;
.implements Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "[HealthSensor]SapBtHandler"


# instance fields
.field protected appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

.field bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field mContext:Landroid/content/Context;

.field private mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

.field protected sessionId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/BTProfileHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    return-void
.end method


# virtual methods
.method public closeConnection()Z
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "closeConnection"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeConnection sessionId  BtAddress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-object v2, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->closeConnection(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "closeConnection sessionId || mSAPAccManager are null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closeSession()Z
    .locals 4

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "closeSession"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeSession sessionId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " BtAddress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-object v2, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->closeSession(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "closeSession - sessionId == -1 or mSAPAccManager == null or appcessoryDevice == null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public deinitialize()V
    .locals 2

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "deinitialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->stopSAPService()V

    return-void
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "initiallize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectedDevice()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onAccessoryAttached(J)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onAccessoryAttached accessoryId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onAccessoryDetached(J)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onAccessoryDetached accessoryId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    return-void
.end method

.method public onCloseSessionRequest(JJ)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCloseSessionRequest accessoryId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sessionId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    cmp-long v0, p3, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v0, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "onCloseSessionRequest close session for this device and Session only"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    :cond_0
    return-void
.end method

.method public onDeviceFound(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)V
    .locals 2

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "[SAPEventHandler]) onDeviceFound "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onError(III)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SAPEventHandler]) onError transportId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " accID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onOpenSessionAccepted(JJ)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SAPEventHandler]) onOpenSessionAccepted accessoryId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sessionId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onOpenSessionRequest(JJ)V
    .locals 5

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onOpenSessionRequest accessoryId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sessionId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->getListofAvailableDevices(I)Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "onOpenSessionRequest availDevices:: is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v0, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_5

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v3, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    cmp-long v3, v3, p1

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "onOpenSessionRequest:: found device"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    if-nez v0, :cond_5

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "onOpenSessionRequest mStoredBtAccessory:: is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "onOpenSessionAccepted::Handled"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide p3, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onOpenSessionRequest Adding addAccessoryListener sessionId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  appcessoryId  ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v2, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->registerDataListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "onOpenSessionRequest registerData Listenerr SUCCESS"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "onOpenSessionRequest registerData Listenerr FAILURE"

    invoke-static {v0, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onPairedStatus(IJZ)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SAPEventHandler]) onPairedStatus arg0 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arg1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arg2 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSessionClosed(JJ)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSessionClosed accessoryId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sessionId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    cmp-long v0, v0, p3

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    iget-wide v0, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "APP calling closeConnection, Session Closed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->sessionId:J

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]SapBtHandler"

    const-string v1, "APP calling closeConnection, accessoryId and sessionId not matching"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onTransportStatusChanged(II)V
    .locals 3

    const-string v0, "[HealthSensor]SapBtHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SAPEventHandler]) onTransportStatusChanged status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " transportId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 2

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "startReceivingData is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->startSAPService()V

    const/4 v0, 0x0

    return v0
.end method

.method public startSAPService()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "startSAPService()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mContext:Landroid/content/Context;

    const-wide/16 v1, 0x6

    invoke-static {v0, v1, v2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->getDefaultManager(Landroid/content/Context;J)Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-nez v0, :cond_1

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "mSAPAccManager == null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->registerEventListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;I)Z

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 3

    const/4 v2, 0x0

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "stopReceivingData is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->closeConnection()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "stopScale close connection Success"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->deinitialize()V

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->appcessoryDevice:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->stopSAPService()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    return v2

    :cond_0
    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "stopSBand : close connection is failed !"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopSAPService()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SapBtHandler"

    const-string/jumbo v1, "stopSAPService()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->deRegisterEventListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;I)Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->deInit()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/SAPBTHandler;->mSAPAccManager:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    :cond_0
    return-void
.end method
