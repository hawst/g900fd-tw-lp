.class public Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler$DummyDataThread;
    }
.end annotation


# static fields
.field private static final CACHED_DATA_INTERVAL_CNT:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TEST_FLAG:Z


# instance fields
.field private bListenerRegistered:Z

.field private mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field private mCachedDataCurrentCount:I

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field mContext:Landroid/content/Context;

.field private mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler$DummyDataThread;

.field mHumidDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mHumiditySensor:Landroid/hardware/Sensor;

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->bListenerRegistered:Z

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumiditySensor:Landroid/hardware/Sensor;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumidDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    return-void
.end method

.method private registerHumidityListener()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumiditySensor:Landroid/hardware/Sensor;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumiditySensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Humidity sensor was "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v1, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "registered"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :cond_0
    return v0

    :cond_1
    const-string/jumbo v0, "not "

    goto :goto_0
.end method

.method private returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    aput-object p1, v0, v1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    :cond_0
    return-void
.end method

.method private stopDataAndClearListeners()V
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->unregisterHumidityListener()V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    return-void
.end method

.method private unregisterHumidityListener()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterHumidityListener : current state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unregisterSensorManager is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->bListenerRegistered:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->stopDataAndClearListeners()V

    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumidDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public get_ShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumidDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumidDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v0, 0x0

    return v0
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mSensorManager:Landroid/hardware/SensorManager;

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HumidityListener onSensorChanged : event.accuracy = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/hardware/SensorEvent;->accuracy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HumidityListener onSensorChanged : Humidity = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;->time:J

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;->humidity:F

    :try_start_0
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    float-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;->accuracy:I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "humidity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;->humidity:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " accuracy : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;->accuracy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    :cond_0
    return-void

    :catch_0
    move-exception v2

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->TAG:Ljava/lang/String;

    const-string v3, "java.lang.ArrayIndexOutOfBoundsException event.values[2] is not supported for humidity"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p1, Landroid/hardware/SensorEvent;->accuracy:I

    if-lez v2, :cond_1

    const/4 v0, 0x3

    :cond_1
    iput v0, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;->accuracy:I

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public set_ShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mHumidDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 4

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mCachedDataCurrentCount:I

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->bListenerRegistered:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->registerHumidityListener()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->bListenerRegistered:Z

    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/16 v3, 0x12

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v3, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    return v1

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->stopDataAndClearListeners()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/humidity/HumidityHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/16 v1, 0x12

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    return v2
.end method
