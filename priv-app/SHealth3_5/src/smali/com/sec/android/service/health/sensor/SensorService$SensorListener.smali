.class final Lcom/sec/android/service/health/sensor/SensorService$SensorListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/manager/ISensorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/SensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SensorListener"
.end annotation


# instance fields
.field private data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field private healthArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/contentservice/_ContentValueData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/SensorService;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/SensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/SensorService;Lcom/sec/android/service/health/sensor/SensorService$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;-><init>(Lcom/sec/android/service/health/sensor/SensorService;)V

    return-void
.end method


# virtual methods
.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIZLjava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v4, -0x1

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onDataReceived() deviceID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deviceObjId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " record : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Extra: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/utils/LogMessage;->secD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z
    invoke-static {v0, p7}, Lcom/sec/android/service/health/sensor/SensorService;->access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->setProcessId(I)V
    invoke-static {v1, p7}, Lcom/sec/android/service/health/sensor/SensorService;->access$500(Lcom/sec/android/service/health/sensor/SensorService;I)V

    invoke-static {p4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->isWearableDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->systolic:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->diastolic:F

    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;->pulse:I

    invoke-static {v0}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/sec/android/service/health/contentservice/SensorData;

    invoke-direct {v1}, Lcom/sec/android/service/health/contentservice/SensorData;-><init>()V

    iput-object p2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    iput-object p1, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/service/health/contentservice/SensorData;->deviceID:Ljava/lang/String;

    iput-object p4, v1, Lcom/sec/android/service/health/contentservice/SensorData;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v0, p9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, p9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    check-cast v0, Ljava/util/ArrayList;

    goto :goto_1

    :cond_3
    if-eqz p3, :cond_6

    if-eqz p1, :cond_6

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v2

    const/16 v3, 0x6c

    invoke-virtual {v2, v3, v4, v4, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "_Health"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v3, "isSHealthPlugin"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz p2, :cond_4

    const-string v0, "_Extra"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_4
    if-eqz p1, :cond_5

    const-string/jumbo v0, "shealth_device_data_type"

    invoke-interface {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_2
    const-string/jumbo v0, "shealth_device"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "shealth_unique_id"

    invoke-virtual {v2, v0, p9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "record"

    invoke-virtual {v2, v0, p8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "shealth_device_data_type"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    :cond_6
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onDataReceived callbackList or data is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIZLjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;",
            "[",
            "Landroid/os/Bundle;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v3, -0x1

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onDataReceived() [bulk] deviceID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DeviceObjId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " record : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/utils/LogMessage;->secD(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    const/16 v2, 0x6d

    invoke-virtual {v1, v2, v3, v3, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "_Health"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v2, "_Extra"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string/jumbo v2, "shealth_device"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v2, "record"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "shealth_unique_id"

    invoke-virtual {v1, v2, p9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onDataReceived[] callbackList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDataRecordStarted(ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZ)V
    .locals 4

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataRecordStarted dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DevObjectId :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " record : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onDataRecordStarted device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    const/16 v1, 0x6f

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "exercise_id"

    invoke-virtual {v1, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v2, "shealth_device"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onDataStarted(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onDataStarted() dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DevObjectId :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " record : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->setProcessId(I)V
    invoke-static {v0, p8}, Lcom/sec/android/service/health/sensor/SensorService;->access$500(Lcom/sec/android/service/health/sensor/SensorService;I)V

    if-nez p4, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onDataStarted device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z
    invoke-static {v0, p8}, Lcom/sec/android/service/health/sensor/SensorService;->access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z

    move-result v0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v2

    const/16 v3, 0x6b

    invoke-virtual {v2, v3, p1, p2, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "isSHealthPlugin"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "shealth_device_data_type"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "exercise_id"

    invoke-virtual {v2, v0, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "shealth_device"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "record"

    invoke-virtual {v2, v0, p9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onDataStarted callbackList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDataStopped(IILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;JIIZLjava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            "JIIZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SensorListener] onDataStopped() dataType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " exerciseId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " processId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DevObjectId :  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " record : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p9

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p4, :cond_0

    const-string v1, "[HealthSensor]SensorService"

    const-string/jumbo v2, "onDataStopped device is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    move/from16 v0, p8

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isPluginApp(I)Z
    invoke-static {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$400(Lcom/sec/android/service/health/sensor/SensorService;I)Z

    move-result v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {p4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->isWearableDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isShealth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isStreamingData : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {p1}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {p1}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    const-string v2, "[HealthSensor]SensorService"

    const-string v3, "arraydata is not null: "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/service/health/contentservice/SensorData;

    iget-object v2, v2, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v2

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v8, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v2, v8}, Lcom/sec/android/service/health/sensor/SensorService;->getConfirmationData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->healthArray:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->healthArray:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/SensorService;->getContentUri(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/SensorService;->getContentUri(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/SensorService;->getContentUri(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    const-string v1, "[HealthSensor]SensorService"

    const-string v2, "Show HealthContentShow : "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    const-class v3, Lcom/sec/android/service/health/contentservice/HealthContentShow;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "content_data"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string/jumbo v2, "requestTime"

    invoke-virtual {v1, v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "action"

    const/16 v3, 0x3e9

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v2, "streaming"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v2, "uniqueKey"

    move-object/from16 v0, p10

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "uri"

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v3, v4}, Lcom/sec/android/service/health/sensor/SensorService;->getContentUri(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "deviceobjectid"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "deviceid"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "device_name"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-virtual {v2, v1}, Lcom/sec/android/service/health/sensor/SensorService;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    if-eqz p3, :cond_5

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v2

    const/16 v3, 0x6e

    invoke-virtual {v2, v3, p1, p2, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "isSHealthPlugin"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v3, "shealth_device_data_type"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "shealth_device"

    invoke-virtual {v2, v3, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v3, "record"

    move/from16 v0, p9

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v3, "shealth_unique_id"

    move-object/from16 v0, p10

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_5
    const-string v1, "[HealthSensor]SensorService"

    const-string/jumbo v2, "onDataStopped callbackList is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 3

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] onDeviceFound()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onDeviceFound device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    const/16 v1, 0x66

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "handler_scanned_device"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onDeviceFound mScanningCallback is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onJoined(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ")V"
        }
    .end annotation

    const/4 v8, 0x4

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onJoined() error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deviceObject :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p4, :cond_1

    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onJoined device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_8

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    const/16 v2, 0x69

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, p1, v3, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    if-ne v0, v8, :cond_3

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    :goto_1
    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v5

    aput-object v6, v4, v5

    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEVICE_ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " already present in user device table : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    if-nez v1, :cond_7

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    if-ne v2, v8, :cond_5

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "device_id"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "connectivity_type"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "device_type"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v3, "model"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "custom_name"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "device_group_type"

    const v4, 0x57e41

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "[HealthSensor]SensorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Adding to user_device DEVICE_ID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding to user_device Model : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "[HealthSensor]SensorService"

    const-string v3, "Adding to user_device Group : Mobile"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_3
    :try_start_2
    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_4
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_1

    :cond_4
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v1, v7

    goto/16 :goto_2

    :cond_5
    const-string v2, "_id"

    invoke-virtual {v0, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "connectivity_type"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "device_type"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "device_id"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "custom_name"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const-string v2, "device_group_type"

    const v3, 0x57e42

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "[HealthSensor]SensorService"

    const-string v3, "Adding to user_device Group : External"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding to user_device : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    :pswitch_1
    :try_start_4
    const-string v2, "device_group_type"

    const v3, 0x57e43

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "[HealthSensor]SensorService"

    const-string v3, "Adding to user_device Group : Companion"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :catch_0
    move-exception v0

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onJoined() Exception occured while UserDevice insert: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_7
    const-string/jumbo v2, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "custom_name"

    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    :try_start_5
    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v2, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_4

    :catch_1
    move-exception v0

    :try_start_6
    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onJoined() Exception occured while UserDevice update: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_4

    :cond_8
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onJoined callbackList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v7

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLeft(ILjava/lang/Integer;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ")V"
        }
    .end annotation

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onLeft() error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p3, :cond_0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    const/16 v2, 0x6a

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, p1, v3, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onLeft callbackList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;Ljava/lang/Integer;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;",
            "Ljava/lang/Integer;",
            "I)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] onResponseReceived()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "handler_command"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "handler_response"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v2

    const/16 v3, 0x65

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onScanningStarted(I)V
    .locals 4

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onScanningStarted() error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    const/16 v1, 0x67

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onScanningStarted mScanningCallback is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onScanningStopped(I)V
    .locals 4

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onScanningStopped() error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    const/16 v1, 0x68

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mScanningCallback:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$200(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onScanningStopped mScanningCallback is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onScanningStopped(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;",
            ">;)V"
        }
    .end annotation

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onScanningStopped() with callbackList error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v0

    const/16 v1, 0x68

    const/4 v2, -0x1

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onScanningStopped callbackList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStateChanged(ILcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;",
            ">;)V"
        }
    .end annotation

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] onStateChanged() state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {p2}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    const/16 v2, 0x70

    const/4 v3, -0x1

    invoke-virtual {v1, v2, p1, v3, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/SensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onStateChanged callbackList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
