.class public Lcom/sec/android/service/health/cp/serversync/syncadapter/task/ProfileTask;
.super Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;


# static fields
.field private static final PROFILE_CODE:Ljava/lang/String; = "profile"


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;-><init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/remote/ActivityListenerStub;II)V

    return-void
.end method


# virtual methods
.method protected getDeletedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getDeletedProfileData(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getDetailHistoryData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getProfileDetailHistoryData(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getSyncTypeString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "profile"

    return-object v0
.end method

.method protected getTableName()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getUnsyncedLocalData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApiForSetToServer;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->getModifiedProfile(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected updateItemsLocally(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ImageUrlDetails;",
            ">;"
        }
    .end annotation

    invoke-static {p2}, Lcom/sec/android/service/health/cp/serversync/util/SyncProfileUtil;->addProfile(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected updateSyncStatusField(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;)V
    .locals 4

    iget-wide v0, p1, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$SyncedRowData;->rowId:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/emulated/0/SHealth3/cache/img/user_photo.jpg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/serversync/common/SyncPreference;->setLastProfileImageSyncedTime(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/cp/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/service/health/cp/configuration/SharedPrefManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/configuration/SharedPrefManager;->getProfilePref()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "in_sync"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
