.class public Lcom/sec/android/service/health/cp/serversync/data/FoodData;
.super Ljava/lang/Object;


# instance fields
.field public appId:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceCreateTimeMeal:Ljava/lang/String;

.field public deviceMealPKId:J

.field public deviceModifyTimeMeal:Ljava/lang/String;

.field public deviceTimeZoneMeal:Ljava/lang/String;

.field public foodList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/FoodDataItem;",
            ">;"
        }
    .end annotation
.end field

.field public imageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/FoodImageItem;",
            ">;"
        }
    .end annotation
.end field

.field public inputType:Ljava/lang/String;

.field public intakeTime:Ljava/lang/String;

.field public meal:Ljava/lang/String;

.field public requestType:Ljava/lang/String;

.field public totalCalorie:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FoodData;->foodList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/FoodData;->imageList:Ljava/util/ArrayList;

    return-void
.end method
