.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

.field private totalNum:I


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->totalNum:I

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private getTotalDataNumber([B)I
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    new-array v1, v4, [B

    const/4 v0, -0x1

    array-length v2, v1

    invoke-static {p1, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_GLUCOSE_RECORD_NUMBER:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-array v0, v4, [B

    array-length v1, v0

    invoke-static {p1, v4, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-byte v0, v0, v3

    if-gez v0, :cond_0

    add-int/lit16 v0, v0, 0x100

    goto :goto_0
.end method


# virtual methods
.method public doAction()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_GLUCOSE_RECORD_NUMBER:[B

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method protected getTotalNumber()I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->totalNum:I

    return v0
.end method

.method public responseReceived([B)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->getTotalDataNumber([B)I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->totalNum:I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendAck()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readUnitCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V

    return-void
.end method
