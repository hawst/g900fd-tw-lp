.class public Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$DummyDataThread;
    }
.end annotation


# static fields
.field private static final CACHED_DATA_INTERVAL_CNT:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TEST_FLAG:Z = false

.field private static final TYPE_BASE:I = 0x10000

.field public static final TYPE_UV_RAY:I = 0x1001d


# instance fields
.field private bListenerRegistered:Z

.field private dataCollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field private mCachedDataCurrentCount:I

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field mContext:Landroid/content/Context;

.field private mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$DummyDataThread;

.field private mMaxMaximTime:I

.field private mMaximUvSensor:Lcom/maximintegrated/bio/uv/MaximUVSensor;

.field public mMeasuringTimer:Landroid/os/CountDownTimer;

.field private mMinMaximTime:I

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field mUvDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private mUvSensor:Landroid/hardware/Sensor;

.field private mVendor:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvSensor:Landroid/hardware/Sensor;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMaximUvSensor:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMinMaximTime:I

    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMaxMaximTime:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMinMaximTime:I

    int-to-long v2, v1

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;JJ)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMeasuringTimer:Landroid/os/CountDownTimer;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;)Ljava/lang/Integer;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->getMaxValuedataCollection()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getMaxValuedataCollection()Ljava/lang/Integer;
    .locals 4

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v3, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    move-object v2, v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private isMaximSensorDevice()Z
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isMaxim mVendor =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private registerUvListener()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "MAXIM"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mContext:Landroid/content/Context;

    const v2, 0x1001d

    invoke-direct {v0, v1, v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMaximUvSensor:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMaximUvSensor:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMinMaximTime:I

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMaxMaximTime:I

    invoke-virtual {v0, p0, v1, v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->registerListener(Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;II)Z

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Maxim Uv sensor was "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "registered"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uv sensor was "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v1, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "registered"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "not "

    goto :goto_1
.end method

.method private returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    aput-object p1, v0, v1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uv returnDataViaCaching : Uv value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    :cond_0
    return-void
.end method

.method private stopDataAndClearListeners()V
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->unregisterUvListener()V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    return-void
.end method

.method private unregisterUvListener()V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterUvListener : current state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMaximUvSensor:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMaximUvSensor:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    invoke-virtual {v0, p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->unregisterListener(Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;)V

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    const-string v1, "Maxim unregisterSensorManager is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unregisterSensorManager is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    goto :goto_0
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->stopDataAndClearListeners()V

    return-void
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public get_ShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 2

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1001d

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvSensor:Landroid/hardware/Sensor;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mVendor:Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mSensorManager:Landroid/hardware/SensorManager;

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onMaximUVSensorChanged(Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;)V
    .locals 4

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    iget v1, p1, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    float-to-int v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uvlistener onSensorChanged : Maxim Uv value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index =  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const v1, 0x1001d

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uvlistener onSensorChanged : event.accuracy = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/hardware/SensorEvent;->accuracy:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->time:J

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    float-to-int v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uvlistener onSensorChanged : Uv value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index =  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->isMaximSensorDevice()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;->index:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->returnDataViaCaching(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public set_ShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mUvDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-void
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 4

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mCachedDataCurrentCount:I

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->registerUvListener()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->isMaximSensorDevice()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/16 v3, 0x14

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-interface {v2, v3, v0}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    return v1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->stopDataAndClearListeners()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    const/16 v1, 0x14

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ultraviolet/UvHandler;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    return v2
.end method
