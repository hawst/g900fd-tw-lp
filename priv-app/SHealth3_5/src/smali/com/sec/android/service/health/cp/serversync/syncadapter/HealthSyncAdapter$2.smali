.class Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$2;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onReceive(): action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.sec.android.service.health.ContentProviderAccessible"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "onReceive(): DB is now accessible. unregistering the receiver"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter$2;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->unRegisterReceiver(Landroid/content/Context;)V
    invoke-static {v0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;Landroid/content/Context;)V

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/HealthSyncAdapter;->lockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
