.class public Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/cp/common/ICompatibilityManager;


# static fields
.field private static final BIRTH_DATE:Ljava/lang/String; = "birth_date"

.field private static final BIRTH_DAY:Ljava/lang/String; = "birth_day"

.field private static final BIRTH_MONTH:Ljava/lang/String; = "birth_month"

.field private static final BIRTH_YEAR:Ljava/lang/String; = "birth_year"

.field private static final GENDER:Ljava/lang/String; = "gender"

.field private static final HEIGHT:Ljava/lang/String; = "height"

.field private static final HEIGHT_UNIT:Ljava/lang/String; = "height_unit"

.field public static KIES_SHARED_PREF_FILE:Ljava/lang/String; = null

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final PREF_TYPE_BOOLEAN:I = 0x4

.field private static final PREF_TYPE_FLOAT:I = 0x3

.field private static final PREF_TYPE_INT:I = 0x5

.field private static final PREF_TYPE_LONG:I = 0x1

.field private static final PREF_TYPE_STRING:I = 0x2

.field private static final SECURE_BIRTH_DAY:Ljava/lang/String; = "secure_birth_day"

.field private static final SECURE_BIRTH_MONTH:Ljava/lang/String; = "secure_birth_month"

.field private static final SECURE_BIRTH_YEAR:Ljava/lang/String; = "secure_birth_year"

.field private static final SECURE_HEIGHT:Ljava/lang/String; = "secure_height"

.field private static final SECURE_PREFIX:Ljava/lang/String; = "secure_"

.field private static final SECURE_WEIGHT:Ljava/lang/String; = "secure_weight"

.field public static SHARED_PREF_FOLDER_NAME:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String;

.field private static final TIMEZONE_KEY:Ljava/lang/String; = "fixed_time_zone_id"

.field private static final WEIGHT:Ljava/lang/String; = "weight"

.field private static final WEIGHT_UNIT:Ljava/lang/String; = "weight_unit"

.field public static mPrefsKeyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

.field private mContext:Landroid/content/Context;

.field private mPref:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v0, "shared_prefs"

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->SHARED_PREF_FOLDER_NAME:Ljava/lang/String;

    const-string/jumbo v0, "prefs.bin"

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->KIES_SHARED_PREF_FILE:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mPrefsKeyList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mPref:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getLatestWeightFromDB(Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 7

    const/4 v1, 0x0

    const/high16 v0, 0x43fa0000    # 500.0f

    const/high16 v2, 0x40000000    # 2.0f

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, "get Latest Weight From DB"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v3, "select weight from weight ORDER BY sample_time desc LIMIT 1 "

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "weight"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    sget-object v4, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "weight value is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    cmpl-float v4, v3, v0

    if-lez v4, :cond_2

    :goto_0
    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setWeight(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    cmpg-float v0, v3, v2

    if-gez v0, :cond_4

    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public static putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v0, "value"

    invoke-virtual {v2, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "shared_pref"

    const-string v3, "key=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "key"

    invoke-virtual {v2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "shared_pref"

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    :cond_0
    return-void
.end method

.method private setActivityType(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " setActivityType "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "activity_type"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "activity_type"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setActivityType activity_type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " k_activity_type "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setActivityType(I)V

    :cond_0
    return-void

    :pswitch_0
    const v0, 0x2bf21

    goto :goto_0

    :pswitch_1
    const v0, 0x2bf22

    goto :goto_0

    :pswitch_2
    const v0, 0x2bf23

    goto :goto_0

    :pswitch_3
    const v0, 0x2bf24

    goto :goto_0

    :pswitch_4
    const v0, 0x2bf25

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setBirthDate(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v2, -0x1

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " setBirthDate "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "birth_year"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "birth_year"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " birth_year "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    :goto_0
    const-string v0, "birth_month"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "birth_month"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " birth_month "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v0

    :goto_1
    const-string v0, "birth_day"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "birth_day"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v4, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " birth_day "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    if-eq v1, v2, :cond_0

    if-eq v3, v2, :cond_0

    if-eq v0, v2, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v1}, Ljava/util/Calendar;->set(II)V

    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Ljava/util/Calendar;->set(II)V

    const/4 v4, 0x5

    invoke-virtual {v2, v4, v0}, Ljava/util/Calendar;->set(II)V

    sget-object v4, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " setting birth date : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v3, 0x1

    invoke-static {v1, v2, v0}, Lcom/sec/android/service/health/cp/common/HealthServiceUtil;->getyyyyMMddFromCalendar(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setBirthDate(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "secure_birth_year"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " SECURE_BIRTH_YEAR "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v0, "secure_birth_year"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " birth_year "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v0, "secure_birth_month"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " SECURE_BIRTH_MONTH "

    invoke-static {v0, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v0, "secure_birth_month"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " birth_month "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v0

    goto/16 :goto_1

    :cond_3
    const-string/jumbo v0, "secure_birth_day"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " SECURE_BIRTH_DAY "

    invoke-static {v0, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v0, "secure_birth_day"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sget-object v4, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " birth_day "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_4
    move v0, v2

    goto/16 :goto_2

    :cond_5
    move v3, v2

    goto/16 :goto_1

    :cond_6
    move v1, v2

    goto/16 :goto_0
.end method

.method private setDistanceUnit(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " setDistanceUnit"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "distance_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "distance_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " DistanceUnit "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, "mile"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x29813

    :goto_0
    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setDistanceUnit(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x29811

    goto :goto_0
.end method

.method private setGender(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const-string v0, "gender"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "gender"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x2e635

    :goto_0
    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setGender(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x2e636

    goto :goto_0
.end method

.method private setGlucoseUnit(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " set glucose unit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "mmol/L"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "mmol/L"

    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "glucose_unit"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    const-string/jumbo v0, "mg/dL"

    goto :goto_0
.end method

.method private setHeight(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v1, 0x43960000    # 300.0f

    const/high16 v2, 0x41a00000    # 20.0f

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " setHeight "

    invoke-static {v0, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "_inch"

    const v4, 0x249f1

    const-string v0, "height_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "height_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Height Unit  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "cm"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "height"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "height"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v3, v0, Ljava/lang/Integer;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->floatValue()F

    move-result v0

    :goto_0
    cmpl-float v3, v0, v1

    if-lez v3, :cond_5

    move v0, v1

    :cond_0
    :goto_1
    move v3, v0

    :cond_1
    :goto_2
    cmpl-float v0, v3, v5

    if-nez v0, :cond_c

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v7, " getting the inch height "

    invoke-static {v0, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "height"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "height"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    instance-of v6, v0, Ljava/lang/Integer;

    if-eqz v6, :cond_8

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->floatValue()F

    move-result v0

    :goto_3
    move v3, v0

    :cond_2
    :goto_4
    cmpl-float v0, v3, v5

    if-eqz v0, :cond_c

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " converting height to cm height in feet = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x249f2

    invoke-static {v3}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertInchToCm(F)F

    move-result v3

    cmpl-float v4, v3, v1

    if-lez v4, :cond_a

    :goto_5
    cmpl-float v2, v1, v5

    if-eqz v2, :cond_3

    sget-object v2, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Height "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setHeightUnit(I)V

    invoke-virtual {p2, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setHeight(F)V

    :cond_3
    return-void

    :cond_4
    if-eqz v0, :cond_10

    instance-of v3, v0, Ljava/lang/Float;

    if-eqz v3, :cond_10

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto/16 :goto_0

    :cond_5
    cmpg-float v3, v0, v2

    if-gez v3, :cond_0

    move v0, v2

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v0, "secure_height"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v0, "secure_height"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v7}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v0, v7}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    :goto_6
    cmpl-float v0, v3, v1

    if-lez v0, :cond_7

    move v3, v1

    goto/16 :goto_2

    :cond_7
    cmpg-float v0, v3, v2

    if-gez v0, :cond_1

    move v3, v2

    goto/16 :goto_2

    :cond_8
    if-eqz v0, :cond_d

    instance-of v6, v0, Ljava/lang/Float;

    if-eqz v6, :cond_d

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto/16 :goto_3

    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "secure_height"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v7, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "secure_height"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v6}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v0, v6}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    goto/16 :goto_4

    :cond_a
    cmpg-float v1, v3, v2

    if-gez v1, :cond_b

    move v1, v2

    goto/16 :goto_5

    :cond_b
    move v1, v3

    goto/16 :goto_5

    :cond_c
    move v0, v4

    move v1, v3

    goto/16 :goto_5

    :cond_d
    move v0, v3

    goto/16 :goto_3

    :cond_e
    move v3, v5

    goto :goto_6

    :cond_f
    move v3, v5

    goto/16 :goto_2

    :cond_10
    move v0, v5

    goto/16 :goto_0
.end method

.method private setName(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/16 v4, 0x32

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " setName "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_2

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name\'s length is greater than 50, Hence Truncating the Name "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setName(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_3

    if-nez v0, :cond_0

    :cond_3
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " Name field is empty. Putting Name as blank space. "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, " "

    goto :goto_0
.end method

.method private setNotification(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " setNotification "

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    const-string/jumbo v1, "notification_enabled"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "notification_enabled"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_0
    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " setNotification value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "notification_enabled"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private setPrivacy(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setPrivacy "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "use_pedometer_ranking"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTemperatureUnit(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, " setTemperatureUnit"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "temperature_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "temperature_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " TEMPERATURE_UNIT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "C"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x27101

    :goto_0
    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setTemperatureUnit(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x27102

    goto :goto_0
.end method

.method private setWeight(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const v7, 0x1fbd1

    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v1, 0x43fa0000    # 500.0f

    const/high16 v2, 0x40000000    # 2.0f

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " setWeight "

    invoke-static {v0, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "weight_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string/jumbo v0, "weight_unit"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Weight Unit  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "kg"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string/jumbo v0, "weight"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "weight"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v0, v3, v1

    if-lez v0, :cond_5

    move v3, v1

    :cond_0
    :goto_0
    cmpl-float v0, v3, v4

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Weight in KG"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2, v7}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setWeightUnit(I)V

    invoke-virtual {p2, v3}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setWeight(F)V

    :cond_1
    :goto_1
    cmpl-float v0, v3, v4

    if-nez v0, :cond_d

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v5, " Weight Unit is LB "

    invoke-static {v0, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "weight_lb"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "weight_lb"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    move v0, v3

    :goto_2
    cmpl-float v3, v0, v4

    if-eqz v3, :cond_3

    invoke-static {v0}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v0

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Weight in KG "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    cmpl-float v3, v0, v1

    if-lez v3, :cond_9

    move v0, v1

    :cond_2
    :goto_3
    const v3, 0x1fbd2

    invoke-virtual {p2, v3}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setWeightUnit(I)V

    invoke-virtual {p2, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setWeight(F)V

    :cond_3
    :goto_4
    cmpl-float v0, v0, v4

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " Weightunit is not present default is KG "

    invoke-static {v0, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "weight"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "weight"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v3, v0, v1

    if-lez v3, :cond_a

    :goto_5
    invoke-virtual {p2, v7}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setWeightUnit(I)V

    invoke-virtual {p2, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->setWeight(F)V

    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->getLatestWeightFromDB(Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V

    return-void

    :cond_5
    cmpg-float v0, v3, v2

    if-gez v0, :cond_0

    move v3, v2

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v0, "secure_weight"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v3, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v0, "secure_weight"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    cmpl-float v0, v3, v1

    if-lez v0, :cond_7

    move v3, v1

    goto/16 :goto_0

    :cond_7
    cmpg-float v0, v3, v2

    if-gez v0, :cond_0

    move v3, v2

    goto/16 :goto_0

    :cond_8
    const-string/jumbo v0, "secure_weight_lb"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v5, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v0, "secure_weight_lb"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v6}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    move v0, v3

    goto/16 :goto_2

    :cond_9
    cmpg-float v3, v0, v2

    if-gez v3, :cond_2

    move v0, v2

    goto/16 :goto_3

    :cond_a
    cmpg-float v1, v0, v2

    if-gez v1, :cond_b

    move v1, v2

    goto :goto_5

    :cond_b
    move v1, v0

    goto :goto_5

    :cond_c
    move v0, v3

    goto/16 :goto_2

    :cond_d
    move v0, v3

    goto/16 :goto_4

    :cond_e
    move v3, v4

    goto/16 :goto_0

    :cond_f
    move v3, v4

    goto/16 :goto_1
.end method

.method private upgradeFrom1to3(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 0

    return-void
.end method

.method private upgradeFrom3to4(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 0

    return-void
.end method

.method private upgradeFrom4to5(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 0

    return-void
.end method

.method private upgradeFrom5to6(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V
    .locals 6

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "upgradeFrom5to6"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, "getting shared pref from restore"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.shealth_preferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v1, "No shared pref to migrate @ com.sec.android.app.shealth_preferences"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " key "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " value "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v3, "privacy_public"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " updatePrivacy "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setPrivacy(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    const-string v3, "glucose_unit"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " update glucose_unit "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setGlucoseUnit(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setNotification(Ljava/util/Map;)V

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    if-nez v2, :cond_6

    new-instance v2, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v2}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    iput-object v2, p0, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    :cond_6
    :try_start_0
    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setBirthDate(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    :try_start_1
    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setName(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    :try_start_2
    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setGender(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_4
    :try_start_3
    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setHeight(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_5
    :try_start_4
    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setWeight(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_6
    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setTemperatureUnit(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V

    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setDistanceUnit(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V

    invoke-direct {p0, v1, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->setActivityType(Ljava/util/Map;Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;)V

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateShealthProfile;->save()V

    goto/16 :goto_0

    :catch_0
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " UnsupportedEncodingException while setting birth date"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " UnsupportedEncodingException while setting Name"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_3

    :catch_2
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " UnsupportedEncodingException while setting Gender"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_4

    :catch_3
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " UnsupportedEncodingException while setting Height"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_5

    :catch_4
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " UnsupportedEncodingException while setting Weight"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_6
.end method


# virtual methods
.method public upgrade(II)Z
    .locals 1

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->upgradeFrom1to3(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    :pswitch_2
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->upgradeFrom3to4(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    :pswitch_3
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->upgradeFrom4to5(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    :pswitch_4
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->upgradeFrom5to6(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
