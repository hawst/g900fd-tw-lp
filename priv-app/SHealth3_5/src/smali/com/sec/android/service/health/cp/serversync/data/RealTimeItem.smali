.class public Lcom/sec/android/service/health/cp/serversync/data/RealTimeItem;
.super Ljava/lang/Object;


# instance fields
.field public accessaryId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public cadenceRatePerMin:Ljava/lang/Double;

.field public calorieBurnPerMin:Ljava/lang/Double;

.field public dataTypeCode:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceLogPKId:Ljava/lang/Long;

.field public deviceRealTimeDataPKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public heartRateDataPerMin:Ljava/lang/Double;

.field public incline:Ljava/lang/Double;

.field public lastHeartBeatTime:Ljava/lang/String;

.field public metabolicEquivalentTask:Ljava/lang/Double;

.field public powerWatt:Ljava/lang/Double;

.field public resistance:Ljava/lang/Double;

.field public responseCode:Ljava/lang/String;

.field public sampleTime:Ljava/lang/String;

.field public speedPerHour:Ljava/lang/Double;

.field public speedUnit:Ljava/lang/String;

.field public stride:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
