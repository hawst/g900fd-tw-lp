.class public Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper$BLOOD_GLUCOSE_TYPE;,
        Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper$WEIGHT_TYPE;
    }
.end annotation


# static fields
.field public static final BLOOD_GLUCOSE_META_CONSTANT:I = 0x8

.field private static final CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F = 1.8f

.field private static final CELSIUS_TO_FAHRENHEIT_SUMMAND:I = 0x20

.field private static final CM_IN_FT:F = 30.48006f

.field private static final CM_IN_INCH:F = 2.54f

.field public static final DISTANCE_META_CONSTANT:I = 0x2

.field private static final FAHRENHEIT_TO_CELSIUS_MULTIPLIER:F = 0.5555556f

.field private static final FAHRENHEIT_TO_CELSIUS_SUMMAND:I = -0x20

.field private static final FT_IN_CM:F = 0.032808334f

.field private static final GLUCOSE_CONVERT_VALUE:F = 18.0182f

.field private static final INCHES_IN_CM:F = 0.39370078f

.field private static final KG_IN_LB:F = 0.45359236f

.field private static final LB_IN_KG:F = 2.2046227f

.field private static final METERS_IN_YARD:F = 0.9144f

.field public static final TEMPERATURE_META_CONSTANT:I = 0x4

.field public static final WEIGHT_META_CONSTANT:I = 0x1

.field private static final YARDS_IN_METER:F = 1.09361f


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertCmToFeet(F)F
    .locals 1

    const v0, 0x3d066208

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertCmToInch(F)F
    .locals 1

    const v0, 0x3ec99326

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertFeetToCm(F)F
    .locals 1

    const v0, 0x41f3d72a

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertInchToCm(F)F
    .locals 1

    const v0, 0x40228f5c    # 2.54f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertKgToLb(F)F
    .locals 1

    const v0, 0x400d188a

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertLbToKg(F)F
    .locals 1

    const v0, 0x3ee83d42

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertMgIntoMmol(F)F
    .locals 1

    const v0, 0x41902546    # 18.0182f

    div-float v0, p0, v0

    return v0
.end method

.method public static convertMilesToKiloMeters(F)F
    .locals 1

    const v0, 0x3f1f121b

    div-float v0, p0, v0

    return v0
.end method

.method public static convertYardsToMeters(F)F
    .locals 1

    const v0, 0x3f8bfb6a

    div-float v0, p0, v0

    return v0
.end method

.method public static fahrenheitToCelsius(F)F
    .locals 2

    const/high16 v0, -0x3e000000    # -32.0f

    add-float/2addr v0, p0

    const v1, 0x3f0e38e4

    mul-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getBloodGlucoseUnit(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "mg/dL"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "mmol/L"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x222e1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getBloodGlucoseUnit(ILandroid/content/Context;)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/service/health/R$string;->bloodglucose_unit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "mmol/L"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x222e1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getWeightUnit(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "kg"

    goto :goto_0

    :pswitch_1
    const-string v0, "lb"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1fbd1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getWeightUnit(ILandroid/content/Context;)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/service/health/R$string;->weightmachine_unit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/service/health/R$string;->weightmachine_unit_lb:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1fbd1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
