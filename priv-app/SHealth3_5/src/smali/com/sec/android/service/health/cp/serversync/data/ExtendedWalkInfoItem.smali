.class public Lcom/sec/android/service/health/cp/serversync/data/ExtendedWalkInfoItem;
.super Ljava/lang/Object;


# instance fields
.field public accessaryId:Ljava/lang/String;

.field public activeTime:Ljava/lang/Long;

.field public appId:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceCreateTimeOfLog:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyTime:Ljava/lang/String;

.field public devicePKId:J

.field public deviceTimeZone:Ljava/lang/String;

.field public endTime:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public powerStep:Ljava/lang/Integer;

.field public startTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
