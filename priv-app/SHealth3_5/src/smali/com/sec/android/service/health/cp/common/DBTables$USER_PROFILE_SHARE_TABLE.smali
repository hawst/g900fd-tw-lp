.class public Lcom/sec/android/service/health/cp/common/DBTables$USER_PROFILE_SHARE_TABLE;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "USER_PROFILE_SHARE_TABLE"
.end annotation


# static fields
.field public static final KEY_ALARM_CYCLE:Ljava/lang/String; = "alarm_cycle"

.field public static final KEY_ALARM_TIME:Ljava/lang/String; = "alarm_time"

.field public static final KEY_BODY_ID:Ljava/lang/String; = "body_id"

.field public static final KEY_FAVORITE:Ljava/lang/String; = "favorite"

.field public static final KEY_IMAGE:Ljava/lang/String; = "image"

.field public static final KEY_NAME:Ljava/lang/String; = "name"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final TABLE_NAME:Ljava/lang/String; = "user_profile_share"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
