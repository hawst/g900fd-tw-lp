.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# instance fields
.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private isCommandExecuted([B)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    new-array v1, v1, [B

    array-length v2, v1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_DELETE_ALL_RECORDS:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public doAction()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->DELETE_ALL_RECORDS:[B

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    return-void
.end method

.method public responseReceived([B)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;->isCommandExecuted([B)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    const-string v1, "RES_CLEAR_DATA"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v1, "device data reset done"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    return-void
.end method
