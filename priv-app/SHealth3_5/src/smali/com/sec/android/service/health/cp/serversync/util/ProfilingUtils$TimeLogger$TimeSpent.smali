.class public Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeSpent"
.end annotation


# instance fields
.field private mEndTime:J

.field private mKey:Ljava/lang/String;

.field private mStartTime:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->mKey:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->setStartTime()V

    return-void
.end method


# virtual methods
.method public dump()V
    .locals 4

    # getter for: Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dump"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->mKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] takes "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->getDuration()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seconds"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getDuration()F
    .locals 7

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->mEndTime:J

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->mStartTime:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    const-string v2, "%.02f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public setEndTime()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->mEndTime:J

    return-void
.end method

.method public setStartTime()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->mStartTime:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TimeSpent [mKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
