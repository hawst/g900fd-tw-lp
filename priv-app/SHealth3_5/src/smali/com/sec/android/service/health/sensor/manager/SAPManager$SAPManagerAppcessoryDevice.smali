.class public Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;
.super Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/SAPManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SAPManagerAppcessoryDevice"
.end annotation


# instance fields
.field public rejoin:Z

.field public sessionId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->rejoin:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->sessionId:J

    return-void
.end method

.method public constructor <init>(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)V
    .locals 2

    invoke-direct {p0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->rejoin:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->sessionId:J

    iget-object v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->address:Ljava/lang/String;

    iget-object v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->name:Ljava/lang/String;

    iget-wide v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->appcessoryId:J

    iget v0, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->transportId:I

    iput v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->transportId:I

    return-void
.end method


# virtual methods
.method public getRejoin()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->rejoin:Z

    return v0
.end method

.method public getSessionId()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->sessionId:J

    return-wide v0
.end method

.method public setRejoin(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->rejoin:Z

    return-void
.end method

.method public setSessionId(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/manager/SAPManager$SAPManagerAppcessoryDevice;->sessionId:J

    return-void
.end method
