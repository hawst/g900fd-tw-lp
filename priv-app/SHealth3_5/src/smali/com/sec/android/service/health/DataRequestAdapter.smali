.class public Lcom/sec/android/service/health/DataRequestAdapter;
.super Ljava/lang/Object;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "Health.DataRequestAdapter"

.field private static final TYPE_NAME_TO_DATABASE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static final sObserverList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/DataRequestAdapter;->sObserverList:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "com.samsung.health.electrocardiography"

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiography;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.samsung.health.step_count"

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/DataRequestAdapter;->TYPE_NAME_TO_DATABASE:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static processReadRequest(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/internal/ReadRequestImpl;)Lcom/samsung/android/health/HealthDataResolver$ReadResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v4, 0x0

    const-string v0, "Health.DataRequestAdapter"

    const-string v1, "Handling read request from client..."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/samsung/android/internal/ReadRequestImpl;->getDataType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/DataRequestAdapter;->TYPE_NAME_TO_DATABASE:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {p2}, Lcom/samsung/android/internal/ReadRequestImpl;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/internal/ReadRequestImpl;->getSortOrder()Ljava/lang/String;

    move-result-object v5

    const/16 v0, 0xf

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "create_time"

    aput-object v0, v2, v7

    const/4 v0, 0x1

    const-string/jumbo v6, "update_time"

    aput-object v6, v2, v0

    const/4 v0, 0x2

    const-string v6, "hdid AS datauuid"

    aput-object v6, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v6, "start_time"

    aput-object v6, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v6, "time_zone AS time_offset"

    aput-object v6, v2, v0

    const/4 v0, 0x5

    const-string v6, "end_time"

    aput-object v6, v2, v0

    const/4 v0, 0x6

    const-string/jumbo v6, "total_step"

    aput-object v6, v2, v0

    const/4 v0, 0x7

    const-string v6, "distance"

    aput-object v6, v2, v0

    const/16 v0, 0x8

    const-string/jumbo v6, "speed"

    aput-object v6, v2, v0

    const/16 v0, 0x9

    const-string/jumbo v6, "sample_position"

    aput-object v6, v2, v0

    const/16 v0, 0xa

    const-string v6, "calorie"

    aput-object v6, v2, v0

    const/16 v0, 0xb

    const-string/jumbo v6, "run_step"

    aput-object v6, v2, v0

    const/16 v0, 0xc

    const-string/jumbo v6, "walk_step"

    aput-object v6, v2, v0

    const/16 v0, 0xd

    const-string/jumbo v6, "up_step"

    aput-object v6, v2, v0

    const/16 v0, 0xe

    const-string v6, "down_step"

    aput-object v6, v2, v0

    const-string/jumbo v0, "platform.db"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$ReadResult;

    invoke-direct {v0, v4}, Lcom/samsung/android/health/HealthDataResolver$ReadResult;-><init>(Lcom/samsung/android/internal/HealthDataCursor;)V

    move-object v4, v0

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "Health.DataRequestAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieved "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " read item(s)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/database/CursorWindow;

    const-string v2, "HealthCursorWindow"

    invoke-direct {v1, v2}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v2, v0}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v2, v7, v1}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    new-instance v3, Lcom/samsung/android/internal/HealthDataCursor;

    invoke-direct {v3, v1}, Lcom/samsung/android/internal/HealthDataCursor;-><init>(Landroid/database/CursorWindow;)V

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/samsung/android/internal/HealthDataCursor;->setColumnNames([Ljava/lang/String;)V

    new-instance v4, Lcom/samsung/android/health/HealthDataResolver$ReadResult;

    invoke-direct {v4, v3}, Lcom/samsung/android/health/HealthDataResolver$ReadResult;-><init>(Lcom/samsung/android/internal/HealthDataCursor;)V

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/database/CrossProcessCursorWrapper;->close()V

    goto :goto_0
.end method

.method public static registerDataObserver(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/health/IHealthDataObserver;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    const-string v0, "Health.DataRequestAdapter"

    const-string v1, "Handling observe request from client..."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/DataRequestAdapter;->TYPE_NAME_TO_DATABASE:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    new-instance v1, Lcom/sec/android/service/health/DataRequestAdapter$1;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p3, p2}, Lcom/sec/android/service/health/DataRequestAdapter$1;-><init>(Landroid/os/Handler;Lcom/samsung/android/health/IHealthDataObserver;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/service/health/DataRequestAdapter;->sObserverList:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lcom/sec/android/service/health/DataRequestAdapter;->sObserverList:Ljava/util/Map;

    invoke-interface {p3}, Lcom/samsung/android/health/IHealthDataObserver;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string v0, "Health.DataRequestAdapter"

    const-string v1, "Client\'s observer is registered successfully"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static unregisterDataObserver(Landroid/content/Context;Lcom/samsung/android/health/IHealthDataObserver;)V
    .locals 5

    invoke-interface {p1}, Lcom/samsung/android/health/IHealthDataObserver;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/DataRequestAdapter;->sObserverList:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/sec/android/service/health/DataRequestAdapter;->sObserverList:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/DataRequestAdapter;->sObserverList:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    return-void

    :cond_0
    const-string v0, "Health.DataRequestAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Observer callback not found : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
