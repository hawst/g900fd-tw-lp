.class public final Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# instance fields
.field cmdArray:[B

.field dataArray:[B

.field private mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

.field time:J


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->dataArray:[B

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    const/4 v0, 0x7

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->cmdArray:[B

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    return-void
.end method

.method private updateDataArray()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/16 v5, 0x10

    const/4 v4, 0x0

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->dataArray:[B

    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->dataArray:[B

    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-byte v1, v1

    aput-byte v1, v0, v7

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->dataArray:[B

    iget-wide v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-byte v1, v1

    aput-byte v1, v0, v6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->dataArray:[B

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->WRITE_RTC:[B

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->cmdArray:[B

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->WRITE_RTC:[B

    array-length v2, v2

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->dataArray:[B

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->cmdArray:[B

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->WRITE_RTC:[B

    array-length v2, v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->dataArray:[B

    array-length v3, v3

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->cmdArray:[B

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendData([BBLcom/sec/android/service/health/sensor/handler/usb/jnj/IMeterResponseListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getResposeHandler()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v7, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method


# virtual methods
.method public doAction()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->updateDataArray()V

    return-void
.end method

.method public responseReceived([B)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [B

    array-length v1, v0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_WRITE_RTC:[B

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mCommManager:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->sendAck()V

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    const-string v1, "RES_SET_TIME"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v1, "device time set"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->mJnjProtocol:Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]JNJ"

    const-string v1, "Protocol listener is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->time:J

    return-void
.end method
