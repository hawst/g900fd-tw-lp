.class public Lcom/sec/android/service/health/sensor/manager/BTManager;
.super Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;,
        Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;,
        Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;,
        Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "[HealthSensor]BTManager"

.field private static mBtManager:Lcom/sec/android/service/health/sensor/manager/BTManager;


# instance fields
.field private final CMD_UNPAIR_DEVICE:Ljava/lang/String;

.field private mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothBroadcastReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;

.field private mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

.field private mBluetoothStateListener:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

.field private mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBtManager:Lcom/sec/android/service/health/sensor/manager/BTManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothStateListener:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    const-string v0, "CMD_UNPAIR_DEVICE"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->CMD_UNPAIR_DEVICE:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/BTManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/manager/BTManager$1;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-void
.end method

.method static synthetic access$302(Lcom/sec/android/service/health/sensor/manager/BTManager;Landroid/bluetooth/BluetoothHealth;)Landroid/bluetooth/BluetoothHealth;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    return-object p1
.end method

.method private static getDataTypeByBtDevice(Landroid/bluetooth/BluetoothDevice;)I
    .locals 4

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v1

    const/16 v2, 0x900

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :sswitch_0
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    const/4 v0, 0x3

    goto :goto_1

    :sswitch_2
    const/4 v0, 0x7

    goto :goto_1

    :sswitch_3
    const/4 v0, 0x1

    goto :goto_1

    :sswitch_4
    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTManager getDeviceTypeByClass no device type("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") nothing registered."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x900 -> :sswitch_5
        0x904 -> :sswitch_0
        0x908 -> :sswitch_2
        0x90c -> :sswitch_3
        0x910 -> :sswitch_1
        0x918 -> :sswitch_4
        0x91c -> :sswitch_5
    .end sparse-switch
.end method

.method private static getDeviceTypeByBtDevice(Landroid/bluetooth/BluetoothDevice;)I
    .locals 4

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v1

    const/16 v2, 0x900

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    return v0

    :sswitch_0
    const/16 v0, 0x2713

    goto :goto_1

    :sswitch_1
    const/16 v0, 0x2714

    goto :goto_1

    :sswitch_2
    const/16 v0, 0x2716

    goto :goto_1

    :sswitch_3
    const/16 v0, 0x2712

    goto :goto_1

    :sswitch_4
    const/16 v0, 0x2718

    goto :goto_1

    :sswitch_5
    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTManager getDeviceTypeByClass no device type("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") nothing registered."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x900 -> :sswitch_5
        0x904 -> :sswitch_0
        0x908 -> :sswitch_2
        0x90c -> :sswitch_3
        0x910 -> :sswitch_1
        0x918 -> :sswitch_4
        0x91c -> :sswitch_5
    .end sparse-switch
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/manager/BTManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBtManager:Lcom/sec/android/service/health/sensor/manager/BTManager;

    return-object v0
.end method


# virtual methods
.method public bondStateChangedEvent(Landroid/content/Intent;)V
    .locals 8

    const/16 v7, 0xc

    const/4 v5, -0x1

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager bondStateChangedEvent()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_2

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager bondStateChangedEvent() btDevice is null, return"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager bondStateChangedEvent() btClass is null, return"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v2, "[HealthSensor]BTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BTManager bondStateChangedEvent() getMajorDeviceClass:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "[HealthSensor]BTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BTManager bondStateChangedEvent() getDeviceClass :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "android.bluetooth.device.extra.BOND_STATE"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "[HealthSensor]BTManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BTManager bondStateChangedEvent:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "->"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v2, v7, :cond_4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v1

    const/16 v4, 0x900

    if-ne v1, v4, :cond_4

    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager bondStateChangedEvent() health profile, Device Bonded "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTManager initialize() pController not there for the BTAddress "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", so Creating it"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->initProfileController(Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    :cond_4
    if-ne v3, v7, :cond_0

    const/16 v1, 0xa

    if-ne v2, v1, :cond_0

    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager bondStateChangedEvent() Device not Bonded"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/manager/BTManager;->dispatchLeave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V

    :cond_5
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    :cond_6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->removeProfileHandlerController(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->removeSensorDevice(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 10

    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    move-object v8, p1

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getDeviceTypeByBtDevice(Landroid/bluetooth/BluetoothDevice;)I

    move-result v5

    invoke-static {v8}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getDataTypeByBtDevice(Landroid/bluetooth/BluetoothDevice;)I

    move-result v6

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->putConnectedDevice(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerFactory;->getProfileHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    move-result-object v2

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->isProtocolSupported()Z

    move-result v3

    if-ne v3, v4, :cond_2

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;->getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;->getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setProtocol(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v1

    const/16 v3, 0x900

    if-eq v1, v3, :cond_1

    :cond_0
    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDataTypeList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setDataType(Ljava/util/List;)V

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceTypeList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setType(I)V

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->putConnectedDevice(Landroid/os/Parcelable;)V

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceManufacturers()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getDeviceManufacturers()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    invoke-interface {v2}, Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setProtocol(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, v7

    goto :goto_1
.end method

.method public deinitialize()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothStateListener:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothStateListener:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothStateListener:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothBroadcastReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothBroadcastReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothBroadcastReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;

    :cond_2
    invoke-super {p0}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->deinitialize()V

    return-void
.end method

.method protected deviceAppDied(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)V
    .locals 2

    if-eqz p1, :cond_2

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/BTManager;->killContentUiActivity(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)V

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "called deviceAppDied of BTManager !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_3

    :cond_0
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "called deviceAppDied of BTManager !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "called deviceAppDied of BTManager but device is null !!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->isContollingDevice(Ljava/lang/Integer;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData(Ljava/lang/Integer;)I

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopListeningData(Ljava/lang/Integer;)I

    goto :goto_0
.end method

.method public getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "getConnectedDevices()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method initHealthHandlers()V
    .locals 4

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager  initHealthHandlers() START"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "[HealthSensor]BTManager"

    const-string v3, "BTManager initialize() Other class is bonded"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->initProfileController(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    :cond_1
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager  initHealthHandlers() END"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method initProfileController(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->addSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "[HealthSensor]BTManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BTManager initProfileController() handler Not There for the BTAddress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", so creating it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->createDeviceHandler(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 4

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager  initialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/BTManager$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothStateListener:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/BTManager$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/BTManager$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothBroadcastReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothStateListener:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothStateListener;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mAConnectionListener:Lcom/sec/android/service/health/sensor/manager/BTManager$ACLConnectionListener;

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothBroadcastReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager  initialize() mBluetoothAdapter is NULL, So return"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager initialize() end"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isDataTypeSupported(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager join BlueTooth is Not Enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getSensorDevice(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->create_ShealthSensorDevice(Landroid/os/Parcelable;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->addSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_4
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    const/16 v2, 0xc

    if-eq v1, v2, :cond_5

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v1

    const-string v2, "[HealthSensor]BTManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "join() device already bonded, controller = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v1, :cond_6

    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "join() controller not there for the BTAddress "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", so creating it"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->initProfileController(Landroid/bluetooth/BluetoothDevice;)V

    :cond_6
    invoke-virtual {p0, p1, v5, v5}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceJoined(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;IZ)V

    goto/16 :goto_0
.end method

.method public leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    return-void
.end method

.method protected remoteCallBackDied(Ljava/lang/Integer;)V
    .locals 0

    return-void
.end method

.method public renameDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager renameDevice BlueTooth is Not Enabled"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual {v1, p3}, Landroid/bluetooth/BluetoothDevice;->setAlias(Ljava/lang/String;)Z

    move-result v0

    :cond_2
    const-string v1, "[HealthSensor]BTManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTManager renameDevice with ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CMD_UNPAIR_DEVICE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/android/service/health/sensor/manager/BTManager;->unpair(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p2, p3, p4}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->request(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    goto :goto_0
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager startReceivingData BlueTooth is Not Enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v4, p5

    move-wide v5, p6

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->startReceivingData(Ljava/lang/Integer;ILcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startScanning(Lcom/sec/android/service/health/sensor/manager/util/Filter;)I
    .locals 3

    const/4 v0, 0x2

    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager startScanning"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager startScanning mContext is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager startScanning BlueTooth is Not Enabled"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/util/Filter;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->setRegistered(Z)V

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager startScanning startDiscovery() "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;)I
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const-string v1, "[HealthSensor]BTManager"

    const-string v2, "BTManager stopReceivingData BlueTooth is Not Enabled"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData(Ljava/lang/Integer;)I

    move-result v0

    goto :goto_0
.end method

.method public stopScanning()Z
    .locals 2

    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager stopScanning "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->isRegistered()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mScanReceiver:Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;->setRegistered(Z)V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public unpair(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v0, "[HealthSensor]BTManager"

    const-string v1, "BTManager join BlueTooth is Not Enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onDeviceLeft(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;I)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v0, p0

    move-object v1, p3

    move-object v3, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V

    goto :goto_0

    :cond_2
    new-instance v6, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;

    invoke-direct {v6, p0, p1}, Lcom/sec/android/service/health/sensor/manager/BTManager$BondingReceiver;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->removeBond()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v0, p0

    move-object v1, p3

    move-object v3, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    const/4 v0, 0x0

    invoke-direct {v2, v0, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v0, p0

    move-object v1, p3

    move-object v3, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/manager/BTManager;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Integer;I)V

    goto :goto_0
.end method
