.class Lcom/sec/android/service/health/sensor/manager/BTManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/BTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3

    const-string v0, "[HealthSensor]BTManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BTManager onServiceConnected profile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    check-cast p2, Landroid/bluetooth/BluetoothHealth;

    # setter for: Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;
    invoke-static {v0, p2}, Lcom/sec/android/service/health/sensor/manager/BTManager;->access$302(Lcom/sec/android/service/health/sensor/manager/BTManager;Landroid/bluetooth/BluetoothHealth;)Landroid/bluetooth/BluetoothHealth;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->initHealthHandlers()V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3

    const-string v0, "[HealthSensor]BTManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BTManager onServiceDisconnected profile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/BTManager;->cleanDeviceHandler()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$1;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/service/health/sensor/manager/BTManager;->mBluetoothHealth:Landroid/bluetooth/BluetoothHealth;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/manager/BTManager;->access$302(Lcom/sec/android/service/health/sensor/manager/BTManager;Landroid/bluetooth/BluetoothHealth;)Landroid/bluetooth/BluetoothHealth;

    :cond_0
    return-void
.end method
