.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;

.field private static sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->instance:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->instance:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;

    return-object v0
.end method


# virtual methods
.method public getDataTranslatorFromProtocolVersion(DZ)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;
    .locals 3

    const-wide/16 v1, 0x0

    cmpl-double v0, p1, v1

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "new SWearableDataTranslatorV0()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV0;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    return-object v0

    :cond_0
    cmpl-double v0, p1, v1

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " new SWearableDataTranslatorV3()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    goto :goto_0

    :cond_1
    const-wide v0, 0x4008147ae147ae14L    # 3.01

    cmpl-double v0, p1, v0

    if-nez v0, :cond_2

    if-eqz p3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " new SWearableDataTranslatorV3()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorV3;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This protocolVersion is not supported : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    goto/16 :goto_0
.end method
