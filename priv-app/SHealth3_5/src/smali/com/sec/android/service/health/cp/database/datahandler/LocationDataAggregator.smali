.class public Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field private static final MILLISEC_IN_ONE_MIN:I = 0xea60

.field private static final MPS_TO_MPH:I = 0xe10

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCurrentTenSec:J

.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

.field private mExerciseId:J

.field private mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

.field private mStartTime:J

.field private mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    return-void
.end method

.method private processBundleData()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Location Data is comming : timeStamp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Location extra value is comming"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private updateExerciseActivity()V
    .locals 6

    const/high16 v4, 0x45610000    # 3600.0f

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v1, :cond_0

    const-string v1, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mExerciseId:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "calorie"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;D)V

    const-string v1, "cadence"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->stepCount:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v1, "cadence_type"

    const/16 v2, 0x7532

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "mean_speed_per_hour"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    mul-float/2addr v2, v4

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_speed_per_hour"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    mul-float/2addr v2, v4

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "incline_distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "decline_distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "distance"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "max_altitude"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "min_altitude"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string/jumbo v1, "start_time"

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mStartTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "end_time"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;J)V

    const-string v1, "duration_min"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mStartTime:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    const v3, 0x476a6000    # 60000.0f

    div-float/2addr v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;F)V

    const-string v1, "duration_millisecond"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    iget-wide v4, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mStartTime:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKeyAllowZero(Landroid/content/ContentValues;Ljava/lang/String;F)V

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/database/datahandler/ActivitySessionManager;->getCurrentActivityUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Insert Data at exercise_activity Contraint Failed: value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Insert Data at exercise_activity Unknown Exception log : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 5

    const/4 v4, 0x1

    monitor-enter p0

    :try_start_0
    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->checkMandatoryFields()Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return v4

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mCurrentTenSec:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mStartTime:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mCurrentTenSec:J

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->processBundleData()V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "addData[] not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method protected checkMandatoryFields()Z
    .locals 5

    const/4 v0, 0x0

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->latitude:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->longitude:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data will be skipped. mandatory fields empty"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->isInvalid(J)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->latitude:D

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    iget-wide v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->longitude:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_2

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sensor issue - not all mandatory fields are assigned"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalizeAggregation()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "finalizeAggregation insert exerciseActivity for final data."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mPrevExtra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "finalizeAggregation Invalid last data."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->updateExerciseActivity()V
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Data is out of range"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mUserDeviceId:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mContext:Landroid/content/Context;

    iput-wide p3, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mExerciseId:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mCurrentTenSec:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mStartTime:J

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mAppId:Ljava/lang/String;

    return-void
.end method

.method public isRunning(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mExerciseId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mExerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/LocationDataAggregator;->mExerciseId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "matched"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
