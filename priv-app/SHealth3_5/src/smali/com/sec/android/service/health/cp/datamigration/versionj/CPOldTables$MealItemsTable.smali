.class public Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$MealItemsTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MealItemsTable"
.end annotation


# static fields
.field public static final CONTENT_ID_URI_BASE:Landroid/net/Uri;

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.samsung.cursor.item/health.meal_items"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.samsung.cursor.dir/health.meal_items"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_ID_FOOD_INFO:Ljava/lang/String; = "id_food_info"

.field public static final KEY_ID_MEAL_TABLE:Ljava/lang/String; = "id_meal_table"

.field public static final KEY_MARK_NUMBER:Ljava/lang/String; = "mark_number"

.field public static final KEY_MARK_POSX:Ljava/lang/String; = "mark_posx"

.field public static final KEY_MARK_POSY:Ljava/lang/String; = "mark_posy"

.field public static final KEY_PERCENT:Ljava/lang/String; = "percent"

.field public static final TABLE_NAME:Ljava/lang/String; = "meal_items"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://SecHealth/meal_items"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$MealItemsTable;->CONTENT_URI:Landroid/net/Uri;

    const-string v0, "content://SecHealth/meal_items/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/datamigration/versionj/CPOldTables$MealItemsTable;->CONTENT_ID_URI_BASE:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
