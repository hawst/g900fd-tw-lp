.class Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->requestAccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private subscribeToEvents()V
    .locals 2

    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    const-string/jumbo v1, "subscribeToEvents()"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeHeartRateDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$2;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribePage4AddtDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$3;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$3;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeCumulativeOperatingTimeEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$ICumulativeOperatingTimeReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$4;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$4;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeManufacturerAndSerialEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;)V

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1$5;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeVersionAndModelEvent(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IVersionAndModelReceiver;)V

    return-void
.end method


# virtual methods
.method public onResultReceived(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$3;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$defines$RequestAccessResult:[I

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error. Unrecognized result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyOnJoinedError(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-static {v0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$002(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SUCCESS status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->getIntValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->setConnectState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyChannelStateChanged()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->state:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onResultReceived goto disconnect "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mAntDeviceID:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    const/4 v1, 0x3

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->state:I
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->access$502(Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;I)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->subscribeToEvents()V

    goto :goto_0

    :pswitch_1
    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    const-string v1, "Error. Channel Not Available"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    const-string v1, "Error. RequestAccess failed. See logcat for details."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    const-string v1, "Error. RequestAccess failed. See logcat for details."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    const-string v1, "User Cancelled."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogI(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    const-string v1, "Error. Upgrade Required?"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "[HealthSensor]HRMAntProfileHandler"

    const-string v1, "Error. Search timeout."

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogW(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler;->notifyOnJoinedError(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 0

    check-cast p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;

    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/ant/HRMAntProfileHandler$1;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    return-void
.end method
