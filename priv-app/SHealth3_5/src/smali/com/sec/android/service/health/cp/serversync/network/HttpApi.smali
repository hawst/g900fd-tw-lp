.class public Lcom/sec/android/service/health/cp/serversync/network/HttpApi;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;
    }
.end annotation


# static fields
.field public static DELETE_BIO_INFO:Ljava/lang/String;

.field public static DELETE_EXERCISE_IMAGE_INFO:Ljava/lang/String;

.field public static DELETE_EXTENDED_WALKINFO:Ljava/lang/String;

.field public static DELETE_FIRSTBEAT_RESULT:Ljava/lang/String;

.field public static DELETE_FOOD_IMAGE_INFO:Ljava/lang/String;

.field public static DELETE_FOOD_INFO:Ljava/lang/String;

.field public static DELETE_FOOD_NUTRIENT_HISTORY:Ljava/lang/String;

.field public static DELETE_SLEEP_INFO:Ljava/lang/String;

.field public static DELETE_STRESS_INFO:Ljava/lang/String;

.field public static DELETE_TAG_INDEX:Ljava/lang/String;

.field public static DELETE_TEMPERATURE_HUMIDITY_HISTORY:Ljava/lang/String;

.field public static DELETE_USER_DATA_IN_SERVER:Ljava/lang/String;

.field public static DELETE_USER_GOAL:Ljava/lang/String;

.field public static DELETE_USER_PROFILE_IMAGE:Ljava/lang/String;

.field public static DELETE_UV_HISTORY:Ljava/lang/String;

.field public static DELETE_UV_PROTECTION_HISTORY:Ljava/lang/String;

.field public static DELETE_WATER_INTAKE_INFO:Ljava/lang/String;

.field public static DELETE_WELSTORY_NUTRIENT_HISTORY:Ljava/lang/String;

.field public static DELETE_WORKOUT_INFO:Ljava/lang/String;

.field public static final DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;",
            ">;"
        }
    .end annotation
.end field

.field public static GET_APP_PERMISSION:Ljava/lang/String;

.field public static GET_BIO_BLOODSUGAR:Ljava/lang/String;

.field public static GET_BIO_ECG:Ljava/lang/String;

.field public static GET_BIO_HEARTRATE:Ljava/lang/String;

.field public static GET_BIO_HISTORY:Ljava/lang/String;

.field public static GET_BIO_SKIN:Ljava/lang/String;

.field public static GET_CUSTOM_ITEM:Ljava/lang/String;

.field public static GET_EXERCISE_HISTORY:Ljava/lang/String;

.field public static GET_EXTENDED_WALKINFO:Ljava/lang/String;

.field public static GET_FIRSTBEAT_RESULT:Ljava/lang/String;

.field public static GET_FIRSTBEAT_VARIABLE:Ljava/lang/String;

.field public static GET_FOOD_INFO:Ljava/lang/String;

.field public static GET_FOOD_INFO_HISTORY:Ljava/lang/String;

.field public static GET_LOCATION_DATA:Ljava/lang/String;

.field public static GET_MAINTENANCE_NOTICE:Ljava/lang/String;

.field public static GET_PHYSIOLOGY_HISTORY:Ljava/lang/String;

.field public static GET_REALTIME_DATA:Ljava/lang/String;

.field public static GET_SYNC_HISTORY:Ljava/lang/String;

.field public static GET_TAG_INDEX:Ljava/lang/String;

.field public static GET_TEMPERATURE_HUMIDITY_HISTORY:Ljava/lang/String;

.field public static GET_USERDEVICE_INFO:Ljava/lang/String;

.field public static GET_USER_GOAL:Ljava/lang/String;

.field public static GET_USER_PROFILE:Ljava/lang/String;

.field public static GET_UV_HISTORY:Ljava/lang/String;

.field public static GET_UV_PROTECTION_HISTORY:Ljava/lang/String;

.field public static GET_WELSTORY_FOOD_INFO:Ljava/lang/String;

.field public static GET_WORKOUT_INFO:Ljava/lang/String;

.field public static SET_ACTIVITY_INFO:Ljava/lang/String;

.field public static SET_BIO_BLOODPRESSURE:Ljava/lang/String;

.field public static SET_BIO_BLOODSUGAR:Ljava/lang/String;

.field public static SET_BIO_ECG:Ljava/lang/String;

.field public static SET_BIO_HEARTRATE:Ljava/lang/String;

.field public static SET_BIO_INFO:Ljava/lang/String;

.field public static SET_BIO_PULSEOXIMETER:Ljava/lang/String;

.field public static SET_BIO_SKIN:Ljava/lang/String;

.field public static SET_BIO_TEMPERATURE:Ljava/lang/String;

.field public static SET_BIO_WEIGHT:Ljava/lang/String;

.field public static SET_CUSTOM_ITEM:Ljava/lang/String;

.field public static SET_EXERCISE_ACTIVITY:Ljava/lang/String;

.field public static SET_EXERCISE_IMAGE:Ljava/lang/String;

.field public static SET_EXERCISE_IMAGE_INFO:Ljava/lang/String;

.field public static SET_EXERCISE_MANUAL_DATA:Ljava/lang/String;

.field public static SET_EXTENDED_WALKINFO:Ljava/lang/String;

.field public static SET_FIRSTBEAT_RESULT:Ljava/lang/String;

.field public static SET_FIRSTBEAT_VARIABLE:Ljava/lang/String;

.field public static SET_FITNESS_INFO:Ljava/lang/String;

.field public static SET_FOOD_IMAGE:Ljava/lang/String;

.field public static SET_FOOD_IMAGE_INFO:Ljava/lang/String;

.field public static SET_FOOD_INFO:Ljava/lang/String;

.field public static SET_INGESTED_FOOD:Ljava/lang/String;

.field public static SET_LOCATION_DATA:Ljava/lang/String;

.field public static SET_REALTIME_DATA:Ljava/lang/String;

.field public static SET_SLEEP_INFO:Ljava/lang/String;

.field public static SET_STRESS_INFO:Ljava/lang/String;

.field public static SET_TAG_INDEX:Ljava/lang/String;

.field public static SET_TEMPERATURE_HUMIDITY:Ljava/lang/String;

.field public static SET_USERDEVICE_INFO:Ljava/lang/String;

.field public static SET_USER_GOAL:Ljava/lang/String;

.field public static SET_USER_IMAGE:Ljava/lang/String;

.field public static SET_USER_IMAGE_INFO:Ljava/lang/String;

.field public static SET_USER_PROFILE:Ljava/lang/String;

.field public static SET_USER_PROFILE_IMAGE:Ljava/lang/String;

.field public static SET_UV:Ljava/lang/String;

.field public static SET_UV_PROTECTION:Ljava/lang/String;

.field public static SET_WALKING_INFO:Ljava/lang/String;

.field public static SET_WATER_INTAKE_INFO:Ljava/lang/String;

.field public static SET_WELSTORY_FOOD_INFO:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string/jumbo v0, "platform/sync/getSyncHistory"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_SYNC_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/getAppPermission"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_APP_PERMISSION:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/getServerOperability"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_MAINTENANCE_NOTICE:Ljava/lang/String;

    const-string/jumbo v0, "platform/profile/getUserProfile"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USER_PROFILE:Ljava/lang/String;

    const-string/jumbo v0, "platform/profile/backupUserProfile"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_PROFILE:Ljava/lang/String;

    const-string/jumbo v0, "platform/profile/backupProfileImage"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_PROFILE_IMAGE:Ljava/lang/String;

    const-string/jumbo v0, "platform/profile/deleteProfileImage"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_USER_PROFILE_IMAGE:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/getBioHistory"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_BIO_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupBioInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupWeight"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_WEIGHT:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupBloodPressure"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_BLOODPRESSURE:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupBloodSugar"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_BLOODSUGAR:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/getBloodSugar"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_BIO_BLOODSUGAR:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupPulseOximeter"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_PULSEOXIMETER:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupBodyTemperature"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_TEMPERATURE:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupECG"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_ECG:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/getECG"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_BIO_ECG:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupSkin"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_SKIN:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/getSkin"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_BIO_SKIN:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/getHeartRateHistory"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_BIO_HEARTRATE:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/backupHeartRate"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_BIO_HEARTRATE:Ljava/lang/String;

    const-string/jumbo v0, "platform/bio/deleteBioInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_BIO_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/getTag"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_TAG_INDEX:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupTag"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_TAG_INDEX:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/deleteTag"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_TAG_INDEX:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/getExerciseHistory"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_EXERCISE_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/getFitnessInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_WORKOUT_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupFitnessInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FITNESS_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupExerciseActivity"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_ACTIVITY_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupWalkingInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_WALKING_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupExerciseActivity"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_EXERCISE_ACTIVITY:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/deleteWorkOutInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_WORKOUT_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/getRealTimeData"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_REALTIME_DATA:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/setRealTimeData"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_REALTIME_DATA:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/getLocationData"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_LOCATION_DATA:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/setLocationData"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_LOCATION_DATA:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupExManualData"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_EXERCISE_MANUAL_DATA:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupFirstBeatVariable"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FIRSTBEAT_VARIABLE:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/getFirstBeatVariable"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FIRSTBEAT_VARIABLE:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/getFirstBeatCoachingResult"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FIRSTBEAT_RESULT:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupFirstBeatCoachingResult"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FIRSTBEAT_RESULT:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/deleteFirstBeatCoachingResult"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_FIRSTBEAT_RESULT:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/getWalkInfoExtended"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_EXTENDED_WALKINFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/backupWalkInfoExtended"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_EXTENDED_WALKINFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/workout/deleteWalkInfoExtended"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_EXTENDED_WALKINFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/backupIngestedFood"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_INGESTED_FOOD:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/getFoodInfoHistory"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FOOD_INFO_HISTORY:Ljava/lang/String;

    const-string v0, "/platform/food/deleteFatSecretFoodInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_FOOD_NUTRIENT_HISTORY:Ljava/lang/String;

    const-string v0, "/platform/food/deleteWelstoryFoodInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_WELSTORY_NUTRIENT_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/backupWaterIntakeInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_WATER_INTAKE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/deleteWaterInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_WATER_INTAKE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/getFatSecretFoodInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FOOD_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/backupFatSecretFoodInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/getWelstoryFoodInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_WELSTORY_FOOD_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/backupWelstoryFoodInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_WELSTORY_FOOD_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/food/deleteMealInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_FOOD_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/physiology/getPhysiologyHistory"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_PHYSIOLOGY_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/physiology/backupSleepInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_SLEEP_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/physiology/deleteSleepInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_SLEEP_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/physiology/backupStressInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_STRESS_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/physiology/deleteStressInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_STRESS_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/getUserAccInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USERDEVICE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserAccInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USERDEVICE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/getUserGoal"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USER_GOAL:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserGoal"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_GOAL:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/deleteUserGoal"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_USER_GOAL:Ljava/lang/String;

    const-string/jumbo v0, "platform/misc/getCustomItem"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_CUSTOM_ITEM:Ljava/lang/String;

    const-string/jumbo v0, "platform/misc/backupCustomItem"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_CUSTOM_ITEM:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserImageInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_IMAGE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserExerciseImageInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_EXERCISE_IMAGE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/deleteUserExerciseImage"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_EXERCISE_IMAGE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/deleteUserFoodImage"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_FOOD_IMAGE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserImage"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_USER_IMAGE:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserFoodImageInfo"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_IMAGE_INFO:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserExerciseImage"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_EXERCISE_IMAGE:Ljava/lang/String;

    const-string/jumbo v0, "platform/common/backupUserFoodImage"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_FOOD_IMAGE:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/backupUVIndex"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_UV:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/getUVIndex"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_UV_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/deleteUVIndex"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_UV_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/backupSPF"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_UV_PROTECTION:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/getSPF"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_UV_PROTECTION_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/deleteSPF"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_UV_PROTECTION_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/backupTemperatureHumidity"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->SET_TEMPERATURE_HUMIDITY:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/getTemperatureHumidity"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_TEMPERATURE_HUMIDITY_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/env/deleteTemperatureHumidity"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_TEMPERATURE_HUMIDITY_HISTORY:Ljava/lang/String;

    const-string/jumbo v0, "platform/profile/deleteUserProfile"

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DELETE_USER_DATA_IN_SERVER:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USER_PROFILE:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;->DETAIL_API_GET_USER_PROFILE:Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_USERDEVICE_INFO:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;->DETAIL_API_GET_USERDEVICE_INFO:Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_BIO_HISTORY:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;->DETAIL_API_GET_BIO_HISTORY:Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_BIO_HEARTRATE:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;->DETAIL_API_GET_BIO_HEARTRATE:Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_EXERCISE_HISTORY:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;->DETAIL_API_GET_EXERCISE_HISTORY:Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_PHYSIOLOGY_HISTORY:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;->DETAIL_API_GET_PHYSIOLOGY_HISTORY:Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->DETAIL_API_FROM_SYNC_HISTORY:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/network/HttpApi;->GET_FOOD_INFO_HISTORY:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;->DETAIL_API_GET_FOOD_INFO_HISTORY:Lcom/sec/android/service/health/cp/serversync/network/HttpApi$DetailApiSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
