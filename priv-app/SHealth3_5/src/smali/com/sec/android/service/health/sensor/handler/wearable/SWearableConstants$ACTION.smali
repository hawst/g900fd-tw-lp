.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$ACTION;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ACTION"
.end annotation


# static fields
.field public static final PROFILE_UPDATED_FROM_HEALTHSERVICE:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

.field public static final SYNC_ERROR:Ljava/lang/String; = "com.samsung.android.shealth.HEALTH_SYNC_ERROR"

.field public static final UPDATE_PROFILE_FROM_APP:Ljava/lang/String; = "com.sec.shealth.UPDATE_PROFILE"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
