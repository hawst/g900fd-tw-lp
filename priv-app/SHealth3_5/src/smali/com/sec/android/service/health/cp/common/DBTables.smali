.class public interface abstract Lcom/sec/android/service/health/cp/common/DBTables;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/common/DBTables$MyBluetoohDevices;,
        Lcom/sec/android/service/health/cp/common/DBTables$BPedometerTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$TrainingLoadPeakTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$RealTimeSpeedTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$RealTimeHeartBeatTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$MapPathTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$AntDeviceListTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$HealthboardTipsTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$TipInstanceTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$RunninProTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$ImagesTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$ExerciseInfoTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$WeightTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$BloodGlucoseView;,
        Lcom/sec/android/service/health/cp/common/DBTables$BloodGlucoseTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$BloodPressureView;,
        Lcom/sec/android/service/health/cp/common/DBTables$BloodPressureTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$ExerciseView;,
        Lcom/sec/android/service/health/cp/common/DBTables$ExerciseTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$MealItemsTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$MealView;,
        Lcom/sec/android/service/health/cp/common/DBTables$MealTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$ExtendedFoodInfoTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$FoodInfoTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$SensorTable;,
        Lcom/sec/android/service/health/cp/common/DBTables$GOAL_INSTANCE_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$SleepMonitorView;,
        Lcom/sec/android/service/health/cp/common/DBTables$SLEEP_MONITOR_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$USER_PROFILE_SHARE_DATA_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$USER_PROFILE_SHARE_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$USER_DEVICE_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$BLUETOOTH_DEVICES_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$NEWS_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$WIDGET_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$MEMO_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$GOAL_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$TEMP_COMFORT_ZONE_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$ComfortZoneView;,
        Lcom/sec/android/service/health/cp/common/DBTables$COMFORT_ZONE_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$TEMP_WALK_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$WalkForLifeView;,
        Lcom/sec/android/service/health/cp/common/DBTables$WALK_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$TEMP_SENSOR_TABLE;,
        Lcom/sec/android/service/health/cp/common/DBTables$CONFIGURATION_TABLE;
    }
.end annotation


# static fields
.field public static final BACKUP_PLATFORM_DB_NAME:Ljava/lang/String; = "platform_new.db"

.field public static final DATABASE_NAME:Ljava/lang/String; = "shealth2.db"

.field public static final OLD_PLATFORM_DB_NAME:Ljava/lang/String; = "platform.db.old"

.field public static final PLATFORM_DB_NAME:Ljava/lang/String; = "platform.db"

.field public static final SECURITY_CP_DATABASE_NAME:Ljava/lang/String; = "secure_sec_health.db"

.field public static final SEC_DATABASE_NAME:Ljava/lang/String; = "secure_shealth2.db"

.field public static final SEC_VALIDITY_CHECK_DATABASE_NAME:Ljava/lang/String; = "secure_shealth2.db_temp"
