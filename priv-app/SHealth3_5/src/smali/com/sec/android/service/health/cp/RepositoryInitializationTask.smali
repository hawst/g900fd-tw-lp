.class public Lcom/sec/android/service/health/cp/RepositoryInitializationTask;
.super Lcom/sec/android/service/health/cp/BaseInitializationTask;


# static fields
.field public static final BASE_DB_NAME:Ljava/lang/String; = "base.db"

.field public static final H_DB_NAME:Ljava/lang/String; = "shealth_.db"

.field public static final LAST_UPGRADED_VERSION:Ljava/lang/String; = "last_version_of_upgrade"

.field public static final SHAREDPREF_UPGRADE_FILE:Ljava/lang/String; = "upgrade_prefs"

.field private static final TAG:Ljava/lang/String;

.field public static final UPGRADE_CHECK_KEY:Ljava/lang/String; = "isUpgradeCheckRequired"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;-><init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    return-void
.end method


# virtual methods
.method protected handelCommand(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[+] handelCommand "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v4, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->publishProgress([Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->initialize()V

    :cond_0
    new-array v0, v4, [Ljava/lang/Integer;

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->publishProgress([Ljava/lang/Object;)V

    sget-object v0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "[-] handelCommand "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/RepositoryInitializationTask;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.ContentProviderAccessible"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
