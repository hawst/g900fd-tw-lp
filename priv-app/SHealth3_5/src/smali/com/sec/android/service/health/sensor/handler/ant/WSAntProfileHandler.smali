.class public Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$11;
    }
.end annotation


# static fields
.field private static final MSG_REQ_ADVANCED_MEASUREMENT:I = 0x2

.field private static final MSG_REQ_BASIC_MEASUREMENT:I = 0x1

.field private static final MSG_REQ_CAPABILITIES:I = 0x3

.field private static final MSG_REQ_DOWNLOAD_ALL_HISTORY:I = 0x4

.field private static final TAG:Ljava/lang/String; = "[HealthSensor]WSAntProfileHandler"

.field private static mLastMeasDeviceId:I

.field private static mLastMeasTime:J

.field private static prevWeight:I


# instance fields
.field private FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

.field private fFirstRequest:Z

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private final mCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

.field mWSdata:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

.field mdata:Landroid/os/Bundle;

.field private request_msg:I

.field wsmsg:Lcom/garmin/fit/WeightScaleMesg;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasTime:J

    const/4 v0, -0x1

    sput v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasDeviceId:I

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->prevWeight:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->fFirstRequest:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->request_msg:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mCount:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->wsmsg:Lcom/garmin/fit/WeightScaleMesg;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mdata:Landroid/os/Bundle;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mWSdata:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$9;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$9;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$10;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$10;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->readFitFromData([B)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->openFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->prevWeight:I

    return v0
.end method

.method static synthetic access$702(I)I
    .locals 0

    sput p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->prevWeight:I

    return p0
.end method

.method static synthetic access$802(J)J
    .locals 0

    sput-wide p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasTime:J

    return-wide p0
.end method

.method static synthetic access$902(I)I
    .locals 0

    sput p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasDeviceId:I

    return p0
.end method

.method private getAgeValue(Ljava/lang/String;)I
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/android/service/health/cp/common/HealthServiceUtil;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ge v3, v4, :cond_1

    add-int/lit8 v0, v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private openFile(Ljava/lang/String;)V
    .locals 7

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "[HealthSensor]WSAntProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2 openFile FilePath::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v2, 0x400

    new-array v2, v2, [B

    :goto_0
    invoke-virtual {v1, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_2
    const-string v1, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v2, "openFile - RuntimeException"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    :cond_0
    :goto_2
    return-void

    :cond_1
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Lcom/garmin/fit/Decode;

    invoke-direct {v0}, Lcom/garmin/fit/Decode;-><init>()V

    new-instance v3, Lcom/garmin/fit/MesgBroadcaster;

    invoke-direct {v3, v0}, Lcom/garmin/fit/MesgBroadcaster;-><init>(Lcom/garmin/fit/Decode;)V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWSListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getAntDeviceType()I

    move-result v5

    invoke-direct {v0, v4, v5}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWSListener;-><init>(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWSListener;->setNotiListener(Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;)V

    invoke-virtual {v3, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/FileIdMesgListener;)V

    invoke-virtual {v3, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/UserProfileMesgListener;)V

    invoke-virtual {v3, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/DeviceInfoMesgListener;)V

    invoke-virtual {v3, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/WeightScaleMesgListener;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v3, v2}, Lcom/garmin/fit/MesgBroadcaster;->run(Ljava/io/InputStream;)V
    :try_end_5
    .catch Lcom/garmin/fit/FitRuntimeException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_3
    if-eqz v1, :cond_0

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v1, "openFile - IOExceptiond"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_8
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v2, "openFile - IOException"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    :catch_3
    move-exception v0

    :goto_4
    :try_start_9
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v2, "openFile - Exception"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v1, :cond_0

    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_2

    :catch_4
    move-exception v0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v1, "openFile - IOExceptiond"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_5
    move-exception v0

    :try_start_b
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v3, "openFile - FitRuntimeException"

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_3

    :catch_6
    move-exception v0

    :try_start_d
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v2, "openFile - IOException"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v1, :cond_2

    :try_start_e
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    :cond_2
    :goto_6
    throw v0

    :catchall_1
    move-exception v0

    :try_start_f
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :goto_7
    :try_start_10
    throw v0

    :catch_7
    move-exception v2

    const-string v2, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v3, "openFile - IOException"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_7

    :catch_8
    move-exception v0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v1, "openFile - IOExceptiond"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :catch_9
    move-exception v1

    const-string v1, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v2, "openFile - IOExceptiond"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_3
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "FileLoadErrer"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :catchall_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    :catchall_3
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    :catch_a
    move-exception v1

    move-object v1, v0

    goto :goto_4

    :catch_b
    move-exception v1

    goto/16 :goto_1
.end method

.method private readFitFromData([B)V
    .locals 5

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Lcom/garmin/fit/Decode;

    invoke-direct {v0}, Lcom/garmin/fit/Decode;-><init>()V

    new-instance v2, Lcom/garmin/fit/MesgBroadcaster;

    invoke-direct {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;-><init>(Lcom/garmin/fit/Decode;)V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWSListener;

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getAntDeviceType()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWSListener;-><init>(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->FitNotilisten:Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitWSListener;->setNotiListener(Lcom/sec/android/service/health/sensor/handler/ant/fitutil/FitListener$FitNotiListener;)V

    invoke-virtual {v2, v0}, Lcom/garmin/fit/MesgBroadcaster;->addListener(Lcom/garmin/fit/WeightScaleMesgListener;)V

    :try_start_0
    invoke-virtual {v2, v1}, Lcom/garmin/fit/MesgBroadcaster;->run(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/garmin/fit/FitRuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v1, "readFitFromData - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v2, "readFitFromData - FitRuntimeException"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v1, "readFitFromData - IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_1
    throw v0

    :catch_3
    move-exception v1

    const-string v1, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v2, "readFitFromData - IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public FitFileopen(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$8;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$8;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WSAntProfileHandler FitFileopen #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getAntDeviceType()I
    .locals 1

    const/16 v0, 0x77

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 1

    const/16 v0, 0x7d1

    return v0
.end method

.method public getUserProfile()V
    .locals 8

    const/4 v7, -0x1

    const/4 v6, 0x0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "getUserProfile ~~"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v1

    const v2, 0x2e635

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    :goto_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v1

    const v2, 0x249f1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iput v7, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iput-boolean v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    :goto_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getAgeValue(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->age:I

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v1

    const v2, 0x2e636

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->gender:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v1

    const v2, 0x249f2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x403e7ae147ae147bL    # 30.48

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iput v7, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->height:I

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    const/4 v2, 0x6

    iput v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    const/4 v2, 0x5

    iput v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iput-boolean v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    const/4 v2, 0x4

    iput v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iput-boolean v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    goto :goto_2

    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    const/4 v2, 0x3

    iput v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iput-boolean v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    goto :goto_2

    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    const/4 v2, 0x2

    iput v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->activityLevel:I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    iput-boolean v6, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;->lifetimeAthlete:Z

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2bf21
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 3

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage() :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestBasicMeasurement()Z

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestAdvancedMeasurement()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CMD_REQUEST_BASIC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->request_msg:I

    const-string v2, "RES_REQUEST_BASIC"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorDescription(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x3ea

    iput v3, v2, Landroid/os/Message;->what:I

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CMD_REQUEST_ADV"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->request_msg:I

    const-string v2, "RES_REQUEST_ADV"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CMD_REQUEST_CAP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->request_msg:I

    const-string v2, "RES_REQUEST_CAP"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CMD_REQUEST_HISTORY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->request_msg:I

    const-string v2, "RES_REQUEST_HISTORY"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x3e9

    goto :goto_1
.end method

.method protected requestAccess()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void
.end method

.method public requestAdvancedMeasurement()Z
    .locals 10

    const-wide/16 v8, 0x7530

    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getConnectState()I

    move-result v0

    if-eq v0, v7, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    sget-wide v3, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasTime:J

    add-long/2addr v3, v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->fFirstRequest:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasDeviceId:I

    iget v5, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I

    if-ne v0, v5, :cond_2

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    cmp-long v0, v3, v8

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    invoke-virtual {v0, v7}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    invoke-virtual {v1, v7}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sendMessageDelayed MSG_REQ_ADVANCED_MEASUREMENT "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->fFirstRequest:Z

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getUserProfile()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$3;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$3;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfile:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    invoke-virtual {v0, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestAdvancedMeasurement(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IAdvancedMeasurementFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "AdvancedMeasurement: Computing"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public requestBasicMeasurement()Z
    .locals 9

    const-wide/16 v7, 0x7530

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getConnectState()I

    move-result v0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    sget-wide v3, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasTime:J

    add-long/2addr v3, v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->fFirstRequest:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mLastMeasDeviceId:I

    iget v5, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I

    if-ne v0, v5, :cond_2

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    cmp-long v0, v3, v7

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sendMessageDelayed MSG_REQ_BASIC_MEASUREMENT "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_2
    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string/jumbo v3, "requestBasicMeasurement"

    invoke-static {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->fFirstRequest:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    invoke-virtual {v0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestBasicMeasurement(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IBasicMeasurementFinishedReceiver;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "BasicMeasurement: Computing"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public requestCapabilities()Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$4;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$4;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestCapabilities(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$ICapabilitiesRequestFinishedReceiver;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "CapabilitiesRequest: Req Capab"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestDownloadAllhistory()Z
    .locals 4

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;

    new-instance v1, Lcom/garmin/fit/WeightScaleMesg;

    invoke-direct {v1}, Lcom/garmin/fit/WeightScaleMesg;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->wsmsg:Lcom/garmin/fit/WeightScaleMesg;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mdata:Landroid/os/Bundle;

    new-instance v1, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$5;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    new-instance v2, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$6;

    invoke-direct {v2, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$6;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$7;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler$7;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;->requestDownloadAllHistory(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$IDownloadAllHistoryFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z

    move-result v0

    return v0
.end method

.method public sendRequest(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    .locals 0

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method

.method public setUserProfileData(IIIII)V
    .locals 3

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    const-string v1, "getUserProfile ~~"

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    if-nez p5, :cond_1

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    :goto_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    invoke-virtual {v2, p1}, Lcom/sec/android/service/health/sensor/data/UserProfileData;->setAge(I)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    invoke-virtual {v2, p2}, Lcom/sec/android/service/health/sensor/data/UserProfileData;->setHeight(I)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    invoke-virtual {v2, p3}, Lcom/sec/android/service/health/sensor/data/UserProfileData;->setActivityLevel(I)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    invoke-virtual {v2, v0}, Lcom/sec/android/service/health/sensor/data/UserProfileData;->setLifeAthlete(Z)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mUserProfileData:Lcom/sec/android/service/health/sensor/data/UserProfileData;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/data/UserProfileData;->setGender(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    goto :goto_1
.end method

.method public startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getConnectState()I

    move-result v2

    const/16 v3, 0x12c

    if-ne v2, v3, :cond_0

    :goto_0
    return v1

    :cond_0
    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->request_msg:I

    packed-switch v2, :pswitch_data_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestBasicMeasurement()Z

    move-result v2

    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->notifyOnDataStarted(I)V

    :cond_1
    if-eqz v2, :cond_2

    :goto_2
    move v1, v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestBasicMeasurement()Z

    move-result v2

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestAdvancedMeasurement()Z

    move-result v2

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestCapabilities()Z

    move-result v2

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->requestDownloadAllhistory()Z

    move-result v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public unregisterDevice()V
    .locals 3

    const-string v0, "[HealthSensor]WSAntProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregistDevice WS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->getAntDeviceType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mAntDeviceID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/WSAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;->removeMessages(I)V

    invoke-super {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;->unregisterDevice()V

    return-void
.end method
