.class final Lcom/sec/android/service/health/contentservice/ContentService$2;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/contentservice/ContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 23

    const/4 v1, 0x2

    new-array v6, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x2

    aput-wide v2, v6, v1

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0xa98

    aput v15, v2, v14

    const/16 v14, -0x68

    aput v14, v2, v13

    const/16 v13, -0x3bc4

    aput v13, v2, v12

    const/16 v12, -0x70

    aput v12, v2, v11

    const/16 v11, -0x7e

    aput v11, v2, v10

    const/16 v10, -0x1e

    aput v10, v2, v9

    const/16 v9, -0x6b

    aput v9, v2, v8

    const/16 v8, -0x47eb

    aput v8, v2, v7

    const/16 v7, -0x37

    aput v7, v2, v4

    const/16 v4, 0x3a42

    aput v4, v2, v3

    const/16 v3, -0x59b8

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, -0xaf3

    aput v16, v1, v15

    const/16 v15, -0xb

    aput v15, v1, v14

    const/16 v14, -0x3bab

    aput v14, v1, v13

    const/16 v13, -0x3c

    aput v13, v1, v12

    const/16 v12, -0xa

    aput v12, v1, v11

    const/16 v11, -0x6f

    aput v11, v1, v10

    const/16 v10, -0x10

    aput v10, v1, v9

    const/16 v9, -0x47a0

    aput v9, v1, v8

    const/16 v8, -0x48

    aput v8, v1, v7

    const/16 v7, 0x3a27

    aput v7, v1, v4

    const/16 v4, -0x59c6

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/16 v12, -0x32

    aput v12, v2, v11

    const/16 v11, -0x6f3

    aput v11, v2, v10

    const/16 v10, -0x73

    aput v10, v2, v9

    const/16 v9, -0x1f

    aput v9, v2, v4

    const/16 v4, -0x15

    aput v4, v2, v3

    const/16 v3, -0x17

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/16 v13, -0x43

    aput v13, v1, v12

    const/16 v12, -0x688

    aput v12, v1, v11

    const/4 v11, -0x7

    aput v11, v1, v10

    const/16 v10, -0x80

    aput v10, v1, v9

    const/16 v9, -0x61

    aput v9, v1, v4

    const/16 v4, -0x66

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v1

    :goto_4
    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, -0x2c

    aput v21, v2, v20

    const/16 v20, -0x5b

    aput v20, v2, v19

    const/16 v19, -0x55e9

    aput v19, v2, v18

    const/16 v18, -0x3a

    aput v18, v2, v17

    const/16 v17, 0xe3d

    aput v17, v2, v16

    const/16 v16, -0x6fb3

    aput v16, v2, v15

    const/16 v15, -0xc

    aput v15, v2, v14

    const/16 v14, -0x24e9

    aput v14, v2, v13

    const/16 v13, -0x53

    aput v13, v2, v12

    const/16 v12, 0x4674

    aput v12, v2, v11

    const/16 v11, 0x6534

    aput v11, v2, v10

    const/16 v10, 0x4915

    aput v10, v2, v9

    const/16 v9, 0x1b39

    aput v9, v2, v4

    const/16 v4, -0x6186

    aput v4, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x2

    const/4 v11, 0x3

    const/4 v12, 0x4

    const/4 v13, 0x5

    const/4 v14, 0x6

    const/4 v15, 0x7

    const/16 v16, 0x8

    const/16 v17, 0x9

    const/16 v18, 0xa

    const/16 v19, 0xb

    const/16 v20, 0xc

    const/16 v21, 0xd

    const/16 v22, -0x46

    aput v22, v1, v21

    const/16 v21, -0x38

    aput v21, v1, v20

    const/16 v20, -0x559e

    aput v20, v1, v19

    const/16 v19, -0x56

    aput v19, v1, v18

    const/16 v18, 0xe52

    aput v18, v1, v17

    const/16 v17, -0x6ff2

    aput v17, v1, v16

    const/16 v16, -0x70

    aput v16, v1, v15

    const/16 v15, -0x248e

    aput v15, v1, v14

    const/16 v14, -0x25

    aput v14, v1, v13

    const/16 v13, 0x461b

    aput v13, v1, v12

    const/16 v12, 0x6546

    aput v12, v1, v11

    const/16 v11, 0x4965

    aput v11, v1, v10

    const/16 v10, 0x1b49

    aput v10, v1, v9

    const/16 v9, -0x61e5

    aput v9, v1, v4

    const/4 v4, 0x0

    :goto_5
    array-length v9, v1

    if-lt v4, v9, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v4, 0x0

    :goto_6
    array-length v9, v1

    if-lt v4, v9, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    const/4 v4, 0x0

    array-length v5, v6

    add-int/lit8 v5, v5, -0x1

    aget-wide v10, v6, v5

    long-to-int v5, v10

    if-gtz v5, :cond_6

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    aget v4, v1, v3

    aget v7, v2, v3

    xor-int/2addr v4, v7

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v1, v3

    aget v9, v2, v3

    xor-int/2addr v4, v9

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :catch_0
    move-exception v1

    const/4 v1, 0x1

    new-array v1, v1, [Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-boolean v3, v1, v2

    move-object v3, v1

    goto/16 :goto_4

    :cond_4
    aget v9, v1, v4

    aget v10, v2, v4

    xor-int/2addr v9, v10

    aput v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_5
    aget v9, v2, v4

    int-to-char v9, v9

    aput-char v9, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_6
    const/4 v10, 0x0

    int-to-long v4, v2

    const/16 v2, 0x20

    shl-long/2addr v4, v2

    const/16 v2, 0x20

    ushr-long v11, v4, v2

    aget-wide v4, v6, v10

    const-wide/16 v13, 0x0

    cmp-long v2, v4, v13

    if-eqz v2, :cond_7

    const-wide v13, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v13

    :cond_7
    const/16 v2, 0x20

    ushr-long/2addr v4, v2

    const/16 v2, 0x20

    shl-long/2addr v4, v2

    xor-long/2addr v4, v11

    const-wide v11, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v11

    aput-wide v4, v6, v10

    const/4 v2, 0x0

    const/4 v4, 0x1

    array-length v5, v6

    add-int/lit8 v5, v5, -0x1

    aget-wide v10, v6, v5

    long-to-int v5, v10

    if-lt v4, v5, :cond_8

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    const/4 v10, 0x0

    int-to-long v4, v2

    const/16 v2, 0x20

    shl-long v11, v4, v2

    aget-wide v4, v6, v10

    const-wide/16 v13, 0x0

    cmp-long v2, v4, v13

    if-eqz v2, :cond_9

    const-wide v13, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v13

    :cond_9
    const/16 v2, 0x20

    shl-long/2addr v4, v2

    const/16 v2, 0x20

    ushr-long/2addr v4, v2

    xor-long/2addr v4, v11

    const-wide v11, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v11

    aput-wide v4, v6, v10

    :goto_7
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v6, v2

    long-to-int v2, v4

    const/4 v4, 0x1

    if-gt v2, v4, :cond_c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x2c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x1b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_c
    const/4 v2, 0x0

    aget-wide v4, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_d

    const-wide v10, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v10

    :cond_d
    const/16 v2, 0x20

    shr-long/2addr v4, v2

    long-to-int v2, v4

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v6, v4

    long-to-int v4, v4

    if-gtz v4, :cond_10

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x3b

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0xb

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_10
    const/4 v4, 0x0

    aget-wide v4, v6, v4

    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-eqz v10, :cond_11

    const-wide v10, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v10

    :cond_11
    const/16 v10, 0x20

    shl-long/2addr v4, v10

    const/16 v10, 0x20

    shr-long/2addr v4, v10

    long-to-int v4, v4

    if-ge v2, v4, :cond_1e

    :try_start_1
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v6, v2

    long-to-int v2, v4

    const/4 v4, 0x1

    if-gt v2, v4, :cond_14

    new-instance v10, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v4, v2, [I

    const/4 v2, 0x0

    const/16 v5, 0xa0c

    aput v5, v4, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v5, 0x0

    const/16 v11, 0xa3d

    aput v11, v2, v5

    const/4 v5, 0x0

    :goto_c
    array-length v11, v2

    if-lt v5, v11, :cond_12

    array-length v2, v4

    new-array v2, v2, [C

    const/4 v5, 0x0

    :goto_d
    array-length v11, v2

    if-lt v5, v11, :cond_13

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v10, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_e
    const/4 v10, 0x1

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v6, v2

    long-to-int v2, v4

    if-lt v10, v2, :cond_1a

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_12
    :try_start_2
    aget v11, v2, v5

    aget v12, v4, v5

    xor-int/2addr v11, v12

    aput v11, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    :cond_13
    aget v11, v4, v5

    int-to-char v11, v11

    aput-char v11, v2, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_d

    :cond_14
    const/4 v2, 0x0

    aget-wide v4, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_15

    const-wide v10, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v10

    :cond_15
    const/16 v2, 0x20

    shr-long/2addr v4, v2

    long-to-int v2, v4

    invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v6, v2

    long-to-int v2, v4

    const/4 v4, 0x1

    if-gt v2, v4, :cond_18

    new-instance v10, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v4, v2, [I

    const/4 v2, 0x0

    const/16 v5, -0x28

    aput v5, v4, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v5, 0x0

    const/16 v11, -0x17

    aput v11, v2, v5

    const/4 v5, 0x0

    :goto_f
    array-length v11, v2

    if-lt v5, v11, :cond_16

    array-length v2, v4

    new-array v2, v2, [C

    const/4 v5, 0x0

    :goto_10
    array-length v11, v2

    if-lt v5, v11, :cond_17

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v10, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_16
    aget v11, v2, v5

    aget v12, v4, v5

    xor-int/2addr v11, v12

    aput v11, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_f

    :cond_17
    aget v11, v4, v5

    int-to-char v11, v11

    aput-char v11, v2, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    :cond_18
    const/4 v2, 0x0

    aget-wide v4, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_19

    const-wide v10, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v10

    :cond_19
    const/16 v2, 0x20

    shr-long/2addr v4, v2

    long-to-int v2, v4

    invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;

    invoke-interface {v2, v7, v8, v3, v9}, Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;->onDataAction(J[Z[Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_e

    :cond_1a
    const/4 v2, 0x0

    aget-wide v4, v6, v2

    const-wide/16 v11, 0x0

    cmp-long v2, v4, v11

    if-eqz v2, :cond_1b

    const-wide v11, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v11

    :cond_1b
    const/16 v2, 0x20

    shr-long/2addr v4, v2

    long-to-int v2, v4

    add-int/lit8 v2, v2, 0x1

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v6, v4

    long-to-int v4, v4

    if-lt v10, v4, :cond_1c

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1c
    const/4 v10, 0x0

    int-to-long v4, v2

    const/16 v2, 0x20

    shl-long v11, v4, v2

    aget-wide v4, v6, v10

    const-wide/16 v13, 0x0

    cmp-long v2, v4, v13

    if-eqz v2, :cond_1d

    const-wide v13, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v13

    :cond_1d
    const/16 v2, 0x20

    shl-long/2addr v4, v2

    const/16 v2, 0x20

    ushr-long/2addr v4, v2

    xor-long/2addr v4, v11

    const-wide v11, -0x54789801c4e6ffb3L    # -5.350603554432241E-99

    xor-long/2addr v4, v11

    aput-wide v4, v6, v10

    goto/16 :goto_7

    :cond_1e
    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    return-void

    :catch_2
    move-exception v2

    goto/16 :goto_e
.end method
