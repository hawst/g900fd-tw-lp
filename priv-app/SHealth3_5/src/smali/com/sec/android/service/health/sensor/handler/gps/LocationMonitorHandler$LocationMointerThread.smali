.class Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationMointerThread"
.end annotation


# instance fields
.field private mLocHandler:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;

.field private volatile mLocationLooper:Landroid/os/Looper;

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->this$0:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->mLocationLooper:Landroid/os/Looper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;-><init>(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)V

    return-void
.end method


# virtual methods
.method public destroyLooper()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->mLocationLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->this$0:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->locationMonitor:Lcom/samsung/location/monitor/LocationMonitor;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;->access$002(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor;

    return-void
.end method

.method public getLocLooper()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->mLocHandler:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;

    return-object v0
.end method

.method public run()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->this$0:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;-><init>(Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->mLocHandler:Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$MyHandler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/gps/LocationMonitorHandler$LocationMointerThread;->mLocationLooper:Landroid/os/Looper;

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
