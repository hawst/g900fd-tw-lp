.class public Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
.super Ljava/lang/Object;
.source "RequestParamameters.java"


# instance fields
.field private httpEntity:Lorg/apache/http/HttpEntity;

.field private isOneTimeDemoMode:Z

.field private methodType:Lcom/sec/android/service/health/connectionmanager2/MethodType;

.field private privateId:I

.field private requestId:J

.field private requester:Ljava/lang/Object;

.field private response:Ljava/lang/Object;

.field private responsehandler:Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

.field private setHeadervalues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tag:Ljava/lang/Object;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(JILjava/lang/Object;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/MethodType;Lorg/apache/http/HttpEntity;Ljava/lang/Object;)V
    .locals 1
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "requester"    # Ljava/lang/Object;
    .param p5, "responsehandler"    # Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .param p6, "url"    # Ljava/lang/String;
    .param p7, "methodType"    # Lcom/sec/android/service/health/connectionmanager2/MethodType;
    .param p8, "httpEntity"    # Lorg/apache/http/HttpEntity;
    .param p9, "tag"    # Ljava/lang/Object;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setHeadervalues:Ljava/util/HashMap;

    .line 52
    iput-wide p1, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->requestId:J

    .line 53
    iput p3, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->privateId:I

    .line 54
    iput-object p4, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->requester:Ljava/lang/Object;

    .line 55
    iput-object p5, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->responsehandler:Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

    .line 56
    iput-object p6, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->url:Ljava/lang/String;

    .line 57
    iput-object p7, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->methodType:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    .line 60
    iput-object p8, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->httpEntity:Lorg/apache/http/HttpEntity;

    .line 61
    iput-object p9, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->tag:Ljava/lang/Object;

    .line 62
    return-void
.end method

.method public constructor <init>(JILjava/lang/Object;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/MethodType;Lorg/apache/http/HttpEntity;Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "requester"    # Ljava/lang/Object;
    .param p5, "responsehandler"    # Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .param p6, "Url"    # Ljava/lang/String;
    .param p7, "methodType"    # Lcom/sec/android/service/health/connectionmanager2/MethodType;
    .param p8, "httpEntity"    # Lorg/apache/http/HttpEntity;
    .param p9, "tag"    # Ljava/lang/Object;
    .param p10, "isOneTimeDemoMode"    # Z

    .prologue
    .line 66
    invoke-direct/range {p0 .. p9}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;-><init>(JILjava/lang/Object;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/MethodType;Lorg/apache/http/HttpEntity;Ljava/lang/Object;)V

    .line 67
    iput-boolean p10, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->isOneTimeDemoMode:Z

    .line 68
    return-void
.end method

.method public constructor <init>(JILjava/lang/Object;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/MethodType;Lorg/apache/http/HttpEntity;Ljava/lang/Object;ZLjava/util/HashMap;)V
    .locals 0
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "requester"    # Ljava/lang/Object;
    .param p5, "responsehandler"    # Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .param p6, "Url"    # Ljava/lang/String;
    .param p7, "methodType"    # Lcom/sec/android/service/health/connectionmanager2/MethodType;
    .param p8, "httpEntity"    # Lorg/apache/http/HttpEntity;
    .param p9, "tag"    # Ljava/lang/Object;
    .param p10, "isOneTimeDemoMode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/lang/Object;",
            "Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/connectionmanager2/MethodType;",
            "Lorg/apache/http/HttpEntity;",
            "Ljava/lang/Object;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p11, "setHeadervalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p10}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;-><init>(JILjava/lang/Object;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/MethodType;Lorg/apache/http/HttpEntity;Ljava/lang/Object;Z)V

    .line 73
    iput-object p11, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setHeadervalues:Ljava/util/HashMap;

    .line 74
    return-void
.end method


# virtual methods
.method public getHttpEntity()Lorg/apache/http/HttpEntity;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->httpEntity:Lorg/apache/http/HttpEntity;

    return-object v0
.end method

.method public getPrivateId()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->privateId:I

    return v0
.end method

.method public getRequestId()J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->requestId:J

    return-wide v0
.end method

.method public getRequester()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->requester:Ljava/lang/Object;

    return-object v0
.end method

.method public getResponse()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->response:Ljava/lang/Object;

    return-object v0
.end method

.method public getResponsehandler()Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->responsehandler:Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;

    return-object v0
.end method

.method public getSetHeadervalues()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setHeadervalues:Ljava/util/HashMap;

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->tag:Ljava/lang/Object;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->url:Ljava/lang/String;

    return-object v0
.end method

.method public isOneTimeDemoMode()Z
    .locals 2

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->isOneTimeDemoMode:Z

    .line 148
    .local v0, "retValue":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->isOneTimeDemoMode:Z

    .line 149
    return v0
.end method

.method public setResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "response"    # Ljava/lang/Object;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->response:Ljava/lang/Object;

    .line 168
    return-void
.end method
