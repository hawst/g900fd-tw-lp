.class final Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/SensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/SensorService;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/SensorService;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    const/4 v6, -0x1

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorListener] mCbHandler handleMessage msg.what : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "[SensorListener] handleMessage case default"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "handler_command"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    const-string v1, "handler_response"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    if-eqz v0, :cond_3

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onResponseReceived id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v2, :cond_0

    sget-object v4, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v6

    :goto_1
    if-ge v5, v6, :cond_2

    invoke-virtual {v2, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    invoke-interface {v3, v0, v1}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    const-string v0, "[HealthSensor]SensorService"

    const-string/jumbo v1, "onResponseReceived null command, This is Response without Command"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v0, "handler_scanned_device"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v1, :cond_0

    sget-object v3, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v4

    :goto_2
    if-ge v5, v4, :cond_5

    invoke-virtual {v1, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    monitor-exit v3

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v3

    :goto_3
    if-ge v5, v3, :cond_7

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;->onScanStarted(I)V

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_7
    monitor-exit v2

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result v3

    :goto_4
    if-ge v5, v3, :cond_9

    :try_start_4
    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;->onScanStopped(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :catchall_3
    move-exception v0

    :try_start_5
    throw v0

    :catchall_4
    move-exception v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    throw v0

    :cond_9
    :try_start_6
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v2

    :try_start_7
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    move-result v3

    :goto_5
    if-ge v5, v3, :cond_c

    :try_start_8
    iget v1, p1, Landroid/os/Message;->arg2:I

    if-ne v1, v6, :cond_b

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;->onConnected(I)V

    :cond_a
    :goto_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_b
    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v4, p1, Landroid/os/Message;->arg2:I

    if-ne v1, v4, :cond_a

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;->onConnected(I)V
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    goto :goto_6

    :catch_0
    move-exception v1

    :try_start_9
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    goto :goto_6

    :catchall_5
    move-exception v0

    :try_start_a
    throw v0

    :catchall_6
    move-exception v0

    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    throw v0

    :cond_c
    :try_start_b
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v2

    :try_start_c
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    move-result v3

    :goto_7
    if-ge v5, v3, :cond_f

    :try_start_d
    iget v1, p1, Landroid/os/Message;->arg2:I

    if-ne v1, v6, :cond_e

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;->onDisconnected(I)V

    :cond_d
    :goto_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_e
    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v4, p1, Landroid/os/Message;->arg2:I

    if-ne v1, v4, :cond_d

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;->onDisconnected(I)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    goto :goto_8

    :catchall_7
    move-exception v0

    :try_start_e
    throw v0

    :catchall_8
    move-exception v0

    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    throw v0

    :cond_f
    :try_start_f
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v2
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    goto/16 :goto_0

    :pswitch_7
    const-string v0, "_Health"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    const-string v1, "_Extra"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_30

    const-string v1, "_Extra"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    move-object v3, v1

    :goto_9
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v1, :cond_14

    sget-object v6, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v6

    :try_start_10
    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v9

    const-string v2, "[HealthSensor]SensorService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "HANDLER_DATA_RECEIVED broadcastNum = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    :goto_a
    if-ge v5, v9, :cond_13

    :try_start_11
    invoke-virtual {v1, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_11

    if-nez v0, :cond_10

    if-eqz v3, :cond_12

    :cond_10
    invoke-virtual {v1, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;

    new-instance v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;

    invoke-direct {v10, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    invoke-interface {v2, v10, v3}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;Landroid/os/Bundle;)V

    :cond_11
    :goto_b
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :cond_12
    const-string v2, "[HealthSensor]SensorService"

    const-string v10, "HANDLER_DATA_RECEIVED  data and extra are null"

    invoke-static {v2, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_9

    goto :goto_b

    :catchall_9
    move-exception v0

    :try_start_12
    throw v0

    :catchall_a
    move-exception v0

    monitor-exit v6
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_a

    throw v0

    :cond_13
    :try_start_13
    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v6
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    :cond_14
    const-string v1, "isSHealthPlugin"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string/jumbo v1, "shealth_device_data_type"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string/jumbo v1, "shealth_device"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2f

    const-string/jumbo v1, "shealth_device"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    :goto_c
    const-string/jumbo v4, "record"

    invoke-virtual {v8, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string/jumbo v6, "shealth_unique_id"

    invoke-virtual {v8, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v9, "_Health"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v9, "isSHealthPlugin"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string/jumbo v9, "shealth_device_data_type"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string/jumbo v9, "shealth_device"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string/jumbo v9, "record"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string/jumbo v9, "shealth_unique_id"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    if-eqz v0, :cond_15

    if-nez v1, :cond_16

    :cond_15
    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "HANDLER_DATA_RECEIVED  data or device is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_16
    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->isWearableDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_18

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HANDLER_DATA_RECEIVED  addData() is called for WearableDevice : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/utils/LogMessage;->secD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    invoke-virtual {v1, v0, v8, v3}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v1

    :goto_d
    if-eqz v0, :cond_17

    const-string v2, "[HealthSensor]SensorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DATA_INSERT data insert finished : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/health/sensor/utils/LogMessage;->secE(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    if-nez v1, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "HANDLER_DATA_RECEIVED  DB data insert Failure"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/utils/LogMessage;->secE(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HANDLER_DATA_RECEIVED  DB data insert Failure : UniqueKey "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mDBInsertFailureID:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1700()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_18
    if-eqz v2, :cond_1a

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v5}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-eqz v1, :cond_19

    if-eqz v4, :cond_1a

    :cond_19
    const-string v1, "[HealthSensor]SensorService"

    const-string v2, "HANDLER_DATA_RECEIVED  addData() is called for Plugin application "

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/utils/LogMessage;->secD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    invoke-virtual {v1, v0, v8, v3}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v1

    goto :goto_d

    :cond_1a
    if-nez v2, :cond_2e

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v5}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-eqz v1, :cond_2e

    if-eqz v4, :cond_2e

    const-string v1, "[HealthSensor]SensorService"

    const-string v2, "HANDLER_DATA_RECEIVED  addData() is called for 3rd Party application "

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/utils/LogMessage;->secD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    invoke-virtual {v1, v0, v8, v3}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v1

    goto/16 :goto_d

    :pswitch_8
    const-string v0, "_Health"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    const-string v1, "_Extra"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, [Landroid/os/Bundle;

    check-cast v1, [Landroid/os/Bundle;

    if-eqz v0, :cond_1b

    array-length v2, v0

    new-array v4, v2, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;

    move v2, v5

    :goto_e
    array-length v3, v0

    if-ge v2, v3, :cond_1b

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;

    aget-object v6, v0, v2

    invoke-direct {v3, v6}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    aput-object v3, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    :cond_1b
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    sget-object v9, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v9

    :try_start_14
    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_c

    move-result v10

    move v6, v5

    :goto_f
    if-ge v6, v10, :cond_1d

    :try_start_15
    invoke-virtual {v2, v6}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1c

    if-eqz v4, :cond_1c

    invoke-virtual {v2, v6}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;

    invoke-interface {v3, v4, v1}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;->onBulkDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;[Landroid/os/Bundle;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_b

    :cond_1c
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_f

    :catchall_b
    move-exception v0

    :try_start_16
    throw v0

    :catchall_c
    move-exception v0

    monitor-exit v9
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_c

    throw v0

    :cond_1d
    :try_start_17
    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v9
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_c

    const-string/jumbo v2, "shealth_device"

    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string/jumbo v3, "record"

    invoke-virtual {v8, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string/jumbo v4, "shealth_unique_id"

    invoke-virtual {v8, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_0

    array-length v6, v0

    if-lez v6, :cond_0

    if-eqz v3, :cond_0

    const-string v3, "[HealthSensor]SensorService"

    const-string v6, "HANDLER_BULK_DATA_RECEIVED  addData() is called"

    invoke-static {v3, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v3

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2, v1}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;[Landroid/os/Bundle;)Z

    move-result v1

    if-nez v1, :cond_1e

    const-string v1, "[HealthSensor]SensorService"

    const-string v2, "HANDLER_BULK_DATA_RECEIVED  DB data insert Failure"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HANDLER_BULK_DATA_RECEIVED  DB data insert Failure : UniqueKey "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mDBInsertFailureID:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1700()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1e
    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DATA_INSERT data insert finished : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v0, v0, v5

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_0

    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v2

    :try_start_18
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_e

    move-result v3

    :goto_10
    if-ge v5, v3, :cond_20

    :try_start_19
    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1f

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;->onStateChanged(I)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_d

    :cond_1f
    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    :catchall_d
    move-exception v0

    :try_start_1a
    throw v0

    :catchall_e
    move-exception v0

    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_e

    throw v0

    :cond_20
    :try_start_1b
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_e

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_23

    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1c
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_10

    move-result v3

    :goto_11
    if-ge v5, v3, :cond_22

    :try_start_1d
    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_21

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v4, v6}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;->onDataStarted(II)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_f

    :cond_21
    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    :catchall_f
    move-exception v0

    :try_start_1e
    throw v0

    :catchall_10
    move-exception v0

    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_10

    throw v0

    :cond_22
    :try_start_1f
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_10

    :cond_23
    const-string v0, "isSHealthPlugin"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v0, "shealth_device_data_type"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "exercise_id"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    const-string/jumbo v0, "shealth_device"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string/jumbo v0, "record"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->isWearableDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v3

    if-eqz v3, :cond_24

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HANDLER_DATA_STARTED  DataManager start() is called for WearableDevice : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1800()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    goto/16 :goto_0

    :cond_24
    if-eqz v4, :cond_26

    if-eqz v1, :cond_26

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-nez v1, :cond_25

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "HANDLER_DATA_STARTED  DataManager start() is called -> 1"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1800()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    goto/16 :goto_0

    :cond_25
    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "HANDLER_DATA_STARTED  DataManager start() is called -> 2"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1800()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    goto/16 :goto_0

    :cond_26
    if-eqz v4, :cond_0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    const-string v1, "HANDLER_DATA_STARTED  DataManager start() is called -> 3"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1800()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    goto/16 :goto_0

    :pswitch_b
    const-string v0, "exercise_id"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    const-string/jumbo v0, "shealth_device"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v4, :cond_0

    const-string v0, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HANDLER_DATA_RECORD  DataManager start() is called DataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exerciseId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1800()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    goto/16 :goto_0

    :pswitch_c
    const-string/jumbo v0, "shealth_unique_id"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_27

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mDBInsertFailureID:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1700()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    iput v7, p1, Landroid/os/Message;->arg2:I

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mDBInsertFailureID:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1700()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_27
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v0, :cond_2a

    sget-object v2, Lcom/sec/android/service/health/sensor/SensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v2

    :try_start_20
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getRegisteredCallbackCount()I
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_12

    move-result v3

    :goto_12
    if-ge v5, v3, :cond_29

    :try_start_21
    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_28

    invoke-virtual {v0, v5}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->getBroadcastItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v4, v6}, Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;->onDataStopped(II)V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_11

    :cond_28
    add-int/lit8 v5, v5, 0x1

    goto :goto_12

    :catchall_11
    move-exception v0

    :try_start_22
    throw v0

    :catchall_12
    move-exception v0

    monitor-exit v2
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_12

    throw v0

    :cond_29
    :try_start_23
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;->unregisterAllCallbacks()Z

    monitor-exit v2
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_12

    :cond_2a
    const-string v0, "isSHealthPlugin"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v0, "shealth_device_data_type"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string/jumbo v0, "shealth_device"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string/jumbo v3, "record"

    invoke-virtual {v8, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v0, :cond_2b

    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->isWearableDevice(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v4

    if-eqz v4, :cond_2b

    const-string v1, "[HealthSensor]SensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HANDLER_DATA_STOPED  DataManager stop() is called for WearableDevice : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    goto/16 :goto_0

    :cond_2b
    if-eqz v0, :cond_2d

    if-eqz v1, :cond_2d

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-nez v1, :cond_2c

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    goto/16 :goto_0

    :cond_2c
    if-eqz v3, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    goto/16 :goto_0

    :cond_2d
    if-eqz v0, :cond_0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->isStreamingData(I)Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$600(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    goto/16 :goto_0

    :cond_2e
    move v1, v7

    goto/16 :goto_d

    :cond_2f
    move-object v1, v4

    goto/16 :goto_c

    :cond_30
    move-object v3, v4

    goto/16 :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_7
        :pswitch_8
        :pswitch_c
        :pswitch_b
        :pswitch_9
        :pswitch_0
    .end packed-switch
.end method
