.class public Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeSpentDumper"
.end annotation


# instance fields
.field private mKey:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->mKey:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public dump()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->dump(Z)V

    return-void
.end method

.method public dump(Z)V
    .locals 2

    # getter for: Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->access$100()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpent;->dump()V

    if-eqz p1, :cond_0

    # getter for: Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->mLogTable:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->access$100()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
