.class public Lcom/sec/android/service/health/sensor/handler/ant/AntDataKey;
.super Ljava/lang/Object;


# static fields
.field public static final CAD_cumulativeRevolutions:Ljava/lang/String; = "ant_CADcumulativeRevolutions"

.field public static final CAD_timestampOfLastEvent:Ljava/lang/String; = "ant_CADtimestampOfLastEvent"

.field public static final CMD_REQSET_ADV:Ljava/lang/String; = "CMD_REQUEST_ADV"

.field public static final CMD_REQSET_BASIC:Ljava/lang/String; = "CMD_REQUEST_BASIC"

.field public static final CMD_REQSET_CAP:Ljava/lang/String; = "CMD_REQUEST_CAP"

.field public static final CMD_REQSET_HISTORY:Ljava/lang/String; = "CMD_REQUEST_HISTORY"

.field public static final CMD_REQ_CRANK:Ljava/lang/String; = "CMD_REQ_CRANK"

.field public static final CMD_REQ_CUSTOM:Ljava/lang/String; = "CMD_REQ_CUSTOM"

.field public static final CMD_REQ_MANUAL:Ljava/lang/String; = "CMD_REQ_MANUAL"

.field public static final CMD_SET_AUTO:Ljava/lang/String; = "CMD_SET_AUTO"

.field public static final CMD_SET_CRANK:Ljava/lang/String; = "CMD_SET_CRANK"

.field public static final CMD_SET_CTF:Ljava/lang/String; = "CMD_SET_CTF"

.field public static final CMD_SET_CUSTOM:Ljava/lang/String; = "CMD_SET_CUSTOM"

.field public static final CUMULATIVE_OPERATING_TIME:Ljava/lang/String; = "ant_cumulative_operating_time"

.field public static final DATA_LATENCY:Ljava/lang/String; = "ant_data_latency"

.field public static final HEART_BEAT_COUNTER:Ljava/lang/String; = "ant_heart_rate_counter"

.field public static final HISTORY_SUPPORT:Ljava/lang/String; = "ant_history_support"

.field public static final HR_DATA_SOURCE:Ljava/lang/String; = "ant_hr_data_source"

.field public static final LAP_COUNT:Ljava/lang/String; = "ant_lap_count"

.field public static final MAP3_SAMPLE_MEAN:Ljava/lang/String; = "ant_map3_sample_mean"

.field public static final MAP_EVENING:Ljava/lang/String; = "ant_map_evening"

.field public static final MAP_MORNING:Ljava/lang/String; = "ant_map_morning"

.field public static final PARAM_AUTO_ZERO:Ljava/lang/String; = "PARAM_AUTO_ZERO"

.field public static final PARAM_CRANK_AUTO:Ljava/lang/String; = "PARAM_CRANK_AUTO"

.field public static final PARAM_CRANK_LEN:Ljava/lang/String; = "PARAM_CRANK_LEN"

.field public static final PARAM_CTF_SLOPE:Ljava/lang/String; = "PARAM_CTF_SLOPE"

.field public static final PARAM_CUSTOM_CAL:Ljava/lang/String; = "PARAM_CUSTOM_CAL"

.field public static final RES_REQSET_ADV:Ljava/lang/String; = "RES_REQUEST_ADV"

.field public static final RES_REQSET_BASIC:Ljava/lang/String; = "RES_REQUEST_BASIC"

.field public static final RES_REQSET_CAP:Ljava/lang/String; = "RES_REQUEST_CAP"

.field public static final RES_REQSET_HISTORY:Ljava/lang/String; = "RES_REQUEST_HISTORY"

.field public static final RES_REQ_CRANK:Ljava/lang/String; = "RES_REQ_CRANK"

.field public static final RES_REQ_CUSTOM:Ljava/lang/String; = "RES_REQ_CUSTOM"

.field public static final RES_REQ_MANUAL:Ljava/lang/String; = "RES_REQ_MANUAL"

.field public static final RES_SET_AUTO:Ljava/lang/String; = "RES_SET_AUTO"

.field public static final RES_SET_CRANK:Ljava/lang/String; = "RES_SET_CRANK"

.field public static final RES_SET_CTF:Ljava/lang/String; = "RES_SET_CTF"

.field public static final RES_SET_CUSTOM:Ljava/lang/String; = "RES_SET_CUSTOM"

.field public static final SENSOR_HEALTH:Ljava/lang/String; = "ant_sensor_health"

.field public static final SENSOR_USESTATE:Ljava/lang/String; = "ant_sensor_usestate"

.field public static final SPD_cumulativeRevolutions:Ljava/lang/String; = "ant_SPDcumulativeRevolutions"

.field public static final SPD_timestampOfLastEvent:Ljava/lang/String; = "ant_SPDtimestampOfLastEvent"

.field public static final USERPROFILE_EXCHANGE_SUPPORT:Ljava/lang/String; = "ant_userprofile_exchange_support"

.field public static final USERPROFILE_ID_NUMBER:Ljava/lang/String; = "ant_userprofile_id_number"

.field public static final USERPROFILE_SELECTED:Ljava/lang/String; = "ant_userprofile_selected"

.field public static final accumulatedCrankPeriod:Ljava/lang/String; = "ant_accumulatedCrankPeriod"

.field public static final accumulatedCrankTicks:Ljava/lang/String; = "ant_accumulatedCrankTicks"

.field public static final accumulatedCrankTorque:Ljava/lang/String; = "ant_accumulatedCrankTorque"

.field public static final accumulatedPower:Ljava/lang/String; = "ant_accumulatedPower"

.field public static final accumulatedTimeStamp:Ljava/lang/String; = "ant_accumulatedTimeStamp"

.field public static final accumulatedTorqueTicksStamp:Ljava/lang/String; = "ant_accumulatedTorqueTicksStamp"

.field public static final accumulatedWheelPeriod:Ljava/lang/String; = "ant_accumulatedWheelPeriod"

.field public static final accumulatedWheelTicks:Ljava/lang/String; = "ant_accumulatedWheelTicks"

.field public static final accumulatedWheelTorque:Ljava/lang/String; = "ant_accumulatedWheelTorque"

.field public static final autoCrankLengthSupport:Ljava/lang/String; = "ant_autoCrankLengthSupport"

.field public static final autoZeroStatus:Ljava/lang/String; = "ant_autoZeroStatus"

.field public static final calculatedAccumulatedDistance:Ljava/lang/String; = "ant_calculatedAccumulatedDistance"

.field public static final calculatedCadence:Ljava/lang/String; = "ant_calculatedCadence"

.field public static final calculatedCrankCadence:Ljava/lang/String; = "ant_calculatedCrankCadence"

.field public static final calculatedPower:Ljava/lang/String; = "ant_calculatedPower"

.field public static final calculatedSpeed:Ljava/lang/String; = "ant_calculatedSpeed"

.field public static final calculatedTorque:Ljava/lang/String; = "ant_calculatedTorque"

.field public static final calculatedWheelDistance:Ljava/lang/String; = "ant_calculatedWheelDistance"

.field public static final calculatedWheelSpeed:Ljava/lang/String; = "ant_calculatedWheelSpeed"

.field public static final calibrationData:Ljava/lang/String; = "ant_calibrationData"

.field public static final calibrationId:Ljava/lang/String; = "ant_calibrationId"

.field public static final crankLengthStatus:Ljava/lang/String; = "ant_crankLengthStatus"

.field public static final crankTorqueUpdateEventCount:Ljava/lang/String; = "ant_crankTorqueUpdateEventCount"

.field public static final ctfOffset:Ljava/lang/String; = "ant_ctfOffset"

.field public static final ctfUpdateEventCount:Ljava/lang/String; = "ant_ctfUpdateEventCount"

.field public static final customCalibrationStatus:Ljava/lang/String; = "ant_customCalibrationStatus"

.field public static final dataSource:Ljava/lang/String; = "ant_dataSource"

.field public static final dataType:Ljava/lang/String; = "ant_dataType"

.field public static final fullCrankLength:Ljava/lang/String; = "ant_fullCrankLength"

.field public static final instantaneousCadence:Ljava/lang/String; = "ant_instantaneousCadence"

.field public static final instantaneousPower:Ljava/lang/String; = "ant_instantaneousPower"

.field public static final instantaneousSlope:Ljava/lang/String; = "ant_instantaneousSlope"

.field public static final leftOrCombinedPedalSmoothness:Ljava/lang/String; = "ant_leftOrCombinedPedalSmoothness"

.field public static final leftTorqueEffectiveness:Ljava/lang/String; = "ant_leftTorqueEffectiveness"

.field public static final manufacturerSpecificData:Ljava/lang/String; = "ant_manufacturerSpecificData"

.field public static final measurementValue:Ljava/lang/String; = "ant_measurementValue"

.field public static final numOfDataTypes:Ljava/lang/String; = "ant_numOfDataTypes"

.field public static final pedalPowerPercentage:Ljava/lang/String; = "ant_pedalPowerPercentage"

.field public static final powerOnlyUpdateEventCount:Ljava/lang/String; = "ant_powerOnlyUpdateEventCount"

.field public static final rightPedalIndicator:Ljava/lang/String; = "ant_rightPedalIndicator"

.field public static final rightPedalSmoothness:Ljava/lang/String; = "ant_rightPedalSmoothness"

.field public static final rightTorqueEffectiveness:Ljava/lang/String; = "ant_rightTorqueEffectiveness"

.field public static final sensorAvailabilityStatus:Ljava/lang/String; = "ant_sensorAvailabilityStatus"

.field public static final sensorSoftwareMismatchStatus:Ljava/lang/String; = "ant_sensorSoftwareMismatchStatus"

.field public static final separatePedalSmoothnessSupport:Ljava/lang/String; = "ant_separatePedalSmoothnessSupport"

.field public static final timeStamp:Ljava/lang/String; = "ant_timeStamp"

.field public static final wheelTorqueUpdateEventCount:Ljava/lang/String; = "ant_wheelTorqueUpdateEventCount"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
