.class public Lcom/sec/android/service/health/cp/serversync/data/BioItem;
.super Ljava/lang/Object;


# instance fields
.field public accId:Ljava/lang/String;

.field public activityMetabolicRate:Ljava/lang/Double;

.field public appId:Ljava/lang/String;

.field public bioCode:Ljava/lang/String;

.field public bloodSugar:Ljava/lang/Float;

.field public bloodSugarUnit:Ljava/lang/String;

.field public bmi:Ljava/lang/Double;

.field public bmr:Ljava/lang/Double;

.field public bodyAge:Ljava/lang/Integer;

.field public bodyFat:Ljava/lang/Double;

.field public bodyFatUnit:Ljava/lang/String;

.field public bodyTemperature:Ljava/lang/Float;

.field public bodyTemperatureType:Ljava/lang/String;

.field public bodyTemperatureUnit:Ljava/lang/String;

.field public bodyWater:Ljava/lang/Double;

.field public bodyWaterUnit:Ljava/lang/String;

.field public boneMass:Ljava/lang/Double;

.field public boneMassUnit:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceBioPKId:Ljava/lang/Long;

.field public deviceCreateDate:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyDate:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public diastolic:Ljava/lang/Integer;

.field public hba1c:Ljava/lang/Float;

.field public hdid:Ljava/lang/String;

.field public height:Ljava/lang/Double;

.field public heightUnit:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public mealTime:Ljava/lang/String;

.field public mealTimeType:Ljava/lang/String;

.field public mean:Ljava/lang/Float;

.field public muscleMass:Ljava/lang/Double;

.field public muscleMassUnit:Ljava/lang/String;

.field public pulse:Ljava/lang/Integer;

.field public requestType:Ljava/lang/String;

.field public responseCode:Ljava/lang/String;

.field public samplePositionType:Ljava/lang/Integer;

.field public sampleTime:Ljava/lang/String;

.field public sampleType:Ljava/lang/String;

.field public skeletalMuscle:Ljava/lang/Double;

.field public skeletalMuscleUnit:Ljava/lang/String;

.field public systolic:Ljava/lang/Integer;

.field public visceralFat:Ljava/lang/Double;

.field public visceralFatUnit:Ljava/lang/String;

.field public weight:Ljava/lang/Double;

.field public weightUnit:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
