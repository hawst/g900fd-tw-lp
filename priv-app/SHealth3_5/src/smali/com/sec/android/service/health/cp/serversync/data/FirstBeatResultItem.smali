.class public Lcom/sec/android/service/health/cp/serversync/data/FirstBeatResultItem;
.super Ljava/lang/Object;


# instance fields
.field public appIdOfLog:Ljava/lang/String;

.field public deviceCreateTime:Ljava/lang/String;

.field public deviceCreateTimeOfLog:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceFirstBeatPKId:J

.field public deviceLogPKId:J

.field public deviceModifyTime:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public distance:Ljava/lang/Double;

.field public endTime:Ljava/lang/String;

.field public hdid:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public maximalMet:Ljava/lang/Double;

.field public resourceRecovery:Ljava/lang/Integer;

.field public trainingLoadPeak:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
