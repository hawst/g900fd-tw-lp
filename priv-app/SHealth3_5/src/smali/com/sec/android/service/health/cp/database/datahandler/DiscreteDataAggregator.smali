.class public abstract Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field lastCreateTime:J

.field protected mAppId:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field protected mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->lastCreateTime:J

    return-void
.end method

.method private insertBulkData([Landroid/content/ContentValues;)V
    .locals 5

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->getTableUri()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    int-to-long v0, v0

    const-string v2, "DiscreteAggregator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Bulk Data insert count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private insertData()V
    .locals 5

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->getContentValues()Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->getTableUri()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Inserting values: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  into table:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "application__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v2, "validation_policy"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->getPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->asMask()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->checkMandatoryFields()Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->insertData()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while inserting to db"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 8

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    array-length v1, p1

    new-array v4, v1, [Landroid/content/ContentValues;

    array-length v5, p1

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v3, p1, v1

    invoke-virtual {p0, v3}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->getContentValues()Landroid/content/ContentValues;

    move-result-object v6

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mAppId:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, "application__id"

    iget-object v7, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mAppId:Ljava/lang/String;

    invoke-virtual {v6, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v3, v2, 0x1

    aput-object v6, v4, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-direct {p0, v4}, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->insertBulkData([Landroid/content/ContentValues;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :try_start_2
    monitor-exit p0

    :goto_1
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while inserting to db"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v1

    :try_start_3
    sget-object v1, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method protected abstract checkMandatoryFields()Z
.end method

.method public finalizeAggregation()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Nothing to be done in finalization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method abstract getContentValues()Landroid/content/ContentValues;
.end method

.method abstract getTableUri()Landroid/net/Uri;
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mUserDeviceId:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->mAppId:Ljava/lang/String;

    return-void
.end method

.method public isRunning(J)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/DiscreteDataAggregator;->TAG:Ljava/lang/String;

    const-string v1, "Don\'t have streaming Data"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method abstract processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
.end method
