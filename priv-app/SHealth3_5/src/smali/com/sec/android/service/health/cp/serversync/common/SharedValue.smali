.class public Lcom/sec/android/service/health/cp/serversync/common/SharedValue;
.super Ljava/lang/Object;


# static fields
.field public static final CLIENT_ID:Ljava/lang/String; = "1y90e30264"

.field public static final CLIENT_SECRET:Ljava/lang/String; = "80E7ECD9D301CB7888C73703639302E5"

.field private static final TAG:Ljava/lang/String; = "SharedValue"

.field private static mContext:Landroid/content/Context;

.field private static mInstance:Lcom/sec/android/service/health/cp/serversync/common/SharedValue;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mDeviceId:Ljava/lang/String;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mInstance:Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mPrefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;
    .locals 2

    const-class v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mInstance:Lcom/sec/android/service/health/cp/serversync/common/SharedValue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getSyncPrefs()Landroid/content/SharedPreferences;
    .locals 3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    const-string v0, "SharedValue"

    const-string v1, "Prefrences value is null, so get it again."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sync_preference"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mPrefs:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public initialization(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    sput-object p1, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sync_preference"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mDeviceId:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setAccessToken(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->mAccessToken:Ljava/lang/String;

    return-void
.end method
