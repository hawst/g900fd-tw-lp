.class public interface abstract Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand$Id;
    }
.end annotation


# static fields
.field public static final ACK:[B

.field public static final CM1:I = 0x3

.field public static final CM2:I = 0x4

.field public static final CM3:I = 0x5

.field public static final DELETE_ALL_RECORDS:[B

.field public static final DISCONNECT:[B

.field public static final DISCONNECT_ACK:[B

.field public static final ETX_VALUE:B = 0x3t

.field public static final LEN:I = 0x1

.field public static final LENGTH_ACK:B = 0x6t

.field public static final LENGTH_DELETE_ALL:B = 0x8t

.field public static final LENGTH_DELETE_ALL_RECORDS:B = 0x8t

.field public static final LENGTH_DISCONNECT:B = 0x6t

.field public static final LENGTH_READ_DATE_FORMAT:B = 0xet

.field public static final LENGTH_READ_GLUCOSE_RECORD:B = 0xat

.field public static final LENGTH_READ_GLUCOSE_RECORD_NUMBER:B = 0xat

.field public static final LENGTH_READ_METER_INFORMATION:B = 0x9t

.field public static final LENGTH_READ_RTC:B = 0xdt

.field public static final LENGTH_READ_SERIAL_NUMBER:B = 0x12t

.field public static final LENGTH_READ_UNIT_SETTING:B = 0xet

.field public static final LENGTH_WRITE_RTC:B = 0xdt

.field public static final LINK:I = 0x2

.field public static final READ_DATE_FORMAT:[B

.field public static final READ_GLUCOSE_RECORD:[B

.field public static final READ_GLUCOSE_RECORD_NUMBER:[B

.field public static final READ_METER_INFORMATION:[B

.field public static final READ_RTC:[B

.field public static final READ_SERIAL_NUMBER:[B

.field public static final READ_UNIT_SETTING:[B

.field public static final REPLY_DATE_FORMAT:[B

.field public static final REPLY_DELETE_ALL_RECORDS:[B

.field public static final REPLY_GLUCOSE_RECORD:[B

.field public static final REPLY_GLUCOSE_RECORD_NUMBER:[B

.field public static final REPLY_METER_INFORMATION:[B

.field public static final REPLY_READ_RTC:[B

.field public static final REPLY_SERIAL_NUMBER:[B

.field public static final REPLY_UNIT_SETTING:[B

.field public static final REPLY_WRITE_RTC:[B

.field public static final STX:I = 0x0

.field public static final STX_VALUE:B = 0x2t

.field public static final TAG:Ljava/lang/String; = "[HealthSensor]JNJ"

.field public static final WRITE_RTC:[B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    new-array v0, v4, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->DISCONNECT:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_METER_INFORMATION:[B

    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_SERIAL_NUMBER:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->DELETE_ALL_RECORDS:[B

    new-array v0, v3, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_GLUCOSE_RECORD_NUMBER:[B

    new-array v0, v3, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_GLUCOSE_RECORD:[B

    new-array v0, v5, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_UNIT_SETTING:[B

    new-array v0, v5, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_DATE_FORMAT:[B

    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_8

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->READ_RTC:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_9

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->WRITE_RTC:[B

    new-array v0, v3, [B

    fill-array-data v0, :array_a

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->ACK:[B

    new-array v0, v4, [B

    fill-array-data v0, :array_b

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->DISCONNECT_ACK:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_c

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_METER_INFORMATION:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_d

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_SERIAL_NUMBER:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_e

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_DELETE_ALL_RECORDS:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_f

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_GLUCOSE_RECORD_NUMBER:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_10

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_GLUCOSE_RECORD:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_11

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_UNIT_SETTING:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_12

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_DATE_FORMAT:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_13

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_READ_RTC:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_14

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->REPLY_WRITE_RTC:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x2t
        0x6t
        0x8t
        0x3t
        -0x3et
        0x62t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x5t
        0xdt
        0x2t
    .end array-data

    :array_2
    .array-data 1
        0x5t
        0xbt
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x7ct
        0x6at
        -0x18t
        0x73t
        0x0t
    .end array-data

    :array_3
    .array-data 1
        0x5t
        0x1at
    .end array-data

    nop

    :array_4
    .array-data 1
        0x5t
        0x1ft
        -0xbt
        0x1t
    .end array-data

    :array_5
    .array-data 1
        0x5t
        0x1ft
        0x0t
        0x0t
    .end array-data

    :array_6
    .array-data 1
        0x5t
        0x9t
        0x2t
        0x9t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_7
    .array-data 1
        0x5t
        0x8t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_8
    .array-data 1
        0x5t
        0x20t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_9
    .array-data 1
        0x5t
        0x20t
        0x1t
    .end array-data

    :array_a
    .array-data 1
        0x2t
        0x6t
        0x7t
        0x3t
    .end array-data

    :array_b
    .array-data 1
        0x2t
        0x6t
        0xct
        0x3t
        0x6t
        -0x52t
    .end array-data

    nop

    :array_c
    .array-data 1
        0x5t
        0x6t
        0x11t
    .end array-data

    :array_d
    .array-data 1
        0x5t
        0x6t
    .end array-data

    nop

    :array_e
    .array-data 1
        0x5t
        0x6t
    .end array-data

    nop

    :array_f
    .array-data 1
        0x5t
        0xft
    .end array-data

    nop

    :array_10
    .array-data 1
        0x5t
        0x6t
    .end array-data

    nop

    :array_11
    .array-data 1
        0x5t
        0x6t
    .end array-data

    nop

    :array_12
    .array-data 1
        0x5t
        0x6t
    .end array-data

    nop

    :array_13
    .array-data 1
        0x5t
        0x6t
    .end array-data

    nop

    :array_14
    .array-data 1
        0x5t
        0x6t
    .end array-data
.end method


# virtual methods
.method public abstract doAction()V
.end method
