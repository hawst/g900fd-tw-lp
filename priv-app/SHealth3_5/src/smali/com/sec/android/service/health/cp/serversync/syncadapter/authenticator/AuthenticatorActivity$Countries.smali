.class public final enum Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Countries"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Bahrain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Brazil:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Canada:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum China:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum France:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Germany:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum GreatBritain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum HongKong:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Iran:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Italy:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Japan:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum KoreaKR:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum KoreaWW:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Laos:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Other:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Peru:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Spain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Taiwan:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum Tunisia:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum USA:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

.field public static final enum UnitedKingdom:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;


# instance fields
.field private countryCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "USA"

    const-string v2, "US"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->USA:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "GreatBritain"

    const-string v2, "GB"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->GreatBritain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "UnitedKingdom"

    const-string v2, "UK"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->UnitedKingdom:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "China"

    const-string v2, "CN"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->China:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Japan"

    const-string v2, "JP"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Japan:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Canada"

    const/4 v2, 0x5

    const-string v3, "CA"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Canada:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Brazil"

    const/4 v2, 0x6

    const-string v3, "BR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Brazil:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "KoreaWW"

    const/4 v2, 0x7

    const-string v3, "WW"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->KoreaWW:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "KoreaKR"

    const/16 v2, 0x8

    const-string v3, "KR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->KoreaKR:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "France"

    const/16 v2, 0x9

    const-string v3, "FR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->France:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Italy"

    const/16 v2, 0xa

    const-string v3, "IT"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Italy:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Germany"

    const/16 v2, 0xb

    const-string v3, "DE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Germany:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Spain"

    const/16 v2, 0xc

    const-string v3, "ES"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Spain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "HongKong"

    const/16 v2, 0xd

    const-string v3, "HK"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->HongKong:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Taiwan"

    const/16 v2, 0xe

    const-string v3, "TW"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Taiwan:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Laos"

    const/16 v2, 0xf

    const-string v3, "LA"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Laos:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Tunisia"

    const/16 v2, 0x10

    const-string v3, "TN"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Tunisia:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Bahrain"

    const/16 v2, 0x11

    const-string v3, "BH"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Bahrain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Iran"

    const/16 v2, 0x12

    const-string v3, "IR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Iran:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Peru"

    const/16 v2, 0x13

    const-string v3, "PE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Peru:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    new-instance v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const-string v1, "Other"

    const/16 v2, 0x14

    const-string v3, "OTHER"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Other:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->USA:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->GreatBritain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->UnitedKingdom:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->China:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Japan:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Canada:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Brazil:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->KoreaWW:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->KoreaKR:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->France:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Italy:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Germany:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Spain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->HongKong:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Taiwan:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Laos:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Tunisia:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Bahrain:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Iran:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Peru:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->Other:Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->$VALUES:[Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->countryCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->countryCode:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->$VALUES:[Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/AuthenticatorActivity$Countries;->countryCode:Ljava/lang/String;

    return-object v0
.end method
