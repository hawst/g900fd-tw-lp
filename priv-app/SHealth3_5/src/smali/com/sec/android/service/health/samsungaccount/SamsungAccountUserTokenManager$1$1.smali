.class Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "SamsungAccountUserTokenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 5
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 150
    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ServiceConnection onServiceConnected onReceiveAccessToken "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    if-eqz p2, :cond_3

    .line 153
    if-eqz p3, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    const-string v2, "access_token"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungToken:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$802(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;

    .line 156
    const-string v1, "login_id"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mLoginId:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    const-string/jumbo v2, "user_id"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mUserId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$902(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    const-string v2, "birthday"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mBirthday:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1002(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;

    .line 161
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    const-string/jumbo v2, "mcc"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mMCC:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1102(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Ljava/lang/String;)Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mMCC:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1100(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->getPlatformServerURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 167
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1300(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    move-result-object v1

    sget-object v3, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->EXPIRED_REQUEST_OFFERED_WHILE_NORMAL_REQUEST_ONGOING:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    if-ne v1, v3, :cond_1

    .line 169
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    sget-object v3, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v1, v3}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 170
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 171
    monitor-exit v2

    .line 205
    :goto_0
    return-void

    .line 176
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 178
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .line 179
    .local v0, "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    if-eqz v0, :cond_1

    .line 181
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungToken:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$800(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v3, v3, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mUserId:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$900(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v4, v4, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mMCC:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1100(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v3, v4}, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;->onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 186
    .end local v0    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 184
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    sget-object v3, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v1, v3}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 186
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    :goto_2
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # invokes: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->unbindSAService()V
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1500(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)V

    goto :goto_0

    .line 190
    :cond_3
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->REQUEST_STATE_LOCK:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1200(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 192
    :cond_4
    :goto_3
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-eqz v1, :cond_5

    if-eqz p3, :cond_5

    .line 194
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    # getter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mSamsungAccountTokenListenerQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1400(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;

    .line 195
    .restart local v0    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    if-eqz v0, :cond_4

    .line 197
    const-string v1, "error_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;->setFailureMessage(Ljava/lang/String;)V

    goto :goto_3

    .line 201
    .end local v0    # "listener":Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 200
    :cond_5
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1$1;->this$1:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;

    iget-object v1, v1, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$1;->this$0:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    sget-object v3, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;->IDLE:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    # setter for: Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mRequestState:Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;
    invoke-static {v1, v3}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->access$1302(Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;)Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager$RequestState;

    .line 201
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2
.end method

.method public onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 139
    return-void
.end method

.method public onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 133
    return-void
.end method

.method public onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 127
    return-void
.end method

.method public onReceivePasswordConfirmation(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 145
    return-void
.end method

.method public onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 121
    return-void
.end method
