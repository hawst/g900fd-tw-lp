.class Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeCadencePcc$IRawCadenceDataReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$2;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewRawCadenceData(JLjava/util/EnumSet;Ljava/math/BigDecimal;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;",
            "Ljava/math/BigDecimal;",
            "J)V"
        }
    .end annotation

    const-string v0, "[HealthSensor]BSACSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewRawCadenceData - timestampOfLastEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cumulativeRevolutions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->time:J

    iput-wide p5, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;->cadence:J

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ant_CADtimestampOfLastEvent"

    invoke-virtual {p4}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "ant_CADcumulativeRevolutions"

    invoke-virtual {v1, v2, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$2;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->notifyResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    return-void
.end method
