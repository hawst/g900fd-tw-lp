.class public Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;
.super Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList",
        "<TT;>;"
    }
.end annotation


# instance fields
.field device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field mHealthRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener",
            "<TT;>;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->mHealthRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-void
.end method
