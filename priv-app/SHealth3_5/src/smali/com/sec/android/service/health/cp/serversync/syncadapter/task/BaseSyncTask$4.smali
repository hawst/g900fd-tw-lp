.class Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->handleGetDetailHistory(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

.field final synthetic val$response:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->val$response:Ljava/lang/Object;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DB-Insert"

    const-string/jumbo v2, "update item to local STARTED"

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->uri:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->start(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v2, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->uri:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->val$response:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->updateItemsLocally(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SyncConfig;->isProfilingEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$000(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "DB-Insert"

    const-string/jumbo v3, "update item to local DONE"

    invoke-static {v0, v2, v3}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;

    iget-object v0, v0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$ServerApi;->uri:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger;->end(Ljava/lang/String;)Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/util/ProfilingUtils$TimeLogger$TimeSpentDumper;->dump()V

    :cond_1
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :cond_2
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mGetDetailHistoryRequestor:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$200(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->getDetailHistory()V

    :goto_1
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/google/gson/JsonSyntaxException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V

    goto :goto_1

    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # getter for: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->mDownloadImageUrl:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$300(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    # invokes: Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->downloadImages()V
    invoke-static {v0}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->access$400(Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask$4;->this$0:Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/syncadapter/task/BaseSyncTask;->finishTask(I)V
    :try_end_3
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1
.end method
