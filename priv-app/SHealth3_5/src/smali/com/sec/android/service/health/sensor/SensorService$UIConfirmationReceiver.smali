.class Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/SensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UIConfirmationReceiver"
.end annotation


# instance fields
.field private checked:Z

.field private mealTime:Z

.field final synthetic this$0:Lcom/sec/android/service/health/sensor/SensorService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/SensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16

    const-string v1, "contentui"

    const-string v2, "[UIConfirmationReceiver] onReceive - sensorService"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.service.health.sensor.contentui"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "contentui"

    const-string v2, "[UIConfirmationReceiver] onReceive - sensorService - confirm "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "status"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "streaming"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "requestTime"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "checked"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getBooleanArrayExtra(Ljava/lang/String;)[Z

    move-result-object v13

    const/4 v11, 0x1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v5, "uniqueId"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/4 v10, 0x0

    if-eqz v12, :cond_1f

    const/4 v4, 0x0

    aget-boolean v4, v12, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1f

    if-nez v1, :cond_1e

    if-eqz v13, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Ljava/util/ArrayList;

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/sec/android/service/health/contentservice/SensorData;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    if-eqz v1, :cond_6

    const/16 v4, 0x2714

    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v10, Lcom/sec/android/service/health/contentservice/SensorData;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v6, 0x0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1800()I

    move-result v8

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    const/4 v1, 0x0

    move v3, v1

    move-object v1, v10

    :goto_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_24

    aget-boolean v2, v13, v3

    if-eqz v2, :cond_f

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/contentservice/SensorData;

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    if-eqz v2, :cond_9

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v6, "meal"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->mealTime:Z

    :cond_1
    :goto_2
    const/4 v2, 0x0

    aget-boolean v2, v12, v2

    if-eqz v2, :cond_d

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    if-eqz v2, :cond_b

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->mealTime:Z

    if-eqz v6, :cond_a

    const v6, 0x13881

    :goto_3
    iput v6, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->mealType:I

    :cond_2
    :goto_4
    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v2

    iget-object v6, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iget-object v7, v1, Lcom/sec/android/service/health/contentservice/SensorData;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    invoke-virtual {v2, v6, v7, v8}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x0

    :goto_5
    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v3

    iget-object v1, v1, Lcom/sec/android/service/health/contentservice/SensorData;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v4, v5}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v11, v2

    :cond_3
    :goto_6
    if-eqz v10, :cond_4

    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v1, :cond_4

    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v2

    const/4 v1, 0x1

    if-ne v11, v1, :cond_1c

    const/4 v1, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v3

    const/16 v4, 0x6e

    iget-object v5, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v5

    invoke-virtual {v3, v4, v5, v1, v2}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    if-nez v1, :cond_1d

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :goto_8
    const-string v3, "_Health"

    iget-object v4, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v3, "isSHealthPlugin"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v3, "shealth_device_data_type"

    iget-object v4, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "shealth_device_ID"

    iget-object v4, v10, Lcom/sec/android/service/health/contentservice/SensorData;->deviceID:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_4
    :goto_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    :goto_a
    return-void

    :cond_6
    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    if-eqz v1, :cond_7

    const/16 v4, 0x2716

    goto/16 :goto_0

    :cond_7
    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    if-eqz v1, :cond_8

    const/16 v4, 0x2712

    goto/16 :goto_0

    :cond_8
    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    if-eqz v1, :cond_0

    const/16 v4, 0x2713

    goto/16 :goto_0

    :cond_9
    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    if-eqz v2, :cond_1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v6, "checked"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->checked:Z

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v6, "bodytemperaturetext"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_a
    const v6, 0x13882

    goto/16 :goto_3

    :cond_b
    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->checked:Z

    if-eqz v6, :cond_c

    const v6, 0x11171

    :goto_b
    iput v6, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureType:I

    goto/16 :goto_4

    :cond_c
    const v6, 0x11173

    goto :goto_b

    :cond_d
    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v2, :cond_f

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v6

    const/16 v7, 0x6c

    iget-object v8, v10, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v8}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v8

    const/4 v15, 0x0

    invoke-virtual {v6, v7, v8, v15, v2}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "_Health"

    iget-object v8, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v7, v1, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    if-eqz v7, :cond_e

    const-string v7, "_Extra"

    iget-object v8, v1, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_e
    const-string v7, "isSHealthPlugin"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v7, "shealth_device_data_type"

    iget-object v8, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v8}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v6}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_f
    move-object v2, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v2

    goto/16 :goto_1

    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_23

    const/4 v4, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/sec/android/service/health/contentservice/SensorData;

    const-string v1, "[HealthSensor]SensorService"

    const-string v2, "[UIConfirmationReceiver] onReceive   !streaming"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "[HealthSensor]SensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[UIConfirmationReceiver] onReceive   data==null?="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v9, :cond_15

    const/4 v1, 0x1

    :goto_c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v9, :cond_5

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    if-eqz v1, :cond_16

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "meal"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->mealTime:Z

    const/16 v4, 0x2714

    :cond_11
    :goto_d
    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    if-eqz v1, :cond_1a

    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->mealTime:Z

    if-eqz v2, :cond_19

    const v2, 0x13881

    :goto_e
    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->mealType:I

    :cond_12
    :goto_f
    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$300()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v9, Lcom/sec/android/service/health/contentservice/SensorData;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v6, 0x0

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getProcessId()I
    invoke-static {}, Lcom/sec/android/service/health/sensor/SensorService;->access$1800()I

    move-result v8

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iget-object v3, v9, Lcom/sec/android/service/health/contentservice/SensorData;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v9, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3, v6}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v11

    invoke-static {}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->getInstance()Lcom/sec/android/service/health/cp/database/datahandler/DataManager;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/android/service/health/contentservice/SensorData;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$1600(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/android/service/health/cp/database/datahandler/DataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v1, :cond_14

    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v2

    const/16 v3, 0x6c

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "_Health"

    iget-object v4, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v3, v9, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    if-eqz v3, :cond_13

    const-string v3, "_Extra"

    iget-object v4, v9, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_13
    const-string v3, "isSHealthPlugin"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v3, "shealth_device_data_type"

    iget-object v4, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_14
    :goto_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v10, v9

    goto/16 :goto_6

    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_c

    :cond_16
    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    if-eqz v1, :cond_17

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "checked"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->checked:Z

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "bodytemperaturetext"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    const/16 v4, 0x2716

    goto/16 :goto_d

    :cond_17
    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    if-eqz v1, :cond_18

    const/16 v4, 0x2712

    goto/16 :goto_d

    :cond_18
    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    if-eqz v1, :cond_11

    const/16 v4, 0x2713

    goto/16 :goto_d

    :cond_19
    const v2, 0x13882

    goto/16 :goto_e

    :cond_1a
    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    instance-of v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    if-eqz v1, :cond_12

    iget-object v1, v9, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->checked:Z

    if-eqz v2, :cond_1b

    const v2, 0x11171

    :goto_11
    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureType:I

    goto/16 :goto_f

    :cond_1b
    const v2, 0x11173

    goto :goto_11

    :cond_1c
    const/4 v1, 0x1

    goto/16 :goto_7

    :cond_1d
    iget-object v1, v10, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    goto/16 :goto_8

    :cond_1e
    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mBinder:Lcom/sec/android/service/health/sensor/SensorService$MyBinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$1900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$MyBinder;

    move-result-object v1

    const/4 v4, 0x0

    aget-boolean v4, v12, v4

    invoke-virtual {v1, v4, v2, v3}, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->updateConfirmation(ZJ)V

    goto/16 :goto_9

    :cond_1f
    if-eqz v1, :cond_21

    if-eqz v12, :cond_20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mBinder:Lcom/sec/android/service/health/sensor/SensorService$MyBinder;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$1900(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$MyBinder;

    move-result-object v1

    const/4 v4, 0x0

    aget-boolean v4, v12, v4

    invoke-virtual {v1, v4, v2, v3}, Lcom/sec/android/service/health/sensor/SensorService$MyBinder;->updateConfirmation(ZJ)V

    :cond_20
    :goto_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    :cond_21
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/SensorService;->mSensorDataMap:Ljava/util/HashMap;

    invoke-virtual {v1, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_20

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/contentservice/SensorData;

    if-eqz v1, :cond_20

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    if-eqz v2, :cond_20

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->appCallbackList:Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    # invokes: Lcom/sec/android/service/health/sensor/SensorService;->createRemoteCallbackListCopy(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/SensorService;->access$000(Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;)Lcom/sec/android/service/health/sensor/manager/RemoteCallbackList;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v3

    const/16 v4, 0x6e

    iget-object v5, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v5

    const/4 v6, 0x6

    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    if-nez v2, :cond_22

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    :goto_13
    const-string v4, "_Health"

    iget-object v5, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v4, "isSHealthPlugin"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v4, "shealth_device_data_type"

    iget-object v5, v1, Lcom/sec/android/service/health/contentservice/SensorData;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v4, "shealth_device_ID"

    iget-object v1, v1, Lcom/sec/android/service/health/contentservice/SensorData;->deviceID:Ljava/lang/String;

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/SensorService$UIConfirmationReceiver;->this$0:Lcom/sec/android/service/health/sensor/SensorService;

    # getter for: Lcom/sec/android/service/health/sensor/SensorService;->mCbHandler:Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/SensorService;->access$100(Lcom/sec/android/service/health/sensor/SensorService;)Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/service/health/sensor/SensorService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_12

    :cond_22
    iget-object v2, v1, Lcom/sec/android/service/health/contentservice/SensorData;->extra:Landroid/os/Bundle;

    goto :goto_13

    :cond_23
    move-object v9, v10

    goto/16 :goto_10

    :cond_24
    move v2, v11

    goto/16 :goto_5
.end method
