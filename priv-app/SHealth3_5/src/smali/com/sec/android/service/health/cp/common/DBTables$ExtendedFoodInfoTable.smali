.class public Lcom/sec/android/service/health/cp/common/DBTables$ExtendedFoodInfoTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExtendedFoodInfoTable"
.end annotation


# static fields
.field public static final KEY_CALCIUM:Ljava/lang/String; = "calcium"

.field public static final KEY_CARBOHYDRATE:Ljava/lang/String; = "carbohydrate"

.field public static final KEY_CHOLESTROL:Ljava/lang/String; = "cholesterol"

.field public static final KEY_DEFAULT_NUMBER:Ljava/lang/String; = "default_number"

.field public static final KEY_DIETARY:Ljava/lang/String; = "dietary"

.field public static final KEY_FAT:Ljava/lang/String; = "fat"

.field public static final KEY_FOOD_INFO_ID:Ljava/lang/String; = "food_info_id"

.field public static final KEY_GRAMM_IN_KCAL:Ljava/lang/String; = "gramm_in_kcal"

.field public static final KEY_IRON:Ljava/lang/String; = "iron"

.field public static final KEY_MONOSATURATED:Ljava/lang/String; = "monosaturated"

.field public static final KEY_OZ_IN_KCAL:Ljava/lang/String; = "oz_in_kcal"

.field public static final KEY_POLYSATURATED:Ljava/lang/String; = "polysaturated"

.field public static final KEY_POTASSIUM:Ljava/lang/String; = "potassium"

.field public static final KEY_PROTEIN:Ljava/lang/String; = "protein"

.field public static final KEY_SATURATED:Ljava/lang/String; = "saturated"

.field public static final KEY_SODIUM:Ljava/lang/String; = "sodium"

.field public static final KEY_SUGARY:Ljava/lang/String; = "sugary"

.field public static final KEY_UNIT:Ljava/lang/String; = "unit"

.field public static final KEY_UNIT_NAME:Ljava/lang/String; = "unit_name"

.field public static final KEY_VITAMIN_A:Ljava/lang/String; = "vitamin_a"

.field public static final KEY_VITAMIN_C:Ljava/lang/String; = "vitamin_c"

.field public static final TABLE_NAME:Ljava/lang/String; = "extend_food_info"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
