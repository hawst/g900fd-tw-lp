.class public abstract Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;
.super Ljava/lang/Object;


# instance fields
.field private mPolicy:Lcom/sec/android/service/health/cp/database/ValidationPolicy;


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;->mPolicy:Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    return-void
.end method


# virtual methods
.method public abstract addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
.end method

.method public abstract addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
.end method

.method public abstract finalizeAggregation()V
.end method

.method protected getPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;->mPolicy:Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    return-object v0
.end method

.method public abstract initialize(Landroid/content/Context;Ljava/lang/String;JI)V
.end method

.method public abstract isRunning(J)Z
.end method
