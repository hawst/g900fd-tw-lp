.class public abstract Lcom/sec/android/service/health/sensor/handler/DummyHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;


# static fields
.field protected static final TAG:Ljava/lang/String; = "[HealthSensor][HEALTH][DummyHandler]"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract deinitialize()V
.end method

.method public abstract handleRequest(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
.end method

.method public abstract startStream(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;I)Z
.end method

.method public abstract stopStream()V
.end method
