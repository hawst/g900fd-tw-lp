.class public final enum Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SHealthUpgradeState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_1_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_1_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_1_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_2_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_2_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_2_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_3_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_4_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_4_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_4_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_4_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

.field public static final enum UPGRADE_STATE_NONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_NONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_1"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_1_1"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_1_2"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_1_3"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_2"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_2_1"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_2_2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_2_3"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_3"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_3_1"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_3_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_4"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_4_1"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_4_2"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_4_3"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_4_4"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    new-instance v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const-string v1, "UPGRADE_STATE_APP_UPGRADE_DONE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_NONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_1_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_2_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_3_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_1:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_2:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_3:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_4_4:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->$VALUES:[Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    .locals 1

    const-class v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->$VALUES:[Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    invoke-virtual {v0}, [Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    return-object v0
.end method
