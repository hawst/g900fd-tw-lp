.class Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;
.super Ljava/io/FilterOutputStream;
.source "UploadOutputStream.java"


# instance fields
.field private PauseSyncObject:Ljava/lang/Object;

.field private TAG:Ljava/lang/String;

.field private isPause:Z

.field private listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

.field private transferred:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "listener"    # Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 21
    const-class v0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->TAG:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->isPause:Z

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->PauseSyncObject:Ljava/lang/Object;

    .line 35
    iput-object p2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    .line 36
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->transferred:J

    .line 37
    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 5
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->PauseSyncObject:Ljava/lang/Object;

    monitor-enter v2

    .line 79
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->isPause:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 83
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->PauseSyncObject:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 87
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 91
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write(I)V

    .line 92
    iget-wide v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->transferred:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->transferred:J

    .line 93
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    iget-wide v2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->transferred:J

    invoke-interface {v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;->transferred(J)V

    .line 94
    return-void
.end method

.method public write([BII)V
    .locals 5
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->PauseSyncObject:Ljava/lang/Object;

    monitor-enter v2

    .line 51
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->isPause:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 55
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->PauseSyncObject:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 62
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 64
    iget-wide v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->transferred:J

    int-to-long v3, p3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->transferred:J

    .line 65
    iget-object v1, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->listener:Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;

    iget-wide v2, p0, Lcom/sec/android/service/health/connectionmanager2/UploadOutputStream;->transferred:J

    invoke-interface {v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/UploadUpdateListener;->transferred(J)V

    .line 66
    return-void
.end method
