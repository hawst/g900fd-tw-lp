.class public abstract Lcom/sec/android/service/health/cp/BaseInitializationTask;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/BaseInitializationTask$4;,
        Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final BASE_DB_NAME:Ljava/lang/String; = "base.db"

.field public static final DATABASE_COPY_BUFFER:I = 0x1000

.field public static final DATABASE_DIR:Ljava/lang/String; = "databases"

.field public static final H_DB_NAME:Ljava/lang/String; = "shealth_.db"

.field public static final INIT_DATABASE:I = 0x1

.field public static final LAST_UPGRADED_VERSION:Ljava/lang/String; = "last_version_of_upgrade"

.field public static final MIGRATE_2X_TO_25:I = 0x2

.field public static final MIGRATE_2X_TO_35:I = 0x3

.field public static final MIGRATE_3X_TO_35:I = 0x4

.field public static final OLD_BACKUP_DIR:Ljava/lang/String; = "old"

.field public static final OLD_BACKUP_DIR_TEMP:Ljava/lang/String; = "tempold"

.field protected static final PROG_0:I = 0x0

.field protected static final PROG_100:I = 0x64

.field protected static final PROG_20:I = 0x14

.field protected static final PROG_40:I = 0x28

.field protected static final PROG_60:I = 0x3c

.field protected static final PROG_80:I = 0x50

.field public static final SHAREDPREF_UPGRADE_FILE:Ljava/lang/String; = "upgrade_prefs"

.field private static final TAG:Ljava/lang/String;

.field public static final UPGRADE_CHECK_KEY:Ljava/lang/String; = "isUpgradeCheckRequired"

.field private static volatile mIsInitialized:Z

.field protected static mLock:Ljava/lang/Object;


# instance fields
.field public final NEW_VERSION:I

.field private mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

.field private mCancelDone:Z

.field protected final mContext:Landroid/content/Context;

.field private mIsStarted:Z

.field protected mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mIsInitialized:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->NEW_VERSION:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCancelDone:Z

    iput-object p1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    iput-object p3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    return-void
.end method

.method public static cleanUpOldDatabase(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "[+] cleanUpOldDatabase"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "databases"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "old"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->deleteRecursive(Ljava/io/File;)V

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "[-] cleanUpOldDatabase"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x400

    new-array v0, v0, [B

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private copyFromTo(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    .locals 8

    const/4 v2, 0x0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-direct {p0, v3, v1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v3, "Failed to close stream"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v2, "Failed to close stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_5
    iget-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    const-string v4, "com.sec.android.app.shealth"

    const-string v5, "ERR_MI"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FAIL3: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v3, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to copy asset file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-eqz v2, :cond_2

    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_2
    :goto_3
    if-eqz v1, :cond_1

    :try_start_7
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    :catch_3
    move-exception v0

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v2, "Failed to close stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_4
    move-exception v0

    sget-object v2, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v3, "Failed to close stream"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    if-eqz v2, :cond_3

    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_3
    :goto_5
    if-eqz v1, :cond_4

    :try_start_9
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    :cond_4
    :goto_6
    throw v0

    :catch_5
    move-exception v2

    sget-object v3, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v4, "Failed to close stream"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catch_6
    move-exception v1

    sget-object v2, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v3, "Failed to close stream"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_4

    :catchall_3
    move-exception v0

    goto :goto_4

    :catch_7
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto/16 :goto_2

    :catch_8
    move-exception v0

    move-object v2, v3

    goto/16 :goto_2
.end method

.method private static deleteRecursive(Ljava/io/File;)V
    .locals 4

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->deleteRecursive(Ljava/io/File;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t delete file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public static getBackupPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "databases"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "old"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isInitialized()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mIsInitialized:Z

    return v0
.end method

.method public static setIsInitialized(Z)V
    .locals 0

    sput-boolean p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mIsInitialized:Z

    return-void
.end method


# virtual methods
.method protected backupOldDatabase()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "[+] backupOldDatabase"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "databases"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "tempold"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "databases"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "old"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v3, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "databases"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask$1;-><init>(Lcom/sec/android/service/health/cp/BaseInitializationTask;)V

    invoke-virtual {v3, v0}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    invoke-direct {p0, v3, v1, v6}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->copyFromTo(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "[-] backupOldDatabase"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "back up  dir not able to create."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 8

    const/4 v1, 0x0

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-eqz p1, :cond_1

    array-length v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    aget-object v0, p1, v1

    :cond_1
    sget-object v2, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mIsStarted:Z

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "locking mlock thread Id ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mIsInitialized : already initialized. concurrent call resulted in this"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    :try_start_3
    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mIsInitialized : false"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->setIsInitialized(Z)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Lcom/sec/android/service/health/cp/database/DBManager;->setContext(Landroid/content/Context;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v6, :cond_4

    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "databases"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "old"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v3, "backup dir is present so restoring the backup db"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->restoreOldDatabase()V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->cleanUpOldDatabase(Landroid/content/Context;)V

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->backupOldDatabase()V

    :cond_4
    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->handelCommand(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v6, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_5

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->setIsInitialized(Z)V

    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->finishInitialize()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    :try_start_5
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->restoreOldDatabase()V

    iget-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->cleanUpOldDatabase(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCancelDone:Z

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1

    :cond_7
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->setIsInitialized(Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_6
    sget-object v3, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doInBackground"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v6, :cond_a

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "databases"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "old"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_8

    sget-object v3, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v4, "backup dir is present so restoring the backup db"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->restoreOldDatabase()V

    iget-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->cleanUpOldDatabase(Landroid/content/Context;)V

    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_9

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "cancel is called so it is success case"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "returning TASK_SUCCESS status"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCancelDone:Z

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    monitor-exit v2

    goto/16 :goto_0

    :cond_9
    iget-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    const-string v4, "com.sec.android.app.shealth"

    const-string v5, "ERR_MI"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FAIL3:return migration failed status for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v3, v4, v5, v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "returning TASK_EXCEPTION status"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    monitor-exit v2

    goto/16 :goto_0

    :cond_a
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.shealth"

    const-string v3, "ERR_KM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Initial failed in ART mode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v0, v1, v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    monitor-exit v2

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "this case cannot be handled as the issue is with secure storage"

    invoke-static {v0, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Crashing the application as secure storage failed"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_c
    const-string/jumbo v0, "unknown issue"
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected finishInitialize()V
    .locals 3

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCancelDone:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "canceled requested so setIsInitialized is not called."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "Notify mLock"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mIsInitialized : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->isInitialized()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract handelCommand(Ljava/lang/Integer;)Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected initialize()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "Start DBManager Initialization."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/cp/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/service/health/cp/configuration/SharedPrefManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/configuration/SharedPrefManager;->initialize(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->getInstance()Lcom/sec/android/service/health/cp/database/validator/DataParser;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/database/validator/DataParser;->initialize(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/database/DBManager;->initialize(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-static {}, Lcom/sec/android/service/health/cp/files/FileManager;->getInstance()Lcom/sec/android/service/health/cp/files/FileManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/files/FileManager;->initialize(Landroid/content/Context;)V

    return-void
.end method

.method protected migrateFiles(Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "upgradeSharedPreference for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateFilesManager;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateFilesManager;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask$4;->$SwitchMap$com$sec$android$service$health$cp$BaseInitializationTask$VERSION_UPGRADE:[I

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v1, 0x5

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/MigrateFilesManager;->upgrade(II)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCancelled(Ljava/lang/Integer;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCancelled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCancelDone:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mIsStarted:Z

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    invoke-interface {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;->onCancelled(Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    :cond_1
    :goto_0
    iput-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    iput-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;->onFinished(Ljava/lang/Integer;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    goto :goto_0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->onCancelled(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onPostExecute"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    iget-object v1, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;->onFinished(Ljava/lang/Integer;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    :cond_0
    iput-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    iput-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCancelDone:Z

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mIsStarted:Z

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onProgressUpdate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mStatusCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;

    aget-object v1, p1, v3

    iget-object v2, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mCallerCallBack:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListenerInternal;->onProgressUpdate(Ljava/lang/Integer;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method protected restoreOldDatabase()V
    .locals 8

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v2, "[+] restoreOldDatabase"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "databases"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "old"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    new-instance v3, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "databases"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/service/health/cp/BaseInitializationTask$2;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask$2;-><init>(Lcom/sec/android/service/health/cp/BaseInitializationTask;)V

    invoke-virtual {v3, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/sec/android/service/health/cp/BaseInitializationTask$3;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/cp/BaseInitializationTask$3;-><init>(Lcom/sec/android/service/health/cp/BaseInitializationTask;)V

    invoke-virtual {v2, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    array-length v4, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v1, v0

    invoke-direct {p0, v2, v3, v5}, Lcom/sec/android/service/health/cp/BaseInitializationTask;->copyFromTo(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    const-string v1, "[-] restoreOldDatabase"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected upgradeSharedPreference(Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "upgradeSharedPreference for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;

    iget-object v2, p0, Lcom/sec/android/service/health/cp/BaseInitializationTask;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;-><init>(Landroid/content/SharedPreferences;Landroid/content/Context;)V

    sget-object v0, Lcom/sec/android/service/health/cp/BaseInitializationTask$4;->$SwitchMap$com$sec$android$service$health$cp$BaseInitializationTask$VERSION_UPGRADE:[I

    invoke-virtual {p1}, Lcom/sec/android/service/health/cp/BaseInitializationTask$VERSION_UPGRADE;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v0, 0x5

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/HtoKPrefUpgradeManager;->upgrade(II)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
