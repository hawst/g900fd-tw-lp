.class public Lcom/sec/android/service/health/cp/serversync/data/BloodGlucoseItem;
.super Ljava/lang/Object;


# instance fields
.field public accId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public bloodSugar:Ljava/lang/Double;

.field public bloodSugarUnit:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public deviceBioPKId:J

.field public deviceCreateDate:Ljava/lang/String;

.field public deviceDaylightSaving:Ljava/lang/String;

.field public deviceModifyDate:Ljava/lang/String;

.field public deviceTimeZone:Ljava/lang/String;

.field public hba1c:Ljava/lang/Double;

.field public hdid:Ljava/lang/String;

.field public inputType:Ljava/lang/String;

.field public mealTime:Ljava/lang/String;

.field public mealTimeType:Ljava/lang/String;

.field public measurementContextList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/service/health/cp/serversync/data/BloodGlucoseMeasurementItem;",
            ">;"
        }
    .end annotation
.end field

.field public requestType:Ljava/lang/String;

.field public samplePositionType:Ljava/lang/Integer;

.field public sampleTime:Ljava/lang/String;

.field public sampleType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/cp/serversync/data/BloodGlucoseItem;->measurementContextList:Ljava/util/ArrayList;

    return-void
.end method
