.class public final Lcom/sec/android/service/health/sensor/handler/bluetooth/hme/command/HmeCommandSetConstants;
.super Ljava/lang/Object;


# static fields
.field public static final APP_SENSOR_GET_DEVICE_TYPE:Ljava/lang/String; = "app_sensor_get_device_type"

.field public static final APP_SENSOR_GET_MODEL_NAME:Ljava/lang/String; = "app_sensor_get_model_name"

.field public static final APP_SENSOR_GET_TIME:Ljava/lang/String; = "app_sensor_get_time"

.field public static final APP_SENSOR_POWER_OFF:Ljava/lang/String; = "app_sensor_power_off"

.field public static final APP_SENSOR_SET_TIME:Ljava/lang/String; = "app_sensor_set_time"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
