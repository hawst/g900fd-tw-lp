.class public Lcom/sec/android/service/health/cp/common/DBTables$MealItemsTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MealItemsTable"
.end annotation


# static fields
.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_ID_FOOD_INFO:Ljava/lang/String; = "id_food_info"

.field public static final KEY_ID_MEAL_TABLE:Ljava/lang/String; = "id_meal_table"

.field public static final KEY_MARK_NUMBER:Ljava/lang/String; = "mark_number"

.field public static final KEY_MARK_POSX:Ljava/lang/String; = "mark_posx"

.field public static final KEY_MARK_POSY:Ljava/lang/String; = "mark_posy"

.field public static final KEY_PERCENT:Ljava/lang/String; = "percent"

.field public static final KEY_UNIT:Ljava/lang/String; = "unit"

.field public static final TABLE_NAME:Ljava/lang/String; = "meal_items"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
