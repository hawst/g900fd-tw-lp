.class public Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
.implements Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;,
        Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;,
        Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$SyncCommands;
    }
.end annotation


# static fields
.field private static final EXTRA_CRYPTOGRAPHIC_NONCE:Ljava/lang/String; = "EXTRA_CRYPTOGRAPHIC_NONCE"

.field private static final RECEIVE_DATA:I = 0x1388

.field private static final SEND_RESPONSE:I = 0x1389

.field private static final STATE_IDLE:I = 0x5

.field private static final STATE_PROCESSING_CMD:I = 0x4

.field private static final STATE_PROCESSING_REQ:I = 0x3

.field public static final TAG:Ljava/lang/String;

.field private static final extra_start_time:Ljava/lang/String; = "EXTRA_START_TIME"


# instance fields
.field private THREAD_NAME:Ljava/lang/String;

.field private builder_notify:Landroid/support/v4/app/NotificationCompat$Builder;

.field private completeDataBuffer:[B

.field private dbInsertError:I

.field private lastSyncTime:J

.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field public mContext:Landroid/content/Context;

.field private mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

.field private mCurrentState:I

.field public mData:Ljava/lang/Object;

.field public mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private volatile mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

.field public mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

.field private mRespTimer:Ljava/util/Timer;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

.field private nonce:J

.field private sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

.field private samle_time:I

.field protected selectedDate:Ljava/util/Date;

.field private send_data:Z

.field private sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

.field private sleep_status:I

.field private thread:Landroid/os/HandlerThread;

.field private wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mRespTimer:Ljava/util/Timer;

    const-string v0, "SWEARABLE_PROTOCOL"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->THREAD_NAME:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->nonce:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->lastSyncTime:J

    iput v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$1;-><init>(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->dbInsertError:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;DLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->coaching_response(DLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sendDataUpdatedToApp(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->closeSocketConnection()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;[B)[B
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    return-object p1
.end method

.method private changeProfileWeight(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 7

    const-wide v5, 0x40c3880000000000L    # 10000.0

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    float-to-double v1, v0

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Original Value == "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    iget v0, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SProfile;

    mul-double/2addr v1, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    div-double/2addr v1, v5

    double-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    return-object p1
.end method

.method private changeProfileWeight(Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;)Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;
    .locals 7

    const-wide v5, 0x40c3880000000000L    # 10000.0

    iget v0, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    float-to-double v0, v0

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Original Value == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    mul-double/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    div-double/2addr v0, v5

    double-to-float v0, v0

    iput v0, p1, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;->weight:F

    return-object p1
.end method

.method private checkHaveSensorPermission(I)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    sparse-switch p1, :sswitch_data_0

    const-string v3, "com.sec.android.service.health.permission.SENSOR"

    const-string v4, "com.samsung.accessory.goproviders"

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v2, "GEAR_MANAGER_PKG_GOPROVIDER is permission granted "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move v1, v0

    goto :goto_0

    :sswitch_0
    const-string v3, "com.sec.android.service.health.permission.SENSOR"

    const-string v4, "com.samsung.android.app.atracker"

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v2, "ACTIVITY_TRACKER_MANAGER_PKG is permission granted "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :sswitch_1
    const-string v3, "com.sec.android.service.health.permission.SENSOR"

    const-string v4, "com.samsung.android.wms"

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v2, "WINGTIP_MANAGER_PKG is permission granted "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2723 -> :sswitch_1
        0x2727 -> :sswitch_0
    .end sparse-switch
.end method

.method private checkNotHavePermition(I)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v2, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    sparse-switch p1, :sswitch_data_0

    const-string v3, "com.samsung.accessory.goproviders"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v3, 0xa5

    if-ge v1, v3, :cond_1

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v4, "GEAR_MANAGER_PKG_GOPROVIDER is not have permission "

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    :cond_1
    :goto_1
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " VersionCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :sswitch_0
    :try_start_1
    const-string v3, "com.samsung.android.app.atracker"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v3, 0x9

    if-ge v1, v3, :cond_1

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v4, "ACTIVITYTRACKER_MANAGER is not have permission "

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_1

    :sswitch_1
    const-string v3, "com.samsung.android.wms"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v3, 0x564

    if-ge v1, v3, :cond_1

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v4, "GEARFIT_MANAGER is not have permission "

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v2

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2723 -> :sswitch_1
        0x2727 -> :sswitch_0
    .end sparse-switch
.end method

.method private closeSocketConnection()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v1, "before calling wearableDataComm.close()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->close()V

    :cond_0
    return-void
.end method

.method private coaching_response(DLjava/lang/String;)V
    .locals 9

    const/4 v8, 0x1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    if-nez v0, :cond_1

    invoke-direct {p0, v8}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sWearableDataTranslator is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-wide v6, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->lastSyncTime:J

    move-object v3, p3

    move-wide v4, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->sendCoachingResponse(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Ljava/lang/String;DJ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v8}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sendCoachingResponse is false"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getCoachingResponseActionName()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCoachingResponseActionName ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :pswitch_1
    const-string v0, "com.samsung.android.shealth.ACTION_COACHING_RESPONSE"

    goto :goto_0

    :pswitch_2
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR2_COACHING_RESPONSE"

    goto :goto_0

    :pswitch_3
    const-string v0, "com.samsung.android.shealth.ACTION_SBAND_COACHING_RESPONSE"

    goto :goto_0

    :pswitch_4
    const-string v0, "com.samsung.android.shealth.ACTION_SHEALTH_RESPONSE"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private getSyncActionName()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "REQUEST_SYNC : getSyncActionName mDevice is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const-string v0, "com.samsung.android.shealth.ACTION_REQUEST_SYNC"

    goto :goto_0

    :pswitch_2
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR2_REQUEST_SYNC"

    goto :goto_0

    :pswitch_3
    const-string v0, "com.samsung.android.shealth.ACTION_SBAND_REQUEST_SYNC"

    goto :goto_0

    :pswitch_4
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR_REQUEST_SYNC"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private noti_stop(Z)V
    .locals 6

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[+] noti_stop"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->stopTimer()V

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->closeSocketConnection()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->interrupt()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    :cond_0
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    if-nez p1, :cond_5

    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v4, v4}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    :cond_2
    :goto_1
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->getInstance()Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->setIsSyncing(Z)V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v1, "[-] noti_stop"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    invoke-direct {p0, v5}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->stopData(Ljava/lang/String;ILjava/util/List;)Z

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    if-nez v0, :cond_4

    const-string v0, "INVALID_COMMAND"

    :goto_2
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_RSP"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v4, v5}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    goto :goto_1

    :cond_6
    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->stopData(Ljava/lang/String;ILjava/util/List;)Z

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    if-nez v0, :cond_7

    const-string v0, "INVALID_COMMAND"

    :goto_3
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    goto/16 :goto_1

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_RSP"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private notifyStartLocal()I
    .locals 9

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mServiceLooper.quit()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->interrupt()V

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "thread.interrupt()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->THREAD_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;-><init>(Landroid/os/Looper;Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->getInstance()Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->setIsSyncing(Z)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startReceivingData type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_0
    :pswitch_0
    new-instance v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;-><init>(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_1
    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/manager/util/SAPHRMSharedPreferencesHelper;->getWearableLastSyncTime(ILjava/lang/String;)J

    move-result-wide v0

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getWearableLastSyncTime (type= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",id= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->getSyncActionName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    const/16 v0, 0x3e8

    :goto_2
    return v0

    :pswitch_1
    const-string v1, "com.samsung.android.shealth.ACTION_DATA_SEND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.samsung.android.shealth.SYNC_ERROR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "com.samsung.android.shealth.ACTION_GEAR2_DATA_SEND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.samsung.android.shealth.GEAR_SYNC_ERROR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3
    const-string v1, "com.samsung.android.shealth.ACTION_SBAND_DATA_SEND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.samsung.android.shealth.SBAND_SYNC_ERROR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_4
    const-string v1, "com.samsung.android.shealth.GEAR_SYNC_ERROR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device is null, mContext is null, mWearableReceiver is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    new-instance v2, Landroid/content/Intent;

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->getSyncActionName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "EXTRA_START_TIME"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->lastSyncTime:J

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "extra_start_time : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lastSyncTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->lastSyncTime:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->nonce:J

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->openSocketForCommunication()V

    const-string v0, "EXTRA_CRYPTOGRAPHIC_NONCE"

    iget-wide v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->nonce:J

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/SWearableManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    const/16 v3, 0x2730

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/SWearableManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_6

    const-string v0, "DEVICE"

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->getWearableFixedNameFromDeviceType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    const-string v0, "VERSION"

    const-wide v3, 0x4008147ae147ae14L    # 3.01

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send BR : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v6, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->startTimer()V

    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v0, v8}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStarted(I)V

    :cond_7
    :goto_4
    move v0, v8

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-static {}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->getInstance()Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getExerciseId()J

    move-result-wide v5

    invoke-virtual {v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getProcessId()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/sensor/protocol/helper/DataHelper;->startData(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)Z

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v1, "current state is Command processing so not posting onDataStarted!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catch_0
    move-exception v0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private openSocketForCommunication()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->closeSocketConnection()V

    new-instance v0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;-><init>(Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->initialize()V

    return-void
.end method

.method private saveJsonHealthData()V
    .locals 7

    const/4 v1, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    if-eqz v0, :cond_1

    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    const-string v3, "END_OF_DATA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    :try_start_1
    new-instance v2, Lcom/google/gson/GsonBuilder;

    invoke-direct {v2}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "[HS_Wearable_sync]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "data received from socket"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "[HS_Wearable_sync]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "action_gear : wearable json data string = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-class v3, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->getInstance()Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;

    move-result-object v2

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;->version:D

    invoke-static {v3, v4}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->protocolNegotication(D)D

    move-result-wide v3

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslatorFactory;->getDataTranslatorFromProtocolVersion(DZ)Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Protocol version is wrong"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v0, v1

    goto/16 :goto_0

    :cond_1
    move-object v0, v1

    goto/16 :goto_1

    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sWearableDataTranslator:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/16 v5, 0x1388

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableDataTranslator;->makeMessageFromJWearableData(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;I)Landroid/os/Message;

    move-result-object v0

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    invoke-virtual {v2, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SYNC_ERROR in send message"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception!! : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v6}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_3
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "[HS_Wearable_sync]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "send_data or msg is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->send_data:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    goto/16 :goto_2

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "WearableData is null"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2
.end method

.method private sendDataUpdatedToApp(II)V
    .locals 4

    const/4 v3, 0x7

    if-nez p1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStopped dataType= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.CONNECTION_TYPE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.ERROR_CODE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.DEVICE_TYPE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStopped dataType with error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.CONNECTION_TYPE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.ERROR_CODE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.health.sensor.extra.DEVICE_TYPE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startTimer()V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->stopTimer()V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mRespTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mRespTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$2;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$2;-><init>(Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;)V

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method

.method private stopTimer()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mRespTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mRespTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->stopTimer()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mWearableReceiver:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$WearableReceiver;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mHandler:Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$MyHandler;->removeMessages(I)V

    :cond_1
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDeepSleep()J
    .locals 15

    const/16 v14, 0xc

    const/16 v13, 0xb

    const/4 v12, 0x0

    const-wide/16 v10, 0x64

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->samle_time:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3c

    mul-int/lit8 v2, v2, 0x3c

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v14}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x3c

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long v6, v2, v4

    iget v8, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sleep_status:I

    int-to-long v8, v8

    mul-long/2addr v6, v8

    div-long/2addr v6, v10

    iget v8, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sleep_status:I

    int-to-long v8, v8

    mul-long/2addr v2, v8

    div-long/2addr v2, v10

    iget v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sleep_status:I

    int-to-long v2, v2

    mul-long/2addr v2, v4

    div-long/2addr v2, v10

    invoke-virtual {v0, v13, v12}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0, v14, v12}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    add-long/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProtocolName()Ljava/lang/String;
    .locals 1

    const-string v0, "WINGTIP"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 3

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iput-object p3, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mData:Ljava/lang/Object;

    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getInstance()Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/ProfileManagerFactory;->getSensorManager(II)Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->sensorManager:Lcom/sec/android/service/health/sensor/manager/AbstractSensorManager;

    const/4 v0, 0x0

    return v0
.end method

.method public notifyRawDataReceived([B)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public notifyStart()I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "notifyStart is called! mCurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/16 v0, 0x3ec

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->notifyStartLocal()I

    move-result v0

    goto :goto_0
.end method

.method public notifyStop()I
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    return v0
.end method

.method public onCertificationDataReceived(JZ)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->nonce:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->receiveData()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string v1, "Certificaton fail"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    goto :goto_0
.end method

.method public onConnectionFailed(I)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onConnectionFailed()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync_ERROR]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Sync_Error_onConnectionFailed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    return-void
.end method

.method public onConnectionSuccess()V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onConnectionSuccess() wearableDataComm = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->wearableDataComm:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->listenToConnect()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->noti_stop(Z)V

    goto :goto_0
.end method

.method public onDataReceived([BIZ)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    if-nez v0, :cond_2

    new-array v0, p2, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    invoke-static {p1, v3, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[HS_Wearable_sync]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Received Data from Gear to use Socket."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->saveJsonHealthData()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    array-length v0, v0

    add-int/2addr v0, p2

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    array-length v1, v1

    invoke-static {p1, v3, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v0

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->completeDataBuffer:[B

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "request()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state of the protocole "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x3ec

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_2

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentCommand:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$SyncCommands;->CMD_START_SYNC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->mCurrentState:I

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->notifyStartLocal()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol$SyncCommands;->CMD_STOP_SYNC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/SWearableProtocol;->notifyStop()I

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x3ea

    goto :goto_0
.end method
