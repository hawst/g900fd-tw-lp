.class public Lcom/sec/android/service/health/contentservice/_ContentValueData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public columnName:Ljava/lang/String;

.field public device_unit:I

.field public index:I

.field public time:J

.field public unit:I

.field public value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/service/health/contentservice/_ContentValueData$1;

    invoke-direct {v0}, Lcom/sec/android/service/health/contentservice/_ContentValueData$1;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->device_unit:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->index:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/service/health/contentservice/_ContentValueData$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/contentservice/_ContentValueData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 11

    const-wide v9, 0x3815020d828dfee3L    # 1.543425452515704E-38

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p2

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->columnName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->unit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->device_unit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->index:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/sec/android/service/health/contentservice/_ContentValueData;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
