.class Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/contentservice/HealthContentShow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CloseContentServiceReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 30

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x69b5

    aput v10, v2, v9

    const/16 v9, -0xb

    aput v9, v2, v8

    const/16 v8, -0x61

    aput v8, v2, v7

    const/16 v7, -0x15

    aput v7, v2, v6

    const/16 v6, -0x12

    aput v6, v2, v3

    const/16 v3, 0x2e7b

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/16 v11, -0x69d2

    aput v11, v1, v10

    const/16 v10, -0x6a

    aput v10, v1, v9

    const/16 v9, -0xa

    aput v9, v1, v8

    const/16 v8, -0x63

    aput v8, v1, v7

    const/16 v7, -0x75

    aput v7, v1, v6

    const/16 v6, 0x2e1f

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, -0x3d

    aput v15, v2, v14

    const/16 v14, -0xb

    aput v14, v2, v13

    const/16 v13, -0x3a95

    aput v13, v2, v12

    const/16 v12, -0x5e

    aput v12, v2, v11

    const/16 v11, -0x4787

    aput v11, v2, v10

    const/16 v10, -0x2f

    aput v10, v2, v9

    const/16 v9, -0x57

    aput v9, v2, v8

    const/16 v8, -0x35

    aput v8, v2, v7

    const/16 v7, -0x56

    aput v7, v2, v3

    const/16 v3, 0x3a01

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, -0x59

    aput v16, v1, v15

    const/16 v15, -0x64

    aput v15, v1, v14

    const/16 v14, -0x3ac5

    aput v14, v1, v13

    const/16 v13, -0x3b

    aput v13, v1, v12

    const/16 v12, -0x47e9

    aput v12, v1, v11

    const/16 v11, -0x48

    aput v11, v1, v10

    const/16 v10, -0x33

    aput v10, v1, v9

    const/16 v9, -0x5b

    aput v9, v1, v8

    const/16 v8, -0x3d

    aput v8, v1, v7

    const/16 v7, 0x3a63

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v6, v4, v3

    long-to-int v3, v6

    if-gtz v3, :cond_4

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    ushr-long v6, v1, v6

    aget-wide v1, v4, v3

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_5

    const-wide v8, 0x6722415d1bd0c135L    # 6.354434606944341E188

    xor-long/2addr v1, v8

    :cond_5
    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    const/16 v8, 0x20

    shl-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x6722415d1bd0c135L    # 6.354434606944341E188

    xor-long/2addr v1, v6

    aput-wide v1, v4, v3

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x4a

    aput v12, v2, v11

    const/4 v11, -0x1

    aput v11, v2, v10

    const/16 v10, -0x4b

    aput v10, v2, v9

    const/16 v9, -0x6a

    aput v9, v2, v8

    const/4 v8, -0x4

    aput v8, v2, v7

    const/16 v7, 0x623f

    aput v7, v2, v6

    const/16 v6, -0x65f9

    aput v6, v2, v3

    const/4 v3, -0x2

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x2e

    aput v13, v1, v12

    const/16 v12, -0x6a

    aput v12, v1, v11

    const/16 v11, -0x30

    aput v11, v1, v10

    const/16 v10, -0xb

    aput v10, v1, v9

    const/16 v9, -0x6b

    aput v9, v1, v8

    const/16 v8, 0x6249

    aput v8, v1, v7

    const/16 v7, -0x659e

    aput v7, v1, v6

    const/16 v6, -0x66

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, -0x33

    aput v15, v2, v14

    const/16 v14, -0x7e

    aput v14, v2, v13

    const/16 v13, -0x2a

    aput v13, v2, v12

    const/16 v12, -0x44c4

    aput v12, v2, v11

    const/16 v11, -0x2e

    aput v11, v2, v10

    const/16 v10, -0x209e

    aput v10, v2, v9

    const/16 v9, -0x46

    aput v9, v2, v8

    const/16 v8, -0x48

    aput v8, v2, v3

    const/16 v3, -0x74

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, -0x57

    aput v16, v1, v15

    const/16 v15, -0x35

    aput v15, v1, v14

    const/16 v14, -0x4d

    aput v14, v1, v13

    const/16 v13, -0x44a1

    aput v13, v1, v12

    const/16 v12, -0x45

    aput v12, v1, v11

    const/16 v11, -0x20ec

    aput v11, v1, v10

    const/16 v10, -0x21

    aput v10, v1, v9

    const/4 v9, -0x4

    aput v9, v1, v8

    const/16 v8, -0x1f

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v8, v1

    if-lt v3, v8, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v8, v1

    if-lt v3, v8, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceId:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$700(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0x18a4

    aput v13, v2, v12

    const/16 v12, -0x72

    aput v12, v2, v11

    const/16 v11, -0x41

    aput v11, v2, v10

    const/16 v10, -0xb

    aput v10, v2, v9

    const/16 v9, -0x5e

    aput v9, v2, v8

    const/16 v8, 0x2a3c

    aput v8, v2, v7

    const/16 v7, -0x57bb

    aput v7, v2, v6

    const/16 v6, -0x26

    aput v6, v2, v3

    const/16 v3, -0x3e

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, -0x18c8

    aput v14, v1, v13

    const/16 v13, -0x19

    aput v13, v1, v12

    const/16 v12, -0x34

    aput v12, v1, v11

    const/16 v11, -0x7a

    aput v11, v1, v10

    const/16 v10, -0x39

    aput v10, v1, v9

    const/16 v9, 0x2a5f

    aput v9, v1, v8

    const/16 v8, -0x57d6

    aput v8, v1, v7

    const/16 v7, -0x58

    aput v7, v1, v6

    const/16 v6, -0x4e

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v6, v1

    if-lt v3, v6, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v6, v1

    if-lt v3, v6, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, -0x4c

    aput v21, v2, v20

    const/16 v20, 0x1629

    aput v20, v2, v19

    const/16 v19, -0x489e

    aput v19, v2, v18

    const/16 v18, -0x2c

    aput v18, v2, v17

    const/16 v17, 0x6235

    aput v17, v2, v16

    const/16 v16, -0x20f8

    aput v16, v2, v15

    const/16 v15, -0x43

    aput v15, v2, v14

    const/16 v14, -0x6f

    aput v14, v2, v13

    const/16 v13, 0x760c

    aput v13, v2, v12

    const/16 v12, -0x2eb

    aput v12, v2, v11

    const/16 v11, -0x6c

    aput v11, v2, v10

    const/16 v10, -0x73

    aput v10, v2, v9

    const/16 v9, -0xc

    aput v9, v2, v8

    const/16 v8, 0x225d

    aput v8, v2, v3

    const/16 v3, -0x5b1

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, -0x30

    aput v22, v1, v21

    const/16 v21, 0x1660

    aput v21, v1, v20

    const/16 v20, -0x48ea

    aput v20, v1, v19

    const/16 v19, -0x49

    aput v19, v1, v18

    const/16 v18, 0x6250

    aput v18, v1, v17

    const/16 v17, -0x209e

    aput v17, v1, v16

    const/16 v16, -0x21

    aput v16, v1, v15

    const/16 v15, -0x22

    aput v15, v1, v14

    const/16 v14, 0x7669

    aput v14, v1, v13

    const/16 v13, -0x28a

    aput v13, v1, v12

    const/4 v12, -0x3

    aput v12, v1, v11

    const/4 v11, -0x5

    aput v11, v1, v10

    const/16 v10, -0x6f

    aput v10, v1, v9

    const/16 v9, 0x2219

    aput v9, v1, v8

    const/16 v8, -0x5de

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v8, v1

    if-lt v3, v8, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v8, v1

    if-lt v3, v8, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceObjectId:I
    invoke-static {v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$800(Lcom/sec/android/service/health/contentservice/HealthContentShow;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceId:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$700(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x57

    aput v11, v2, v10

    const/16 v10, -0x17

    aput v10, v2, v9

    const/16 v9, -0x45d1

    aput v9, v2, v8

    const/16 v8, -0x27

    aput v8, v2, v7

    const/16 v7, 0x7c26

    aput v7, v2, v6

    const/16 v6, -0x14f6

    aput v6, v2, v5

    const/16 v5, -0x72

    aput v5, v2, v3

    const/16 v3, 0x4147

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x33

    aput v12, v1, v11

    const/16 v11, -0x80

    aput v11, v1, v10

    const/16 v10, -0x45b6

    aput v10, v1, v9

    const/16 v9, -0x46

    aput v9, v1, v8

    const/16 v8, 0x7c4f

    aput v8, v1, v7

    const/16 v7, -0x1484

    aput v7, v1, v6

    const/16 v6, -0x15

    aput v6, v1, v5

    const/16 v5, 0x4123

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, -0x31

    aput v21, v2, v20

    const/16 v20, -0x6e

    aput v20, v2, v19

    const/16 v19, 0x1a62

    aput v19, v2, v18

    const/16 v18, 0x3d69

    aput v18, v2, v17

    const/16 v17, -0x3ad

    aput v17, v2, v16

    const/16 v16, -0x6b

    aput v16, v2, v15

    const/16 v15, -0x48

    aput v15, v2, v14

    const/16 v14, -0xf

    aput v14, v2, v13

    const/16 v13, 0x1e22

    aput v13, v2, v12

    const/16 v12, 0x577b

    aput v12, v2, v11

    const/16 v11, 0x6a34

    aput v11, v2, v10

    const/16 v10, 0x3a03

    aput v10, v2, v9

    const/16 v9, -0x34b4

    aput v9, v2, v8

    const/16 v8, -0x52

    aput v8, v2, v7

    const/16 v7, -0x369c

    aput v7, v2, v3

    const/16 v3, -0x5c

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, -0x56

    aput v22, v1, v21

    const/16 v21, -0xa

    aput v21, v1, v20

    const/16 v20, 0x1a0b

    aput v20, v1, v19

    const/16 v19, 0x3d1a

    aput v19, v1, v18

    const/16 v18, -0x3c3

    aput v18, v1, v17

    const/16 v17, -0x4

    aput v17, v1, v16

    const/16 v16, -0x19

    aput v16, v1, v15

    const/16 v15, -0x6b

    aput v15, v1, v14

    const/16 v14, 0x1e6b

    aput v14, v1, v13

    const/16 v13, 0x571e

    aput v13, v1, v12

    const/16 v12, 0x6a57

    aput v12, v1, v11

    const/16 v11, 0x3a6a

    aput v11, v1, v10

    const/16 v10, -0x34c6

    aput v10, v1, v9

    const/16 v9, -0x35

    aput v9, v1, v8

    const/16 v8, -0x36e0

    aput v8, v1, v7

    const/16 v7, -0x37

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v7, v1

    if-lt v3, v7, :cond_10

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v7, v1

    if-lt v3, v7, :cond_11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceId:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$700(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_14

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x2d3

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x2e3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_7
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_8
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_9
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_a
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_b
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_c
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_d
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_10
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_11
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_12
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_13
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_14
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_15

    const-wide v3, 0x6722415d1bd0c135L    # 6.354434606944341E188

    xor-long/2addr v1, v3

    :cond_15
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceObjectId:I
    invoke-static {v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$800(Lcom/sec/android/service/health/contentservice/HealthContentShow;)I

    move-result v2

    if-ne v1, v2, :cond_16

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x2502

    aput v11, v2, v10

    const/16 v10, 0x2e4c

    aput v10, v2, v9

    const/16 v9, 0x405d

    aput v9, v2, v8

    const/16 v8, -0x2ecd

    aput v8, v2, v7

    const/16 v7, -0x4c

    aput v7, v2, v6

    const/16 v6, 0x4b4b

    aput v6, v2, v5

    const/16 v5, -0x79dc

    aput v5, v2, v4

    const/16 v4, -0xc

    aput v4, v2, v3

    const/4 v3, -0x7

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x2566

    aput v12, v1, v11

    const/16 v11, 0x2e25

    aput v11, v1, v10

    const/16 v10, 0x402e

    aput v10, v1, v9

    const/16 v9, -0x2ec0

    aput v9, v1, v8

    const/16 v8, -0x2f

    aput v8, v1, v7

    const/16 v7, 0x4b28

    aput v7, v1, v6

    const/16 v6, -0x79b5

    aput v6, v1, v5

    const/16 v5, -0x7a

    aput v5, v1, v4

    const/16 v4, -0x77

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_12
    array-length v4, v1

    if-lt v3, v4, :cond_17

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_13
    array-length v4, v1

    if-lt v3, v4, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x18

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, -0x3589

    aput v28, v2, v27

    const/16 v27, -0x52

    aput v27, v2, v26

    const/16 v26, 0x505d

    aput v26, v2, v25

    const/16 v25, -0x70dd

    aput v25, v2, v24

    const/16 v24, -0x1f

    aput v24, v2, v23

    const/16 v23, -0x7cf3

    aput v23, v2, v22

    const/16 v22, -0x24

    aput v22, v2, v21

    const/16 v21, -0x23

    aput v21, v2, v20

    const/16 v20, -0x52

    aput v20, v2, v19

    const/16 v19, 0x7014

    aput v19, v2, v18

    const/16 v18, -0x17c7

    aput v18, v2, v17

    const/16 v17, -0x64

    aput v17, v2, v16

    const/16 v16, 0x180d

    aput v16, v2, v15

    const/16 v15, -0x6283

    aput v15, v2, v14

    const/16 v14, -0x9

    aput v14, v2, v13

    const/16 v13, -0xd

    aput v13, v2, v12

    const/16 v12, 0x3d44

    aput v12, v2, v11

    const/16 v11, 0x3c58

    aput v11, v2, v10

    const/16 v10, 0x505f

    aput v10, v2, v9

    const/16 v9, 0x7639

    aput v9, v2, v8

    const/16 v8, -0x1300

    aput v8, v2, v7

    const/16 v7, -0x78

    aput v7, v2, v6

    const/16 v6, 0x856

    aput v6, v2, v3

    const/16 v3, -0x1f9b

    aput v3, v2, v1

    const/16 v1, 0x18

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, -0x35ee

    aput v29, v1, v28

    const/16 v28, -0x36

    aput v28, v1, v27

    const/16 v27, 0x5034

    aput v27, v1, v26

    const/16 v26, -0x70b0

    aput v26, v1, v25

    const/16 v25, -0x71

    aput v25, v1, v24

    const/16 v24, -0x7c9c

    aput v24, v1, v23

    const/16 v23, -0x7d

    aput v23, v1, v22

    const/16 v22, -0x47

    aput v22, v1, v21

    const/16 v21, -0x39

    aput v21, v1, v20

    const/16 v20, 0x7070

    aput v20, v1, v19

    const/16 v19, -0x1790

    aput v19, v1, v18

    const/16 v18, -0x18

    aput v18, v1, v17

    const/16 v17, 0x186e

    aput v17, v1, v16

    const/16 v16, -0x62e8

    aput v16, v1, v15

    const/16 v15, -0x63

    aput v15, v1, v14

    const/16 v14, -0x6f

    aput v14, v1, v13

    const/16 v13, 0x3d0b

    aput v13, v1, v12

    const/16 v12, 0x3c3d

    aput v12, v1, v11

    const/16 v11, 0x503c

    aput v11, v1, v10

    const/16 v10, 0x7650

    aput v10, v1, v9

    const/16 v9, -0x128a

    aput v9, v1, v8

    const/16 v8, -0x13

    aput v8, v1, v7

    const/16 v7, 0x812

    aput v7, v1, v6

    const/16 v6, -0x1ff8

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_14
    array-length v6, v1

    if-lt v3, v6, :cond_19

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_15
    array-length v6, v1

    if-lt v3, v6, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mDeviceObjectId:I
    invoke-static {v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$800(Lcom/sec/android/service/health/contentservice/HealthContentShow;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->closeByApp:Z
    invoke-static {v1, v2}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$902(Lcom/sec/android/service/health/contentservice/HealthContentShow;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->nm:Landroid/app/NotificationManager;
    invoke-static {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$1000(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Landroid/app/NotificationManager;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # getter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->nm:Landroid/app/NotificationManager;
    invoke-static {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$1000(Lcom/sec/android/service/health/contentservice/HealthContentShow;)Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/NotificationManager;->cancelAll()V

    :goto_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/contentservice/HealthContentShow$CloseContentServiceReceiver;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    invoke-virtual {v1}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->finish()V

    :cond_16
    return-void

    :cond_17
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_12

    :cond_18
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_13

    :cond_19
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_14

    :cond_1a
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_15

    :catch_0
    move-exception v1

    goto :goto_16
.end method
