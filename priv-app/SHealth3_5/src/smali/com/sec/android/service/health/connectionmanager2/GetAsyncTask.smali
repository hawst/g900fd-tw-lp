.class Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;
.super Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;
.source "GetAsyncTask.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V
    .locals 0
    .param p1, "connectionManager"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "requestparameters"    # Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/connectionmanager2/AbstractASyncTask;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected varargs doInBackground2([Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;)Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;
    .locals 14
    .param p1, "params"    # [Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;

    .prologue
    .line 39
    const/4 v2, 0x0

    .line 40
    .local v2, "httpResponse":Lorg/apache/http/HttpResponse;
    const/4 v7, 0x0

    .line 41
    .local v7, "resCode":I
    const/4 v12, 0x0

    aget-object v12, p1, v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getSetHeadervalues()Ljava/util/HashMap;

    move-result-object v9

    .line 42
    .local v9, "setHeaderValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const/4 v12, 0x0

    aget-object v12, p1, v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->getUrl()Ljava/lang/String;

    move-result-object v10

    .line 45
    .local v10, "strURL":Ljava/lang/String;
    iget-object v12, p0, Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    if-nez v12, :cond_0

    .line 47
    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v12, -0x1

    const/4 v13, -0x7

    invoke-direct {v8, v12, v13}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 48
    .local v8, "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/4 v12, 0x0

    aget-object v12, p1, v12

    invoke-virtual {v12, v8}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 49
    const/4 v12, 0x0

    aget-object v12, p1, v12

    .line 131
    .end local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :goto_0
    return-object v12

    .line 54
    :cond_0
    :try_start_0
    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v3, v10}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 83
    .local v3, "httpget":Lorg/apache/http/client/methods/HttpGet;
    if-eqz v9, :cond_3

    .line 85
    const/4 v11, 0x0

    .line 86
    .local v11, "value":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 87
    .local v6, "key":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 88
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "key":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 89
    .restart local v6    # "key":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 91
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "value":[Ljava/lang/String;
    check-cast v11, [Ljava/lang/String;

    .line 92
    .restart local v11    # "value":[Ljava/lang/String;
    if-eqz v11, :cond_2

    .line 94
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_2
    array-length v12, v11

    if-ge v5, v12, :cond_1

    .line 95
    aget-object v12, v11, v5

    invoke-virtual {v3, v6, v12}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 99
    .end local v5    # "k":I
    :cond_2
    const/4 v12, 0x0

    invoke-virtual {v3, v6, v12}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 125
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v3    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "key":Ljava/lang/String;
    .end local v11    # "value":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 127
    .local v1, "ex":Ljava/lang/Exception;
    invoke-static {v7, v1}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v8

    .line 130
    .end local v1    # "ex":Ljava/lang/Exception;
    :goto_3
    const/4 v12, 0x0

    aget-object v12, p1, v12

    invoke-virtual {v12, v8}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 131
    const/4 v12, 0x0

    aget-object v12, p1, v12

    goto :goto_0

    .line 105
    .restart local v3    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    :cond_3
    :try_start_1
    iget-object v12, p0, Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v12}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 107
    iget-object v12, p0, Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;->httpClientManager:Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;

    invoke-virtual {v12}, Lcom/sec/android/service/health/connectionmanager2/HttpClientManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;->getHttpContext()Lorg/apache/http/protocol/BasicHttpContext;

    move-result-object v13

    invoke-interface {v12, v3, v13}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 109
    :cond_4
    if-nez v2, :cond_5

    .line 111
    new-instance v8, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v12, -0x1

    const/4 v13, -0x7

    invoke-direct {v8, v12, v13}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    .line 112
    .restart local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const/4 v12, 0x0

    aget-object v12, p1, v12

    invoke-virtual {v12, v8}, Lcom/sec/android/service/health/connectionmanager2/RequestParamameters;->setResponse(Ljava/lang/Object;)V

    .line 113
    const/4 v12, 0x0

    aget-object v12, p1, v12

    goto :goto_0

    .line 115
    .end local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_5
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v12

    const/16 v13, 0xc8

    if-eq v12, v13, :cond_6

    .line 118
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v12

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/sec/android/service/health/connectionmanager2/NetException;->createNetException(ILjava/lang/Exception;)Lcom/sec/android/service/health/connectionmanager2/NetException;

    move-result-object v8

    .restart local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    goto :goto_3

    .line 122
    .end local v8    # "result":Lcom/sec/android/service/health/connectionmanager2/NetException;
    :cond_6
    invoke-virtual {p0, v10, v2}, Lcom/sec/android/service/health/connectionmanager2/GetAsyncTask;->processGetResponse(Ljava/lang/String;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    .local v8, "result":Ljava/lang/Object;
    goto :goto_3
.end method
