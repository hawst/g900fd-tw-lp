.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$GEARFIT;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GEARFIT"
.end annotation


# static fields
.field public static final ACTION_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_WINGTIP_SYNC"

.field public static final COACHING_RESPONSE:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_COACHING_RESPONSE"

.field public static final DATA_SEND:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_DATA_SEND"

.field public static final REQUEST_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_REQUEST_SYNC"

.field public static final SERVICE_STATUS:Ljava/lang/String; = "com.samsung.android.shealth.SERVICE_STATUS"

.field public static final START_SERVICE:Ljava/lang/String; = "com.samsung.android.shealth.START_SERVICE"

.field public static final SYNC_ERROR:Ljava/lang/String; = "com.samsung.android.shealth.SYNC_ERROR"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
