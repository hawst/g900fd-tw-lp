.class Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onDataStarted(I)V
    .locals 3

    const/4 v2, 0x0

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onDataStarted is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, v2, v2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStarted(II)V

    :cond_0
    return-void
.end method

.method public onDataStopped(II)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataStopped - dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v1

    invoke-interface {v0, v1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onDataStopped(II)V

    :cond_0
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$000(Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;)Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    :cond_0
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3

    # getter for: Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStateChanged() - state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public sendRawData([B)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
