.class public Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;
.super Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;

.field private mSkinId:J

.field private mUserDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/database/datahandler/BaseDataAggregator;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    return-void
.end method

.method private getContentValuesSummary()Landroid/content/ContentValues;
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "skin_tone"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;->skinTone:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string/jumbo v1, "skin_type"

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;->skinType:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->addValueKey(Landroid/content/ContentValues;Ljava/lang/String;I)V

    return-object v0
.end method

.method private insertData()V
    .locals 4

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "insertData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->getContentValuesSummary()Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "application__id"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "SkinDataAggregator failed. insertUri is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mSkinId:J

    goto :goto_0
.end method

.method private processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 2

    if-eqz p1, :cond_0

    check-cast p1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;

    :goto_0
    return-void

    :cond_0
    const-string v0, "SkinDataAggregator"

    const-string v1, "Nothing to process"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "addData not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v3, "addData[]"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    array-length v2, p1

    const/4 v3, 0x0

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v3

    invoke-virtual {p0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z

    move-result v3

    if-nez v3, :cond_0

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->processData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->insertData()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "Error occurred while inserting into db"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v2, "Data is out of range"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    move v0, v1

    goto :goto_0

    :cond_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected checkMandatoryFields(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public finalizeAggregation()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "Nothing to be done in finalization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mUserDeviceId:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mContext:Landroid/content/Context;

    invoke-static {v0, p5}, Lcom/sec/android/service/health/cp/database/datahandler/AggregatorUtil;->getApplicationIDFromPid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mAppId:Ljava/lang/String;

    return-void
.end method

.method public isRunning(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mSkinId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mSkinId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/service/health/cp/database/datahandler/SkinDataAggregatorForGearThree;->mSkinId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "matched"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
