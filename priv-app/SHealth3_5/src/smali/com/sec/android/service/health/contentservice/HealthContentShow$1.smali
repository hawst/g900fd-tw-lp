.class Lcom/sec/android/service/health/contentservice/HealthContentShow$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/contentservice/HealthContentShow;->getViewForContentUri(Landroid/net/Uri;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/contentservice/HealthContentShow;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$1;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x59b6cb684dd77777L    # 1.5068480144767916E124

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p2

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0xc

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x3c

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    sget v1, Lcom/sec/android/service/health/R$id;->rb_after_meal:I

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$1;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # setter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mMealTime:Z
    invoke-static {v0, v8}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$102(Lcom/sec/android/service/health/contentservice/HealthContentShow;Z)Z

    :goto_2
    return-void

    :cond_6
    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$1;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # setter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mMealTime:Z
    invoke-static {v0, v3}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$102(Lcom/sec/android/service/health/contentservice/HealthContentShow;Z)Z

    iget-object v0, p0, Lcom/sec/android/service/health/contentservice/HealthContentShow$1;->this$0:Lcom/sec/android/service/health/contentservice/HealthContentShow;

    # setter for: Lcom/sec/android/service/health/contentservice/HealthContentShow;->mealChecked:I
    invoke-static {v0, v8}, Lcom/sec/android/service/health/contentservice/HealthContentShow;->access$202(Lcom/sec/android/service/health/contentservice/HealthContentShow;I)I

    goto :goto_2
.end method
