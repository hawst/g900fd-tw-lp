.class public Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$1;,
        Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;
    }
.end annotation


# static fields
.field public static final HTTP_CODE_INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final HTTP_CODE_INVALID_ACCESS_TOKEN_ERROR:I = 0x191

.field private static final NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final POSITIVE_STATUS_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final STATUS_CODE_INVALID_ACCESS_TOKEN_ERROR:Ljava/lang/String; = "LIC_4104"

.field public static final STATUS_CODE_NO_USER_PROFILE:Ljava/lang/String; = "PRO_5412"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "ACC_1001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "ACF_0403"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "ACT_4302"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_4004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_4005"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_4002"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_4001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_4003"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5000"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5110"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5130"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5230"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5411"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_5412"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5000"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_5000"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SHT_4001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "CMM_1000"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "CMM_1004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "CMM_1007"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "CMM_1009"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "OAS_4104"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SHT_4002"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_4501"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRO_4502"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4713"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4714"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_4001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_4101"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_4102"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_4103"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_4104"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_4105"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5020"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5011"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5131"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5132"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5410"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5411"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5510"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5511"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5230"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5231"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5710"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5711"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "WRK_5712"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5110"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5111"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5112"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5113"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5120"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5121"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5123"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5130"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5210"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5211"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5212"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5213"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5220"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5221"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5223"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5230"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5310"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5311"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5312"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5313"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5320"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5321"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5323"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5330"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5610"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5612"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5613"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5614"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5620"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5621"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5622"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5623"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5630"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5631"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5710"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5711"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5712"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5713"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5720"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5721"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5723"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5730"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5810"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5820"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5822"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5823"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5830"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "BIO_5840"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_4010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_4020"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_4030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_4000"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_4040"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_5010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_5011"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_5012"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_5013"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_5014"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "SLP_5020"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_4010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_4020"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_4030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_4000"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_4040"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_5010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_5011"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_5012"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_5030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "STR_5031"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_4004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_4005"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_4002"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_4001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_4003"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_4007"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_4320"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5011"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5320"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5130"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5131"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5110"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5111"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5410"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5411"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5430"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5431"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5610"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "FOD_5710"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_4004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_4005"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_4002"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_4001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_4003"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_5010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_5011"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "GOL_5030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4613"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4612"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4610"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4712"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4711"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4710"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4813"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4811"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4812"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4810"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_4814"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5610"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5630"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5631"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5710"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5730"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5731"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5810"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5811"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5830"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PLF_5831"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_4004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_4005"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_4002"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_4001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_4003"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_4006"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5010"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5011"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5031"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5110"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5111"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5130"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    const-string v1, "PRF_5131"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isStatusCode(Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;Ljava/lang/String;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$1;->$SwitchMap$com$sec$android$service$health$cp$serversync$network$ServerResponseStatusCodes$Category:[I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes$Category;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->NEGATIVE_STATUS_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/service/health/cp/serversync/network/ServerResponseStatusCodes;->POSITIVE_STATUS_LIST:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
