.class public Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;
.super Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;

# interfaces
.implements Lcom/sec/android/service/health/sensor/handler/usb/jnj/IJnjCommandListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/protocol/JNJProtocol$ProtocolHandler;
    }
.end annotation


# static fields
.field public static AUTO_UPDATE_MODE:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "[HealthSensor]JNJProtocol"

.field private static final PROFILE_NAME:Ljava/lang/String; = "usb"

.field public static final SEND_READ_TIME_FAILURE_RESPONSE:I = 0x2

.field public static final SEND_SET_TIME_FAILURE_RESPONSE:I = 0x1

.field public static final SEND_UNSUPPORTED_COMMAND_RESPONSE:I = 0x3


# instance fields
.field public agentInfoObj:Lcom/sec/android/service/health/sensor/data/SensorInfo;

.field bufferForCommand:[B

.field public curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public deleteAllCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public disconnectCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field private isPingCommandMode:Z

.field mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

.field private mContext:Landroid/content/Context;

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field public mHandler:Landroid/os/Handler;

.field private mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

.field private mUsbDevice:Landroid/hardware/usb/UsbDevice;

.field public measureDataObj:Lcom/sec/android/service/health/sensor/data/MeasurementData;

.field public offCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public onCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field pingReplied:Z

.field public preCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public readDateFormatCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public readMeterInfoCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public readRTCCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public readRecordCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public readSerialNumberCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public readTotalNumberCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public readUnitCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

.field public writeRTCCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->AUTO_UPDATE_MODE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/usb/USBProtocol;-><init>()V

    iput-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, ""

    const-string v2, "OneTouch Ultra Mini"

    const/4 v3, 0x2

    const/16 v4, 0x2714

    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->getProtocolName()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mContext:Landroid/content/Context;

    iput-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    iput-boolean v8, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->isPingCommandMode:Z

    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    iput-boolean v8, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->pingReplied:Z

    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol$1;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol$ProtocolHandler;

    invoke-direct {v1, p0, v7}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol$ProtocolHandler;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/protocol/JNJProtocol$1;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->initJNJProtocolObject()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->onCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    return-void
.end method

.method private createFinishResponse()V
    .locals 3

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->isPingCommandMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    :cond_0
    return-void
.end method

.method private initJNJProtocolObject()V
    .locals 2

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OffCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OffCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->offCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->onCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadMeterInfoCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadMeterInfoCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readMeterInfoCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadSerialNumber;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readSerialNumberCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadDateFormatCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readDateFormatCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadTotalNumberCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readTotalNumberCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRecordCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readRecordCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadUnitCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readUnitCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/ReadRTCCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readRTCCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->writeRTCCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DeleteAllRecords;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->deleteAllCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DisconnectCommand;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/DisconnectCommand;-><init>(Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->disconnectCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    return-void
.end method

.method private sendDeleteCommand()V
    .locals 2

    const-string v0, "[HealthSensor]JNJProtocol"

    const-string v1, "[JNJManager]  sendDeleteCommand()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->deleteAllCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->doAction()V

    return-void
.end method

.method private sendReadRTCCommand()V
    .locals 2

    const-string v0, "[HealthSensor]JNJProtocol"

    const-string v1, "[JNJManager]  sendReadRTCCommand()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readRTCCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->doAction()V

    return-void
.end method

.method private sendWriteRTCCommand(J)V
    .locals 3

    const-string v0, "[HealthSensor]JNJProtocol"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[JNJManager]  sendWriteRTCCommand() time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->writeRTCCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->writeRTCCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/WriteRTCCommand;->setTime(J)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->doAction()V

    return-void
.end method


# virtual methods
.method public commandFinished(Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->preCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->doAction()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->createFinishResponse()V

    goto :goto_0
.end method

.method public deinitialize()V
    .locals 2

    const-string v0, "[HealthSensor]JNJProtocol"

    const-string v1, "JNJProtocol deinitialize called!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public getBaudRate()I
    .locals 1

    const/16 v0, 0x2580

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    return-object v0
.end method

.method public getProtocolName()Ljava/lang/String;
    .locals 1

    const-string v0, "jnj"

    return-object v0
.end method

.method public getResposeHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->setProtocolListener(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;)V

    if-eqz p4, :cond_0

    check-cast p4, Landroid/hardware/usb/UsbDevice;

    iput-object p4, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setConnectedState(Z)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "JNJ_GLUCOSE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, "Johnson&Johnson company"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    :cond_1
    return v2
.end method

.method public notifyRawDataReceived([B)I
    .locals 10

    const/4 v3, -0x1

    const/4 v6, 0x1

    const/4 v1, 0x0

    array-length v0, p1

    if-lez v0, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    invoke-static {p1, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int v4, v3, v0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    aget-byte v2, v2, v6

    aget-byte v5, p1, v1

    sget-object v7, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->S:[B

    aget-byte v7, v7, v1

    if-eq v5, v7, :cond_0

    aget-byte v5, p1, v1

    sget-object v7, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->T:[B

    aget-byte v7, v7, v1

    if-eq v5, v7, :cond_0

    aget-byte v5, p1, v1

    sget-object v7, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->PDT:[B

    aget-byte v7, v7, v1

    if-eq v5, v7, :cond_0

    aget-byte v5, p1, v1

    sget-object v7, Lcom/sec/android/service/health/sensor/handler/usb/infopia/command/InfopiaCommand;->ERS:[B

    aget-byte v7, v7, v1

    if-ne v5, v7, :cond_2

    :cond_0
    move v5, v6

    :goto_0
    if-eqz v5, :cond_4

    :cond_1
    :goto_1
    return v1

    :cond_2
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-direct {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;-><init>()V

    const-string/jumbo v7, "ping_response"

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setCommandId(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->setErrorCode(I)V

    iget-boolean v7, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->pingReplied:Z

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    invoke-interface {v7, v5}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    iput-boolean v6, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->pingReplied:Z

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readSerialNumberCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-interface {v5}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->doAction()V

    :cond_3
    move v5, v1

    goto :goto_0

    :cond_4
    if-lez v2, :cond_5

    const/16 v5, 0x32

    if-le v2, v5, :cond_6

    :cond_5
    iget-object v5, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    const-string v7, "invalid length data."

    const/16 v8, 0x67

    invoke-virtual {v5, v7, v8}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->errorStatus(Ljava/lang/String;I)V

    :cond_6
    move v5, v4

    :goto_2
    if-lez v2, :cond_1

    add-int/lit8 v4, v2, -0x1

    if-gt v4, v5, :cond_1

    if-lez v0, :cond_1

    const-string v0, "[HealthSensor]JNJProtocol"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[JNJProtocol] getCompleteMsg commandLen="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v2, :cond_1

    new-array v7, v2, [B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    invoke-static {v0, v1, v7, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sub-int v0, v5, v2

    add-int/lit8 v4, v0, 0x1

    if-lez v4, :cond_9

    new-array v2, v4, [B

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    sub-int/2addr v5, v4

    add-int/lit8 v5, v5, 0x1

    invoke-static {v0, v5, v2, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v0, v1

    :goto_3
    iget-object v5, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    array-length v5, v5

    if-ge v0, v5, :cond_7

    iget-object v5, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    aput-byte v3, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    invoke-static {v2, v1, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v4, -0x1

    move v2, v0

    :goto_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    aget-byte v5, v0, v6

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    instance-of v0, v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    iget v0, v0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0, v7}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->checkCRC([B)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "[HealthSensor]JNJProtocol"

    const-string v8, "[JNJProtocol] curCmd == onCommand"

    invoke-static {v0, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->onCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;

    invoke-virtual {v0, v7}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/OnCommand;->responseReceived([B)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    iput v6, v0, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->commStateFlag:I

    :cond_8
    :goto_5
    move v0, v4

    move v9, v5

    move v5, v2

    move v2, v9

    goto :goto_2

    :cond_9
    if-nez v4, :cond_b

    move v0, v1

    :goto_6
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    array-length v2, v2

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->bufferForCommand:[B

    aput-byte v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    move v2, v3

    goto :goto_4

    :cond_b
    const-string v0, "[HealthSensor]JNJProtocol"

    const-string v2, "[JNJProtocol] ELSE BREAK"

    invoke-static {v0, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mCommunicationMngr:Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;

    invoke-virtual {v0, v7}, Lcom/sec/android/service/health/sensor/handler/usb/CommunicationManager;->parsingCommnadData([B)V

    goto :goto_5
.end method

.method public notifyStart()I
    .locals 3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->isPingCommandMode:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->readDateFormatCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->doAction()V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStarted(I)V

    return v2
.end method

.method public notifyStop()I
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mShealthProtocolListener:Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;->onDataStopped(II)V

    return v2
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
    .locals 4

    const-string/jumbo v0, "send_ping"

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "command_request_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->isPingCommandMode:Z

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->sendPing()V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CMD_GET_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->sendReadRTCCommand()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CMD_SET_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PARAM_TIME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->sendWriteRTCCommand(J)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CMD_CLEAR_DATA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->sendDeleteCommand()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const/16 v0, 0x3ea

    goto :goto_1
.end method

.method public sendPing()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->onCommand:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->isPingCommandMode:Z

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->curCmd:Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;

    invoke-interface {v0}, Lcom/sec/android/service/health/sensor/handler/usb/jnj/command/JNJCommand;->doAction()V

    return-void
.end method

.method public setDeviceSerialNumber(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/JNJProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
