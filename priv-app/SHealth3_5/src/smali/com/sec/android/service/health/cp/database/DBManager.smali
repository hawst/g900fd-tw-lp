.class public Lcom/sec/android/service/health/cp/database/DBManager;
.super Ljava/lang/Object;


# static fields
.field public static final ENCRYPTED_KEY:Ljava/lang/String; = "ENCRYPTED_KEY"

.field private static IS_DEVELOPER_MODE:Z = false

.field private static final IS_SECURE:Z = true

.field public static final KM_METHOD:Ljava/lang/String; = "KEY_MANAGER"

.field public static final NA:Ljava/lang/String; = "NA"

.field private static final SS_METHOD:Ljava/lang/String; = "SECURE_SUPPORTED"

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/service/health/cp/database/DBManager;

.field private static mBackupDatabaseHelper:Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;

.field private static mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;


# instance fields
.field private mBackupDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

.field private mContext:Landroid/content/Context;

.field private mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

.field private mIsInitialized:Z

.field private mKey:Ljava/lang/String;

.field private mPrefKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/service/health/cp/database/DBManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/service/health/cp/database/DBManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/database/DBManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->instance:Lcom/sec/android/service/health/cp/database/DBManager;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/cp/database/DBManager;->IS_DEVELOPER_MODE:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mIsInitialized:Z

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v0, "NA"

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mPrefKey:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/cp/database/DBManager;
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->instance:Lcom/sec/android/service/health/cp/database/DBManager;

    return-object v0
.end method

.method private getKeyFromSecureStorage(Ljava/lang/String;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, p1, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "value_of_password"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There\'s no available key in the bundle : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    :cond_2
    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "connection has been broken! : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "DUMMY EXCEPTION"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private renameFile()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "platform.db.back"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "platform.db"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, " !platformDbBackupFile.renameTo(getContext().getDatabasePath(platform_backup.db))"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "platform_backup.db"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "rename to platform_backup.db failed"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "db rename failed : platform.db.back to platform_backup.db"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "if platformDbBackupFile.exists() && !platformDbFile.exists()"

    invoke-static {v2}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "rename to platform.db failed"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "db rename failed : platform.db.back to platform.db"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private replaceBackupDB()Z
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "replaceBackupDB()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "[DBManager] replaceBackupDB()"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "platform.db.old"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "platform.db"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "platform_new.db"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "platform.db"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "[DBManager] Renaming .old file to .db failed"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "[DBManager] Renaming .old file to .db success"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "replaceBackupDB() old platform db"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "platform.db.old"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v1, "[DBManager] Deleting .old file failed."

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "[DBManager] Deleting .old file success."

    invoke-static {v2}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_3
    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "replaceBackupDB() Rename platform.db to platform.db.old"

    invoke-static {v2, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "platform.db"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "platform.db.old"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v1, "[DBManager] Renaming .db file to .old failed."

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "[DBManager] Renaming .db file to .old success."

    invoke-static {v2}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    :cond_5
    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "replaceBackupDB() Rename platform_new.db to platform.db"

    invoke-static {v2, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "platform.db"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "Renaming .new file to .db failed, will rename .old to .db"

    invoke-static {v2}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "platform.db"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "Is_Restore_Completed"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public deleteBackupDatabase()V
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "deleteBackupDatabase(): delete temp db if restore fails"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/DatabaseUtility;->clearBackupDatabase(ZLcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;)V

    return-void
.end method

.method public deleteDatabase()Z
    .locals 1

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;->deleteDatabase()Z

    move-result v0

    return v0
.end method

.method public deleteDatabase(Landroid/content/Context;)Z
    .locals 2

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "deleteDatabase(Context context) : delete db"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;->deleteDatabase()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mIsInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid State. Check CP initialization"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    :try_start_2
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "Try to get pass from TrustZone"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v2, "get_secure_password"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string/jumbo v1, "value_of_password"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "value_of_password"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDatabaseHelper:Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    :cond_1
    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v2, "Try Opening the Db..(BACKUP_DB)"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDatabaseHelper:Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :try_start_3
    new-instance v1, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {v1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    const-string v1, "PRAGMA foreign_keys=ON;"

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->execSQL(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_3
    :try_start_4
    const-string v0, "ThinCPConstants.KEY_GET_SECURE_PASSWORD failed"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_0
    move-exception v0

    :try_start_5
    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not able to create/open restore db. throw new RuntimeException;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ThinCPConstants.KEY_GET_SECURE_PASSWORD failed"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_4
    :try_start_6
    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "Try to get pass from keyManager (BACKUP_DB)"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDBKey()[B

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "KeyManager.getDBKey is returning null (BACKUP_DB)"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "KeyManager.getDBKey is returning null"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Exception;

    const-string v1, " failed KEY MANAGER"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public declared-synchronized getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    .locals 7

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mIsInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid State. Check CP initialization"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->renameFile()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "ENCRYPTED_KEY"

    const-string v3, "NA"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mPrefKey:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Key retrieved from sharedpreferneces is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mPrefKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v1, "SECURE_SUPPORTED"

    const-string v0, "get_secure_password"

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getKeyFromSecureStorage(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "SECURE_SUPPORTED"

    iput-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v3, "Try Opening the Db.."

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-virtual {v2, v0}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mPrefKey:Ljava/lang/String;

    const-string v3, "NA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ENCRYPTED_KEY"

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "key saved to sharedpreferences is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_1
    :try_start_3
    const-string v1, "PRAGMA foreign_keys=ON;"

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {v1, v0}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mDb:Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_3
    :try_start_4
    const-string v2, "KEY_MANAGER"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "Try to get pass using keyManager"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDBKey()[B

    move-result-object v0

    const-string v1, "KEY_MANAGER"

    iput-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;

    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v3, "Try Opening the Db.."

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    move-object v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_7
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->renameFile()V

    const-string v1, "get_secure_password"

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/cp/database/DBManager;->getKeyFromSecureStorage(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->replaceKeyFromSecureStorage(Ljava/lang/String;)Ljava/lang/String;

    sget-object v3, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-virtual {v3, v1}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "ENCRYPTED_KEY"

    const-string v5, "KEY_MANAGER"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-object v0, v1

    move-object v1, v2

    goto/16 :goto_0

    :catch_1
    move-exception v1

    :try_start_8
    new-instance v3, Ljava/lang/Exception;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to replace, pref key was saved as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mPrefKey:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " [ Exception while replacing : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " ], [ Exception : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_2
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_9
    sget-object v3, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    const-string v2, "Exception for getting key from TZ or opening db with TZ key"

    :goto_3
    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->renameFile()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "platform.db"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_6

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v2, "Only generate new key"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v1, "SECURE_SUPPORTED"

    const-string v0, "create_secure_password"

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getKeyFromSecureStorage(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "SECURE_SUPPORTED"

    iput-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :goto_4
    :try_start_b
    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v3, "Try to open db"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :try_start_c
    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-virtual {v2, v0}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v0

    :try_start_d
    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mPrefKey:Ljava/lang/String;

    const-string v2, "NA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ENCRYPTED_KEY"

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v1, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key saved to sharedpreferences is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result-object v2

    goto/16 :goto_3

    :cond_5
    :try_start_e
    const-string v1, "KEY_MANAGER"

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v2, "Try to get pass using keyManager"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDBKey()[B

    move-result-object v0

    const-string v2, "KEY_MANAGER"

    iput-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_4

    :catch_3
    move-exception v0

    :try_start_f
    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createTrustZoneSecurePasswd makes error -- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "Create for SS password failed for KEY_CREATE_SECURE_PASSWORD"

    invoke-static {v2}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth"

    const-string v4, "ERR_USTATUS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FAIL9_1:Create key failed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", msg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Create key failed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", msg : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    sget-object v2, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v3, "Try to open with AES key!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :try_start_10
    new-instance v2, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v2}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    iget-object v2, v2, Lcom/sec/android/service/health/cp/common/AESEncryption;->str:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    sget-object v3, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-virtual {v3, v2}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "is_aes_key"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v3, "AES_KEY"

    iput-object v3, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mKey:Ljava/lang/String;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    move-object v0, v2

    goto/16 :goto_4

    :catch_4
    move-exception v2

    :try_start_11
    const-string v2, "DB is exist, but getting password from SS is failed"

    invoke-static {v2}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->renameFile()V

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth"

    const-string v4, "ERR_USTATUS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FAIL10_1:Failed to open db or key is not availalbe : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", msg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to open db or key is not availalbe : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", msg : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_5
    move-exception v0

    iget-object v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth"

    const-string v4, "ERR_USTATUS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FAIL12_1:Not able to open with db key given by "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", msg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not able to open with db key given by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", msg : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_6
    move-exception v0

    goto/16 :goto_2
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialize - mIsInitialized:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mIsInitialized:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mIsInitialized:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->initialization(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getInstance()Lcom/sec/android/service/health/cp/serversync/common/SharedValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/serversync/common/SharedValue;->getSyncPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "Is_Restore_Completed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "Is_Restore_Completed flow"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/database/DBManager;->replaceBackupDB()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/service/health/cp/database/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->mDatabaseHelper:Lcom/sec/android/service/health/cp/database/DatabaseHelper;

    new-instance v0, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/health/cp/database/DBManager;->mBackupDatabaseHelper:Lcom/sec/android/service/health/cp/database/BackupDatabaseHelper;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mIsInitialized:Z

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/cp/database/DBManager;->mContext:Landroid/content/Context;

    return-void
.end method
