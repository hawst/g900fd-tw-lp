.class public Lcom/sec/android/service/health/cp/common/DBTables$WeightTable;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WeightTable"
.end annotation


# static fields
.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_BMI:Ljava/lang/String; = "bmi"

.field public static final KEY_BMR:Ljava/lang/String; = "bmr"

.field public static final KEY_BODY_FAT:Ljava/lang/String; = "body_fat"

.field public static final KEY_BODY_YEAR:Ljava/lang/String; = "body_year"

.field public static final KEY_COMMENT:Ljava/lang/String; = "comment"

.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_HEIGHT:Ljava/lang/String; = "height"

.field public static final KEY_MUSCLE:Ljava/lang/String; = "skeletal_muscle"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final KEY_PERSON_ID:Ljava/lang/String; = "person_id"

.field public static final KEY_SENSOR_ID:Ljava/lang/String; = "sensor_id"

.field public static final KEY_VALUE:Ljava/lang/String; = "value"

.field public static final KEY_VISC_FAT:Ljava/lang/String; = "visceral_fat"

.field public static final TABLE_NAME:Ljava/lang/String; = "weight"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_weight_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_weight_trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
