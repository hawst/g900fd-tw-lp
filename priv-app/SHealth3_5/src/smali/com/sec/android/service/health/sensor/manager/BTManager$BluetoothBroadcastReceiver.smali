.class Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/manager/BTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/manager/BTManager;Lcom/sec/android/service/health/sensor/manager/BTManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;-><init>(Lcom/sec/android/service/health/sensor/manager/BTManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BTManager$BluetoothBroadcastReceiver;->this$0:Lcom/sec/android/service/health/sensor/manager/BTManager;

    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/manager/BTManager;->bondStateChangedEvent(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
