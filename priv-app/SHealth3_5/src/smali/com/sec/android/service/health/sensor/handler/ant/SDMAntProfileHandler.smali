.class public Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;
.super Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$3;
    }
.end annotation


# static fields
.field static final SENSOR_STATUS_Battery:I = 0x2

.field static final SENSOR_STATUS_Health:I = 0x3

.field static final SENSOR_STATUS_Location:I = 0x1

.field static final SENSOR_STATUS_UseState:I = 0x4

.field private static final TAG:Ljava/lang/String; = "[HealthSensor]SDMSensor"


# instance fields
.field private mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

.field private oldTotalStep:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/AntProfileHandler;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->oldTotalStep:J

    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-void
.end method

.method private ConvertStatus(II)Ljava/lang/String;
    .locals 6

    const/4 v1, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-array v1, v1, [Ljava/lang/String;

    const-string v0, "LACES"

    aput-object v0, v1, v2

    const-string v0, "MIDSOLE"

    aput-object v0, v1, v3

    const-string v0, "OTHER"

    aput-object v0, v1, v4

    const-string v0, "ANKLE"

    aput-object v0, v1, v5

    array-length v0, v1

    if-lt p2, v0, :cond_0

    aget-object v0, v1, v2

    const-string v2, "[HealthSensor]SDMSensor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bufferoverflow: length:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/index:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    aget-object v0, v1, p2

    goto :goto_0

    :pswitch_1
    new-array v1, v1, [Ljava/lang/String;

    const-string v0, "NEW"

    aput-object v0, v1, v2

    const-string v0, "GOOD"

    aput-object v0, v1, v3

    const-string v0, "OK"

    aput-object v0, v1, v4

    const-string v0, "LOW"

    aput-object v0, v1, v5

    array-length v0, v1

    if-lt p2, v0, :cond_1

    aget-object v0, v1, v2

    const-string v2, "[HealthSensor]SDMSensor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bufferoverflow: length:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/index:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    aget-object v0, v1, p2

    goto :goto_0

    :pswitch_2
    new-array v1, v5, [Ljava/lang/String;

    const-string v0, "OK"

    aput-object v0, v1, v2

    const-string v0, "ERROR"

    aput-object v0, v1, v3

    const-string v0, "WARNING"

    aput-object v0, v1, v4

    array-length v0, v1

    if-lt p2, v0, :cond_2

    aget-object v0, v1, v2

    const-string v2, "[HealthSensor]SDMSensor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bufferoverflow: length:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/index:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    aget-object v0, v1, p2

    goto/16 :goto_0

    :pswitch_3
    new-array v1, v4, [Ljava/lang/String;

    const-string v0, "INACTIVE"

    aput-object v0, v1, v2

    const-string v0, "ACTIVE"

    aput-object v0, v1, v3

    array-length v0, v1

    if-lt p2, v0, :cond_3

    aget-object v0, v1, v2

    const-string v2, "[HealthSensor]SDMSensor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bufferoverflow: length:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/index:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    aget-object v0, v1, p2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic access$002(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->state:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mAntDeviceID:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mPccServiceHandler:Lcom/sec/android/service/health/sensor/handler/AntProfileHandler$PCCServiceHandler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->state:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mAntPluginPcc:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->oldTotalStep:J

    return-wide v0
.end method

.method static synthetic access$702(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->oldTotalStep:J

    return-wide p1
.end method


# virtual methods
.method public getAntDeviceType()I
    .locals 1

    const/16 v0, 0x7c

    return v0
.end method

.method public getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mCapability:Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;

    return-object v0
.end method

.method public getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubState()I
    .locals 1

    const/16 v0, 0x7d2

    return v0
.end method

.method protected handleMessageSub(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method public isProtocolSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected requestAccess()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mAntDeviceID:I

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;

    invoke-direct {v3, p0}, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;)V

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ant/SDMAntProfileHandler;->mDeviceStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusStrideSdmPcc;->requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    return-void
.end method

.method public setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
    .locals 0

    return-void
.end method
