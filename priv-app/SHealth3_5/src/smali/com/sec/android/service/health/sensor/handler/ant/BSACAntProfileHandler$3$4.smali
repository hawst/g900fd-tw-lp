.class Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc$IManufacturerAndSerialReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->subscribeToEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewManufacturerAndSerial(JLjava/util/EnumSet;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;II)V"
        }
    .end annotation

    const/16 v3, 0x7d8

    const-string v0, "[HealthSensor]BSACSensor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewManufacturerAndSerial - manufacturerID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", serialNumber: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->LogV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->manufacture:I
    invoke-static {v0, p4}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->access$1702(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;I)I

    const/4 v0, -0x1

    invoke-static {p4, v0}, Lcom/sec/android/service/health/sensor/handler/ant/AntPlusUtil;->getProductString(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v1, v1, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->access$1800(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setManufacturer(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->access$1900(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->access$2000(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->access$2100(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->access$2200(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->notifyStateChanged(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->access$2300(Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setSerialNumber(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3$4;->this$1:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler$3;->this$0:Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/service/health/sensor/handler/ant/BSACAntProfileHandler;->notifyStateChanged(I)V

    goto :goto_0
.end method
