.class public Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants$BundleKey;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/wearable/SWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BundleKey"
.end annotation


# static fields
.field public static final AVERAGE_SPEED:Ljava/lang/String; = "AVERAGE_SPEED"

.field public static final HEARTRATE_RAW_DATA:Ljava/lang/String; = "HEARTRATE_RAW_DATA"

.field public static final MAX_ALTITUDE:Ljava/lang/String; = "MAX_ALTITUDE"

.field public static final MAX_HEARTRATE:Ljava/lang/String; = "MAX_HEARTRATE"

.field public static final MAX_SPEED:Ljava/lang/String; = "MAX_SPEED"

.field public static final MIN_ALTITUDE:Ljava/lang/String; = "MIN_ALTITUDE"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
