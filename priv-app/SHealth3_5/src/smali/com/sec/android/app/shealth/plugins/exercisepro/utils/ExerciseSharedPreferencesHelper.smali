.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;
.super Ljava/lang/Object;
.source "ExerciseSharedPreferencesHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper$ExerciseSharedPreferencesKeys;
    }
.end annotation


# static fields
.field private static DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

.field static unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    .line 24
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 528
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    .line 543
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper$2;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555
    return-void
.end method

.method public static ClearRealtimeAllGoalType()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 109
    const/16 v0, 0x4653

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeActivityGoalType(II)V

    .line 110
    const/16 v0, 0x4652

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeActivityGoalType(II)V

    .line 111
    const/16 v0, 0x4654

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeActivityGoalType(II)V

    .line 112
    const/16 v0, 0x4655

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeActivityGoalType(II)V

    .line 113
    return-void
.end method

.method public static ClearRealtimeAllGoalValue()V
    .locals 2

    .prologue
    const/16 v1, 0x1e

    .line 116
    const v0, 0x3dcccccd    # 0.1f

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeDistanceGoalValue(F)V

    .line 117
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeTimeGoalValue(I)V

    .line 118
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeCaloriesGoalValue(I)V

    .line 119
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeTrainingLevel(I)V

    .line 120
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeTrainingTimeGoalValue(I)V

    .line 121
    return-void
.end method

.method public static getBackupRealtimeGoalType()I
    .locals 4

    .prologue
    .line 281
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_backup_realtime_goal_type"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 282
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    return v0
.end method

.method public static getBackupRealtimeGoalValue()I
    .locals 4

    .prologue
    .line 287
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_backup_realtime_goal_value"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 288
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    return v0
.end method

.method public static getBackupRealtimeTrainingLevel()I
    .locals 4

    .prologue
    .line 298
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_backup_realtime_training_goal_value"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 299
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    return v0
.end method

.method public static getChartFlag()Ljava/lang/String;
    .locals 4

    .prologue
    .line 484
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_chartFlagButton"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 485
    .local v0, "val":Ljava/lang/String;
    return-object v0
.end method

.method public static getDistanceUnitFromSharedPreferences(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 506
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-nez v1, :cond_0

    .line 507
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 508
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    .line 509
    .local v0, "unit":Ljava/lang/String;
    if-eqz v0, :cond_1

    .end local v0    # "unit":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "unit":Ljava/lang/String;
    :cond_1
    const-string v0, "km"

    goto :goto_0
.end method

.method public static getEquipmentType()I
    .locals 4

    .prologue
    .line 62
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_equipment_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 64
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getActivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    return v0
.end method

.method public static getFirstbeatAscrActivated()Z
    .locals 3

    .prologue
    .line 440
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_firstbeat_ascr_activated"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getFirstbeatTrainingGoal()I
    .locals 4

    .prologue
    .line 316
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string v2, "firstbeat_training_goal"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 318
    .local v0, "val":I
    return v0
.end method

.method public static getLocationPopupWasShown()Z
    .locals 3

    .prologue
    .line 497
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_location_popup_flag"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getPreviousGoalSelectId()J
    .locals 4

    .prologue
    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_key_beat_my_previous_record_exid"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getRealTimeAudioGuideItemIndex()I
    .locals 3

    .prologue
    .line 416
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_audio_guide_on"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealTimeGymGuidePopupWasShown()Z
    .locals 3

    .prologue
    .line 420
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_gym_guide_popup_was_shown"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getRealTimeManualyMaxHR(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 346
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 347
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 348
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 349
    .local v1, "birthDate":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getDay()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v5

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v6

    add-int/lit16 v6, v6, 0x76c

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/firstbeat/utils/Utils;->getAgeValue(III)I

    move-result v0

    .line 350
    .local v0, "age":I
    const-wide v4, 0x406a400000000000L    # 210.0

    const-wide v6, 0x3fe4cccccccccccdL    # 0.65

    int-to-double v8, v0

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-int v2, v4

    .line 351
    .local v2, "manual_max_hr":I
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v5, "manualy_max_hr"

    invoke-virtual {v4, v5, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v4

    return v4
.end method

.method public static getRealtimeActivityGoalType(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "activityType"    # I

    .prologue
    .line 143
    const/4 v0, 0x1

    .line 144
    .local v0, "value":I
    packed-switch p1, :pswitch_data_0

    .line 158
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 159
    const/4 v0, 0x1

    .line 161
    :cond_0
    return v0

    .line 146
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeRunningGoalType()I

    move-result v0

    .line 147
    goto :goto_0

    .line 149
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeWalkingGoalType()I

    move-result v0

    .line 150
    goto :goto_0

    .line 152
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeCyclingGoalType()I

    move-result v0

    .line 153
    goto :goto_0

    .line 155
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeHikingGoalType()I

    move-result v0

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getRealtimeActivityType()I
    .locals 4

    .prologue
    .line 49
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_activity_type"

    const/16 v3, 0x4653

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 51
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getActivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return v0
.end method

.method public static getRealtimeBeatMyPreviousRecordGoalValue()I
    .locals 4

    .prologue
    .line 304
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_beat_my_previous_record_value"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 305
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "saveRealtimeBeatMyPreviousRecordGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    return v0
.end method

.method private static getRealtimeCaloriesGoalValue()I
    .locals 4

    .prologue
    .line 245
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_calories_value"

    const/16 v3, 0x12c

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 246
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCaloriesGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    return v0
.end method

.method public static getRealtimeCyclingGoalType()I
    .locals 4

    .prologue
    .line 177
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_cycling_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 178
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    return v0
.end method

.method public static getRealtimeDispDataBackupTypeForHRM()I
    .locals 3

    .prologue
    .line 384
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_disp_data_backup"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealtimeDispDataMap()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 403
    .local v0, "intAry":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 404
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pro_realtime_disp_data_map"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 406
    .local v2, "val":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 408
    .end local v2    # "val":I
    :cond_0
    return-object v0
.end method

.method public static getRealtimeDispDataStatus()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 375
    .local v0, "intAry":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 376
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pro_realtime_disp_data_status"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 378
    .local v2, "val":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 380
    .end local v2    # "val":I
    :cond_0
    return-object v0
.end method

.method private static getRealtimeDistanceGoalValue()I
    .locals 4

    .prologue
    .line 223
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_distance_value"

    const/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 224
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDistanceGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    return v0
.end method

.method public static getRealtimeExerciseId()J
    .locals 4

    .prologue
    .line 448
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_exercise_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getRealtimeFrequencyOfAudioGuide()I
    .locals 3

    .prologue
    .line 456
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_frequency_of_audio_guide"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealtimeGoalType()I
    .locals 4

    .prologue
    .line 102
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_goal_type"

    const/16 v3, 0x4653

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 104
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    return v0
.end method

.method public static getRealtimeGoalValue()I
    .locals 2

    .prologue
    .line 189
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 190
    .local v0, "type":I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeGoalValue(I)I

    move-result v1

    return v1
.end method

.method public static getRealtimeGoalValue(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 194
    const/4 v0, -0x1

    .line 195
    .local v0, "val":I
    packed-switch p0, :pswitch_data_0

    .line 209
    .end local v0    # "val":I
    :goto_0
    return v0

    .line 197
    .restart local v0    # "val":I
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeDistanceGoalValue()I

    move-result v0

    goto :goto_0

    .line 199
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeTimeGoalValue()I

    move-result v0

    goto :goto_0

    .line 201
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeCaloriesGoalValue()I

    move-result v0

    goto :goto_0

    .line 203
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeTrainingTimeGoalValue()I

    move-result v0

    goto :goto_0

    .line 205
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->getRealtimeBeatMyPreviousRecordGoalValue()I

    move-result v0

    goto :goto_0

    .line 195
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getRealtimeHikingGoalType()I
    .locals 4

    .prologue
    .line 183
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_hiking_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 184
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    return v0
.end method

.method public static getRealtimeMapZoomLevel()F
    .locals 4

    .prologue
    .line 435
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_map_zoom_level"

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 436
    .local v0, "zoom":F
    return v0
.end method

.method public static getRealtimeMode()I
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealtimeRunningGoalType()I
    .locals 4

    .prologue
    .line 165
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_running_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 166
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return v0
.end method

.method private static getRealtimeTimeGoalValue()I
    .locals 4

    .prologue
    .line 234
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_time_value"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 235
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTimeGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    return v0
.end method

.method public static getRealtimeTrainingLevel()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 256
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_training_level"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 258
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrainingLevel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    if-le v0, v4, :cond_0

    .line 260
    const/4 v0, 0x2

    .line 261
    :cond_0
    return v0
.end method

.method public static getRealtimeTrainingTimeGoalValue()I
    .locals 4

    .prologue
    .line 310
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_training_time_value"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 311
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRealtimeTrainingTimeGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    return v0
.end method

.method public static getRealtimeTypeOfFeedBack()J
    .locals 4

    .prologue
    .line 464
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_type_of_feedback"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getRealtimeWalkingGoalType()I
    .locals 4

    .prologue
    .line 171
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_walking_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 172
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    return v0
.end method

.method public static isNeedMaxHeartrateConfirm()Z
    .locals 3

    .prologue
    .line 489
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_need_max_hr_confirm"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isRealTimeMaxHRAutoUpdate()Z
    .locals 3

    .prologue
    .line 342
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "max_hr_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static loadUnitsFromSharedPreferences(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 513
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-nez v0, :cond_0

    .line 514
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 515
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoHeightUnit:Ljava/lang/String;

    .line 517
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v0

    :goto_1
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoWeightUnit:Ljava/lang/String;

    .line 519
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    :goto_2
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoTemperatureUnit:Ljava/lang/String;

    .line 521
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    :goto_3
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    .line 523
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v0

    :goto_4
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoGlucoseUnit:Ljava/lang/String;

    .line 525
    return-void

    .line 515
    :cond_1
    const-string v0, "inch"

    goto :goto_0

    .line 517
    :cond_2
    const-string v0, "lb"

    goto :goto_1

    .line 519
    :cond_3
    const-string v0, "F"

    goto :goto_2

    .line 521
    :cond_4
    const-string v0, "km"

    goto :goto_3

    .line 523
    :cond_5
    const-string/jumbo v0, "mg/dL"

    goto :goto_4
.end method

.method public static saveBackupRealtimeGoalType(II)V
    .locals 3
    .param p0, "type"    # I
    .param p1, "val"    # I

    .prologue
    .line 275
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveBackupRealtimeGoalType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_backup_realtime_goal_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 277
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_backup_realtime_goal_value"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 278
    return-void
.end method

.method public static saveBackupRealtimeTrainingLevel(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 293
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveBackuprainingGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_backup_realtime_training_goal_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 295
    return-void
.end method

.method public static saveChartFlag(Ljava/lang/String;)V
    .locals 2
    .param p0, "val"    # Ljava/lang/String;

    .prologue
    .line 480
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_chartFlagButton"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 481
    return-void
.end method

.method public static saveEquipmentType(I)V
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveActivityType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_equipment_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method public static saveLocationPopupWasShown(Z)V
    .locals 3
    .param p0, "val"    # Z

    .prologue
    .line 501
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_location_popup_flag"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 502
    return-void
.end method

.method public static savePreviousGoalSelectId(J)V
    .locals 3
    .param p0, "ex_id"    # J

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_key_beat_my_previous_record_exid"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public static saveRealtimeActivityGoalType(II)V
    .locals 3
    .param p0, "activityType"    # I
    .param p1, "typeValue"    # I

    .prologue
    .line 124
    packed-switch p0, :pswitch_data_0

    .line 127
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_running_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 137
    :goto_0
    return-void

    .line 130
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_walking_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_cycling_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_hiking_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static saveRealtimeActivityType(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveActivityType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_activity_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public static saveRealtimeBeatMyPreviousRecordGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 270
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveRealtimeBeatMyPreviousRecordGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_beat_my_previous_record_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 272
    return-void
.end method

.method private static saveRealtimeCaloriesGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveCaloriesGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_calories_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 242
    return-void
.end method

.method public static saveRealtimeDispDataBackupTypeForHRM(I)V
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 388
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_disp_data_backup"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 389
    return-void
.end method

.method public static saveRealtimeDispDataMap(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 392
    .local p0, "val":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez p0, :cond_1

    .line 393
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDispDataMap Failed! length not 2 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p0, :cond_0

    const-string/jumbo v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :goto_1
    return-void

    .line 393
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 397
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDispDataMap : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_disp_data_map"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 359
    .local p0, "val":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 360
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDispDataStatus Failed! length not 4 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p0, :cond_1

    const-string/jumbo v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :goto_1
    return-void

    .line 360
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 364
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDispDataStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_disp_data_status"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static saveRealtimeDistanceGoalValue(F)V
    .locals 3
    .param p0, "val"    # F

    .prologue
    .line 213
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDistanceGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_distance_value"

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 215
    return-void
.end method

.method private static saveRealtimeDistanceGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDistanceGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_distance_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method public static saveRealtimeGoalType(II)V
    .locals 3
    .param p0, "type"    # I
    .param p1, "val"    # I

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveGoalType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_goal_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 71
    packed-switch p0, :pswitch_data_0

    .line 90
    :goto_0
    return-void

    .line 73
    :pswitch_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeDistanceGoalValue(I)V

    goto :goto_0

    .line 76
    :pswitch_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeTimeGoalValue(I)V

    goto :goto_0

    .line 79
    :pswitch_2
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeCaloriesGoalValue(I)V

    goto :goto_0

    .line 82
    :pswitch_3
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeTrainingTimeGoalValue(I)V

    goto :goto_0

    .line 85
    :pswitch_4
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->saveRealtimeBeatMyPreviousRecordGoalValue(I)V

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static saveRealtimeMapZoomLevel(F)V
    .locals 3
    .param p0, "zoom"    # F

    .prologue
    .line 431
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_map_zoom_level"

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 432
    return-void
.end method

.method public static saveRealtimeMode(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_mode"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method private static saveRealtimeTimeGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveTimeGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_time_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 231
    return-void
.end method

.method public static saveRealtimeTrainingLevel(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveTrainingLevel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_training_level"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 253
    return-void
.end method

.method public static saveRealtimeTrainingTimeGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 265
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveRealtimeTrainingTimeGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_training_time_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 267
    return-void
.end method

.method public static setFirstbeatAscrActivated(Z)V
    .locals 3
    .param p0, "activated"    # Z

    .prologue
    .line 444
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_firstbeat_ascr_activated"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 445
    return-void
.end method

.method public static setFirstbeatTrainingGoal(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 322
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string v1, "firstbeat_training_goal"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 323
    return-void
.end method

.method public static setIsRealTimeMaxHRAutoUpdate(Landroid/content/Context;Z)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "val"    # Z

    .prologue
    const/4 v6, 0x0

    .line 326
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v5, "max_hr_mode"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 327
    .local v2, "old_val":Z
    if-eq v2, p1, :cond_0

    .line 328
    if-eqz v2, :cond_1

    .line 329
    invoke-static {v6}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->setRealTimeMaxHR(I)V

    .line 338
    :cond_0
    :goto_0
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v5, "max_hr_mode"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 339
    return-void

    .line 331
    :cond_1
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 332
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 333
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 334
    .local v1, "birthDate":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getDay()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v5

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v6

    add-int/lit16 v6, v6, 0x76c

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/firstbeat/utils/Utils;->getAgeValue(III)I

    move-result v0

    .line 335
    .local v0, "age":I
    const-wide v4, 0x406a400000000000L    # 210.0

    const-wide v6, 0x3fe4cccccccccccdL    # 0.65

    int-to-double v8, v0

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->setRealTimeManualyMaxHR(I)V

    goto :goto_0
.end method

.method public static setNeedMaxHeartrateConfirm(Z)V
    .locals 3
    .param p0, "isNeed"    # Z

    .prologue
    .line 493
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_need_max_hr_confirm"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 494
    return-void
.end method

.method public static setRealTimeAudioGuideIndex(I)V
    .locals 3
    .param p0, "index"    # I

    .prologue
    .line 412
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_audio_guide_on"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 413
    return-void
.end method

.method public static setRealTimeGymGuidePopupWasShown(Z)V
    .locals 3
    .param p0, "guidePopupWasShown"    # Z

    .prologue
    .line 426
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_gym_guide_popup_was_shown"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 428
    return-void
.end method

.method public static setRealTimeManualyMaxHR(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 355
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "manualy_max_hr"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 356
    return-void
.end method

.method public static setRealtimeExerciseId(J)V
    .locals 3
    .param p0, "id"    # J

    .prologue
    .line 452
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_exercise_id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 453
    return-void
.end method

.method public static setRealtimeFrequencyOfAudioGuide(I)V
    .locals 3
    .param p0, "id"    # I

    .prologue
    .line 460
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_frequency_of_audio_guide"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 461
    return-void
.end method

.method public static setRealtimeTypeOfFeedBack(J)V
    .locals 3
    .param p0, "type"    # J

    .prologue
    .line 468
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_type_of_feedback"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseSharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 469
    return-void
.end method
