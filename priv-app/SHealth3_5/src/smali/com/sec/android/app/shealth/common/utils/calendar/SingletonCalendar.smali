.class public Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
.super Ljava/lang/Object;
.source "SingletonCalendar.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final WEEK_STARTS_FROM_SUNDAY:B = 0x1t

.field public static infoWeekFormat:I

.field private static instance:Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;


# instance fields
.field private calendarInstance:Ljava/util/GregorianCalendar;

.field private savedTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->TAG:Ljava/lang/String;

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->instance:Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 23
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->infoWeekFormat:I

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->savedTime:J

    .line 28
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 2

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->instance:Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->instance:Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->setUserFirstDayOfWeek()V

    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->instance:Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    return-object v0
.end method

.method public static isWeekStartsFromSunday()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 328
    sget v1, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->infoWeekFormat:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setInstanceTimeZone(Ljava/util/TimeZone;)V
    .locals 1
    .param p0, "tz"    # Ljava/util/TimeZone;

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->instance:Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 50
    return-void
.end method

.method private setTimeZone(Ljava/util/TimeZone;)V
    .locals 1
    .param p1, "tz"    # Ljava/util/TimeZone;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 100
    return-void
.end method


# virtual methods
.method public add(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 1
    .param p1, "field"    # I
    .param p2, "value"    # I

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->add(II)V

    .line 246
    return-object p0
.end method

.method public get(I)I
    .locals 1
    .param p1, "field"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getActualMaximum(I)I
    .locals 1
    .param p1, "field"    # I

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->getActualMaximum(I)I

    move-result v0

    return v0
.end method

.method public getDayOfMonth()I
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getDayOfWeek()I
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getFirstDayOfWeek()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getFirstDayOfWeek()I

    move-result v0

    return v0
.end method

.method public getHour()I
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getMinimalDayInFirstWeek()I
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getMinimalDaysInFirstWeek()I

    move-result v0

    return v0
.end method

.method public getMinute()I
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getMonth()I
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getTime()Ljava/sql/Date;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    check-cast v0, Ljava/sql/Date;

    return-object v0
.end method

.method public getTimeInMillis()J
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getYear()I
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    return v0
.end method

.method public init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 60
    return-object p0
.end method

.method public initWithCurrentTime()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 2

    .prologue
    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    return-object v0
.end method

.method public restoreCalendarState()V
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    .line 310
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->savedTime:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->savedTime:J

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->isWeekStartsFromSunday()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 314
    iput-wide v3, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->savedTime:J

    .line 320
    :goto_1
    return-void

    .line 313
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 318
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->TAG:Ljava/lang/String;

    const-string v1, "There is no saved time value!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public roll(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 1
    .param p1, "field"    # I
    .param p2, "value"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->roll(II)V

    .line 234
    return-object p0
.end method

.method public saveCalendarState()V
    .locals 4

    .prologue
    .line 299
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->savedTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->savedTime:J

    .line 303
    :goto_0
    return-void

    .line 302
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->TAG:Ljava/lang/String;

    const-string v1, "There is a saved time value already!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 1
    .param p1, "field"    # I
    .param p2, "value"    # I

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 259
    return-object p0
.end method

.method public set(III)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 1
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "day"    # I

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/GregorianCalendar;->set(III)V

    .line 273
    return-object p0
.end method

.method public set(IIIIII)V
    .locals 7
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "day"    # I
    .param p4, "hour"    # I
    .param p5, "minute"    # I
    .param p6, "second"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 95
    return-void
.end method

.method public setFirstDayOfWeek(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 193
    return-void
.end method

.method public setTime(Ljava/sql/Date;)V
    .locals 1
    .param p1, "date"    # Ljava/sql/Date;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 81
    return-void
.end method

.method public setTimeInMillis(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 71
    return-object p0
.end method

.method public setUserFirstDayOfWeek()V
    .locals 2

    .prologue
    .line 200
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->calendarInstance:Ljava/util/GregorianCalendar;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->isWeekStartsFromSunday()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 203
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
