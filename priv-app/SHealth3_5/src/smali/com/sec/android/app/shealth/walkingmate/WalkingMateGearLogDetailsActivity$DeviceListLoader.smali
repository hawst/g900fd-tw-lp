.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;
.super Landroid/os/AsyncTask;
.source "WalkingMateGearLogDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceListLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 171
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 178
    .local v0, "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDeviceNames:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTime:J
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDeviceNames:[Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v1

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getDetailGearData(JLjava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;JLjava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_0

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 187
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->updateDeviceList()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V

    .line 188
    const/4 v2, 0x0

    return-object v2
.end method
