.class abstract Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "TaskWithDialog"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 0

    .prologue
    .line 266
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;, "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog<TE;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;

    .prologue
    .line 266
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;, "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog<TE;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    return-void
.end method


# virtual methods
.method public accompanyWithLoadingDialog()Z
    .locals 1

    .prologue
    .line 269
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;, "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog<TE;>;"
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract getDisplayStage()Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
.end method

.method protected isAlreadyOnRequiredStage()Z
    .locals 2

    .prologue
    .line 275
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;, "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog<TE;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;->getDisplayStage()Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
