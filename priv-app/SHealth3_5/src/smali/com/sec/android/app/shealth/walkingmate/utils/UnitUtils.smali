.class public Lcom/sec/android/app/shealth/walkingmate/utils/UnitUtils;
.super Ljava/lang/Object;
.source "UnitUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/utils/UnitUtils$UnitConverter;
    }
.end annotation


# static fields
.field public static final CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F = 1.8f

.field public static final CELSIUS_TO_FAHRENHEIT_SUMMAND:I = 0x20

.field public static final CM_IN_FT:F = 30.48006f

.field public static final CM_IN_INCH:F = 2.54f

.field public static final FAHRENHEIT_TO_CELSIUS_MULTIPLIER:F = 0.5555556f

.field public static final FAHRENHEIT_TO_CELSIUS_SUMMAND:I = -0x20

.field public static final FT_IN_CM:F = 0.032808334f

.field public static final INCHES_IN_CM:F = 0.39370078f

.field public static final KG_IN_LB:F = 0.45359236f

.field public static final LB_IN_KG:F = 2.2046227f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method
