.class public Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;
.super Ljava/lang/Object;
.source "GraphDataUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;,
        Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;
    }
.end annotation


# static fields
.field private static final CHART_HANDLER_INITIAL_PLACE_RELATIONAL_OFFSET:D = 0.8

.field private static cdata:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

.field private static mDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463
    return-void
.end method

.method static filterList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, "listToFilterKeepingOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p1, "contentsToFilter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 140
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 141
    .local v2, "str":Ljava/lang/String;
    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 142
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v2    # "str":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public static getCandleTimeSeries(Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;ILandroid/os/Bundle;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;
    .locals 25
    .param p0, "measureCountManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .param p3, "timeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p4, "countContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p5, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "seriesId"    # I
    .param p8, "summaryFragmentBundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "I",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "contract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    .local p2, "extractContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    new-instance v15, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;

    invoke-direct {v15}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;-><init>()V

    .line 78
    .local v15, "candleSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;
    const-wide/high16 v6, -0x8000000000000000L

    const-wide v8, 0x7fffffffffffffffL

    invoke-static/range {p6 .. p6}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v5

    iget-object v10, v5, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    const/4 v12, 0x0

    sget-object v13, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->ASC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    move-object/from16 v5, p5

    move-object/from16 v11, p1

    invoke-interface/range {v5 .. v13}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v17

    .line 81
    .local v17, "graphCursor":Landroid/database/Cursor;
    if-nez v17, :cond_0

    .line 112
    :goto_0
    return-object v15

    .line 84
    :cond_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    .line 85
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v24, "valuesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-object/from16 v0, p8

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v20

    .line 87
    .local v20, "summaryViewTime":J
    const-wide/16 v22, 0x0

    .line 88
    .local v22, "time":J
    :goto_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_3

    .line 89
    new-instance v14, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;

    invoke-direct {v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;-><init>()V

    .line 90
    .local v14, "candleData":Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
    if-eqz p4, :cond_1

    .line 91
    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 93
    .local v18, "graphValue":I
    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    move-object/from16 v1, p6

    invoke-static {v0, v5, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getTime(Landroid/database/Cursor;Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v5

    move-object/from16 v0, p0

    move/from16 v1, p7

    move/from16 v2, v18

    invoke-interface {v0, v1, v5, v6, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->setMeasureCountForPeriod(IJI)V

    .line 97
    .end local v18    # "graphValue":I
    :cond_1
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;

    .line 98
    .local v16, "daoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    move-object/from16 v0, p5

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 101
    .end local v16    # "daoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    :cond_2
    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    move-object/from16 v1, p6

    invoke-static {v0, v5, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getTime(Landroid/database/Cursor;Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v22

    .line 102
    move-wide/from16 v0, v22

    move-wide/from16 v2, v20

    move-object/from16 v4, p0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateHandlerTime(JJLcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 103
    invoke-static/range {v24 .. v24}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    invoke-virtual {v14, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->setValueHigh(D)V

    .line 104
    invoke-static/range {v24 .. v24}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    invoke-virtual {v14, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->setValueLow(D)V

    .line 105
    move-wide/from16 v0, v22

    invoke-virtual {v14, v0, v1}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->setTime(J)V

    .line 106
    invoke-virtual {v15, v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;)V

    .line 107
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->clear()V

    .line 108
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_1

    .line 110
    .end local v14    # "candleData":Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
    .end local v19    # "i$":Ljava/util/Iterator;
    :cond_3
    const-wide/16 v5, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    invoke-static {v0, v1, v5, v6, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateHandlerTime(JJLcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 111
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public static getDataSeriesList(Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;ILandroid/os/Bundle;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    .locals 19
    .param p0, "measureCountManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .param p3, "timeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p4, "countContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p5, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "additionalColumn"    # Ljava/lang/String;
    .param p8, "additionalValue"    # Ljava/lang/Object;
    .param p9, "valueConversionProvider"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;
    .param p10, "seriesId"    # I
    .param p11, "summaryFragmentBundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;",
            "I",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, "contract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    .local p2, "extractContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    const/4 v10, 0x0

    .line 297
    .local v10, "rangeFilter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    sget-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->mDatas:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 298
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->mDatas:Ljava/util/ArrayList;

    .line 303
    :goto_0
    if-eqz p7, :cond_0

    if-eqz p8, :cond_0

    .line 305
    new-instance v10, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    .end local v10    # "rangeFilter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    const-string v3, "="

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {v10, v0, v1, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    .restart local v10    # "rangeFilter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    :cond_0
    new-instance v16, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct/range {v16 .. v16}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    .line 311
    .local v16, "seriesDataList":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    const-wide/high16 v4, -0x8000000000000000L

    const-wide v6, 0x7fffffffffffffffL

    invoke-static/range {p6 .. p6}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v3

    iget-object v8, v3, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    sget-object v11, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->ASC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    move-object/from16 v3, p5

    move-object/from16 v9, p1

    invoke-interface/range {v3 .. v11}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v14

    .line 317
    .local v14, "graphCursor":Landroid/database/Cursor;
    if-nez v14, :cond_2

    .line 361
    :goto_1
    return-object v16

    .line 300
    .end local v14    # "graphCursor":Landroid/database/Cursor;
    .end local v16    # "seriesDataList":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    :cond_1
    sget-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 322
    .restart local v14    # "graphCursor":Landroid/database/Cursor;
    .restart local v16    # "seriesDataList":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    :cond_2
    const-wide/16 v17, 0x0

    .line 324
    .local v17, "time":J
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 326
    :goto_2
    invoke-interface {v14}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_5

    .line 328
    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 333
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;

    .line 335
    .local v13, "daoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    move-object/from16 v0, p5

    invoke-interface {v0, v13}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, p9

    invoke-interface {v0, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;->getValueInCurrentUnits(F)F

    move-result v12

    .line 340
    .local v12, "cursorValue":F
    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-eqz v3, :cond_3

    .line 341
    new-instance v3, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>()V

    sput-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->cdata:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    .line 342
    sget-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->cdata:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setTime(J)V

    .line 343
    sget-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->cdata:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    float-to-double v4, v12

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setValue(D)V

    .line 345
    sget-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->mDatas:Ljava/util/ArrayList;

    sget-object v4, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->cdata:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 353
    .end local v12    # "cursorValue":F
    .end local v13    # "daoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    :cond_4
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    .line 357
    .end local v15    # "i$":Ljava/util/Iterator;
    :cond_5
    const-wide/16 v3, 0x0

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateHandlerTime(JJLcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 359
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 360
    sget-object v3, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->mDatas:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    goto :goto_1
.end method

.method public static getGraphStartTime(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J
    .locals 8
    .param p0, "handlerTime"    # J
    .param p2, "period"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 124
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    .line 125
    .local v1, "chartStartDateCalendar":Ljava/util/Calendar;
    invoke-static {p2}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getCalendarPeriodType()I

    move-result v0

    .line 127
    .local v0, "calendarPeriodType":I
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 129
    invoke-static {p2}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getHorizontalMarksCount()I

    move-result v4

    neg-int v4, v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v0, v4}, Ljava/util/Calendar;->add(II)V

    .line 131
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long v2, p0, v4

    .line 134
    .local v2, "visibleChartWidthInMilliseconds":J
    long-to-double v4, v2

    const-wide v6, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v4, v6

    double-to-long v4, v4

    sub-long v4, p0, v4

    return-wide v4
.end method

.method protected static getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)J
    .locals 3
    .param p0, "time"    # J
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    .prologue
    .line 175
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->HOUR:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p2, v0, :cond_0

    .line 176
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfHour(J)J

    move-result-wide v0

    .line 180
    :goto_0
    return-wide v0

    .line 177
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p2, v0, :cond_1

    .line 178
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    goto :goto_0

    .line 179
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p2, v0, :cond_2

    .line 180
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v0

    goto :goto_0

    .line 182
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "periodType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected static getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J
    .locals 3
    .param p0, "time"    # J
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_0

    .line 196
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfHour(J)J

    move-result-wide v0

    .line 200
    :goto_0
    return-wide v0

    .line 197
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_1

    .line 198
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    goto :goto_0

    .line 199
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_2

    .line 200
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v0

    goto :goto_0

    .line 202
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "periodType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J
    .locals 2
    .param p0, "bundle"    # Landroid/os/Bundle;
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 390
    if-eqz p0, :cond_0

    const-string v0, "SUMMARY_VIEW_DATE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    const-string v0, "SUMMARY_VIEW_DATE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v0

    .line 394
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getTime(Landroid/database/Cursor;Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "cursorColumnName"    # Ljava/lang/String;
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 159
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {p2}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)J

    move-result-wide v0

    .line 163
    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public static getTimeSeriesList(Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;ILandroid/os/Bundle;)Ljava/util/List;
    .locals 26
    .param p0, "measureCountManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .param p3, "timeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p4, "countContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p5, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "additionalColumn"    # Ljava/lang/String;
    .param p8, "additionalValue"    # Ljava/lang/Object;
    .param p9, "valueConversionProvider"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;
    .param p10, "seriesId"    # I
    .param p11, "summaryFragmentBundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;",
            "I",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    .local p1, "contract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    .local p2, "extractContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    const/4 v12, 0x0

    .line 234
    .local v12, "rangeFilter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    if-eqz p7, :cond_0

    if-eqz p8, :cond_0

    .line 235
    new-instance v12, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    .end local v12    # "rangeFilter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    const-string v5, "="

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {v12, v0, v1, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    .restart local v12    # "rangeFilter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    :cond_0
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .local v20, "seriesDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;>;"
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->size()I

    move-result v5

    move/from16 v0, v18

    if-ge v0, v5, :cond_1

    .line 240
    new-instance v5, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-interface {v0, v1, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 239
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 242
    :cond_1
    const-wide/high16 v6, -0x8000000000000000L

    const-wide v8, 0x7fffffffffffffffL

    invoke-static/range {p6 .. p6}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v5

    iget-object v10, v5, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    sget-object v13, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->ASC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    move-object/from16 v5, p5

    move-object/from16 v11, p1

    invoke-interface/range {v5 .. v13}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v16

    .line 245
    .local v16, "graphCursor":Landroid/database/Cursor;
    if-nez v16, :cond_2

    .line 277
    :goto_1
    return-object v20

    .line 249
    :cond_2
    move-object/from16 v0, p11

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v22

    .line 250
    .local v22, "summaryViewTime":J
    const-wide/16 v24, 0x0

    .line 251
    .local v24, "time":J
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 252
    :goto_2
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_6

    .line 253
    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    move-object/from16 v1, p6

    invoke-static {v0, v5, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getTime(Landroid/database/Cursor;Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v24

    .line 256
    move-wide/from16 v0, v24

    move-wide/from16 v2, v22

    move-object/from16 v4, p0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateHandlerTime(JJLcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 257
    if-eqz p4, :cond_3

    .line 258
    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 261
    .local v17, "graphValue":I
    move-object/from16 v0, p0

    move/from16 v1, p10

    move-wide/from16 v2, v24

    move/from16 v4, v17

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->setMeasureCountForPeriod(IJI)V

    .line 263
    .end local v17    # "graphValue":I
    :cond_3
    const/16 v21, 0x0

    .line 264
    .local v21, "seriesIndex":I
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;

    .line 265
    .local v15, "daoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    move-object/from16 v0, p5

    invoke-interface {v0, v15}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    move-object/from16 v0, p9

    invoke-interface {v0, v5}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;->getValueInCurrentUnits(F)F

    move-result v14

    .line 268
    .local v14, "cursorValue":F
    const/4 v5, 0x0

    cmpl-float v5, v14, v5

    if-eqz v5, :cond_4

    .line 269
    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    new-instance v6, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    float-to-double v7, v14

    move-wide/from16 v0, v24

    invoke-direct {v6, v0, v1, v7, v8}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>(JD)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 271
    :cond_4
    add-int/lit8 v21, v21, 0x1

    .line 272
    goto :goto_3

    .line 273
    .end local v14    # "cursorValue":F
    .end local v15    # "daoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    :cond_5
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_2

    .line 275
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v21    # "seriesIndex":I
    :cond_6
    const-wide/16 v5, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    invoke-static {v0, v1, v5, v6, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateHandlerTime(JJLcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 276
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1
.end method

.method public static updateHandlerTime(JJLcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 4
    .param p0, "time"    # J
    .param p2, "compareTime"    # J
    .param p4, "measureCountManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    .prologue
    .line 376
    invoke-interface {p4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->getHandlerTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    cmp-long v0, p0, p2

    if-ltz v0, :cond_0

    .line 377
    invoke-interface {p4, p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->setHandlerTime(J)V

    .line 379
    :cond_0
    return-void
.end method

.method private static updateLatestMeasureTimeInHour(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V
    .locals 12
    .param p0, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p1, "rangeFilter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .param p3, "maxTimeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;

    .prologue
    .line 427
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 428
    .local v6, "cursorProjection":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    invoke-interface {v6, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 429
    const-wide/high16 v1, -0x8000000000000000L

    const-wide v3, 0x7fffffffffffffffL

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v0

    iget-object v5, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    sget-object v8, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    move-object v0, p0

    move-object v7, p1

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v9

    .line 432
    .local v9, "graphCursor":Landroid/database/Cursor;
    if-nez v9, :cond_0

    .line 443
    :goto_0
    return-void

    .line 435
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 436
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 437
    invoke-interface {p0, p3}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 439
    .local v10, "time":J
    invoke-interface {p2, v10, v11}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->addLatestMeasureTimeInHour(J)V

    .line 440
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 442
    .end local v10    # "time":J
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static updateLatestMeasureTimeInHour(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Ljava/lang/String;Ljava/util/List;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V
    .locals 4
    .param p0, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p1, "additionalColumn"    # Ljava/lang/String;
    .param p3, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .param p4, "maxTimeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ")V"
        }
    .end annotation

    .prologue
    .line 414
    .local p2, "additionalValueList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-eqz p2, :cond_0

    .line 415
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 416
    .local v0, "additionalValue":Ljava/lang/Object;
    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v3, "="

    invoke-direct {v2, p1, v0, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v2, p3, p4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateLatestMeasureTimeInHour(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V

    goto :goto_0

    .line 420
    .end local v0    # "additionalValue":Ljava/lang/Object;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    const/4 v2, 0x0

    invoke-static {p0, v2, p3, p4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateLatestMeasureTimeInHour(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V

    .line 422
    :cond_1
    return-void
.end method
