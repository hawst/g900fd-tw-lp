.class Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;
.super Ljava/lang/Object;
.source "WalkingTimePicker.java"

# interfaces
.implements Landroid/widget/TimePicker$OnTimeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRoundedMinute(I)I
    .locals 2
    .param p1, "minute"    # I

    .prologue
    .line 118
    rem-int/lit8 v1, p1, 0xa

    if-eqz v1, :cond_0

    .line 119
    rem-int/lit8 v1, p1, 0xa

    sub-int v0, p1, v1

    .line 120
    .local v0, "minuteFloor":I
    add-int/lit8 v1, v0, 0x1

    if-ne p1, v1, :cond_1

    const/16 v1, 0xa

    :goto_0
    add-int p1, v0, v1

    .line 121
    const/16 v1, 0x3c

    if-ne p1, v1, :cond_0

    const/4 p1, 0x0

    .line 124
    .end local v0    # "minuteFloor":I
    :cond_0
    return p1

    .line 120
    .restart local v0    # "minuteFloor":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 2
    .param p1, "timePicker"    # Landroid/widget/TimePicker;
    .param p2, "hourOfDay"    # I
    .param p3, "minute"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mIgnoreEvent:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->access$100(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;->getRoundedMinute(I)I

    move-result p3

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mIgnoreEvent:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->access$102(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;Z)Z

    .line 109
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mIgnoreEvent:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->access$102(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;Z)Z

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mCurrentHour:I
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->access$202(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;I)I

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mCurrentMin:I
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->access$302(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;I)I

    .line 115
    :cond_0
    return-void
.end method
