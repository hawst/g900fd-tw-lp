.class public Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "LifeStyleScoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
    }
.end annotation


# static fields
.field public static final LIFESTYLE_MULTI_SCORE:I = 0x2

.field public static final LIFESTYLE_NO_SCORE:I = 0x0

.field public static final LIFESTYLE_ONE_SCORE:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

.field private mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

.field private mInitializeLifeStyleScore:Z

.field private mIsTimeToReassess:Z

.field private mLastAssessmentDate:Ljava/lang/String;

.field private mLastScoreDifference:I

.field private mLastUpdateMessage:Ljava/lang/String;

.field private mLifeStyleScores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation
.end field

.field private mMultiScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

.field private mNoScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

.field private mOneScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

.field private mOutOfMessage:Ljava/lang/String;

.field private mOverallScore:I

.field private mReassessGoalWarningDescription:Ljava/lang/String;

.field private mReassessGoalWarningMessage:Ljava/lang/String;

.field private mReassessMssage:Ljava/lang/String;

.field private mScoreCoachDescription:Ljava/lang/String;

.field private mScoreCoachMessage:Ljava/lang/String;

.field private mState:I

.field private mVarianceMessage:Ljava/lang/String;

.field private newDayCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 37
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOverallScore:I

    .line 38
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mIsTimeToReassess:Z

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLastUpdateMessage:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mVarianceMessage:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOutOfMessage:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mScoreCoachMessage:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mScoreCoachDescription:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessMssage:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessGoalWarningMessage:Ljava/lang/String;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessGoalWarningDescription:Ljava/lang/String;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLastAssessmentDate:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLifeStyleScores:Ljava/util/List;

    .line 55
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mState:I

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mInitializeLifeStyleScore:Z

    .line 58
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->MONTHLY:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    .line 452
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->newDayCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mInitializeLifeStyleScore:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mIsTimeToReassess:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessMssage:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->showLifestyleFragment(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    return-object v0
.end method

.method private showLifestyleFragment(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "lss":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 302
    if-nez p1, :cond_1

    .line 304
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->showFragment(I)V

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 311
    .local v0, "scoreSize":I
    if-nez v0, :cond_2

    .line 313
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->showFragment(I)V

    goto :goto_0

    .line 315
    :cond_2
    if-ne v0, v5, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    sget-object v3, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->MONTHLY:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    if-ne v2, v3, :cond_4

    .line 317
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreType()Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-result-object v1

    .line 319
    .local v1, "scoreType":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    sget-object v2, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    if-eq v1, v2, :cond_3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreType()Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    if-ne v2, v3, :cond_0

    .line 321
    :cond_3
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOverallScore:I

    .line 322
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->showFragment(I)V

    goto :goto_0

    .line 327
    .end local v1    # "scoreType":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    :cond_4
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->showFragment(I)V

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const v3, 0x7f090c5a

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 127
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 128
    return-void

    .line 104
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)V

    .line 123
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f02020d

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 124
    .local v1, "moreButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method

.method public finish()V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->finish()V

    .line 83
    return-void
.end method

.method public getLastAssessmentDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLastAssessmentDate:Ljava/lang/String;

    return-object v0
.end method

.method public getLastScoreDifference()I
    .locals 1

    .prologue
    .line 486
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLastScoreDifference:I

    return v0
.end method

.method public getLastUpdateMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLastUpdateMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getLifeStyleScores()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLifeStyleScores:Ljava/util/List;

    if-nez v0, :cond_0

    .line 385
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLifeStyleScores:Ljava/util/List;

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLifeStyleScores:Ljava/util/List;

    return-object v0
.end method

.method public getOutOfMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOutOfMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getOverallScore()I
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOverallScore:I

    return v0
.end method

.method public getPeriodType()Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    return-object v0
.end method

.method public getReassessGoalWarningDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessGoalWarningDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getReassessGoalWarningMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessGoalWarningMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getScoreCoachDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mScoreCoachDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getScoreCoachMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mScoreCoachMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getScoreData(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V
    .locals 1
    .param p1, "periodType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    if-eq v0, p1, :cond_0

    .line 477
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->initializeLifeStyleScore(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V

    .line 479
    :cond_0
    return-void
.end method

.method public getScoreReassessMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessMssage:Ljava/lang/String;

    return-object v0
.end method

.method public getVarianceMessageMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mVarianceMessage:Ljava/lang/String;

    return-object v0
.end method

.method public initializeLifeStyleScore(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V
    .locals 10
    .param p1, "peridType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    .prologue
    const/4 v9, 0x1

    .line 215
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    .line 217
    const/4 v4, 0x0

    .line 219
    .local v4, "lss":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScoreStatus(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v0

    .line 220
    .local v0, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    if-eqz v0, :cond_1

    .line 221
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/ScoreInfo;->isTimeToReassess()Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mIsTimeToReassess:Z

    .line 223
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/ScoreInfo;->getLastUpdateMessage()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLastUpdateMessage:Ljava/lang/String;

    .line 224
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/ScoreInfo;->getOutOfMessage()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOutOfMessage:Ljava/lang/String;

    .line 225
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/ScoreInfo;->getLssList()Ljava/util/List;

    move-result-object v4

    .line 232
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 235
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    .line 236
    .local v6, "msgListSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v6, :cond_1

    .line 237
    if-nez v2, :cond_0

    .line 238
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mScoreCoachDescription:Ljava/lang/String;

    .line 236
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 240
    :cond_0
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessMssage:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 269
    .end local v0    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    .end local v2    # "i":I
    .end local v6    # "msgListSize":I
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 274
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    if-eqz v4, :cond_4

    .line 276
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-lt v7, v9, :cond_4

    .line 278
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLifeStyleScores:Ljava/util/List;

    .line 280
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 281
    .local v5, "lssSize":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    if-ge v2, v5, :cond_4

    .line 282
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLifeStyleScores:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne v2, v7, :cond_2

    .line 285
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 286
    .local v3, "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getVarianceText()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mVarianceMessage:Ljava/lang/String;

    .line 287
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getVarianceValue()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mLastScoreDifference:I

    .line 281
    .end local v3    # "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 266
    .end local v2    # "i":I
    .end local v5    # "lssSize":I
    .restart local v0    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    :cond_3
    :try_start_1
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->TAG:Ljava/lang/String;

    const-string v8, "coach msg is null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 296
    .end local v0    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    :cond_4
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mInitializeLifeStyleScore:Z

    .line 298
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->showLifestyleFragment(Ljava/util/List;)V

    .line 299
    return-void
.end method

.method public isTimeToReassess()Z
    .locals 1

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mIsTimeToReassess:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f030064

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->setContentView(I)V

    .line 68
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->MONTHLY:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->initializeLifeStyleScore(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->newDayCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 71
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->newDayCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->cancel(Z)Z

    .line 95
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 78
    return-void
.end method

.method public setRequestedOrientation(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 470
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mInitializeLifeStyleScore:Z

    if-eqz v0, :cond_0

    .line 471
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->setRequestedOrientation(I)V

    .line 473
    :cond_0
    return-void
.end method

.method public showFragment(I)V
    .locals 7
    .param p1, "state"    # I

    .prologue
    const/4 v5, 0x1

    .line 134
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mState:I

    .line 136
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mState:I

    packed-switch v4, :pswitch_data_0

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 139
    :pswitch_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->setRequestedOrientation(I)V

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mNoScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    if-nez v4, :cond_1

    .line 142
    new-instance v4, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mNoScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    .line 147
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 148
    .local v1, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 149
    .local v3, "ft":Landroid/support/v4/app/FragmentTransaction;
    const v4, 0x7f0801e3

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mNoScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    const-string/jumbo v6, "noscore"

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 150
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    .end local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->finish()V

    goto :goto_0

    .line 160
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->setRequestedOrientation(I)V

    .line 162
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOneScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    if-nez v4, :cond_2

    .line 163
    new-instance v4, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOneScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    .line 166
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 167
    .restart local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 168
    .restart local v3    # "ft":Landroid/support/v4/app/FragmentTransaction;
    const v4, 0x7f0801e3

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mOneScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    const-string/jumbo v6, "onescore"

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 169
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 171
    .end local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :catch_1
    move-exception v0

    .line 172
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->finish()V

    goto :goto_0

    .line 178
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v5, "onescore"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 179
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_0

    .line 180
    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->initOneScoreContents()V

    goto :goto_0

    .line 187
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mMultiScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    if-nez v4, :cond_3

    .line 188
    new-instance v4, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mMultiScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    .line 191
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 192
    .restart local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 193
    .restart local v3    # "ft":Landroid/support/v4/app/FragmentTransaction;
    const v4, 0x7f0801e3

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mMultiScoreFragment:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    const-string/jumbo v6, "multiscore"

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 194
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 196
    .end local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :catch_2
    move-exception v0

    .line 197
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->finish()V

    goto/16 :goto_0

    .line 203
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v5, "multiscore"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 204
    .restart local v2    # "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_0

    .line 205
    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->updateMultiScoreContent()V

    goto/16 :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
