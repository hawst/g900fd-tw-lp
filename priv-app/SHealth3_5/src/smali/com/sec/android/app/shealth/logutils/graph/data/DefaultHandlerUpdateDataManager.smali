.class public Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;
.super Ljava/lang/Object;
.source "DefaultHandlerUpdateDataManager.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;


# static fields
.field private static final KEY_SPLITTER:Ljava/lang/String; = "@"


# instance fields
.field private mAverageDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHandlerTime:J

.field private mLatestMeasureInHourTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mAverageDataMap:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mLatestMeasureInHourTimeMap:Ljava/util/Map;

    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mHandlerTime:J

    return-void
.end method

.method private static getKey(IJ)Ljava/lang/String;
    .locals 2
    .param p0, "seriesId"    # I
    .param p1, "periodStart"    # J

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addLatestMeasureTimeInHour(J)V
    .locals 8
    .param p1, "latestMeasureTime"    # J

    .prologue
    .line 51
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfHour(J)J

    move-result-wide v2

    .line 52
    .local v2, "startOfHourTime":J
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mLatestMeasureInHourTimeMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 53
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mLatestMeasureInHourTimeMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 54
    .local v0, "currentTimeValue":J
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mLatestMeasureInHourTimeMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    .end local v0    # "currentTimeValue":J
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mLatestMeasureInHourTimeMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getHandlerTime()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mHandlerTime:J

    return-wide v0
.end method

.method public getLatestMeasureTimeInHour(J)J
    .locals 3
    .param p1, "hourStartTime"    # J

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mLatestMeasureInHourTimeMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There is no latest measurement time in hour for hour start time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mLatestMeasureInHourTimeMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMeasureCountForPeriod(IJ)I
    .locals 2
    .param p1, "seriesId"    # I
    .param p2, "periodStart"    # J

    .prologue
    .line 39
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->getKey(IJ)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mAverageDataMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 44
    const/4 v1, 0x0

    .line 46
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mAverageDataMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public setHandlerTime(J)V
    .locals 0
    .param p1, "handlerTime"    # J

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mHandlerTime:J

    .line 77
    return-void
.end method

.method public setMeasureCountForPeriod(IJI)V
    .locals 3
    .param p1, "seriesId"    # I
    .param p2, "periodStart"    # J
    .param p4, "measureCount"    # I

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->mAverageDataMap:Ljava/util/Map;

    invoke-static {p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;->getKey(IJ)Ljava/lang/String;

    move-result-object v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method
