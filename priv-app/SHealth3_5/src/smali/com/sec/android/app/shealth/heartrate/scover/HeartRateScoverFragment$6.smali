.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;
.super Ljava/util/TimerTask;
.source "HeartRateScoverFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startSensorAndDelayUIForDetectedFinger(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

.field final synthetic val$animation:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;->val$animation:I

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 494
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sth  startSensorAndDelayUIForDetectedFinger task enter"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 496
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 499
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;->val$animation:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 506
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 507
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sth  startSensorAndDelayUIForDetectedFinger task exit"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    return-void

    .line 503
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 504
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;->val$animation:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0
.end method
