.class public Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BoundaryChecker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/lang/Comparable",
        "<TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mValue:Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Comparable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 392
    .local p0, "this":Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;, "Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker<TT;>;"
    .local p1, "value":Ljava/lang/Comparable;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->mValue:Ljava/lang/Comparable;

    .line 394
    return-void
.end method


# virtual methods
.method public isValueInBounds(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 400
    .local p0, "this":Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;, "Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker<TT;>;"
    .local p1, "lowerBound":Ljava/lang/Comparable;, "TT;"
    .local p2, "upperBound":Ljava/lang/Comparable;, "TT;"
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_0

    .line 401
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Lower bound is larger than upper, while it is supposed to be vice versa."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 404
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->isValueNotLessThan(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->isValueNotLargerThan(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValueLargerThan(Ljava/lang/Comparable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 411
    .local p0, "this":Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;, "Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker<TT;>;"
    .local p1, "lowerBound":Ljava/lang/Comparable;, "TT;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->mValue:Ljava/lang/Comparable;

    invoke-interface {v0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValueLessThan(Ljava/lang/Comparable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 418
    .local p0, "this":Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;, "Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker<TT;>;"
    .local p1, "lowerBound":Ljava/lang/Comparable;, "TT;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->mValue:Ljava/lang/Comparable;

    invoke-interface {v0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValueNotLargerThan(Ljava/lang/Comparable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 425
    .local p0, "this":Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;, "Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker<TT;>;"
    .local p1, "lowerBound":Ljava/lang/Comparable;, "TT;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->mValue:Ljava/lang/Comparable;

    invoke-interface {v0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValueNotLessThan(Ljava/lang/Comparable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 432
    .local p0, "this":Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;, "Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker<TT;>;"
    .local p1, "lowerBound":Ljava/lang/Comparable;, "TT;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->mValue:Ljava/lang/Comparable;

    invoke-interface {v0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
