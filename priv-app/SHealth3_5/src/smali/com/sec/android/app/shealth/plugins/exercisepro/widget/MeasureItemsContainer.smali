.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;
.super Landroid/widget/LinearLayout;
.source "MeasureItemsContainer.java"


# instance fields
.field public ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

.field public averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

.field public averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

.field public averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

.field public cadenceItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field public calorieItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field public distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

.field public durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field public lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

.field public timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field public traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->init()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->init()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->init()V

    .line 49
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030192

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initTimeAndDateItem()V

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initDuration()V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initCalorie()V

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initDistanceAndCalorie()V

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initAverageSpdAndMaxSpd()V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initAveragePaceAndMaxPace()V

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initLowElevAndHighElev()V

    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initAscentAndDecent()V

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initAvgHeartAndMaxHeart()V

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->initTraningEfctAndRecovTime()V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 66
    return-void
.end method

.method private initAscentAndDecent()V
    .locals 3

    .prologue
    .line 121
    const v0, 0x7f0806eb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f5a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftTitle(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020583

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftIconSrc(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f59

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightTitle(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020579

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightIconSrc(I)V

    .line 126
    return-void
.end method

.method private initAveragePaceAndMaxPace()V
    .locals 3

    .prologue
    .line 105
    const v0, 0x7f0806e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a53

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftTitle(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f02057e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftIconSrc(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a54

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightTitle(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020590

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightIconSrc(I)V

    .line 110
    return-void
.end method

.method private initAverageSpdAndMaxSpd()V
    .locals 3

    .prologue
    .line 97
    const v0, 0x7f0806e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a51

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftTitle(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f02057f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftIconSrc(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a52

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightTitle(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020591

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightIconSrc(I)V

    .line 102
    return-void
.end method

.method private initAvgHeartAndMaxHeart()V
    .locals 3

    .prologue
    .line 129
    const v0, 0x7f0806ec

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f6a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftTitle(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f02057d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftIconSrc(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f69

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightTitle(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f02058f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightIconSrc(I)V

    .line 134
    return-void
.end method

.method private initCalorie()V
    .locals 3

    .prologue
    .line 83
    const v0, 0x7f080428

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->calorieItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->calorieItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090954

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setTitle(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->calorieItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const v1, 0x7f020581

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setIconSrc(I)V

    .line 86
    return-void
.end method

.method private initDistanceAndCalorie()V
    .locals 3

    .prologue
    .line 89
    const v0, 0x7f0806e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090132

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftTitle(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020584

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftIconSrc(I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090954

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightTitle(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020581

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightIconSrc(I)V

    .line 94
    return-void
.end method

.method private initDuration()V
    .locals 3

    .prologue
    .line 77
    const v0, 0x7f0806e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a1b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setTitle(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const v1, 0x7f020585

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setIconSrc(I)V

    .line 80
    return-void
.end method

.method private initLowElevAndHighElev()V
    .locals 3

    .prologue
    .line 113
    const v0, 0x7f0806ea

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f57

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftTitle(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020588

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftIconSrc(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f58

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightTitle(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f020587

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightIconSrc(I)V

    .line 118
    return-void
.end method

.method private initTimeAndDateItem()V
    .locals 3

    .prologue
    .line 69
    const v0, 0x7f0806e5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setDateVisible(Z)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a50

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setTitle(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const v1, 0x7f020582

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setIconSrc(I)V

    .line 73
    return-void
.end method

.method private initTraningEfctAndRecovTime()V
    .locals 3

    .prologue
    const v2, 0x7f090a56

    .line 137
    const v0, 0x7f0806ed

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftTitle(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f02059c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftIconSrc(I)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightTitle(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    const v1, 0x7f02059b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightIconSrc(I)V

    .line 142
    return-void
.end method


# virtual methods
.method public setAscent(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "ascentStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 292
    return-void

    .line 291
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAvgHeartRate(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "avgHeartRateStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    const/16 v0, 0x8

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 246
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 250
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setAvgHeartRateInDetails(Ljava/lang/String;ZI)V
    .locals 2
    .param p1, "avgHeartRateStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z
    .param p3, "dataType"    # I

    .prologue
    const/16 v0, 0x8

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 263
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x2726

    if-eq p3, v1, :cond_0

    const/16 v1, 0x2723

    if-eq p3, v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setAvgPace(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "avgPaceStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 237
    return-void

    .line 236
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAvgSpeed(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "avgSpeedStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 227
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBurntCalories(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "burntCaloriesStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 222
    return-void

    .line 221
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBurntCaloriesforOthers(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "calorieStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->calorieItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->calorieItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setVisibility(I)V

    .line 165
    return-void

    .line 164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDecent(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "decentStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 302
    return-void

    .line 301
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDistance(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "distanceStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 217
    return-void

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDuration(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "durationStr"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "fInit"    # Z

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    if-eqz p3, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a1b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 160
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHighElevation(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "elevationStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 297
    return-void

    .line 296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLowElevation(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "elevationStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 287
    return-void

    .line 286
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMaxHeartRate(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "mxHeartRateStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    const/16 v0, 0x8

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 259
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setMaxHeartRateInDetails(Ljava/lang/String;ZI)V
    .locals 2
    .param p1, "mxHeartRateStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z
    .param p3, "dataType"    # I

    .prologue
    const/16 v0, 0x8

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 272
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x2726

    if-eq p3, v1, :cond_0

    const/16 v1, 0x2723

    if-eq p3, v1, :cond_0

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 276
    :goto_0
    return-void

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setMaxPace(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "maxPaceStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 242
    return-void

    .line 241
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMaxSpeed(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "maxSpeedStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 232
    return-void

    .line 231
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRecoverytime(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "recoveryStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 312
    return-void

    .line 311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTimeAndDate(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "dateStr"    # Ljava/lang/String;
    .param p2, "timeStr"    # Ljava/lang/String;
    .param p3, "fInit"    # Z

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setDateText(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    if-eqz p3, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setVisibility(I)V

    .line 153
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTraningefct(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "traningStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->traningEfctAndRecovTime:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setVisibility(I)V

    .line 307
    return-void

    .line 306
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
