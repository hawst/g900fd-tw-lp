.class public Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$29;,
        Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;
    }
.end annotation


# static fields
.field private static final DELAYTIME_CHECK_SENSOR_DETECT:I = 0x96

.field private static final MEASURE_FAIL_MAX_COUNT:I

.field public static final SPO2_NONE_ERROR:I

.field public static final TAG:Ljava/lang/String;

.field static effectAudio:[I


# instance fields
.field private IMAGE_IDS:[I

.field public currentimageindex:I

.field private errorAnimation:Landroid/view/animation/AnimationSet;

.field private isFingerDetected:Z

.field private isReadyScreenTimeout:Z

.field private isRetryMeasuring:Z

.field private mBpmLayout:Landroid/widget/LinearLayout;

.field private mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

.field private mContext:Landroid/content/Context;

.field public mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

.field private mDeviceSupportCoverSDK:Z

.field private mErrorType:I

.field private mFirstLayout:Landroid/widget/LinearLayout;

.field private mFirstMessage:Landroid/widget/TextView;

.field private mFirstSpO2ValueForTesting:Landroid/widget/TextView;

.field public mFirstStatus:Landroid/widget/TextView;

.field public mHandler:Landroid/os/Handler;

.field private mHeartRate:I

.field private mHighStatus:Landroid/widget/TextView;

.field private mIsGreen:Z

.field mIsOnConfigChanged:Z

.field private mMeasureFailCount:I

.field private mMiddleStatus1:Landroid/widget/TextView;

.field private mMiddleStatus2:Landroid/widget/TextView;

.field private mO2Data:F

.field mPrevError:I

.field private mScaleAnimator:Landroid/animation/ValueAnimator;

.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field private mSecondBpm:Landroid/widget/TextView;

.field private mSecondBpmIcon:Landroid/widget/ImageView;

.field private mSecondBpmUnit:Landroid/widget/TextView;

.field private mSecondCenterIcon:Landroid/widget/ImageView;

.field private mSecondCenterIconBig:Landroid/widget/ImageView;

.field private mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

.field private mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

.field private mSecondDiscard:Landroid/widget/LinearLayout;

.field private mSecondDiscardText:Landroid/widget/TextView;

.field private mSecondIcon:Landroid/widget/ImageView;

.field private mSecondLayout:Landroid/widget/FrameLayout;

.field private mSecondO2:Landroid/widget/TextView;

.field private mSecondO2Unit:Landroid/widget/TextView;

.field private mSecondRetry:Landroid/widget/LinearLayout;

.field private mSecondRetryText:Landroid/widget/TextView;

.field private mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

.field private mSmallBarFooter:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

.field private mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

.field private mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

.field private mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

.field private mSpO2OnClcickListener:Landroid/view/View$OnClickListener;

.field private mSpo2Layout:Landroid/widget/LinearLayout;

.field public mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field mTestHandler:Landroid/os/Handler;

.field private mThirdBpm:Landroid/widget/TextView;

.field private mThirdBpmUnit:Landroid/widget/TextView;

.field private mThirdDate:Landroid/widget/TextView;

.field private mThirdGraph:Landroid/widget/ImageButton;

.field private mThirdO2:Landroid/widget/TextView;

.field private mThirdO2Unit:Landroid/widget/TextView;

.field private mThirdPreviousLayout:Landroid/widget/LinearLayout;

.field private mThirdTime:Landroid/widget/TextView;

.field private mlowStatus:Landroid/widget/TextView;

.field private pool:Landroid/media/SoundPool;

.field private readyTimer:Landroid/os/CountDownTimer;

.field sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private sensorHandler:Landroid/os/Handler;

.field private sensorRunnable:Ljava/lang/Runnable;

.field private slidingimage:Landroid/widget/ImageView;

.field private timer:Ljava/util/Timer;

.field private timerForCenterAnim:Ljava/util/Timer;

.field private wasShowInformationDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-class v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    .line 103
    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 105
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z

    .line 106
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isReadyScreenTimeout:Z

    .line 107
    iput v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMeasureFailCount:I

    .line 112
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isRetryMeasuring:Z

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    .line 121
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->wasShowInformationDialog:Z

    .line 123
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    .line 157
    iput-object v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 163
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 165
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    .line 170
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->IMAGE_IDS:[I

    .line 174
    iput v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    .line 175
    iput-object v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->slidingimage:Landroid/widget/ImageView;

    .line 176
    iput-object v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timer:Ljava/util/Timer;

    .line 177
    iput-object v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timerForCenterAnim:Ljava/util/Timer;

    .line 178
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHandler:Landroid/os/Handler;

    .line 201
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    .line 321
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsOnConfigChanged:Z

    .line 822
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2OnClcickListener:Landroid/view/View$OnClickListener;

    .line 1528
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;

    .line 1713
    iput-object v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    .line 1926
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$28;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$28;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mTestHandler:Landroid/os/Handler;

    return-void

    .line 170
    :array_0
    .array-data 4
        0x7f0201f2
        0x7f0201f3
        0x7f0201f4
    .end array-data
.end method

.method private AnimateandSlideShow()V
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 1717
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 1718
    iput v8, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    .line 1719
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->slidingimage:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1720
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->slidingimage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->IMAGE_IDS:[I

    iget v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1721
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->IMAGE_IDS:[I

    array-length v6, v6

    if-ge v5, v6, :cond_1

    .line 1722
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    .line 1726
    :goto_0
    const/16 v1, 0x1f4

    .line 1727
    .local v1, "fadeInDuration":I
    const/16 v4, 0x3e8

    .line 1728
    .local v4, "timeBetween":I
    const/16 v3, 0x1f4

    .line 1730
    .local v3, "fadeOutDuration":I
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v9, v10}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1731
    .local v0, "fadeIn":Landroid/view/animation/Animation;
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1732
    int-to-long v5, v1

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1734
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v10, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1735
    .local v2, "fadeOut":Landroid/view/animation/Animation;
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1736
    const/16 v5, 0x5dc

    int-to-long v5, v5

    invoke-virtual {v2, v5, v6}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1737
    int-to-long v5, v3

    invoke-virtual {v2, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1739
    new-instance v5, Landroid/view/animation/AnimationSet;

    invoke-direct {v5, v8}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    .line 1740
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1741
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1742
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->setRepeatCount(I)V

    .line 1743
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->slidingimage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1744
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    new-instance v6, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$25;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$25;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1756
    return-void

    .line 1724
    .end local v0    # "fadeIn":Landroid/view/animation/Animation;
    .end local v1    # "fadeInDuration":I
    .end local v2    # "fadeOut":Landroid/view/animation/Animation;
    .end local v3    # "fadeOutDuration":I
    .end local v4    # "timeBetween":I
    :cond_1
    iput v8, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setReadyUI(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setMeasuringUI(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setMeasuringFailUI(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showGraphFragment()V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mO2Data:F

    return v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isRetryMeasuring:Z

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->changeUIReadyToMeasuring(Z)V

    return-void
.end method

.method static synthetic access$4302(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isReadyScreenTimeout:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showErrorDialog(I)V

    return-void
.end method

.method static synthetic access$4502(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->slidingimage:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->initInfoSlideShow()V

    return-void
.end method

.method static synthetic access$4700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/view/animation/AnimationSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    return-object v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Landroid/view/animation/AnimationSet;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    return-object p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->AnimateandSlideShow()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mErrorType:I

    return v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sendTestData2()V

    return-void
.end method

.method static synthetic access$5300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sendTestData1()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    return-object p1
.end method

.method private changeUIReadyToMeasuring(Z)V
    .locals 5
    .param p1, "isFail"    # Z

    .prologue
    .line 1816
    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeReadyToMeasuringUI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1819
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v2}, Landroid/os/CountDownTimer;->cancel()V

    .line 1821
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    if-eqz v2, :cond_0

    .line 1822
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->cancel()V

    .line 1826
    :cond_0
    if-eqz p1, :cond_1

    .line 1827
    const/4 v0, 0x0

    .line 1831
    .local v0, "failMsg":I
    :goto_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1832
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1833
    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 1834
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1835
    return-void

    .line 1829
    .end local v0    # "failMsg":I
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "failMsg":I
    goto :goto_0
.end method

.method private firstAnimation(Landroid/view/View;I)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "stringID"    # I

    .prologue
    .line 901
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-nez v0, :cond_0

    .line 929
    :goto_0
    return-void

    .line 903
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$10;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private initAoudioFile()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1423
    new-instance v0, Landroid/media/SoundPool;

    invoke-direct {v0, v5, v6, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    .line 1424
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const v3, 0x7f06002c

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v4

    .line 1425
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const v3, 0x7f060027

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v5

    .line 1426
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const v4, 0x7f06002d

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 1427
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const v3, 0x7f060028

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v6

    .line 1428
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const v4, 0x7f06002a

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 1429
    return-void
.end method

.method private initInfoSlideShow()V
    .locals 1

    .prologue
    .line 1710
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->currentimageindex:I

    .line 1711
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->AnimateandSlideShow()V

    .line 1712
    return-void
.end method

.method private initMeasureStatus()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1838
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z

    .line 1839
    iput v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    .line 1840
    iput v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMeasureFailCount:I

    .line 1841
    return-void
.end method

.method private initSCover()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 427
    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v2, "initSCover"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 430
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 436
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    if-eqz v1, :cond_0

    .line 437
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 438
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 469
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 471
    :cond_0
    return-void

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    goto :goto_0

    .line 433
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 434
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    goto :goto_0
.end method

.method private initView(Landroid/view/View;)Landroid/view/View;
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v9, 0x7f09020a

    const/4 v8, -0x2

    const v7, 0x7f090fa1

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 237
    const v1, 0x7f0805ca

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    .line 238
    const v1, 0x7f0805cb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    .line 239
    const v1, 0x7f0809b5

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    .line 240
    const v1, 0x7f0809b6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    .line 242
    const v1, 0x7f080595

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    .line 243
    const v1, 0x7f0805bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    .line 244
    const v1, 0x7f0805c1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    .line 246
    const v1, 0x7f0809c1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    .line 247
    const v1, 0x7f0805c8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    .line 248
    const v1, 0x7f0809c2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    .line 249
    const v1, 0x7f0809b8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    .line 250
    const v1, 0x7f0809b9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    .line 251
    const v1, 0x7f0809ba

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    .line 252
    const v1, 0x7f0809bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    .line 253
    const v1, 0x7f0809be

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    .line 254
    const v1, 0x7f0809bc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    .line 255
    const v1, 0x7f0809bd

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    .line 256
    const v1, 0x7f0809c0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmIcon:Landroid/widget/ImageView;

    .line 257
    const v1, 0x7f0805be

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    .line 258
    const v1, 0x7f0805bf

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    .line 259
    const v1, 0x7f0805c0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901ea

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 262
    const v1, 0x7f0809b7

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    .line 263
    const v1, 0x7f0809b4

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscardText:Landroid/widget/TextView;

    .line 264
    const v1, 0x7f0809ce

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetryText:Landroid/widget/TextView;

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090f4d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 267
    const v1, 0x7f0805e5

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    .line 268
    const v1, 0x7f0809c4

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdO2:Landroid/widget/TextView;

    .line 269
    const v1, 0x7f0809c5

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdO2Unit:Landroid/widget/TextView;

    .line 270
    const v1, 0x7f0805e9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdBpm:Landroid/widget/TextView;

    .line 271
    const v1, 0x7f0805ea

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdBpmUnit:Landroid/widget/TextView;

    .line 272
    const v1, 0x7f0805ec

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    .line 273
    const v1, 0x7f0809c3

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpo2Layout:Landroid/widget/LinearLayout;

    .line 274
    const v1, 0x7f0809c6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mBpmLayout:Landroid/widget/LinearLayout;

    .line 275
    const v1, 0x7f0809cd

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdTime:Landroid/widget/TextView;

    .line 276
    const v1, 0x7f0809c8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarFooter:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    .line 278
    const v1, 0x7f0805ef

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2OnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2OnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2OnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2OnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->show()V

    .line 290
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setGesturesEnabled(Z)V

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpo2Layout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090fa3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090fa4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090fa5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090fa6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    :goto_0
    return-object p1

    .line 305
    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 306
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 307
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 308
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarFooter:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpo2Layout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a69

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a6b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private isToday(J)Z
    .locals 4
    .param p1, "timeMills"    # J

    .prologue
    .line 635
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private measureFinishAnimation()V
    .locals 2

    .prologue
    .line 943
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-nez v0, :cond_0

    .line 1004
    :goto_0
    return-void

    .line 947
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$11;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private measureFinishAnimationMedical()V
    .locals 2

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-nez v0, :cond_0

    .line 1057
    :goto_0
    return-void

    .line 1010
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private playSound(I)V
    .locals 3
    .param p1, "effectAudio"    # I

    .prologue
    .line 1433
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1435
    .local v0, "aManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 1454
    :cond_0
    :goto_0
    return-void

    .line 1439
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v1, :cond_0

    .line 1440
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$18;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$18;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static randBetween(II)I
    .locals 4
    .param p0, "start"    # I
    .param p1, "end"    # I

    .prologue
    .line 1962
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    sub-int v2, p1, p0

    int-to-double v2, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/2addr v0, p0

    return v0
.end method

.method private scaleAnimationStarter(Z)V
    .locals 7
    .param p1, "isGreen"    # Z

    .prologue
    const v1, 0x7f0201d6

    const v2, 0x7f0201d2

    const/4 v6, 0x1

    const/high16 v5, 0x3f000000    # 0.5f

    .line 1157
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "scaleAnimationStarter isGreen: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1160
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 1161
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 1162
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 1163
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1164
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$13;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1172
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$14;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$14;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1204
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1210
    :goto_0
    return-void

    .line 1206
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    if-ne v0, v6, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1207
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    if-ne v3, v6, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1208
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    if-eqz p1, :cond_3

    const v0, 0x7f020757

    :goto_3
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setMovieResource(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1206
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1207
    goto :goto_2

    .line 1208
    :cond_3
    const v0, 0x7f02075a

    goto :goto_3

    .line 1162
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3fbd70a4    # 1.48f
    .end array-data
.end method

.method private sendTestData1()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1940
    new-array v1, v4, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;

    .line 1941
    .local v1, "test":[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 1942
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;-><init>()V

    aput-object v2, v1, v0

    .line 1943
    aget-object v2, v1, v0

    const/4 v3, -0x1

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->heartRate:I

    .line 1944
    aget-object v2, v1, v0

    const/high16 v3, -0x40800000    # -1.0f

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->pulseOximetry:F

    .line 1941
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1946
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->onDataReceivedArray([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;)V

    .line 1947
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mTestHandler:Landroid/os/Handler;

    const/16 v3, 0x3e8

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1948
    return-void
.end method

.method private sendTestData2()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1951
    new-array v1, v5, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;

    .line 1952
    .local v1, "test":[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 1953
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;-><init>()V

    aput-object v2, v1, v0

    .line 1954
    aget-object v2, v1, v0

    const/16 v3, 0x59

    const/16 v4, 0x60

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->randBetween(II)I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->heartRate:I

    .line 1955
    aget-object v2, v1, v0

    const/16 v3, 0x63

    const/16 v4, 0x64

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->randBetween(II)I

    move-result v3

    int-to-float v3, v3

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->pulseOximetry:F

    .line 1952
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1957
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->onDataReceivedArray([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;)V

    .line 1958
    return-void
.end method

.method private setEmotionImageNonMedical()V
    .locals 4

    .prologue
    .line 932
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mO2Data:F

    const/high16 v1, 0x42bd0000    # 94.5f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 933
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201d4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 939
    :goto_0
    return-void

    .line 934
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mO2Data:F

    const/high16 v1, 0x42b50000    # 90.5f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mO2Data:F

    float-to-double v0, v0

    const-wide v2, 0x4057a00000000000L    # 94.5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 935
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201d7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 937
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201d5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setFinishedStateMessage()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1345
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isAdded()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1346
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09090b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1347
    .local v0, "mMoreString":Ljava/lang/String;
    const-string v1, ""

    .line 1349
    .local v1, "message":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090f9e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1350
    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1351
    .local v3, "ss1":Landroid/text/SpannableString;
    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$15;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    .line 1361
    .local v2, "span2":Landroid/text/style/ClickableSpan;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    const/16 v6, 0x21

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1362
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1363
    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x78

    if-le v4, v5, :cond_1

    .line 1364
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1371
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1374
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isSamsungAccount(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v5, "eng"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1375
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v4, v6}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1376
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mO2Data:F

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1379
    .end local v0    # "mMoreString":Ljava/lang/String;
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "span2":Landroid/text/style/ClickableSpan;
    .end local v3    # "ss1":Landroid/text/SpannableString;
    :cond_0
    return-void

    .line 1365
    .restart local v0    # "mMoreString":Ljava/lang/String;
    .restart local v1    # "message":Ljava/lang/String;
    .restart local v2    # "span2":Landroid/text/style/ClickableSpan;
    .restart local v3    # "ss1":Landroid/text/SpannableString;
    :cond_1
    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x2d

    if-le v4, v5, :cond_2

    .line 1366
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v5, 0x41980000    # 19.0f

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1368
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v5, 0x41b80000    # 23.0f

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1375
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private setMeasuringFailUI(I)V
    .locals 6
    .param p1, "errorType"    # I

    .prologue
    const v5, 0x7f090c24

    const v4, 0x7f090c04

    const/4 v3, 0x3

    .line 1213
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringFailUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1215
    const/4 v0, -0x6

    if-ne p1, v0, :cond_3

    .line 1216
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v0, :cond_0

    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1220
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1221
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1222
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    aget v0, v0, v3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V

    .line 1223
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showErrorDialog(I)V

    .line 1258
    :cond_1
    :goto_0
    return-void

    .line 1226
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    aget v0, v0, v3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V

    .line 1227
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showErrorDialog(I)V

    goto :goto_0

    .line 1231
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    .line 1232
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->scaleAnimationStarter(Z)V

    .line 1234
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v0, :cond_5

    .line 1235
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->getIsRun()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1236
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->startAnimation(Z)V

    .line 1238
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->changeColor(Z)V

    .line 1242
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1243
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1247
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1248
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f02051f

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1249
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201d2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1250
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1251
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1254
    iput p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mErrorType:I

    .line 1255
    sget-object v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V

    goto/16 :goto_0
.end method

.method private setMeasuringUI(I)V
    .locals 8
    .param p1, "isShowAnimation"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const v5, 0x7f090c23

    const v4, 0x7f090c04

    const/16 v3, 0x8

    .line 1110
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v0, :cond_0

    .line 1113
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1115
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1116
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1118
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    .line 1119
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->scaleAnimationStarter(Z)V

    .line 1120
    sget-object v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->MEASURING:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v0, :cond_2

    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->getIsRun()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->startAnimation(Z)V

    .line 1126
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->changeColor(Z)V

    .line 1129
    :cond_2
    if-nez p1, :cond_3

    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1131
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v0, v5}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1142
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    const v1, 0x7f0201d6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1143
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f02051e

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 1148
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1150
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1151
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1154
    return-void

    .line 1133
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1134
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1135
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1137
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1138
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1139
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method private setReadyUI(I)V
    .locals 9
    .param p1, "animation"    # I

    .prologue
    const v8, 0x106000c

    const v7, 0x7f090c22

    const v6, 0x7f090c05

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 1060
    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setReadyUI"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v1, :cond_0

    .line 1063
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 1066
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1067
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1068
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 1069
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1070
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v2, 0x7f0201d8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1071
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040020

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1072
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1073
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/high16 v3, 0x41b80000    # 23.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1075
    if-nez p1, :cond_1

    .line 1076
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v1, v6}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1077
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v1, v7}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1089
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f02051e

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1090
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1091
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1092
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1093
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1094
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1095
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1096
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 1097
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1098
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1099
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1100
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1101
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1102
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 1103
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1104
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1106
    sget-object v1, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->READY:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V

    .line 1107
    return-void

    .line 1079
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1080
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1081
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1083
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1084
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1085
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1086
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method private showErrorDialog(I)V
    .locals 8
    .param p1, "errorType"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 1596
    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "showErrorDialog error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isReadyScreenTimeout:Z

    if-nez v1, :cond_4

    .line 1599
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.shealth.spo2"

    const-string v3, "SP05"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1606
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1607
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1608
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    if-eqz v1, :cond_0

    .line 1609
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->clearAnimation()V

    .line 1610
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 1612
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const/4 v1, -0x6

    if-ne p1, v1, :cond_5

    const v1, 0x7f02051e

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1614
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v1, :cond_1

    .line 1615
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 1618
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timerForCenterAnim:Ljava/util/Timer;

    if-eqz v1, :cond_2

    .line 1619
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timerForCenterAnim:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 1621
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-nez v1, :cond_6

    .line 1707
    :cond_3
    :goto_2
    return-void

    .line 1603
    :cond_4
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isReadyScreenTimeout:Z

    goto :goto_0

    .line 1612
    :cond_5
    const v1, 0x7f02051f

    goto :goto_1

    .line 1624
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1626
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1628
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1629
    const v1, 0x7f0907ae

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1630
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1631
    const v1, 0x7f03021e

    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$20;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$20;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1645
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$21;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$21;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1656
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$22;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1667
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1678
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1699
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1701
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_7

    .line 1702
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1704
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private showGraphFragment()V
    .locals 4

    .prologue
    .line 893
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->switchFragmentToGraph()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 897
    return-void

    .line 894
    :catch_0
    move-exception v0

    .line 895
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be instance of SpO2Activity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private showRecommendedDialog()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1382
    const/4 v1, 0x0

    .line 1383
    .local v1, "title":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1384
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1385
    const v2, 0x7f090047

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1387
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1388
    const v2, 0x7f030226

    new-instance v3, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$16;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$16;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1401
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1402
    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$17;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1409
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1410
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v2, :cond_0

    .line 1411
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1413
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string/jumbo v4, "recommendedDialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1416
    :cond_1
    return-void
.end method

.method private startTimer(Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V
    .locals 2
    .param p1, "timerType"    # Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    .prologue
    .line 1844
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v0, :cond_0

    .line 1845
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1885
    :cond_0
    return-void
.end method

.method private stopSensorAndAnimationInitialize()V
    .locals 2

    .prologue
    .line 864
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    if-nez v0, :cond_0

    .line 866
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopMeasuring()V

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopCountDownTimer()V

    .line 879
    :goto_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$9;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 889
    return-void

    .line 874
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopMeasuring()V

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopCountDownTimer()V

    goto :goto_0
.end method

.method private updateErrorDialogContent()V
    .locals 4

    .prologue
    .line 1912
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1913
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    .line 1914
    .local v0, "content":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1915
    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1916
    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0907ae

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1918
    const v1, 0x7f080551

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090c28

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1919
    const v1, 0x7f080552

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090c27

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1920
    const v1, 0x7f080553

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090c25

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1921
    const v1, 0x7f080554

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090c2d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1924
    .end local v0    # "content":Landroid/view/View;
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBarButtons(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;Landroid/view/View$OnClickListener;)V
    .locals 5
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v4, 0x0

    .line 1888
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 1889
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207c0

    const v3, 0x7f09005a

    invoke-direct {v1, v2, v4, v3, p2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v1, v0, v4

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 1890
    return-void
.end method

.method public deleteDailyData()V
    .locals 3

    .prologue
    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->deleteDailyDataByTime(J)Z

    .line 1420
    return-void
.end method

.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 655
    const-class v0, Lcom/sec/android/app/shealth/spo2/calendar/SpO2CalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 645
    const-string v0, "create_time"

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 640
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v2, "getContentView"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 230
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 231
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030222

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->initView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1

    .prologue
    .line 660
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 209
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onAttach(Landroid/app/Activity;)V

    .line 210
    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onAttach"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    instance-of v1, p1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v1, :cond_0

    .line 212
    check-cast p1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .line 213
    :cond_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 214
    .local v0, "model":Ljava/lang/String;
    const-string v1, "SM-G850F"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SM-G850S"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SM-G850A"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->IMAGE_IDS:[I

    const/4 v2, 0x0

    const v3, 0x7f0201ef

    aput v3, v1, v2

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->IMAGE_IDS:[I

    const/4 v2, 0x1

    const v3, 0x7f0201f0

    aput v3, v1, v2

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->IMAGE_IDS:[I

    const/4 v2, 0x2

    const v3, 0x7f0201f1

    aput v3, v1, v2

    .line 224
    :cond_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v3, 0x7f0900d2

    .line 324
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 326
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SpO2SummaryFragmentNew onConfigurationChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Landroid/content/res/Configuration;->userSetLocale:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setGesturesEnabled(Z)V

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a6b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a69

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 344
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 347
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdBpmUnit:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 351
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->updateErrorDialogContent()V

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsOnConfigChanged:Z

    .line 353
    return-void
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;)V
    .locals 10
    .param p1, "spo2"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 1461
    sget-object v3, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onDataReceived"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->pulseOximetry:F

    cmpg-float v3, v3, v6

    if-gez v3, :cond_1

    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->pulseOximetry:F

    .line 1463
    .local v1, "o2Data":F
    :goto_0
    iget v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->heartRate:I

    .line 1465
    .local v2, "rate":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-nez v3, :cond_2

    .line 1466
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndAnimationInitialize()V

    .line 1511
    :cond_0
    :goto_1
    return-void

    .line 1462
    .end local v1    # "o2Data":F
    .end local v2    # "rate":I
    :cond_1
    iget v3, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->pulseOximetry:F

    invoke-static {v3}, Lcom/sec/android/app/shealth/sensor/Spo2Cal;->spo2cal(F)F

    move-result v1

    goto :goto_0

    .line 1470
    .restart local v1    # "o2Data":F
    .restart local v2    # "rate":I
    :cond_2
    sget-object v3, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ShealthSensorDevice.DataListener onDataReceived: rate :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "o2Data : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isDetected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1472
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z

    if-eqz v3, :cond_7

    .line 1473
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isRetryMeasuring:Z

    .line 1474
    if-eq v2, v7, :cond_0

    .line 1477
    if-gez v2, :cond_3

    const/16 v3, -0x9

    if-ge v2, v3, :cond_4

    :cond_3
    cmpg-float v3, v1, v6

    if-gez v3, :cond_5

    .line 1478
    :cond_4
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z

    .line 1480
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1481
    .local v0, "msg":Landroid/os/Message;
    iput v9, v0, Landroid/os/Message;->what:I

    .line 1482
    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 1483
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 1485
    .end local v0    # "msg":Landroid/os/Message;
    :cond_5
    if-lez v2, :cond_0

    cmpl-float v3, v1, v6

    if-lez v3, :cond_0

    .line 1486
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v4, "com.sec.android.app.shealth.spo2"

    const-string v5, "SP02"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1487
    iput v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    .line 1488
    iput v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mO2Data:F

    .line 1489
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1490
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->measureFinishAnimationMedical()V

    .line 1495
    :goto_2
    sget-object v3, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    const/4 v4, 0x4

    aget v3, v3, v4

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V

    goto/16 :goto_1

    .line 1493
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->measureFinishAnimation()V

    goto :goto_2

    .line 1498
    :cond_7
    if-ne v2, v7, :cond_0

    .line 1499
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z

    .line 1500
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isRetryMeasuring:Z

    if-eqz v3, :cond_8

    .line 1501
    sget-object v3, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->MEASURING:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V

    .line 1504
    :cond_8
    iget v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mPrevError:I

    if-ne v3, v9, :cond_9

    .line 1505
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isRetryMeasuring:Z

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->changeUIReadyToMeasuring(Z)V

    goto/16 :goto_1

    .line 1507
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v4, "com.sec.android.app.shealth.spo2"

    const-string v5, "SP01"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onDataReceivedArray([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;)V
    .locals 4
    .param p1, "spo2Datas"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    .prologue
    .line 1894
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_3

    .line 1895
    aget-object v1, p1, v0

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;

    .line 1896
    .local v1, "sensorData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;
    if-eqz v1, :cond_1

    .line 1899
    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->heartRate:I

    if-ltz v2, :cond_0

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->pulseOximetry:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 1900
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;)V

    .line 1894
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1903
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onDataReceivedArray - valid data"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;)V

    .line 1909
    .end local v1    # "sensorData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;
    :cond_3
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 583
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->clearListener()V

    .line 587
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_1

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 591
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;

    .line 594
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v0, :cond_2

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->releaseAnimation()V

    .line 597
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .line 599
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 600
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 574
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 576
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .line 578
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroyView()V

    .line 579
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 542
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 546
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f02051e

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v0, :cond_1

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 550
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .line 552
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    if-eqz v0, :cond_2

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->clearAnimation()V

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 556
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    if-eqz v0, :cond_3

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 558
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->pool:Landroid/media/SoundPool;

    .line 560
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onPause()V

    .line 561
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 357
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 358
    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onResume"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .line 362
    .local v1, "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isDrawerVisible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 364
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->customizeActionBarButtons(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->initAoudioFile()V

    .line 372
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-nez v2, :cond_1

    .line 419
    :goto_0
    return-void

    .line 368
    .end local v1    # "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of SpO2Activity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 378
    .end local v0    # "e":Ljava/lang/ClassCastException;
    .restart local v1    # "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .line 380
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->wasShowInformationDialog:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getCallFromHome()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "warning_checked_spo2"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    .line 381
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->callShowInformationDialog()V

    .line 382
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->wasShowInformationDialog:Z

    .line 418
    :cond_2
    :goto_1
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsOnConfigChanged:Z

    goto :goto_0

    .line 383
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getShowDeleteDialog()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-boolean v2, v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    if-eqz v2, :cond_7

    .line 384
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mIsOnConfigChanged:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v2

    if-nez v2, :cond_2

    .line 394
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v2, :cond_2

    .line 400
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-boolean v2, v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    if-nez v2, :cond_5

    .line 402
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_1

    .line 404
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getDataCount()I

    move-result v2

    if-nez v2, :cond_6

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 408
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_1

    .line 411
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 416
    :cond_7
    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v3, "Do not dispaly anything!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 650
    const-string v0, "Date"

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 651
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 567
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .line 569
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onStop()V

    .line 570
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 6

    .prologue
    .line 605
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 606
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 607
    .local v0, "sytemChangedTime":J
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->selectedDate:Ljava/util/Date;

    .line 608
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 609
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->updateBpmDataView(Z)V

    .line 611
    .end local v0    # "sytemChangedTime":J
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1525
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CountDownTimer onTick: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1526
    return-void
.end method

.method public onTimeout()V
    .locals 2

    .prologue
    .line 1515
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    if-lez v0, :cond_0

    .line 1521
    :goto_0
    return-void

    .line 1518
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onTimeout"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    const/4 v0, -0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V

    goto :goto_0
.end method

.method public setMeasuringEndUI()V
    .locals 7

    .prologue
    const v6, 0x7f090c06

    const/4 v5, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 1261
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setMeasuringEndUI"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1264
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v0, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1269
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    if-eqz v0, :cond_0

    .line 1270
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1276
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1277
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1278
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1279
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->clearAnimation()V

    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 1284
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f02051e

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1285
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v0, :cond_1

    .line 1286
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 1289
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    if-lez v0, :cond_4

    .line 1290
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1291
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1292
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1293
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1294
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1295
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setFinishedStateMessage()V

    .line 1298
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1299
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1300
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1301
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->clearAnimation()V

    .line 1302
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 1304
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1305
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1306
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1308
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1309
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1318
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 1319
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1320
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1321
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1322
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1323
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1324
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1325
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1326
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscardText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f4d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1327
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetryText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1329
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1330
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1331
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1332
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1333
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "     "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1336
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1338
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->updateBpmDataView(Z)V

    .line 1340
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timerForCenterAnim:Ljava/util/Timer;

    if-eqz v0, :cond_5

    .line 1341
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timerForCenterAnim:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1342
    :cond_5
    return-void

    .line 1273
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1311
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1312
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1313
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1314
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1315
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setEmotionImageNonMedical()V

    .line 1316
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method protected setNextAndPrevDates()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 615
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getPreviousDateOfData(J)Ljava/util/Date;

    move-result-object v1

    .line 616
    .local v1, "prevdate":Ljava/util/Date;
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getNextDateOfData(J)Ljava/util/Date;

    move-result-object v0

    .line 618
    .local v0, "nextdate":Ljava/util/Date;
    if-eqz v1, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    .line 623
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevDate(Ljava/util/Date;)V

    .line 625
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 626
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 627
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 632
    .end local v0    # "nextdate":Ljava/util/Date;
    :goto_1
    return-void

    .line 621
    .restart local v0    # "nextdate":Ljava/util/Date;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    goto :goto_0

    .line 629
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 630
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    if-eqz v0, :cond_2

    .end local v0    # "nextdate":Ljava/util/Date;
    :goto_2
    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    goto :goto_1

    .restart local v0    # "nextdate":Ljava/util/Date;
    :cond_2
    new-instance v0, Ljava/util/Date;

    .end local v0    # "nextdate":Ljava/util/Date;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    goto :goto_2
.end method

.method public startSensorAndDelayUIForDetectedFinger(I)V
    .locals 6
    .param p1, "animation"    # I

    .prologue
    const/4 v5, 0x1

    .line 664
    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startSensorAndDelayUIForDetectedFinger : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->initMeasureStatus()V

    .line 668
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-nez v2, :cond_0

    .line 723
    :goto_0
    return-void

    .line 673
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    if-nez v2, :cond_1

    .line 675
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 676
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, p0, v5}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;Z)V

    .line 684
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v3, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$6;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 706
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    .line 707
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$7;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$7;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    .line 721
    .local v0, "task":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 722
    .local v1, "timer":Ljava/util/Timer;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 680
    .end local v0    # "task":Ljava/util/TimerTask;
    .end local v1    # "timer":Ljava/util/Timer;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, p0, v5}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;Z)V

    goto :goto_1
.end method

.method protected startSensorforSCover(I)V
    .locals 6
    .param p1, "animation"    # I

    .prologue
    .line 475
    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startSensorforSCover : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->initMeasureStatus()V

    .line 479
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-nez v2, :cond_0

    .line 538
    :goto_0
    return-void

    .line 484
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;

    .line 495
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 498
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v3, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$4;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    .line 521
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$5;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    .line 535
    .local v0, "task":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 536
    .local v1, "timer":Ljava/util/Timer;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method public stopSensorAndUpdateUI(IF)V
    .locals 4
    .param p1, "heartRate"    # I
    .param p2, "o2Data"    # F

    .prologue
    const/16 v3, 0x8

    .line 1759
    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopSensorAndUpdateUI"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1761
    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMeasureFailCount:I

    if-lez v1, :cond_1

    .line 1762
    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMeasureFailCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMeasureFailCount:I

    .line 1813
    :cond_0
    :goto_0
    return-void

    .line 1766
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndAnimationInitialize()V

    .line 1768
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    if-eqz v1, :cond_0

    .line 1772
    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    if-nez v1, :cond_2

    .line 1773
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1774
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1775
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1776
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    if-eqz v1, :cond_2

    .line 1777
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 1782
    :cond_2
    if-ltz p1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    if-nez v1, :cond_4

    .line 1785
    if-lez p1, :cond_3

    .line 1786
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    .line 1787
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;FI)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1805
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1806
    :cond_4
    if-gez p1, :cond_0

    .line 1807
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1808
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1809
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1810
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public updateBpmDataView(Z)V
    .locals 11
    .param p1, "isMeasuring"    # Z

    .prologue
    const v10, 0x7f0907ac

    const/4 v9, 0x1

    const/16 v7, 0x8

    const/4 v8, 0x0

    .line 726
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    if-nez v3, :cond_0

    .line 727
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 729
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v2

    .line 730
    .local v2, "spO2Datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/spo2/data/SpO2Data;>;"
    if-eqz p1, :cond_2

    .line 731
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 732
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 819
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setNextAndPrevDates()V

    .line 820
    return-void

    .line 734
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 735
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdO2:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 736
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdBpm:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getHeartrate()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 738
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarFooter:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-double v5, v3

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->moveToPolygon(D)V

    .line 739
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 740
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdTime:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 741
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy/MM/dd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 742
    .local v1, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getStartTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 743
    .local v0, "date":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 744
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 745
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdTime:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdTime:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 748
    .end local v0    # "date":Ljava/lang/String;
    .end local v1    # "format":Ljava/text/SimpleDateFormat;
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    if-nez v3, :cond_3

    .line 749
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 750
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 751
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 752
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 753
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 754
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 755
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 756
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 757
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 758
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 759
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 760
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5

    iget v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I

    if-lez v3, :cond_5

    .line 761
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 762
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 763
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 764
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 765
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 772
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 773
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 775
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 776
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 777
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 778
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 779
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 767
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 768
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 769
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 770
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 781
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 782
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 783
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 784
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 785
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 792
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mlowStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 793
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 795
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 796
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 797
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 798
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 799
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-double v5, v3

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->moveToPolygon(D)V

    .line 800
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getHeartrate()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 801
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    .line 802
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 803
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdO2:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 804
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdBpm:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getHeartrate()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 805
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarFooter:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-double v5, v3

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->moveToPolygon(D)V

    .line 806
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 807
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdTime:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 808
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy/MM/dd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 809
    .restart local v1    # "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getStartTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 810
    .restart local v0    # "date":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 811
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 812
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdTime:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdTime:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 787
    .end local v0    # "date":Ljava/lang/String;
    .end local v1    # "format":Ljava/text/SimpleDateFormat;
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 788
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 789
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus1:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 790
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mMiddleStatus2:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 814
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method
