.class public interface abstract Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;
.super Ljava/lang/Object;
.source "IBaiduAnalyticsStatService.java"


# virtual methods
.method public abstract onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract onEventDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
.end method

.method public abstract onPause(Landroid/content/Context;)V
.end method

.method public abstract onResume(Landroid/content/Context;)V
.end method

.method public abstract setAppChannel(Landroid/content/Context;Ljava/lang/String;Z)V
.end method

.method public abstract setDebugOn(Z)V
.end method
