.class public Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
.super Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
.source "VerticalInputModule.java"


# instance fields
.field protected mTitleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method


# virtual methods
.method protected getAbnormalBackgroundDrawableID()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->vertical_input_module_controller_view_background_orange_pin:I

    return v0
.end method

.method protected getNormalBackgroundDrawableID()I
    .locals 1

    .prologue
    .line 51
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->vertical_input_module_controller_view_background_green_pin:I

    return v0
.end method

.method protected getRootLayoutId()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$layout;->inputmodule_vertical_layout:I

    return v0
.end method

.method protected initAdditionalViews(Landroid/view/View;)V
    .locals 1
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 27
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$id;->inputmodule_tv_title:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->mTitleTextView:Landroid/widget/TextView;

    .line 28
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-void
.end method
