.class public Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
.super Ljava/lang/Object;
.source "ExercisePhotoData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private create_time:J

.field private exerciseId:J

.field private file_path:Ljava/lang/String;

.field private latitude:F

.field private longitude:F

.field private rowId:J

.field private time_zone:I

.field private update_time:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, -0x1

    const/high16 v0, -0x40800000    # -1.0f

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    .line 13
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    .line 14
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    .line 15
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->update_time:J

    .line 19
    const/16 v0, 0x37

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->time_zone:I

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide/16 v1, -0x1

    const/high16 v0, -0x40800000    # -1.0f

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    .line 13
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    .line 14
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    .line 15
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->update_time:J

    .line 19
    const/16 v0, 0x37

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->time_zone:I

    .line 25
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setDatas(Landroid/os/Parcel;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const-wide/16 v1, -0x1

    const/high16 v0, -0x40800000    # -1.0f

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    .line 13
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    .line 14
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    .line 15
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->update_time:J

    .line 19
    const/16 v0, 0x37

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->time_zone:I

    .line 39
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->create_time:J

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public getContentValue()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 117
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 118
    .local v0, "value":Landroid/content/ContentValues;
    const-string v1, "_id"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 119
    const-string v1, "exercise__id"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 120
    const-string v1, "create_time"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->create_time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 121
    const-string/jumbo v1, "update_time"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->update_time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 122
    const-string v1, "latitude"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 123
    const-string v1, "longitude"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 124
    const-string v1, "file_path"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->create_time:J

    return-wide v0
.end method

.method public getExerciseId()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    return-wide v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    return v0
.end method

.method public getLongitude()F
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    return v0
.end method

.method public getRowId()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    return-wide v0
.end method

.method public getTimeZone()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->time_zone:I

    return v0
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 81
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->create_time:J

    .line 82
    return-void
.end method

.method public setDatas(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->create_time:J

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->update_time:J

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->time_zone:I

    .line 113
    return-void
.end method

.method public setExerciseId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    .line 53
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setLocate(FF)V
    .locals 0
    .param p1, "latitude"    # F
    .param p2, "longitude"    # F

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    .line 61
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    .line 62
    return-void
.end method

.method public setRowId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    .line 45
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timezone"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->time_zone:I

    .line 98
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->update_time:J

    .line 90
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->rowId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 137
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->exerciseId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 138
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->create_time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 139
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->update_time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 140
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->latitude:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 141
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->longitude:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->file_path:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->time_zone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    return-void
.end method
