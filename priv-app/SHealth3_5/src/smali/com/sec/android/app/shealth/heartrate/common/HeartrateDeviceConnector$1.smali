.class Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;
.super Ljava/lang/Object;
.source "HeartrateDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 3
    .param p1, "arg0"    # I

    .prologue
    .line 275
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$100(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 277
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "startReceivingData is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->isRecord:Z

    if-eqz v1, :cond_0

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V

    .line 282
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :goto_0
    return-void

    .line 283
    :catch_0
    move-exception v0

    .line 284
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onLeft(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    const/4 v3, 0x0

    .line 291
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$100(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V

    .line 293
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "stopReceivingData_1 is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    .line 308
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    goto :goto_0

    .line 297
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 298
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    goto :goto_0

    .line 299
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 300
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :try_start_3
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    goto :goto_0

    .line 301
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 302
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    goto :goto_0

    .line 303
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 304
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :try_start_5
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    goto :goto_0

    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    throw v1
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 3
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 312
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Response ---------"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response commandId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response errorDescription : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 320
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response onStateChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    return-void
.end method
