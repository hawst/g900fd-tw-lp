.class Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;
.super Ljava/lang/Object;
.source "HomeShortcutManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeShortcutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/data/IconData;

    .line 68
    .local v2, "icondata":Lcom/sec/android/app/shealth/home/data/IconData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getIconType()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 70
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    # getter for: Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->access$000(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)Lcom/sec/android/app/shealth/home/HomeActivity;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    .local v3, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    # getter for: Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->access$000(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)Lcom/sec/android/app/shealth/home/HomeActivity;

    move-result-object v4

    const/16 v5, 0xc9

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 95
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 77
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 79
    const/4 v4, 0x1

    sput-boolean v4, Lcom/sec/android/app/shealth/home/HomeActivity;->isThirdPartyAppLaunched:Z

    .line 81
    :cond_1
    const/4 v0, 0x0

    .line 82
    .local v0, "absolute_action_name":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 83
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    # invokes: Lcom/sec/android/app/shealth/home/HomeShortcutManager;->checkActionForLogging(Ljava/lang/String;)V
    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->access$100(Lcom/sec/android/app/shealth/home/HomeShortcutManager;Ljava/lang/String;)V

    .line 84
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const/high16 v4, 0x20000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 87
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    # getter for: Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->access$000(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)Lcom/sec/android/app/shealth/home/HomeActivity;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    .end local v0    # "absolute_action_name":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    sput-boolean v4, Lcom/sec/android/app/shealth/home/HomeActivity;->isThirdPartyAppLaunched:Z

    .line 92
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
