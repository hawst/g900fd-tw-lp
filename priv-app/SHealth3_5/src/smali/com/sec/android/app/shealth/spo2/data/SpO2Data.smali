.class public Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
.super Ljava/lang/Object;
.source "SpO2Data.java"


# instance fields
.field private avgHeartrate:D

.field private avgSpO2:D

.field private comment:Ljava/lang/String;

.field private createTime:J

.field private deviceName:Ljava/lang/String;

.field private heartrate:F

.field private id:J

.field private spo2:F

.field private startTime:J

.field private userDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->userDeviceId:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->comment:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->deviceName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccessoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getAvgHeartrate()D
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->avgHeartrate:D

    return-wide v0
.end method

.method public getAvgSpO2()D
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->avgSpO2:D

    return-wide v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->createTime:J

    return-wide v0
.end method

.method public getHeartrate()F
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->heartrate:F

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->id:J

    return-wide v0
.end method

.method public getSpo2()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->spo2:F

    return v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->startTime:J

    return-wide v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public setAccessoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->deviceName:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setAvgHeartrate(D)V
    .locals 0
    .param p1, "avgHeartrate"    # D

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->avgHeartrate:D

    .line 84
    return-void
.end method

.method public setAvgSpO2(D)V
    .locals 0
    .param p1, "avgSpO2"    # D

    .prologue
    .line 91
    iput-wide p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->avgSpO2:D

    .line 92
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->comment:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 95
    iput-wide p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->createTime:J

    .line 96
    return-void
.end method

.method public setHeartrate(F)V
    .locals 0
    .param p1, "heartrate"    # F

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->heartrate:F

    .line 68
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->id:J

    .line 23
    return-void
.end method

.method public setSpo2(F)V
    .locals 0
    .param p1, "spo2"    # F

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->spo2:F

    .line 76
    return-void
.end method

.method public setStartTime(J)V
    .locals 0
    .param p1, "startTime"    # J

    .prologue
    .line 42
    iput-wide p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->startTime:J

    .line 43
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->userDeviceId:Ljava/lang/String;

    .line 31
    return-void
.end method
