.class public abstract Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;
.super Ljava/lang/Object;
.source "ListItem.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/RingingCheckable;
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/Selectable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$1;,
        Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;
    }
.end annotation


# instance fields
.field private mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

.field private mIsChecked:Z

.field private mIsSoundEffectEnabled:Z

.field private mNameSelection:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mIsChecked:Z

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mIsSoundEffectEnabled:Z

    .line 172
    return-void
.end method

.method private setNameText(Ljava/lang/String;)V
    .locals 1
    .param p1, "nameText"    # Ljava/lang/String;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->mealNameText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method


# virtual methods
.method protected abstract getDescription(Landroid/content/res/Resources;)Ljava/lang/String;
.end method

.method public getFavoriteWrapper()Landroid/view/View;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    return-object v0
.end method

.method protected abstract getName()Ljava/lang/String;
.end method

.method public getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 43
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_3

    .line 44
    :cond_0
    const v8, 0x7f0300dd

    const/4 v9, 0x0

    invoke-virtual {p1, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 45
    new-instance v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$1;)V

    iput-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    .line 46
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    const v8, 0x7f0803d1

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    iput-object v8, v9, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favorite:Landroid/widget/CheckBox;

    .line 47
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    const v9, 0x7f0803ae

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    .line 48
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {p2, v8}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 49
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 50
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    const v8, 0x7f0803b0

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v9, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->mealNameText:Landroid/widget/TextView;

    .line 51
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    const v8, 0x7f0803b1

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v9, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->caloriesText:Landroid/widget/TextView;

    .line 52
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    const v8, 0x7f0803d0

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    iput-object v8, v9, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    .line 53
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    invoke-virtual {p2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 57
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 58
    .local v4, "resources":Landroid/content/res/Resources;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favorite:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->isFavorite()Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->isFavorite()Z

    move-result v8

    if-eqz v8, :cond_4

    const v0, 0x7f09092b

    .line 66
    .local v0, "actualStringResId":I
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    sget-object v9, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, ""

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    iget-boolean v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mIsChecked:Z

    invoke-virtual {v8, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->getName()Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, "nameText":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mNameSelection:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mNameSelection:Ljava/lang/String;

    const-string v9, "\\s+"

    invoke-virtual {v8, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 71
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mNameSelection:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 72
    .local v6, "selectionStart":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mNameSelection:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int v5, v6, v8

    .line 74
    .local v5, "selectionEnd":I
    const/4 v1, 0x0

    .line 75
    .local v1, "highlightLength":I
    move v3, v5

    .line 77
    .local v3, "pos":I
    :cond_1
    :goto_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    add-int v9, v3, v1

    if-le v8, v9, :cond_5

    add-int v8, v3, v1

    invoke-virtual {v2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isDependentVowel(C)Z

    move-result v8

    if-nez v8, :cond_2

    add-int v8, v3, v1

    invoke-virtual {v2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isSpecialVowel(I)Z

    move-result v8

    if-nez v8, :cond_2

    add-int v8, v3, v1

    invoke-virtual {v2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isSpecialMatra(I)Z

    move-result v8

    if-nez v8, :cond_2

    add-int v8, v3, v1

    invoke-virtual {v2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isNuktaSymbol(I)Z

    move-result v8

    if-nez v8, :cond_2

    add-int v8, v3, v1

    invoke-virtual {v2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isHalant(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 83
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 84
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    add-int v9, v3, v1

    if-le v8, v9, :cond_1

    add-int v8, v3, v1

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isHalant(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 55
    .end local v0    # "actualStringResId":I
    .end local v1    # "highlightLength":I
    .end local v2    # "nameText":Ljava/lang/String;
    .end local v3    # "pos":I
    .end local v4    # "resources":Landroid/content/res/Resources;
    .end local v5    # "selectionEnd":I
    .end local v6    # "selectionStart":I
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iput-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    goto/16 :goto_0

    .line 62
    .restart local v4    # "resources":Landroid/content/res/Resources;
    :cond_4
    const v0, 0x7f09092a

    goto/16 :goto_1

    .line 89
    .restart local v0    # "actualStringResId":I
    .restart local v1    # "highlightLength":I
    .restart local v2    # "nameText":Ljava/lang/String;
    .restart local v3    # "pos":I
    .restart local v5    # "selectionEnd":I
    .restart local v6    # "selectionStart":I
    :cond_5
    add-int v5, v3, v1

    .line 91
    if-ltz v6, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-gt v5, v8, :cond_6

    .line 92
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 93
    .local v7, "spannable":Landroid/text/Spannable;
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    const v9, 0x7f0701d1

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v9, 0x21

    invoke-interface {v7, v8, v6, v5, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 96
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->mealNameText:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    .end local v1    # "highlightLength":I
    .end local v3    # "pos":I
    .end local v5    # "selectionEnd":I
    .end local v6    # "selectionStart":I
    .end local v7    # "spannable":Landroid/text/Spannable;
    :goto_3
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v8, v8, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->caloriesText:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->getDescription(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    return-object p2

    .line 98
    .restart local v1    # "highlightLength":I
    .restart local v3    # "pos":I
    .restart local v5    # "selectionEnd":I
    .restart local v6    # "selectionStart":I
    :cond_6
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->setNameText(Ljava/lang/String;)V

    goto :goto_3

    .line 101
    .end local v1    # "highlightLength":I
    .end local v3    # "pos":I
    .end local v5    # "selectionEnd":I
    .end local v6    # "selectionStart":I
    :cond_7
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->setNameText(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public isCheckBoxSoundEffectsEnabled()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mIsSoundEffectEnabled:Z

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mIsChecked:Z

    return v0
.end method

.method public setCheckBoxSoundEffectsEnabled(Z)V
    .locals 0
    .param p1, "isEnabled"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mIsSoundEffectEnabled:Z

    .line 151
    return-void
.end method

.method public setCheckBoxVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 160
    return-void
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "isChecked"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mIsChecked:Z

    .line 142
    return-void
.end method

.method public setCheckedWithoutSoundEffect(Z)V
    .locals 1
    .param p1, "isChecked"    # Z

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->setChecked(Z)V

    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->setCheckBoxSoundEffectsEnabled(Z)V

    .line 147
    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 165
    return-void
.end method

.method public setSelection(Ljava/lang/String;)V
    .locals 0
    .param p1, "nameSelection"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->mNameSelection:Ljava/lang/String;

    .line 170
    return-void
.end method
