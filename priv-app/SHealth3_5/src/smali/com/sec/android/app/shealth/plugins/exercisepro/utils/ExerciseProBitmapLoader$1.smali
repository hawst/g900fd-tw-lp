.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;
.super Ljava/lang/Object;
.source "ExerciseProBitmapLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->getBitmapImage(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$h:I

.field final synthetic val$imgView:Landroid/widget/ImageView;

.field final synthetic val$url:Ljava/lang/String;

.field final synthetic val$w:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Landroid/content/Context;Ljava/lang/String;IILandroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$url:Ljava/lang/String;

    iput p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$w:I

    iput p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$h:I

    iput-object p6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$imgView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 52
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$url:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$w:I

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$h:I

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getImageThumbnail(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 53
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;)V

    .line 54
    .local v1, "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;
    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->bitmap:Landroid/graphics/Bitmap;

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$url:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->url:Ljava/lang/String;

    .line 56
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->val$imgView:Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->imageView:Landroid/widget/ImageView;

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 58
    return-void
.end method
