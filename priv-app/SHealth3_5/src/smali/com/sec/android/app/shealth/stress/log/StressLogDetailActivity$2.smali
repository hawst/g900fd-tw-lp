.class Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;
.super Ljava/lang/Object;
.source "StressLogDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->showDeletePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)Lcom/sec/android/app/shealth/stress/data/StressData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/data/StressData;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->deleteRowById(Ljava/lang/String;)Z

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->setResult(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->finish()V

    .line 156
    return-void
.end method
