.class Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;
.super Ljava/lang/Object;
.source "MaskedImageCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MaskedBitmapsProvider"
.end annotation


# instance fields
.field mCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 6

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x8

    div-long v0, v2, v4

    .line 175
    .local v0, "memory":J
    new-instance v2, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider$1;

    long-to-int v3, v0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider$1;-><init>(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;->mCache:Landroid/support/v4/util/LruCache;

    .line 182
    return-void
.end method


# virtual methods
.method addToCache(Landroid/graphics/Bitmap;Ljava/util/List;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p2, "key":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p2, p1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    return-void
.end method

.method clearLruCache()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 191
    return-void
.end method

.method getImage(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "key":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method
