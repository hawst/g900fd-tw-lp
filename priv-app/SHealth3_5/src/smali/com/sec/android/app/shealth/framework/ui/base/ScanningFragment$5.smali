.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 32

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->scan:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, -0x5efc

    aput v30, v2, v29

    const/16 v29, -0x2b

    aput v29, v2, v28

    const/16 v28, -0x20

    aput v28, v2, v27

    const/16 v27, -0x80

    aput v27, v2, v26

    const/16 v26, -0x44

    aput v26, v2, v25

    const/16 v25, -0x2f

    aput v25, v2, v24

    const/16 v24, -0x6d

    aput v24, v2, v23

    const/16 v23, -0x73

    aput v23, v2, v22

    const/16 v22, -0x13e8

    aput v22, v2, v21

    const/16 v21, -0x64

    aput v21, v2, v20

    const/16 v20, -0x19

    aput v20, v2, v19

    const/16 v19, -0x5b

    aput v19, v2, v18

    const/16 v18, -0x4f

    aput v18, v2, v17

    const/16 v17, -0x4e

    aput v17, v2, v16

    const/16 v16, -0x6e8d

    aput v16, v2, v15

    const/16 v15, -0x1d

    aput v15, v2, v14

    const/16 v14, 0x1d5f

    aput v14, v2, v13

    const/16 v13, -0x738d

    aput v13, v2, v12

    const/16 v12, -0x13

    aput v12, v2, v11

    const/16 v11, -0x5e

    aput v11, v2, v10

    const/16 v10, -0x66

    aput v10, v2, v9

    const/16 v9, -0x22

    aput v9, v2, v8

    const/16 v8, -0x42bb

    aput v8, v2, v7

    const/16 v7, -0x6d

    aput v7, v2, v6

    const/16 v6, 0x5c54

    aput v6, v2, v5

    const/16 v5, 0x4f33

    aput v5, v2, v3

    const/16 v3, 0x702c

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0x5e94

    aput v31, v1, v30

    const/16 v30, -0x5f

    aput v30, v1, v29

    const/16 v29, -0x74

    aput v29, v1, v28

    const/16 v28, -0x1f

    aput v28, v1, v27

    const/16 v27, -0x27

    aput v27, v1, v26

    const/16 v26, -0x47

    aput v26, v1, v25

    const/16 v25, -0x20

    aput v25, v1, v24

    const/16 v24, -0x5d

    aput v24, v1, v23

    const/16 v23, -0x1398

    aput v23, v1, v22

    const/16 v22, -0x14

    aput v22, v1, v21

    const/16 v21, -0x7a

    aput v21, v1, v20

    const/16 v20, -0x75

    aput v20, v1, v19

    const/16 v19, -0x2b

    aput v19, v1, v18

    const/16 v18, -0x25

    aput v18, v1, v17

    const/16 v17, -0x6ee4

    aput v17, v1, v16

    const/16 v16, -0x6f

    aput v16, v1, v15

    const/16 v15, 0x1d3b

    aput v15, v1, v14

    const/16 v14, -0x73e3

    aput v14, v1, v13

    const/16 v13, -0x74

    aput v13, v1, v12

    const/16 v12, -0x74

    aput v12, v1, v11

    const/4 v11, -0x7

    aput v11, v1, v10

    const/16 v10, -0x45

    aput v10, v1, v9

    const/16 v9, -0x42ca

    aput v9, v1, v8

    const/16 v8, -0x43

    aput v8, v1, v7

    const/16 v7, 0x5c39

    aput v7, v1, v6

    const/16 v6, 0x4f5c

    aput v6, v1, v5

    const/16 v5, 0x704f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/16 v8, -0x568e

    aput v8, v2, v7

    const/16 v7, -0x68

    aput v7, v2, v6

    const/16 v6, -0x1a9b

    aput v6, v2, v3

    const/16 v3, -0x4a

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/16 v9, -0x56c0

    aput v9, v1, v8

    const/16 v8, -0x57

    aput v8, v1, v7

    const/16 v7, -0x1aab

    aput v7, v1, v6

    const/16 v6, -0x1b

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->toggleScanState()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->refreshFragmentFocusables()V

    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method
