.class Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "HomeWallpaperChooser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GalleryImageAdapter"
.end annotation


# instance fields
.field images:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 205
    .local p2, "images":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/drawable/Drawable;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 206
    iput-object p2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->images:Ljava/util/ArrayList;

    .line 207
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->images:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->images:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 221
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 226
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 227
    .local v1, "image":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/Gallery$LayoutParams;

    const/16 v3, 0x12c

    const/16 v4, 0x190

    invoke-direct {v2, v3, v4}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 229
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->images:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    const v3, 0x7f090fae

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$000(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 231
    const/4 v0, 0x0

    .line 232
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->images:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 233
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 235
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$200(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)I

    move-result v2

    if-ne p1, v2, :cond_0

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->setGalleryBackgroundFocus(Landroid/view/View;)V

    .line 238
    :cond_0
    return-object v1
.end method
