.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showAutoCompletePopup(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0

    .prologue
    .line 826
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "itemContent"    # Ljava/lang/String;
    .param p3, "popupWindow"    # Landroid/widget/PopupWindow;

    .prologue
    .line 831
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/app/Activity;)V

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->setSearchText(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2200(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->performSearchFood()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 834
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 835
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_TO_AUTO_COMPLETE_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 836
    return-void
.end method
