.class Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceCheckedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeFragment;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;->this$0:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 233
    if-nez p2, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "com.sec.android.app.shealth.pedometer.viewstepcount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    const-string v1, "DeviceCheckedReceiver"

    const-string v2, "Changed device"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;->this$0:Lcom/sec/android/app/shealth/home/HomeFragment;

    # getter for: Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeFragment;->access$400(Lcom/sec/android/app/shealth/home/HomeFragment;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;->this$0:Lcom/sec/android/app/shealth/home/HomeFragment;

    # getter for: Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeFragment;->access$400(Lcom/sec/android/app/shealth/home/HomeFragment;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->updateView(Ljava/lang/Boolean;)V

    goto :goto_0
.end method
