.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;
.super Landroid/database/ContentObserver;
.source "HeartrateLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mPrevCount:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mPrevCount:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$002(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;I)I

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->executeQuery()Landroid/database/Cursor;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mCursor:Landroid/database/Cursor;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$202(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->refreshAdapter()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->notifyDataSetChanged()V

    .line 78
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->invalidateOptionsMenu()V

    .line 79
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->refreshAdapter()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)V

    goto :goto_0
.end method
