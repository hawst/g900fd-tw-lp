.class Lcom/sec/android/app/shealth/common/utils/FocusUtils$SHealthFocusabilityComparator;
.super Ljava/lang/Object;
.source "FocusUtils.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/FocusUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SHealthFocusabilityComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/utils/FocusUtils$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$1;

    .prologue
    .line 1053
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$SHealthFocusabilityComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/view/View;Landroid/view/View;)I
    .locals 11
    .param p1, "firstComparableView"    # Landroid/view/View;
    .param p2, "secondComparableView"    # Landroid/view/View;

    .prologue
    const/16 v10, 0x46

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v4, -0x1

    const/16 v9, -0x46

    .line 1064
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRectDescribingViewFully(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1065
    .local v0, "firstViewGlobalRect":Landroid/graphics/Rect;
    invoke-static {p2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRectDescribingViewFully(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1067
    .local v1, "secondViewGlobalRect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v8

    sub-int v3, v7, v8

    .line 1069
    .local v3, "yDiff":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v8

    sub-int v2, v7, v8

    .line 1071
    .local v2, "xDiff":I
    if-le v3, v10, :cond_1

    .line 1084
    :cond_0
    :goto_0
    return v4

    .line 1074
    :cond_1
    if-ge v3, v9, :cond_2

    move v4, v5

    .line 1075
    goto :goto_0

    .line 1077
    :cond_2
    if-gtz v2, :cond_0

    .line 1080
    if-gez v2, :cond_3

    move v4, v5

    .line 1081
    goto :goto_0

    .line 1082
    :cond_3
    if-le v3, v10, :cond_4

    if-lt v3, v9, :cond_5

    :cond_4
    if-nez v2, :cond_5

    move v4, v6

    .line 1083
    goto :goto_0

    :cond_5
    move v4, v6

    .line 1084
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 1053
    check-cast p1, Landroid/view/View;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/view/View;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$SHealthFocusabilityComparator;->compare(Landroid/view/View;Landroid/view/View;)I

    move-result v0

    return v0
.end method
