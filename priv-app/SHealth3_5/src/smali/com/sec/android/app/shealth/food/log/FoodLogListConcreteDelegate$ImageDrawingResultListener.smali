.class Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;
.super Ljava/lang/Object;
.source "FoodLogListConcreteDelegate.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageDrawingResultListener"
.end annotation


# instance fields
.field private mealId:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

.field private view:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;J)V
    .locals 0
    .param p2, "view"    # Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    .param p3, "mealId"    # J

    .prologue
    .line 490
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;->view:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    .line 492
    iput-wide p3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;->mealId:J

    .line 493
    return-void
.end method


# virtual methods
.method public onDrawingResult(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 497
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;->view:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;->view:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftImageView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    # getter for: Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskDrawingCallbackProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->access$200(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;->mealId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->removeCallback(Ljava/lang/Object;)V

    .line 503
    return-void
.end method
