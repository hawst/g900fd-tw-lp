.class public Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$SummaryToGraphFragmentSwitchController;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;
.source "GraphLogUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SummaryToGraphFragmentSwitchController"
.end annotation


# instance fields
.field private switchable:Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;)V
    .locals 0
    .param p1, "switchable"    # Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$SummaryToGraphFragmentSwitchController;->switchable:Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;

    .line 83
    return-void
.end method


# virtual methods
.method public onClickAction(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$SummaryToGraphFragmentSwitchController;->switchable:Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;->switchFragmentToGraph()V

    .line 88
    return-void
.end method
