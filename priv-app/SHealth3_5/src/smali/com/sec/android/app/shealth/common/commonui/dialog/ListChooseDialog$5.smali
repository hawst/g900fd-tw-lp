.class Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;
.super Ljava/lang/Object;
.source "ListChooseDialog.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/view/View;)V
    .locals 3
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 276
    .local v0, "position":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->getCheckedItem(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->isChecked:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 282
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->isChecked:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    goto :goto_0
.end method
