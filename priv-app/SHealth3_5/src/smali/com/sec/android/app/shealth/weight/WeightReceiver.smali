.class public Lcom/sec/android/app/shealth/weight/WeightReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WeightReceiver.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/weight/WeightReceiver;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/weight/WeightReceiver;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.shealth.action.RESET_WEIGHT_DATA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    sget-object v1, Lcom/sec/android/app/shealth/weight/WeightReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v2, "ACTION_RESET_WEIGHT_DATA"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 44
    .local v0, "weightDao":Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    invoke-interface {v0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->deleteAllGoals()I

    .line 45
    invoke-interface {v0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->deleteAll()I

    .line 47
    .end local v0    # "weightDao":Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    :cond_0
    return-void
.end method
