.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;
.super Landroid/os/Handler;
.source "UvSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 230
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setReadyUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$000(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V

    goto :goto_0

    .line 233
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setMeasuringUI(I)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->fromMeasuringState:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$102(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)Z

    goto :goto_0

    .line 239
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setMeasuringWarningUI(I)V

    goto :goto_0

    .line 242
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setMeasuringFailUI(I)V

    goto :goto_0

    .line 245
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setMeasuringEndUI()V

    goto :goto_0

    .line 248
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->dataCollectingUI(II)V

    goto :goto_0

    .line 251
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showFailurePopup()V

    goto :goto_0

    .line 228
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
