.class Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)V
    .locals 0

    .prologue
    .line 692
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->moveToClosestGradation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1500(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V

    .line 703
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 695
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->moveToClosestGradation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1500(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V

    .line 697
    return-void
.end method
