.class public Lcom/sec/android/app/shealth/backup/server/BackupNotifier;
.super Ljava/lang/Object;
.source "BackupNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/backup/server/BackupNotifier$OnBackupListener;,
        Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;,
        Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;
    }
.end annotation


# static fields
.field private static isBlocked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier;->isBlocked:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static blockBackup()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier;->isBlocked:Z

    .line 109
    return-void
.end method

.method public static unblockBackup()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier;->isBlocked:Z

    .line 114
    return-void
.end method
