.class public final enum Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;
.super Ljava/lang/Enum;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdditionalDrawingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

.field public static final enum EMPTY:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

.field public static final enum NORMAL_RANGE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 104
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->EMPTY:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    .line 105
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    const-string v1, "NORMAL_RANGE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->NORMAL_RANGE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    .line 103
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->EMPTY:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->NORMAL_RANGE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 103
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    return-object v0
.end method
