.class Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$3;
.super Ljava/lang/Object;
.source "UvDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 390
    const/16 v1, 0x14

    if-ne p1, v1, :cond_0

    move-object v0, p2

    .line 391
    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;

    .line 393
    .local v0, "uv":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;->index:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 394
    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onReceived valid data "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->mUvSensorListener:Lcom/sec/android/app/shealth/uv/UvSensorListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$500(Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;)Lcom/sec/android/app/shealth/uv/UvSensorListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/uv/UvSensorListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;)V

    .line 398
    .end local v0    # "uv":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;
    :cond_0
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 402
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->mUvSensorListener:Lcom/sec/android/app/shealth/uv/UvSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$500(Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;)Lcom/sec/android/app/shealth/uv/UvSensorListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/app/shealth/uv/UvSensorListener;->onDataReceivedArray([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;)V

    .line 405
    :cond_0
    return-void
.end method

.method public onStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 380
    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    return-void
.end method

.method public onStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 385
    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStopped : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    return-void
.end method
