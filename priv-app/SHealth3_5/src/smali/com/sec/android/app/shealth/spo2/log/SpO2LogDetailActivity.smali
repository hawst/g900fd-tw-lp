.class public Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "SpO2LogDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$MarginSpanForNotesText;
    }
.end annotation


# static fields
.field public static final ACTIONBAR_EDIT:I = 0x0

.field public static final EDIT:Ljava/lang/String; = "Edit"


# instance fields
.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field private mAccessoryType:Landroid/widget/TextView;

.field private mComment:Landroid/widget/TextView;

.field private mHighStatus:Landroid/widget/TextView;

.field private mLine:Landroid/view/View;

.field private mLowStatus:Landroid/widget/TextView;

.field private mNotes:Landroid/widget/ImageView;

.field private mPulse:Landroid/widget/TextView;

.field private mSpO2:Landroid/widget/TextView;

.field private mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

.field private mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

.field private mSpan:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$MarginSpanForNotesText;

.field private mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

.field private mTimeDate:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 59
    new-instance v0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$MarginSpanForNotesText;

    const/4 v1, 0x1

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$MarginSpanForNotesText;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpan:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$MarginSpanForNotesText;

    .line 161
    new-instance v0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    .line 196
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mPulse:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    return-object v0
.end method

.method private initView(Ljava/lang/String;)V
    .locals 9
    .param p1, "spo2Id"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 68
    invoke-static {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    .line 71
    const v3, 0x7f08052e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    .line 72
    const v3, 0x7f08052f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    .line 73
    const v3, 0x7f08098d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mPulse:Landroid/widget/TextView;

    .line 74
    const v3, 0x7f080989

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2:Landroid/widget/TextView;

    .line 75
    const v3, 0x7f08098a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    .line 76
    const v3, 0x7f080539

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mComment:Landroid/widget/TextView;

    .line 77
    const v3, 0x7f08098e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mLine:Landroid/view/View;

    .line 78
    const v3, 0x7f080990

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mNotes:Landroid/widget/ImageView;

    .line 79
    const v3, 0x7f08098b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mLowStatus:Landroid/widget/TextView;

    .line 80
    const v3, 0x7f08098c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mHighStatus:Landroid/widget/TextView;

    .line 81
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getUserDeviceId()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "userDeviceId":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090a8c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getAccessoryName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mPulse:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getHeartrate()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {p0, v3}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mLowStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mNotes:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mLine:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getComment()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getComment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "comment":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 112
    new-instance v1, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getComment()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 113
    .local v1, "spannableString":Landroid/text/SpannableString;
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpan:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$MarginSpanForNotesText;

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v4

    invoke-virtual {v1, v3, v7, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 114
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mComment:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mNotes:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 123
    .end local v0    # "comment":Ljava/lang/String;
    .end local v1    # "spannableString":Landroid/text/SpannableString;
    :goto_1
    return-void

    .line 101
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 103
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->moveToPolygon(D)V

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mLowStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 117
    .restart local v0    # "comment":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mLine:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 120
    .end local v0    # "comment":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mLine:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private showDeletePopup()V
    .locals 3

    .prologue
    .line 185
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09078e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 195
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 153
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f09005b

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020299

    const v6, 0x7f090040

    iget-object v7, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 158
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f03021c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->setContentView(I)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SPO2_ID_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->initView(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100023

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 139
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 148
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 141
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->prepareShareView()V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.spo2"

    const-string v2, "SP09"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->showDeletePopup()V

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x7f080c8d
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 127
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    return-void
.end method
