.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$1;
.super Ljava/lang/Object;
.source "CompatibleAccessoryDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "http"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 127
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 130
    .local v0, "i":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->isBrowserSupported(Landroid/content/Intent;)Z
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->access$000(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 140
    :goto_1
    return-void

    .line 124
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 138
    .restart local v0    # "i":Landroid/content/Intent;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    const v4, 0x7f090805

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
