.class public final enum Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;
.super Ljava/lang/Enum;
.source "CommonDaoWithDateTime.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SqlAggregateFunction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

.field public static final enum AVG:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

.field public static final enum MAX:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

.field public static final enum MIN:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

.field public static final enum SUM:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    const-string v1, "SUM"

    const-string v2, "SUM"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->SUM:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    const-string v1, "AVG"

    const-string v2, "AVG"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->AVG:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    const-string v1, "MIN"

    const-string v2, "MIN"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->MIN:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    const-string v1, "MAX"

    const-string v2, "MAX"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->MAX:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    .line 30
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    sget-object v1, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->SUM:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->AVG:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->MIN:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->MAX:Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->$VALUES:[Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->value:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->$VALUES:[Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->value:Ljava/lang/String;

    return-object v0
.end method
