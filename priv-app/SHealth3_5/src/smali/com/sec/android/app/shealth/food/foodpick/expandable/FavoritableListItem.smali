.class interface abstract Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritableListItem;
.super Ljava/lang/Object;
.source "FavoritableListItem.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem;


# virtual methods
.method public abstract addToFavorites()V
.end method

.method public abstract getFavoriteButton()Landroid/view/View;
.end method

.method public abstract getFavoriteWrapper()Landroid/view/View;
.end method

.method public abstract isFavorite()Z
.end method

.method public abstract removeFromFavorites()V
.end method
