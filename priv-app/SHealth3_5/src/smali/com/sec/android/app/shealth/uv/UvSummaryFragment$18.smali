.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->scaleAnimatorStarterNoSKin(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0

    .prologue
    .line 1961
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1985
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1986
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1987
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2502(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 1988
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1975
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1976
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1977
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1978
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2502(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 1980
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setSecondCenterIconAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2600(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    .line 1981
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 1971
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 1965
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201e0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1967
    return-void
.end method
