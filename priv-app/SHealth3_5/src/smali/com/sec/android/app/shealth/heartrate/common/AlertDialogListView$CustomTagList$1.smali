.class Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;
.super Ljava/lang/Object;
.source "AlertDialogListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;I)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 229
    const v2, 0x7f0805f6

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    invoke-virtual {v2, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->val$position:I

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->checkPosition:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$202(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;I)I

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iget-object v3, v3, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v3, v3, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iget-object v4, v4, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->checkPosition:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$200(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->mPref:Landroid/content/SharedPreferences;
    invoke-static {v5}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->access$300(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;)Landroid/content/SharedPreferences;

    move-result-object v5

    # invokes: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->setcheckedPosition(Landroid/content/Context;ILandroid/content/SharedPreferences;)V
    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$400(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;Landroid/content/Context;ILandroid/content/SharedPreferences;)V

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->access$500(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;)Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->val$position:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 233
    .local v1, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 234
    .local v0, "hasTag":Ljava/lang/Boolean;
    iget-object v2, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v2, v3, :cond_1

    .line 235
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 239
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mDialogItemClickListener:Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget v4, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    iget v5, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagIconId:I

    iget-object v6, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;->onListItemClicked(ZIILjava/lang/String;)V

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$100(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$100(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 243
    :cond_0
    return-void

    .line 237
    :cond_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method
