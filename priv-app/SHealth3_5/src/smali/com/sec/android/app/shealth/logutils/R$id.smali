.class public final Lcom/sec/android/app/shealth/logutils/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/logutils/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Nodevices:I = 0x7f0808d4

.field public static final Scanning_layout:I = 0x7f0808d1

.field public static final ab_action_title_holder:I = 0x7f080301

.field public static final ab_btn_parent:I = 0x7f08030d

.field public static final accessary_connection_icon:I = 0x7f08032c

.field public static final accessary_name:I = 0x7f08067b

.field public static final actionBarCancelDone:I = 0x7f080311

.field public static final actionBarMain:I = 0x7f0802ff

.field public static final actionBarParent:I = 0x7f0802fe

.field public static final actionBarShareLayout:I = 0x7f08030f

.field public static final actionBarSpinner:I = 0x7f080024

.field public static final actionBardShareTag:I = 0x7f080310

.field public static final actionContentBarLayout:I = 0x7f080300

.field public static final action_bar_title_icon:I = 0x7f080308

.field public static final action_select_button_layout:I = 0x7f080307

.field public static final action_up_button:I = 0x7f080306

.field public static final action_up_button_layout:I = 0x7f080305

.field public static final actionbar_switch:I = 0x7f080025

.field public static final add_button:I = 0x7f0800b1

.field public static final alert_text:I = 0x7f080040

.field public static final alert_title_container:I = 0x7f080032

.field public static final animated_view:I = 0x7f08036b

.field public static final app_icon:I = 0x7f080338

.field public static final bg_activity:I = 0x7f08008f

.field public static final bg_image:I = 0x7f08064e

.field public static final blank_label:I = 0x7f080038

.field public static final blood_glucose_radio_group:I = 0x7f08003d

.field public static final blood_glucose_radio_group_layout:I = 0x7f08004c

.field public static final blood_glucose_switch:I = 0x7f08003c

.field public static final btn_cancel:I = 0x7f0800ad

.field public static final btn_date:I = 0x7f0803a4

.field public static final btn_next_period:I = 0x7f0800ab

.field public static final btn_previous_period:I = 0x7f0800a8

.field public static final btn_time:I = 0x7f0803a5

.field public static final btns_margin:I = 0x7f0800b2

.field public static final btns_margin2:I = 0x7f0800b0

.field public static final btns_margin_bottom:I = 0x7f0800b6

.field public static final button_icon:I = 0x7f080313

.field public static final button_text:I = 0x7f080314

.field public static final buttons_layout:I = 0x7f0804d0

.field public static final calendar_button_button:I = 0x7f0800a3

.field public static final calendar_button_button1:I = 0x7f08009e

.field public static final calendar_button_medal:I = 0x7f08009d

.field public static final calendar_button_text:I = 0x7f0800a4

.field public static final calendar_button_text1:I = 0x7f08009f

.field public static final calendar_currentmonth:I = 0x7f0800a9

.field public static final calendar_currentyear:I = 0x7f0800aa

.field public static final calendar_date:I = 0x7f0800a6

.field public static final calendar_pager:I = 0x7f0800ac

.field public static final calendar_root_layout:I = 0x7f0800a5

.field public static final calendar_today:I = 0x7f0800a7

.field public static final cancel_button:I = 0x7f0800af

.field public static final cancel_ok_buttons_container:I = 0x7f080034

.field public static final cancel_ok_buttons_layout:I = 0x7f0800ae

.field public static final cancel_ok_buttons_layout_bottom:I = 0x7f0800b4

.field public static final change_period_day_btn:I = 0x7f0804d2

.field public static final change_period_hour_btn:I = 0x7f0804d1

.field public static final change_period_month_btn:I = 0x7f0804d3

.field public static final checkBox1:I = 0x7f08004a

.field public static final check_box_all:I = 0x7f08054d

.field public static final check_box_all_container:I = 0x7f08054c

.field public static final circle_view_stub:I = 0x7f0803b7

.field public static final clickRemove:I = 0x7f08000a

.field public static final container:I = 0x7f080632

.field public static final contentParentLayout:I = 0x7f080043

.field public static final contentParentLayout_total:I = 0x7f080035

.field public static final contentParentLayout_value:I = 0x7f080036

.field public static final content_container:I = 0x7f080033

.field public static final content_frame:I = 0x7f080091

.field public static final contentui_dialog_cancel_bottom:I = 0x7f0800b5

.field public static final contentui_dialog_ok_bottom:I = 0x7f0800b7

.field public static final contentui_list_description:I = 0x7f080046

.field public static final contentui_list_layout:I = 0x7f080045

.field public static final custom_view_holder:I = 0x7f08030b

.field public static final date:I = 0x7f08003a

.field public static final date_bar:I = 0x7f0804cf

.field public static final date_divider:I = 0x7f080631

.field public static final date_layout:I = 0x7f08031e

.field public static final date_selector:I = 0x7f080a07

.field public static final date_selector_divider:I = 0x7f080324

.field public static final date_switcher:I = 0x7f080321

.field public static final date_time_buttons:I = 0x7f0803e8

.field public static final dateandtime:I = 0x7f08004b

.field public static final datepicker:I = 0x7f080325

.field public static final days_of_week_container:I = 0x7f0800a0

.field public static final detected_devices_header:I = 0x7f0808cf

.field public static final device_connected_status:I = 0x7f080020

.field public static final device_image_name:I = 0x7f08001d

.field public static final device_name:I = 0x7f08001f

.field public static final device_name_layout:I = 0x7f08001e

.field public static final device_type_text:I = 0x7f0808d0

.field public static final dialog_top_text:I = 0x7f08032f

.field public static final divider:I = 0x7f080129

.field public static final drawer_layout:I = 0x7f080090

.field public static final drawer_menu_layout:I = 0x7f080339

.field public static final empty:I = 0x7f080009

.field public static final empty_layout:I = 0x7f080303

.field public static final expandable_listView:I = 0x7f08065a

.field public static final first_legend_mark_layout:I = 0x7f080644

.field public static final first_sub_tab:I = 0x7f0802f9

.field public static final first_sub_tab_rd_btn:I = 0x7f0802fa

.field public static final first_text:I = 0x7f080528

.field public static final flingRemove:I = 0x7f08000b

.field public static final food_tracker_zoom_in_popup:I = 0x7f08052a

.field public static final foreground_image_view:I = 0x7f08063c

.field public static final fringe_layout:I = 0x7f080031

.field public static final general_view_container:I = 0x7f08036d

.field public static final goal:I = 0x7f080c99

.field public static final gradation_view:I = 0x7f08063b

.field public static final graph_information_area_amount_layout:I = 0x7f0804cd

.field public static final graph_information_area_amount_view:I = 0x7f0803b6

.field public static final graph_information_area_date_value:I = 0x7f0803b5

.field public static final graph_information_area_item_subtitle:I = 0x7f0804ca

.field public static final graph_information_area_item_title_area:I = 0x7f0804cb

.field public static final graph_information_area_item_title_view_avg_label:I = 0x7f0804cc

.field public static final graph_information_area_unit_view:I = 0x7f0803b9

.field public static final graph_information_area_unit_view_avg_label:I = 0x7f0804ce

.field public static final graph_information_item_container:I = 0x7f0804c9

.field public static final graph_triangular_switch_fragment_button:I = 0x7f080646

.field public static final grid:I = 0x7f0800a2

.field public static final gridInfoPreview:I = 0x7f0804d4

.field public static final gridview:I = 0x7f0808f7

.field public static final gv_tag_icon_list:I = 0x7f080639

.field public static final header_layout:I = 0x7f080802

.field public static final health_care_summary_content_left_bar:I = 0x7f0804d6

.field public static final health_care_summary_content_right_bar:I = 0x7f0804d7

.field public static final help:I = 0x7f080c8a

.field public static final hoverGridChild:I = 0x7f080526

.field public static final icon:I = 0x7f080525

.field public static final icon_boundary:I = 0x7f080635

.field public static final information:I = 0x7f08036a

.field public static final information_container:I = 0x7f08036c

.field public static final input_activity_root_layout:I = 0x7f080630

.field public static final inputmodule_btn_decrease:I = 0x7f08063a

.field public static final inputmodule_btn_increase:I = 0x7f08063d

.field public static final inputmodule_et_value:I = 0x7f080472

.field public static final inputmodule_horizontal_top_layout:I = 0x7f08063e

.field public static final inputmodule_tv_title:I = 0x7f080641

.field public static final inputmodule_tv_unit:I = 0x7f080473

.field public static final label:I = 0x7f080060

.field public static final label_container_left:I = 0x7f0804da

.field public static final label_container_right:I = 0x7f0804dd

.field public static final layout_left_arrow:I = 0x7f08031f

.field public static final layout_right_arrow:I = 0x7f080322

.field public static final layout_root:I = 0x7f08008e

.field public static final layout_text:I = 0x7f080386

.field public static final left:I = 0x7f080006

.field public static final left_arrow:I = 0x7f080320

.field public static final left_drawer:I = 0x7f080092

.field public static final legen_frame_container:I = 0x7f080642

.field public static final legend_container:I = 0x7f080371

.field public static final legend_mark_container:I = 0x7f080643

.field public static final list:I = 0x7f080093

.field public static final listItemParent:I = 0x7f0808aa

.field public static final listParent:I = 0x7f08032d

.field public static final list_in_dialog:I = 0x7f080331

.field public static final list_item_text:I = 0x7f08033a

.field public static final list_title:I = 0x7f080647

.field public static final listitem_layout:I = 0x7f080337

.field public static final listview:I = 0x7f080047

.field public static final ln_container:I = 0x7f080634

.field public static final ln_tag_avg:I = 0x7f080652

.field public static final ln_tag_icon_2:I = 0x7f080638

.field public static final ln_tag_max:I = 0x7f080655

.field public static final ln_tag_max_min_avg:I = 0x7f080651

.field public static final loading_img:I = 0x7f08064b

.field public static final loading_popup_background:I = 0x7f08064a

.field public static final log_list_accessory_image:I = 0x7f08066b

.field public static final log_list_bottom_text:I = 0x7f080668

.field public static final log_list_center_text:I = 0x7f080667

.field public static final log_list_data_row:I = 0x7f080661

.field public static final log_list_data_row_medal_image:I = 0x7f08066a

.field public static final log_list_delete_check_box:I = 0x7f080663

.field public static final log_list_expand_button:I = 0x7f080374

.field public static final log_list_group_header_left_text:I = 0x7f080372

.field public static final log_list_icons_container:I = 0x7f080669

.field public static final log_list_item_divider:I = 0x7f080662

.field public static final log_list_left_text:I = 0x7f080666

.field public static final log_list_memo_image:I = 0x7f08066c

.field public static final log_list_right_groupe_text:I = 0x7f080373

.field public static final log_list_right_text:I = 0x7f08066e

.field public static final log_list_row_left_image:I = 0x7f080664

.field public static final log_list_row_left_text_container:I = 0x7f080665

.field public static final log_list_team_header:I = 0x7f08065c

.field public static final log_list_team_header_divider:I = 0x7f080660

.field public static final log_list_team_header_left_text_view:I = 0x7f08065d

.field public static final log_list_team_header_medal_image:I = 0x7f08065e

.field public static final log_list_team_header_right_text_view:I = 0x7f08065f

.field public static final log_list_top_right_text:I = 0x7f08066d

.field public static final ly_action_bar_button:I = 0x7f080302

.field public static final ly_action_btn_holder:I = 0x7f08030e

.field public static final ly_action_btn_holder_canceldone:I = 0x7f080312

.field public static final ly_sub_action_title_holder:I = 0x7f080304

.field public static final main_item:I = 0x7f08001c

.field public static final memo_title:I = 0x7f0803e6

.field public static final memo_view:I = 0x7f080633

.field public static final memo_view_input:I = 0x7f080637

.field public static final middle_layout:I = 0x7f08032e

.field public static final month_divider:I = 0x7f0800a1

.field public static final multi_item_checkbox:I = 0x7f080672

.field public static final multi_item_list:I = 0x7f080671

.field public static final multi_item_text:I = 0x7f080673

.field public static final no_data_message:I = 0x7f08065b

.field public static final no_data_view:I = 0x7f08054f

.field public static final no_devices_image:I = 0x7f0808d5

.field public static final no_devices_text:I = 0x7f0808d6

.field public static final nodata_container:I = 0x7f08036e

.field public static final nodata_text:I = 0x7f080370

.field public static final normal_range:I = 0x7f080008

.field public static final ok_button:I = 0x7f0800b3

.field public static final onDown:I = 0x7f08000c

.field public static final onLongPress:I = 0x7f08000e

.field public static final onMove:I = 0x7f08000d

.field public static final paired_device_layout:I = 0x7f0808cb

.field public static final paired_devices_divider:I = 0x7f080023

.field public static final paired_list_container:I = 0x7f0808cd

.field public static final paired_list_layout:I = 0x7f0808ca

.field public static final paired_type_text:I = 0x7f0808cc

.field public static final parentLayout:I = 0x7f08064d

.field public static final piece_of_pie_diagram:I = 0x7f0804c8

.field public static final pin_code_edit_text:I = 0x7f080650

.field public static final pop_up_textTitle:I = 0x7f080042

.field public static final pop_up_title_layout:I = 0x7f080041

.field public static final primary_text:I = 0x7f080648

.field public static final print:I = 0x7f080ca7

.field public static final profile_email:I = 0x7f080336

.field public static final profile_image:I = 0x7f080333

.field public static final profile_image_layout:I = 0x7f080332

.field public static final profile_name:I = 0x7f080335

.field public static final profile_text_layout:I = 0x7f080334

.field public static final progressBar1:I = 0x7f0808d2

.field public static final progress_bar_left:I = 0x7f0804d8

.field public static final progress_bar_right:I = 0x7f0804db

.field public static final progress_title_left:I = 0x7f0804d9

.field public static final progress_title_right:I = 0x7f0804dc

.field public static final progressbar_backup:I = 0x7f08008b

.field public static final radio_button:I = 0x7f080778

.field public static final rb_after_meal:I = 0x7f08003e

.field public static final rb_fasting:I = 0x7f08003f

.field public static final rel_lay_data:I = 0x7f080049

.field public static final rename:I = 0x7f08067a

.field public static final rename_layout:I = 0x7f080679

.field public static final reset:I = 0x7f0802db

.field public static final right:I = 0x7f080007

.field public static final right_arrow:I = 0x7f080323

.field public static final root:I = 0x7f080048

.field public static final row_view_id_tag_key:I = 0x7f080005

.field public static final scanButton:I = 0x7f0808d8

.field public static final scanButtonLayout:I = 0x7f0808c7

.field public static final scancontainer:I = 0x7f0802fd

.field public static final scanlistlayout:I = 0x7f0808ce

.field public static final scannedlist:I = 0x7f0808d7

.field public static final scanning_header_text:I = 0x7f0808c8

.field public static final scanning_txt:I = 0x7f0808d3

.field public static final scrollview:I = 0x7f0808c6

.field public static final second_legend_mark_layout:I = 0x7f080645

.field public static final second_sub_tab:I = 0x7f0802fb

.field public static final second_sub_tab_rd_btn:I = 0x7f0802fc

.field public static final second_text:I = 0x7f080529

.field public static final secondary_text:I = 0x7f080649

.field public static final select:I = 0x7f080c8b

.field public static final setting:I = 0x7f080022

.field public static final setting_layout:I = 0x7f080021

.field public static final settings_menu:I = 0x7f080c9c

.field public static final shareViaHomeLayout:I = 0x7f080026

.field public static final share_content_frame:I = 0x7f080027

.field public static final sharevia:I = 0x7f080c98

.field public static final spinner_view_holder:I = 0x7f08030c

.field public static final subtabs_rd_grp:I = 0x7f0802f8

.field public static final summary_frag_container:I = 0x7f080a08

.field public static final text:I = 0x7f08080a

.field public static final textContent:I = 0x7f080044

.field public static final textParent:I = 0x7f080a6c

.field public static final text_below_pin_code:I = 0x7f08064f

.field public static final text_icon:I = 0x7f080636

.field public static final text_list_divider:I = 0x7f080330

.field public static final text_popup_hint:I = 0x7f08068a

.field public static final texts:I = 0x7f080527

.field public static final time:I = 0x7f08003b

.field public static final timepicker:I = 0x7f080a8c

.field public static final title:I = 0x7f08004d

.field public static final tvActionTitle:I = 0x7f080309

.field public static final tvActionTitle2:I = 0x7f08030a

.field public static final tvShareViewDate:I = 0x7f08002a

.field public static final tvShareViewHomeTitle:I = 0x7f080028

.field public static final tvShareViewOtherTitle:I = 0x7f080029

.field public static final tv_inputTag:I = 0x7f080546

.field public static final tv_progress_title:I = 0x7f08008a

.field public static final tv_scanning_info:I = 0x7f0808c9

.field public static final tv_tag_avg_txt:I = 0x7f080653

.field public static final tv_tag_avg_val:I = 0x7f080654

.field public static final tv_tag_max_txt:I = 0x7f080656

.field public static final tv_tag_max_val:I = 0x7f080657

.field public static final tv_tag_min_txt:I = 0x7f080658

.field public static final tv_tag_min_val:I = 0x7f080659

.field public static final tw_backing_in_mb:I = 0x7f08008d

.field public static final tw_backing_up_percents:I = 0x7f08008c

.field public static final txt_receiving:I = 0x7f08064c

.field public static final unpair:I = 0x7f08067d

.field public static final unpair_layout:I = 0x7f08067c

.field public static final value:I = 0x7f080037

.field public static final value_symbol:I = 0x7f080039

.field public static final view_by:I = 0x7f080c8c

.field public static final zoom_in_icon:I = 0x7f0804d5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
