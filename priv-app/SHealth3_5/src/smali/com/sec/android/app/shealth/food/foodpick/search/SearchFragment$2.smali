.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 6

    .prologue
    .line 232
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 233
    .local v0, "extras":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 235
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    const-string v2, "MEAL_ITEM_LIST"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->finishFoodPickActivityWithResult(Landroid/os/Bundle;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Landroid/os/Bundle;)V

    .line 237
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mNoFoodSelectedToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_0

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f09095c

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mNoFoodSelectedToast:Landroid/widget/Toast;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$202(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mNoFoodSelectedToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mNoFoodSelectedToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 251
    :cond_1
    return-void
.end method
