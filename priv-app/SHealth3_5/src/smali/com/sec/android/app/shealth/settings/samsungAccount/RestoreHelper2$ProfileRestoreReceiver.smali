.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RestoreHelper2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProfileRestoreReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0

    .prologue
    .line 1436
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$1;

    .prologue
    .line 1436
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1441
    if-eqz p2, :cond_2

    .line 1444
    const-string/jumbo v3, "progress_percent"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1446
    .local v1, "percentage":I
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Migration progress percentage: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1448
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1449
    const/16 v2, 0x32

    .line 1450
    .local v2, "startValue":I
    const/4 v0, 0x2

    .line 1456
    .local v0, "denominatorValue":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1457
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    div-int v4, v1, v0

    add-int/2addr v4, v2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    .line 1460
    :cond_0
    const/16 v3, 0x64

    if-ne v1, v3, :cond_2

    .line 1462
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1463
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1464
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    .line 1467
    :cond_1
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;)V

    const-wide/16 v5, 0xc8

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1477
    .end local v0    # "denominatorValue":I
    .end local v1    # "percentage":I
    .end local v2    # "startValue":I
    :cond_2
    return-void

    .line 1452
    .restart local v1    # "percentage":I
    :cond_3
    const/16 v2, 0x4b

    .line 1453
    .restart local v2    # "startValue":I
    const/4 v0, 0x4

    .restart local v0    # "denominatorValue":I
    goto :goto_0
.end method
