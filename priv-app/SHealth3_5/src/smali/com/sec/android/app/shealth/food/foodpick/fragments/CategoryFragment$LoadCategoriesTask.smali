.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;
.super Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadCategoriesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 1

    .prologue
    .line 279
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;

    .prologue
    .line 279
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    return-void
.end method


# virtual methods
.method protected doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performCategorySearch()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    return-object v0
.end method

.method protected getDisplayStage()Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    .locals 1

    .prologue
    .line 311
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->ROOT_CATEGORIES:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    return-object v0
.end method

.method protected onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;)V
    .locals 6
    .param p1, "result"    # Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->createCategoryAdapter(Ljava/util/List;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$1000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Ljava/util/List;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    move-result-object v1

    .line 304
    .local v1, "categoryAdapter":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->isFragmentRunning:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$1200(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    const v4, 0x7f090076

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->ROOT_CATEGORIES:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    const/4 v5, 0x0

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->setDisplayedItems(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$1300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V

    goto :goto_0
.end method

.method protected bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 279
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;)V

    return-void
.end method

.method protected onTaskStarted()V
    .locals 2

    .prologue
    .line 282
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;->onTaskStarted()V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mCategoryListHeader:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$500(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$800(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 285
    return-void
.end method
