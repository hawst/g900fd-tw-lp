.class public abstract Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;
.super Ljava/lang/Object;
.source "CalendarButton.java"


# static fields
.field private static final CALENDAR_BUTTON_MEDAL:Ljava/lang/String; = "calendar_day_button_star_selector"

.field private static final CALENDAR_BUTTON_UNDERLINE_RESOURCE:Ljava/lang/String; = "s_health_calendar_bar"


# instance fields
.field protected final mButton:Landroid/widget/RelativeLayout;

.field protected mContent:Landroid/view/View;

.field protected mMedal:Landroid/widget/ImageView;

.field protected mText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->getButtonLayoutResourceId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    const v1, 0x7f0800a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mButton:Landroid/widget/RelativeLayout;

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    const v1, 0x7f08009d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    const v1, 0x7f0800a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mButton:Landroid/widget/RelativeLayout;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    const v1, 0x7f08009d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    .line 47
    return-void
.end method


# virtual methods
.method protected abstract getButtonLayoutResourceId()I
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mContent:Landroid/view/View;

    return-object v0
.end method

.method setMedalImage(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "medalImageDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 84
    return-void
.end method

.method public setMedalState(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    return-void

    .line 73
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 103
    return-void
.end method
