.class public final enum Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;
.super Ljava/lang/Enum;
.source "DayMealDataHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

.field public static final enum MEAL_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

.field public static final enum NO_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->NO_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    .line 66
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    const-string v1, "MEAL_DATA"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->MEAL_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->NO_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->MEAL_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    return-object v0
.end method
