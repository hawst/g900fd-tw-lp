.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->addSelectSpinner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, 0x626aab008d90ca8bL    # 1.22856376774151E166

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x626aab008d90ca8bL    # 1.22856376774151E166

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, 0x6c16

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, 0x6c26

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-eqz v3, :cond_5

    const-wide v3, 0x626aab008d90ca8bL    # 1.22856376774151E166

    xor-long/2addr v0, v3

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v0, v3

    const/16 v3, 0x20

    shr-long/2addr v0, v3

    long-to-int v0, v0

    if-lez v0, :cond_a

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x42

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x72

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-eqz v3, :cond_9

    const-wide v3, 0x626aab008d90ca8bL    # 1.22856376774151E166

    xor-long/2addr v0, v3

    :cond_9
    const/16 v3, 0x20

    shl-long/2addr v0, v3

    const/16 v3, 0x20

    shr-long/2addr v0, v3

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->select_all:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_a

    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLogActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1102(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshFocusables()V

    return-void

    :cond_b
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_e

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, 0x2413

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, 0x2423

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_6
    array-length v4, v0

    if-lt v2, v4, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_7
    array-length v4, v0

    if-lt v2, v4, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_c
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_d
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_e
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_f

    const-wide v2, 0x626aab008d90ca8bL    # 1.22856376774151E166

    xor-long/2addr v0, v2

    :cond_f
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->unselect_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_a

    :goto_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLogActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1102(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    goto/16 :goto_5

    :catch_0
    move-exception v0

    goto/16 :goto_4

    :catch_1
    move-exception v0

    goto :goto_8
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
