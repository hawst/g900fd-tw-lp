.class public Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;
.super Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;
.source "FoodLogListActivity.java"


# static fields
.field private static final FOOD_FORMAT_FOR_LOG_DATA_SHARING:Ljava/lang/String; = "(%s %s, %s, %d %s)\n"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final SHARING_DATA_SEPARATOR:Ljava/lang/String; = "\n"


# instance fields
.field mExpandableListView:Landroid/widget/ExpandableListView;

.field private mFoodLogListConcreteDelegate:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mFoodLogListConcreteDelegate:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->refreshAdapter()V

    return-void
.end method


# virtual methods
.method protected createConcreteDelegate()Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->registerContentObserver()V

    .line 147
    new-instance v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mFoodLogListConcreteDelegate:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mFoodLogListConcreteDelegate:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    return-object v0
.end method

.method public getNoDataImageResource()I
    .locals 1

    .prologue
    .line 96
    const v0, 0x7f0205b7

    return v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 17

    .prologue
    .line 170
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .local v8, "stringBuilder":Ljava/lang/StringBuilder;
    new-instance v5, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 173
    .local v5, "mealDao":Lcom/sec/android/app/shealth/food/fooddao/MealDao;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 174
    .local v1, "dateFormat":Ljava/text/DateFormat;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v10

    .line 176
    .local v10, "timeFormat":Ljava/text/DateFormat;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 177
    .local v9, "tag":Ljava/lang/String;
    const-string v11, "_"

    invoke-virtual {v9, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v11, v11, v12

    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 178
    .local v3, "id":J
    invoke-interface {v5, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 179
    .local v6, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/food/constants/MealType;->getMealTypeByMealTypeId(I)Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v7

    .line 180
    .local v7, "mealType":Lcom/sec/android/app/shealth/food/constants/MealType;
    const-string v11, "(%s %s, %s, %d %s)\n"

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v1, v14}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    iget v15, v7, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v14

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0900b9

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 188
    .end local v3    # "id":J
    .end local v6    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v7    # "mealType":Lcom/sec/android/app/shealth/food/constants/MealType;
    .end local v9    # "tag":Ljava/lang/String;
    :cond_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 189
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    const-string v12, "\n"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    invoke-virtual {v8, v11, v12}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 192
    :cond_1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 207
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 209
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 211
    if-eqz p3, :cond_1

    const-string v0, "MEAL_WAS_EDITED"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    .line 214
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$2;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 230
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_LOG_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 66
    const v0, 0x7f08065a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 90
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodMaskedImageCollectorHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodMaskedImageCollectorHolder;->getMaskedImageCollector()Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->clearCache()V

    .line 268
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onDestroy()V

    .line 269
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 123
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 141
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 126
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_VIEW_BY_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 132
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 135
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_DELETE_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 138
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 123
    :sswitch_data_0
    .sparse-switch
        0x7f080c8c -> :sswitch_0
        0x7f080c8d -> :sswitch_2
        0x7f080c98 -> :sswitch_1
        0x7f080ca7 -> :sswitch_3
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 112
    .local v0, "result":Z
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isDataAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    const v1, 0x7f080c8c

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 116
    :cond_0
    const v1, 0x7f080c8b

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09110b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 118
    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onResume()V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_LOG_LIST_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 106
    :cond_0
    return-void
.end method

.method protected registerContentObserver()V
    .locals 0

    .prologue
    .line 258
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 7
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 153
    if-eqz p1, :cond_0

    const-string v5, "_"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    if-le v5, v6, :cond_0

    .line 154
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v6, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_LOG_LISTS_MEAL_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v5, v6}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 156
    :try_start_0
    const-string v5, "_"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 157
    .local v1, "id":J
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    .local v3, "intent":Landroid/content/Intent;
    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>()V

    .line 159
    .local v4, "mealDataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    invoke-virtual {v4, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealId(J)V

    .line 160
    const-string v5, "MEAL_DATA_HOLDER"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 161
    const/16 v5, 0x1388

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    .end local v1    # "id":J
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "mealDataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    :cond_0
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v5, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Given tag stores incorrect meal ID"

    invoke-static {v5, v6, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
