.class final Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$5;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->getOnHoverHapticFeedbackListener()Landroid/view/View$OnHoverListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    const/16 v5, 0x9

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "air_view_master_onoff"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "air_view_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v4, :cond_2

    invoke-static {p1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->isPenHoveringSoundAndHapticFeedbackOn(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v5, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    invoke-virtual {p1, v5}, Landroid/view/View;->performHapticFeedback(I)Z

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    if-ne v2, v0, :cond_5

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v0, :cond_4

    invoke-static {p1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->isFingerHoveringSoundAndHapticFeedbackOn(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    if-ne v2, v4, :cond_7

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v0, :cond_6

    invoke-static {p1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->isFingerHoveringSoundAndHapticFeedbackOn(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    :cond_6
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v4, :cond_7

    invoke-static {p1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->isPenHoveringSoundAndHapticFeedbackOn(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0
.end method
