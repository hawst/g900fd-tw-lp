.class Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$2;
.super Ljava/lang/Object;
.source "ExpandableListFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->initAdapterListeners(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;)Z
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->uncheckAll()V

    .line 188
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setChecked(Z)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$100(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->createSelectedPanelHolder()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;->onListItemLongClick(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onItemLongClick(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 184
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$2;->onItemLongClick(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;)Z

    move-result v0

    return v0
.end method
