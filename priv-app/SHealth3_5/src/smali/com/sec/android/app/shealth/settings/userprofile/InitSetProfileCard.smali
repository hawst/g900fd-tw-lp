.class public Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "InitSetProfileCard.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$DismissDialogController;
    }
.end annotation


# static fields
.field private static final CROP_FROM_ALBUM:I = 0x3

.field private static final CROP_FROM_CAMERA:I = 0x2

.field private static final DELETE_PHOTO_ACTION:I = 0x2

.field public static final EXTRA_CAMERA_FACING:Ljava/lang/String; = "android.intent.extras.CAMERA_FACING"

.field private static final FIRST_CARD_INDEX:I = 0x1

.field private static final FOURTH_CARD_INDEX:I = 0x4

.field private static final LOG_EXTRA:Ljava/lang/String; = "FAIL"

.field private static final LOG_FEATURE:Ljava/lang/String; = "ERR_IS"

.field private static final PICK_FROM_ALBUM:I = 0x1

.field private static final PICK_FROM_CAMERA:I = 0x0

.field private static final SECOND_CARD_INDEX:I = 0x2

.field public static TAG:Ljava/lang/String; = null

.field private static final TAKE_ALBUM_ACTION:I = 0x0

.field private static final TAKE_PHOTO_ACTION:I = 0x1

.field private static final TEMP_IMAGE_PREFIX:Ljava/lang/String; = "temp_"

.field private static final TEMP_IMAGE_TIME_FORMAT:Ljava/lang/String; = "HH_mm_ss_SSS"

.field private static final THIRD_CARD_INDEX:I = 0x3

.field private static final TOTAL_CARD_INDEX:I = 0x4


# instance fields
.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field private SCREEN_POSITION:Ljava/lang/String;

.field private SHEALTHPROFILE_DATA:Ljava/lang/String;

.field private bSaveInstaceState:Z

.field private imageClickListener:Landroid/view/View$OnClickListener;

.field private isUpgraded:Z

.field private mCameraFlag:Z

.field private mContext:Landroid/content/Context;

.field private final mDismissControlMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;",
            ">;"
        }
    .end annotation
.end field

.field private mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

.field private mFirstCardNumber:Landroid/widget/ImageView;

.field private mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

.field private mFourthCardNumber:Landroid/widget/ImageView;

.field private mFourthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mIndex:I

.field private mListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

.field private mMainContainer:Landroid/widget/LinearLayout;

.field private mNextText:Landroid/widget/TextView;

.field private mPhoto:Landroid/graphics/Bitmap;

.field private mSecondCardNumber:Landroid/widget/ImageView;

.field private mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private mThirdCardNumber:Landroid/widget/ImageView;

.field private mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

.field private nextButton:Landroid/widget/RelativeLayout;

.field original_file:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-class v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 125
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstCardNumber:Landroid/widget/ImageView;

    .line 126
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondCardNumber:Landroid/widget/ImageView;

    .line 127
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdCardNumber:Landroid/widget/ImageView;

    .line 128
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFourthCardNumber:Landroid/widget/ImageView;

    .line 130
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    .line 131
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    .line 132
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .line 133
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFourthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;

    .line 136
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    .line 146
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 151
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mCameraFlag:Z

    .line 153
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 154
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 155
    const-string/jumbo v0, "shealthProfile_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->SHEALTHPROFILE_DATA:Ljava/lang/String;

    .line 156
    const-string/jumbo v0, "screen_position"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->SCREEN_POSITION:Ljava/lang/String;

    .line 158
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->bSaveInstaceState:Z

    .line 159
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->isUpgraded:Z

    .line 160
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mContext:Landroid/content/Context;

    .line 168
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mDismissControlMap:Ljava/util/Map;

    .line 297
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$3;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->imageClickListener:Landroid/view/View$OnClickListener;

    .line 1094
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->saveProfile()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;
    .param p1, "x1"    # Z

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->createSetPicturePopup(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->doTakePhotoAction()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->doTakeAlbumAction()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->doDeletePhotoAction()V

    return-void
.end method

.method private changeCardNumber(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const v2, 0x7f0205f4

    const v1, 0x7f0205f1

    .line 567
    packed-switch p1, :pswitch_data_0

    .line 591
    :goto_0
    return-void

    .line 570
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstCardNumber:Landroid/widget/ImageView;

    const v1, 0x7f0205f9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondCardNumber:Landroid/widget/ImageView;

    const v1, 0x7f0205f3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdCardNumber:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 577
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstCardNumber:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondCardNumber:Landroid/widget/ImageView;

    const v1, 0x7f0205fa

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdCardNumber:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 584
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstCardNumber:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondCardNumber:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdCardNumber:Landroid/widget/ImageView;

    const v1, 0x7f0205fb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 567
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private changeLayout(I)V
    .locals 13
    .param p1, "index"    # I

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x0

    const v10, 0x7f090042

    const/4 v8, 0x2

    const/4 v9, 0x1

    .line 339
    if-eq p1, v9, :cond_0

    if-eq p1, v8, :cond_0

    const/4 v3, 0x3

    if-ne p1, v3, :cond_1

    .line 340
    :cond_0
    invoke-direct {p0, v11}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setLayoutHeight(Z)V

    .line 344
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 447
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->finishProfile()V

    .line 450
    :goto_1
    return-void

    .line 342
    :cond_1
    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setLayoutHeight(Z)V

    goto :goto_0

    .line 346
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    if-eqz v3, :cond_2

    .line 347
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->hideEditKeyboard()V

    .line 348
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->isChanged()Z

    move-result v3

    if-nez v3, :cond_2

    .line 349
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    .line 354
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04db

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 355
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04de

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 356
    const v3, 0x7f08089b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 357
    .local v2, "profileCenter":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v1, v3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 358
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeCardNumber(I)V

    .line 361
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 362
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    if-nez v3, :cond_3

    .line 363
    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .line 365
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v3, :cond_7

    .line 366
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_6

    .line 367
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->SaveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 368
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->onSaveImageView()V

    .line 384
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->imageClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setImageClickListener(Landroid/view/View$OnClickListener;)V

    .line 385
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 387
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 389
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->removeEditNameDafaultFocus()V

    .line 390
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mNextText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->requestFocus()Z

    .line 392
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09081d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f8

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 394
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mNextText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->nextButton:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 371
    :cond_6
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    goto/16 :goto_2

    .line 374
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->isUserFile()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    .line 377
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_8

    .line 379
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setImage(Landroid/graphics/Bitmap;)V

    .line 381
    :cond_8
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    goto/16 :goto_2

    .line 398
    .end local v0    # "height":I
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "profileCenter":Landroid/widget/LinearLayout;
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    if-eqz v3, :cond_9

    .line 399
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->hideKeyboard()V

    .line 402
    :cond_9
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeCardNumber(I)V

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 404
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    if-nez v3, :cond_b

    .line 405
    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    .line 406
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->setDefaultValue(I)V

    .line 410
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    if-nez v3, :cond_a

    .line 411
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->setGenderImage(I)V

    .line 413
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 414
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09081d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f8

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 416
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mNextText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->nextButton:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 408
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->setGenderImage(I)V

    goto/16 :goto_3

    .line 420
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    if-eqz v3, :cond_c

    .line 421
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->hideEditKeyboard()V

    .line 423
    :cond_c
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeCardNumber(I)V

    .line 424
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 425
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    if-nez v3, :cond_d

    .line 426
    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    .line 428
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 429
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09081d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f8

    new-array v7, v8, [Ljava/lang/Object;

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mNextText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 432
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->nextButton:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 435
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeCardNumber(I)V

    .line 436
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 437
    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    .line 438
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 440
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09081d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f8

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mNextText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090050

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->setWeightValue(ILcom/samsung/android/sdk/health/content/ShealthProfile;)V

    .line 444
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->nextButton:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090050

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 4
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    .line 1032
    const/4 v2, 0x0

    .line 1034
    .local v2, "result":Z
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1036
    .local v1, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 1038
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1043
    .end local v1    # "in":Ljava/io/InputStream;
    :goto_0
    return v2

    .line 1038
    .restart local v1    # "in":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1040
    .end local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 1041
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 1048
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1050
    .local v3, "out":Ljava/io/OutputStream;
    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    .line 1052
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    if-ltz v1, :cond_0

    .line 1053
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1056
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v5

    .line 1059
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 1060
    :goto_1
    return v4

    .line 1056
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v3    # "out":Ljava/io/OutputStream;
    :cond_0
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1058
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private createSaveCropFile()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 1017
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1018
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 1019
    .local v3, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v7, "HH_mm_ss_SSS"

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1020
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "temp_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1024
    .local v6, "url":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Camera"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1025
    .local v1, "camera":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1027
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 1028
    .local v5, "uri":Landroid/net/Uri;
    return-object v5
.end method

.method private createSetPicturePopup(Z)V
    .locals 4
    .param p1, "picture"    # Z

    .prologue
    .line 1070
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v1, 0x7f0907a7

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 1072
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListItem:Ljava/util/ArrayList;

    .line 1073
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListItem:Ljava/util/ArrayList;

    const v2, 0x7f0907a9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1074
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListItem:Ljava/util/ArrayList;

    const v2, 0x7f0907a8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1076
    if-eqz p1, :cond_0

    .line 1077
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListItem:Ljava/util/ArrayList;

    const v2, 0x7f09003a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1079
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 1080
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 1083
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->bSaveInstaceState:Z

    if-nez v1, :cond_1

    .line 1084
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "SetPicture"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1088
    :goto_0
    return-void

    .line 1086
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    goto :goto_0
.end method

.method private deleteTempFile()V
    .locals 2

    .prologue
    .line 474
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 475
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 476
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 480
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private doDeletePhotoAction()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 843
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->deleteTempFile()V

    .line 845
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->deleteProfileBitmap()Ljava/lang/Boolean;

    .line 847
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mCameraFlag:Z

    .line 848
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    .line 849
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    .line 850
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setDefaultImage()V

    .line 852
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->clearProfileImageCache()V

    .line 853
    return-void
.end method

.method private doTakeAlbumAction()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 871
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 872
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "set-as-image"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 873
    const-string/jumbo v1, "vnd.android.cursor.dir/image"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 874
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivityForResult(Landroid/content/Intent;I)V

    .line 875
    return-void
.end method

.method private doTakePhotoAction()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 856
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 859
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 861
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->deleteTempFile()V

    .line 863
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    .line 864
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 865
    const-string/jumbo v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 866
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivityForResult(Landroid/content/Intent;I)V

    .line 868
    return-void
.end method

.method private finishProfile()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v9, 0x0

    const/4 v13, 0x1

    .line 715
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/service/health/keyManager/KeyManager;->finishOOBE()V

    .line 716
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->clearMigrationState(Landroid/content/Context;)V

    .line 718
    invoke-static {p0, v9}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 719
    new-instance v6, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    .line 720
    .local v6, "uiPrefsHelper":Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    invoke-virtual {v6, v9}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setInitialSettingOngoing(Z)V

    .line 721
    invoke-static {p0, v9}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreOngoing(Landroid/content/Context;Z)V

    .line 725
    new-instance v7, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 726
    .local v7, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "mi"

    if-ne v8, v9, :cond_3

    .line 727
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    const v9, 0x29813

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setDistanceUnit(I)V

    .line 733
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupSwitchStatus(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 735
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupInterval(Landroid/content/Context;)J

    move-result-wide v0

    .line 736
    .local v0, "autoBackupInterval":J
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9, v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->setPeroidicSync(Landroid/content/Context;J)V

    .line 740
    .end local v0    # "autoBackupInterval":J
    :cond_0
    :try_start_0
    new-instance v5, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 741
    .local v5, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setCountry(Ljava/lang/String;)V

    .line 742
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setName(Ljava/lang/String;)V

    .line 743
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setGender(I)V

    .line 744
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    .line 745
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V

    .line 746
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 747
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    .line 748
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getTemperatureUnit()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setTemperatureUnit(I)V

    .line 749
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setBirthDate(Ljava/lang/String;)V

    .line 750
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivityType(I)V

    .line 751
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setDistanceUnit(I)V

    .line 752
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setProfileDisclose(Z)V

    .line 753
    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V

    .line 756
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 757
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->updateUserProfile(Landroid/content/Context;)V

    .line 759
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "com.sec.android.app.shealth"

    const-string v10, "IS99"

    iget-object v11, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v11

    rem-int/lit8 v11, v11, 0xa

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsSendProfileDisclose(Landroid/content/Context;Z)V

    .line 761
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v8

    invoke-static {p0, v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsUsePedometerRanking(Landroid/content/Context;Z)V

    .line 762
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isUsePedometerRanking(Landroid/content/Context;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setProfileDiscloseYN(Landroid/content/Context;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 774
    .end local v5    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->StartAfterProfileSetUp()V

    .line 775
    const-string v8, "InitSetProfile"

    const-string/jumbo v9, "start_pedometer"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    invoke-static {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setCalorieGoal(Landroid/content/Context;)V

    .line 778
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v9

    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setWeightProfile(FF)V

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLastUpdated(Landroid/content/Context;)V

    .line 780
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setFirstActivity(Landroid/content/Context;)V

    .line 781
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v4, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 782
    .local v4, "intent":Landroid/content/Intent;
    const-string v8, "com.sec.shealth.action.STEALTH_MODE"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 783
    const-string v8, "IS_FIRST"

    invoke-virtual {v4, v8, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 784
    const-string v8, "IS_FIRST_RECYCLE"

    invoke-virtual {v4, v8, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 785
    const/high16 v8, 0x4000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 786
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->isUpgraded:Z

    if-eqz v8, :cond_2

    .line 787
    const-string v8, "IS_UPGRADE_DONE"

    invoke-virtual {v4, v8, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 788
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivity(Landroid/content/Intent;)V

    .line 791
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 792
    .local v3, "finishProfile":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->sendBroadcast(Landroid/content/Intent;)V

    .line 796
    :try_start_1
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x15

    if-lt v8, v9, :cond_4

    .line 797
    const-string v8, "InitSetProfileCard"

    const-string v9, "LOS"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "lock_additional_steps"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 807
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->finish()V

    .line 808
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setUpdateCodeVersion(Landroid/content/Context;)V

    .line 809
    return-void

    .line 729
    .end local v3    # "finishProfile":Landroid/content/Intent;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    const v9, 0x29811

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setDistanceUnit(I)V

    goto/16 :goto_0

    .line 765
    :catch_0
    move-exception v2

    .line 766
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mContext:Landroid/content/Context;

    const-string v9, "com.sec.android.app.shealth"

    const-string v10, "ERR_IS"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FAIL2: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11, v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 767
    const-string v8, "InitSetProfileCard"

    invoke-static {v8, v2}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 769
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 770
    .local v2, "e":Ljava/lang/Exception;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mContext:Landroid/content/Context;

    const-string v9, "com.sec.android.app.shealth"

    const-string v10, "ERR_IS"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FAIL3: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11, v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 771
    const-string v8, "InitSetProfileCard"

    invoke-static {v8, v2}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 800
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "finishProfile":Landroid/content/Intent;
    .restart local v4    # "intent":Landroid/content/Intent;
    :cond_4
    :try_start_2
    const-string v8, "InitSetProfileCard"

    const-string v9, "<LOS"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "walk_mate"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 803
    :catch_2
    move-exception v2

    .line 804
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v8, "InitSetProfileCard"

    invoke-static {v8, v2}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public static getSamsungAccountBirthday()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1196
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->sBirthday:Ljava/lang/String;

    return-object v0
.end method

.method private initLayout()V
    .locals 11

    .prologue
    const v10, 0x7f0901f8

    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 526
    const v2, 0x7f0301ed

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setContentView(I)V

    .line 527
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "profileLog":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-gt v2, v3, :cond_0

    .line 538
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 540
    .local v1, "window":Landroid/view/Window;
    if-eqz v1, :cond_0

    .line 543
    const/16 v2, 0x400

    const/16 v3, 0x500

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 547
    .end local v1    # "window":Landroid/view/Window;
    :cond_0
    const v2, 0x7f08089b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mMainContainer:Landroid/widget/LinearLayout;

    .line 548
    const v2, 0x7f08039d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mNextText:Landroid/widget/TextView;

    .line 549
    const v2, 0x7f080613

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->nextButton:Landroid/widget/RelativeLayout;

    .line 550
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->nextButton:Landroid/widget/RelativeLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901f6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09020a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 552
    const v2, 0x7f080616

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstCardNumber:Landroid/widget/ImageView;

    .line 553
    const v2, 0x7f080617

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondCardNumber:Landroid/widget/ImageView;

    .line 554
    const v2, 0x7f080618

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdCardNumber:Landroid/widget/ImageView;

    .line 555
    const v2, 0x7f080619

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFourthCardNumber:Landroid/widget/ImageView;

    .line 557
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstCardNumber:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 558
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondCardNumber:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdCardNumber:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFourthCardNumber:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 562
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeLayout(I)V

    .line 563
    return-void
.end method

.method private saveProfile()V
    .locals 2

    .prologue
    .line 484
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    packed-switch v0, :pswitch_data_0

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 486
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->saveProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    .line 490
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mCameraFlag:Z

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->savePhoto(Landroid/graphics/Bitmap;)V

    .line 494
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeLayout(I)V

    goto :goto_0

    .line 499
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSecondView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->saveProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    .line 502
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeLayout(I)V

    goto :goto_0

    .line 507
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mThirdView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->saveProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    .line 510
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeLayout(I)V

    goto :goto_0

    .line 515
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    if-eqz v0, :cond_0

    .line 516
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    .line 517
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeLayout(I)V

    goto :goto_0

    .line 484
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setLayoutHeight(Z)V
    .locals 6
    .param p1, "isFinal"    # Z

    .prologue
    .line 319
    const v4, 0x7f08085e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 321
    .local v3, "profileTop":Landroid/widget/RelativeLayout;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a04db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 323
    .local v0, "height":I
    if-eqz p1, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0527

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 325
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 331
    :goto_0
    const v4, 0x7f08089b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 332
    .local v2, "profileCenter":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v1, v4, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 333
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 334
    return-void

    .line 328
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "profileCenter":Landroid/widget/LinearLayout;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "audioManager"    # Landroid/media/AudioManager;

    .prologue
    const/4 v0, 0x1

    .line 703
    if-eqz p2, :cond_0

    .line 705
    invoke-virtual {p2, v0}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 707
    :cond_0
    if-eqz p1, :cond_1

    .line 709
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 711
    :cond_1
    return v0
.end method

.method private setUpDefaultUnitsByLocale()V
    .locals 2

    .prologue
    .line 1168
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 1170
    .local v0, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isUSAModel()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isUKModel()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1172
    :cond_0
    const-string v1, "lb"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    .line 1173
    const-string v1, "ft, inch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V

    .line 1174
    const-string/jumbo v1, "mi"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    .line 1176
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isUSAModel()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1178
    const-string v1, "F"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putTemperatureUnit(Ljava/lang/String;)V

    .line 1192
    :goto_0
    return-void

    .line 1182
    :cond_1
    const-string v1, "C"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putTemperatureUnit(Ljava/lang/String;)V

    goto :goto_0

    .line 1187
    :cond_2
    const-string v1, "kg"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    .line 1188
    const-string v1, "cm"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V

    .line 1189
    const-string v1, "km"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    .line 1190
    const-string v1, "C"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putTemperatureUnit(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setWeightProfile(FF)V
    .locals 7
    .param p1, "weight"    # F
    .param p2, "height"    # F

    .prologue
    .line 812
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 814
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "input_source_type"

    const v3, 0x3f7a1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 815
    const-string v2, "height"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 816
    const-string/jumbo v2, "weight"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 817
    const-string/jumbo v2, "sample_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 820
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 825
    :goto_0
    return-void

    .line 821
    :catch_0
    move-exception v0

    .line 822
    .local v0, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth"

    const-string v4, "ERR_IS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FAIL4: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 823
    sget-object v2, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert setWeightProfile exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public closeScreen()V
    .locals 0

    .prologue
    .line 836
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->onBackPressed()V

    .line 837
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 829
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 830
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 831
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f09081d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 832
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const-string v1, "#73b90f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 833
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 596
    const-string v8, "audio"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 597
    .local v0, "audioManager":Landroid/media/AudioManager;
    const/4 v6, 0x0

    .line 598
    .local v6, "profileImageView":Landroid/view/View;
    const/4 v5, 0x0

    .line 599
    .local v5, "nameView":Landroid/view/View;
    const/4 v3, 0x0

    .line 600
    .local v3, "femaleView":Landroid/view/View;
    const/4 v4, 0x0

    .line 601
    .local v4, "maleView":Landroid/view/View;
    const/4 v2, 0x0

    .line 602
    .local v2, "datePicker":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    if-eqz v8, :cond_0

    .line 604
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getView()Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 606
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getView()Landroid/view/View;

    move-result-object v8

    const v9, 0x7f080332

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 607
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getView()Landroid/view/View;

    move-result-object v8

    const v9, 0x7f080325

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 608
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getView()Landroid/view/View;

    move-result-object v8

    const v9, 0x7f080335

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 609
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getView()Landroid/view/View;

    move-result-object v8

    const v9, 0x7f08084b

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 610
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getView()Landroid/view/View;

    move-result-object v8

    const v9, 0x7f080848

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 615
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x14

    if-ne v8, v9, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_2

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 619
    .local v1, "currentView":Landroid/view/View;
    if-eqz v1, :cond_1

    if-eqz v6, :cond_1

    if-ne v1, v6, :cond_1

    .line 621
    if-eqz v2, :cond_2

    .line 623
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    .line 695
    .end local v1    # "currentView":Landroid/view/View;
    :goto_0
    return v8

    .line 628
    .restart local v1    # "currentView":Landroid/view/View;
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v5, :cond_2

    if-ne v1, v5, :cond_2

    .line 630
    if-eqz v4, :cond_2

    .line 632
    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 638
    .end local v1    # "currentView":Landroid/view/View;
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x13

    if-ne v8, v9, :cond_5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_5

    .line 640
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 641
    .restart local v1    # "currentView":Landroid/view/View;
    if-eqz v1, :cond_3

    if-eqz v6, :cond_3

    if-ne v1, v6, :cond_3

    .line 643
    if-eqz v5, :cond_5

    .line 645
    const/16 v8, 0x21

    invoke-virtual {v5, v8}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v7

    .line 646
    .local v7, "upFocusView":Landroid/view/View;
    if-eqz v7, :cond_5

    .line 648
    invoke-direct {p0, v7, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 653
    .end local v7    # "upFocusView":Landroid/view/View;
    :cond_3
    if-eqz v1, :cond_5

    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    if-eq v1, v4, :cond_4

    if-ne v1, v3, :cond_5

    .line 655
    :cond_4
    if-eqz v5, :cond_5

    .line 657
    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 661
    .end local v1    # "currentView":Landroid/view/View;
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x16

    if-ne v8, v9, :cond_7

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_7

    .line 663
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 665
    .restart local v1    # "currentView":Landroid/view/View;
    if-eqz v1, :cond_6

    if-eqz v6, :cond_6

    if-ne v1, v6, :cond_6

    .line 667
    if-eqz v5, :cond_7

    .line 669
    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 674
    :cond_6
    if-eqz v1, :cond_7

    if-eqz v5, :cond_7

    if-ne v1, v5, :cond_7

    .line 676
    if-eqz v4, :cond_7

    .line 678
    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 682
    .end local v1    # "currentView":Landroid/view/View;
    :cond_7
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x15

    if-ne v8, v9, :cond_8

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_8

    .line 684
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 685
    .restart local v1    # "currentView":Landroid/view/View;
    if-eqz v1, :cond_8

    if-eqz v5, :cond_8

    if-ne v1, v5, :cond_8

    .line 687
    if-eqz v6, :cond_8

    .line 689
    invoke-direct {p0, v6, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto/16 :goto_0

    .line 695
    .end local v1    # "currentView":Landroid/view/View;
    :cond_8
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v8

    goto/16 :goto_0
.end method

.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mDismissControlMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1114
    const-string v0, "SetPicture"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1115
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$4;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V

    .line 1130
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x1

    .line 896
    sget-object v8, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "onActivityResult : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    const/4 v8, -0x1

    if-eq p2, v8, :cond_1

    .line 1013
    :cond_0
    :goto_0
    return-void

    .line 902
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 935
    :pswitch_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_4

    .line 937
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 938
    .local v5, "mediaScanIntent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 939
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->sendBroadcast(Landroid/content/Intent;)V

    .line 941
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.android.camera.action.CROP"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 942
    .local v3, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    const-string v9, "image/*"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 944
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    .line 945
    const-string/jumbo v8, "output"

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 946
    const-string v8, "aspectX"

    invoke-virtual {v3, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 947
    const-string v8, "aspectY"

    invoke-virtual {v3, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 948
    const/4 v8, 0x2

    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivityForResult(Landroid/content/Intent;I)V

    .line 949
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v8, v11}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto :goto_0

    .line 906
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "mediaScanIntent":Landroid/content/Intent;
    :pswitch_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    if-nez v8, :cond_3

    .line 908
    :cond_2
    sget-object v8, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    const-string v9, "OnactivityResult: Data or data.getdata() is NULL"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 912
    :cond_3
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getImageFile(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->original_file:Ljava/io/File;

    .line 913
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->original_file:Ljava/io/File;

    if-eqz v8, :cond_0

    .line 918
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->deleteTempFile()V

    .line 920
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    .line 921
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 922
    .local v0, "copy_file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->original_file:Ljava/io/File;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 924
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.android.camera.action.CROP"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 925
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    const-string v9, "image/*"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 926
    const-string/jumbo v8, "output"

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 927
    const-string v8, "aspectX"

    invoke-virtual {v3, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 928
    const-string v8, "aspectY"

    invoke-virtual {v3, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 929
    const-string/jumbo v8, "set-as-image"

    invoke-virtual {v3, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 930
    const/4 v8, 0x3

    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivityForResult(Landroid/content/Intent;I)V

    .line 931
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v8, v11}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto/16 :goto_0

    .line 957
    .end local v0    # "copy_file":Ljava/io/File;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_4
    :pswitch_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-nez v8, :cond_5

    .line 958
    if-eqz p3, :cond_5

    .line 959
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getImageFile(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->original_file:Ljava/io/File;

    .line 960
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    .line 961
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 962
    .restart local v0    # "copy_file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->original_file:Ljava/io/File;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 965
    .end local v0    # "copy_file":Ljava/io/File;
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_b

    .line 967
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 968
    .local v2, "full_path":Ljava/lang/String;
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 969
    .local v6, "tempPhotoBitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 970
    .local v4, "isSaved":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v7

    .line 971
    .local v7, "tempUri":Landroid/net/Uri;
    if-eqz v6, :cond_6

    if-eqz v7, :cond_6

    .line 973
    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveImageToInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v4

    .line 976
    :cond_6
    if-eqz v4, :cond_8

    .line 979
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_7

    .line 981
    new-instance v1, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 983
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 985
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 988
    .end local v1    # "f":Ljava/io/File;
    :cond_7
    iput-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    .line 991
    :cond_8
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    .line 992
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_9

    .line 993
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setImage(Landroid/graphics/Bitmap;)V

    .line 995
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_a

    .line 997
    new-instance v1, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 999
    .restart local v1    # "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1001
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1004
    .end local v1    # "f":Ljava/io/File;
    :cond_a
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mCameraFlag:Z

    .line 1008
    .end local v2    # "full_path":Ljava/lang/String;
    .end local v4    # "isSaved":Z
    .end local v6    # "tempPhotoBitmap":Landroid/graphics/Bitmap;
    .end local v7    # "tempUri":Landroid/net/Uri;
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->clearProfileImageCache()V

    goto/16 :goto_0

    .line 902
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1135
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1136
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    .line 1137
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->changeLayout(I)V

    .line 1141
    :goto_0
    return-void

    .line 1139
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 177
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 178
    sget-object v13, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    const-string/jumbo v14, "onCreate"

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mContext:Landroid/content/Context;

    .line 180
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v13

    if-nez v13, :cond_0

    .line 182
    sget-object v13, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    const-string v14, "[VM] inside set profile on create and !KeyManager.getInstance().isContentProviderAccessible() is true"

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance v6, Landroid/content/Intent;

    const-string v13, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v6, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 184
    .local v6, "i":Landroid/content/Intent;
    const-string/jumbo v13, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    const/high16 v13, 0x10000000

    invoke-virtual {v6, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 186
    const/high16 v13, 0x4000000

    invoke-virtual {v6, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 187
    const/high16 v13, 0x20000000

    invoke-virtual {v6, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 188
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivity(Landroid/content/Intent;)V

    .line 190
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v13

    invoke-static {v13}, Landroid/os/Process;->killProcess(I)V

    .line 194
    .end local v6    # "i":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;)Z

    .line 195
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 197
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v10

    .line 198
    .local v10, "pref":Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->initialize(Landroid/content/Context;)V

    .line 199
    const/4 v8, 0x0

    .line 201
    .local v8, "isRestoredByKies":Z
    const-string/jumbo v13, "restored_data_from_kies"

    invoke-virtual {v10, v13}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 202
    const-string/jumbo v13, "restored_data_from_kies"

    invoke-virtual {v10, v13}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 205
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v9

    .line 206
    .local v9, "migrationState":I
    new-instance v13, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 208
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 209
    .local v4, "extras":Landroid/os/Bundle;
    const-string v13, "key"

    const-string v14, "autobackup_interval"

    invoke-virtual {v4, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string/jumbo v13, "value"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupInterval(Landroid/content/Context;)J

    move-result-wide v14

    invoke-virtual {v4, v13, v14, v15}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 211
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v15, "CONFIG_OPTION_PUT"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v15, v0, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 214
    new-instance v11, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-direct {v11, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    .line 215
    .local v11, "shcm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    if-eqz v11, :cond_2

    .line 216
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupWifiEnabled(Landroid/content/Context;)Z

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->useWifiForBackup(Z)Z

    .line 219
    :cond_2
    if-eqz v9, :cond_3

    .line 220
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->isUpgraded:Z

    .line 223
    :cond_3
    if-eqz v8, :cond_4

    .line 225
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->skipProfileSetup(Landroid/content/Context;)V

    .line 226
    new-instance v12, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    .line 227
    .local v12, "uiPrefsHelper":Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setInitialSettingOngoing(Z)V

    .line 228
    sget-object v13, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    const-string v14, "Restore has done by Kies : Skip setting profile step and go home"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    new-instance v7, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-class v14, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v7, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .local v7, "intentA":Landroid/content/Intent;
    const-string v13, "com.sec.shealth.action.STEALTH_MODE"

    invoke-virtual {v7, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    const/high16 v13, 0x4000000

    invoke-virtual {v7, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 232
    const-string v13, "IS_FIRST"

    const/4 v14, 0x1

    invoke-virtual {v7, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 233
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivity(Landroid/content/Intent;)V

    .line 236
    new-instance v5, Landroid/content/Intent;

    const-string v13, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 237
    .local v5, "finishProfile":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->sendBroadcast(Landroid/content/Intent;)V

    .line 240
    .end local v5    # "finishProfile":Landroid/content/Intent;
    .end local v7    # "intentA":Landroid/content/Intent;
    .end local v12    # "uiPrefsHelper":Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v13}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_6

    if-eqz v9, :cond_6

    .line 243
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->skipProfileSetup(Landroid/content/Context;)V

    .line 244
    new-instance v12, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    .line 245
    .restart local v12    # "uiPrefsHelper":Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setInitialSettingOngoing(Z)V

    .line 246
    sget-object v13, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    const-string v14, "Upgrade has be done : Skip setting profile step and go home"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    new-instance v7, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-class v14, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v7, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 248
    .restart local v7    # "intentA":Landroid/content/Intent;
    const-string v13, "com.sec.shealth.action.STEALTH_MODE"

    invoke-virtual {v7, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    const/high16 v13, 0x4000000

    invoke-virtual {v7, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 250
    const-string v13, "IS_UPGRADE_DONE"

    const/4 v14, 0x1

    invoke-virtual {v7, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 252
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupSwitchStatus(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 254
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupInterval(Landroid/content/Context;)J

    move-result-wide v2

    .line 255
    .local v2, "autoBackupInterval":J
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13, v14, v2, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->setPeroidicSync(Landroid/content/Context;J)V

    .line 258
    .end local v2    # "autoBackupInterval":J
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->startActivity(Landroid/content/Intent;)V

    .line 261
    new-instance v5, Landroid/content/Intent;

    const-string v13, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 262
    .restart local v5    # "finishProfile":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->sendBroadcast(Landroid/content/Intent;)V

    .line 265
    .end local v5    # "finishProfile":Landroid/content/Intent;
    .end local v7    # "intentA":Landroid/content/Intent;
    .end local v12    # "uiPrefsHelper":Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    :cond_6
    if-eqz p1, :cond_7

    .line 266
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    .line 267
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    .line 268
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->SCREEN_POSITION:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    .line 269
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->SHEALTHPROFILE_DATA:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 271
    :cond_7
    new-instance v13, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 272
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->initLayout()V

    .line 273
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->setUpDefaultUnitsByLocale()V

    .line 275
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->nextButton:Landroid/widget/RelativeLayout;

    new-instance v14, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->getPeriodicSync(Landroid/content/Context;)J

    move-result-wide v13

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-eqz v13, :cond_8

    .line 290
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "com.sec.android.app.shealth"

    const-string v15, "IS05"

    const-string v16, "ON"

    invoke-static/range {v13 .. v16}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :goto_0
    return-void

    .line 292
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "com.sec.android.app.shealth"

    const-string v15, "IS05"

    const-string v16, "OFF"

    invoke-static/range {v13 .. v16}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->deleteTempFile()V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFifthView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->cleanUp()V

    .line 467
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->bSaveInstaceState:Z

    .line 468
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 469
    return-void
.end method

.method public onFinished(II)V
    .locals 0
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    .line 1206
    return-void
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 1203
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 1146
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->bSaveInstaceState:Z

    .line 1147
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 1148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1149
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 1151
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1152
    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$5;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$5;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;Landroid/view/View;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1160
    :cond_0
    return-void
.end method

.method public onSaveImageView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 452
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "full_path":Ljava/lang/String;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    .line 454
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setImage(Landroid/graphics/Bitmap;)V

    .line 457
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mCameraFlag:Z

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    .line 459
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 878
    if-eqz p1, :cond_3

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 883
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 885
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v0, :cond_2

    .line 886
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->SHEALTHPROFILE_DATA:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 888
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->SCREEN_POSITION:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 889
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->bSaveInstaceState:Z

    .line 890
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 893
    :cond_3
    return-void
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 0
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 1200
    return-void
.end method
