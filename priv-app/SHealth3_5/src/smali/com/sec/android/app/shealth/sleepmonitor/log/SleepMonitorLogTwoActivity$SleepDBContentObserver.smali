.class public Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;
.super Landroid/database/ContentObserver;
.source "SleepMonitorLogTwoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SleepDBContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    .line 260
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 261
    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 265
    invoke-super {p0}, Landroid/database/ContentObserver;->deliverSelfNotifications()Z

    move-result v0

    return v0
.end method

.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 270
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    const-string v1, "0==0"

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->executeQuery(Ljava/lang/CharSequence;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->access$000(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->access$100(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->setGroupCursor(Landroid/database/Cursor;)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->notifyDataSetChanged(Z)V

    .line 274
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 278
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    const-string v1, "0==0"

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->executeQuery(Ljava/lang/CharSequence;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->access$000(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;Ljava/lang/CharSequence;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->setGroupCursor(Landroid/database/Cursor;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->notifyDataSetChanged(Z)V

    .line 282
    return-void
.end method
