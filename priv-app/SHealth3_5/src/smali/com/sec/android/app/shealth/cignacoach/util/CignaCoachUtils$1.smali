.class synthetic Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;
.super Ljava/lang/Object;
.source "CignaCoachUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

.field static final synthetic $SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetType:[I

.field static final synthetic $SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 766
    invoke-static {}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->values()[Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetType:[I

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    .line 262
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->values()[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    :try_start_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    :goto_2
    :try_start_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_3
    :try_start_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_4
    :try_start_5
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_5
    :try_start_6
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    .line 203
    :goto_6
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    :try_start_7
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_7
    :try_start_8
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    :goto_8
    :try_start_9
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_9
    :try_start_a
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_a
    :try_start_b
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_b
    return-void

    :catch_0
    move-exception v0

    goto :goto_b

    :catch_1
    move-exception v0

    goto :goto_a

    :catch_2
    move-exception v0

    goto :goto_9

    :catch_3
    move-exception v0

    goto :goto_8

    :catch_4
    move-exception v0

    goto :goto_7

    .line 262
    :catch_5
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_4

    :catch_8
    move-exception v0

    goto :goto_3

    :catch_9
    move-exception v0

    goto :goto_2

    .line 766
    :catch_a
    move-exception v0

    goto/16 :goto_1

    :catch_b
    move-exception v0

    goto/16 :goto_0
.end method
