.class public Lcom/sec/android/app/shealth/home/HomeFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;,
        Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static context:Landroid/content/Context;


# instance fields
.field private homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

.field private mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

.field private mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

.field private registryContentObserver:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

.field private rootView:Landroid/view/View;

.field private shortcut_container:Landroid/widget/FrameLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/home/HomeFragment;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->rootView:Landroid/view/View;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeFragment;

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/HomeFragment;)Lcom/sec/android/app/shealth/home/HomeShortcutManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeFragment;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/HomeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeFragment;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->isGearDevice()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/home/HomeFragment;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeFragment;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    return-object v0
.end method

.method private isGearDevice()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    .line 128
    .local v0, "checkForGear":Z
    return v0
.end method


# virtual methods
.method public getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    return-object v0
.end method

.method public getPluginListCount()I
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->isGearDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->getFavCount()I

    move-result v0

    .line 226
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/shealth/home/HomeFragment;->context:Landroid/content/Context;

    .line 82
    new-instance v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->rootView:Landroid/view/View;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;-><init>(Landroid/app/Activity;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "wmanager_connected"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "connected":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->updateView(Ljava/lang/Boolean;)V

    .line 91
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    new-instance v3, Lcom/sec/android/app/shealth/home/HomeFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/home/HomeFragment$1;-><init>(Lcom/sec/android/app/shealth/home/HomeFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setRefreshListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;)V

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->rootView:Landroid/view/View;

    const v3, 0x7f0804f7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->shortcut_container:Landroid/widget/FrameLayout;

    .line 105
    new-instance v3, Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v3, v2}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->isGearDevice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->shortcut_container:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->shortcut_container:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->initialise()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 119
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->rootView:Landroid/view/View;

    const v3, 0x7f0804f3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    .line 114
    .local v1, "dashboardScroll":Landroid/widget/ScrollView;
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->shortcut_container:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 115
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    const v0, 0x7f03012e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->rootView:Landroid/view/View;

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->initialize()V

    .line 75
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;-><init>(Lcom/sec/android/app/shealth/home/HomeFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->registryContentObserver:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->registryContentObserver:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->rootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->registryContentObserver:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->registryContentObserver:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 160
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->registryContentObserver:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->destroy()V

    .line 165
    :cond_1
    sput-object v2, Lcom/sec/android/app/shealth/home/HomeFragment;->context:Landroid/content/Context;

    .line 166
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 167
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->doUnbindWalkStepService()V

    .line 152
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 153
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    if-nez v2, :cond_1

    .line 174
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 195
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

    if-nez v2, :cond_2

    .line 179
    new-instance v2, Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;-><init>(Lcom/sec/android/app/shealth/home/HomeFragment;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

    .line 182
    :cond_2
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.app.shealth.pedometer.viewstepcount"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 183
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/home/HomeFragment$DeviceCheckedReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    sget-object v3, Lcom/sec/android/app/shealth/home/HomeFragment;->context:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->doBindWalkStepService(Landroid/content/Context;)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "wmanager_connected"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "connected":Ljava/lang/String;
    if-eqz v0, :cond_3

    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 189
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->mHomeLatestData:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->updateView(Ljava/lang/Boolean;)V

    .line 192
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeFragment;->isGearDevice()Z

    move-result v2

    if-nez v2, :cond_5

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->refresh()V

    .line 194
    :cond_5
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    goto :goto_0
.end method
