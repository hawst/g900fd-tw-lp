.class public Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "SubCategoryListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mSelectedCategoryId:I

.field private mSelectedCategoryTitle:Ljava/lang/String;

.field private mSubCategoryListData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSubCategoryListData:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 31
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_name_category_id"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryId:I

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->validateIntentData()Z

    move-result v2

    if-nez v2, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->finish()V

    .line 50
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v2, 0x7f030055

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->setContentView(I)V

    .line 41
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryId:I

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSubCategoryListData:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getLibrarySubCategory(ILjava/util/ArrayList;)V

    .line 42
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getLibraryCategroyTitle(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryTitle:Ljava/lang/String;

    .line 43
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryTitle:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 47
    :cond_1
    const v2, 0x7f0801c3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 48
    .local v1, "subCategoryList":Landroid/widget/ListView;
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSubCategoryListData:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 49
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    .line 57
    .local v1, "subCategoryData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 59
    const-string v2, "extra_name_category_id"

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryId:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 60
    const-string v2, "extra_name_sub_category_id"

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    return-void
.end method

.method public validateIntentData()Z
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->mSelectedCategoryId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 68
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;->TAG:Ljava/lang/String;

    const-string v1, "Intent data is error"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
