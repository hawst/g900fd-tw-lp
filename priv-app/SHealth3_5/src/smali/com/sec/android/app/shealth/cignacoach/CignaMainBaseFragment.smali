.class public Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "CignaMainBaseFragment.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 16
    const-string v0, "CignaMainBaseFragment"

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method private processBadgeData(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    if-eqz p1, :cond_1

    .line 38
    const-string v2, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 39
    .local v1, "receiveBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v2, "intent_lifestyle_category"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 40
    .local v0, "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 41
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->startBadgeActivity(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 46
    .end local v0    # "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .end local v1    # "receiveBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    return-void
.end method

.method private startBadgeActivity(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V
    .locals 4
    .param p2, "category"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "badgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v3, 0x115c

    .line 49
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_NEED_UPATE_MAIN_SCORE_ANIMATION"

    const/16 v2, 0x2b67

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 51
    if-eqz p2, :cond_0

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->TAG:Ljava/lang/String;

    const-string v2, "CignaMainBaseFragment single assessment"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const-string v1, "intent_lifestyle_category"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 55
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 56
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 58
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->convertCategoryToRequestCode(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 68
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->TAG:Ljava/lang/String;

    const-string v2, "CignaMainBaseFragment answer quetion"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 31
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->processBadgeData(Landroid/content/Intent;)V

    .line 26
    :cond_0
    return-void
.end method
