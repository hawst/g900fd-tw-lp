.class Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;

.field final synthetic val$okButtonHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;->val$okButtonHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8

    const/4 v0, 0x3

    new-array v2, v0, [J

    const/4 v0, 0x2

    const-wide/16 v3, 0x3

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p2

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, 0x3a753cd8c69436a1L    # 4.288915872477467E-27

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x3a753cd8c69436a1L    # 4.288915872477467E-27

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_2

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_3

    const-wide v6, 0x3a753cd8c69436a1L    # 4.288915872477467E-27

    xor-long/2addr v0, v6

    :cond_3
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x3a753cd8c69436a1L    # 4.288915872477467E-27

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x2

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_4

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const/4 v3, 0x1

    int-to-long v0, p4

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_5

    const-wide v6, 0x3a753cd8c69436a1L    # 4.288915872477467E-27

    xor-long/2addr v0, v6

    :cond_5
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x3a753cd8c69436a1L    # 4.288915872477467E-27

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8

    const/4 v0, 0x3

    new-array v2, v0, [J

    const/4 v0, 0x2

    const-wide/16 v3, 0x3

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p2

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, 0x1c30cf0d7fbeb7beL    # 6.796091231946864E-173

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x1c30cf0d7fbeb7beL    # 6.796091231946864E-173

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_2

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_3

    const-wide v6, 0x1c30cf0d7fbeb7beL    # 6.796091231946864E-173

    xor-long/2addr v0, v6

    :cond_3
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x1c30cf0d7fbeb7beL    # 6.796091231946864E-173

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x2

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_4

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const/4 v3, 0x1

    int-to-long v0, p4

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_5

    const-wide v6, 0x1c30cf0d7fbeb7beL    # 6.796091231946864E-173

    xor-long/2addr v0, v6

    :cond_5
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x1c30cf0d7fbeb7beL    # 6.796091231946864E-173

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$502(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;->val$okButtonHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;->setEnabled(Z)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;->val$okButtonHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;->setEnabled(Z)V

    goto :goto_0
.end method
