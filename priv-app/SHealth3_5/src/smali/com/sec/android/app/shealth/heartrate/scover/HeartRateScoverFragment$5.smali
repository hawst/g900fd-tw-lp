.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;
.super Ljava/lang/Object;
.source "HeartRateScoverFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startSensorAndDelayUIForDetectedFinger(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 0

    .prologue
    .line 477
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020526

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 485
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 487
    :cond_2
    return-void
.end method
