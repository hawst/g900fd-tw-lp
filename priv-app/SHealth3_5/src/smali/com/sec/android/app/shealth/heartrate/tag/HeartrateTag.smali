.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag;
.super Ljava/lang/Object;
.source "HeartrateTag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;,
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;
    }
.end annotation


# static fields
.field public static final TAG_ICON_ID_ANGRY:I = 0x56bb

.field public static final TAG_ICON_ID_AT_WORK:I = 0x5725

.field public static final TAG_ICON_ID_BABY_BOY:I = 0x5781

.field public static final TAG_ICON_ID_BABY_GIRL:I = 0x5782

.field public static final TAG_ICON_ID_BOY:I = 0x5783

.field public static final TAG_ICON_ID_CAT:I = 0x57e6

.field public static final TAG_ICON_ID_COFFEE:I = 0x5720

.field public static final TAG_ICON_ID_DISCUSS:I = 0x5727

.field public static final TAG_ICON_ID_DOG:I = 0x57e5

.field public static final TAG_ICON_ID_DOSAGE:I = 0x5728

.field public static final TAG_ICON_ID_DRINKING:I = 0x5721

.field public static final TAG_ICON_ID_ELDERLY_MAN:I = 0x5789

.field public static final TAG_ICON_ID_ELDERLY_WOMAN:I = 0x578a

.field public static final TAG_ICON_ID_EXCITED:I = 0x56b9

.field public static final TAG_ICON_ID_EXERCISE:I = 0x571e

.field public static final TAG_ICON_ID_FEARFUL:I = 0x56bd

.field public static final TAG_ICON_ID_GIRL:I = 0x5784

.field public static final TAG_ICON_ID_HORSE:I = 0x57e7

.field public static final TAG_ICON_ID_IN_LOVE:I = 0x56be

.field public static final TAG_ICON_ID_MAN:I = 0x5785

.field public static final TAG_ICON_ID_MEETING:I = 0x5726

.field public static final TAG_ICON_ID_MIDDLEAGE_MAN:I = 0x5787

.field public static final TAG_ICON_ID_MIDDLEAGE_WOMAN:I = 0x5788

.field public static final TAG_ICON_ID_NONE:I = 0x55f0

.field public static final TAG_ICON_ID_PARTY:I = 0x571f

.field public static final TAG_ICON_ID_PLANE:I = 0x5729

.field public static final TAG_ICON_ID_RELAXING:I = 0x571d

.field public static final TAG_ICON_ID_RUNNING:I = 0x5655

.field public static final TAG_ICON_ID_SAD:I = 0x56bc

.field public static final TAG_ICON_ID_SMOKING:I = 0x5722

.field public static final TAG_ICON_ID_SURPRISED:I = 0x56ba

.field public static final TAG_ICON_ID_WALKING:I = 0x5657

.field public static final TAG_ICON_ID_WOMAN:I = 0x5786

.field public static final TAG_ID_ANGRY:I = 0x52d3

.field public static final TAG_ID_COFFEE:I = 0x5338

.field public static final TAG_ID_DEFAULT:I = 0x4e20

.field public static final TAG_ID_DRINKING:I = 0x5339

.field public static final TAG_ID_EXCITED:I = 0x52d1

.field public static final TAG_ID_EXERCISE:I = 0x5336

.field public static final TAG_ID_FEARFUL:I = 0x52d5

.field public static final TAG_ID_HIDDEN_HEALTHY_PACE:I = 0x533c

.field public static final TAG_ID_HIDDEN_SLEEP:I = 0x533b

.field public static final TAG_ID_IN_LOVE:I = 0x52d6

.field public static final TAG_ID_MAX:I = 0x533c

.field public static final TAG_ID_NONE:I = 0x5208

.field public static final TAG_ID_OTHER_EXERCISES:I = 0x527d

.field public static final TAG_ID_PARTY:I = 0x5337

.field public static final TAG_ID_RESTING:I = 0x5335

.field public static final TAG_ID_RUNNING:I = 0x526d

.field public static final TAG_ID_SAD:I = 0x52d4

.field public static final TAG_ID_SMOKING:I = 0x533a

.field public static final TAG_ID_SURPRISED:I = 0x52d2

.field public static final TAG_ID_WALKING:I = 0x526f

.field public static final TAG_SLEEP:I = 0x533b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    return-void
.end method
