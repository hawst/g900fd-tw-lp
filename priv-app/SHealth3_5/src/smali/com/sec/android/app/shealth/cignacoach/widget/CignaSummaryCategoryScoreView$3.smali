.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$3;
.super Ljava/lang/Object;
.source "CignaSummaryCategoryScoreView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScore(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 295
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 297
    .local v0, "anmWidth":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 298
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    float-to-int v2, v0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 300
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 301
    return-void
.end method
