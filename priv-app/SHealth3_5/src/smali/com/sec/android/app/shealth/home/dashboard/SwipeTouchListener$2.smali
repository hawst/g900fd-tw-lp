.class Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SwipeTouchListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->performDismiss()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

.field final synthetic val$lp:Landroid/view/ViewGroup$LayoutParams;

.field final synthetic val$originalHeight:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;Landroid/view/ViewGroup$LayoutParams;I)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    iput-object p2, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    iput p3, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->val$originalHeight:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mCallbacks:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->access$300(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->access$100(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mObj:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->access$200(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;->onDismiss(Landroid/view/View;Ljava/lang/Object;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->access$100(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->access$100(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->val$originalHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->access$100(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    return-void
.end method
