.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;
.super Ljava/lang/Object;
.source "UnitSettingActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveUnitsDismissController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0

    .prologue
    .line 594
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Lcom/sec/android/app/shealth/settings/UnitSettingActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity$1;

    .prologue
    .line 594
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$900(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$900(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->clear()V

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->initUnitValue()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1000(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1102(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 609
    return-void
.end method
