.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ImagePager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExercisePhotoAdapter"
.end annotation


# instance fields
.field private photoDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V
    .locals 1

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 322
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;

    .prologue
    .line 321
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 326
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 327
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 358
    const/4 v0, -0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 331
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 332
    .local v1, "filepath":Ljava/lang/String;
    new-instance v2, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 333
    .local v2, "imageView":Landroid/widget/ImageView;
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mWidth:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mHeight:I
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 334
    .local v3, "param":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 335
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mSynObject:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 336
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 337
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 338
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mWidth:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mHeight:I
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I

    move-result v7

    invoke-static {v4, v1, v6, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getImageThumbnail(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 339
    if-nez v0, :cond_0

    .line 340
    const/4 v2, 0x0

    monitor-exit v5

    .line 348
    .end local v2    # "imageView":Landroid/widget/ImageView;
    :goto_0
    return-object v2

    .line 341
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    :cond_1
    const/high16 v4, -0x1000000

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 344
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 345
    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 346
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 347
    monitor-exit v5

    goto :goto_0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "imageView":Landroid/widget/ImageView;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 363
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 368
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->isDelBtnForceGone:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButton:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 374
    :goto_0
    return-void

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButton:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
