.class public final Lcom/sec/android/app/shealth/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CircleProgressView:[I

.field public static final CircleProgressView_progressImage:I = 0x0

.field public static final CustomFont:[I

.field public static final CustomFontTextView:[I

.field public static final CustomFontTextView_customFont:I = 0x0

.field public static final CustomFont_cigna_customFont:I = 0x0

.field public static final CustomTextViewStroke:[I

.field public static final CustomTextViewStroke_textStroke:I = 0x0

.field public static final CustomTextViewStroke_textStrokeColor:I = 0x2

.field public static final CustomTextViewStroke_textStrokeWidth:I = 0x1

.field public static final DragSortListView:[I

.field public static final DragSortListView_click_remove_id:I = 0x10

.field public static final DragSortListView_collapsed_height:I = 0x0

.field public static final DragSortListView_drag_enabled:I = 0xa

.field public static final DragSortListView_drag_handle_id:I = 0xe

.field public static final DragSortListView_drag_scroll_start:I = 0x1

.field public static final DragSortListView_drag_start_mode:I = 0xd

.field public static final DragSortListView_drop_animation_duration:I = 0x9

.field public static final DragSortListView_fling_handle_id:I = 0xf

.field public static final DragSortListView_float_alpha:I = 0x6

.field public static final DragSortListView_float_background_color:I = 0x3

.field public static final DragSortListView_max_drag_scroll_speed:I = 0x2

.field public static final DragSortListView_remove_animation_duration:I = 0x8

.field public static final DragSortListView_remove_enabled:I = 0xc

.field public static final DragSortListView_remove_mode:I = 0x4

.field public static final DragSortListView_slide_shuffle_speed:I = 0x7

.field public static final DragSortListView_sort_enabled:I = 0xb

.field public static final DragSortListView_track_drag_sort:I = 0x5

.field public static final DragSortListView_use_default_controller:I = 0x11

.field public static final DropDownListButton:[I

.field public static final DropDownListButton_text:I = 0x0

.field public static final DropDownListButton_textColor:I = 0x2

.field public static final DropDownListButton_textFont:I = 0x3

.field public static final DropDownListButton_textSize:I = 0x1

.field public static final EditTextWithLengthLimit:[I

.field public static final EditTextWithLengthLimit_idForToastText:I = 0x0

.field public static final GIFView:[I

.field public static final GIFView_gif:I = 0x0

.field public static final HealthCareProgressBar:[I

.field public static final HealthCareProgressBar_isLeftAligned:I = 0x0

.field public static final HeartrateCircleProgressView:[I

.field public static final HeartrateCircleProgressView_externalRadius:I = 0x1

.field public static final HeartrateCircleProgressView_innerRadius:I = 0x0

.field public static final HeartrateCustomTextView:[I

.field public static final HeartrateCustomTextView_font:I = 0x0

.field public static final HelpImageViewer:[I

.field public static final HelpImageViewer_autoStart:I = 0x3

.field public static final HelpImageViewer_drawables:I = 0x0

.field public static final HelpImageViewer_oneshot:I = 0x4

.field public static final HelpImageViewer_period:I = 0x2

.field public static final HelpImageViewer_show_slide_time:I = 0x5

.field public static final HelpImageViewer_startButton:I = 0x1

.field public static final HelpTextView:[I

.field public static final HelpTextView_help_text:I = 0x0

.field public static final HelpTextView_image:I = 0xe

.field public static final HelpTextView_images:I = 0xd

.field public static final HelpTextView_insideImg_gravity:I = 0xc

.field public static final HelpTextView_insideImg_height:I = 0x6

.field public static final HelpTextView_insideImg_padding:I = 0x5

.field public static final HelpTextView_insideImg_paddingBottom:I = 0x4

.field public static final HelpTextView_insideImg_paddingLeft:I = 0x1

.field public static final HelpTextView_insideImg_paddingRight:I = 0x2

.field public static final HelpTextView_insideImg_paddingTop:I = 0x3

.field public static final HelpTextView_insideImg_width:I = 0x7

.field public static final HelpTextView_insideImg_yDiff:I = 0x8

.field public static final HelpTextView_newLineString:I = 0xb

.field public static final HelpTextView_showDisplayString:I = 0xa

.field public static final HelpTextView_showDisplayValue:I = 0x9

.field public static final InputModule:[I

.field public static final InputModule_drawing_strategy:I = 0x0

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd

.field public static final SHealthCircleProgressView:[I

.field public static final SHealthCircleProgressView_colorFirst:I = 0x2

.field public static final SHealthCircleProgressView_externalRadiusFirst:I = 0x1

.field public static final SHealthCircleProgressView_innerRadiusFirst:I = 0x0

.field public static final SpO2CircleProgressView:[I

.field public static final SpO2CircleProgressView_externalRadius_spo2:I = 0x1

.field public static final SpO2CircleProgressView_innerRadius_spo2:I = 0x0

.field public static final StressCircleProgressView:[I

.field public static final StressCircleProgressView_color1:I = 0x2

.field public static final StressCircleProgressView_color2:I = 0x3

.field public static final StressCircleProgressView_externalRadiusStm:I = 0x1

.field public static final StressCircleProgressView_innerRadiusStm:I = 0x0

.field public static final SummaryButton:[I

.field public static final SummaryButton_btnIcon:I = 0x1

.field public static final SummaryButton_btnIconVisibility:I = 0x3

.field public static final SummaryButton_btnRightMargin:I = 0x2

.field public static final SummaryButton_btnTitleText:I = 0x0

.field public static final VerticalProgressbar:[I

.field public static final VerticalProgressbar_balance_background:I = 0x6

.field public static final VerticalProgressbar_layoutType:I = 0x7

.field public static final VerticalProgressbar_maxBalance:I = 0x3

.field public static final VerticalProgressbar_maxValue:I = 0x1

.field public static final VerticalProgressbar_minBalance:I = 0x2

.field public static final VerticalProgressbar_minValue:I = 0x0

.field public static final VerticalProgressbar_progressbar_background:I = 0x5

.field public static final VerticalProgressbar_value_labels_array:I = 0x4

.field public static final cigna_workout_HoloCircleSeekBar:[I

.field public static final cigna_workout_HoloCircleSeekBar_cigna_viewSize:I = 0xe

.field public static final cigna_workout_HoloCircleSeekBar_color:I = 0x8

.field public static final cigna_workout_HoloCircleSeekBar_end_angle:I = 0x5

.field public static final cigna_workout_HoloCircleSeekBar_init_position:I = 0x7

.field public static final cigna_workout_HoloCircleSeekBar_max:I = 0x2

.field public static final cigna_workout_HoloCircleSeekBar_pointer_color:I = 0xb

.field public static final cigna_workout_HoloCircleSeekBar_pointer_halo_color:I = 0xc

.field public static final cigna_workout_HoloCircleSeekBar_pointer_size:I = 0x1

.field public static final cigna_workout_HoloCircleSeekBar_show_text:I = 0x3

.field public static final cigna_workout_HoloCircleSeekBar_start_angle:I = 0x4

.field public static final cigna_workout_HoloCircleSeekBar_text_color:I = 0xd

.field public static final cigna_workout_HoloCircleSeekBar_text_size:I = 0x6

.field public static final cigna_workout_HoloCircleSeekBar_wheel_active_color:I = 0x9

.field public static final cigna_workout_HoloCircleSeekBar_wheel_size:I = 0x0

.field public static final cigna_workout_HoloCircleSeekBar_wheel_unactive_color:I = 0xa

.field public static final workout_ExerciseProCircleSeekBar:[I

.field public static final workout_ExerciseProCircleSeekBar_pro_color:I = 0x8

.field public static final workout_ExerciseProCircleSeekBar_pro_end_angle:I = 0x5

.field public static final workout_ExerciseProCircleSeekBar_pro_init_position:I = 0x7

.field public static final workout_ExerciseProCircleSeekBar_pro_max:I = 0x2

.field public static final workout_ExerciseProCircleSeekBar_pro_pointer_color:I = 0xb

.field public static final workout_ExerciseProCircleSeekBar_pro_pointer_halo_color:I = 0xc

.field public static final workout_ExerciseProCircleSeekBar_pro_pointer_size:I = 0x1

.field public static final workout_ExerciseProCircleSeekBar_pro_show_text:I = 0x3

.field public static final workout_ExerciseProCircleSeekBar_pro_start_angle:I = 0x4

.field public static final workout_ExerciseProCircleSeekBar_pro_text_color:I = 0xd

.field public static final workout_ExerciseProCircleSeekBar_pro_text_size:I = 0x6

.field public static final workout_ExerciseProCircleSeekBar_pro_viewSize:I = 0xe

.field public static final workout_ExerciseProCircleSeekBar_pro_wheel_active_color:I = 0x9

.field public static final workout_ExerciseProCircleSeekBar_pro_wheel_size:I = 0x0

.field public static final workout_ExerciseProCircleSeekBar_pro_wheel_unactive_color:I = 0xa

.field public static final workout_HoloCircleSeekBar:[I

.field public static final workout_HoloCircleSeekBar_ex_color:I = 0x8

.field public static final workout_HoloCircleSeekBar_ex_end_angle:I = 0x5

.field public static final workout_HoloCircleSeekBar_ex_init_position:I = 0x7

.field public static final workout_HoloCircleSeekBar_ex_max:I = 0x2

.field public static final workout_HoloCircleSeekBar_ex_pointer_color:I = 0xb

.field public static final workout_HoloCircleSeekBar_ex_pointer_halo_color:I = 0xc

.field public static final workout_HoloCircleSeekBar_ex_pointer_size:I = 0x1

.field public static final workout_HoloCircleSeekBar_ex_show_text:I = 0x3

.field public static final workout_HoloCircleSeekBar_ex_start_angle:I = 0x4

.field public static final workout_HoloCircleSeekBar_ex_text_color:I = 0xd

.field public static final workout_HoloCircleSeekBar_ex_text_size:I = 0x6

.field public static final workout_HoloCircleSeekBar_ex_viewSize:I = 0xe

.field public static final workout_HoloCircleSeekBar_ex_wheel_active_color:I = 0x9

.field public static final workout_HoloCircleSeekBar_ex_wheel_size:I = 0x0

.field public static final workout_HoloCircleSeekBar_ex_wheel_unactive_color:I = 0xa

.field public static final workout_HoloCircleSeekBar_goalColor:I = 0x1e

.field public static final workout_HoloCircleSeekBar_viewSize:I = 0x1d

.field public static final workout_HoloCircleSeekBar_walk_color:I = 0x17

.field public static final workout_HoloCircleSeekBar_walk_end_angle:I = 0x14

.field public static final workout_HoloCircleSeekBar_walk_init_position:I = 0x16

.field public static final workout_HoloCircleSeekBar_walk_max:I = 0x11

.field public static final workout_HoloCircleSeekBar_walk_pointer_color:I = 0x1a

.field public static final workout_HoloCircleSeekBar_walk_pointer_halo_color:I = 0x1b

.field public static final workout_HoloCircleSeekBar_walk_pointer_size:I = 0x10

.field public static final workout_HoloCircleSeekBar_walk_show_text:I = 0x12

.field public static final workout_HoloCircleSeekBar_walk_start_angle:I = 0x13

.field public static final workout_HoloCircleSeekBar_walk_text_color:I = 0x1c

.field public static final workout_HoloCircleSeekBar_walk_text_size:I = 0x15

.field public static final workout_HoloCircleSeekBar_walk_wheel_active_color:I = 0x18

.field public static final workout_HoloCircleSeekBar_walk_wheel_size:I = 0xf

.field public static final workout_HoloCircleSeekBar_walk_wheel_unactive_color:I = 0x19


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/16 v5, 0xf

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x7f01001c

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->CircleProgressView:[I

    new-array v0, v3, [I

    const v1, 0x7f010041

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->CustomFont:[I

    new-array v0, v3, [I

    const v1, 0x7f010037

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->CustomFontTextView:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->CustomTextViewStroke:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->DragSortListView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->DropDownListButton:[I

    new-array v0, v3, [I

    const v1, 0x7f01001b

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->EditTextWithLengthLimit:[I

    new-array v0, v3, [I

    const v1, 0x7f010031

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->GIFView:[I

    new-array v0, v3, [I

    const v1, 0x7f01000e

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->HealthCareProgressBar:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->HeartrateCircleProgressView:[I

    new-array v0, v3, [I

    const v1, 0x7f010038

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->HeartrateCustomTextView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->HelpImageViewer:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f01001d

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->InputModule:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->MapAttrs:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->SHealthCircleProgressView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->SpO2CircleProgressView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->StressCircleProgressView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->SummaryButton:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->VerticalProgressbar:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->cigna_workout_HoloCircleSeekBar:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->workout_ExerciseProCircleSeekBar:[I

    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/sec/android/app/shealth/R$styleable;->workout_HoloCircleSeekBar:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f01003c
        0x7f01003d
        0x7f01003e
    .end array-data

    :array_1
    .array-data 4
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
    .end array-data

    :array_2
    .array-data 4
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
    .end array-data

    :array_3
    .array-data 4
        0x7f01003f
        0x7f010040
    .end array-data

    :array_4
    .array-data 4
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
    .end array-data

    :array_5
    .array-data 4
        0x7f01007f
        0x7f010080
        0x7f010081
        0x7f010082
        0x7f010083
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
        0x7f01008b
        0x7f01008c
        0x7f01008d
    .end array-data

    :array_6
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
    .end array-data

    :array_7
    .array-data 4
        0x7f010039
        0x7f01003a
        0x7f01003b
    .end array-data

    :array_8
    .array-data 4
        0x7f010094
        0x7f010095
    .end array-data

    :array_9
    .array-data 4
        0x7f010096
        0x7f010097
        0x7f010098
        0x7f010099
    .end array-data

    :array_a
    .array-data 4
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
    .end array-data

    :array_b
    .array-data 4
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
    .end array-data

    :array_c
    .array-data 4
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
    .end array-data

    :array_d
    .array-data 4
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f010065
        0x7f010066
        0x7f010067
        0x7f010068
        0x7f010069
        0x7f01006a
        0x7f01006b
        0x7f01006c
        0x7f01006d
        0x7f01006e
    .end array-data

    :array_e
    .array-data 4
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f01006f
        0x7f010070
        0x7f010071
        0x7f010072
        0x7f010073
        0x7f010074
        0x7f010075
        0x7f010076
        0x7f010077
        0x7f010078
        0x7f010079
        0x7f01007a
        0x7f01007b
        0x7f01007c
        0x7f01007d
        0x7f01007e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
