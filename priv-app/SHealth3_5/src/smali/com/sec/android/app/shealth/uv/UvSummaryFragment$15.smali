.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$15;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->scaleAnimationStarter(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0

    .prologue
    .line 1899
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "value"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 1903
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 1904
    .local v0, "scaleValue":Ljava/lang/Float;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1905
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$2300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1906
    return-void
.end method
