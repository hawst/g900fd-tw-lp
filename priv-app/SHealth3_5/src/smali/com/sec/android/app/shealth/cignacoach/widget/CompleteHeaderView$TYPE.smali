.class public final enum Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;
.super Ljava/lang/Enum;
.source "CompleteHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

.field public static final enum BADGE:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

.field public static final enum GOAL:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

.field public static final enum MISSION:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    const-string v1, "GOAL"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->GOAL:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    .line 17
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    const-string v1, "MISSION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->MISSION:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    .line 18
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    const-string v1, "BADGE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->BADGE:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->GOAL:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->MISSION:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->BADGE:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    return-object v0
.end method
