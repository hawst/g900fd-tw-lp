.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;
.super Landroid/view/animation/Animation;
.source "DropDownAnime.java"


# instance fields
.field private mIsExpandAnimation:Z

.field private mTargetHeight:I

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;IZ)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "targetHeight"    # I
    .param p3, "isExpandAnimation"    # Z

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mView:Landroid/view/View;

    .line 15
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mTargetHeight:I

    .line 16
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mIsExpandAnimation:Z

    .line 17
    return-void
.end method


# virtual methods
.method public afterChanges()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 21
    const-string v1, "applyTransformation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "applyTransformation : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mIsExpandAnimation:Z

    if-eqz v1, :cond_1

    .line 24
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mTargetHeight:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 27
    .local v0, "newHeight":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 28
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->afterChanges()V

    .line 30
    cmpl-float v1, p1, v4

    if-nez v1, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->finished()V

    .line 33
    :cond_0
    return-void

    .line 26
    .end local v0    # "newHeight":I
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->mTargetHeight:I

    int-to-float v1, v1

    sub-float v2, v4, p1

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .restart local v0    # "newHeight":I
    goto :goto_0
.end method

.method public finished()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public initialize(IIII)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "parentWidth"    # I
    .param p4, "parentHeight"    # I

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 38
    return-void
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method
