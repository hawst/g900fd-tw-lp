.class Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;
.super Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;
.source "SearchEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mOldCharseq:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V
    .locals 1

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;-><init>()V

    .line 109
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;->mOldCharseq:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v1, 0x0

    .line 123
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 124
    invoke-interface {p1, v6}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    sget-object v2, Ljava/lang/Character$UnicodeBlock;->HIGH_SURROGATES:Ljava/lang/Character$UnicodeBlock;

    if-ne v0, v2, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f09092c

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 127
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;->mOldCharseq:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;->mOldCharseq:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p1

    move v4, v1

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->refreshInnerIconsVisibilities(Ljava/lang/CharSequence;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$300(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;Ljava/lang/CharSequence;)V

    .line 134
    return-void

    .line 123
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 113
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 114
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;->mOldCharseq:Ljava/lang/String;

    .line 116
    return-void
.end method
