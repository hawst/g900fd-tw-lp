.class public interface abstract Lcom/sec/android/app/shealth/food/fooddao/MealDao;
.super Ljava/lang/Object;
.source "MealDao.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/GoalContainable;
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperationsWithFilter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperationsWithFilter",
        "<",
        "Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/GoalContainable;"
    }
.end annotation


# virtual methods
.method public abstract getMealsByName(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
            ">;"
        }
    .end annotation
.end method
