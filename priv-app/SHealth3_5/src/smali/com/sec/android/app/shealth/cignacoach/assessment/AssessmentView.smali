.class public Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;
.super Landroid/widget/RelativeLayout;
.source "AssessmentView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static gestureOn:Z

.field private static mMoveFisrtXAxis:F

.field private static mMoveLastXAxis:F


# instance fields
.field private activity:Landroid/app/Activity;

.field private dialogInterface:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

.field private layout:Landroid/view/View;

.field private mAnsweredIndex:I

.field private mAssessmentBottom:Landroid/widget/LinearLayout;

.field private mAssessmentFirst:Landroid/widget/RelativeLayout;

.field private mAssessmentFourth:Landroid/widget/RelativeLayout;

.field private mAssessmentHeaderNormal:Landroid/widget/RelativeLayout;

.field private mAssessmentHeaderWeight:Landroid/widget/RelativeLayout;

.field private mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

.field private mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

.field private mAssessmentSecond:Landroid/widget/RelativeLayout;

.field private mAssessmentThird:Landroid/widget/RelativeLayout;

.field private mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

.field private mContext:Landroid/content/Context;

.field private mGenderImage:Landroid/widget/ImageView;

.field private mGgestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private mHeaderBmpImg:Landroid/graphics/Bitmap;

.field private mHeightValueInCm:F

.field private mListener:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

.field private mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private mTopBackgroundImage:Landroid/widget/ImageView;

.field private mTouchXSlop:I

.field private mTouchYSlop:I

.field private mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

.field private mView:Landroid/view/View;

.field private mWeightAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

.field private mWeightValueInKg:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->TAG:Ljava/lang/String;

    .line 84
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->gestureOn:Z

    .line 85
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveLastXAxis:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAnsweredIndex:I

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGenderImage:Landroid/widget/ImageView;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGgestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 805
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$6;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->dialogInterface:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->activity:Landroid/app/Activity;

    .line 93
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mTouchXSlop:I

    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->initGesture()V

    .line 96
    return-void
.end method

.method static synthetic access$000()F
    .locals 1

    .prologue
    .line 56
    sget v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveLastXAxis:F

    return v0
.end method

.method static synthetic access$002(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 56
    sput p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveLastXAxis:F

    return p0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 56
    sget-boolean v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->gestureOn:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 56
    sput-boolean p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->gestureOn:Z

    return p0
.end method

.method static synthetic access$200()F
    .locals 1

    .prologue
    .line 56
    sget v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveFisrtXAxis:F

    return v0
.end method

.method static synthetic access$202(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 56
    sput p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveFisrtXAxis:F

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mTouchXSlop:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)Landroid/view/GestureDetector$SimpleOnGestureListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGgestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->showRangeDialog(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    return-object v0
.end method

.method private checkEditFocus()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 776
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 777
    .local v0, "v":Landroid/view/View;
    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 779
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 802
    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 782
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_0

    .line 783
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    move v1, v2

    .line 784
    goto :goto_0

    .line 792
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_0

    .line 793
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    move v1, v2

    .line 794
    goto :goto_0

    .line 779
    :sswitch_data_0
    .sparse-switch
        0x7f0801a7 -> :sswitch_1
        0x7f0801a9 -> :sswitch_1
        0x7f0802a4 -> :sswitch_0
    .end sparse-switch
.end method

.method private initGesture()V
    .locals 1

    .prologue
    .line 580
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGgestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 622
    return-void
.end method

.method private initUnit()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-nez v0, :cond_1

    .line 208
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 210
    :cond_1
    return-void
.end method

.method private saveAssessmentHeightValue()V
    .locals 3

    .prologue
    .line 935
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightUnit()I

    move-result v0

    .line 936
    .local v0, "heightUnit":I
    const/4 v1, 0x0

    .line 937
    .local v1, "heightValue":F
    const v2, 0x249f1

    if-ne v0, v2, :cond_0

    .line 938
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getCmValue()F

    move-result v1

    .line 946
    :goto_0
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentHeightUnit(I)V

    .line 947
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentHeightValue(F)V

    .line 948
    return-void

    .line 940
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getFtToCmValue()F

    move-result v1

    goto :goto_0
.end method

.method private saveAssessmentWeightValue()V
    .locals 3

    .prologue
    .line 951
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getWeightUnit()I

    move-result v0

    .line 952
    .local v0, "weightUnit":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getKgValue()F

    move-result v1

    .line 957
    .local v1, "weightValue":F
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentWeightUnit(I)V

    .line 958
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getKgValue()F

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentWeightValue(F)V

    .line 959
    return-void
.end method

.method private saveProfile()Z
    .locals 8

    .prologue
    const v3, 0x249f1

    const v4, 0x1fbd1

    const/4 v5, 0x0

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->checkEditFocus()Z

    move-result v6

    if-eqz v6, :cond_0

    move v3, v5

    .line 356
    :goto_0
    return v3

    .line 312
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v6, :cond_1

    .line 313
    new-instance v6, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 316
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getListValue()I

    move-result v6

    if-nez v6, :cond_2

    move v1, v3

    .line 318
    .local v1, "heightUnit":I
    :goto_1
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setHeightValue()V

    .line 319
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightValue()F

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    .line 320
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v6, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V

    .line 321
    if-ne v1, v3, :cond_3

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string v6, "cm"

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getDropDownListValue()I

    move-result v3

    if-nez v3, :cond_4

    move v2, v4

    .line 334
    .local v2, "weightUnit":I
    :goto_3
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setWeightValue()V

    .line 335
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getWeight()F

    move-result v6

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 336
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    .line 337
    if-ne v2, v4, :cond_5

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string v4, "kg"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 349
    :goto_4
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveWeightProfile(FF)V

    .line 351
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveUnitforUnitHelper(II)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 356
    const/4 v3, 0x1

    goto :goto_0

    .line 316
    .end local v1    # "heightUnit":I
    .end local v2    # "weightUnit":I
    :cond_2
    const v1, 0x249f2

    goto :goto_1

    .line 324
    .restart local v1    # "heightUnit":I
    :cond_3
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string v6, "ft, inch"

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 327
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    const-string v4, "Height range Error"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v5

    .line 329
    goto/16 :goto_0

    .line 332
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_4
    const v2, 0x1fbd2

    goto :goto_3

    .line 340
    .restart local v2    # "weightUnit":I
    :cond_5
    :try_start_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string v4, "lb"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_4

    .line 343
    :catch_1
    move-exception v0

    .line 344
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    const-string v4, "Weight range Error"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v5

    .line 345
    goto/16 :goto_0

    .line 352
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    move v3, v5

    .line 353
    goto/16 :goto_0
.end method

.method private saveUnitforUnitHelper(II)V
    .locals 2
    .param p1, "heightUnit"    # I
    .param p2, "weightUnit"    # I

    .prologue
    .line 378
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 382
    .local v0, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    const v1, 0x249f1

    if-ne p1, v1, :cond_2

    .line 383
    const-string v1, "cm"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V

    .line 387
    :goto_1
    const v1, 0x1fbd1

    if-ne p2, v1, :cond_3

    .line 388
    const-string v1, "kg"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    goto :goto_0

    .line 385
    :cond_2
    const-string v1, "ft, inch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V

    goto :goto_1

    .line 390
    :cond_3
    const-string v1, "lb"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private saveUserSelectIndex(I)V
    .locals 2
    .param p1, "userSelIndex"    # I

    .prologue
    .line 404
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionID()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->addAnswer(II)V

    .line 405
    return-void
.end method

.method private saveWeightProfile(FF)V
    .locals 5
    .param p1, "weight"    # F
    .param p2, "height"    # F

    .prologue
    .line 360
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 362
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "input_source_type"

    const v3, 0x3f7a1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 363
    const-string v2, "height"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 364
    const-string/jumbo v2, "weight"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 365
    const-string/jumbo v2, "sample_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 369
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    :goto_0
    return-void

    .line 371
    :catch_0
    move-exception v0

    .line 373
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error inserting Weight data from Coach Assessment screen. Invalid values: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setGestureModeNormal()V
    .locals 3

    .prologue
    .line 625
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeaderNormal:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 646
    const v1, 0x7f0800c1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 647
    .local v0, "headerScroll":Landroid/widget/ScrollView;
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 666
    return-void
.end method

.method private setGestureModeNormalBottom()V
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentBottom:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 712
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFirst:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentSecond:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentThird:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFourth:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 716
    return-void
.end method

.method private setGestureModeWeight()V
    .locals 3

    .prologue
    .line 669
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeaderWeight:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 689
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeaderWeight:Landroid/widget/RelativeLayout;

    const v2, 0x7f0800db

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 690
    .local v0, "weightHeaderScroll":Landroid/widget/ScrollView;
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 708
    return-void
.end method

.method private setNextFocusForWeight()V
    .locals 7

    .prologue
    const v6, 0x7f0801a5

    const v5, 0x7f0800e3

    const v4, 0x7f0802a4

    const v3, 0x7f0801a7

    const v2, 0x7f0800db

    .line 1012
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 1016
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0802a3

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 1020
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 1022
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 1026
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 1028
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    const v1, 0x7f0802a3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    const v1, 0x7f0802a3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 1030
    return-void
.end method

.method private showRangeDialog(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0301e3

    .line 814
    if-nez p1, :cond_0

    .line 816
    const-string v2, "ProfileSecondView"

    const-string v3, "View is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    :goto_0
    return-void

    .line 819
    :cond_0
    move-object v1, p1

    .line 820
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 821
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090ae2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 822
    const v2, 0x7f090047

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 823
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$7;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$7;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 832
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$8;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$8;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 839
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 882
    const/4 v0, 0x0

    goto :goto_0

    .line 842
    :sswitch_0
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$9;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 859
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 862
    :sswitch_1
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 879
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 839
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0801a7 -> :sswitch_0
        0x7f0801a9 -> :sswitch_0
        0x7f0802a4 -> :sswitch_1
    .end sparse-switch
.end method

.method private updateUserSeletIndex(I)V
    .locals 3
    .param p1, "selectedIndex"    # I

    .prologue
    const v2, 0x7f0200a9

    const v1, 0x7f020059

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFirst:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentSecond:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentThird:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFourth:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 414
    packed-switch p1, :pswitch_data_0

    .line 430
    :goto_0
    return-void

    .line 416
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFirst:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 419
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentSecond:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 422
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentThird:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 425
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFourth:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 414
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateYourTrackerSays(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "answerList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    const v8, 0x7f0800cf

    const v7, 0x7f0800cb

    const v6, 0x7f0800c7

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 434
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 435
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 436
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 437
    const v3, 0x7f0800d3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 439
    const/4 v2, 0x0

    .line 440
    .local v2, "preSelectIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 441
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    .line 442
    .local v0, "answer":Lcom/cigna/coach/apiobjects/Answer;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Answer;->isPreselected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 448
    .end local v0    # "answer":Lcom/cigna/coach/apiobjects/Answer;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_1

    .line 449
    packed-switch v2, :pswitch_data_0

    .line 466
    :cond_1
    :goto_1
    return-void

    .line 445
    .restart local v0    # "answer":Lcom/cigna/coach/apiobjects/Answer;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 440
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 451
    .end local v0    # "answer":Lcom/cigna/coach/apiobjects/Answer;
    :pswitch_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 454
    :pswitch_1
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 457
    :pswitch_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 460
    :pswitch_3
    const v3, 0x7f0800d3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 449
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getAnsweredIndex()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAnsweredIndex:I

    return v0
.end method

.method public hideKeyboard()V
    .locals 2

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    if-ne v0, v1, :cond_0

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 567
    :cond_0
    return-void
.end method

.method public isKeyboardShow()Z
    .locals 3

    .prologue
    .line 570
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    if-ne v1, v2, :cond_0

    .line 571
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 572
    .local v0, "mgr":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 573
    const/4 v1, 0x1

    .line 576
    .end local v0    # "mgr":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 223
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 301
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mListener:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mListener:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

    invoke-interface {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;->onClick(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V

    .line 304
    :cond_0
    return-void

    .line 226
    :sswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->addClickCount()V

    .line 227
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getClickCount()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->setAnsweredIndex(I)V

    .line 232
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveUserSelectIndex(I)V

    .line 233
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateUserSeletIndex(I)V

    goto :goto_0

    .line 238
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->addClickCount()V

    .line 239
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getClickCount()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->setAnsweredIndex(I)V

    .line 244
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveUserSelectIndex(I)V

    .line 245
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateUserSeletIndex(I)V

    goto :goto_0

    .line 250
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->addClickCount()V

    .line 251
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getClickCount()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->setAnsweredIndex(I)V

    .line 256
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveUserSelectIndex(I)V

    .line 257
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateUserSeletIndex(I)V

    goto :goto_0

    .line 262
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->addClickCount()V

    .line 263
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getClickCount()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->setAnsweredIndex(I)V

    .line 268
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveUserSelectIndex(I)V

    .line 269
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateUserSeletIndex(I)V

    goto :goto_0

    .line 274
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightUnit()I

    move-result v0

    const v1, 0x249f2

    if-ne v0, v1, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getFtToCmValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeightValueInCm:F

    .line 281
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getKgValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mWeightValueInKg:F

    .line 283
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeightValueInCm:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mWeightValueInKg:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_0

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeightValueInCm:F

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->setAnsweredValue(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mWeightAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mWeightValueInKg:F

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->setAnsweredValue(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getCmValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeightValueInCm:F

    goto :goto_1

    .line 223
    :sswitch_data_0
    .sparse-switch
        0x7f0800c6 -> :sswitch_0
        0x7f0800ca -> :sswitch_1
        0x7f0800ce -> :sswitch_2
        0x7f0800d2 -> :sswitch_3
        0x7f0800e3 -> :sswitch_4
    .end sparse-switch
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeaderBmpImg:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeaderBmpImg:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 762
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeaderBmpImg:Landroid/graphics/Bitmap;

    .line 765
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    if-eqz v0, :cond_1

    .line 766
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveAssessmentHeightValue()V

    .line 769
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    if-eqz v0, :cond_2

    .line 770
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->saveAssessmentWeightValue()V

    .line 772
    :cond_2
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 745
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 747
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGgestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    .line 749
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGgestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    .line 753
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGgestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    .line 756
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public setData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 12
    .param p1, "data"    # Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;
    .param p2, "position"    # I
    .param p3, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    .line 103
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v10

    sget-object v11, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    if-ne v10, v11, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->getContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f030027

    invoke-static {v10, v11, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 107
    const v10, 0x7f0800bc

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeaderNormal:Landroid/widget/RelativeLayout;

    .line 110
    const v10, 0x7f0800c5

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentBottom:Landroid/widget/LinearLayout;

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setGestureModeNormal()V

    .line 123
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeaderNormal:Landroid/widget/RelativeLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 126
    const v10, 0x7f0800bf

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mTopBackgroundImage:Landroid/widget/ImageView;

    .line 127
    const v10, 0x7f0800c3

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 128
    .local v7, "mTitleTextView":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionString()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    const v10, 0x7f0800c6

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFirst:Landroid/widget/RelativeLayout;

    .line 132
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFirst:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    const v10, 0x7f0800c9

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 134
    .local v0, "assessmentFirst_tvStr":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    const v10, 0x7f0800ca

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentSecond:Landroid/widget/RelativeLayout;

    .line 137
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentSecond:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    const v10, 0x7f0800cd

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 139
    .local v3, "assessmentSecond_tvStr":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    const v10, 0x7f0800ce

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentThird:Landroid/widget/RelativeLayout;

    .line 142
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentThird:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    const v10, 0x7f0800d1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 144
    .local v4, "assessmentThird_tvStr":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    const v10, 0x7f0800d2

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFourth:Landroid/widget/RelativeLayout;

    .line 147
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentFourth:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    const v10, 0x7f0800d5

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 149
    .local v1, "assessmentFourth_tvStr":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    const/4 v11, 0x3

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionID()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getAnswer(I)I

    move-result v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateUserSeletIndex(I)V

    .line 152
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerList()Ljava/util/List;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateYourTrackerSays(Ljava/util/List;)V

    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setGestureModeNormalBottom()V

    .line 189
    .end local v0    # "assessmentFirst_tvStr":Landroid/widget/TextView;
    .end local v1    # "assessmentFourth_tvStr":Landroid/widget/TextView;
    .end local v3    # "assessmentSecond_tvStr":Landroid/widget/TextView;
    .end local v4    # "assessmentThird_tvStr":Landroid/widget/TextView;
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionImageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateHeaderImage(Ljava/lang/String;)V

    .line 191
    return-void

    .line 157
    .end local v7    # "mTitleTextView":Landroid/widget/TextView;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->getContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f030029

    invoke-static {v10, v11, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    .line 159
    const v10, 0x7f0800d8

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeaderWeight:Landroid/widget/RelativeLayout;

    .line 160
    const v10, 0x7f0800da

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mTopBackgroundImage:Landroid/widget/ImageView;

    .line 161
    const v10, 0x7f0800d7

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->layout:Landroid/view/View;

    .line 162
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    const v11, 0x7f0800de

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGenderImage:Landroid/widget/ImageView;

    .line 163
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    const v11, 0x7f0800e0

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .line 164
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mView:Landroid/view/View;

    const v11, 0x7f0800e2

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    .line 166
    const v10, 0x7f0800dc

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 167
    .restart local v7    # "mTitleTextView":Landroid/widget/TextView;
    const v10, 0x7f0800e3

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 168
    .local v2, "assessmentNext":Landroid/widget/Button;
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionString()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 172
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->dialogInterface:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setOnCignaDialogListener(Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;)V

    .line 173
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->dialogInterface:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setOnCignaDialogListener(Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;)V

    .line 175
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->initUnit()V

    .line 176
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setGestureModeWeight()V

    .line 177
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setGenderImage(I)V

    .line 179
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getAssessmentWeightUnit()I

    move-result v8

    .line 180
    .local v8, "weightUnit":I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getAssessmentHeightUnit()I

    move-result v5

    .line 181
    .local v5, "heightUnit":I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getAssessmentWeightValue()F

    move-result v9

    .line 182
    .local v9, "weightValue":F
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getAssessmentHeightValue()F

    move-result v6

    .line 184
    .local v6, "heightValue":F
    invoke-virtual {p0, v5, v8, v6, v9}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateTrackingData(IIFF)V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setNextFocusForWeight()V

    goto/16 :goto_0
.end method

.method public setGenderImage(I)V
    .locals 2
    .param p1, "gender"    # I

    .prologue
    .line 195
    const v0, 0x2e635

    if-ne p1, v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGenderImage:Landroid/widget/ImageView;

    const v1, 0x7f0205eb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 200
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mGenderImage:Landroid/widget/ImageView;

    const v1, 0x7f0205db

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setOnItemClickListener(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mListener:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

    .line 396
    return-void
.end method

.method public setWeightAssessmentQAData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;)V
    .locals 0
    .param p1, "data"    # Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mWeightAssessmentQAData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    .line 214
    return-void
.end method

.method public updateHeaderImage(Ljava/lang/String;)V
    .locals 5
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 469
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 470
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getGoalImageResourceID(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 471
    .local v0, "resId":I
    if-eqz v0, :cond_0

    .line 472
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mContext:Landroid/content/Context;

    const v3, 0x7f0200bf

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getMaskedImage(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeaderBmpImg:Landroid/graphics/Bitmap;

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mTopBackgroundImage:Landroid/widget/ImageView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mHeaderBmpImg:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 479
    .end local v0    # "resId":I
    :cond_0
    :goto_0
    return-void

    .line 477
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "updateHeaderImage() imagePath is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateTrackingData(IIFF)V
    .locals 7
    .param p1, "heightUnit"    # I
    .param p2, "weightUnit"    # I
    .param p3, "heightValue"    # F
    .param p4, "weightValue"    # F

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/high16 v3, 0x42480000    # 50.0f

    const/4 v2, 0x0

    .line 963
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    cmpl-float v0, p3, v2

    if-nez v0, :cond_0

    cmpl-float v0, p4, v2

    if-nez v0, :cond_0

    .line 1009
    :goto_0
    return-void

    .line 967
    :cond_0
    if-eq p1, v4, :cond_1

    const v0, 0x249f1

    if-ne p1, v0, :cond_1

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setListValue(I)V

    .line 970
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    const v1, 0x249f1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setHeightUnit(I)V

    .line 978
    :goto_1
    if-eq p2, v4, :cond_2

    const v0, 0x1fbd1

    if-ne p2, v0, :cond_2

    .line 980
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setDropDownListValue(I)V

    .line 981
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    const v1, 0x1fbd1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setWeightUnit(I)V

    .line 989
    :goto_2
    int-to-float v0, p2

    invoke-static {v0, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_3

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setWeightPreviousValue(F)V

    .line 992
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setValue(F)V

    .line 999
    :goto_3
    invoke-static {p3, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_4

    .line 1001
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setValue(F)V

    goto :goto_0

    .line 974
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setListValue(I)V

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    const v1, 0x249f2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setHeightUnit(I)V

    goto :goto_1

    .line 985
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setDropDownListValue(I)V

    .line 986
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    const v1, 0x1fbd2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setWeightUnit(I)V

    goto :goto_2

    .line 996
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0, p4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setWeightPreviousValue(F)V

    .line 997
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0, p4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setValue(F)V

    goto :goto_3

    .line 1006
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentHeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setValue(F)V

    goto :goto_0
.end method
