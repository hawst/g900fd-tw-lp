.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;
.super Ljava/lang/Object;
.source "WalkingMateDayStepService.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->walkingMateServiceStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0

    .prologue
    .line 606
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivetimeChanged(J)V
    .locals 0
    .param p1, "duration"    # J

    .prologue
    .line 640
    return-void
.end method

.method public onHealthyStepCountChanged(J)V
    .locals 0
    .param p1, "steps"    # J

    .prologue
    .line 644
    return-void
.end method

.method public onHealthyStepStatusChanged(Z)V
    .locals 2
    .param p1, "isHealthyStep"    # Z

    .prologue
    .line 610
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateWidgets(Landroid/content/Context;)V

    .line 611
    return-void
.end method

.method public onInactiveTimeChanged(ZJZ)V
    .locals 3
    .param p1, "isInactive"    # Z
    .param p2, "duration"    # J
    .param p4, "isNoti"    # Z

    .prologue
    .line 615
    const-string v0, "WalkingMateDayStepService"

    const-string/jumbo v1, "onInactiveTimeChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    const-string v0, "WalkingMateDayStepService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isInactive = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    const-string v0, "WalkingMateDayStepService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "duration = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    const-string v0, "WalkingMateDayStepService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isNoti = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    if-nez p1, :cond_0

    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mInactiveNotify:Z

    if-eqz v0, :cond_0

    .line 621
    const-string/jumbo v0, "unset inactive Noti."

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 623
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mInactiveNotify:Z

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopNotification()V

    .line 627
    :cond_0
    # setter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mInactiveTime:J
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1002(J)J

    .line 628
    if-eqz p4, :cond_1

    .line 629
    const-string/jumbo v0, "set first inactive Noti."

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 630
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    const/4 v1, 0x4

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setNotification(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1100(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;I)V

    .line 631
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mInactiveNotify:Z

    .line 635
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateWidgets(Landroid/content/Context;)V

    .line 636
    return-void
.end method
