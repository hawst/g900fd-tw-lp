.class Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DecodeBitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final imageViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;Landroid/widget/ImageView;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->isUserFile()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$000(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$000(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;)Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->drawer_list_photo_bg_01:I

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->drawermenu_list_profile_height:I

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$000(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->contacts_default_caller_id:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$000(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$color;->bg_drawer_default_profile_picture:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$000(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;)Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->drawer_list_photo_bg_01:I

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->drawermenu_list_profile_height:I

    invoke-static {v2, v1, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_2
    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "newBitmap variable value became null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->this$0:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->isProfileImageChanged(Z)V

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->sProfileImageCache:Landroid/support/v4/util/LruCache;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$200()Landroid/support/v4/util/LruCache;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->sProfileImageCache:Landroid/support/v4/util/LruCache;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$200()Landroid/support/v4/util/LruCache;

    move-result-object v0

    const-string/jumbo v1, "profile_image"

    invoke-virtual {v0, v1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->sProfileImageCache:Landroid/support/v4/util/LruCache;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$200()Landroid/support/v4/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->maxSize()I

    move-result v0

    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByteCount(Landroid/graphics/Bitmap;)I

    move-result v1

    if-le v0, v1, :cond_0

    # getter for: Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->sProfileImageCache:Landroid/support/v4/util/LruCache;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->access$200()Landroid/support/v4/util/LruCache;

    move-result-object v0

    const-string/jumbo v1, "profile_image"

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter$DecodeBitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
