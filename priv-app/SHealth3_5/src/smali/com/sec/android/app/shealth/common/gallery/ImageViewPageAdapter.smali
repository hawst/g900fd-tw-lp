.class public Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ImageViewPageAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ImageViewPageAdapter"


# instance fields
.field private mBackgroundBitmapLoader:Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;

.field private mContext:Landroid/content/Context;

.field private mImagePath:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p2, "imagesPath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mImagePath:Ljava/util/List;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mContext:Landroid/content/Context;

    .line 52
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 61
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 63
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mImagePath:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 72
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->preparingAndCachingViewByPosition(I)Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    move-result-object v0

    .line 73
    .local v0, "scalableView":Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 75
    :cond_0
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 67
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public preparingAndCachingViewByPosition(I)Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 99
    new-instance v3, Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;-><init>(Landroid/content/Context;)V

    .line 100
    .local v3, "scalableView":Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mImagePath:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 103
    .local v2, "path":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByPath(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapWithCorrectingOrientationToNormal(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 105
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090028

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901e5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 117
    .end local v3    # "scalableView":Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    :goto_0
    return-object v3

    .line 108
    .restart local v3    # "scalableView":Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    :catch_0
    move-exception v1

    .line 111
    .local v1, "exception":Ljava/lang/IllegalArgumentException;
    const-string v4, "ImageViewPageAdapter"

    const-string v5, "Exception occured while creating the bitmap"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const/4 v3, 0x0

    goto :goto_0

    .line 113
    .end local v1    # "exception":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 4
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mBackgroundBitmapLoader:Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mBackgroundBitmapLoader:Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->cancel(Z)Z

    .line 83
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;

    check-cast p3, Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-direct {v0, p3}, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;-><init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mBackgroundBitmapLoader:Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mBackgroundBitmapLoader:Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->mImagePath:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 85
    return-void
.end method
