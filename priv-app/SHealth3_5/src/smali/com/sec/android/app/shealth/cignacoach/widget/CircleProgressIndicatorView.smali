.class public Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;
.super Landroid/widget/LinearLayout;
.source "CircleProgressIndicatorView.java"


# static fields
.field private static final MARGIN_BOTTOM:I

.field private static final MARGIN_LEFT:I

.field private static final MARGIN_RIGHT:I

.field private static final MARGIN_TOP:I

.field private static final PADDING_BOTTOM:I

.field private static final PADDING_LEFT:I

.field private static final PADDING_RIGHT:I

.field private static final PADDING_TOP:I


# instance fields
.field private final INDICATOR_HEIGHT:I

.field private final INDICATOR_LEFT_MARGIN:I

.field private final INDICATOR_WIDTH:I

.field private mIndicatorCount:I

.field private mProgressCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a026d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->INDICATOR_WIDTH:I

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a026e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->INDICATOR_HEIGHT:I

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a026f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->INDICATOR_LEFT_MARGIN:I

    .line 28
    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mIndicatorCount:I

    .line 29
    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mProgressCount:I

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->initLayout(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method private initLayout(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 45
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setOrientation(I)V

    .line 47
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 49
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setGravity(I)V

    .line 52
    invoke-virtual {p0, v3, v3, v3, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setPadding(IIII)V

    .line 53
    return-void

    .line 51
    :cond_0
    const/4 v1, 0x3

    goto :goto_0
.end method


# virtual methods
.method public setIndicatorCount(I)V
    .locals 6
    .param p1, "indicatorCount"    # I

    .prologue
    const/4 v4, 0x0

    .line 56
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mIndicatorCount:I

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->removeAllViews()V

    .line 60
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mIndicatorCount:I

    if-ge v0, v3, :cond_1

    .line 61
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 62
    .local v1, "indicatorView":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->INDICATOR_WIDTH:I

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->INDICATOR_HEIGHT:I

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 63
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-lez v0, :cond_0

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->INDICATOR_LEFT_MARGIN:I

    :goto_1
    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 64
    const v3, 0x7f020083

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v4

    .line 63
    goto :goto_1

    .line 67
    .end local v1    # "indicatorView":Landroid/widget/ImageView;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method public setProgressIndicatorCount(I)V
    .locals 6
    .param p1, "progressCount"    # I

    .prologue
    const/4 v5, 0x1

    .line 70
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mIndicatorCount:I

    if-le p1, v3, :cond_0

    iget p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mIndicatorCount:I

    .end local p1    # "progressCount":I
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mProgressCount:I

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 75
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 76
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mIndicatorCount:I

    add-int/lit8 v1, v3, -0x1

    .local v1, "index":I
    :goto_0
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mIndicatorCount:I

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mProgressCount:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    if-le v1, v3, :cond_2

    .line 78
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 79
    .local v2, "indicatorView":Landroid/widget/ImageView;
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 76
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 83
    .end local v1    # "index":I
    .end local v2    # "indicatorView":Landroid/widget/ImageView;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "index":I
    :goto_1
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->mProgressCount:I

    if-ge v1, v3, :cond_2

    .line 84
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 85
    .restart local v2    # "indicatorView":Landroid/widget/ImageView;
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 88
    .end local v2    # "indicatorView":Landroid/widget/ImageView;
    :cond_2
    return-void
.end method
